The following license covers:
- Maze Burrow _V1.mp4, located in "Promotional Materials/Trailer"
- Maze Burrow_v2.mp4, located in "Promotional Materials/Trailer"
- Maze_Burrow_v3.mp4, located in "Promotional Materials/Trailer"
- Mazz Burrow.prproj located in "Promotional Materials/Trailer/Trailer Source Files"

This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

***

The following license covers:
- All files in the "Humble Bundle", "Logo", and "Steam Page" subdirectories of the "Promotional Materials" directory

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
