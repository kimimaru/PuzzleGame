# Game Manual
The game manual and all related files, located inside the "Game Manual" directory, are licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

https://creativecommons.org/licenses/by-sa/4.0/

# Ideas Document
The ideas document, titled "Even Newer Puzzle Game Design.txt", is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

https://creativecommons.org/licenses/by-sa/4.0/
