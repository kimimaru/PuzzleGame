# Game Assets
The source forms of all game assets carry their own licenses and attribution requirements, which are listed below.

# Music

The following attribution and license covers these music files, which can be found in the "PuzzleGame/PuzzleGameShared/Content/Audio/Music" directory. Some may also be found in the "PuzzleGame/PuzzleGameShared/Content/Audio/Originals" directory:

"Puzzle Caves"
<br />"Puzzle Dreams"
<br />"Puzzle Dreams 2"
<br />"Digital Dreaming"
<br />"Puzzle Action"
<br />"Puzzle Action 2"
<br />"Puzzle Action 3"
<br />"Brain Teaser"
<br />"Quirky Puzzler"
<br />"Light Puzzles"
<br />"Cool Puzzle Groovin'"
<br />"Building Stuff"
<br />"Bounce Light"
<br />"Mellow Puzzler"
<br />"Puzzle Madness"

by Eric Matyas

www.soundimage.org

Soundimage International Public License

By exercising the Licensed Rights (defined below), You accept and agree to be bound by the terms and conditions of this Soundimage International Public License ("Public License"). To the extent this Public License may be interpreted as a contract, You are granted the Licensed Rights in consideration of Your acceptance of these terms and conditions, and the Licensor grants You such rights in consideration of benefits the Licensor receives from making the Licensed Material available under these terms and conditions.

Section 1 – Definitions.

1. Adapted Material means material subject to Copyright and Similar Rights that is derived from or based upon the Licensed Material and in which the Licensed Material is translated, altered, arranged, transformed, or otherwise modified in a manner requiring permission under the Copyright and Similar Rights held by the Licensor. For purposes of this Public License, where the Licensed Material is a musical work, performance, or sound recording, Adapted Material is always produced where the Licensed Material is synched in timed relation with a moving image.
2. Adapter's License means the license You apply to Your Copyright and Similar Rights in Your contributions to Adapted Material in accordance with the terms and conditions of this Public License.
3. Copyright and Similar Rights means copyright and/or similar rights closely related to copyright including, without limitation, performance, broadcast, sound recording, and Sui Generis Database Rights, without regard to how the rights are labeled or categorized. For purposes of this Public License, the rights specified in Section 2(b)(1)-(2) are not Copyright and Similar Rights.
4. Effective Technological Measures means those measures that, in the absence of proper authority, may not be circumvented under laws fulfilling obligations under Article 11 of the WIPO Copyright Treaty adopted on December 20, 1996, and/or similar international agreements.
5. Exceptions and Limitations means fair use, fair dealing, and/or any other exception or limitation to Copyright and Similar Rights that applies to Your use of the Licensed Material.
6. Licensed Material means the artistic or literary work, database, or other material to which the Licensor applied this Public License.
7. Licensed Rights means the rights granted to You subject to the terms and conditions of this Public License, which are limited to all Copyright and Similar Rights that apply to Your use of the Licensed Material and that the Licensor has authority to license.
8. Licensor means the individual(s) or entity(ies) granting rights under this Public License.
9. Share means to provide material to the public by any means or process that requires permission under the Licensed Rights, such as reproduction, public display, public performance, distribution, dissemination, communication, or importation, and to make material available to the public including in ways that members of the public may access the material from a place and at a time individually chosen by them.
10. Sui Generis Database Rights means rights other than copyright resulting from Directive 96/9/EC of the European Parliament and of the Council of 11 March 1996 on the legal protection of databases, as amended and/or succeeded, as well as other essentially equivalent rights anywhere in the world.
11. You means the individual or entity exercising the Licensed Rights under this Public License. Your has a corresponding meaning.

Section 2 – Scope.

1. License grant.
    A. Subject to the terms and conditions of this Public License, the Licensor hereby grants You a worldwide, royalty-free, non-sublicensable, non-exclusive, irrevocable license to exercise the Licensed Rights in the Licensed Material to:
reproduce and Share the Licensed Material, in whole or in part; and
produce, reproduce, and Share Adapted Material.
    B. Exceptions and Limitations. For the avoidance of doubt, where Exceptions and Limitations apply to Your use, this Public License does not apply, and You do not need to comply with its terms and conditions.
    C. Term. The term of this Public License is specified in Section 6(a).
    D. Media and formats; technical modifications allowed. The Licensor authorizes You to exercise the Licensed Rights in all media and formats whether now known or hereafter created, and to make technical modifications necessary to do so. The Licensor waives and/or agrees not to assert any right or authority to forbid You from making technical modifications necessary to exercise the Licensed Rights, including technical modifications necessary to circumvent Effective Technological Measures. For purposes of this Public License, simply making modifications authorized by this Section 2(a)(4) never produces Adapted Material.
    E. Downstream recipients.
        i. Offer from the Licensor – Licensed Material. Every recipient of the Licensed Material automatically receives an offer from the Licensor to exercise the Licensed Rights under the terms and conditions of this Public License.
        ii. No downstream restrictions. You may not offer or impose any additional or different terms or conditions on, or apply any Effective Technological Measures to, the Licensed Material if doing so restricts exercise of the Licensed Rights by any recipient of the Licensed Material.
    F. No endorsement. Nothing in this Public License constitutes or may be construed as permission to assert or imply that You are, or that Your use of the Licensed Material is, connected with, or sponsored, endorsed, or granted official status by, the Licensor or others designated to receive attribution as provided in Section 3(a)(1)(A)(i).
    G. 

2. Other rights.
    A. Moral rights, such as the right of integrity, are not licensed under this Public License, nor are publicity, privacy, and/or other similar personality rights; however, to the extent possible, the Licensor waives and/or agrees not to assert any such rights held by the Licensor to the limited extent necessary to allow You to exercise the Licensed Rights, but not otherwise.
    B. Patent and trademark rights are not licensed under this Public License.
    C. To the extent possible, the Licensor waives any right to collect royalties from You for the exercise of the Licensed Rights, whether directly or through a collecting society under any voluntary or waivable statutory or compulsory licensing scheme. In all other cases the Licensor expressly reserves any right to collect such royalties.

Section 3 – License Conditions.

Your exercise of the Licensed Rights is expressly made subject to the following conditions.

1. Attribution.
    A. If You Share the Licensed Material (including in modified form), You must:
        i. retain the following if it is supplied by the Licensor with the Licensed Material:
            a. identification of the creator(s) of the Licensed Material and any others designated to receive attribution, in any reasonable manner requested by the Licensor (including by pseudonym if designated);
            b. a copyright notice;
            c. a notice that refers to this Public License;
            d. a notice that refers to the disclaimer of warranties;
            e. a URI or hyperlink to the Licensed Material to the extent reasonably practicable;
        ii. indicate if You modified the Licensed Material and retain an indication of any previous modifications; and
        iii. indicate the Licensed Material is licensed under this Public License, and include the text of, or the URI or hyperlink to, this Public License.
    B. You may satisfy the conditions in Section 3(a)(1) in any reasonable manner based on the medium, means, and context in which You Share the Licensed Material. For example, it may be reasonable to satisfy the conditions by providing a URI or hyperlink to a resource that includes the required information.
    C. If requested by the Licensor, You must remove any of the information required by Section 3(a)(1)(A) to the extent reasonably practicable.
    D. If You Share Adapted Material You produce, the Adapter's License You apply must not prevent recipients of the Adapted Material from complying with this Public License.

2. Restrictions

The use of the Licensed Material in media that is obscene or pornographic is prohibited.

Section 4 – Sui Generis Database Rights.

Where the Licensed Rights include Sui Generis Database Rights that apply to Your use of the Licensed Material:

  1. for the avoidance of doubt, Section 2(a)(1) grants You the right to extract, reuse, reproduce, and Share all or a substantial portion of the contents of the database;
  2. if You include all or a substantial portion of the database contents in a database in which You have Sui Generis Database Rights, then the database in which You have Sui Generis Database Rights (but not its individual contents) is Adapted Material; and
  3. You must comply with the conditions in Section 3(a) if You Share all or a substantial portion of the contents of the database.

For the avoidance of doubt, this Section 4 supplements and does not replace Your obligations under this Public License where the Licensed Rights include other Copyright and Similar Rights.

Section 5 – Disclaimer of Warranties and Limitation of Liability.

1. Unless otherwise separately undertaken by the Licensor, to the extent possible, the Licensor offers the Licensed Material as-is and as-available, and makes no representations or warranties of any kind concerning the Licensed Material, whether express, implied, statutory, or other. This includes, without limitation, warranties of title, merchantability, fitness for a particular purpose, non-infringement, absence of latent or other defects, accuracy, or the presence or absence of errors, whether or not known or discoverable. Where disclaimers of warranties are not allowed in full or in part, this disclaimer may not apply to You.
2. To the extent possible, in no event will the Licensor be liable to You on any legal theory (including, without limitation, negligence) or otherwise for any direct, special, indirect, incidental, consequential, punitive, exemplary, or other losses, costs, expenses, or damages arising out of this Public License or use of the Licensed Material, even if the Licensor has been advised of the possibility of such losses, costs, expenses, or damages. Where a limitation of liability is not allowed in full or in part, this limitation may not apply to You.

1. The disclaimer of warranties and limitation of liability provided above shall be interpreted in a manner that, to the extent possible, most closely approximates an absolute disclaimer and waiver of all liability.

Section 6 – Term and Termination.

1. This Public License applies for the term of the Copyright and Similar Rights licensed here. However, if You fail to comply with this Public License, then Your rights under this Public License terminate automatically.
2. Where Your right to use the Licensed Material has terminated under Section 6(a), it reinstates:
    A. automatically as of the date the violation is cured, provided it is cured within 30 days of Your discovery of the violation; or
    B. upon express reinstatement by the Licensor.

For the avoidance of doubt, this Section 6(b) does not affect any right the Licensor may have to seek remedies for Your violations of this Public License.

1. For the avoidance of doubt, the Licensor may also offer the Licensed Material under separate terms or conditions or stop distributing the Licensed Material at any time; however, doing so will not terminate this Public License.
2. Sections 1, 5, 6, 7, and 8 survive termination of this Public License.

Section 7 – Other Terms and Conditions.

1. The Licensor shall not be bound by any additional or different terms or conditions communicated by You unless expressly agreed.
2. Any arrangements, understandings, or agreements regarding the Licensed Material not stated herein are separate from and independent of the terms and conditions of this Public License.

Section 8 – Interpretation.

1. For the avoidance of doubt, this Public License does not, and shall not be interpreted to, reduce, limit, restrict, or impose conditions on any use of the Licensed Material that could lawfully be made without permission under this Public License.
2. To the extent possible, if any provision of this Public License is deemed unenforceable, it shall be automatically reformed to the minimum extent necessary to make it enforceable. If the provision cannot be reformed, it shall be severed from this Public License without affecting the enforceability of the remaining terms and conditions.
3. No term or condition of this Public License will be waived and no failure to comply consented to unless expressly agreed to by the Licensor.
4. Nothing in this Public License constitutes or may be interpreted as a limitation upon, or waiver of, any privileges and immunities that apply to the Licensor or You, including from the legal processes of any jurisdiction or authority.

***

The following license covers these files, which can be found in the "PuzzleGame/PuzzleGameShared/Content/Audio/Music" directory:

* Level_Complete.mp3

As well as everything contained in the "Old Music" subdirectory.


Copyright (C) 2020 Eric Matyas (www.soundimage.org)

The exclusive license to use and distribute this music is granted to the author of the software, Thomas "Kimimaru" Deeb (kimimaru@posteo.net).

- You may not use this music for either commercial or non-commercial use alone or in any projects.
- You may not adapt or remix this music for any purpose.
- You may not resell the original version or remixed derivatives without the consent of the copyright holder.

# Sound Effects

The following license covers these files, which can be found in the "PuzzleGame/PuzzleGameShared/Content/Audio/SFX" directory:

* RockHitHard.mp3
* Tally.mp3

As well as the following in the "Old SFX" subdirectory:

* Impact Sound.mp3
* Impact Sound_2.mp3
* Impact Sound_Lowered.mp3
* Impact Sound_lowered.ogg
* Points Tally Sound_1.mp3
* Points Tally Sound_2.mp3
* Points Tally Sound_3.mp3
* Points Tally Sound_3a.mp3
* Points Tally Sound_4.mp3

Copyright (C) 2020 Eric Matyas (www.soundimage.org)

The exclusive license to use and distribute these sounds is granted to the author of the software, Thomas "Kimimaru" Deeb (kimimaru@posteo.net).

- You may not use these sounds for either commercial or non-commercial use alone or in any projects.
- You may not adapt or remix these sounds for any purpose.
- You may not resell the original version or remixed derivatives without the consent of the copyright holder.

*** 

The attribution and licenses below correspond to the given files, which can be found in the "PuzzleGame/PuzzleGameShared/Content/Audio/SFX" directory. Some may also be found in the "PuzzleGame/PuzzleGameShared/Content/Audio/Originals" directory:

* richerlandtv__gametinywarp.wav
<br />"GameTinyWarp" by RICHERlandTV, used under CC BY 3.0 / shortened from original
<br />https://freesound.org/people/RICHERlandTV/sounds/234802/
<br />http://creativecommons.org/licenses/by/3.0/

* 164549__adam-n__switch-click-4.wav
<br />"Switch click 4" by Adam_N
<br />https://freesound.org/people/Adam_N/sounds/164549/
<br />https://creativecommons.org/publicdomain/zero/1.0/

* 50561__broumbroum__sf3-sfx-menu-select.wav
<br />"sf3-sfx-menu-select" by broumbroum, used under CC BY 3.0 / intro shortened from original
<br />https://freesound.org/people/broumbroum/sounds/50561/
<br />http://creativecommons.org/licenses/by/3.0/

* 328120__kianda__powerup.wav
<br />"PowerUp" by kianda / shortened from original
<br />https://freesound.org/people/kianda/sounds/328120/
<br />https://creativecommons.org/publicdomain/zero/1.0/

* 404359__kagateni__success2.wav
<br />"Success2" by kagateni / shortened from original
<br />https://freesound.org/people/kagateni/sounds/404359/
<br />https://creativecommons.org/publicdomain/zero/1.0/

* 249618__vincentm400__invalid.mp3
<br />"Invalid" by VincentM400, used under CC BY 3.0 / shortened from original
<br />https://freesound.org/people/VincentM400/sounds/249618/
<br />http://creativecommons.org/licenses/by/3.0/

* myfox14__game-over-arcade.wav
<br />"Game Over Arcade" by myfox14
<br />https://freesound.org/people/myfox14/sounds/382310/
<br />https://creativecommons.org/publicdomain/zero/1.0/

* leszek_szary-shoot.wav
<br />"shoot" by Leszek_Szary
<br />https://freesound.org/people/Leszek_Szary/sounds/146730/
<br />https://creativecommons.org/publicdomain/zero/1.0/

* wim__stonebreak01.wav
<br />"stonebreak01" by WIM, used under CC BY 3.0 / shortened from original
<br />https://freesound.org/people/WIM/sounds/25064/
<br />http://creativecommons.org/licenses/by/3.0/

* lloydevans09_warping.wav
<br />"Warping" by LloydEvans09, used under CC BY 3.0 / shortened from original
<br />https://freesound.org/people/LloydEvans09/sounds/185849/
<br />http://creativecommons.org/licenses/by/3.0/

* terry93d_8-bit-rumbling.wav
<br />"8-Bit Rumbling" by Terry93D, trimmed to loop
<br />https://freesound.org/people/Terry93D/sounds/327946/
<br />https://creativecommons.org/publicdomain/zero/1.0/

* wjoojoo_tink01.wav
<br />"tink01" by wjoojoo, used under CC BY 3.0 / shortened from original
<br />https://freesound.org/people/wjoojoo/sounds/367417/
<br />http://creativecommons.org/licenses/by/3.0/

* plasterbrain_8bit-question-mark.wav
<br />"8bit Question Mark" by plasterbrain
<br />https://freesound.org/people/plasterbrain/sounds/399093/
<br />https://creativecommons.org/publicdomain/zero/1.0/

* plasterbrain_cartoon-ui-back-cancel.wav
<br />"Cartoon UI Back/Cancel" by plasterbrain / pitch lowered from original
<br />https://freesound.org/people/plasterbrain/sounds/423168/
<br />https://creativecommons.org/publicdomain/zero/1.0/

* lefty-studios_jumping.wav
<br />"Jumping SFX" by Lefty_Studios / shortened from original
<br />https://freesound.org/people/Lefty_Studios/sounds/369515/
<br />https://creativecommons.org/publicdomain/zero/1.0/

* completetask_0.mp3
<br />"Completion Sound." by HaelDB
<br />https://opengameart.org/content/completion-sound
<br />http://static.opengameart.org/OGA-BY-3.0.txt

* ambient_techno1.wav
<br />"Ambient Pulse Noise" by Gobusto, used under CC BY-SA 3.0
<br />https://opengameart.org/content/ambient-pulse-noise
<br />http://creativecommons.org/licenses/by-sa/3.0/

* jingle-win-synth-00.wav
<br />"Jingle_Win_Synth_00" by LittleRobotSoundFactory, used under CC BY 3.0 / shortened from original
<br />https://freesound.org/people/LittleRobotSoundFactory/sounds/274180/
<br />http://creativecommons.org/licenses/by/3.0/

***

The following license covers these files in the "PuzzleGame/PuzzleGameShared/Content/Audio/SFX" and "PuzzleGame/PuzzleGameShared/Content/Audio/Originals" directories:

* RockEnterPipe.bfxrsound
* RockEnterPipe.mp3
* Block_Move.mp3
* Block_Move2.mp3

As well as the following in the "Old SFX" subdirectory:

* Sfx 1, @Kimimaru (Nicole Marie T, 24.02.20).wav
* Sfx 2, @Kimimaru (Nicole Marie T, 24.02.20).wav
* Sfx, @kimimaru (4 Variations, Nicole Marie T).wav
* Sfx, @kimimaru (Edits, 2 Variations, Nicole Marie T, 24.02.20).wav

And lastly, everything contained in the "Enter Pipe" subdirectory of "Old SFX".


Copyright (C) 2020 Nicole Marie T (https://nicolemakesmusic.wixsite.com/nicolemariet)

The exclusive license to use and distribute these sounds is granted to the author of the software, Thomas "Kimimaru" Deeb (kimimaru@posteo.net).

- You may not use these sounds for either commercial or non-commercial use alone or in any projects.
- You may adapt, remix, and redistribute these sounds for listening purposes only, and the original author must be credited.
- You may not resell the original version or remixed derivatives without the consent of the copyright holder.

# Fonts

The following license applies to "Puzzlegame/PuzzleGameShared/Content/Fonts/FFFFORWA.TTF":

"FFF Forward" by FFF Web Media Inc.
www.fontsforflash.com

This disclaimer and license agreement is a legal agreement between you and FFF Web Media Inc.,(referred to as Fonts For Flash). By downloading these fonts, you agree that you have read and agreed to the disclaimer on the Fonts For Flash website.

Fonts may be used for personal and/or commercial projects such as websites, offline presentations and other multimedia ventures. 

These fonts may be used for single or multiple users depending on the type of license purchased. 

A font for which an unlimited user license is purchased, may be used on an unlimited amount of computers within the same organization only, and cannot be distributed to other companies or third parties under this agreement. FFF fonts may not be redistributed, modified or resold in any way without the written permission of Fonts For Flash. 

Fonts For Flash does not take any responsibility for any damage caused through use of these fonts, be it indirect, special, incidental or consequential damages (including damages for loss of business, loss of profits, interruption or the like).



These fonts are designed to work on the specific operating system for which they were purchased only. Fonts For Flash does not take responsibility for the correct workings of a font that has been converted from PC to Macintosh or vice versa, or by use in programs other than Macromedia Flash 5 or Flash MX and/or ways not specifically designed for. 

Ensure that you have read the Frequently Asked Questions and the user guide (provided with your download) in the FFF website for all the technical details.

All fonts are sold on an AS IS basis. 

All available characters can be seen on the font preview screen and all additional characters could be supplied by special order only.

You agree that you have viewed and tested the fonts that you wish to purchase and are satisfied with the quality and that you will not hold Fonts For Flash responsible in any way.Fonts are guaranteed to work provided that you follow the guidelines for its usage. Any problems will be addressed and corrected.


If there are any upgrades to the fonts you have purchased, these will be provided to you free of charge. It is understood that all fonts received by way of a free upgrade, are accepted under the same terms and conditions as stated here.



Purchasing any fonts means that you have understood and agreed to all the terms and conditions on this disclaimer.

Thank you.
The FFF Team




www.fontsforflash.com

***

The following license applies to "Puzzlegame/PuzzleGameShared/Content/Fonts/Upheaval TT -BRK-.ttf":

"Upheaval TT" by Brian Kent
kentpw@norwich.net
http://www.aenigmafonts.com/

DISCLAIMER<br />
¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
<br />-The font(s) in this zip file were created by me (Brian Kent).  All
of my Fonts are Freeware, you can use them any way you want to
(Personal use, Commercial use, or whatever).

-If you have a Font related site and would like to offer my fonts on
your site, go right ahead. All I ask is that you keep this text file
intact with the Font.

-You may not Sell or Distribute my Fonts for profit or alter them in
any way without asking me first.  [e-mail -  kentpw@norwich.net]

# Shaders

All shaders, located in the "PuzzleGame/PuzzleGameShared/Content/Shaders" folder, are licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

https://creativecommons.org/licenses/by-sa/4.0/

# Map Files

All map files, located in the "PuzzleGame/PuzzleGameShared/Content/TiledMaps" folder, are licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

https://creativecommons.org/licenses/by-sa/4.0/

# Sprites

All image and sprite files, located in the "PuzzleGame/PuzzleGameShared/Content/Sprites" folder, are licensed under a Creative Commons Attribution-ShareAlike 4.0 International License, aside from the exceptions listed below.

https://creativecommons.org/licenses/by-sa/4.0/

***

Exceptions include all files in the "PuzzleGame/PuzzleGameShared/Content/Sprites/Old Sprites/Tileset/Cave 32x32 Adaption (zaicuch)" directory, which are licensed under CC0 (https://creativecommons.org/share-your-work/public-domain/cc0/).

***

More exceptions include the following MonoGame logos which are contained in the "PuzzleGame/PuzzleGameShared/Content/Sprites" directory:

* CreditIcons.png
* CreditIcons.psd

The following license applies to these logos:

Logo Usage Guidelines
=====================
The MonoGame logo is a [registered trademark](https://trademarks.justia.com/864/56/monogame-86456908.html) and is a brand for official MonoGame projects and content. The MonoGame logo **is not** part of the Ms-PL license of the MonoGame project itself.

Please refer to the following guidelines when using the MonoGame logos:

1. The MonoGame logo can be used to link to the MonoGame project and credit MonoGame in your own works.
2. The MonoGame logo can be used only as a secondary brand in your own works, meaning the average viewer should know that this is not a MonoGame website, project, or publication. In short, don't use the MonoGame logo as your own logo.
3. The MonoGame logo can be printed on promotional products, such as t-shirts, as long as it's a secondary brand as described in point 2.
4. The MonoGame logo of your choice must be used **unaltered and in original colors and typography**.
5. The MonoGame logo cannot be used on commercial products without explicit permission of the MonoGame team. Displaying the logo in your game credits is not considered commercial use.

If you have questions or doubts about your usage of the logo, please file an issue on this repository or contact the team.
