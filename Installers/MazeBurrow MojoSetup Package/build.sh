#!/bin/bash
# flibitMojoSetup Build Script
# Written by Ethan "flibitijibibo" Lee
#
# This script is ripped off of Ryan's build-example script in MojoSetup.
# https://hg.icculus.org/icculus/mojosetup/file/default/examples/build-example.sh

# Stop if anything produces an error.
set -e

# Get in the flibitMojoSetup directory
cd "`dirname "$0"`"

# We need a folder name!
if [ -z "$1" ]; then
    echo "USAGE: $0 foldername" 1>&2
    exit 1
fi

# Get some initial variables...
APPID="$1"
BASEDIR=`pwd`/"$APPID"
source "$BASEDIR/details.txt"
if [ -z "$APPTITLE" ]; then
    echo "Missing APPTITLE argument." 1>&2
    echo "Please make sure $BASEDIR/details.txt exists." 1>&2
    exit 1
fi

# The real work happens below here.

# Show everything that we do here on stdout.
set -x

# Enter the folder we specified.
cd "$BASEDIR"

# this is a little nasty, but it works!
TOTALINSTALL=`du -sb data |perl -w -pi -e 's/\A(\d+)\s+data\Z/$1/;'`
perl -w -pi -e "s/\A\s*(local TOTAL_INSTALL_SIZE)\s*\=\s*\d+\s*;\s*\Z/\$1 = $TOTALINSTALL;\n/;" config.lua

# Clean up previous run, build fresh dirs for Base Archive.
rm -rf image "$APPID-installer" pdata.zip
cp -R ../base_image image

# Compile the Lua config script, put it in the base archive.
../bin/mojoluac -s -o "$BASEDIR/image/scripts/config.luac" "$BASEDIR/config.lua"

# Fill in the rest of the Base Archive...
cd "$BASEDIR"
cp -R data image/data
cp splash.bmp image/meta/

# Tweak up the permissions in the final image...
chmod -R a+r image
chmod -R go-w image
find image -type d -exec chmod a+x {} \;

# Make a .zip archive of the Base Archive dirs and nuke the originals...
cd image
zip -9r ../pdata.zip *
cd ..
rm -rf image

# Append the .zip archive to the mojosetup binary, so it's "self-extracting."
cp ../bin/installer "$APPID-installer"
../bin/make_self_extracting "$APPID-installer" pdata.zip
rm -f pdata.zip
mv "$APPID-installer" ../

# ... and that's that.
set +e
set +x
echo "Successfully built!"
echo "The installer is in '$APPID-installer' ..."

exit 0
