local TOTAL_INSTALL_SIZE = 87295931;
local _ = MojoSetup.translate

Setup.Package
{
    vendor = "Kimimaru",
    id = "MazeBurrow",
    description = "Maze Burrow",
    version = "0.7",
    splash = "splash.bmp",
    superuser = false,
    write_manifest = true,
    support_uninstall = true,
    recommended_destinations =
    {
        MojoSetup.info.homedir,
        "/opt/games",
        "/usr/local/games"
    },

    Setup.Readme
    {
        description = _("Maze Burrow Installer"),
        source = _("Linux.README")
    },

    Setup.Option
    {
        -- !!! FIXME: All this filter nonsense is because
        -- !!! FIXME:   source = "base:///some_dir_in_basepath/"
        -- !!! FIXME: doesn't work, since it wants a file when drilling
        -- !!! FIXME: for the final archive, not a directory. Fixing this
        -- !!! FIXME: properly is a little awkward, though.

        value = true,
        required = true,
        disabled = false,
        bytes = TOTAL_INSTALL_SIZE,
        description = "Maze Burrow",

        Setup.File
        {
            wildcards = "*";
        },

        Setup.DesktopMenuItem
        {
            disabled = false,
            name = "Maze Burrow",
            genericname = "MazeBurrow",
            tooltip = _("Solve puzzles to help the Echidna escape the burrow!"),
            builtin_icon = false,
            icon = "Icon.bmp",
            commandline = "%0/MazeBurrow",
            workingdir = "%0",
            category = "Game"
        }
    }
}

-- end of config.lua ...
