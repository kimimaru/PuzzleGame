# Maze Burrow
Move blocks into matching spots to unlock the portal to the next level. Defeat the moles and help the echidna escape the burrow in this Sokoban-inspired puzzler!

This is the repository for [Maze](https://kimimaru.itch.io/maze-burrow) [Burrow](https://store.steampowered.com/app/1244190/Maze_Burrow/), an indie puzzle game originally released on March 31, 2020. While the game is [free software](https://www.gnu.org/philosophy/free-sw.html) and each release comes packaged with source code, this repository has many more goodies, including source assets, ideas, and **the entire Git history of the project!** That's right: every phase of development Maze Burrow has been through is right here!

This repository also includes an updated .NET 6 port of Maze Burrow, allowing it to be compiled and run on all major desktop platforms. See the [OriginalRelease](https://codeberg.org/kimimaru/PuzzleGame/src/branch/OriginalRelease) branch for the repository as it was on the game's v1.0.4 release, which was built in the .NET Framework.

## Points of Interest
- Source code is located in the [PuzzleGame](./PuzzleGame) directory. See [instructions for building](./Building.md) to build the game for yourself.
- All game assets are included in the [Content](./PuzzleGame/PuzzleGameShared/Content/) directory.
- [Even Newer Puzzle Game Design.txt](./Even%20Newer%20Puzzle%20Game%20Design.txt) is a massive document with many different ideas written down during Maze Burrow's development. While many ideas made it in, even more were left out or scrapped entirely. Check it out!
- [Game Manual](./Game%20Manual/) contains the source assets and files used to generate the PDF manual. You can edit the **Manual.odt** file with [LibreOffice Writer](https://www.libreoffice.org/).

## Licenses
- Maze Burrow's source code is licensed under the [Mozilla Public License 2.0](https://www.mozilla.org/en-US/MPL/2.0/). See the [LICENSE](./LICENSE) file for all the details.
- [Third Party Licenses.txt](./Third%20Party%20Licenses.txt) covers the licenses of third party libraries used to develop Maze Burrow.
- [Game Asset Licenses.txt](./Game%20Asset%20Licenses.txt) covers licenses and attribution for game assets, including art, music, and sounds. **NOTE: NOT ALL game assets are under the same license or even free licenses!**
- [Misc Licenses.txt](./Misc%20Licenses.txt) covers licenses and attribution for the game manual and the ideas document.
- Licenses regarding the Maze Burrow logo and promotional materials are covered in the [Logo Name Promo Licenses.md](./Logo%20Name%20Promo%20Licenses.md) file.

Please read the license for the given materials before sharing them or using them in your own works.

## Copyright
Maze Burrow and all contents of this repository are copyright (C) 2020 Thomas "Kimimaru" Deeb, unless stated otherwise. Check [licenses](#licenses) for more information.
