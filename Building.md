# Building from source
This document describes how to build Maze Burrow from the source code. This section primarily covers the .NET 6 port of the game. For notes about compiling the original release version of the game, see the [OriginalRelease branch](#originalrelease-branch) section.

## Prerequisites
1. Clone the repo with `git clone https://codeberg.org/kimimaru/PuzzleGame.git`
2. [Install the .NET 6.0 SDK and Runtime](https://dotnet.microsoft.com/download/dotnet/6.0)
  * Before installing, set the `DOTNET_CLI_TELEMETRY_OPTOUT` environment variable to 1 if you don't want dotnet CLI commands sending telemetry.

Maze Burrow is built in the MonoGame framework. MonoGame packages are included in the repository, but if you run into troubles building, see their setup instructions [here](https://docs.monogame.net/articles/getting_started/0_getting_started.html) for more information.
  
## Build with CLI
Command line:
* Navigate to the **PuzzleGame/PuzzleGameGL** directory: `cd PuzzleGame/PuzzleGameGL`
* Building: `dotnet build`
* Publishing: `dotnet publish -c (config) -o (dir) --self-contained --runtime (RID)`
  * config = "Debug" or "Release"
  * dir = output directory
  * [RID](https://raw.githubusercontent.com/dotnet/runtime/main/src/libraries/Microsoft.NETCore.Platforms/src/runtime.json) = usually "win-x64" or "linux-x64". See link for a full list of runtime identifiers.
  * Example: `dotnet publish -c Release -o ~/Desktop/MazeBurrow --self-contained --runtime linux-x64`

Building this way will **also** build all of the game's assets!

From here you can navigate to the output folder then run the game, named "PuzzleGameGL". For example, `cd ~/Desktop/MazeBurrow` then `./PuzzleGameGL`. You may have to first mark it as executable on GNU/Linux or macOS with `chmod +x PuzzleGameGL`.

There hasn't been progress on making a .app bundle of the .NET 6 port for macOS. If you found a way to do so, please submit a pull request with instructions.

**NOTE**

The distort shader, used by the game when undoing moves, is not included in builds by default due to technical obstacles compiling it across all platforms. If the shader isn't present, the game will crash after undoing a move. See the [Building Assets](#building-assets) section for more information.

### DLL files
All pre-compiled DLL files are included in the "DLLs" folder. Here are the sources for them:

- MonoGame Framework
  - Build for DesktopGL off commit a854b98d1832a22cf3e7f61bd88c90e7bf93ef97
- MonoGame.Extended custom fork (https://github.com/kimimaru4000/MonoGame.Extended)
  - Build off commit 9f555744531041c077f93ad4f7da29336e14543f
  - This fork was based off the base MonoGame.Extended repository commit e107de530c564de6ed8eeda0c7862c27d461a8bd
  - Build MonoGame.Extended.Framework, MonoGame.Extended.Tiled, and MonoGame.Extended.Content.Pipeline, referencing the MonoGame DLL compiled from above
- Newtonsoft.Json 12.0.1 NuGet package

## Platform-specific preprocessor directives
- `WINDOWS` conditional compilation symbol for Windows builds
- `LINUX` conditional compilation symbol for GNU/Linux builds
- `MACOS` conditional compilation symbol for macOS builds

Set **one and only one** of these in the **PuzzleGameGL.csproj** file under the `DefineConstants` section of the Debug and Release profiles. Maze Burrow loads native code for each platform, thus specifying the incorrect directive for the desired platform will probably cause loads of problems.

# OriginalRelease branch
The original release of Maze Burrow can be found in the `OriginalRelease` branch of the repository. Since it was developed in .NET Framework, it has different build requirements.

Note that this branch requires the Windows operating system to build. It has not been tested with WINE or other compatibility layers.

## Environment:
- Visual Studio 2017 Community
- C# 7.3
- .NET Framework 4.7.2
- MonoGame DesktopGL template

Build with the included **PuzzleGame.sln** file from Visual Studio.

### GNU/Linux builds
- Built from Windows with the LINUX conditional compilation symbol
- Native builds cross-compiled with mkbundle using the Mono 5.18.0 runtime Debian 9 x64
  - Command: mkbundle --simple %inputname% -o %outputname% --cross mono-5.18.0-debian-9-x64 -L lib

### macOS builds
- Built from Windows with the MACOS conditional compilation symbol
  - Native builds made using MonoKickstart (https://github.com/flibitijibibo/MonoKickstart) commit a75aad7be2cad5cd7c9f8c0e42ae45c12d8fdbae
  - "osx" folder not present - the .dylibs are in the "Contents/Frameworks" folder of a .app bundle
  - If you don't want a .app bundle, cross-compile with mkbundle using mono-5.18.0-osx-10.9-x64

# Building Assets
Install the MonoGame Content Builder Editor through the CLI with `dotnet tool install --global dotnet-mgcb-editor --version 3.8.0.1641`.

Use the included **Content.mgcb** MonoGame Content Builder file in "PuzzleGame/PuzzleGameShared/Content" to build all assets. This folder contains all the source assets included in the game with many extras that are excluded. All assets are built for the **DesktopGL** platform with the **Reach** profile.

The **Content.mgcb** file has an overview of the asset list and structure. Assets are automatically built along with the game code.

**NOTE:** If you're on GNU/Linux and see an error similar to "Couldn't find a valid ICU package installed on the system.", then install the ICU package on your distribution. For Arch Linux, install the `libicu50` package through the AUR with `yay -S libicu50`. Alternatively, it may build if you disable globalization by setting the `DOTNET_SYSTEM_GLOBALIZATION_INVARIANT` environment variable to `true` or `1`.

## Shaders
Shaders are not included in the MonoGame Content Builder file because they require additional setup to be built on platforms other than Windows due to the use of the DirectX compiler. You can read the official MonoGame setup guide for each platform [here](https://docs.monogame.net/articles/getting_started/0_getting_started.html), which includes how to build shaders with Wine.

Shaders are also pre-compiled and can be found in the "Compiled Content/Shaders" directory. Copy the contents to the output folder of your build to allow the game to use them.

## Fonts
Fonts are included in the MonoGame Content Builder file. The spritefont files for them are as follows:

- Font.spritefont (Arial size 12)
- PixelFont.spritefont (FFFFORWA.ttf size 24)
- PixelFont2.spritefont (FFFFORWA.ttf size 18)
- PixelFont3.spritefont (FFFFORWA.ttf size 10)
- PixelFont4.spritefont (FFFFORWA.ttf size 13)
- PixelFont5.spritefont (FFFFORWA.ttf size 21)
- PixelFont6.spritefont (FFFFORWA.ttf size 12)
- PixelFont7.spritefont (FFFFORWA.ttf size 17)
- PixelFont8.spritefont (FFFFORWA.ttf size 15)
- PixelFontSpecial.spritefont (Upheaval TT -BRK-.ttf size 19)
- PixelFontSpecial2.spritefont (Upheaval TT -BRK-.ttf size 13)
 
All fonts use '?' as the default character, and the character regions are &#32; Start and &#256; End.

However, it's strongly recommended to use the pre-compiled fonts in the "Compiled Content/Fonts" directory. The **FFFFORWA.ttf** font in particular does not look the same when modified in any way or built on operating systems other than Windows. The exact reason is unknown, but it might be due to the age of the font and/or the software used to create it - which is also unknown.

## Map Files
Maze Burrow's levels are built with the [Tiled Map Editor](https://www.mapeditor.org/), version 1.2. Later versions may also work but haven't been tested.

You can import the **objecttypes.xml** file in the "Tiled Files" directory into Tiled to autocomplete properties on specific types of objects.

Reference the tileset file located in "PuzzleGame/PuzzleGameShared/Content/Sprites/Tilesets/Tileset.png". Make sure it's imported as **embedded** into the map!

**IMPORTANT**

Building Tiled maps relies on MonoGame.Extended's `TiledMapImporter`. The **Content.mgcb** file already has a reference to the DLL so it can process the maps.

In case it's needed, add the following to the **Content.mgcb** file to include a reference to MonoGame.Extended. Open **Content.mgcb** in a text editor and add the following above the "Content" section:

```
#-------------------------------- References --------------------------------#

/reference:..\..\..\DLLs\MonoGame.Extended.Content.Pipeline.dll

```
