@echo off
echo Cross compiling for macOS with mkbundle

SET /P inputname= Executable Name:
SET /P outputname= Output Name:

call mkbundle --simple %inputname% -o %outputname% --cross mono-5.18.0-osx-10.9-x64 -L lib

pause