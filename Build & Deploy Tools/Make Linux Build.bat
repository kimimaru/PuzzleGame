@echo off
echo Cross compiling for Linux with mkbundle

SET /P inputname= Executable Name:
SET /P outputname= Output Name:

call mkbundle --simple %inputname% -o %outputname% --cross mono-5.18.0-debian-9-x64 -L lib

pause