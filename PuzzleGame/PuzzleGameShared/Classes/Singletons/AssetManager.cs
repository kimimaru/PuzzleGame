﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Helps manage content
    /// <para>This is a Singleton</para>
    /// </summary>
    public class AssetManager : ICleanup
    {
        #region Singleton Fields

        public static AssetManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AssetManager();
                }

                return instance;
            }
        }

        public static bool HasInstance => (instance != null);

        private static AssetManager instance = null;

        #endregion

        public MBContentManager TiledMapsContent { get; private set; } = null;

        public MBContentManager MusicContent { get; private set; } = null;

        public MBContentManager SoundContent { get; private set; } = null;

        public MBContentManager FontContent { get; private set; } = null;

        public MBContentManager TextureContent { get; private set; } = null;

        public MBContentManager ShaderContent { get; private set; } = null;

        private AssetManager()
        {
            
        }

        public void Initialize(ContentManager content)
        {
            TiledMapsContent = new MBContentManager(content.ServiceProvider, ContentGlobals.ContentRoot);
            MusicContent = new MBContentManager(content.ServiceProvider, ContentGlobals.ContentRoot);
            SoundContent = new MBContentManager(content.ServiceProvider, ContentGlobals.ContentRoot);
            FontContent = new MBContentManager(content.ServiceProvider, ContentGlobals.ContentRoot);
            TextureContent = new MBContentManager(content.ServiceProvider, ContentGlobals.ContentRoot);
            ShaderContent = new MBContentManager(content.ServiceProvider, ContentGlobals.ContentRoot);
        }

        public void CleanUp()
        {
            //Unload the content
            UnloadLoadedContent();

            instance = null;
        }

        /// <summary>
        /// Loads a TiledMap with a specified name. <see cref="ContentGlobals.MapRoot"/> is used as the path.
        /// </summary>
        /// <param name="mapName">The name of the map.</param>
        /// <returns>A TiledMap instance if the map was successfully loaded, otherwise null.</returns>
        public TiledMap LoadTiledMap(in string mapName)
        {
            string fullPath = ContentGlobals.MapRoot + mapName;

            return LoadAsset<TiledMap>(fullPath, TiledMapsContent);
        }

        /// <summary>
        /// Loads a font with a specified name. <see cref="ContentGlobals.FontRoot"/> is used as the path.
        /// </summary>
        /// <param name="fontName">The name of the font.</param>
        /// <returns>A SpriteFont instance if the font was successfully loaded, otherwise null.</returns>
        public SpriteFont LoadFont(in string fontName)
        {
            string fullPath = ContentGlobals.FontRoot + fontName;

            return LoadAsset<SpriteFont>(fullPath, FontContent);
        }

        /// <summary>
        /// Loads a shader with a specified name. <see cref="ContentGlobals.ShaderRoot"/> is used as the path.
        /// </summary>
        /// <param name="shaderName">The name of the shader.</param>
        /// <returns>An Effect instance if the shader was successfully loaded, otherwise null.</returns>
        public Effect LoadShader(in string shaderName)
        {
            string fullPath = ContentGlobals.ShaderRoot + shaderName;

            return LoadAsset<Effect>(fullPath, ShaderContent);
        }

        /// <summary>
        /// Loads a texture with a specified name. <see cref="ContentGlobals.SpriteRoot"/> is used as the path.
        /// </summary>
        /// <param name="textureName">The name of the texture.</param>
        /// <returns>A Texture2D instance if the texture was successfully loaded, otherwise null.</returns>
        public Texture2D LoadTexture(in string textureName)
        {
            string fullPath = ContentGlobals.SpriteRoot + textureName;

            return LoadAsset<Texture2D>(fullPath, TextureContent);
        }

        /// <summary>
        /// Loads a SoundEffect containing music with a specified name. <see cref="ContentGlobals.MusicRoot"/> is used as the path.
        /// </summary>
        /// <param name="musicName">The name of the music to load.</param>
        /// <returns>A SoundEffect instance if the sound was successfully loaded, otherwise null.</returns>
        public SoundEffect LoadMusic(in string musicName)
        {
            string fullPath = ContentGlobals.MusicRoot + musicName;

            //Set the name of the music to exclude the path
            SoundEffect sound = LoadAsset<SoundEffect>(fullPath, MusicContent);
            if (sound != null) sound.Name = musicName;

            return sound;
        }

        /// <summary>
        /// Loads a SoundEffect containing a sound with a specified name. <see cref="ContentGlobals.SFXRoot"/> is used as the path.
        /// </summary>
        /// <param name="soundName">The name of the sound to load.</param>
        /// <returns>A SoundEffect instance if the sound was successfully loaded, otherwise null.</returns>
        public SoundEffect LoadSound(in string soundName)
        {
            string fullPath = ContentGlobals.SFXRoot + soundName;

            //Set the name of the sound to exclude the path
            SoundEffect sound = LoadAsset<SoundEffect>(fullPath, SoundContent);
            if (sound != null) sound.Name = soundName;

            return sound;
        }

        /// <summary>
        /// Load an asset of a particular type from a content manager.
        /// </summary>
        /// <typeparam name="T">The type of content to load.</typeparam>
        /// <param name="assetPath">The path to load the asset from.</param>
        /// <param name="contentManager">The ContentManager to load the asset from.</param>
        /// <returns>The asset if it was successfully found. Returns the same instance if the same asset was loaded previously.</returns>
        public T LoadAsset<T>(in string assetPath, MBContentManager contentManager)
        {
            //I opt for this rather than not handling the exception to make the content workflow less of a hassle for debug builds
            //I find that missing assets are very easy to spot, so just look at the logs if you notice an asset missing
            //Also, in the event there's no audio hardware, it'll throw an exception and let the player play without audio instead of crashing
            try
            {
                return contentManager.Load<T>(assetPath);
            }
            catch (NoAudioHardwareException noAudioHardwareException)
            {
                if (Engine.IgnoreAudioErrors == false)
                {
                    Debug.LogError($"Error loading sound {assetPath} due to audio hardware being unavailable. Full message: {noAudioHardwareException.Message}");
                }
                return default(T);
            }
            catch (Exception exception)
            {
                Debug.LogError($"Error loading asset {assetPath}: {exception.Message}\nTrace: {exception.StackTrace}");
                return default(T);
            }
        }

        /// <summary>
        /// Unloads all currently loaded content.
        /// </summary>
        public void UnloadLoadedContent()
        {
            TiledMapsContent.Unload();
            MusicContent.Unload();
            SoundContent.Unload();
            FontContent.Unload();
            TextureContent.Unload();
            ShaderContent.Unload();
        }

        /// <summary>
        /// Unloads all Tiled maps.
        /// </summary>
        public void UnloadTiledMaps()
        {
            TiledMapsContent.Unload();
        }

        /// <summary>
        /// Unloads all music.
        /// </summary>
        public void UnloadMusic()
        {
            MusicContent.Unload();
        }

        /// <summary>
        /// Unloads all sounds.
        /// </summary>
        public void UnloadSounds()
        {
            SoundContent.Unload();
        }

        /// <summary>
        /// Unloads all fonts.
        /// </summary>
        public void UnloadFonts()
        {
            FontContent.Unload();
        }

        /// <summary>
        /// Unloads all textures.
        /// </summary>
        public void UnloadTextures()
        {
            TextureContent.Unload();
        }

        /// <summary>
        /// Unloads all shaders.
        /// </summary>
        public void UnloadShaders()
        {
            ShaderContent.Unload();
        }

        /// <summary>
        /// Gets the total number of loaded assets.
        /// </summary>
        /// <returns>An integer representing the total number of loaded assets.</returns>
        public int GetTotalLoadedAssetCounts()
        {
            return (TiledMapsContent.LoadedAssetCount + MusicContent.LoadedAssetCount + SoundContent.LoadedAssetCount
                + FontContent.LoadedAssetCount + TextureContent.LoadedAssetCount + ShaderContent.LoadedAssetCount);
        }

        /// <summary>
        /// Loads in-game content to prevent stutters when loading them the first time.
        /// </summary>
        public void LoadInGameContent()
        {
            LoadMusic(ContentGlobals.LevelCompleteMusic);
            LoadShader(ContentGlobals.DistortShader);
        }
    }
}
