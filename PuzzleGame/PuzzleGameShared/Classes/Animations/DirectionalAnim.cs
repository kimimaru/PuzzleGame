﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// Handles sprite animation in four directions.
    /// </summary>
    public class DirectionalAnimation : IUpdateable, IAnimation
    {
        public Sprite SpriteToChange { get; set; } = null;
        public IDirectional DirectionalObj = null;
        protected readonly DirAnimationFrame[] AnimFrames = null;

        /// <summary>
        /// A key identifying the animation.
        /// </summary>
        public string Key { get; set; } = string.Empty;

        public int MaxFrameIndex => (AnimFrames.Length - 1);

        [Recordable(RecordableFlags.None)]
        public int CurFrameIndex { get; protected set; } = 0;

        public ref readonly DirAnimationFrame CurDirFrame => ref AnimFrames[CurFrameIndex];

        public AnimationFrame CurFrame
        {
            get
            {
                DirAnimationFrame frame = CurDirFrame;

                return new AnimationFrame(frame.DrawRegions[(int)DirectionalObj.FacingDir], frame.Duration, frame.Pivot);
            }
        }

        public AnimTypes AnimType = AnimTypes.Normal;
        public int Loops { get; private set; } = 0;

        [Recordable(RecordableFlags.None)]
        private double ElapsedFrameTime = 0d;

        public DirectionalAnimation(Sprite spriteToChange, IDirectional directionalObj, in AnimTypes animType, params DirAnimationFrame[] frames)
        {
            SpriteToChange = spriteToChange;
            DirectionalObj = directionalObj;
            AnimType = animType;
            AnimFrames = frames;
        }

        protected void Progress()
        {
            if (AnimType == AnimTypes.Normal)
            {
                CurFrameIndex = UtilityGlobals.Clamp(CurFrameIndex + 1, 0, MaxFrameIndex);
            }
            else
            {
                if ((CurFrameIndex + 1) > MaxFrameIndex)
                    Loops++;

                CurFrameIndex = UtilityGlobals.Wrap(CurFrameIndex + 1, 0, MaxFrameIndex);
            }

            ElapsedFrameTime = 0d;

            UpdateSpriteInfo(CurDirFrame, DirectionalObj.FacingDir);
        }

        public void Update()
        {
            ElapsedFrameTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedFrameTime >= CurFrame.Duration)
            {
                Progress();
            }
            else
            {
                UpdateSpriteInfo(CurDirFrame, DirectionalObj.FacingDir);
            }
        }

        /// <summary>
        /// Plays an animation from the start.
        /// </summary>
        public void Play()
        {
            CurFrameIndex = 0;

            ElapsedFrameTime = 0;
            Loops = 0;

            UpdateSpriteInfo(CurDirFrame, DirectionalObj.FacingDir);
        }

        private void UpdateSpriteInfo(in DirAnimationFrame frame, in Direction direction)
        {
            SpriteToChange.SourceRect = frame.DrawRegions[(int)direction];
            SpriteToChange.Pivot = frame.Pivot;
        }
    }
}
