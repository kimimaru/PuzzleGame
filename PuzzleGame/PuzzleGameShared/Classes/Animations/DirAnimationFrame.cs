﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// Represents a directional animation frame.
    /// </summary>
    public struct DirAnimationFrame : ICopyable<DirAnimationFrame>
    {
        public Rectangle[] DrawRegions;
        public double Duration;
        public Vector2 Pivot;

        public DirAnimationFrame(in double duration, params Rectangle[] drawRegions)
        {
            DrawRegions = drawRegions;
            Duration = duration;
            Pivot = new Vector2(.5f, .5f);
        }

        public DirAnimationFrame(in double duration, in Vector2 pivot, params Rectangle[] drawRegions)
        {
            DrawRegions = drawRegions;
            Duration = duration;
            Pivot = pivot;
        }

        public override bool Equals(object obj)
        {
            if (obj is DirAnimationFrame dirAnimFrame)
            {
                return (DrawRegions == dirAnimFrame.DrawRegions && Duration == dirAnimFrame.Duration && Pivot == dirAnimFrame.Pivot);
            }

            return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 19;
                hash = (hash * 23) + DrawRegions.GetHashCode();
                hash = (hash * 23) + Duration.GetHashCode();
                hash = (hash * 23) + Pivot.GetHashCode();
                return hash;
            }
        }

        public DirAnimationFrame Copy()
        {
            return new DirAnimationFrame(Duration, Pivot, DrawRegions);
        }
    }
}
