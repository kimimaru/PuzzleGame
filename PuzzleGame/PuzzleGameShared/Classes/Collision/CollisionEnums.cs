/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// The types of objects.
    /// </summary>
    public enum ObjectTypes
    {
        Player, Wall, Block, Spot, Portal, Switch, Warp, RockShooter, Rock
    }

    /// <summary>
    /// The types of collision.
    /// </summary>
    public enum CollisionTypes
    {
        Blocking = 1 << 0,
        Trigger = 1 << 1,
        All = Blocking | Trigger
    }

    /// <summary>
    /// The object layer of the collision.
    /// </summary>
    public enum CollisionLayers
    {
        Player = 1 << 0,
        Wall = 1 << 1,
        Block = 1 << 2,
        Spot = 1 << 3,
        Portal = 1 << 4,
        Switch = 1 << 5,
        Warp = 1 << 6,
        BlockExcept = 1 << 7,
        All = Player | Wall | Block | Spot | Portal | Switch | Warp | BlockExcept,
        AllExceptPlayer = All & (~Player)
    }

    /// <summary>
    /// The types of trigger queries.
    /// </summary>
    public enum TriggerQueries
    {
        None, Constant
    }
}