/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// Represents a row-major tile engine of <see cref="SurfaceTypes"/>.
    /// </summary>
    public class SurfaceTileEngine : TileEngine<SurfaceTypes>
    {
        private Dictionary<SurfaceTypes, int> MaxTileRegionCount = null;

        public SurfaceTileEngine(in Vector2 position, in Vector2 tileSize, in int columns, in int rows)
            : base(position, tileSize, columns, rows)
        {

        }

        /// <summary>
        /// Gets the most dominant tile in a region.
        /// </summary>
        /// <param name="objRect">A region to find the most dominant tile in.</param>
        /// <returns>The most dominant tile contained in <paramref name="objRect"/>.</returns>
        public SurfaceTypes GetTileInRegion(RectangleF objRect)
        {
            //Shortcut - if the size is less than or equal to the tile size, just use the position
            if (objRect.Width <= TileSize.X && objRect.Height <= TileSize.Y)
                return GetTile(objRect.Center);

            if (MaxTileRegionCount == null)
            {
                MaxTileRegionCount = new Dictionary<SurfaceTypes, int>(4);
            }

            MaxTileRegionCount.Clear();

            Vector2 size = objRect.Size;

            //Start from the top-left and half the tile size
            Vector2 start = objRect.TopLeft + TileSizeHalf;

            //Find out how many tiles the object takes up
            int tilesX = (int)(size.X / TileSize.X);
            int tilesY = (int)(size.Y / TileSize.Y);
            int objTiles = tilesX * tilesY;

            for (int i = 0; i < objTiles; i++)
            {
                int xOffset = i % tilesX;
                int yOffset = i / tilesY;

                //Use the initial position from the top-left and go forwards by the number of tiles
                Vector2 pos = start + new Vector2(xOffset * TileSize.X, yOffset * TileSize.Y);
                
                //Get the type of tile at this position
                SurfaceTypes tile = GetTile(pos);

                if (MaxTileRegionCount.ContainsKey(tile) == false)
                    MaxTileRegionCount.Add(tile, 0);
                MaxTileRegionCount[tile]++;
            }

            //Find the most dominant surface the object is on
            SurfaceTypes dominantSurface = default(SurfaceTypes);
            int max = -1;
            foreach (KeyValuePair<SurfaceTypes, int> val in MaxTileRegionCount)
            {
                //Update the dominant surface and max
                if (val.Value > max)
                {
                    max = val.Value;
                    dominantSurface = val.Key;
                }
                //If the value is equal, prioritize a lower surface value
                else if (val.Value == max && val.Key < dominantSurface)
                {
                    dominantSurface = val.Key;
                }
            }

            MaxTileRegionCount.Clear();

            return dominantSurface;
        }
    }
}