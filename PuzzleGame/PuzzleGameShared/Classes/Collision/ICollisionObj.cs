/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public interface ICollisionObj : ITransformable, ICleanup
    {
        /// <summary>
        /// A unique ID for the object.
        /// </summary>
        string ID { get; set; }

        /// <summary>
        /// The playable context of the object.
        /// </summary>
        PlayableContext Context { get; set; }

        ObjectTypes ObjectType { get; }

        CollisionTypes CollisionType { get; set; }

        CollisionLayers CollisionLayer { get; }

        TriggerQueries TriggerQuery { get; }

        RectangleF CollisionRect { get; }

        /// <summary>
        /// Queries trigger collisions with other objects.
        /// A return value of true means to end querying collisions for all other objects (Ex. the player steps into a portal).
        /// </summary>
        /// <param name="player"></param>
        /// <param name="collisions"></param>
        /// <returns>true if a collision should end querying collisions for all other objects, otherwise false.</returns>
        bool QueryTrigger(List<ICollisionObj> collisions);

        /// <summary>
        /// Initializes the object.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="initInfo"></param>
        void Initialize(PlayableContext context, in ObjectInitInfo initInfo);

        void PostInitialize(PlayableContext context);
    }
}