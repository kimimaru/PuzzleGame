/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class CollisionObject : ICollisionObj
    {
        public string ID { get; set; } = string.Empty;

        public PlayableContext Context { get; set; } = null;

        public Transform transform { get; private set; } = new Transform();

        public ObjectTypes ObjectType { get; private set; } = ObjectTypes.Wall;

        public CollisionTypes CollisionType { get; set; } = CollisionTypes.Blocking;

        public CollisionLayers CollisionLayer { get; protected set; }

        public TriggerQueries TriggerQuery { get; protected set; } = TriggerQueries.None;

        public RectangleF CollisionRect => new RectangleF(transform.Position, transform.Scale);

        public CollisionObject()
        {

        }

        public CollisionObject(in Vector2 position, in Vector2 size, in CollisionTypes collisionType,
            in CollisionLayers collObjType) : this()
        {
            CollisionType = collisionType;
            CollisionLayer = collObjType;

            transform.Position = position;
            transform.Scale = size;
        }

        public void CleanUp()
        {

        }

        public virtual void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            CollisionType = CollisionTypes.Blocking;
            CollisionLayer = CollisionLayers.Wall;

            transform.Position = initInfo.Position;
            transform.Scale = initInfo.Size;

            if (initInfo.Properties.TryGetValue(LevelGlobals.IDKey, out string idVal) == true)
            {
                ID = idVal;
            }
        }

        public void PostInitialize(PlayableContext context)
        {

        }

        public virtual bool QueryTrigger(List<ICollisionObj> collisions)
        {
            return false;
        }
    }
}