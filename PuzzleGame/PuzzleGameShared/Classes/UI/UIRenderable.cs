/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public abstract class UIRenderable : UIElement, ITintable, IRenderDepth
    {
        public Color TintColor { get; set; } = Color.White;
        public float RenderDepth { get; set; } = .5f;
        public bool Visible { get; set; } = true;

        public abstract void Render();
    }
}