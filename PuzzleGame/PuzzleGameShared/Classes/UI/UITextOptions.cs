/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// The options for rendering UI text.
    /// </summary>
    public enum UITextOptions
    {
        None = 0,
        Outline = 1 << 0,
        Shadow = 1 << 1,
        Individual = 1 << 2
    }

    public struct UITextData
    {
        public Color OutlineColor;
        public float OutlineThickness;
        public float OutlineDepthOffset;
        public Vector2 ShadowOffset;
        public Color ShadowColor;
        public float ShadowDepthOffset;

        public static UITextData Standard => new UITextData(Color.Black, 2f, -.001f, Vector2.Zero, Color.Transparent, 0f);

        public UITextData(in Color outlineColor, in float outlineThickness, in float outlineDepthOffset)
            : this(outlineColor, outlineThickness, outlineDepthOffset, default, default, default)
        {

        }

        public UITextData(in Color outlineColor, in float outlineThickness, in float outlineDepthOffset,
            in Vector2 shadowOffset, in Color shadowColor, in float shadowDepthOffset)
        {
            OutlineColor = outlineColor;
            OutlineThickness = outlineThickness;
            OutlineDepthOffset = outlineDepthOffset;
            ShadowOffset = shadowOffset;
            ShadowColor = shadowColor;
            ShadowDepthOffset = shadowDepthOffset;
        }
    }
}