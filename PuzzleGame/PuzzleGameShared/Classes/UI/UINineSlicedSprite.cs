/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class UINineSlicedSprite : UISprite
    {
        public NineSlicedSprite nineSlicedSprite { get; set; } = null;

        public UINineSlicedSprite(NineSlicedSprite uiNineSlicedSprite)
            : base(uiNineSlicedSprite)
        {
            nineSlicedSprite = uiNineSlicedSprite;
        }

        public override void Render()
        {
            //Don't throw an exception because we might want containers with no sprite
            if (sprite == null || sprite.Tex == null)
            {
                return;
            }
            
            //Fix miniscule errors in floating point numbers causing it to round down
            Vector2 pos = new Vector2((float)Math.Round(Position.X, 1), (float)Math.Round(Position.Y, 1));
            Rectangle nineSlicedRect = new Rectangle((int)pos.X, (int)pos.Y, (int)Scale.X, (int)Scale.Y);

            for (int i = 0; i < nineSlicedSprite.Slices; i++)
            {
                Rectangle nineRect = nineSlicedSprite.GetRectForIndex(nineSlicedRect, i);

                RenderingManager.Instance.DrawSprite(nineSlicedSprite.Tex, nineRect, nineSlicedSprite.Regions[i],
                    TintColor, 0f, Vector2.Zero, SpriteEffects.None, RenderDepth);
            }
        }
    }
}