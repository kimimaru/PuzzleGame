/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace PuzzleGame
{
    /// <summary>
    /// Helps manage UI menus.
    /// </summary>
    public class UIMenuManager
    {
        public Stack<UIInputMenu> MenuStack { get; protected set; } = null;
        private MenuTransition CurTransition = null;

        public UIMenuManager()
            : this(10)
        {
            
        }

        public UIMenuManager(in int capacity)
        {
            MenuStack = new Stack<UIInputMenu>(capacity);
        }

        public UIInputMenu Peek()
        {
            return MenuStack.Peek();
        }

        public UIInputMenu Pop()
        {
            return MenuStack.Pop();
        }

        public void Push(UIInputMenu uiInputMenu)
        {
            MenuStack.Push(uiInputMenu);
        }

        public void PushWithTransition(UIInputMenu uiInputMenu, MenuTransition transition)
        {
            MenuStack.Push(uiInputMenu);
            SetTransition(transition);
        }

        public void SetTransition(MenuTransition transition)
        {
            CurTransition = transition;
            CurTransition.StartTransition();
        }

        public void UpdateMenu()
        {
            if (CurTransition != null)
            {
                CurTransition.Update();
                if (CurTransition.InProgress == false)
                {
                    CurTransition = null;
                }

                return;
            }

            MenuStack.Peek().Update();
        }

        public void RenderMenu()
        {
            if (CurTransition != null)
            {
                CurTransition.RenderMenus();
                return;
            }

            MenuStack.Peek().Render();
        }
    }
}