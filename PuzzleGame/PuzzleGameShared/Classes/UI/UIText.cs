/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class UIText : UIRenderable
    {
        public SpriteFont Font { get; set; } = null;
        private Dictionary<char, SpriteFont.Glyph> FontGlyphs = null;

        private string TextStr = string.Empty;
        public string Text
        {
            get => TextStr;
            set
            {
                TextStr = value;
                Origin = OriginVal;
            }
        }

        public Vector2 TextOriginVal { get; private set; } = Vector2.Zero;

        private Vector2 OriginVal = Vector2.Zero;
        public Vector2 Origin
        {
            get => OriginVal;
            set
            {
                OriginVal = value;
                TextOriginVal = Font.GetOrigin(TextStr, value.X, value.Y);
            }
        }

        private UITextOptions uiTextOptions = UITextOptions.None;
        public UITextOptions TextOptions
        {
            get => uiTextOptions;
            set
            {
                uiTextOptions = value;
                if (EnumUtility.HasEnumVal((long)uiTextOptions, (long)UITextOptions.Individual) == true && FontGlyphs == null)
                {
                    FontGlyphs = Font.GetGlyphs();
                }
            }
        }

        public UITextData TextData = default(UITextData);

        public UIText(SpriteFont font, string text, in Vector2 origin)
        {
            Font = font;
            Text = text;
            Origin = origin;
        }

        public UIText(SpriteFont font, string text, in Vector2 origin, in UITextOptions textOptions)
            : this(font, text, origin)
        {
            TextOptions = textOptions;
        }

        public override void Render()
        {
            RenderBasedOnTextOption();
        }

        private void RenderBasedOnTextOption()
        {
            bool hasShadow = EnumUtility.HasEnumVal((long)TextOptions, (long)UITextOptions.Shadow);
            bool hasOutline = EnumUtility.HasEnumVal((long)TextOptions, (long)UITextOptions.Outline);

            if (EnumUtility.HasEnumVal((long)TextOptions, (long)UITextOptions.Individual) == false)
            {
                if (hasShadow == true)
                {
                    RenderingManager.Instance.spriteBatch.DrawStringWithShadowOnly(TextData.ShadowOffset, TextData.ShadowColor,
                        TextData.ShadowDepthOffset, Font, Text, Position, TintColor, Rotation, TextOriginVal, Scale, SpriteEffects.None, RenderDepth);
                }

                if (hasOutline == true)
                {
                    RenderingManager.Instance.spriteBatch.DrawStringOutlineOnly(TextData.OutlineThickness, TextData.OutlineColor, Font,
                        Text, Position, TintColor, Rotation, TextOriginVal, Scale, SpriteEffects.None, RenderDepth + TextData.OutlineDepthOffset);
                }

                RenderingManager.Instance.spriteBatch.DrawString(Font, Text, Position, TintColor, 0f, TextOriginVal, Scale, SpriteEffects.None,
                    RenderDepth);
            }
            else
            {
                Vector2 offset = Vector2.Zero;

                for (int i = 0; i < Text.Length; i++)
                {
                    char c = Text[i];

                    if (hasShadow == true)
                    {
                        RenderingManager.Instance.spriteBatch.DrawCharacterShadowOnly(TextData.ShadowOffset, TextData.ShadowColor,
                            TextData.ShadowDepthOffset, Font, c, FontGlyphs, offset, Position, TintColor, Rotation, TextOriginVal, Scale,
                            SpriteEffects.None, RenderDepth);
                    }

                    if (hasOutline == true)
                    {
                        RenderingManager.Instance.spriteBatch.DrawCharacterOutlineOnly(TextData.OutlineThickness, TextData.OutlineColor,
                            Font, c, FontGlyphs, offset, Position, TintColor, Rotation, TextOriginVal, Scale, SpriteEffects.None, RenderDepth + TextData.OutlineDepthOffset);
                    }

                    offset = RenderingManager.Instance.spriteBatch.DrawCharacter(Font, c, FontGlyphs, offset, Position, TintColor, Rotation,
                        TextOriginVal, Scale, SpriteEffects.None, RenderDepth);
                }
            }
        }
    }
}