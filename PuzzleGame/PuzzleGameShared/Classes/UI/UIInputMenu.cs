/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace PuzzleGame
{
    public class UIInputMenu : UIRenderableContainer, ICleanup
    {
        public delegate void MenuBackedOut();

        public int CurSelection { get; protected set; } = 0;
        public int MaxSelections => MenuOptions.Count;

        public int OptionSelectionAmount = 1;
        public int OptionChangeAmount = 1;

        protected SoundEffect OptionChangeSelectionSound = null;
        protected SoundEffect OptionChangedSound = null;
        protected SoundEffect OptionChosenSound = null;
        protected SoundEffect OptionBackSound = null;
        protected float OptionChangeSelectionVolume = 0f;
        protected float OptionChangedVolume = 0f;
        protected float OptionChosenVolume = 0f;
        protected float OptionBackVolume = 0f;

        protected MenuBackedOut OnBackedOut = null;
        public readonly List<UIMenuOption> MenuOptions = null;

        public UIInputMenu()
            : base(4, Vector2.Zero)
        {
            MenuOptions = new List<UIMenuOption>(4);
        }

        public UIInputMenu(in int initialElementCapacity, in Vector2 pos)
            : base(initialElementCapacity, pos)
        {
            MenuOptions = new List<UIMenuOption>(initialElementCapacity);
        }

        public virtual void CleanUp()
        {

        }

        public virtual void TransitionUpdate()
        {

        }

        public override void Update()
        {
            base.Update();

            HandleCursorInput();
            HandleSelectionInput();
        }

        protected virtual void HandleCursorInput()
        {
            if (Input.GetButtonDown(InputActions.Left) == true)
            {
                ChangeOption(-OptionChangeAmount, true);
            }
            else if (Input.GetButtonDown(InputActions.Right) == true)
            {
                ChangeOption(OptionChangeAmount, true);
            }
            else if (Input.GetButtonDown(InputActions.Down) == true)
            {
                ChangeSelection(OptionSelectionAmount, true);
            }
            else if (Input.GetButtonDown(InputActions.Up) == true)
            {
                ChangeSelection(-OptionSelectionAmount, true);
            }
        }

        protected virtual void HandleSelectionInput()
        {
            if (Input.GetButtonDown(InputActions.Select) == true)
            {
                UIMenuOption option = MenuOptions[CurSelection];
                if (option.OnOptionChosen != null)
                {
                    option.OnOptionChosen();

                    if (OptionChosenSound?.IsDisposed == false)
                    {
                        SoundManager.Instance.PlaySound(OptionChosenSound, false, OptionChosenVolume);
                    }
                }
            }
            else if (Input.GetButtonDown(InputActions.BackOut) == true)
            {
                BackOut();

                if (OptionBackSound?.IsDisposed == false)
                {
                    SoundManager.Instance.PlaySound(OptionBackSound, false, OptionBackVolume);
                }
            }
        }

        protected virtual void OnMenuBack()
        {
            
        }

        protected void BackOut()
        {
            OnMenuBack();

            OnBackedOut?.Invoke();
        }

        public void ChangeSelection(in int amount, in bool playSound)
        {
            int newSelection = UtilityGlobals.Wrap(CurSelection + amount, 0, MenuOptions.Count - 1);
            SetSelection(newSelection, playSound);
        }

        public void SetSelection(in int newSelectionIndex, in bool playSound)
        {
            //Deselect the previous option
            MenuOptions[CurSelection].OnOptionDeselected?.Invoke(CurSelection);

            CurSelection = newSelectionIndex;

            //Select the new option
            MenuOptions[CurSelection].OnOptionSelected?.Invoke(CurSelection);

            if (OptionChangeSelectionSound?.IsDisposed == false && playSound == true)
            {
                SoundManager.Instance.PlaySound(OptionChangeSelectionSound, false, OptionChangeSelectionVolume);
            }
        }

        public void ChangeOption(in int amount, in bool playSound)
        {
            MenuOptions[CurSelection].OnOptionChanged?.Invoke(amount);

            if (OptionChangedSound?.IsDisposed == false && playSound == true)
            {
                SoundManager.Instance.PlaySound(OptionChangedSound, false, OptionChangedVolume);
            }
        }

        public void AddMenuElement(UIRenderable element, in Vector2 position, in UIMenuOption menuOption)
        {
            AddElement(element, position);
            AddMenuElement(menuOption);
        }

        public void AddMenuElementRelative(UIRenderable element, in Vector2 position, in UIMenuOption menuOption)
        {
            AddElementRelative(element, position);
            AddMenuElement(menuOption);
        }

        public void AddMenuElement(UIRenderable element, in UIMenuOption menuOption)
        {
            AddElement(element);
            AddMenuElement(menuOption);
        }

        public void AddMenuElement(in UIMenuOption menuOption)
        {
            MenuOptions.Add(menuOption);
        }

        public void SetBackoutOption(in MenuBackedOut onBackedOut)
        {
            OnBackedOut = onBackedOut;
        }

        public void SetSounds(SoundEffect optionChangeSelectionSound, SoundEffect optionChosenSound, SoundEffect optionChangedSound, SoundEffect optionBackSound,
            in float optionChangeSelectionVolume, in float optionChosenVolume, in float optionChangedVolume, in float optionBackVolume)
        {
            OptionChangeSelectionSound = optionChangeSelectionSound;
            OptionChosenSound = optionChosenSound;
            OptionChangedSound = optionChangedSound;
            OptionBackSound = optionBackSound;

            OptionChangeSelectionVolume = optionChangeSelectionVolume;
            OptionChosenVolume = optionChosenVolume;
            OptionChangedVolume = optionChangedVolume;
            OptionBackVolume = optionBackVolume;
        }

        public void ResetSelectionStates()
        {
            for (int i = 0; i < MenuOptions.Count; i++)
            {
                MenuOptions[i].OnOptionDeselected?.Invoke(i);
            }

            MenuOptions[CurSelection].OnOptionSelected?.Invoke(CurSelection);
        }
    }
}