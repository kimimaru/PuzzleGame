/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class UISprite : UIRenderable
    {
        public Sprite sprite { get; set; } = null;

        public UISprite(Sprite uiSprite)
        {
            sprite = uiSprite;
        }

        public UISprite(Sprite uiSprite, in Vector2 scale)
            : this(uiSprite)
        {
            Scale = scale;
        }

        public override void Render()
        {
            //Don't throw an exception because we might want containers with no sprite
            if (sprite != null && sprite.Tex != null)
            {
                RenderingManager.Instance.DrawSprite(sprite.Tex, Position, sprite.SourceRect, TintColor, Rotation, sprite.GetOrigin(),
                    Scale, SpriteEffects.None, RenderDepth);
            }
        }
    }
}