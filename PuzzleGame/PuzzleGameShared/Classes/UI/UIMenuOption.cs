/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;

namespace PuzzleGame
{
    public struct UIMenuOption
    {
        public delegate void OptionSelected(in int optionIndex);
        public delegate void OptionDeselected(in int optionIndex);
        public delegate void OptionChosen();
        public delegate void OptionChanged(in int amount);

        public OptionSelected OnOptionSelected;
        public OptionDeselected OnOptionDeselected;
        public OptionChosen OnOptionChosen;
        public OptionChanged OnOptionChanged;

        public UIMenuOption(OptionSelected optionSelected, OptionDeselected optionDeselected,
            OptionChosen optionChosen, OptionChanged optionChanged)
        {
            OnOptionSelected = optionSelected;
            OnOptionDeselected = optionDeselected;
            OnOptionChosen = optionChosen;
            OnOptionChanged = optionChanged;
        }

        public override bool Equals(object obj)
        {
            if (obj is UIMenuOption uiMenuOption)
            {
                return (this == uiMenuOption);
            }

            return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 43;

                hash = (hash * 29) + OnOptionSelected.GetHashCode();
                hash = (hash * 29) + OnOptionDeselected.GetHashCode();
                hash = (hash * 29) + OnOptionChosen.GetHashCode();
                hash = (hash * 29) + OnOptionChanged.GetHashCode();

                return hash;
            }
        }

        public static bool operator ==(UIMenuOption a, UIMenuOption b)
        {
            return (a.OnOptionSelected == b.OnOptionSelected && a.OnOptionDeselected == b.OnOptionDeselected
                && a.OnOptionChosen == b.OnOptionChosen && a.OnOptionChanged == b.OnOptionChanged);
        }

        public static bool operator !=(UIMenuOption a, UIMenuOption b)
        {
            return !(a == b);
        }
    }
}