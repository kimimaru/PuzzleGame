/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace PuzzleGame
{
    public static class UIHelpers
    {
        public static void SetDefaultMenuSounds(UIInputMenu uiMenu, in bool includeChanged, in bool includeBack)
        {
            SoundEffect optionSelectionChangeSound = AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionChangeSound);
            SoundEffect optionChosenSound = AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound);
            SoundEffect optionChangedSound = null;
            SoundEffect optionBackSound = null;

            if (includeChanged == true)
            {
                optionChangedSound = optionSelectionChangeSound;
            }

            if (includeBack == true)
            {
                optionBackSound = AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionBackSound);
            }

            uiMenu.SetSounds(optionSelectionChangeSound, optionChosenSound, optionChangedSound, optionBackSound, 1f, 1f, 1f, .8f);
        }

        public static UIText CreateCenterUIText(SpriteFont font, string text, in Vector2 scale, in Color color)
        {
            UIText uiText = CreateCenterUIText(font, text, scale);
            uiText.TintColor = color;

            return uiText;
        }

        public static UIText CreateCenterUIText(SpriteFont font, string text, in Vector2 scale)
        {
            UIText uiText = new UIText(font, text, new Vector2(.5f, .5f));
            uiText.Scale = scale;

            return uiText;
        }

        public static UIText CreateCenterUIText(SpriteFont font, string text, in Vector2 scale,
            in UITextOptions textOptions, in UITextData textData)
        {
            UIText uiText = new UIText(font, text, new Vector2(.5f, .5f));
            uiText.Scale = scale;
            uiText.TextOptions = textOptions;
            uiText.TextData = textData;

            return uiText;
        }

        public static UIText CreateCenterUIText(SpriteFont font, string text, in Vector2 scale, in Color tintColor,
            in UITextOptions textOptions, in UITextData textData)
        {
            UIText uiText = new UIText(font, text, new Vector2(.5f, .5f));
            uiText.Scale = scale;
            uiText.TintColor = tintColor;
            uiText.TextOptions = textOptions;
            uiText.TextData = textData;

            return uiText;
        }

        public static UIText CreateUIText(SpriteFont font, string text, in Vector2 origin, in Vector2 scale)
        {
            UIText uiText = new UIText(font, text, origin);
            uiText.Scale = scale;

            return uiText;
        }

        public static UIText CreateUIText(SpriteFont font, string text, in Vector2 origin, in Vector2 scale, in Color color)
        {
            UIText uiText = new UIText(font, text, origin);
            uiText.Scale = scale;
            uiText.TintColor = color;

            return uiText;
        }

        public static UIText CreateUIText(SpriteFont font, string text, in Vector2 origin, in Vector2 scale,
            in UITextOptions textOptions, in UITextData textData)
        {
            UIText uiText = new UIText(font, text, origin);
            uiText.Scale = scale;
            uiText.TextOptions = textOptions;
            uiText.TextData = textData;

            return uiText;
        }

        public static UIText CreateUIText(SpriteFont font, string text, in Vector2 origin, in Vector2 scale, in Color color,
            in UITextOptions textOptions, in UITextData textData)
        {
            UIText uiText = new UIText(font, text, origin);
            uiText.Scale = scale;
            uiText.TintColor = color;
            uiText.TextOptions = textOptions;
            uiText.TextData = textData;

            return uiText;
        }

        public static UISprite CreateCenteredUISprite(Sprite sprite, in Vector2 scale, in Color color)
        {
            sprite.Pivot = new Vector2(.5f, .5f);
            return CreateUISprite(sprite, scale, color);
        }

        public static UISprite CreateCenteredUISprite(Texture2D tex, Rectangle? sourceRect, in Vector2 scale, in Color color)
        {
            return CreateUISprite(tex, sourceRect, new Vector2(.5f, .5f), scale, color);
        }

        public static UISprite CreateUISprite(Texture2D tex, Rectangle? sourceRect, in Vector2 pivot, in Vector2 scale, in Color color)
        {
            return CreateUISprite(new Sprite(tex, sourceRect, pivot), scale, color);
        }

        public static UISprite CreateUISprite(Sprite sprite, in Vector2 scale, in Color color)
        {
            UISprite uiSprite = new UISprite(sprite);
            uiSprite.Scale = scale;
            uiSprite.TintColor = color;

            return uiSprite;
        }
    }
}