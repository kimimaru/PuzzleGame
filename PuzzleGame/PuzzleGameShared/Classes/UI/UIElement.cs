/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public abstract class UIElement : IUpdateable, IPosition, IRotatable, IScalable
    {
        public virtual Vector2 Position { get; set; } = Vector2.Zero;
        public virtual float Rotation { get; set; } = 0f;
        public virtual Vector2 Scale { get; set; } = Vector2.One;

        public virtual void Update()
        {

        }
    }
}