/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class UIRenderableContainer : UIRenderable
    {
        private Vector2 position = Vector2.Zero;
        public override Vector2 Position
        {
            get => position;
            set
            {
                Vector2 cur = position;
                position = value;

                //Vector2 diff = position - cur;

                for (int i = 0; i < Elements.Count; i++)
                {
                    UIRenderable element = Elements[i];
                    Vector2 diff = element.Position - cur;

                    element.Position = position + diff;
                    //element.Position += diff;
                }
            }
        }

        public List<UIRenderable> Elements = null;

        public UIRenderableContainer(in int initialElementCapacity, in Vector2 pos)
        {
            Elements = new List<UIRenderable>(initialElementCapacity);
            Position = pos;
        }

        public void AddElement(UIRenderable element)
        {
            Elements.Add(element);
        }

        public void AddElement(UIRenderable element, in Vector2 position)
        {
            Elements.Add(element);
            element.Position = position;
        }

        public void AddElementRelative(UIRenderable element, in Vector2 position)
        {
            Elements.Add(element);
            element.Position = Position + position;
        }

        public void RemoveElement(UIRenderable element)
        {
            Elements.Remove(element);
        }

        public override void Update()
        {
            base.Update();
            for (int i = 0; i < Elements.Count; i++)
            {
                Elements[i].Update();
            }
        }

        public override void Render()
        {
            for (int i = 0; i < Elements.Count; i++)
            {
                UIRenderable element = Elements[i];
                if (element.Visible == true)
                {
                    Elements[i].Render();
                }
            }
        }
    }
}