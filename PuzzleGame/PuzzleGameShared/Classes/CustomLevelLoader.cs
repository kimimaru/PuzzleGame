/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;
//using MonoGame.Extended.Content.Pipeline.Tiled;
//using MonoGame.Framework.Content.Pipeline.Builder;

namespace PuzzleGame
{
    /// <summary>
    /// A class to help load custom levels.
    /// </summary>
    public static class CustomLevelLoader
    {
        private const string LevelExtension = ".tmx";

        /// <summary>
        /// Loads a custom level with a level name.
        /// </summary>
        /// <param name="levelName">The name of the level.</param>
        /// <returns>A <see cref="TiledMapContent"/> with the level data if found, otherwise null.</returns>
        //public static TiledMapContent LoadLevel(in string levelName)
        //{
        //    string fullPath = ContentGlobals.CustomLevelPath + levelName + LevelExtension;
        //
        //    if (File.Exists(fullPath) == false)
        //    {
        //        Debug.LogError($"No level exists at path: {fullPath}");
        //        return null;
        //    }
        //
        //    return null;
        //    //TiledMapContent tiledMapContent = null;
        //    //
        //    //try
        //    //{
        //    //    PipelineManager PM = new PipelineManager(ContentGlobals.ProjectRoot, ContentGlobals.ProjectRoot, ContentGlobals.ProjectRoot);
        //    //    
        //    //    PM.Platform = Microsoft.Xna.Framework.Content.Pipeline.TargetPlatform.DesktopGL;
        //    //
        //    //    //Add the assembly for MonoGame Extended's Content Pipeline so the PipelineManager can build everything
        //    //    string assemblyPath = Path.Combine(Environment.CurrentDirectory, typeof(TiledMapContent).Assembly.ManifestModule.Name);
        //    //    //Debug.Log($"Assembly Path: {assemblyPath}");
        //    //    PM.AddAssembly(assemblyPath);
        //    //
        //    //    //Build the content using the TiledMapImporter then process it
        //    //    //NOTE: We also need to copy (or generate) the tileset images to this directory or the maps won't be able to load!
        //    //    PipelineBuildEvent builtContent = PM.BuildContent(fullPath, outputFilepath: fullPath, importerName: "TiledMapImporter",
        //    //        processorName: "TiledMapProcessor");
        //    //
        //    //    tiledMapContent = (TiledMapContent)PM.ProcessContent(builtContent);
        //    //
        //    //    //Delete the .mgcontent file
        //    //    File.Delete($"{ContentGlobals.CustomLevelPath}{levelName}.mgcontent");
        //    //}
        //    //catch(Exception e)
        //    //{
        //    //    Debug.LogError($"Error loading custom level at {fullPath}: {e.Message}\nStack Trace: {e.StackTrace}");
        //    //    return null;
        //    //}
        //    //
        //    //return tiledMapContent;
        //}
    }
}