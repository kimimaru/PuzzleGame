﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The game part of the state.
    /// </summary>
    public class IngameState : IGameState
    {
        public Camera2D camera { get; private set; } = null;
        public GameHUD gameHUD { get; private set; } = null;

        private IGameState LevelState = null;

        public int LevelNum { get; private set; } = -1;
        
        public LevelMap LevelMap { get; private set; } = null;

        public LevelEventTypes LevelEventType { get; private set; } = LevelEventTypes.Base;

        /// <summary>
        /// The playable context of the level.
        /// </summary>
        public PlayableContext Context { get; private set; } = null;

        public TiledMap tiledMap { get; private set; } = null;

        /// <summary>
        /// The stats for this level.
        /// </summary>
        public readonly LevelStats levelStats = new LevelStats();

        /// <summary>
        /// Demonstration data for this level.
        /// This is referenced when a level is completed to determine which demonstrations to unlock.
        /// </summary>
        public readonly DemonstrationData DemoData = new DemonstrationData();

        //For undoing
        public readonly List<RecordingGlobals.RecordedLevelData> RecordedMoveList = new List<RecordingGlobals.RecordedLevelData>(RecordingGlobals.MaxMoveRecords);

        //For displaying level information in the pause screen
        public string DisplayLevelNum { get; private set; } = string.Empty;
        public string DisplayLevelName { get; private set; } = string.Empty;
        public string DisplayBestMoves { get; private set; } = string.Empty;
        public string DisplayBestTime { get; private set; } = string.Empty;

        /// <summary>
        /// A RenderTarget used to draw the map.
        /// </summary>
        private RenderTarget2D MapRenderTarget = null;
        private bool RenderTargetRendered = false;

        public IngameState(TiledMap tMap, in LevelEventTypes levelEventType)
        {
            tiledMap = tMap;
            LevelEventType = levelEventType;

            DemoData.Reset();
            Initialize();
        }

        public IngameState(in int levelNum, TiledMap tMap, in LevelEventTypes levelEventType)
            : this(tMap, levelEventType)
        {
            LevelNum = levelNum;
        }

        public IngameState(in int levelNum, TiledMap tMap, in LevelEventTypes levelEventType,
            string displayLevelNum, string displayLevelName, string displayBestMoves, string displayBestTime)
            : this(levelNum, tMap, levelEventType)
        {
            DisplayLevelNum = displayLevelNum;
            DisplayLevelName = displayLevelName;
            DisplayBestMoves = displayBestMoves;
            DisplayBestTime = displayBestTime;
        }

        protected void InitializeLevelObjects()
        {
            Context.AddObject(LevelGlobals.CreatePortal(Context, LevelMap.PortalTile));

            for (int i = 0; i < LevelMap.CollisionLayer.Objects.Length; i++)
            {
                TiledMapObject obj = LevelMap.CollisionLayer.Objects[i];
                if (obj.IsVisible == false) continue;

                Context.AddObject(LevelGlobals.CreateWall(Context, obj));
            }

            for (int i = 0; i < LevelMap.BlockLayer.Objects.Length; i++)
            {
                TiledMapObject obj = LevelMap.BlockLayer.Objects[i];
                if (obj.IsVisible == false) continue;

                Block b = LevelGlobals.CreateBlock(Context, obj);
                Context.AddObject(b);

                if (b.BlockDirection != null && b.InitGrabbable == true)
                {
                    DemoData.HasDirections = true;
                }

                if (b.BlockType == BlockTypes.Pipe)
                {
                    DemoData.HasPipes = true;
                }

                if (b.BlockType == BlockTypes.Paired)
                {
                    DemoData.HasPaired = true;
                }
            }

            for (int i = 0; i < LevelMap.SpotLayer.Objects.Length; i++)
            {
                TiledMapObject obj = LevelMap.SpotLayer.Objects[i];
                if (obj.IsVisible == false) continue;

                Context.AddObject(LevelGlobals.CreateSpot(Context, obj));

                DemoData.HasSpot = true;
            }

            if (LevelMap.WarpLayer != null)
            {
                for (int i = 0; i < LevelMap.WarpLayer.Objects.Length; i++)
                {
                    TiledMapObject obj = LevelMap.WarpLayer.Objects[i];
                    if (obj.IsVisible == false) continue;

                    Context.AddObject(LevelGlobals.CreateWarp(Context, obj));

                    DemoData.HasWarps = true;
                }
            }

            if (LevelMap.SwitchLayer != null)
            {
                for (int i = 0; i < LevelMap.SwitchLayer.Objects.Length; i++)
                {
                    TiledMapObject obj = LevelMap.SwitchLayer.Objects[i];
                    if (obj.IsVisible == false) continue;

                    Switch s = LevelGlobals.CreateSwitch(Context, obj);
                    Context.AddObject(s);

                    DemoData.HasSwitch = true;
                }
            }

            if (LevelMap.RockShooterLayer != null)
            {
                for (int i = 0; i < LevelMap.RockShooterLayer.Objects.Length; i++)
                {
                    TiledMapObject obj = LevelMap.RockShooterLayer.Objects[i];
                    if (obj.IsVisible == false) continue;

                    Context.AddObject(LevelGlobals.CreateRockShooter(Context, obj));
                }
            }

            TiledMapObjectLayer cutsceneTriggerLayer = LevelMap.Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.CutsceneTriggerLayerName);

            if (cutsceneTriggerLayer != null)
            {
                for (int i = 0; i < cutsceneTriggerLayer.Objects.Length; i++)
                {
                    TiledMapObject obj = cutsceneTriggerLayer.Objects[i];
                    if (obj.IsVisible == false) continue;

                    CutsceneTrigger trigger = new CutsceneTrigger();
                    trigger.Initialize(Context, (ObjectInitInfo)cutsceneTriggerLayer.Objects[i]);
                    Context.AddObject(trigger);
                }
            }
        }

        private void InitializeText()
        {
            TiledMapObjectLayer textLayer = LevelMap.TextLayer;
            if (textLayer != null && textLayer.IsVisible == true)
            {
                int count = textLayer.Objects.Length;
                for (int i = 0; i < count; i++)
                {
                    TiledMapObject obj = textLayer.Objects[i];
                    if (obj.IsVisible == false) continue;

                    Context.AddText(LevelGlobals.CreateLevelText(Context, obj));
                }
            }
        }

        private void InitializeTileEngine()
        {
            //Don't recreate the tile engine if it has already been created
            if (LevelMap.SurfaceTiles != null) return;

            LevelMap.SurfaceTiles = new SurfaceTileEngine(Vector2.Zero, LevelMap.TileSize, LevelMap.Map.Width, LevelMap.Map.Height);

            if (LevelMap.SurfaceLayer == null || LevelMap.SurfaceLayer.Objects == null || LevelMap.SurfaceLayer.Objects.Length == 0) return;

            for (int i = 0; i < LevelMap.SurfaceLayer.Objects.Length; i++)
            {
                TiledMapObject surfaceObj = LevelMap.SurfaceLayer.Objects[i];
                if (surfaceObj.IsVisible == false) continue;

                //No point in changing the tiles if this has no other surface type
                if (surfaceObj.Properties.TryGetValue(LevelGlobals.SurfaceTypeKey, out string val) == false)
                {
                    continue;
                }

                //Couldn't parse
                if (Enum.TryParse(val, true, out SurfaceTypes surfaceType) == false)
                {
                    continue;
                }

                if (surfaceType == SurfaceTypes.Ice)
                {
                    DemoData.HasIce = true;
                }

                if (surfaceType == SurfaceTypes.Mud)
                {
                    DemoData.HasMud = true;
                }

                //Get the start position of the object in tiles
                Vector2 start = surfaceObj.Position / LevelMap.TileSize;
                int startX = (int)start.X;
                int startY = (int)start.Y;

                //Get the width and height of the object in tiles
                int tileWidth = (int)(surfaceObj.Size.Width / LevelMap.TileSize.X);
                int tileHeight = (int)(surfaceObj.Size.Height / LevelMap.TileSize.Y);

                //Debug.Log($"Start: ({startX}, {startY}), Size: ({tileWidth}, {tileHeight})");

                int tileCount = tileWidth * tileHeight;

                //Go through each tile in the object region and set it to the correct surface type
                for (int j = 0; j < tileCount; j++)
                {
                    int column = (j % tileWidth) + startX;
                    int row = (j / tileWidth) + startY;

                    //Debug.Log($"col: {column}, row: {row}");

                    LevelMap.SurfaceTiles.ChangeTileAtRowCol(column, row, surfaceType);
                }
            }
        }

        private void Initialize()
        {
            if (LevelMap == null)
                LevelMap = new LevelMap(tiledMap);

            if (camera == null)
            {
                camera = new Camera2D();
            }
            camera.SetBounds(new Rectangle(0, 0, (int)RenderingGlobals.BaseResolution.X, (int)RenderingGlobals.BaseResolution.Y));

            //If there's a camera focus tile, use that position; otherwise, use the player's position
            Vector2 cameraStartPos = LevelMap.PlayerStartTile.Position;
            if (LevelMap.CameraFocusTile != null)
            {
                cameraStartPos = LevelMap.CameraFocusTile.Position;
            }

            //Set the initial camera position to the player's position, then clamp it
            camera.DefaultPosition = Camera2D.ClampCamera(cameraStartPos + LevelMap.TileSizeHalf,
                camera.ScreenBounds, new RectangleF(0f, 0f, LevelMap.Map.WidthInPixels, LevelMap.Map.HeightInPixels));
            camera.LookAt(camera.DefaultPosition);

            if (Context == null)
            {
                Context = new PlayableContext();
            }
            else
            {
                Context.ClearAllObjectsAndCamera();
            }

            Context.SetCamera(camera);

            Context.LevelSpace.TileSize = new Vector2(LevelMap.Map.TileWidth, LevelMap.Map.TileHeight);
            Context.LevelSpace.WidthInPixels = LevelMap.Map.WidthInPixels;
            Context.LevelSpace.HeightInPixels = LevelMap.Map.HeightInPixels;

            InitializeTileEngine();
            Context.SetSurfaceTiles(LevelMap.SurfaceTiles);

            Echidna echidna = new Echidna();
            echidna.Initialize(Context, (ObjectInitInfo)LevelMap.PlayerStartTile);

            Context.AddObject(echidna);

            InitializeLevelObjects();
            InitializeText();

            InitializeHUD();

            //Add the Echidna last so harmful objects (Ex. rocks) collide with other objects first if both would get hurt
            //Context.AddObject(echidna);

            RecordedMoveList.Clear();

            //Reset level stats
            levelStats.Clear();

            InitializeEvents();

            for (int i = 0; i < Context.LevelCollision.Count; i++)
            {
                Context.LevelCollision[i].PostInitialize(Context);
            }

            Context.ResetSpotsForPortal();
            Context.IncrementPortalUnlock(0);
        }

        protected void InitializeHUD()
        {
            if (gameHUD == null)
            {
                gameHUD = new GameHUD(this, LevelMap.AllowsUndo);
            }

            //Reset HUD
            gameHUD.Reset();
        }

        protected void InitializeEvents()
        {
            Context.Player.BlockMovedEvent -= OnPlayerBlockMoved;
            Context.Player.HitByStrongRockEvent -= OnPlayerHitByStrongRock;
            Context.LevelPortal.PlayerEnteredPortalEvent -= OnPlayerEnterPortal;

            if (LevelEventType == LevelEventTypes.Base)
            {
                Context.Player.BlockMovedEvent += OnPlayerBlockMoved;
                Context.Player.HitByStrongRockEvent += OnPlayerHitByStrongRock;
                Context.LevelPortal.PlayerEnteredPortalEvent += OnPlayerEnterPortal;
            }
        }

        protected void OnPlayerBlockMoved(Block blockGrabbed, in Vector2 moveAmount, in double moveTime)
        {
            levelStats.NumMoves = UtilityGlobals.Clamp(levelStats.NumMoves + 1, 0, LevelGlobals.MaxLevelMoves);
        }

        protected void OnPlayerHitByStrongRock()
        {
            ChangeLevelState(new GameOverState(this));
        }

        protected void OnPlayerEnterPortal()
        {
            ChangeLevelState(new LevelCompletionState(this));
        }

        public void RestartLevel()
        {
            //Clean up all objects
            for (int i = 0; i < Context.LevelCollision.Count; i++)
            {
                Context.LevelCollision[i].CleanUp();
            }

            Initialize();
            ChangeLevelState(new LevelRestartState(this, Context.Player.transform.Position));
        }

        public void Enter()
        {
            //Initialize map render target
            MapRenderTarget?.Dispose();
            MapRenderTarget = new RenderTarget2D(RenderingManager.Instance.graphicsDevice, LevelMap.Map.WidthInPixels, LevelMap.Map.HeightInPixels);

#if DEBUG
            Debug.DebugCamera = camera;

            Debug.AddCustomDebugCommand(DebugCameraControls);
            Debug.AddCustomDebugCommand(DebugCompleteLevel);
            Debug.AddCustomDebugCommand(DebugMovement);
            Debug.AddCustomDebugCommand(DebugChangeTimer);
            Debug.AddCustomDebugCommand(DebugDefeatMoles);

            Debug.AddCustomDebugDrawMethod(DebugDrawCameraInfo);
            //Debug.AddCustomDebugDrawMethod(DebugDrawLevelTime);
            Debug.AddCustomDebugDrawMethod(DebugDrawStaticCollision);
            Debug.AddCustomDebugDrawMethod(DebugDrawSurfaceTiles);
            Debug.AddCustomDebugDrawMethod(DebugDrawPlayerPos);
            Debug.AddCustomDebugDrawMethod(DebugDrawObjectCount);
            //Debug.AddCustomDebugDrawMethod(DebugDrawLevelStats);
#endif
        }

        public void Exit()
        {
            LevelState?.Exit();

            gameHUD?.CleanUp();

            camera.CleanUp();

            Context.Player.BlockMovedEvent -= OnPlayerBlockMoved;
            Context.Player.HitByStrongRockEvent -= OnPlayerHitByStrongRock;
            Context.LevelPortal.PlayerEnteredPortalEvent -= OnPlayerEnterPortal;

            //Clean up all level objects
            for (int i = 0; i < Context.LevelCollision.Count; i++)
            {
                Context.LevelCollision[i].CleanUp();
            }

            Context.SetCamera(null);

            if (MapRenderTarget != null)
            {
                MapRenderTarget.Dispose();
                MapRenderTarget = null;
            }

            RenderTargetRendered = false;

#if DEBUG
            Debug.DebugCamera = null;

            Debug.RemoveCustomDebugCommand(DebugCameraControls);
            Debug.RemoveCustomDebugCommand(DebugCompleteLevel);
            Debug.RemoveCustomDebugCommand(DebugMovement);
            Debug.RemoveCustomDebugCommand(DebugChangeTimer);
            Debug.RemoveCustomDebugCommand(DebugDefeatMoles);

            Debug.RemoveCustomDebugDrawMethod(DebugDrawCameraInfo);
            //Debug.RemoveCustomDebugDrawMethod(DebugDrawLevelTime);
            Debug.RemoveCustomDebugDrawMethod(DebugDrawStaticCollision);
            Debug.RemoveCustomDebugDrawMethod(DebugDrawSurfaceTiles);
            Debug.RemoveCustomDebugDrawMethod(DebugDrawPlayerPos);
            Debug.RemoveCustomDebugDrawMethod(DebugDrawObjectCount);
            //Debug.RemoveCustomDebugDrawMethod(DebugDrawLevelStats);
#endif
        }

        public void ChangeLevelState(IGameState levelState)
        {
            LevelState?.Exit();
            LevelState = levelState;
            LevelState.Enter();
        }

        public void Update()
        {
            LevelState.Update();
        }

        public void Render()
        {
            //If we haven't rendered the map to a RenderTarget yet, do so now
            //This lets us render the map in one draw call
            if (RenderTargetRendered == false)
            {
                RenderingManager.Instance.graphicsDevice.SetRenderTarget(MapRenderTarget);

                RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                    SamplerState.PointClamp, null, null, null, null);

                LevelMap.DrawMap();

                RenderingManager.Instance.EndCurrentBatch();

                //Set the RenderTarget back
                RenderingManager.Instance.graphicsDevice.SetRenderTarget(RenderingManager.Instance.GetMainRenderTarget);
                RenderingManager.Instance.graphicsDevice.Clear(RenderingManager.Instance.ClearColor);

                RenderTargetRendered = true;
            }

            LevelState.Render();
        }

        public void RenderLevel()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, camera.TransformMatrix);

            RenderLevelNoBatch();

            RenderingManager.Instance.EndCurrentBatch();

            if (LevelMap.ShowHUD == true)
            {
                RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                    BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

                gameHUD.Render();

                RenderingManager.Instance.EndCurrentBatch();
            }
        }

        public void RenderLevelNoBatch()
        {
            RenderingManager.Instance.DrawSprite(MapRenderTarget, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, Vector2.One,
                SpriteEffects.None, 0f);

            for (int i = 0; i < Context.LevelText.Count; i++)
            {
                Context.LevelText[i].Render();
            }

            Context.LevelPortal.Render();

            for (int i = 0; i < Context.Spots.Count; i++)
            {
                Context.Spots[i].Render();
            }

            for (int i = 0; i < Context.Switches.Count; i++)
            {
                Context.Switches[i].Render();
            }

            for (int i = 0; i < Context.Warps.Count; i++)
            {
                Context.Warps[i].Render();
            }

            for (int i = 0; i < Context.RockShooters.Count; i++)
            {
                Context.RockShooters[i].Render();
            }

            for (int i = 0; i < Context.Blocks.Count; i++)
            {
                Context.Blocks[i].Render();
            }

            Context.Player.Render();
        }

        private void DebugCompleteLevel()
        {
            if (KeyboardInput.GetKey(Keys.LeftControl, Debug.DebugKeyboard))
            {
                if (KeyboardInput.GetKeyDown(Keys.C, Debug.DebugKeyboard))
                {
                    ChangeLevelState(new LevelCompletionState(this));
                }
            }
        }

        private void DebugMovement()
        {
            if (KeyboardInput.GetKey(Keys.Tab, Debug.DebugKeyboard) == true)
            {
                if (KeyboardInput.GetKeyDown(Keys.Right, Debug.DebugKeyboard) == true)
                {
                    Context.Player.transform.Position += new Vector2(LevelMap.TileSize.X, 0f);
                }
                else if (KeyboardInput.GetKeyDown(Keys.Left, Debug.DebugKeyboard) == true)
                {
                    Context.Player.transform.Position += new Vector2(-LevelMap.TileSize.X, 0f);
                }
                if (KeyboardInput.GetKeyDown(Keys.Up, Debug.DebugKeyboard) == true)
                {
                    Context.Player.transform.Position += new Vector2(0f, -LevelMap.TileSize.Y);
                }
                else if (KeyboardInput.GetKeyDown(Keys.Down, Debug.DebugKeyboard) == true)
                {
                    Context.Player.transform.Position += new Vector2(0f, LevelMap.TileSize.Y);
                }
            }
        }

        private void DebugChangeTimer()
        {
            if (KeyboardInput.GetKey(Keys.Tab, Debug.DebugKeyboard) == true)
            {
                if (KeyboardInput.GetKeyDown(Keys.T, Debug.DebugKeyboard) == true)
                {
                    levelStats.LevelTime = UtilityGlobals.Clamp(levelStats.LevelTime + 1000d, 0d, LevelGlobals.MaxLevelTime);
                }
                else if (KeyboardInput.GetKeyDown(Keys.Y, Debug.DebugKeyboard) == true)
                {
                    levelStats.LevelTime = UtilityGlobals.Clamp(levelStats.LevelTime + 10000d, 0d, LevelGlobals.MaxLevelTime);
                }
                else if (KeyboardInput.GetKeyDown(Keys.U, Debug.DebugKeyboard) == true)
                {
                    levelStats.LevelTime = UtilityGlobals.Clamp(levelStats.LevelTime + 100000d, 0d, LevelGlobals.MaxLevelTime);
                }
                else if (KeyboardInput.GetKeyDown(Keys.I, Debug.DebugKeyboard) == true)
                {
                    levelStats.LevelTime = UtilityGlobals.Clamp(levelStats.LevelTime - 1000d, 0d, LevelGlobals.MaxLevelTime);
                }
                else if (KeyboardInput.GetKeyDown(Keys.O, Debug.DebugKeyboard) == true)
                {
                    levelStats.LevelTime = UtilityGlobals.Clamp(levelStats.LevelTime - 10000d, 0d, LevelGlobals.MaxLevelTime);
                }
                else if (KeyboardInput.GetKeyDown(Keys.P, Debug.DebugKeyboard) == true)
                {
                    levelStats.LevelTime = UtilityGlobals.Clamp(levelStats.LevelTime - 100000d, 0d, LevelGlobals.MaxLevelTime);
                }
                else if (KeyboardInput.GetKeyDown(Keys.H, Debug.DebugKeyboard) == true)
                {
                    levelStats.NumMoves = UtilityGlobals.Clamp(levelStats.NumMoves + 1, 0, LevelGlobals.MaxLevelMoves);
                }
                else if (KeyboardInput.GetKeyDown(Keys.J, Debug.DebugKeyboard) == true)
                {
                    levelStats.NumMoves = UtilityGlobals.Clamp(levelStats.NumMoves + 10, 0, LevelGlobals.MaxLevelMoves);
                }
                else if (KeyboardInput.GetKeyDown(Keys.K, Debug.DebugKeyboard) == true)
                {
                    levelStats.NumMoves = UtilityGlobals.Clamp(levelStats.NumMoves - 1, 0, LevelGlobals.MaxLevelMoves);
                }
                else if (KeyboardInput.GetKeyDown(Keys.L, Debug.DebugKeyboard) == true)
                {
                    levelStats.NumMoves = UtilityGlobals.Clamp(levelStats.NumMoves - 10, 0, LevelGlobals.MaxLevelMoves);
                }
            }
        }

        private void DebugCameraControls()
        {
            //Camera controls
            if (KeyboardInput.GetKey(Keys.LeftShift, Debug.DebugKeyboard) == false)
            {
                return;
            }

            if (KeyboardInput.GetKey(Keys.Space, Debug.DebugKeyboard))
            {
                //Reset camera coordinates
                camera.SetTranslation(camera.DefaultPosition);
                camera.SetRotation(0f);
                camera.SetZoom(1f);
            }
            else
            {
                Vector2 translation = Vector2.Zero;
                float rotation = 0f;
                float zoom = 0f;

                const float translateSpeed = 2f * 60f;
                float translateChange = translateSpeed * (float)Time.ElapsedTime.TotalSeconds;

                const float rotationSpeed = .1f * 60f;
                float rotationChange = rotationSpeed * (float)Time.ElapsedTime.TotalSeconds;

                const float zoomSpeed = .1f * 60f;
                float zoomChange = zoomSpeed * (float)Time.ElapsedTime.TotalSeconds;

                //Translation
                if (KeyboardInput.GetKey(Keys.Left, Debug.DebugKeyboard)) translation.X -= translateChange;
                if (KeyboardInput.GetKey(Keys.Right, Debug.DebugKeyboard)) translation.X += translateChange;
                if (KeyboardInput.GetKey(Keys.Down, Debug.DebugKeyboard)) translation.Y += translateChange;
                if (KeyboardInput.GetKey(Keys.Up, Debug.DebugKeyboard)) translation.Y -= translateChange;

                //Rotation
                if (KeyboardInput.GetKey(Keys.OemComma, Debug.DebugKeyboard)) rotation -= rotationChange;
                if (KeyboardInput.GetKey(Keys.OemPeriod, Debug.DebugKeyboard)) rotation += rotationChange;

                //Scale
                if (KeyboardInput.GetKey(Keys.OemMinus, Debug.DebugKeyboard)) zoom -= zoomChange;
                if (KeyboardInput.GetKey(Keys.OemPlus, Debug.DebugKeyboard)) zoom += zoomChange;

                if (translation != Vector2.Zero) camera.Translate(translation);
                if (rotation != 0f) camera.Rotate(rotation);
                if (zoom != 0f) camera.Zoom(zoom);
            }
        }

        private void DebugDefeatMoles()
        {
            if (KeyboardInput.GetKey(Keys.Tab, Debug.DebugKeyboard) == false)
                return;

            if (KeyboardInput.GetKeyDown(Keys.M) == false)
                return;

            for (int i = 0; i < Context.RockShooters.Count; i++)
            {
                RockShooter rs = Context.RockShooters[i];

                rs.OnHitByRock();
            }
        }

        private void DebugDrawCameraInfo()
        {
            //Camera info
            if (camera != null)
            {
                SpriteFont font = Debug.DebugFont;

                Vector2 cameraBasePos = new Vector2(0, RenderingManager.Instance.BackBufferDimensions.Y - 98f);
                Debug.DebugUIBatch?.DrawString(font, "Camera:", cameraBasePos, Color.White, 0f, Vector2.Zero, 1.2f, SpriteEffects.None, .1f);
                Debug.DebugUIBatch?.DrawString(font, $"Pos: {camera.Position}", cameraBasePos + new Vector2(0, 20), Color.White, 0f, Vector2.Zero, 1.2f, SpriteEffects.None, .1f);
                Debug.DebugUIBatch?.DrawString(font, $"Rot: {camera.Rotation}", cameraBasePos + new Vector2(0, 40), Color.White, 0f, Vector2.Zero, 1.2f, SpriteEffects.None, .1f);
                Debug.DebugUIBatch?.DrawString(font, $"Zoom: {camera.Scale}", cameraBasePos + new Vector2(0, 60), Color.White, 0f, Vector2.Zero, 1.2f, SpriteEffects.None, .1f);
            }
        }

        private void DebugDrawLevelTime()
        {
            SpriteFont font = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont24px);

            Vector2 levelTimePos = new Vector2(RenderingManager.Instance.BackBufferDimensions.X / 6f, RenderingManager.Instance.BackBufferDimensions.Y - 38f);
            string formattedTime = $"Level Time: {LevelGlobals.GetFormattedLevelTimeString(levelStats.LevelTime)}";

            Debug.DebugUIBatch.DrawString(font, formattedTime, levelTimePos, Color.White, 0f, Vector2.Zero, .4f, SpriteEffects.None, .3f);
        }

        private void DebugDrawStaticCollision()
        {
            if (KeyboardInput.GetKey(Keys.OemTilde, Debug.DebugKeyboard) == false) return;

            Texture2D tex = null;
            if (Context.LevelCollision.Count > 0)
            {
                tex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            }

            for (int i = 0; i < Context.LevelCollision.Count; i++)
            {
                ICollisionObj coll = Context.LevelCollision[i];
                if (EnumUtility.HasEnumVal((long)coll.CollisionLayer, (long)CollisionLayers.Wall) == true)
                {
                    Debug.DebugSpriteBatch.Draw(tex, (Rectangle)coll.CollisionRect,
                        ContentGlobals.BoxRect, Color.Red, 0f, Vector2.Zero, SpriteEffects.None, .41f);
                }
            }
        }

        private void DebugDrawSurfaceTiles()
        {
            if (KeyboardInput.GetKey(Keys.D1, Debug.DebugKeyboard) == false) return;

            Texture2D tex = null;

            if (Context.SurfaceTiles.NumTiles > 0)
            {
                tex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            }

            for (int i = 0; i < Context.SurfaceTiles.NumTiles; i++)
            {
                SurfaceTypes surfaceType = Context.SurfaceTiles.GetTile(i);
                Color color = Color.DarkSeaGreen;
                if (surfaceType == SurfaceTypes.Ice)
                {
                    color = Color.Blue;
                }
                else if (surfaceType == SurfaceTypes.Mud)
                {
                    color = Color.Brown;
                }

                color *= .7f;

                Context.SurfaceTiles.GetColRowFromIndex(i, out int column, out int row);

                Vector2 tilePos = Context.SurfaceTiles.Position + new Vector2(column * Context.SurfaceTiles.TileSize.X, row * Context.SurfaceTiles.TileSize.Y);

                Debug.DebugSpriteBatch.Draw(tex, tilePos, ContentGlobals.BoxRect, color, 0f,
                    Vector2.Zero, Context.SurfaceTiles.TileSize, SpriteEffects.None, .4f);
            }
        }

        private void DebugDrawPlayerPos()
        {
            Debug.DebugUIBatch.DrawString(Debug.DebugFont, $"Player Pos: {Context.Player.transform.Position}",
                new Vector2(RenderingManager.Instance.BackBufferDimensions.X / 2f, RenderingManager.Instance.BackBufferDimensions.Y - 28f), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .5f);
        }

        private void DebugDrawObjectCount()
        {
            Debug.DebugUIBatch.DrawString(Debug.DebugFont, $"Object Count: {Context.LevelCollision.Count}",
                new Vector2(RenderingManager.Instance.BackBufferDimensions.X / 2f, RenderingManager.Instance.BackBufferDimensions.Y - 48f), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, .5f);
        }

        private void DebugDrawLevelStats()
        {
            SpriteFont font = Debug.DebugFont;

            Vector2 levelStatsPos = new Vector2(RenderingManager.Instance.BackBufferDimensions.X / 2f, RenderingManager.Instance.BackBufferDimensions.Y - 38f);
            string numMoves = "Num Moves: " + levelStats.NumMoves.ToString();

            Debug.DebugUIBatch.DrawString(font, numMoves, levelStatsPos, Color.White, 0f, Vector2.Zero, .4f, SpriteEffects.None, .3f);
        }
    }
}
