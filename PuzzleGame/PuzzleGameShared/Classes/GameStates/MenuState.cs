﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// The menu state; it contains a stack of input menus.
    /// </summary>
    public abstract class MenuState : IGameState
    {
        protected Stack<InputMenu> Menus = new Stack<InputMenu>();

        public int MenuCount => Menus.Count;

        public MenuState()
        {

        }

        public void PushMenu(in InputMenu menu)
        {
            Menus.Push(menu);
        }

        public InputMenu PopMenu()
        {
            return Menus.Pop();
        }

        public virtual void Enter()
        {
            
        }

        public virtual void Exit()
        {
            
        }

        public virtual void Update()
        {
            if (Menus.Count > 0)
                Menus.Peek().Update();
        }

        public virtual void Render()
        {

        }
    }
}
