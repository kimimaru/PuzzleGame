﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The overworld game state where the player chooses levels.
    /// </summary>
    public class OverworldState : IGameState
    {
        public Camera2D camera { get; private set; } = null;

        public PlayableContext Context { get; private set; } = null;

        public OverworldMap overworldMap { get; private set; } = null;
        public int WorldID => overworldMap.WorldID;
        public string WorldName => overworldMap.WorldName;

        public readonly List<Portal> Portals = new List<Portal>(32);

        public int CurrentNodeIndex = 0;

        public OverworldMapNode CurNode => overworldMap.Nodes[CurrentNodeIndex];

        public OverworldMapNode GetNode(in int index) => overworldMap.Nodes[index];

        /// <summary>
        /// A dictionary for quick lookup of nodes.
        /// The key is the LevelID of the node, and the value is the index in the node list.
        /// </summary>
        private Dictionary<int, int> NodeLookupDict = null;

        private static readonly Vector2 LineOrigin = new Vector2(0f, 0.5f);
        private const int LineThickness = 2;
        private const double EmptyRadius = 5d;
        public static readonly Color IncompleteLineColor = Color.Gray;
        public static readonly Color CompleteLineColor = Color.LightGray;

        private IGameState OWState = null;

        private Texture2D Box = null;

        public OverworldLevelPanel LevelPanel { get; private set; } = null;

        public bool IsPlayerMoving = false;

        public Vector2 PlayerStart = Vector2.Zero;
        public Vector2 PlayerDest = Vector2.Zero;

        public double ElapsedTime = 0d;
        public int StartingNodeIndex = 0;

        public WorldInfo WorldInformation = default;

        /// <summary>
        /// A RenderTarget used to draw the map.
        /// </summary>
        private RenderTarget2D MapRenderTarget = null;
        private bool RenderTargetRendered = false;

        public OverworldState(in int worldID) : this(AssetManager.Instance.LoadTiledMap($"World{worldID}"))
        {
            
        }

        public OverworldState(TiledMap tMap)
        {
            overworldMap = new OverworldMap(tMap);
            
            //Initialize();
        }

        public int GetNodeIndex(in int levelID)
        {
            if (NodeLookupDict.TryGetValue(levelID, out int index) == false)
            {
                index = -1;
            }

            return index;
        }

        public OverworldMapNode GetNodeAtDirection(int? nodeDir)
        {
            if (nodeDir == null) return null;

            int nodeInd = GetNodeIndex(nodeDir.Value);
            if (nodeInd < 0) return null;

            return GetNode(nodeInd);
        }

        public LevelData GetSaveDataForLevel(in int levelID)
        {
            SaveData.GetLevelData(WorldID, levelID, out LevelData lvlData);
            return lvlData;
        }

        public bool HasSaveDataForLevel(in int levelID, out LevelData lvlData) => SaveData.GetLevelData(WorldID, levelID, out lvlData);

        public bool IsLevelComplete(in int levelID, out LevelData lvlData)
            => (HasSaveDataForLevel(levelID, out lvlData) == true && lvlData.Complete == true);

        public bool IsLevelComplete(in int levelID)
            => IsLevelComplete(levelID, out LevelData lvlData) == true;

        private void Initialize()
        {
            camera = new Camera2D();
            camera.SetBounds(new Rectangle(0, 0, (int)RenderingGlobals.BaseResolution.X, (int)RenderingGlobals.BaseResolution.Y));

            int lastLevelID = DataHandler.saveData.LastPlayedLevel;
            bool found = false;

            //Initialize the dictionary
            NodeLookupDict = new Dictionary<int, int>(overworldMap.Nodes.Count);

            int lowest = int.MaxValue;
            int indexOfLowest = int.MaxValue;

            for (int i = 0; i < overworldMap.Nodes.Count; i++)
            {
                OverworldMapNode node = overworldMap.Nodes[i];
                NodeLookupDict.Add(node.LevelID, i);

                if (node.LevelID < lowest)
                {
                    lowest = node.LevelID;
                    indexOfLowest = i;
                }
            }

            //Find the starting node index
            for (int i = 0; i < overworldMap.Nodes.Count; i++)
            {
                if (overworldMap.Nodes[i].LevelID == lastLevelID)
                {
                    CurrentNodeIndex = i;
                    found = true;
                    break;
                }
            }

            //If we didn't find it, go to the last node if the ID is greater than or equal to the node count, otherwise go to the first node
            if (found == false)
            {
                if (lastLevelID >= overworldMap.Nodes.Count)
                {
                    CurrentNodeIndex = overworldMap.Nodes.Count - 1;
                }
                else
                {
                    CurrentNodeIndex = indexOfLowest;
                }
            }

            StartingNodeIndex = CurrentNodeIndex;

            //Set the initial camera position to the player's position, then clamp it
            camera.DefaultPosition = Camera2D.ClampCamera(CurNode.Position,
                camera.ScreenBounds, overworldMap.MapBounds);
            camera.LookAt(camera.DefaultPosition);

            if (Context == null)
            {
                Context = new PlayableContext();
            }
            else
            {
                Context.ClearAllObjectsAndCamera();
            }

            Context.LevelSpace.TileSize = overworldMap.TileSize;
            Context.LevelSpace.WidthInPixels = overworldMap.Map.WidthInPixels;
            Context.LevelSpace.HeightInPixels = overworldMap.Map.HeightInPixels;

            Echidna player = new Echidna(new BlankState());
            player.transform.Position = CurNode.Position;

            Context.AddObject(player);

            WorldInformation.WorldID = WorldID;

            CheckNodeCompletion(StartingNodeIndex);

            Context.SetSurfaceTiles(new SurfaceTileEngine(Vector2.Zero, overworldMap.TileSize, overworldMap.Map.TileWidth, overworldMap.Map.TileHeight));

            LoadCollision();
            LoadSwitches();
            LoadWarps();
            LoadBlocks();
            LoadRockShooters();

            Portals.Clear();

            ObjectInitInfo objInitInfo = default;

            //Create a portal or warp for each node in the map
            for (int i = 0; i < overworldMap.Nodes.Count; i++)
            {
                OverworldMapNode node = overworldMap.Nodes[i];

                if (node.NodeType == MapNodeTypes.Level)
                {
                    Portal portal = new Portal();

                    if (objInitInfo.Properties == null)
                        objInitInfo.Properties = new Dictionary<string, string>(1);
                    
                    objInitInfo.Properties[LevelGlobals.PortalBossAnimKey] = (node.HasAltAnim == true) ? "true" : "false";

                    portal.Initialize(Context, objInitInfo);
                    portal.transform.Position = node.Position;
                    Portals.Add(portal);

                    node.NodeVisual = new LevelNodeVisual(portal);
                }
                else if (node.NodeType == MapNodeTypes.World)
                {
                    Warp warp = new Warp();
                    if (objInitInfo.Properties == null)
                        objInitInfo.Properties = new Dictionary<string, string>(1);

                    string altAnimVal = (node.WorldID != WorldID || node.HasAltAnim == true) ? "true" : "false";

                    objInitInfo.Properties[LevelGlobals.WarpAltAnimKey] = altAnimVal;

                    warp.Initialize(Context, objInitInfo);
                    warp.transform.Position = node.Position;

                    //For warps, set the warp position to the node this one leads to if its in the same world
                    //For different worlds, set the destination to its own position
                    if (node.WorldID != WorldID)
                    {
                        warp.SetWarpDestination(node.Position);
                    }
                    else
                    {
                        WorldMapNode worldNode = (WorldMapNode)node;
                        OverworldMapNode destNode = GetNode(GetNodeIndex(worldNode.LandLevelID));
                        warp.SetWarpDestination(destNode.Position);
                    }

                    Context.AddObject(warp);

                    node.NodeVisual = new WorldNodeVisual(warp);
                }
            }

            InitNodes();
            UpdateLevelNodeUnlocked();

            LevelPanel = new OverworldLevelPanel();
            LevelPanel.UpdateLevelInfo(CurNode);

            //At this point the world loaded with no issues, so we can safely set the last played world to this one
            SaveData.SetLastPlayedWorld(WorldID);

            if (CurNode.NodeType == MapNodeTypes.World)
            {
                WorldNodeVisual worldVisual = (WorldNodeVisual)CurNode.NodeVisual;
                Warp warp = worldVisual.WarpVisual;

                //Initialize the warp by warping out the player
                warp.HasSound = false;
                warp.WarpObject(Context.Player, Context.Player.CollisionRect);
                warp.HasSound = true;
                warp.SetWarpState(Warp.WarpStates.Out);
                warp.SetWarpProgress(0d);

                ChangeState(new OverworldWarpOutState(this, warp, false, warp.WarpDuration, Color.Black, Color.Transparent));
            }
            else
            {
                ChangeState(new OverworldIntroState(this));
            }

            //Initialize map render target
            MapRenderTarget?.Dispose();
            MapRenderTarget = new RenderTarget2D(RenderingManager.Instance.graphicsDevice, overworldMap.Map.WidthInPixels, overworldMap.Map.HeightInPixels);
        }

        private void LoadCollision()
        {
            TiledMapObjectLayer collisionLayer = overworldMap.Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.CollisionLayerName);

            if (collisionLayer == null) return;

            for (int i = 0; i < collisionLayer.Objects.Length; i++)
            {
                TiledMapObject obj = collisionLayer.Objects[i];
                if (obj.IsVisible == false) continue;

                Context.AddObject(LevelGlobals.CreateWall(Context, obj));
            }
        }

        private void LoadBlocks()
        {
            TiledMapObjectLayer blockLayer = overworldMap.Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.BlockLayerName);

            if (blockLayer == null) return;

            for (int i = 0; i < blockLayer.Objects.Length; i++)
            {
                TiledMapObject obj = blockLayer.Objects[i];
                if (obj.IsVisible == false) continue;

                Block b = LevelGlobals.CreateBlock(Context, obj);
                Context.AddObject(b);

                b.PostInitialize(Context);
            }
        }

        private void LoadSwitches()
        {
            TiledMapObjectLayer switchLayer = overworldMap.Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.SwitchLayerName);

            if (switchLayer != null)
            {
                for (int i = 0; i < switchLayer.Objects.Length; i++)
                {
                    TiledMapObject obj = switchLayer.Objects[i];
                    if (obj.IsVisible == false) continue;

                    Switch switchObj = LevelGlobals.CreateSwitch(Context, obj);

                    Context.AddObject(switchObj);

                    switchObj.PostInitialize(Context);
                }
            }
        }

        private void LoadWarps()
        {
            TiledMapObjectLayer warpLayer = overworldMap.Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.WarpLayerName);

            if (warpLayer != null)
            {
                for (int i = 0; i < warpLayer.Objects.Length; i++)
                {
                    TiledMapObject obj = warpLayer.Objects[i];
                    if (obj.IsVisible == false) continue;

                    Warp warp = LevelGlobals.CreateWarp(Context, obj);

                    Context.AddObject(warp);

                    warp.PostInitialize(Context);
                }
            }
        }

        private void LoadRockShooters()
        {
            TiledMapObjectLayer rockShooterLayer = overworldMap.Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.RockShooterLayerName);

            if (rockShooterLayer == null) return;

            for (int i = 0; i < rockShooterLayer.Objects.Length; i++)
            {
                TiledMapObject obj = rockShooterLayer.Objects[i];
                if (obj.IsVisible == false) continue;

                RockShooter rs = LevelGlobals.CreateRockShooter(Context, obj);

                Context.AddObject(rs);

                rs.PostInitialize(Context);
            }
        }

        public void CheckNodeCompletion(in int startingNode)
        {
            OverworldMapNode startNode = GetNode(startingNode);

            //Check if you started on a world or empty node and complete it if so
            //This allows world nodes to be used as shortcuts without making the player stuck or unable to move to its connected levels
            if (startNode.NodeType == MapNodeTypes.World || startNode.NodeType == MapNodeTypes.Empty)
            {
                if (HasSaveDataForLevel(startNode.LevelID, out LevelData lvlData) == false || lvlData.Complete == false)
                {
                    //Add the level data if it doesn't exist
                    if (lvlData == null)
                    {
                        lvlData = new LevelData();
                        WorldData worldData = SaveData.GetWorldData(WorldID);
                        worldData.levelData.Add(startNode.LevelID, lvlData);
                    }

                    lvlData.IncrementTimesComplete();
                    DataHandler.saveData.Misc.JustFinishedLevels.AddIfNotIn(startNode.LevelID);
                }
            }

            //Check for completing all nodes
            //Add all nodes to the level data regardless
            for (int i = 0; i < overworldMap.Nodes.Count; i++)
            {
                OverworldMapNode node = overworldMap.Nodes[i];

                LevelData lvlData = null;

                if (HasSaveDataForLevel(node.LevelID, out lvlData) == false || lvlData.Complete == false)
                {
                    //Add the level data if it doesn't exist
                    if (lvlData == null)
                    {
                        lvlData = new LevelData();
                        WorldData worldData = SaveData.GetWorldData(WorldID);
                        worldData.levelData.Add(node.LevelID, lvlData);
                    }

                    //If we should auto-complete this node, do so
                    if (node.AutoComplete == false)
                    {
                        continue;
                    }

                    lvlData.IncrementTimesComplete();
                    DataHandler.saveData.Misc.JustFinishedLevels.AddIfNotIn(node.LevelID);
                }

                //If the level is completed, check if the connecting nodes are empty or worlds, and complete them if so
                CheckCompleteLevel(node, node.UpNode);
                CheckCompleteLevel(node, node.DownNode);
                CheckCompleteLevel(node, node.LeftNode);
                CheckCompleteLevel(node, node.RightNode);
            }
        }

        private void CheckCompleteLevel(OverworldMapNode node, int? nodeDir)
        {
            OverworldMapNode dirNode = GetNodeAtDirection(nodeDir);
            if (dirNode == null) return;

            if (dirNode.NodeType == MapNodeTypes.World || dirNode.NodeType == MapNodeTypes.Empty)
            {
                //Complete it and add an entry if not present
                if (HasSaveDataForLevel(dirNode.LevelID, out LevelData dirData) == false)
                {
                    dirData = new LevelData();
                    WorldData worldData = SaveData.GetWorldData(WorldID);
                    worldData.levelData.Add(dirNode.LevelID, dirData);
                }

                if (dirData.Complete == false)
                {
                    dirData.IncrementTimesComplete();
                    DataHandler.saveData.Misc.JustFinishedLevels.AddIfNotIn(dirNode.LevelID);

                    //Check if any empty or world nodes that were already visited should be completed if they aren't already
                    //This fixes problems not completing them if a connected level that was just completed
                    //is at a higher index than a node that's now completed, causing any adjacent nodes to that one not being revisited
                    CheckCompleteLevel(dirNode, dirNode.UpNode);
                    CheckCompleteLevel(dirNode, dirNode.DownNode);
                    CheckCompleteLevel(dirNode, dirNode.LeftNode);
                    CheckCompleteLevel(dirNode, dirNode.RightNode);
                }
            }
        }

        /// <summary>
        /// Tells if any level linked to a particular map node has just been completed.
        /// </summary>
        /// <param name="node">The node to check the linked levels for.</param>
        /// <returns>true if any level linked to the given map node has just been completed, otherwise false.</returns>
        public bool IsAnyDirLevelJustCompleted(OverworldMapNode node)
        {
            if (node == null) return false;

            return IsDirLevelJustCompleted(node.UpNode) || IsDirLevelJustCompleted(node.DownNode)
                || IsDirLevelJustCompleted(node.LeftNode) || IsDirLevelJustCompleted(node.RightNode);
        }

        /// <summary>
        /// Tells if a level with a particular ID has just been completed.
        /// </summary>
        /// <param name="levelID">The level ID to check.</param>
        /// <returns>true if the level associated with the given ID has just been completed, otherwise false.</returns>
        public bool IsDirLevelJustCompleted(int? levelID)
        {
            if (levelID == null) return false;

            return DataHandler.saveData.Misc.JustFinishedLevels.ContainsNB(levelID.Value);
        }

        /// <summary>
        /// Tells if any level linked to a particular map node is complete.
        /// </summary>
        /// <param name="node">The node to check the linked levels for.</param>
        /// <returns>true if any level linked to the given map node is complete, otherwise false.</returns>
        public bool IsAnyDirLevelComplete(OverworldMapNode node)
        {
            return IsDirLevelComplete(node.UpNode) || IsDirLevelComplete(node.DownNode)
                    || IsDirLevelComplete(node.LeftNode) || IsDirLevelComplete(node.RightNode);
        }

        /// <summary>
        /// Tells if a level with a particular ID is complete.
        /// </summary>
        /// <param name="levelID">The level ID to check.</param>
        /// <returns>true if the level associated with the given ID is complete, otherwise false.</returns>
        public bool IsDirLevelComplete(int? nodeDir)
        {
            OverworldMapNode dirNode = GetNodeAtDirection(nodeDir);
            if (dirNode == null) return false;

            //Check if the level is complete
            if (HasSaveDataForLevel(dirNode.LevelID, out LevelData dirData) == false)
            {
                return false;
            }

            return dirData.Complete;
        }

        public void Enter()
        {
            Box = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);

            Initialize();

#if DEBUG
            Debug.AddCustomDebugCommand(DebugCameraControls);
            Debug.AddCustomDebugCommand(DebugCompleteWorld);
            Debug.AddCustomDebugCommand(DebugCompleteAllLevels);
            Debug.AddCustomDebugDrawMethod(DebugDrawCameraInfo);
#endif
        }

        public void Exit()
        {
            OWState.Exit();

            camera?.CleanUp();

            Box = null;
            LevelPanel = null;

            //Clean up all level objects
            for (int i = 0; i < Context.LevelCollision.Count; i++)
            {
                Context.LevelCollision[i].CleanUp();
            }

            Context.SetCamera(null);

            if (MapRenderTarget != null)
            {
                MapRenderTarget.Dispose();
                MapRenderTarget = null;
            }

            RenderTargetRendered = false;

#if DEBUG
            Debug.RemoveCustomDebugCommand(DebugCameraControls);
            Debug.RemoveCustomDebugCommand(DebugCompleteWorld);
            Debug.RemoveCustomDebugCommand(DebugCompleteAllLevels);
            Debug.RemoveCustomDebugDrawMethod(DebugDrawCameraInfo);
#endif
        }

        public void ChangeState(IGameState owState)
        {
            OWState?.Exit();
            OWState = owState;
            OWState.Enter();
        }

        public void Update()
        {
            OWState.Update();
        }

        public void LoadAndPlayOverworldMusic()
        {
            //Load the world's music track
            Microsoft.Xna.Framework.Audio.SoundEffect musicTrack = null;

            try
            {
                musicTrack = AssetManager.Instance.LoadMusic(overworldMap.MusicName);
            }
            catch (Microsoft.Xna.Framework.Content.ContentLoadException)
            {
                musicTrack = null;
            }

            //Play the music track if it's available
            if (musicTrack != null)
            {
                SoundManager.Instance.PlayMusic(musicTrack, true);
            }
            //Otherwise, stop the existing music
            else
            {
                SoundManager.Instance.StopAndClearMusicTrack();
            }
        }

        private void InitNodes()
        {
            for (int i = 0; i < overworldMap.Nodes.Count; i++)
            {
                OverworldMapNode node = overworldMap.Nodes[i];

                //Check if the level is complete in the save data
                //It's also marked incomplete if it's a level that was just completed for the purposes of starting it off in the correct initial state
                //for the complete animation
                bool complete = (SaveData.GetLevelData(WorldID, node.LevelID, out LevelData data) == true && data.Complete == true);

                bool justFinished = DataHandler.saveData.Misc.JustFinishedLevels.ContainsNB(node.LevelID);

                bool shouldStartComplete = (complete == true && justFinished == false);

                InitNodeLineHelper(node, node.UpNode, i, ref node.UpLine, ref node.UplineColor, shouldStartComplete);
                InitNodeLineHelper(node, node.DownNode, i, ref node.DownLine, ref node.DownlineColor, shouldStartComplete);
                InitNodeLineHelper(node, node.LeftNode, i, ref node.LeftLine, ref node.LeftlineColor, shouldStartComplete);
                InitNodeLineHelper(node, node.RightNode, i, ref node.RightLine, ref node.RightlineColor, shouldStartComplete);
                
                if (node.NodeType == MapNodeTypes.Empty || node.NodeType == MapNodeTypes.Cutscene)
                {
                    Color completeColor = node.NodeType == MapNodeTypes.Empty ? CompleteLineColor : Color.SkyBlue;
                    node.NodeVisual = new EmptyNodeVisual(new Circle(node.Position, EmptyRadius), Box, ContentGlobals.BoxRect, IncompleteLineColor, completeColor);
                }

                //If it's a level node, get information for the stats
                if (node.NodeType == MapNodeTypes.Level)
                {
                    WorldInformation.TotalLevels++;

                    if (data?.Complete == true)
                    {
                        WorldInformation.LevelsCompleted++;
                        WorldInformation.TotalMoves += data.LvlStats.NumMoves;
                        WorldInformation.TotalTime += data.LvlStats.LevelTime;
                    }
                }

                node.NodeVisual.SetCompleteState(shouldStartComplete);
            }
        }

        /// <summary>
        /// Updates the unlocked state of all level nodes.
        /// </summary>
        public void UpdateLevelNodeUnlocked()
        {
            for (int i = 0; i < overworldMap.Nodes.Count; i++)
            {
                OverworldMapNode node = overworldMap.Nodes[i];

                if (node.NodeType != MapNodeTypes.Level)
                {
                    continue;
                }

                LevelMapNode levelNode = (LevelMapNode)node;

                if (SaveData.GetLevelData(WorldID, node.LevelID, out LevelData data) == true && data?.Complete == true)
                {
                    levelNode.SetUnlocked(false);
                    continue;
                }

                levelNode.SetUnlocked(IsAnyDirLevelJustCompleted(node) == false && IsAnyDirLevelComplete(node) == true);
            }
        }

        private void InitNodeLineHelper(in OverworldMapNode node, int? nodeVal, in int i, ref Line? line, ref Color lineColor, in bool complete)
        {
            if (nodeVal == null) return;

            //Set lines to all connecting neighbors
            int ind = GetNodeIndex(nodeVal.Value);

            //We avoid overdraw and initialize a path only if it hasn't already been set, which is
            //indicated if the current index is less than the index of the node at the path
            if (i >= ind) return;

            OverworldMapNode dirNode = GetNode(ind);
            line = new Line(node.Position, dirNode.Position);

            lineColor = IncompleteLineColor;

            if (complete == true)
            {
                lineColor = CompleteLineColor;
            }
            else
            {
                //If this level isn't completed, check if the level this leads to is completed and adjust the color if so
                //Don't adjust the color if the level this leads to was just completed so we can display the animation
                if (SaveData.GetLevelData(WorldID, dirNode.LevelID, out LevelData lData) == true && lData.Complete == true
                    && DataHandler.saveData.Misc.JustFinishedLevels.ContainsNB(dirNode.LevelID) == false)
                {
                    lineColor = CompleteLineColor;
                }
            }
        }

        private void DrawNodeLineHelper(Line? line, in Color lineColor)
        {
            if (line == null) return;

            Line drawLine = line.Value;
            RenderingManager.Instance.spriteBatch.DrawLine(Box, ContentGlobals.BoxRect, drawLine.P1, drawLine.P2, lineColor, LineOrigin, .3f, LineThickness);
        }

        public void DrawMap()
        {
            RenderingManager.Instance.DrawSprite(MapRenderTarget, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, Vector2.One,
                SpriteEffects.None, 0f);
            //overworldMap.DrawMap();
        }

        public void DrawNodes()
        {
            for (int i = 0; i < overworldMap.Nodes.Count; i++)
            {
                OverworldMapNode curNode = overworldMap.Nodes[i];

                DrawNodeLineHelper(curNode.UpLine, curNode.UplineColor);
                DrawNodeLineHelper(curNode.DownLine, curNode.DownlineColor);
                DrawNodeLineHelper(curNode.LeftLine, curNode.LeftlineColor);
                DrawNodeLineHelper(curNode.RightLine, curNode.RightlineColor);

                //Currently render only the visuals of all empty and cutscene nodes here since they use the same texture as the lines
                if (curNode.NodeType == MapNodeTypes.Empty || curNode.NodeType == MapNodeTypes.Cutscene)
                {
                    curNode.NodeVisual.Render();
                }
            }
        }

        public void DrawSwitches()
        {
            for (int i = 0; i < Context.Switches.Count; i++)
            {
                Context.Switches[i].Render();
            }
        }

        public void DrawPortals()
        {
            for (int i = 0; i < Portals.Count; i++)
            {
                Portals[i].Render();
            }
        }

        public void DrawWarps()
        {
            for (int i = 0; i < Context.Warps.Count; i++)
            {
                Context.Warps[i].Render();
            }
        }

        public void RenderOverworld()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, camera.TransformMatrix);

            RenderOverworldNoBatch();

            RenderingManager.Instance.EndCurrentBatch();
        }

        public void RenderOverworldNoBatch()
        {
            DrawMap();

            DrawNodes();

            //Draw level objects separately since they use different textures
            DrawSwitches();
            DrawWarps();
            DrawPortals();

            for (int i = 0; i < Context.RockShooters.Count; i++)
            {
                Context.RockShooters[i].Render();
            }

            for (int i = 0; i < Context.Blocks.Count; i++)
            {
                Context.Blocks[i].Render();
            }

            LevelPanel.Render();
            Context.Player.Render();
        }

        public void Render()
        {
            //If we haven't rendered the map to a RenderTarget yet, do so now
            //This lets us render the map in one draw call
            if (RenderTargetRendered == false)
            {
                RenderingManager.Instance.graphicsDevice.SetRenderTarget(MapRenderTarget);

                RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                    SamplerState.PointClamp, null, null, null, null);

                overworldMap.DrawMap();

                RenderingManager.Instance.EndCurrentBatch();

                //Set the RenderTarget back
                RenderingManager.Instance.graphicsDevice.SetRenderTarget(RenderingManager.Instance.GetMainRenderTarget);
                RenderingManager.Instance.graphicsDevice.Clear(RenderingManager.Instance.ClearColor);

                RenderTargetRendered = true;
            }

            OWState.Render();
        }

        private void DebugCameraControls()
        {
            //Camera controls
            if (KeyboardInput.GetKey(Keys.LeftShift, Debug.DebugKeyboard) == false)
            {
                return;
            }
                
            if (KeyboardInput.GetKey(Keys.Space, Debug.DebugKeyboard))
            {
                //Reset camera coordinates
                camera.SetTranslation(camera.DefaultPosition);
                camera.SetRotation(0f);
                camera.SetZoom(1f);
            }
            else
            {
                Vector2 translation = Vector2.Zero;
                float rotation = 0f;
                float zoom = 0f;

                const float translateSpeed = 2f * 60f;
                float translateChange = translateSpeed * (float)Time.ElapsedTime.TotalSeconds;

                const float rotationSpeed = .1f * 60f;
                float rotationChange = rotationSpeed * (float)Time.ElapsedTime.TotalSeconds;

                const float zoomSpeed = .1f * 60f;
                float zoomChange = zoomSpeed * (float)Time.ElapsedTime.TotalSeconds;

                //Translation
                if (KeyboardInput.GetKey(Keys.Left, Debug.DebugKeyboard)) translation.X -= translateChange;
                if (KeyboardInput.GetKey(Keys.Right, Debug.DebugKeyboard)) translation.X += translateChange;
                if (KeyboardInput.GetKey(Keys.Down, Debug.DebugKeyboard)) translation.Y += translateChange;
                if (KeyboardInput.GetKey(Keys.Up, Debug.DebugKeyboard)) translation.Y -= translateChange;

                //Rotation
                if (KeyboardInput.GetKey(Keys.OemComma, Debug.DebugKeyboard)) rotation -= rotationChange;
                if (KeyboardInput.GetKey(Keys.OemPeriod, Debug.DebugKeyboard)) rotation += rotationChange;

                //Scale
                if (KeyboardInput.GetKey(Keys.OemMinus, Debug.DebugKeyboard)) zoom -= zoomChange;
                if (KeyboardInput.GetKey(Keys.OemPlus, Debug.DebugKeyboard)) zoom += zoomChange;

                if (translation != Vector2.Zero) camera.Translate(translation);
                if (rotation != 0f) camera.Rotate(rotation);
                if (zoom != 0f) camera.Zoom(zoom);
            }
        }

        private void DebugCompleteWorld()
        {
            if (KeyboardInput.GetKey(Keys.LeftControl, Debug.DebugKeyboard))
            {
                if (KeyboardInput.GetKeyDown(Keys.Tab, Debug.DebugKeyboard))
                {
                    ChangeState(new AllLevelsCompleteState(this));
                }
            }
        }

        private void DebugCompleteAllLevels()
        {
            if (KeyboardInput.GetKey(Keys.LeftControl, Debug.DebugKeyboard))
            {
                if (KeyboardInput.GetKeyDown(Keys.C, Debug.DebugKeyboard))
                {
                    ChangeState(new WorldCompleteState(this));
                }
            }
        }

        private void DebugDrawCameraInfo()
        {
            //Camera info
            if (camera != null)
            {
                SpriteFont font = Debug.DebugFont;

                Vector2 cameraBasePos = new Vector2(0, RenderingManager.Instance.BackBufferDimensions.Y - 98f);
                Debug.DebugUIBatch?.DrawString(font, "Camera:", cameraBasePos, Color.White, 0f, Vector2.Zero, 1.2f, SpriteEffects.None, .1f);
                Debug.DebugUIBatch?.DrawString(font, $"Pos: {camera.Position}", cameraBasePos + new Vector2(0, 20), Color.White, 0f, Vector2.Zero, 1.2f, SpriteEffects.None, .1f);
                Debug.DebugUIBatch?.DrawString(font, $"Rot: {camera.Rotation}", cameraBasePos + new Vector2(0, 40), Color.White, 0f, Vector2.Zero, 1.2f, SpriteEffects.None, .1f);
                Debug.DebugUIBatch?.DrawString(font, $"Zoom: {camera.Scale}", cameraBasePos + new Vector2(0, 60), Color.White, 0f, Vector2.Zero, 1.2f, SpriteEffects.None, .1f);
            }
        }
    }
}
