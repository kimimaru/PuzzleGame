/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class LevelFlyOutState : IGameState
    {
        private IngameState inGameState = null;

        private Texture2D Box = null;

        private readonly Direction[] ClockwiseDirections = new Direction[4] { Direction.Down, Direction.Left, Direction.Up, Direction.Right };
        private Direction CurDirection = Direction.Down;

        private readonly Color StartFadeColor = Color.Transparent;
        private readonly Color EndFadeColor = Color.Black;
        private Color CurFadeColor = Color.Transparent;

        private Vector2 OrigPlayerPos = Vector2.Zero;

        private double FlyTime = 500d;
        private Vector2 PlayerStartScale = Vector2.Zero;
        private Vector2 PlayerEndScale = Vector2.Zero;

        private double CharRotateTime = 0d;
        private double CurRotationTime = 0d;

        private double FadeTime = 500d;
        private double ElapsedTime = 0d;
        private double ElapsedFadeTime = 0d;

        private bool SaveDataOnFinish = true;

        public LevelFlyOutState(IngameState ingameState, in Color startFadeColor, in Color endFadeColor, in double fadeTime, in double flyTime,
            in bool saveDataOnFinish)
        {
            inGameState = ingameState;
            StartFadeColor = startFadeColor;
            EndFadeColor = endFadeColor;
            FadeTime = fadeTime;
            FlyTime = flyTime;

            CharRotateTime = (int)(FlyTime / 8d);

            SaveDataOnFinish = saveDataOnFinish;
        }

        public void Enter()
        {
            PlayerStartScale = inGameState.Context.Player.transform.Scale;

            //Set the player to the portal's position and make it face down
            inGameState.Context.Player.transform.Position = inGameState.Context.LevelPortal.transform.Position;
            inGameState.Context.Player.ChangeState(new BlankState(false));
            inGameState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Success);

            CurDirection = ClockwiseDirections[0];
            inGameState.Context.Player.FacingDir = CurDirection;

            OrigPlayerPos = inGameState.Context.Player.transform.Position;
            inGameState.Context.Player.transform.Scale = PlayerStartScale;

            Box = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);

            CurFadeColor = StartFadeColor;

            //Hide the HUD
            inGameState.LevelMap.ShowHUD = false;
        }

        public void Exit()
        {

        }

        public void Update()
        {
            //Update the fade over the screen
            ElapsedFadeTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedFadeTime >= FadeTime)
            {
                ElapsedFadeTime = FadeTime;
                CurFadeColor = EndFadeColor;
            }
            else
            {
                CurFadeColor = Interpolation.Interpolate(StartFadeColor, EndFadeColor, ElapsedFadeTime / FadeTime, Interpolation.InterpolationTypes.Linear);
            }

            inGameState.Context.LevelPortal.Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            HandleFlyOut();
        }

        private void HandleFlyOut()
        {
            inGameState.Context.Player.AnimationManager.CurrentAnim.Update();

            if (ElapsedTime >= FlyTime)
            {
                if (DataHandler.saveData.Misc.InCustomLevel == false)
                {
                    if (SaveDataOnFinish == true)
                    {
                        //Save data and go back to the overworld map
                        HandleSaveData();
                    }

                    //Load a cutscene at the end of the level if one is defined
                    if (inGameState.LevelMap.CutsceneID != CutsceneEnums.None)
                    {
                        Cutscene cutScene = CutsceneLoader.LoadCutscene(inGameState.LevelMap.CutsceneID);
                        cutScene.Start();
                    }
                    else
                    {
                        //Unload all content when exiting the level
                        SoundManager.Instance.ClearAllSounds();
                        SoundManager.Instance.ClearMusicCache();

                        AssetManager.Instance.UnloadLoadedContent();

                        GameStateManager.Instance.ChangeGameState(new OverworldState(DataHandler.saveData.LastPlayedWorld));
                    }
                }
                else
                {
                    DataHandler.saveData.Misc.InCustomLevel = false;

                    //Unload all content when exiting the level
                    SoundManager.Instance.ClearAllSounds();
                    SoundManager.Instance.ClearMusicCache();

                    AssetManager.Instance.UnloadLoadedContent();

                    GameStateManager.Instance.ChangeGameState(new TitleState(TitleState.States.Menu, false));
                }
            }
            else
            {
                inGameState.Context.Player.transform.Scale = Interpolation.Interpolate(PlayerStartScale, PlayerEndScale,
                    ElapsedTime / FlyTime, Interpolation.InterpolationTypes.CubicIn);

                int fullRotations = (int)(ElapsedTime / (FlyTime / 2));
                CurRotationTime = CharRotateTime / (fullRotations + 1);

                CurDirection = ClockwiseDirections[(int)(ElapsedTime / CurRotationTime) % ClockwiseDirections.Length];
                inGameState.Context.Player.FacingDir = CurDirection;
            }
        }

        private void HandleSaveData()
        {
            //Copy demonstration data from the level
            DataHandler.saveData.DemoData.CopyDataFrom(inGameState.DemoData);

            WorldData worldData = SaveData.GetWorldData(DataHandler.saveData.LastPlayedWorld);

            //Add data for this level if we don't have it
            if (SaveData.GetLevelData(DataHandler.saveData.LastPlayedWorld, inGameState.LevelNum, out LevelData lvlData) == false)
            {
                lvlData = new LevelData();
                worldData.levelData.Add(inGameState.LevelNum, lvlData);
            }

            //If the level isn't complete, copy over the level data and save it
            if (lvlData.Complete == false)
            {
                lvlData.IncrementTimesComplete();
                lvlData.LvlStats.CopyDataFrom(inGameState.levelStats);

                DataHandler.saveData.Misc.JustFinishedLevels.AddIfNotIn(inGameState.LevelNum);

                DataHandler.SaveSaveData();
            }
            else
            {
                lvlData.IncrementTimesComplete();

                //If the level data is improved, copy it over and save it
                if (LevelStats.IsImprovedData(inGameState.levelStats, lvlData.LvlStats) == true)
                {
                    lvlData.LvlStats.CopyImprovedDataFrom(inGameState.levelStats);

                    DataHandler.SaveSaveData();
                }
            }
        }

        public void Render()
        {
            //Don't render the level once the screen is completely black
            inGameState.RenderLevel();

            //Draw a black box over the screen
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

            RenderingManager.Instance.DrawSprite(Box, Vector2.Zero, ContentGlobals.BoxRect, CurFadeColor, 0f, Vector2.Zero,
                RenderingGlobals.BaseResolution, SpriteEffects.None, 1f);

            RenderingManager.Instance.EndCurrentBatch();
        }
    }
}