/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class LevelCompletionState : IGameState
    {
        protected enum CompletePhase
        {
            Spin, Text, Flash, StatCount, InputWait
        }

        private IngameState inGameState = null;

        protected Texture2D Box = null;
        protected SpriteFont Font24px = null;
        protected SpriteFont Font21px = null;
        protected SpriteFont Font17px = null;
        protected Dictionary<char, SpriteFont.Glyph> FontGlyphs = null;
        private SoundEffect ImprovedStatsSound = null;
        protected SoundEffect TallySound = null;

        protected readonly Color StartFadeColor = Color.Black * 0f;
        protected readonly Color EndFadeColor = Color.Black * .5f;
        protected Color CurFadeColor = Color.Black;

        #region Spin

        private const double TotalSpinTime = (SpinTime * 12) + 6d;
        private const double SpinTime = 80d;
        private readonly Direction[] SpinDirections = new Direction[4] { Direction.Left, Direction.Up, Direction.Right, Direction.Down };
        private double ElapsedSpinTime = 0d;

        #endregion

        #region Text

        protected string CompleteText = "LEVEL COMPLETE!";
        protected double TimeBetweenChars = 40d;
        private readonly Color StartTextColor = Color.White;
        private readonly Color EndTextColor = Color.Gold;

        private readonly Vector2 StartEmphasisTextScale = Vector2.One;
        private readonly Vector2 EndEmphasisTextScale = new Vector2(1.4f, 1.4f);
        private readonly Color StartEmphasisTextColor = Color.Gold;

        private Color CurTextColor = Color.White;
        private Vector2 CompleteTextOrigin = Vector2.Zero;
        protected int NumCharsPrinted = 0;

        #endregion

        #region Player Jumping

        private const double JumpTime = 150d;
        protected Vector2 OrigPlayerPos = Vector2.Zero;
        private double ElapsedJumpTime = 0d;

        #endregion

        #region Stat Counting

        protected const double CountTime = 400d;
        protected const double TallyTime = 105d;
        protected const float StatsScale = 1f;
        protected int NumMoves = 0;
        protected double TimeVal = 0d;

        protected Vector2 CompleteTextPos = RenderingGlobals.BaseResolutionHalved + new Vector2(0f, -80f);
        protected Vector2 StatsStartPos = RenderingGlobals.BaseResolutionHalved + new Vector2(20f, 10f);
        protected Vector2 MovesIconOffset = new Vector2(-70f, -20f);
        protected Vector2 TimeIconOffset = new Vector2(-56f, 22f);

        protected string CompleteMoves = string.Empty;
        protected string CompleteTime = string.Empty;
        private bool ImprovedMoves = false;
        private bool ImprovedTime = false;

        protected Vector2 CompleteMovesOrigin = Vector2.Zero;
        protected Vector2 CompleteTimeOrigin = Vector2.Zero;
        private Vector2 MoveDiffOrigin = Vector2.Zero;
        private Vector2 TimeDiffOrigin = Vector2.Zero;

        //These are not empty if improved and there are existing stats
        private string MoveDiff = string.Empty;
        private string TimeDiff = string.Empty;

        protected readonly Color StatColor = Color.White;
        private readonly Color ImprovedStatColor = new Color(0, 130, 255, 255);

        private const double StatTimeColorLerp = 250f;
        private double ElapsedStatTime = 0d;
        protected double ElapsedTallyTime = TallyTime;

        #endregion

        #region Input

        //private const string InputWaitText = "Press Select/Grab";
        public const double InputTextMoveRate = 1200d;
        protected readonly Color InputTextColor = new Color(113, 195, 225, 255);//new Color(232, 95, 123, 255);
        protected Vector2 InputTextStartPos = new Vector2(RenderingGlobals.BaseResolutionWidth / 2, RenderingGlobals.BaseResolutionHeight - 105f);
        public static readonly Vector2 InputTextStartBob = new Vector2(0f, 5f);
        public static readonly Vector2 InputTextEndBob = new Vector2(0f, -5f);
        private UIText InputTextObj = null;
        private double InputTextElapsedTime = 0d;

        #endregion

        protected const double FadeTime = 500d;
        private const double FlashTime = 1000d;
        private const double PostWaitTime = 800d;
        private const double GlowTime = 250d;
        protected double ElapsedTime = 0d;
        protected double ElapsedFadeTime = 0d;
        private double ElapsedGlowTime = 0d;
        private Color CurGlowColor = Color.Gold;

        /// <summary>
        /// If true, prevents restarting the level from the input prompt.
        /// This is set when unlocking bonus levels so the ending is forced to play.
        /// </summary>
        private bool PreventRestart = false;

        protected CompletePhase Phase = CompletePhase.Spin;

        protected virtual Echidna Player => inGameState.Context.Player;

        protected LevelCompletionState()
        {

        }

        public LevelCompletionState(IngameState ingameState)
        {
            inGameState = ingameState;
        }

        protected virtual void InitPlayerState()
        {
            //Set the player to the portal's position and make it face down
            inGameState.Context.Player.transform.Position = inGameState.Context.LevelPortal.transform.Position;
            inGameState.Context.Player.FacingDir = Direction.Down;
            inGameState.Context.Player.ChangeState(new BlankState());
            inGameState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.Idle);

            OrigPlayerPos = inGameState.Context.Player.transform.Position;
        }

        protected virtual void LoadAssets()
        {
            Box = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            Font24px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont24px);
            Font21px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont21px);
            Font17px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont17px);
            FontGlyphs = Font24px.GetGlyphs();
            ImprovedStatsSound = AssetManager.Instance.LoadSound(ContentGlobals.ConfirmSound);
            TallySound = AssetManager.Instance.LoadSound(ContentGlobals.TallySound);
        }

        protected virtual void InitData()
        {
            SaveData.GetLevelData(DataHandler.saveData.LastPlayedWorld, inGameState.LevelNum, out LevelData lvlData);

            //If the level hasn't been completed, moves and time are automatically improvements
            if (lvlData == null || lvlData.Complete == false)
            {
                ImprovedMoves = ImprovedTime = true;
            }
            //Check if there are improvements
            else
            {
                ImprovedMoves = LevelStats.IsImprovedMoves(inGameState.levelStats, lvlData.LvlStats);

                //Round to the nearest second
                int curSeconds = (int)(inGameState.levelStats.LevelTime / Time.MsPerS);
                int lvlSeconds = (int)(lvlData.LvlStats.LevelTime / Time.MsPerS);
                int diff = curSeconds - lvlSeconds;

                //For improved time, don't display an improved time if the difference is under 1 second, as we currently don't track milliseconds
                ImprovedTime = (LevelStats.IsImprovedTime(inGameState.levelStats, lvlData.LvlStats) == true && diff < 0);

                if (ImprovedMoves == true)
                {
                    MoveDiff = $"-{lvlData.LvlStats.NumMoves - inGameState.levelStats.NumMoves}";
                    MoveDiffOrigin = Font17px.GetOrigin(MoveDiff, 0f, .5f);
                }
                if (ImprovedTime == true)
                {
                    TimeDiff = $"-{LevelGlobals.GetFormattedLevelTimeString(-diff * Time.MsPerS)}";
                    TimeDiffOrigin = Font17px.GetOrigin(TimeDiff, 0f, .5f);
                }
            }

            //Save data immediately after obtaining the improvements
            HandleSaveData();

            //Hide the HUD
            inGameState.LevelMap.ShowHUD = false;
        }

        public virtual void Enter()
        {
            InitPlayerState();

            LoadAssets();

            CompleteTextOrigin = Font24px.GetCenterOrigin(CompleteText);
            ElapsedTime = TimeBetweenChars;

            string selectStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.Select);
            //string backOutStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.BackOut);

            string inputWaitStr = $"Press {selectStr}";///{backOutStr}";

            InputTextObj = UIHelpers.CreateCenterUIText(Font21px, inputWaitStr, Vector2.One, InputTextColor, UITextOptions.Outline, UITextData.Standard);
            InputTextObj.Position = InputTextStartPos + InputTextStartBob;

            //Start stats at 0
            UpdateStatsText(0, 0d);

            InitData();

            CurFadeColor = StartFadeColor;
            CurGlowColor = EndTextColor;

            SoundManager.Instance.StopAndClearMusicTrack();
            SoundManager.Instance.PlayMusic(AssetManager.Instance.LoadMusic(ContentGlobals.LevelCompleteMusic), false);
        }

        public virtual void Exit()
        {
            
        }

        protected void UpdateStatsText(int moves, double time)
        {
            CompleteMoves = moves.ToString();
            CompleteMovesOrigin = Font17px.GetOrigin(CompleteMoves, 0f, .5f);

            CompleteTime = LevelGlobals.GetFormattedLevelTimeString(time);
            CompleteTimeOrigin = Font17px.GetOrigin(CompleteTime, 0f, .5f);
        }

        protected virtual void UpdateObjects()
        {
            inGameState.Context.LevelPortal.Update();
        }

        public void Update()
        {
            //Update the fade over the screen
            ElapsedFadeTime += Time.ElapsedTime.TotalMilliseconds;
            ElapsedStatTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedFadeTime >= FadeTime)
            {
                ElapsedFadeTime = FadeTime;
                CurFadeColor = EndFadeColor;
            }
            else
            {
                CurFadeColor = Color.Lerp(StartFadeColor, EndFadeColor, (float)(ElapsedFadeTime / FadeTime));
            }

            UpdateObjects();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            //Make the player hop as a victory animation
            if (Phase != CompletePhase.Spin)
            {
                UpdatePlayerHopping();
            }

            if (Phase == CompletePhase.Spin)
            {
                HandleSpin();
            }
            else if (Phase == CompletePhase.Text)
            {
                HandleText();
            }
            else if (Phase == CompletePhase.Flash)
            {
                HandleFlash();
            }
            else if (Phase == CompletePhase.StatCount)
            {
                //Count up all stats
                HandleStatCount();
            }
            else if (Phase == CompletePhase.InputWait)
            {
                HandleInputWait();
            }

            if (Phase == CompletePhase.Flash || Phase == CompletePhase.StatCount || Phase == CompletePhase.InputWait)
            {
                ElapsedGlowTime += Time.ElapsedTime.TotalMilliseconds;

                double time = UtilityGlobals.PingPong(ElapsedGlowTime / GlowTime, 0d, 1d);
                CurGlowColor = Interpolation.Interpolate(EndTextColor, StartTextColor, time, Interpolation.InterpolationTypes.Linear);
            }

            //Check for skipping the animation
            if (Phase != CompletePhase.InputWait)
            {
                if (Input.GetButtonDown(InputActions.Select) == true)
                {
                    SkipAnimation();
                }
            }
        }

        /// <summary>
        /// Skips the level complete animation before the input.
        /// </summary>
        protected virtual void SkipAnimation()
        {
            ElapsedFadeTime = FadeTime;
            CurFadeColor = EndFadeColor;
            NumCharsPrinted = CompleteText.Length;

            Player.FacingDir = Direction.Down;
            Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Success);
            Player.AnimationManager.CurrentAnim.Update();

            NumMoves = inGameState.levelStats.NumMoves;
            TimeVal = inGameState.levelStats.LevelTime;

            UpdateStatsText(NumMoves, TimeVal);

            Phase = CompletePhase.InputWait;

            if (string.IsNullOrEmpty(MoveDiff) == false || string.IsNullOrEmpty(TimeDiff) == false)
            {
                SoundManager.Instance.PlaySound(ImprovedStatsSound, false, 1f);
            }
        }

        private void UpdatePlayerHopping()
        {
            ElapsedJumpTime += Time.ElapsedTime.TotalMilliseconds;

            double time = ElapsedJumpTime / JumpTime;

            float sin = UtilityGlobals.Clamp((float)Math.Sin(time), 0f, 1f);
            Vector2 vec = new Vector2(0f, (-10f * sin));

            Player.transform.Position = OrigPlayerPos + vec;
        }

        private void HandleSpin()
        {
            ElapsedSpinTime += Time.ElapsedTime.TotalMilliseconds;
            
            //Change the spin direction
            //We do this so it's more accurate for all framerates
            //If we reset to 0 after reaching the spin time, we'd lose some leftover time, which causes discrepancies over the animation
            Direction curDirection = SpinDirections[(int)(ElapsedSpinTime / SpinTime) % SpinDirections.Length];
            Player.FacingDir = curDirection;
            Player.AnimationManager.CurrentAnim.Update();

            if (ElapsedTime >= TotalSpinTime)
            {
                ElapsedTime = 0d;
                Player.FacingDir = Direction.Down;
                Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Success);

                Phase = CompletePhase.Text;
            }
        }

        private void HandleText()
        {
            //The number of characters printed is the elapsed time over the time between each character
            //This method is more accurate for all framerates as the leftover time is accounted for
            NumCharsPrinted = (int)(ElapsedTime / TimeBetweenChars);

            //If we're finished printing the text, move onto the next phase
            //We wait for it to go over the length so the last character can print the same way as the rest
            if (NumCharsPrinted > CompleteText.Length)
            {
                ElapsedTime = 0d;
                NumCharsPrinted = CompleteText.Length;
                Phase = CompletePhase.Flash;
            }
        }

        private void HandleFlash()
        {
            if (ElapsedTime >= FlashTime)
            {
                ElapsedTime = 0d;
                Phase = CompletePhase.StatCount;
                ElapsedTallyTime = TallyTime;
                ElapsedGlowTime = 0d;
            }
        }

        protected virtual void HandleStatCount()
        {
            ElapsedTallyTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTallyTime >= TallyTime)
            {
                SoundManager.Instance.PlaySound(TallySound, false, 1f);
                ElapsedTallyTime = 0d;
            }

            if (ElapsedTime >= CountTime)
            {
                ElapsedTime = 0d;
                Phase = CompletePhase.InputWait;

                NumMoves = inGameState.levelStats.NumMoves;
                TimeVal = inGameState.levelStats.LevelTime;

                UpdateStatsText(NumMoves, TimeVal);

                if (string.IsNullOrEmpty(MoveDiff) == false || string.IsNullOrEmpty(TimeDiff) == false)
                {
                    SoundManager.Instance.PlaySound(ImprovedStatsSound, false, 1f);
                }
            }
            else
            {
                double time = ElapsedTime / CountTime;
                NumMoves = Interpolation.Interpolate(0, inGameState.levelStats.NumMoves, time, Interpolation.InterpolationTypes.Linear);
                TimeVal = Interpolation.Interpolate(0, inGameState.levelStats.LevelTime, time, Interpolation.InterpolationTypes.Linear);

                UpdateStatsText(NumMoves, TimeVal);
            }
        }

        protected virtual void HandlePressInput()
        {
            inGameState.ChangeLevelState(new LevelFlyOutState(inGameState, EndFadeColor, Color.Black, FadeTime, FadeTime, false));
        }

        protected virtual void HandleRestartInput()
        {
            LevelData data = SaveData.GetLevelData(DataHandler.saveData.LastPlayedWorld, inGameState.LevelNum);

            string displayBestMoves = data.LvlStats.NumMoves.ToString();
            string displayBestTime = LevelGlobals.GetFormattedLevelTimeString(data.LvlStats.LevelTime);

            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);

            //Restart the level using the saved level data
            IngameState ingameState = new IngameState(inGameState.LevelNum, inGameState.LevelMap.Map, inGameState.LevelEventType,
                inGameState.DisplayLevelNum, inGameState.DisplayLevelName, displayBestMoves, displayBestTime);
            ingameState.ChangeLevelState(new LevelIntroState(ingameState, ingameState.Context.Player.transform.Position));

            GameStateManager.Instance.ChangeGameState(ingameState);
        }

        private void HandleInputWait()
        {
            InputTextElapsedTime += Time.ElapsedTime.TotalMilliseconds;
            double time = UtilityGlobals.PingPong(InputTextElapsedTime / (InputTextMoveRate / 2), 0d, 1d);
            InputTextObj.Position = Interpolation.Interpolate(InputTextStartPos + InputTextStartBob, InputTextStartPos + InputTextEndBob,
                time, Interpolation.InterpolationTypes.CubicInOut);

            //Wait for input
            if (Input.GetButtonDown(InputActions.Select) == true)
            {
                ElapsedTime = 0d;

                //Transition to the fly out state
                SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);
                HandlePressInput();
            }
            //Restart the level if the player presses restart here
            else if (PreventRestart == false && Input.GetButtonDown(InputActions.Restart) == true)
            {
                HandleRestartInput();
            }
        }

        private void HandleSaveData()
        {
            //Copy demonstration data from the level
            DataHandler.saveData.DemoData.CopyDataFrom(inGameState.DemoData);

            WorldData worldData = SaveData.GetWorldData(DataHandler.saveData.LastPlayedWorld);

            //Add data for this level if we don't have it
            if (SaveData.GetLevelData(DataHandler.saveData.LastPlayedWorld, inGameState.LevelNum, out LevelData lvlData) == false)
            {
                lvlData = new LevelData();
                worldData.levelData.Add(inGameState.LevelNum, lvlData);
            }

            //If the level isn't complete, copy over the level data and save it
            if (lvlData.Complete == false)
            {
                lvlData.IncrementTimesComplete();
                lvlData.LvlStats.CopyDataFrom(inGameState.levelStats);

                DataHandler.saveData.Misc.JustFinishedLevels.AddIfNotIn(inGameState.LevelNum);

                DataHandler.SaveSaveData();
            }
            else
            {
                lvlData.IncrementTimesComplete();

                //If the level data is improved, copy it over and save it
                if (LevelStats.IsImprovedData(inGameState.levelStats, lvlData.LvlStats) == true)
                {
                    lvlData.LvlStats.CopyImprovedDataFrom(inGameState.levelStats);

                    DataHandler.SaveSaveData();
                }
            }

            //Check for unlocking bonus levels
            if (inGameState.LevelMap.UnlocksBonus == true && DataHandler.saveData.Misc.InCustomLevel == false && DataHandler.saveData.CompleteData.UnlockBonus == false)
            {
                HandleUnlockBonusLevels();

                //Prevent restarting to force the ending to play
                PreventRestart = true;

                DataHandler.SaveSaveData();

                //Reload level description data for when the player goes back to the game after bonuses are unlocked
                DataHandler.ReloadLevelDescriptionData(Engine.NumWorlds);
            }
        }

        private void HandleUnlockBonusLevels()
        {
            DataHandler.saveData.CompleteData.UnlockBonus = true;
            DataHandler.saveData.Misc.ShowBonusUnlock = true;

            //Send the player back to world 1 after the credits
            DataHandler.saveData.LastPlayedLevel = int.MinValue;
            DataHandler.saveData.LastPlayedWorld = 0;

            //Clear the levels completed
            DataHandler.saveData.Misc.JustFinishedLevels.Clear();
        }

        protected virtual void DrawMap()
        {
            //Don't render the level once the screen is completely black
            inGameState.RenderLevel();
        }

        public void Render()
        {
            DrawMap();

            //Draw a black box over the screen
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

            RenderingManager.Instance.DrawSprite(Box, Vector2.Zero, ContentGlobals.BoxRect, CurFadeColor, 0f, Vector2.Zero,
                RenderingGlobals.BaseResolution, SpriteEffects.None, 1f);

            Vector2 offset = Vector2.Zero;

            //Render characters individually for the text phase
            if (Phase == CompletePhase.Text)
            {
                for (int i = 0; i < NumCharsPrinted; i++)
                {
                    Color color = EndTextColor;

                    if (i == (NumCharsPrinted - 1))
                    {
                        color = StartTextColor;
                    }

                    //Draw text
                    offset = RenderingManager.Instance.spriteBatch.DrawCharacterOutline(2f, Color.Black, Font24px, CompleteText[i], FontGlyphs, offset,
                        CompleteTextPos, color, 0f, CompleteTextOrigin, Vector2.One, SpriteEffects.None, .31f);
                }
            }
            //For every other phase, just draw the whole string in one go
            else if (Phase != CompletePhase.Spin)
            {
                RenderingManager.Instance.spriteBatch.DrawStringOutline(2f, Color.Black, Font24px, CompleteText, CompleteTextPos, CurGlowColor, 0f,
                    CompleteTextOrigin, Vector2.One, SpriteEffects.None, .31f);

                //In the flash phase, draw an expanding duplicate to emphasize the completion
                if (Phase == CompletePhase.Flash)
                {
                    double time = ElapsedTime / FlashTime;
                    Vector2 scale = Interpolation.Interpolate(StartEmphasisTextScale, EndEmphasisTextScale, time, Interpolation.InterpolationTypes.Linear);

                    Color colorLerp = Interpolation.Interpolate(StartEmphasisTextColor, EndTextColor, time, Interpolation.InterpolationTypes.Linear);

                    RenderingManager.Instance.spriteBatch.DrawString(Font24px, CompleteText, CompleteTextPos, colorLerp * (1f - (float)time),
                        0f, CompleteTextOrigin, scale, SpriteEffects.None, .31f);
                }
            }

            //Show stats
            if (Phase == CompletePhase.StatCount || Phase == CompletePhase.InputWait)
            {
                DrawStats();
            }

            if (Phase == CompletePhase.InputWait)
            {
                DrawInputText();
            }
            
            RenderingManager.Instance.EndCurrentBatch();
        }

        protected virtual void DrawStatText()
        {
            //Draw the level stats
            RenderingManager.Instance.spriteBatch.DrawStringOutline(2f, Color.Black, Font17px, CompleteMoves, StatsStartPos + new Vector2(0f, -1f), StatColor, 0f,
                CompleteMovesOrigin, StatsScale, SpriteEffects.None, .32f);
            RenderingManager.Instance.spriteBatch.DrawStringOutline(2f, Color.Black, Font17px, CompleteTime, StatsStartPos + new Vector2(0f, 41f), StatColor, 0f,
                CompleteTimeOrigin, StatsScale, SpriteEffects.None, .32f);
        }

        protected void DrawStatIcons()
        {
            Vector2 origin = new Vector2(0f, .5f);

            //Draw stat icons
            RenderingManager.Instance.DrawSprite(Box, StatsStartPos + MovesIconOffset, ContentGlobals.MovesIconRectLarge, Color.White, 0f, origin,
                Vector2.One, SpriteEffects.None, .32f);
            RenderingManager.Instance.DrawSprite(Box, StatsStartPos + TimeIconOffset, ContentGlobals.TimeIconRectLarge, Color.White, 0f, origin,
                Vector2.One, SpriteEffects.None, .32f);
        }

        protected void DrawStats()
        {
            DrawStatText();

            //Draw improved stats and change colors only after they're finished counting up
            if (Phase != CompletePhase.StatCount)
            {
                float time = UtilityGlobals.PingPong(ElapsedStatTime / StatTimeColorLerp, 0f, 1f);

                const float posXOffsetMoves = 10f;

                if (string.IsNullOrEmpty(MoveDiff) == false)
                {
                    Color movesColor = Interpolation.Interpolate(StatColor, ImprovedStatColor, time, Interpolation.InterpolationTypes.Linear);
                    Vector2 posOffset = Font17px.MeasureString(CompleteMoves) * StatsScale;

                    RenderingManager.Instance.spriteBatch.DrawStringOutline(2f, Color.Black, Font17px, MoveDiff, StatsStartPos + new Vector2((int)(posOffset.X) + posXOffsetMoves, -1f), movesColor, 0f,
                        MoveDiffOrigin, StatsScale, SpriteEffects.None, .32f);
                }
                if (string.IsNullOrEmpty(TimeDiff) == false)
                {
                    Color timeColor = Interpolation.Interpolate(StatColor, ImprovedStatColor, time, Interpolation.InterpolationTypes.Linear);
                    Vector2 posOffset = Font17px.MeasureString(CompleteTime) * StatsScale;

                    RenderingManager.Instance.spriteBatch.DrawStringOutline(2f, Color.Black, Font17px, TimeDiff, StatsStartPos + new Vector2((int)(posOffset.X) + posXOffsetMoves, 41f), timeColor, 0f,
                        TimeDiffOrigin, StatsScale, SpriteEffects.None, .32f);
                }
            }

            DrawStatIcons();
        }

        private void DrawInputText()
        {
            InputTextObj.Render();
        }
    }
}