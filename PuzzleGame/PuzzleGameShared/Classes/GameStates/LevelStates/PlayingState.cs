/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace PuzzleGame
{
    public class PlayingState : IGameState
    {
        private IngameState inGameState = null;
        private const double RestartTime = 300d;
        private double ElapsedRestartTime = 0d;

        private SoundEffect UndoSound = null;

        public PlayingState(IngameState ingameState)
        {
            inGameState = ingameState;
        }

        public void Enter()
        {
            if (inGameState.LevelMap.AllowsUndo == true)
            {
                inGameState.Context.Player.BlockMovedEvent -= OnPlayerBlockMoved;
                inGameState.Context.Player.BlockMovedEvent += OnPlayerBlockMoved;

                UndoSound = AssetManager.Instance.LoadSound(ContentGlobals.UndoSound);
            }
        }

        public void Exit()
        {
            inGameState.Context.Player.BlockMovedEvent -= OnPlayerBlockMoved;

            UndoSound = null;
        }

        private void OnPlayerBlockMoved(Block blockMoved, in Vector2 moveAmount, in double moveTime)
        {
            RecordObjects();
        }

        private void RecordObjects()
        {
            //GCWatcher.Start();

            RecordingGlobals.RecordedLevelData rodLvlData = new RecordingGlobals.RecordedLevelData();

            //rodLvlData.LevelTime = inGameState.levelStats.LevelTime;

            //Count how many objects will be recorded and allocate a list with that capacity to avoid the list resizing partway through
            int recordedObjCount = 1 + inGameState.Context.Blocks.Count + inGameState.Context.Switches.Count + inGameState.Context.Warps.Count
                + inGameState.Context.RockShooters.Count;
            List<RecordingGlobals.RecordedObjData> Rods = new List<RecordingGlobals.RecordedObjData>(recordedObjCount);
            rodLvlData.ObjData = Rods;

            RecordingGlobals.RecordedObjData Rod = new RecordingGlobals.RecordedObjData(inGameState.Context.Player, rodLvlData);
            Rod.RecordObject();

            Rods.Add(Rod);

            int blockCount = inGameState.Context.Blocks.Count;
            for (int i = 0; i < blockCount; i++)
            {
                Block b = inGameState.Context.Blocks[i];

                //Don't record ungrabbable blocks without a state machine, which means they're not controlled by switches or anything else
                //Always record pipe blocks, as even ungrabbable ones can be interacted with
                if (b.Grabbable == true || b.BlockFSM != null || b.BlockType == BlockTypes.Pipe)
                {
                    Rod = new RecordingGlobals.RecordedObjData(b, rodLvlData);
                    Rod.RecordObject();

                    Rods.Add(Rod);
                }
            }

            int switchCount = inGameState.Context.Switches.Count;
            for (int i = 0; i < switchCount; i++)
            {
                Rod = new RecordingGlobals.RecordedObjData(inGameState.Context.Switches[i], rodLvlData);
                Rod.RecordObject();
            
                Rods.Add(Rod);
            }

            int warpCount = inGameState.Context.Warps.Count;
            for (int i = 0; i < warpCount; i++)
            {
                Rod = new RecordingGlobals.RecordedObjData(inGameState.Context.Warps[i], rodLvlData);
                Rod.RecordObject();

                Rods.Add(Rod);
            }

            int rockShooterCount = inGameState.Context.RockShooters.Count;
            for (int i = 0; i < rockShooterCount; i++)
            {
                Rod = new RecordingGlobals.RecordedObjData(inGameState.Context.RockShooters[i], rodLvlData);
                Rod.RecordObject();

                Rods.Add(Rod);
            }

            rodLvlData.CollisionList = new List<ICollisionObj>(inGameState.Context.LevelCollision);

            //If we're at the max number of recorded moves, remove the first one, as it's the oldest
            if (inGameState.RecordedMoveList.Count == RecordingGlobals.MaxMoveRecords)
            {
                inGameState.RecordedMoveList.RemoveAt(0);
            }

            inGameState.RecordedMoveList.Add(rodLvlData);

            //GCWatcher.Stop();
            //long memory = GCWatcher.TotalMemoryAllocatedInTheBlock;
            //GCWatcher.Reset();
            //Debug.Log(memory);
        }

        public void Update()
        {
            //Pause the game
            if (inGameState.LevelMap.AllowsPause == true && Input.GetButtonDown(InputActions.Pause) == true)
            {
                inGameState.ChangeLevelState(new PausedState(inGameState, /*inGameState.LevelMap.AllowsRestart, */inGameState.LevelMap.AllowsExiting));
                return;
            }

            //Handle undoing
            if (Input.GetButton(InputActions.Undo) == true && 
                (inGameState.Context.Player.PlayerState == PlayerStates.Idle || inGameState.Context.Player.PlayerState == PlayerStates.Grab))
            {
                if (inGameState.RecordedMoveList.Count > 0)
                {
                    //Reset the camera in case it moved from a camera shake effect
                    inGameState.camera.LookAtAndClamp(inGameState.camera.DefaultPosition, inGameState.LevelMap.MapBounds);

                    int removeIndex = inGameState.RecordedMoveList.Count - 1;
                    RecordingGlobals.RecordedLevelData rodLvlData = inGameState.RecordedMoveList[removeIndex];

                    //Subtract a move, then update the HUD to have it reflect right away
                    inGameState.levelStats.NumMoves = UtilityGlobals.Clamp(inGameState.levelStats.NumMoves - 1, 0, LevelGlobals.MaxLevelMoves);
                    //inGameState.levelStats.LevelTime = rodLvlData.LevelTime;

                    if (inGameState.LevelMap.ShowHUD == true)
                        inGameState.gameHUD.Update();

                    //Remove from the list
                    inGameState.RecordedMoveList.RemoveAt(removeIndex);

                    SoundManager.Instance.PlaySound(UndoSound, false, .8f);

                    inGameState.ChangeLevelState(new LevelUndoState(inGameState, rodLvlData));
                    return;
                }
            }

            //Restart the level by holding R
            if (inGameState.LevelMap.AllowsRestart == true && Input.GetButton(InputActions.Restart) == true)
            {
                ElapsedRestartTime += Time.ElapsedTime.TotalMilliseconds;
                if (inGameState.LevelMap.ShowHUD == true)
                {
                    inGameState.gameHUD.RestartingLevel = true;
                }

                //Restart the level after holding it for enough time
                if (ElapsedRestartTime >= RestartTime)
                {
                    SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false);
                    inGameState.RestartLevel();
                    return;
                }
            }
            else
            {
                ElapsedRestartTime = 0d;
                if (inGameState.LevelMap.ShowHUD == true)
                {
                    inGameState.gameHUD.RestartingLevel = false;
                }
            }

            //Increment time spent in the level - cap it at 99 minutes
            inGameState.levelStats.LevelTime = UtilityGlobals.Clamp(inGameState.levelStats.LevelTime + Time.ElapsedTime.TotalMilliseconds, 0d, LevelGlobals.MaxLevelTime);

            QueryTriggers();

            inGameState.Context.LevelPortal.Update();

            //for (int i = 0; i < inGameState.Context.Spots.Count; i++)
            //{
            //    inGameState.Context.Spots[i].Update();
            //}

            for (int i = 0; i < inGameState.Context.Blocks.Count; i++)
            {
                inGameState.Context.Blocks[i].Update();
            }

            inGameState.Context.Player.Update();

            //Have the camera follow the player if it should
            if (inGameState.LevelMap.CameraFollow == true)
            {
                inGameState.camera.LookAtAndClamp(inGameState.Context.Player.transform.Position, inGameState.LevelMap.MapBounds);

                //Set the default position so it can be used for camera shake effects
                inGameState.camera.DefaultPosition = inGameState.camera.Position;
            }

            for (int i = 0; i < inGameState.Context.Warps.Count; i++)
            {
                inGameState.Context.Warps[i].Update();
            }

            for (int i = 0; i < inGameState.Context.Switches.Count; i++)
            {
                inGameState.Context.Switches[i].Update();
            }

            for (int i = 0; i < inGameState.Context.RockShooters.Count; i++)
            {
                inGameState.Context.RockShooters[i].Update();
            }

            for (int i = 0; i < inGameState.Context.LevelText.Count; i++)
            {
                inGameState.Context.LevelText[i].Update();
            }

            if (inGameState.LevelMap.ShowHUD == true)
                inGameState.gameHUD.Update();
        }

        private void QueryTriggers()
        {
            //Query trigger collisions
            for (int i = 0; i < inGameState.Context.LevelCollision.Count; i++)
            {
                ICollisionObj collision = inGameState.Context.LevelCollision[i];

                //Query only if the object should constantly query
                if (collision.TriggerQuery == TriggerQueries.Constant)
                {
                    bool shouldBreak = collision.QueryTrigger(inGameState.Context.LevelCollision);

                    if (shouldBreak == true)
                    {
                        break;
                    }
                }
            }
        }

        public void Render()
        {
            inGameState.RenderLevel();
        }
    }
}