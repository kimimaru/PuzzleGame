/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using static PuzzleGame.RecordingGlobals;

namespace PuzzleGame
{
    public class LevelUndoState : IGameState
    {
        private IngameState inGameState = null;

        private RenderTarget2D RTarget = null;
        private Effect DistortShader = null;

        private RecordedLevelData RecordedLvlData = null;
        private TransformData[] OrigTransformData = null;

        private const float BaseXOffset = 5f;

        private const double TotalTime = 402d;
        private double ElapsedTime = 0d;

        private bool FirstRun = false;

        public LevelUndoState(IngameState ingameState, RecordedLevelData recordedLvlData)
        {
            inGameState = ingameState;
            RecordedLvlData = recordedLvlData;

            DistortShader = AssetManager.Instance.LoadShader(ContentGlobals.DistortShader);
            DistortShader.SetParameterValue("textureSize", RenderingGlobals.BaseResolution);
            DistortShader.SetParameterValue("intensity", 7f);
            DistortShader.SetParameterValue("moveAmtX", BaseXOffset);
            DistortShader.SetParameterValue("shiftTime", 0f);
        }

        public void Enter()
        {
            //Put the player in idle if not already
            if (inGameState.Context.Player.PlayerState != PlayerStates.Idle)
            {
                inGameState.Context.Player.ChangeState(new IdleState());
            }

            //Optimization: only lerp the transform data of objects at the root level
            List<RecordedObjData> objData = RecordedLvlData.ObjData;
            int count = objData.Count;
            OrigTransformData = new TransformData[count];

            for (int i = 0; i < count; i++)
            {
                OrigTransformData[i] = new TransformData(objData[i].ObjTransform);
            }

            RenderingGlobals.ResizeRenderTarget(ref RTarget, RenderingGlobals.BaseResolution);
        }

        public void Exit()
        {
            RTarget.Dispose();
            RTarget = null;
        }

        public void Update()
        {
            List<RecordedObjData> objData = RecordedLvlData.ObjData;

            //Set all properties the first time and set up the lists
            if (FirstRun == false)
            {
                for (int i = objData.Count - 1; i >= 0; i--)
                {
                    RecordedObjData obj = objData[i];
                    obj.SetObjectProperties();

                    //Consolidate to only the objects with transforms
                    //if (obj.ObjTransform == null)
                    //{
                    //    objData.RemoveAt(i);
                    //}
                }

                //Clear the level's lists and use the recorded data
                inGameState.Context.ClearAllObjects();
                inGameState.Context.AddObjects(RecordedLvlData.CollisionList);

                FirstRun = true;
            }

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            double timeFactor = ElapsedTime / TotalTime;

            DistortShader.SetParameterValue("moveAmtX", Interpolation.Interpolate(BaseXOffset, 0f, timeFactor, Interpolation.InterpolationTypes.Linear));
            DistortShader.SetParameterValue("shiftTime", (float)ElapsedTime / (184f / 2f));

            int count = objData.Count;

            for (int i = 0; i < count; i++)
            {
                RecordedObjData rod = objData[i];
                Transform rodTrans = rod.ObjTransform;

                if (rodTrans == null)
                {
                    continue;
                }

                ref TransformData transformData = ref OrigTransformData[i];

                rodTrans.Position = Interpolation.Interpolate(transformData.Position, rod.Position, timeFactor, Interpolation.InterpolationTypes.CubicInOut);
                rodTrans.Rotation = Interpolation.Interpolate(transformData.Rotation, rod.Rotation, timeFactor, Interpolation.InterpolationTypes.CubicInOut);
                rodTrans.Scale = Interpolation.Interpolate(transformData.Scale, rod.Scale, timeFactor, Interpolation.InterpolationTypes.CubicInOut);
            }

            //Have the camera follow the player while undoing if it should
            if (inGameState.LevelMap.CameraFollow == true)
            {
                inGameState.camera.LookAtAndClamp(inGameState.Context.Player.transform.Position, inGameState.LevelMap.MapBounds);
            }

            if (ElapsedTime >= TotalTime)
            {
                for (int i = 0; i < count; i++)
                {
                    RecordedObjData rod = objData[i];
                    Transform rodTrans = rod.ObjTransform;

                    rodTrans.Position = rod.Position;
                    rodTrans.Rotation = rod.Rotation;
                    rodTrans.Scale = rod.Scale;
                }

                inGameState.ChangeLevelState(new PlayingState(inGameState));
            }
        }

        public void Render()
        {
            //Render only the level with the distort shader and exclude the HUD
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();

            RenderingManager.Instance.EndCurrentBatch();

            RenderingGlobals.DrawRenderTargetToOther(RTarget, RenderingManager.Instance.GetMainRenderTarget, DistortShader);

            if (inGameState.LevelMap.ShowHUD == true)
            {
                RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                    BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

                inGameState.gameHUD.Render();

                RenderingManager.Instance.EndCurrentBatch();
            }

            RenderingGlobals.DrawRenderTargetToOther(RenderingManager.Instance.GetMainRenderTarget, RTarget, null);
        }
    }
}