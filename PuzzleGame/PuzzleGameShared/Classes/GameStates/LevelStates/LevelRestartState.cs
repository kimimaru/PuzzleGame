/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace PuzzleGame
{
    public class LevelRestartState : IGameState
    {
        private IngameState inGameState = null;

        private Texture2D Box = null;
        private Vector2 PlayerStartPos = Vector2.Zero;

        private const double TotalFadeTime = 300d;
        private double ElapsedTime = 0d;

        /// <summary>
        /// A single frame used to wait out a longer update loop in case the level takes longer than a frame to reload.
        /// This allows this to play smoothly regardless of how long the load takes.
        /// </summary>
        private bool FrameWait = false;

        public LevelRestartState(IngameState ingameState, in Vector2 playerStartPos)
        {
            inGameState = ingameState;
            PlayerStartPos = playerStartPos;

            Box = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
        }

        public void Enter()
        {
            inGameState.Context.Player.transform.Position = PlayerStartPos;
            inGameState.Context.Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.Idle);
            inGameState.Context.Player.FacingDir = Direction.Down;
        }

        public void Exit()
        {

        }

        public void Update()
        {
            //Wait out the frame
            if (FrameWait == false)
            {
                FrameWait = true;
                return;
            }

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= TotalFadeTime)
            {
                inGameState.ChangeLevelState(new PlayingState(inGameState));
            }
        }

        public void Render()
        {
            inGameState.RenderLevel();

            //Draw a black overlay over everything else
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

            Color color = Interpolation.Interpolate(Color.Black, Color.Transparent, ElapsedTime / TotalFadeTime, Interpolation.InterpolationTypes.Linear);

            RenderingManager.Instance.DrawSprite(Box, Vector2.Zero, ContentGlobals.BoxRect, color, 0f, Vector2.Zero, RenderingGlobals.BaseResolution, SpriteEffects.None, 1f);

            RenderingManager.Instance.EndCurrentBatch();
        }
    }
}