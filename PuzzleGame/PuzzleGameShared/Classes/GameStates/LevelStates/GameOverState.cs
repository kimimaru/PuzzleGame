/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class GameOverState : IGameState
    {
        private enum Transitions
        {
            Wait, Rise, Fall
        }

        private IngameState inGameState = null;

        private Texture2D Overlay = null;

        private const double TotalRiseTime = 100d;
        private Vector2 RiseOffset = new Vector2(0f, -10f);
        private const double TotalFallTime = 600d;
        private const float FallEndYOffset = RenderingGlobals.BaseResolutionHeight + 100f;
        private Vector2 FallEndPos = new Vector2(0f, RenderingGlobals.BaseResolutionHeight + 100f);

        private const double TotalWaitTime = 250d;
        private readonly Color StartingColor = Color.White;
        private readonly Color EndingColor = Color.Red;
        private double FadeOffset = 200d;
        private Vector2 StartingPlayerPos = Vector2.Zero;
        private Vector2 PlayerRiseEndPos = Vector2.Zero;

        private double ElapsedTime = 0d;

        private Transitions CurTransition = Transitions.Wait;

        private Vector2 CameraStartPos = Vector2.Zero;
        private int CamShakeIndex = 0;
        private const double ShakeChangeTime = 25d;
        private double ElapsedShakeTime = 0d;

        public GameOverState(IngameState ingameState)
        {
            inGameState = ingameState;
        }

        public void Enter()
        {
            Overlay = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            inGameState.Context.Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.Idle);

            StartingPlayerPos = inGameState.Context.Player.transform.Position;
            PlayerRiseEndPos = StartingPlayerPos + RiseOffset;
            FallEndPos = StartingPlayerPos;
            FallEndPos.Y += FallEndYOffset;

            CameraStartPos = inGameState.camera.Position;
        }

        public void Exit()
        {
            Overlay = null;
        }

        public void Update()
        {
            if (CurTransition == Transitions.Wait)
            {
                HandleWait();
            }
            else if (CurTransition == Transitions.Rise)
            {
                HandleRise();
            }
            else
            {
                HandleFall();
            }

            inGameState.Context.Player.AnimationManager.CurrentAnim.Update();
        }

        private void HandleWait()
        {
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            inGameState.camera.LookAtAndClamp(CameraStartPos + LevelGlobals.StandardCameraShakes[CamShakeIndex], inGameState.LevelMap.MapBounds);

            ElapsedShakeTime += Time.ElapsedTime.TotalMilliseconds;
            if (ElapsedShakeTime >= ShakeChangeTime)
            {
                CamShakeIndex = UtilityGlobals.Wrap(CamShakeIndex + 1, 0, LevelGlobals.StandardCameraShakes.Length - 1);
                ElapsedShakeTime = 0d;
            }

            if (ElapsedTime >= TotalWaitTime)
            {
                CurTransition = Transitions.Rise;
                ElapsedTime = 0d;
                inGameState.Context.Player.TintColor = Color.White;
                inGameState.camera.SetTranslation(CameraStartPos);

                SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.GameOverSound), false, 1f);
            }
            else
            {
                float time = UtilityGlobals.PingPong(ElapsedTime / (TotalWaitTime / 2d), 0f, 1f);
                inGameState.Context.Player.TintColor = Interpolation.Interpolate(StartingColor, EndingColor, time, Interpolation.InterpolationTypes.Linear);
            }
        }

        private void HandleRise()
        {
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            //Start falling
            if (ElapsedTime >= TotalRiseTime)
            {
                inGameState.Context.Player.transform.Position = PlayerRiseEndPos;
                CurTransition = Transitions.Fall;
                ElapsedTime = 0d;
            }
            else
            {
                inGameState.Context.Player.transform.Position = Interpolation.Interpolate(StartingPlayerPos, PlayerRiseEndPos, ElapsedTime / TotalRiseTime,
                    Interpolation.InterpolationTypes.QuadOut);
            }
        }

        private void HandleFall()
        {
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            //Restart the level
            if (ElapsedTime >= TotalFallTime)
            {
                inGameState.Context.Player.transform.Position = FallEndPos;
                inGameState.RestartLevel();
            }
            else
            {
                inGameState.Context.Player.transform.Position = Interpolation.Interpolate(PlayerRiseEndPos, FallEndPos, ElapsedTime / TotalFallTime,
                    Interpolation.InterpolationTypes.QuadIn);
            }
        }

        public void Render()
        {
            inGameState.RenderLevel();

            if (CurTransition != Transitions.Fall) return;

            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

            double time = UtilityGlobals.Clamp(ElapsedTime - FadeOffset, 0d, TotalFallTime);
            double fallTime = TotalFallTime - FadeOffset;
            Color overlayColor = Interpolation.Interpolate(Color.Transparent, Color.Black, time / fallTime, Interpolation.InterpolationTypes.Linear);

            RenderingManager.Instance.DrawSprite(Overlay, Vector2.Zero, ContentGlobals.BoxRect, overlayColor, 0f, Vector2.Zero, RenderingGlobals.BaseResolution,
                SpriteEffects.None, 1f);

            RenderingManager.Instance.EndCurrentBatch();
        }
    }
}