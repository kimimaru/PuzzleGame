/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace PuzzleGame
{
    public class PausedState : UIMenuManager, IGameState
    {
        private IngameState inGameState = null;
        private SpriteFont Font = null;
        private SpriteFont DescriptionFont = null;
        private SpriteFont StatsFont = null;
        private Texture2D UITex = null;
        private UIRenderableContainer LevelInfoContainer = null;

        private readonly Color OutlineColor = Color.Black;

        private readonly Color FadeColor = Color.Black * .7f;

        private readonly Color TextColor = Color.White;//new Color(0, 140, 0, 255);

        private const double FadeTime = 200d;
        private double ElapsedTime = 0d;

        private UIMenuOption.OptionSelected MenuOptionSelected = null;
        private UIMenuOption.OptionDeselected MenuOptionDeselected = null;

        private UIInputMenu InitMenu = null;
        private OptionsMenuNew OptionMenu = null;
        private PuzzleInfoMenu PuzzleInfoMenu = null;
        private ControlSelectMenu ControlScreen = null;
        //private ControlsMenu ControlsScreen = null;

        private MenuTransition.TransitionEnd OnTransitionEnd = null;

        public PausedState(IngameState ingameState, /*in bool allowRestart, */in bool allowExitLevel)
            : base()
        {
            inGameState = ingameState;

            Font = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont21px);
            DescriptionFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont13px);
            StatsFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont10px);
            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);

            OnTransitionEnd = ShowLevelInfoContainer;

            if (ShouldLoadDrawLevelContainer() == true)
            {
                SetUpLevelInfo();
            }

            MenuOptionSelected = OnOptionSelected;
            MenuOptionDeselected = OnOptionDeselected;

            InitMenu = new UIInputMenuPauseBack(5, RenderingGlobals.BaseResolutionHalved + new Vector2(0f, -40f));
            InitMenu.SetBackoutOption(MenuBackOut);

            const float yDiff = 45f;

            Vector2 relativePos = new Vector2(-85f, 15f);
            Vector2 origin = new Vector2(0f, .5f);

            InitMenu.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Continue", origin, Vector2.One, UITextOptions.Outline, UITextData.Standard),
                relativePos, new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, Continue, null));

            relativePos.Y += yDiff;
            //if (allowRestart == true)
            //{
            //    InitMenu.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Restart", origin, Vector2.One, UITextOptions.Outline, UITextData.Standard),
            //        relativePos, new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, Restart, null));
            //    relativePos.Y += yDiff;
            //}

            InitMenu.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Options", origin, Vector2.One, UITextOptions.Outline, UITextData.Standard),
                relativePos, new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, Options, null));
            relativePos.Y += yDiff;

            InitMenu.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Controls", origin, Vector2.One, UITextOptions.Outline, UITextData.Standard),
                relativePos, new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, Controls, null));
            relativePos.Y += yDiff;

            InitMenu.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Glossary", origin, Vector2.One, UITextOptions.Outline, UITextData.Standard),
                relativePos, new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, OpenInfoScreen, null));
            relativePos.Y += yDiff;

            //Allow exiting only to either the level or the menu
            //The menu is relevant only if exiting the level is disallowed (Ex. first-time intro cutscene)
            if (allowExitLevel == true)
            {
                InitMenu.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Exit level", origin, Vector2.One, UITextOptions.Outline, UITextData.Standard),
                    relativePos, new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, LevelExit, null));
                relativePos.Y += yDiff;
            }
            else
            {
                InitMenu.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Exit to menu", origin, Vector2.One, UITextOptions.Outline, UITextData.Standard),
                    relativePos, new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, MenuExit, null));
            }

            MenuStack.Push(InitMenu);

            UIHelpers.SetDefaultMenuSounds(InitMenu, false, false);
            InitMenu.ResetSelectionStates();
        }

        private void OnOptionSelected(in int optionIndex)
        {
            UIInputMenu menu = MenuStack.Peek();

            menu.Elements[optionIndex].TintColor = TextColor;
        }

        private void OnOptionDeselected(in int optionIndex)
        {
            UIInputMenu menu = MenuStack.Peek();

            menu.Elements[optionIndex].TintColor = new Color(180, 180, 180, 255);
        }

        private void ShowLevelInfoContainer()
        {
            if (LevelInfoContainer != null)
            {
                LevelInfoContainer.Visible = true;
            }
        }

        public void Enter()
        {

        }

        public void Exit()
        {
            InitMenu?.CleanUp();
            OptionMenu?.CleanUp();
            OptionMenu = null;
            PuzzleInfoMenu?.CleanUp();
            ControlScreen?.CleanUp();
        }

        private bool ShouldLoadDrawLevelContainer()
        {
            return (string.IsNullOrEmpty(inGameState.DisplayLevelName) == false || string.IsNullOrEmpty(inGameState.DisplayBestMoves) == false ||
                string.IsNullOrEmpty(inGameState.DisplayBestTime) == false);
        }

        public void Update()
        {
            base.UpdateMenu();

            if (ElapsedTime >= FadeTime) return;

            //Lerp the view between full color and grayscale
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
            if (ElapsedTime >= FadeTime)
            {
                ElapsedTime = FadeTime;
            }
        }

        private void SetUpLevelInfo()
        {
            Vector2 containerSize = new Vector2(180f, 104f);
            if (string.IsNullOrEmpty(inGameState.DisplayBestMoves) == true || string.IsNullOrEmpty(inGameState.DisplayBestTime) == true)
            {
                //Reduce the size of the container
                containerSize.Y -= 40f;
            }

            NineSlicedSprite nineSliced = new NineSlicedSprite(UITex, ContentGlobals.PanelRect, 5, 5, 5, 5);

            Vector2 pos = new Vector2(RenderingGlobals.BaseResolutionWidth / 2, (containerSize.Y / 2) - nineSliced.BottomLine);

            Vector2 containerOrigin = (containerSize / 2);
            containerOrigin.Floor();
            Vector2 containerPos = pos - containerOrigin;

            LevelInfoContainer = new UIRenderableContainer(5, containerPos);

            UINineSlicedSprite uiNineSliced = new UINineSlicedSprite(nineSliced);
            uiNineSliced.Scale = containerSize;
            LevelInfoContainer.AddElementRelative(uiNineSliced, Vector2.Zero);

            UISprite movesIcon = new UISprite(new Sprite(UITex, ContentGlobals.MovesIconRectSmall, new Vector2(0f, .5f)));
            UISprite timeIcon = new UISprite(new Sprite(UITex, ContentGlobals.TimeIconRectSmall, new Vector2(0f, .5f)));

            bool displayMoves = string.IsNullOrEmpty(inGameState.DisplayBestMoves) == false;
            bool displayTime = string.IsNullOrEmpty(inGameState.DisplayBestTime) == false;

            if (displayMoves == true)
            {
                LevelInfoContainer.AddElementRelative(movesIcon, new Vector2(containerOrigin.X - 32f, 68f));
            }

            if (displayTime == true)
            {
                LevelInfoContainer.AddElementRelative(timeIcon, new Vector2(containerOrigin.X - 22f, 89f));
            }
            
            if (string.IsNullOrEmpty(inGameState.DisplayLevelNum) == false)
            {
                LevelInfoContainer.AddElementRelative(UIHelpers.CreateCenterUIText(DescriptionFont, inGameState.DisplayLevelNum,
                    Vector2.One, Color.Black), new Vector2(containerOrigin.X, 23f));
            }

            if (string.IsNullOrEmpty(inGameState.DisplayLevelName) == false)
            {
                LevelInfoContainer.AddElementRelative(UIHelpers.CreateCenterUIText(StatsFont, inGameState.DisplayLevelName,
                    Vector2.One, Color.Blue), new Vector2(containerOrigin.X, 48f));
            }

            if (displayMoves == true)
            {
                LevelInfoContainer.AddElementRelative(UIHelpers.CreateUIText(StatsFont, inGameState.DisplayBestMoves, new Vector2(0f, .5f),
                    Vector2.One, Color.Black), new Vector2(containerOrigin.X, 73f));
            }

            if (displayTime == true)
            {
                LevelInfoContainer.AddElementRelative(UIHelpers.CreateUIText(StatsFont, inGameState.DisplayBestTime, new Vector2(0f, .5f),
                    Vector2.One, Color.Black), new Vector2(containerOrigin.X, 95f));
            }
        }

        public void Render()
        {
            //Draw the level
            inGameState.RenderLevel();

            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

            RenderingManager.Instance.DrawSprite(UITex, Vector2.Zero, ContentGlobals.BoxRect, FadeColor * (float)(ElapsedTime / FadeTime), 0f, Vector2.Zero,
                RenderingGlobals.BaseResolution, SpriteEffects.None, .3f);

            //Show level information
            if (LevelInfoContainer != null && LevelInfoContainer.Visible == true)
            {
                LevelInfoContainer.Render();
            }

            //Draw all the menu options
            base.RenderMenu();

            RenderingManager.Instance.EndCurrentBatch();
        }

        private void Continue()
        {
            inGameState.ChangeLevelState(new PlayingState(inGameState));
        }

        private void Restart()
        {
            inGameState.RestartLevel();
        }

        private void Options()
        {
            if (OptionMenu == null)
            {
                OptionMenu = new OptionsMenuNew(this, RenderingGlobals.BaseResolutionHalved + new Vector2(RenderingGlobals.BaseResolutionWidth, -150f),
                    new Vector2(0f, 35f), AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont15px));
                OptionMenu.SetBackoutOption(MenuBackOut);
                UIHelpers.SetDefaultMenuSounds(OptionMenu, true, true);
            }

            OptionMenu.ChangeSelection(-OptionMenu.CurSelection, false);
            OptionMenu.ResetSelectionStates();

            if (LevelInfoContainer != null)
            {
                LevelInfoContainer.Visible = false;
            }

            PushWithTransition(OptionMenu, MenuTransition.CreateTransitionDir(InitMenu, OptionMenu, Direction.Right, 200d));
        }

        private void Controls()
        {
            if (ControlScreen == null)
            {
                ControlScreen = new ControlSelectMenu(RenderingGlobals.BaseResolutionHalved + new Vector2(RenderingGlobals.BaseResolutionWidth, 0f), this);
                ControlScreen.SetBackoutOption(MenuBackOut);
                UIHelpers.SetDefaultMenuSounds(ControlScreen, true, true);
            }

            ControlScreen.ChangeSelection(-ControlScreen.CurSelection, false);
            ControlScreen.ResetSelectionStates();

            if (LevelInfoContainer != null)
            {
                LevelInfoContainer.Visible = false;
            }

            PushWithTransition(ControlScreen, MenuTransition.CreateTransitionDir(InitMenu, ControlScreen, Direction.Right, 200d));

            //if (ControlsScreen == null)
            //{
            //    ControlsScreen = new ControlsMenu(new Vector2(RenderingGlobals.BaseResolutionWidth, 0f), true);
            //    ControlsScreen.SetBackoutOption(MenuBackOut);
            //    ControlsScreen.SetSounds(null, AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), null, AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionBackSound),
            //        0f, 1f, 0f, .8f);
            //}
            //
            //if (LevelInfoContainer != null)
            //{
            //    LevelInfoContainer.Visible = false;
            //}
            //
            //PushWithTransition(ControlsScreen, MenuTransition.CreateTransitionDir(InitMenu, ControlsScreen, Direction.Right, 200d));
        }

        private void OpenInfoScreen()
        {
            if (PuzzleInfoMenu == null)
            {
                PuzzleInfoMenu = new PuzzleInfoMenu(RenderingGlobals.BaseResolutionHalved + new Vector2(RenderingGlobals.BaseResolutionWidth, -43f),
                    new Vector2(6f, 6f), Font, AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont10px));

                PuzzleInfoMenu.SetBackoutOption(MenuBackOut);
                UIHelpers.SetDefaultMenuSounds(PuzzleInfoMenu, false, true);
            }

            PuzzleInfoMenu.ResetSelectionStates();

            PushWithTransition(PuzzleInfoMenu, MenuTransition.CreateTransitionDir(InitMenu, PuzzleInfoMenu, Direction.Right, 200d));

            if (LevelInfoContainer != null)
            {
                LevelInfoContainer.Visible = false;
            }
        }

        private void LevelExit()
        {
            //Unload some types of assets when exiting the level
            SoundManager.Instance.ClearMusicCache();

            AssetManager.Instance.UnloadMusic();
            AssetManager.Instance.UnloadFonts();

            GameStateManager.Instance.ChangeGameState(new OverworldState(DataHandler.saveData.LastPlayedWorld));
        }

        private void MenuExit()
        {
            //Unload all content when exiting back to the title screen
            SoundManager.Instance.ClearAllSounds();
            SoundManager.Instance.ClearMusicCache();

            AssetManager.Instance.UnloadLoadedContent();

            //Manually play the selection sound since it just got unloaded
            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false);

            GameStateManager.Instance.ChangeGameState(new TitleState(TitleState.States.Menu, false));
        }

        private void MenuBackOut()
        {
            if (MenuStack.Count > 1)
            {
                UIInputMenu prevMenu = MenuStack.Pop();
                UIInputMenu curMenu = MenuStack.Peek();

                MenuTransition transition = MenuTransition.CreateTransitionDir(prevMenu, curMenu, Direction.Left, 200d, OnTransitionEnd);

                SetTransition(transition);
            }
            else
            {
                Continue();
            }
        }
    }
}