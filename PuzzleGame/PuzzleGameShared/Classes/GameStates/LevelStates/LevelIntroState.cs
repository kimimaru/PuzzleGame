/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace PuzzleGame
{
    public class LevelIntroState : IGameState
    {
        private enum Transitions
        {
            FadeIn, Rotate, Wait
        }

        private IngameState inGameState = null;

        private Texture2D Box = null;
        private readonly Rectangle ArrowRect = new Rectangle(34, 1, 13, 15);

        private const double FadeTime = 500d;

        //private double ElapsedArrowTime = 0d;
        private double ElapsedTime = 0d;

        private Vector2 PlayerStartPos = Vector2.Zero;

        private Vector2 PlayerStartScale = Vector2.Zero;
        private Vector2 PlayerEndScale = Vector2.Zero;

        private const double WaitTime = 300d;
        private const double RotateTime = 800d;
        private const double CharRotateTime = 50d;

        private double CurRotationTime = CharRotateTime;

        private Transitions CurTransition = Transitions.FadeIn;

        private float BoxColorAlpha = 0f;
        private Vector2 ArrowStartPos = Vector2.Zero;
        private const double ArrowMoveTime = 300d;
        private const float ArrowYOffset = 8f;

        private readonly Direction[] ClockwiseDirections = new Direction[4] { Direction.Down, Direction.Left, Direction.Up, Direction.Right };

        private Direction CurDirection = Direction.Down;

        private ParticleEngine BurrowingParticles = null;

        /// <summary>
        /// A single frame used to wait out a longer update loop in case the level takes longer than a frame to load.
        /// This allows the level intro to play smoothly regardless of how long the load takes.
        /// </summary>
        private bool FrameWait = false;

        public LevelIntroState(IngameState ingameState, in Vector2 playerStartPos)
        {
            inGameState = ingameState;
            PlayerStartPos = playerStartPos;
        }

        public void Enter()
        {
            Box = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);

            PlayerEndScale = inGameState.Context.Player.transform.Scale;

            inGameState.Context.Player.transform.Scale = PlayerStartScale;
            inGameState.Context.Player.transform.Position = PlayerStartPos;
            inGameState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Success);

            CurDirection = ClockwiseDirections[0];

            inGameState.Context.Player.FacingDir = CurDirection;

            ElapsedTime = 0d;
            ArrowStartPos = inGameState.camera.WorldToScreenSpace(PlayerStartPos + new Vector2(0f, -inGameState.Context.LevelSpace.TileSize.Y - (inGameState.Context.LevelSpace.TileSizeHalf.Y / 2f)));

            BoxColorAlpha = 1f;

            InitializeParticleEngine();
        }

        public void Exit()
        {
            inGameState.Context.Player.transform.Scale = PlayerEndScale;
            BurrowingParticles.Stop(true);
        }

        private void InitializeParticleEngine()
        {
            Sprite particleSprite = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.LevelObjectsTex), ContentGlobals.BlockParticleRect);
            BurrowingParticles = new ParticleEngine(25, PlayerStartPos, particleSprite, LevelGlobals.BasePlayerRenderDepth - .01f, 300d, 800d);
            BurrowingParticles.EmissionRate = 16d;
            BurrowingParticles.Position = PlayerStartPos;
            BurrowingParticles.MinAngularVelocity = UtilityGlobals.ToRadians(0f);
            BurrowingParticles.MaxAngularVelocity = UtilityGlobals.ToRadians(0f);
            BurrowingParticles.MinPositionOffset = new Vector2(-6f, -6f);
            BurrowingParticles.MaxPositionOffset = new Vector2(6f, 6f);
            BurrowingParticles.MinVelocity = new Vector2(-.1f * 60f, -.1f * 60f);
            BurrowingParticles.MaxVelocity = new Vector2(.1f * 60f, .1f * 60f);
            BurrowingParticles.MinParticleLife = 101d;
            BurrowingParticles.MaxParticleLife = 167d;
            BurrowingParticles.MinInitScale = new Vector2(2f, 1f);
            BurrowingParticles.MaxInitScale = new Vector2(3f, 2f);
            BurrowingParticles.MinAcceleration = new Vector2(-.02f * 60f, -.02f * 60f);
            BurrowingParticles.MaxAcceleration = new Vector2(.02f * 60f, .02f * 60f);

            SurfaceTypes surface = inGameState.Context.SurfaceTiles.GetTile(inGameState.Context.Player.SurfaceCheckPos);

            BurrowingParticles.ParticleColor = Block.GetParticleColorForSurface(surface);
            BurrowingParticles.Resume();
        }

        public void Update()
        {
            //Wait out the frame
            if (FrameWait == false)
            {
                FrameWait = true;

                //Load the level's music track here - prevents issues when unloading music and sounds from prior states
                SoundEffect musicTrack = null;
                if (string.IsNullOrEmpty(inGameState.LevelMap.MusicName) == false)
                {
                    try
                    {
                        musicTrack = AssetManager.Instance.LoadMusic(inGameState.LevelMap.MusicName);
                    }
                    catch (Microsoft.Xna.Framework.Content.ContentLoadException)
                    {
                        musicTrack = null;
                    }
                }

                //If the music was found, play it
                if (musicTrack != null)
                {
                    SoundManager.Instance.PlayMusic(AssetManager.Instance.LoadMusic(inGameState.LevelMap.MusicName), true, true);
                }
                //Otherwise, don't play any music
                else
                {
                    SoundManager.Instance.StopAndClearMusicTrack();
                }

                return;
            }

            //ElapsedArrowTime += Time.ElapsedTime.TotalMilliseconds;

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            inGameState.Context.Player.AnimationManager.CurrentAnim.Update();
            BurrowingParticles.Update();

            if (CurTransition == Transitions.FadeIn)
            {
                HandleFade();
            }
            else if (CurTransition == Transitions.Rotate)
            {
                HandleRotate();
            }
            else
            {
                HandleWait();
            }

            inGameState.Context.Player.AnimationManager.CurrentAnim.Update();
        }

        private void HandleFade()
        {
            if (ElapsedTime >= FadeTime)
            {
                ElapsedTime = 0d;
                BoxColorAlpha = 0f;
                CurTransition = Transitions.Rotate;
            }
            else
            {
                BoxColorAlpha = Interpolation.Interpolate(1f, 0f, ElapsedTime / FadeTime, Interpolation.InterpolationTypes.Linear);
            }
        }

        private void HandleRotate()
        {
            if (ElapsedTime >= RotateTime)
            {
                ElapsedTime = 0d;
                CurTransition = Transitions.Wait;

                CurDirection = ClockwiseDirections[0];
                inGameState.Context.Player.FacingDir = CurDirection;

                inGameState.Context.Player.transform.Scale = PlayerEndScale;

                BurrowingParticles.Stop(false);
            }
            else
            {
                inGameState.Context.Player.transform.Scale = Interpolation.Interpolate(PlayerStartScale, PlayerEndScale,
                    ElapsedTime / RotateTime, Interpolation.InterpolationTypes.CubicOut);

                int fullRotations = (int)(ElapsedTime / (RotateTime / 2));
                CurRotationTime = CharRotateTime * (fullRotations + 1);

                CurDirection = ClockwiseDirections[(int)(ElapsedTime / CurRotationTime) % ClockwiseDirections.Length];
                inGameState.Context.Player.FacingDir = CurDirection;
            }
        }

        private void HandleWait()
        {
            if (ElapsedTime >= WaitTime)
            {
                inGameState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.Idle);
                inGameState.ChangeLevelState(new PlayingState(inGameState));
            }
        }

        public void Render()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();
            BurrowingParticles.Render();

            RenderingManager.Instance.EndCurrentBatch();

            //Draw a black overlay over everything else
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

            if (inGameState.LevelMap.ShowHUD == true)
                inGameState.gameHUD.Render();

            //double arrowTime = UtilityGlobals.PingPong(ElapsedArrowTime / ArrowMoveTime, 0d, 1d);
            //float arrowYOffset = (float)(arrowTime * ArrowYOffset);

            //Draw the arrow
            //RenderingManager.Instance.DrawSprite(Box, ArrowStartPos + new Vector2(0f, arrowYOffset), ArrowRect, Color.White, UtilityGlobals.ToRadians(180f),
            //    ArrowRect.GetCenterOrigin(), Vector2.One * 2f, SpriteEffects.None, .8f);

            //Draw the overlay if the alpha is 0
            if (BoxColorAlpha > 0f)
            {
                RenderingManager.Instance.DrawSprite(Box, Vector2.Zero, ContentGlobals.BoxRect, Color.Black * BoxColorAlpha, 0f, Vector2.Zero,
                    RenderingGlobals.BaseResolution, SpriteEffects.None, 1f);
            }

            RenderingManager.Instance.EndCurrentBatch();
        }
    }
}