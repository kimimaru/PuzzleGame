﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public class TestUIState : IGameState
    {
        private UIInputMenu Container = null;
        private SpriteFont Font = null;

        private Vector2 StartPos = Vector2.Zero;
        private Vector2 EndPos = Vector2.Zero;

        public TestUIState()
        {
            
        }

        private void OnOptionSelected(in int optionIndex)
        {
            if (Container.Elements[Container.CurSelection] is UIText text)
            {
                text.TextOptions = (UITextOptions)EnumUtility.AddEnumVal((long)text.TextOptions, (long)UITextOptions.Outline);
            }

            Container.Elements[Container.CurSelection].TintColor = Color.DarkGreen;
        }

        private void OnOptionDeselected(in int optionIndex)
        {
            if (Container.Elements[optionIndex] is UIText text)
            {
                text.TextOptions = (UITextOptions)EnumUtility.RemoveEnumVal((long)text.TextOptions, (long)UITextOptions.Outline);
            }

            Container.Elements[optionIndex].TintColor = Color.White * .8f;
        }

        private void Option1Chosen()
        {
            Debug.Log("Test!!");
        }

        private void Option2Changed(in int amount)
        {
            UIRenderable element = Container.Elements[Container.Elements.Count - 1];
            element.Position = new Vector2(Container.Position.X + (75 * amount), element.Position.Y);
        }

        public void Enter()
        {
            Font = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont24px);

            Container = new UIInputMenu(5, RenderingGlobals.BaseResolutionHalved);
            Container.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Test", new Vector2(.5f, .5f), new Vector2(.8f, .8f),
                UITextOptions.Outline, new UITextData(Color.Black, 2f, -.01f, new Vector2(3f, 3f), Color.Green, -.01f)),
                new Vector2(0f, 0f), new UIMenuOption(OnOptionSelected, OnOptionDeselected, Option1Chosen, null));
            Container.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Test", new Vector2(.5f, .5f), new Vector2(.8f, .8f),
                UITextOptions.Outline, new UITextData(Color.Black, 2f, -.01f, new Vector2(3f, 3f), Color.Green, -.01f)),
                new Vector2(0f, 32f), new UIMenuOption(OnOptionSelected, OnOptionDeselected, null, Option2Changed));
            Container.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Test2", new Vector2(.5f, .5f), new Vector2(.8f, .8f),
                UITextOptions.Outline, new UITextData(Color.Black, 2f, -.01f, new Vector2(3f, 3f), Color.Green, -.01f)),
                new Vector2(0f, 64f), new UIMenuOption(OnOptionSelected, OnOptionDeselected, null, null));
            Container.AddMenuElementRelative(UIHelpers.CreateUISprite(AssetManager.Instance.LoadTexture(ContentGlobals.UITex),
                new Rectangle(65, 30, 4, 14), new Vector2(.5f, 1f), new Vector2(3f, 3f), Color.White), new Vector2(75f, 0f),
                new UIMenuOption(OnOptionSelected, OnOptionDeselected, null, null));

            StartPos = EndPos = Container.Position;
        }

        public void Exit()
        {
            
        }

        public void Update()
        {
            Container.Update();
            //ElapsedTime = UtilityGlobals.Clamp(ElapsedTime + Time.ElapsedTime.TotalMilliseconds, 0d, InterpolateTime);

            //if (Input.GetButtonDown(InputActions.Left) == true)
            //{
            //    ElapsedTime = 0d;
            //    StartPos = Container.Position;
            //    EndPos = new Vector2(100f, StartPos.Y);
            //}
            //else if (Input.GetButtonDown(InputActions.Right) == true)
            //{
            //    ElapsedTime = 0d;
            //    StartPos = Container.Position;
            //    EndPos = new Vector2(600f, StartPos.Y);
            //}

            //Container.Position = Interpolation.Interpolate(StartPos, EndPos, ElapsedTime / InterpolateTime, Interpolation.InterpolationTypes.QuadInOut);
        }

        public void Render()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

            Container.Render();

            RenderingManager.Instance.EndCurrentBatch();
        }
    }
}
