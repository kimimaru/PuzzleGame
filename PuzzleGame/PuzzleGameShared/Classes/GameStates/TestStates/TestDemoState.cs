﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public class TestDemoState : IGameState
    {
        private Camera2D Camera = null;

        private List<DemonstrationBase> Demos = new List<DemonstrationBase>();

        public TestDemoState()
        {
            
        }

        public void Enter()
        {
            Camera = new Camera2D();
            Camera.SetBounds(new Rectangle(0, 0, RenderingGlobals.BaseResolutionWidth, RenderingGlobals.BaseResolutionHeight));
            Camera.DefaultPosition = RenderingGlobals.BaseResolutionHalved;
            Camera.LookAt(Camera.DefaultPosition);

            Demos.Add(new WalkDemonstration(new Vector2(400f, 64)));
            Demos.Add(new GrabDemonstration(new Vector2(384, 128)));
            Demos.Add(new PushPullDemonstration(new Vector2(384, 192)));
            Demos.Add(new UndoDemonstration(new Vector2(384, 256), BlockPatterns.None, null));
            Demos.Add(new SpotDemonstration(new Vector2(384, 320)));
            Demos.Add(new SwitchDemonstration(new Vector2(352, 384)));
            Demos.Add(new PipeDemonstration(new Vector2(352, 448)));

            //This acts as the direction demonstration
            Demos.Add(new UndoDemonstration(new Vector2(384, 512), BlockPatterns.None, Direction.Right));
            Demos.Add(new WarpDemonstration(new Vector2(384, 576)));
            Demos.Add(new IceDemonstration(new Vector2(352, 640)));
            Demos.Add(new MudDemonstration(new Vector2(384, 704)));
            Demos.Add(new PairedDemonstration(new Vector2(352, 768)));
        }

        public void Exit()
        {
            Camera.CleanUp();

            for (int i = 0; i < Demos.Count; i++)
            {
                Demos[i].CleanUp();
            }
        }

        public void Update()
        {
            if (Input.GetButton(InputActions.Up) == true)
            {
                Camera.Translate(new Vector2(0f, -4f));
            }
            else if (Input.GetButton(InputActions.Down) == true)
            {
                Camera.Translate(new Vector2(0f, 4f));
            }
            
            for (int i = 0; i < Demos.Count; i++)
            {
                Demos[i].Update();
            }
        }

        public void Render()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, Camera.TransformMatrix);

            for (int i = 0; i < Demos.Count; i++)
            {
                Demos[i].Render();
            }

            RenderingManager.Instance.EndCurrentBatch();
        }
    }
}
