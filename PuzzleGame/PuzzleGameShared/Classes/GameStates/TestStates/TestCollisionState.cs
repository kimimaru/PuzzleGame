﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public class TestCollisionState : IGameState
    {
        private Transform trans1 = null;
        private Sprite sprite1 = null;

        private Transform trans2 = null;
        private Sprite sprite2 = null;

        private Transform UsedTrans = null;
        private RectangleF DrawnColl1;
        private RectangleF DrawnColl2;

        public RectangleF CollisionRect1
        {
            get
            {
                return new RectangleF(trans1.Position.X - (trans1.Scale.X / 2),
                                      trans1.Position.Y - (trans1.Scale.Y / 2),
                                      trans1.Scale.X, trans1.Scale.Y);
            }
        }

        public RectangleF CollisionRect2
        {
            get
            {
                return new RectangleF(trans2.Position.X - (trans2.Scale.X / 2),
                                      trans2.Position.Y,
                                      trans2.Scale.X, trans2.Scale.Y / 2);
            }
        }

        public RectangleF CollisionRect3
        {
            get
            {
                return new RectangleF(trans2.Position.X - 9,
                                      trans2.Position.Y + 15,
                                      trans2.Scale.X + 8, trans2.Scale.Y - 8);
            }
        }

        public RectangleF CollisionRect4
        {
            get
            {
                const int size = 32;
                return new RectangleF(trans2.Position.X - (size / 2), trans2.Position.Y - (size / 2), size, size);
            }
        }

        public TestCollisionState()
        {
            
        }

        public void Enter()
        {
            trans1 = new Transform(new Vector2(100f, 100f), 0f, new Vector2(32));
            trans2 = new Transform(new Vector2(200f, 100f), 0f, new Vector2(32));
            sprite1 = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.UITex), ContentGlobals.BoxRect);
            sprite2 = new Sprite(sprite1.Tex, sprite1.SourceRect, new Vector2(0f, 1f));

            UsedTrans = trans1;
        }

        public void Exit()
        {
            
        }

        public void Update()
        {
            if (KeyboardInput.GetKeyDown(Keys.R))
            {
                trans1.Position = new Vector2(100, 100);
                trans2.Position = new Vector2(200, 100);
            }

            if (KeyboardInput.GetKeyDown(Keys.Space))
            {
                UsedTrans = (UsedTrans == trans1) ? trans2 : trans1;
            }

            Transform AltTrans = trans2;

            RectangleF coll1 = CollisionRect1;
            RectangleF coll2 = CollisionRect2;

            if (UsedTrans == trans2)
            {
                UtilityGlobals.Swap(ref coll1, ref coll2);
                AltTrans = trans1;
            }

            if (KeyboardInput.GetKeyDown(Keys.Up))
            {
                UsedTrans.Position = UtilityGlobals.ClampRectToOther(Direction.Up, UsedTrans.Position, coll1, coll2);
                UsedTrans.Position = new Vector2(coll2.Center.X, UsedTrans.Position.Y);
            }
            else if (KeyboardInput.GetKeyDown(Keys.Down))
            {
                UsedTrans.Position = UtilityGlobals.ClampRectToOther(Direction.Down, UsedTrans.Position, coll1, coll2);
                UsedTrans.Position = new Vector2(coll2.Center.X, UsedTrans.Position.Y);
            }
            else if (KeyboardInput.GetKeyDown(Keys.Left))
            {
                UsedTrans.Position = UtilityGlobals.ClampRectToOther(Direction.Left, UsedTrans.Position, coll1, coll2);
                UsedTrans.Position = new Vector2(UsedTrans.Position.X, coll2.Center.Y);
            }
            else if (KeyboardInput.GetKeyDown(Keys.Right))
            {
                UsedTrans.Position = UtilityGlobals.ClampRectToOther(Direction.Right, UsedTrans.Position, coll1, coll2);
                UsedTrans.Position = new Vector2(UsedTrans.Position.X, coll2.Center.Y);
            }

            DrawnColl1 = coll1;
            DrawnColl2 = coll2;
        }

        private Vector2 BrushColliderToOther(in Direction brushDir, in Vector2 posToBrush, in RectangleF rectToBrush, in RectangleF brushedColl)
        {
            if (brushDir == Direction.Up)
            {
                float diff = (float)Math.Round(rectToBrush.Center.Y - posToBrush.Y, 2);

                Vector2 val = new Vector2(posToBrush.X, brushedColl.Bottom);
                val.Y += (rectToBrush.Height / 2) - diff;

                return val;
            }
            else if (brushDir == Direction.Down)
            {
                float diff = (float)Math.Round(rectToBrush.Center.Y - posToBrush.Y, 2);

                Vector2 val = new Vector2(posToBrush.X, brushedColl.Top);
                val.Y -= (rectToBrush.Height / 2) + diff;

                return val;
            }
            else if (brushDir == Direction.Left)
            {
                float diff = (float)Math.Round(rectToBrush.Center.X - posToBrush.X, 2);

                Vector2 val = new Vector2(brushedColl.Right, posToBrush.Y);
                val.X += (rectToBrush.Width / 2) - diff;

                return val;
            }
            else if (brushDir == Direction.Right)
            {
                float diff = (float)Math.Round(rectToBrush.Center.X - posToBrush.X, 2);

                Vector2 val = new Vector2(brushedColl.Left, posToBrush.Y);
                val.X -= (rectToBrush.Width / 2) + diff;

                return val;
            }

            return Vector2.Zero;
        }

        public void Render()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

            RenderingManager.Instance.DrawSprite(sprite1.Tex, trans1.Position, null, Color.LightBlue, 0f, sprite1.Pivot, trans1.Scale,
                SpriteEffects.None, .1f);
            RenderingManager.Instance.DrawSprite(sprite2.Tex, trans2.Position, null, Color.LightGreen, 0f, sprite2.Pivot, trans2.Scale,
                SpriteEffects.None, .1f);

            RenderingManager.Instance.spriteBatch.DrawHollowRect(sprite1.Tex, DrawnColl1, Color.Red, .2f, 1);
            RenderingManager.Instance.spriteBatch.DrawHollowRect(sprite1.Tex, DrawnColl2, Color.Red, .2f, 1);

            RenderingManager.Instance.EndCurrentBatch();
        }
    }
}
