/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace PuzzleGame
{
    /// <summary>
    /// The bonus unlock screen.
    /// </summary>
    public class BonusUnlockState : IGameState
    {
        private enum States
        {
            FadeIn, InputWait, Input, FadeOut, Wait
        }

        private Texture2D UITex = null;
        private SpriteFont Font = null;

        private Camera2D camera = null;
        private PlayableContext Context = null;
        private Echidna Player = null;
        private Portal[] Portals = null;

        //private readonly Vector2 PlayerPos = new Vector2(RenderingGlobals.BaseResWidthHalved, RenderingGlobals.BaseResHeightHalved + 50);
        private readonly Color BGColor = new Color(59, 169, 169, 255);
        private const double PortalDist = 100d;

        private readonly Color FadeOpaqueColor = Color.Black;
        private readonly Color FadeTransparentColor = Color.Black * 0f;

        private const string UnlockText = "YOU'VE UNLOCKED BONUS LEVELS!";
        private UIText UnlockTextObj = null;
        private readonly Color UnlockTextColor = Color.White;
        private readonly Vector2 UnlockTextPos = new Vector2(RenderingGlobals.BaseResWidthHalved, 60f);

        private const double FadeInTime = 1200d;
        private const double InputAcceptTime = 1000d;
        private const double FadeOutTime = 1200d;
        private const double WaitTime = 700d;

        private float OrigMusicVolume = .5f;

        private LevelMap LvlMap = null;
        private RenderTarget2D MapRenderTarget = null;
        private bool RenderTargetRendered = false;

        private States CurState = States.FadeIn;
        private Color CurFadeColor = Color.Black;
        private double ElapsedTime = 0d;

        public BonusUnlockState()
        {
            
        }

        public void Enter()
        {
            //Play the credits music
            SoundManager.Instance.PlayMusic(AssetManager.Instance.LoadMusic(ContentGlobals.CreditsMusic), true, false);

            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            Font = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont18px);

            UnlockTextObj = UIHelpers.CreateCenterUIText(Font, UnlockText, Vector2.One, UnlockTextColor, UITextOptions.Outline, UITextData.Standard);
            UnlockTextObj.Position = UnlockTextPos;

            LvlMap = new LevelMap(AssetManager.Instance.LoadTiledMap(ContentGlobals.BonusUnlockLevel));
            
            //Initialize map render target
            MapRenderTarget?.Dispose();
            MapRenderTarget = new RenderTarget2D(RenderingManager.Instance.graphicsDevice, LvlMap.Map.WidthInPixels, LvlMap.Map.HeightInPixels);

            if (camera == null)
            {
                camera = new Camera2D();
            }
            camera.SetBounds(new Rectangle(0, 0, (int)RenderingGlobals.BaseResolution.X, (int)RenderingGlobals.BaseResolution.Y));

            //If there's a camera focus tile, use that position; otherwise, use the player's position
            Vector2 cameraStartPos = LvlMap.PlayerStartTile.Position;
            if (LvlMap.CameraFocusTile != null)
            {
                cameraStartPos = LvlMap.CameraFocusTile.Position;
            }

            //Set the initial camera position to the player's position, then clamp it
            camera.DefaultPosition = Camera2D.ClampCamera(cameraStartPos + LvlMap.TileSizeHalf,
                camera.ScreenBounds, new RectangleF(0f, 0f, LvlMap.Map.WidthInPixels, LvlMap.Map.HeightInPixels));
            camera.LookAt(camera.DefaultPosition);

            Context = new PlayableContext(new PlayableSpace(Vector2.Zero, RenderingGlobals.BaseResolutionWidth, RenderingGlobals.BaseResolutionHeight));

            Player = new Echidna(new BlankState(false));
            Player.Initialize(Context, (ObjectInitInfo)LvlMap.PlayerStartTile);
            Player.FacingDir = Direction.Down;
            Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Success);
            Player.transform.Position += new Vector2(16, 16);

            InitPortals();

            CurFadeColor = FadeOpaqueColor;

            OrigMusicVolume = SoundManager.Instance.MusicVolume;
        }

        public void Exit()
        {
            SoundManager.Instance.StopAndClearMusicTrack();
            SoundManager.Instance.MusicVolume = OrigMusicVolume;

            camera.CleanUp();

            Context.ClearAllObjectsAndCamera();

            Player.CleanUp();
            for (int i = 0; i < Portals.Length; i++)
            {
                Portals[i].CleanUp();
            }

            UITex = null;
            Font = null;

            UnlockTextObj = null;

            if (MapRenderTarget != null)
            {
                MapRenderTarget.Dispose();
                MapRenderTarget = null;
            }

            RenderTargetRendered = false;
        }

        public void Update()
        {
            for (int i = 0; i < Portals.Length; i++)
            {
                Portals[i].Update();
            }

            if (CurState == States.FadeIn)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
                if (ElapsedTime >= FadeInTime)
                {
                    ElapsedTime = 0d;
                    CurFadeColor = FadeTransparentColor;
                    CurState = States.InputWait;
                }
                else
                {
                    CurFadeColor = Interpolation.Interpolate(FadeOpaqueColor, FadeTransparentColor, ElapsedTime / FadeInTime, Interpolation.InterpolationTypes.Linear);
                }
                return;
            }

            if (CurState == States.InputWait)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
                if (ElapsedTime >= InputAcceptTime)
                {
                    ElapsedTime = 0d;
                    CurState = States.Input;
                }
                return;
            }

            if (CurState == States.Input)
            {
                if (Input.GetButtonDown(InputActions.Select) == true || Input.GetButtonDown(InputActions.Pause) == true)
                {
                    DataHandler.saveData.Misc.ShowBonusUnlock = false;
                    ElapsedTime = 0d;
                    CurState = States.FadeOut;
                    CurFadeColor = FadeTransparentColor;
                }
                return;
            }

            if (CurState == States.FadeOut)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
                if (ElapsedTime >= FadeOutTime)
                {
                    ElapsedTime = 0d;
                    CurFadeColor = FadeOpaqueColor;
                    CurState = States.Wait;

                    SoundManager.Instance.StopAndClearMusicTrack();
                    SoundManager.Instance.MusicVolume = OrigMusicVolume;
                }
                else
                {
                    double time = ElapsedTime / FadeOutTime;

                    SoundManager.Instance.MusicVolume = Interpolation.Interpolate(OrigMusicVolume, 0f, time, Interpolation.InterpolationTypes.Linear);
                    CurFadeColor = Interpolation.Interpolate(FadeTransparentColor, FadeOpaqueColor, time, Interpolation.InterpolationTypes.Linear);
                }

                return;
            }

            if (CurState == States.Wait)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
                if (ElapsedTime >= WaitTime)
                {
                    ElapsedTime = 0d;

                    //Unload all content
                    SoundManager.Instance.ClearAllSounds();
                    SoundManager.Instance.ClearMusicCache();

                    AssetManager.Instance.UnloadLoadedContent();

                    //Go to the title screen
                    GameStateManager.Instance.ChangeGameState(new TitleState(TitleState.States.Title, false));
                }

                return;
            }
        }

        private void InitPortals()
        {
            //Arrange the portals like a heptagon
            Vector2[] points = UtilityGlobals.GetPointsForRegularPolygon(7, Player.transform.Position, PortalDist, Math.PI * 1.5d);

            //Truncate to integers
            for (int i = 0; i < points.Length; i++)
            {
                points[i].X = (int)(points[i].X);
                points[i].Y = (int)(points[i].Y);
            }

            //Initialize portals
            Portals = new Portal[7];

            ObjectInitInfo objInitInfo = default(ObjectInitInfo);
            objInitInfo.Properties = new Dictionary<string, string>(0);

            for (int i = 0; i < Portals.Length; i++)
            {
                Portals[i] = new Portal(points[i]);

                objInitInfo.Position = points[i];
                Portals[i].Initialize(Context, objInitInfo);
                Portals[i].Activated = true;
            }
        }

        private void CheckSetMapRT()
        {
            //If we haven't rendered the map to a RenderTarget yet, do so now
            //This lets us render the map in one draw call
            if (RenderTargetRendered == false)
            {
                RenderingManager.Instance.graphicsDevice.SetRenderTarget(MapRenderTarget);

                RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                    SamplerState.PointClamp, null, null, null, null);

                LvlMap.DrawMap();

                RenderingManager.Instance.EndCurrentBatch();

                //Set the RenderTarget back
                RenderingManager.Instance.graphicsDevice.SetRenderTarget(RenderingManager.Instance.GetMainRenderTarget);
                RenderingManager.Instance.graphicsDevice.Clear(RenderingManager.Instance.ClearColor);

                RenderTargetRendered = true;
            }
        }

        public void Render()
        {
            CheckSetMapRT();

            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, camera.TransformMatrix);

            if (RenderTargetRendered == true)
            {
                RenderingManager.Instance.DrawSprite(MapRenderTarget, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, Vector2.One,
                    SpriteEffects.None, 0f);
            }
            else
            {
                RenderBox(BGColor);
            }

            //UnlockTextObj.Render();

            Player.Render();
            for (int i = 0; i < Portals.Length; i++)
            {
                Portals[i].Render();
            }

            //if (CurState == States.FadeIn || CurState == States.FadeOut || CurState == States.Wait)
            //{
            //    RenderBox(CurFadeColor);
            //}

            RenderingManager.Instance.EndCurrentBatch();

            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

            UnlockTextObj.Render();

            if (CurState == States.FadeIn || CurState == States.FadeOut || CurState == States.Wait)
            {
                RenderBox(CurFadeColor);
            }

            RenderingManager.Instance.EndCurrentBatch();
        }

        private void RenderBox(in Color color)
        {
            RenderingManager.Instance.DrawSprite(UITex, Vector2.Zero, ContentGlobals.BoxRect, color, 0f, Vector2.Zero,
                RenderingGlobals.BaseResolution, SpriteEffects.None, .2f);
        }
    }
}