/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The title screen.
    /// </summary>
    public class TitleState : UIMenuManager, IGameState
    {
        public enum States
        {
            Title, Menu, Controls, Options, Credits
        }

        private SpriteFont Font21px = null;
        private SpriteFont Font18px = null;
        private SpriteFont Font10px = null;

        private States CurState = States.Title;

        private double TitleElapsedTime = 0d;
        private const string VersionText = "v" + Engine.VersionString;
        private const string CopyrightText = "© 2020 Thomas \"Kimimaru\" Deeb";
        //private const string StartText = "Press Select/Grab";
        
        private readonly Color TitleColor = Color.White;
        private readonly Color TextColor = Color.White;

        private CreditsDisplay CreditDisplay = null;

        private Texture2D ScrollingBG = null;

        private Texture2D UITex = null;

        private UISprite TitleLogo = null;
        private const double LogoMoveTime = 1200d;
        private readonly Vector2 LogoStartPos = new Vector2(RenderingGlobals.BaseResWidthHalved, 10f);
        private readonly Vector2 LogoBobStart = new Vector2(0f, 3f);
        private readonly Vector2 LogoBobEnd = new Vector2(0f, -3f);
        private double LogoElapsedTime = 0d;

        private UIMenuOption.OptionSelected MenuOptionSelected = null;
        private UIMenuOption.OptionDeselected MenuOptionDeselected = null;

        private OptionsMenuNew OptionMenu = null;
        private ControlSelectMenu ControlScreen = null;

        private UIInputMenu TitleMenu = null;
        private UIInputMenu MainMenu = null;

        private UIText StartTextObj = null;
        private const double StartTextMoveRate = 1200d;
        private readonly Vector2 StartTextStartPos = new Vector2(0f, 5f);
        private readonly Vector2 StartTextEndPos = new Vector2(0f, -5f);

        private const double FadeTime = 500d;
        private double FadeElapsedTime = FadeTime;

        private readonly Vector2 BGScale = new Vector2(30f, 30f);
        private int BGXAmount = 1;
        private int BGYAmount = 1;
        private readonly Color BGColor = new Color(103, 148, 99, 235);
        private readonly Color OutlineColor = Color.Black;

        private readonly Color FadeOverlayColor = Color.White;

        private UIText VersionUIText = null;
        private UIText CopyrightUIText = null;

        private MenuTransition.TransitionEnd TransitionEnd = null;

        #region Title Level

        private IngameState TitleIGState = null;

        #endregion

        public TitleState(in States titleState, in bool useFade)
            : base()
        {
            Font21px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont21px);
            Font18px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont18px);
            Font10px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont10px);
            Font10px.LineSpacing = 24;

            ScrollingBG = AssetManager.Instance.LoadTexture(ContentGlobals.BGIcons);
            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);

            CurState = titleState;

            MenuOptionSelected = OnOptionSelected;
            MenuOptionDeselected = OnOptionDeselected;

            SetUpInitTitle();
            SetUpMainMenu();

            if (CurState == States.Menu || CurState == States.Credits)
            {
                EnterMainMenu();
                CurState = titleState;
            }

            if (CurState == States.Credits)
            {
                CreditDisplay = new CreditsDisplay();
                CreditDisplay.Reset();
            }

            TitleLogo = new UISprite(new Sprite(ScrollingBG, ContentGlobals.MBLogoRect, new Vector2(.5f, 0f)), Vector2.One);
            TitleLogo.Position = LogoStartPos + LogoBobStart;

            VersionUIText = UIHelpers.CreateUIText(Font10px, VersionText, new Vector2(0f, .5f), Vector2.One, Color.Black,
                UITextOptions.Outline, new UITextData(new Color(200, 200, 200, 255), 1f, -.01f));
            VersionUIText.Position = new Vector2(2, RenderingGlobals.BaseResolutionHeight - 9);

            CopyrightUIText = UIHelpers.CreateUIText(Font10px, CopyrightText, new Vector2(1f, .5f), Vector2.One, Color.Black,
                UITextOptions.Outline, new UITextData(new Color(200, 200, 200, 255), 1f, -.01f));
            CopyrightUIText.Position = new Vector2(RenderingGlobals.BaseResolutionWidth - 13, RenderingGlobals.BaseResolutionHeight - 9);

            TransitionEnd = SetStateToMenu;

            FadeElapsedTime = (useFade == true) ? FadeTime : 0d;
        }

        private void OnOptionSelected(in int optionIndex)
        {
            UIInputMenu menu = MenuStack.Peek();

            menu.Elements[optionIndex].TintColor = TextColor;
        }

        private void OnOptionDeselected(in int optionIndex)
        {
            UIInputMenu menu = MenuStack.Peek();

            menu.Elements[optionIndex].TintColor = new Color(180, 180, 180, 255);
        }

        private void SetUpInitTitle()
        {
            TitleMenu = new UIInputMenu(1, RenderingGlobals.BaseResolutionHalved);

            StartTextObj = UIHelpers.CreateCenterUIText(Font18px, string.Empty, Vector2.One, TextColor,
                UITextOptions.Outline, UITextData.Standard);

            TitleMenu.AddMenuElementRelative(StartTextObj, StartTextStartPos, new UIMenuOption(null, null, TransitionToMainMenu, null));
            TitleMenu.SetSounds(null, AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), null, null, 0f, 1f, 0f, 0f);
            MenuStack.Push(TitleMenu);
        }

        private void SetUpMainMenu()
        {
            Vector2 origin = new Vector2(0f, .5f);
            float xOffset = -61f;

            MainMenu = new UIInputMenu(4, RenderingGlobals.BaseResolutionHalved + new Vector2(RenderingGlobals.BaseResolutionWidth, -40f));

            UIText startText = UIHelpers.CreateUIText(Font21px, "Start", origin, Vector2.One, Color.White, UITextOptions.Outline, UITextData.Standard);
            MainMenu.AddMenuElementRelative(startText, new Vector2(xOffset, -12f), new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, StartGame, null));

            UIText optionsText = UIHelpers.CreateUIText(Font21px, "Options", origin, Vector2.One, Color.White, UITextOptions.Outline, UITextData.Standard);
            MainMenu.AddMenuElementRelative(optionsText, new Vector2(xOffset, 33f), new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, Options, null));

            UIText controlsText = UIHelpers.CreateUIText(Font21px, "Controls", origin, Vector2.One, Color.White, UITextOptions.Outline, UITextData.Standard);
            MainMenu.AddMenuElementRelative(controlsText, new Vector2(xOffset, 78f), new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, Controls, null));

            UIText creditsText = UIHelpers.CreateUIText(Font21px, "Credits", origin, Vector2.One, Color.White, UITextOptions.Outline, UITextData.Standard);
            MainMenu.AddMenuElementRelative(creditsText, new Vector2(xOffset, 123f), new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, Credits, null));

            UIText quitText = UIHelpers.CreateUIText(Font21px, "Quit", origin, Vector2.One, Color.White, UITextOptions.Outline, UITextData.Standard);
            MainMenu.AddMenuElementRelative(quitText, new Vector2(xOffset, 168f), new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, QuitGame, null));

            MainMenu.SetBackoutOption(MenuBackOut);
            UIHelpers.SetDefaultMenuSounds(MainMenu, false, true);
        }

        public void Enter()
        {
            //Play title screen music
            SoundManager.Instance.PlayMusic(AssetManager.Instance.LoadMusic(ContentGlobals.TitleMusic), true, false);

            //Load menu choose and select sounds to prevent stutters
            AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound);
            AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionChangeSound);

            //Calculate how many times to duplicate the background based on its scale
            //Ceiling the value to fit more in and ensure it fills the screen
            BGXAmount = (int)Math.Ceiling((RenderingGlobals.BaseResolution.X / ContentGlobals.TitleBGRect.Width) / BGScale.X);
            BGYAmount = (int)Math.Ceiling((RenderingGlobals.BaseResolution.Y / ContentGlobals.TitleBGRect.Height) / BGScale.Y);

            InitializeTitleLevel();

            TitleIGState.Enter();

            //Call this to update the input text
            OnKeyboardMappingsChanged(0);

            DataHandler.inputConfigData.KeyboardMappingsChangedEvent -= OnKeyboardMappingsChanged;
            DataHandler.inputConfigData.KeyboardMappingsChangedEvent += OnKeyboardMappingsChanged;
#if DEBUG
            Debug.AddCustomDebugCommand(ReloadTitle);
            Debug.AddCustomDebugCommand(EnterDemoEndTitleDemo);
#endif
        }

        public void Exit()
        {
            OptionMenu?.CleanUp();
            ControlScreen?.CleanUp();

            TitleIGState.Exit();

            DataHandler.inputConfigData.KeyboardMappingsChangedEvent -= OnKeyboardMappingsChanged;
#if DEBUG
            Debug.RemoveCustomDebugCommand(ReloadTitle);
            Debug.RemoveCustomDebugCommand(EnterDemoEndTitleDemo);
#endif
        }

        public void Update()
        {
            TitleIGState.Update();

            FadeElapsedTime = UtilityGlobals.Clamp(FadeElapsedTime - Time.ElapsedTime.TotalMilliseconds, 0d, FadeTime);

            if (CurState != States.Credits)
            {
                base.UpdateMenu();

                LogoElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                double logoTime = UtilityGlobals.PingPong(LogoElapsedTime / LogoMoveTime, 0d, 1d);

                TitleLogo.Position = Interpolation.Interpolate(LogoStartPos + LogoBobStart, LogoStartPos + LogoBobEnd, logoTime, Interpolation.InterpolationTypes.QuadInOut);

                if (CurState == States.Title)
                {
                    TitleElapsedTime += Time.ElapsedTime.TotalMilliseconds;
                    double time = UtilityGlobals.PingPong(TitleElapsedTime / (StartTextMoveRate / 2), 0d, 1d);
                    StartTextObj.Position = Interpolation.Interpolate(TitleMenu.Position + StartTextStartPos, TitleMenu.Position + StartTextEndPos,
                        time, Interpolation.InterpolationTypes.CubicInOut);
                }
            }
            else if (CurState == States.Credits)
            {
                //Set the default scroll speed
                //Let them scroll faster by holding down the buttons to move up and stop by holding the buttons to move down
                float scrollSpeed = CreditsDisplay.CreditsScrollSpeed;
                if (Input.GetButton(InputActions.Up) == true)
                {
                    scrollSpeed = CreditsDisplay.CreditsScrollSpeedFaster;
                }
                else if (Input.GetButton(InputActions.Down) == true)
                {
                    scrollSpeed = 0f;
                }

                CreditDisplay.ScrollSpeed = scrollSpeed;
                CreditDisplay.Update();

                if (Input.GetButtonDown(InputActions.BackOut) == true || Input.GetButtonDown(InputActions.Select) == true)
                {
                    CurState = States.Menu;
                }
            }
        }

        public void Render()
        {
            //Render the level
            TitleIGState.Render();

            //Render the UI
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

            if (CurState == States.Title || CurState == States.Menu || CurState == States.Controls || CurState == States.Options)
            {
                if (CurState != States.Controls && CurState != States.Options)
                {
                    DrawTitle();
                }

                //Render the top menu
                base.RenderMenu();

                //Draw version number
                if (VersionUIText.Visible == true)
                    VersionUIText.Render();

                //Draw copyright text
                if (CopyrightUIText.Visible == true)
                    CopyrightUIText.Render();
            }
            else if (CurState == States.Credits)
            {
                DrawCredits();
            }

            if (FadeElapsedTime > 0d)
            {
                Color overlayColor = Interpolation.Interpolate(Color.Transparent, FadeOverlayColor, FadeElapsedTime / FadeTime, Interpolation.InterpolationTypes.Linear);
                RenderingManager.Instance.DrawSprite(UITex, Vector2.Zero, ContentGlobals.BoxRect, overlayColor, 0f, Vector2.Zero, RenderingGlobals.BaseResolution, SpriteEffects.None, .3f);
            }

            RenderingManager.Instance.EndCurrentBatch();
        }

        private void StartGame()
        {
            //Check if we have any level data in the first world
            WorldData worldData = SaveData.GetWorldData(0);

            //We don't, so load the intro cutscene
            if (worldData.levelData.Count == 0)
            {
                //NOTE: This works for now, but find a more reliable way to retrieve this information
                Cutscene introScene = CutsceneLoader.LoadCutscene(CutsceneEnums.Intro);
                introScene.LevelName = ContentGlobals.IntroCutscene1Level;
                introScene.LevelDescription = "Intro";
                introScene.LevelID = -1;

                introScene.Start();
            }
            //Otherwise enter the overworld
            else
            {
                string lastWorld = $"World{DataHandler.saveData.LastPlayedWorld}";
                MonoGame.Extended.Tiled.TiledMap overworldMap = AssetManager.Instance.LoadTiledMap(lastWorld);

                GameStateManager.Instance.ChangeGameState(new OverworldState(overworldMap));
            }
        }

        private void Options()
        {
            if (OptionMenu == null)
            {
                OptionMenu = new OptionsMenuNew(this, RenderingGlobals.BaseResolutionHalved + new Vector2(RenderingGlobals.BaseResolutionWidth, -150f),
                    new Vector2(0f, 35f), AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont15px));
                OptionMenu.SetBackoutOption(MenuBackOut);
                UIHelpers.SetDefaultMenuSounds(OptionMenu, true, true);
            }

            OptionMenu.ChangeSelection(-OptionMenu.CurSelection, false);
            OptionMenu.ResetSelectionStates();

            CurState = States.Options;

            PushWithTransition(OptionMenu, MenuTransition.CreateTransitionDir(MainMenu, OptionMenu, Direction.Right, 200d));
        }

        private void Controls()
        {
            if (ControlScreen == null)
            {
                ControlScreen = new ControlSelectMenu(RenderingGlobals.BaseResolutionHalved + new Vector2(RenderingGlobals.BaseResolutionWidth, 0f), this);
                ControlScreen.SetBackoutOption(MenuBackOut);
                UIHelpers.SetDefaultMenuSounds(ControlScreen, true, true);
            }

            ControlScreen.ChangeSelection(-ControlScreen.CurSelection, false);
            ControlScreen.ResetSelectionStates();

            CurState = States.Controls;
            PushWithTransition(ControlScreen, MenuTransition.CreateTransitionDir(MainMenu, ControlScreen, Direction.Right, 200d));
        }

        private void Credits()
        {
            CurState = States.Credits;

            if (CreditDisplay == null)
            {
                CreditDisplay = new CreditsDisplay();
            }

            CreditDisplay.Reset();
        }

        private void TransitionToMainMenu()
        {
            CurState = States.Menu;
            MenuStack.Push(MainMenu);
            MainMenu.ResetSelectionStates();
            SetTransition(MenuTransition.CreateTransitionDir(TitleMenu, MainMenu, Direction.Right, 200d));
        }

        private void EnterMainMenu()
        {
            CurState = States.Menu;
            MenuStack.Push(MainMenu);
            MainMenu.ResetSelectionStates();

            //Create a transition and end it immediately to put screens into the correct places
            MenuTransition transition = MenuTransition.CreateTransitionDir(TitleMenu, MainMenu, Direction.Right, 0d);
            SetTransition(transition);
            transition.EndTransition();
        }

        private void QuitGame()
        {
            Engine.QuitGame();
        }

        private void SetStateToMenu()
        {
            CurState = States.Menu;
        }

        private void MenuBackOut()
        {
            if (MenuStack.Count > 1)
            {
                UIInputMenu prevMenu = MenuStack.Pop();
                UIInputMenu curMenu = MenuStack.Peek();

                MenuTransition transition = MenuTransition.CreateTransitionDir(prevMenu, curMenu, Direction.Left, 200d);

                if (MenuStack.Count == 1)
                {
                    CurState = States.Title;
                    TitleElapsedTime = 0d;
                    StartTextObj.Position = TitleMenu.Position + StartTextStartPos;
                }
                else
                {
                    transition.OnTransitionEnd = TransitionEnd;
                }

                SetTransition(transition);
            }
        }

        private void InitializeTitleLevel()
        {
            TitleIGState = new IngameState(AssetManager.Instance.LoadTiledMap(ContentGlobals.TitleLevel), LevelEventTypes.Custom);
            TitleIGState.ChangeLevelState(new TitleScreenCutsceneState(TitleIGState));
        }

        private void OnKeyboardMappingsChanged(in int kbIndex)
        {
            InputHandler inputHandler = Input.GetInputHandler(kbIndex);
            if (inputHandler == null)
            {
                return;
            }

            string selectStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.Select);
            //string backOutStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.BackOut);

            StartTextObj.Text = $"Press {selectStr}";///{backOutStr}";
        }

        private void DrawTitle()
        {
            TitleLogo.Render();
        }

        private void DrawCredits()
        {
            CreditDisplay.RenderCredits();
        }

        private void ReloadTitle()
        {
            if (KeyboardInput.GetKey(Keys.Tab) == false)
            {
                return;
            }

            if (KeyboardInput.GetKeyDown(Keys.R) == true)
            {
                if (KeyboardInput.GetKey(Keys.Back) == true)
                {
                    //Randomize save data
                    DebugRandomizeSaveData();
                    return;
                }

                GameStateManager.Instance.ChangeGameState(new TitleState(States.Menu, false));
                return;
            }
        }

        private void EnterDemoEndTitleDemo()
        {
            if (KeyboardInput.GetKey(Keys.Tab) == false)
            {
                return;
            }

            if (KeyboardInput.GetKeyDown(Keys.D) == true)
            {
                GameStateManager.Instance.ChangeGameState(new DemoEndState());
                return;
            }

            if (KeyboardInput.GetKeyDown(Keys.T) == true)
            {
                CutsceneLoader.LoadCutscene(CutsceneEnums.TitleDemo).Start();
                return;
            }
        }

        private void DebugRandomizeSaveData()
        {
            for (int i = 0; i < Engine.NumWorlds; i++)
            {
                if (SaveData.GetWorldData(i, out WorldData worldData) == false)
                {
                    continue;
                }

                const int minMoves = 10;
                const int maxMoves = 51;
                const int worldIncreaseMoves = 5;
                const int worldIncreaseMaxMoves = 10;

                const int minTime = 10;
                const int maxTime = (3 * 60) + 1;
                const int worldIncreaseTime = 3;

                foreach (KeyValuePair<int, LevelData> lvlPair in worldData.levelData)
                {
                    int randMoveRange = RandomGlobals.Randomizer.Next(minMoves + (i * worldIncreaseMoves), maxMoves + (i * worldIncreaseMaxMoves));
                    int randTimeRange = RandomGlobals.Randomizer.Next((minTime + (i * worldIncreaseTime)) * 1000, maxTime * 1000);

                    lvlPair.Value.LvlStats.NumMoves = randMoveRange;
                    lvlPair.Value.LvlStats.LevelTime = randTimeRange;
                }
            }
        }
    }
}