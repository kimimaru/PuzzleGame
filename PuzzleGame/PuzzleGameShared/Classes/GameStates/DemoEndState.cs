/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// The title screen.
    /// </summary>
    public class DemoEndState : UIMenuManager, IGameState
    {
        private Texture2D UITex = null;
        private SpriteFont Font24px = null;
        private SpriteFont Font21px = null;
        private SpriteFont Font13px = null;

        private const string HeaderText = "Thanks for playing!";
        private const string PromoText = "Stay up-to-date with Maze Burrow's development!";
        //private const string PromoText2 = "development through the following avenues!";
        private const string SocialText = "Twitter";
        private const string SocialLink = "https://twitter.com/Kimimaru4000";
        private const string NewsletterText = "Newsletter";
        private const string NewsletterLink = "https://www.subscribepage.com/q0g3i7";
        private const string BackText = "Back to title";

        private readonly Color TitleColor = Color.White;
        private readonly Color TextColor = Color.White;

        private UIMenuOption.OptionSelected MenuOptionSelected = null;
        private UIMenuOption.OptionDeselected MenuOptionDeselected = null;

        private UIInputMenu MainMenu = null;
        private UIText HeaderTextObj = null;
        private UIText PromoTextObj = null;
        //private UIText PromoText2Obj = null;

        private readonly Color BGColor = new Color(59, 86, 68, 235);
        private readonly Color OutlineColor = Color.Black;

        public DemoEndState()
        {
            
        }

        private void OnOptionSelected(in int optionIndex)
        {
            UIInputMenu menu = MenuStack.Peek();

            menu.Elements[optionIndex].TintColor = TextColor;
        }

        private void OnOptionDeselected(in int optionIndex)
        {
            UIInputMenu menu = MenuStack.Peek();

            menu.Elements[optionIndex].TintColor = new Color(180, 180, 180, 255);
        }

        public void Enter()
        {
            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);//AssetManager.Instance.LoadTexture(ContentGlobals.TitleBG);
            Font24px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont24px);
            Font21px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont21px);
            Font13px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont13px);

            MenuOptionSelected = OnOptionSelected;
            MenuOptionDeselected = OnOptionDeselected;

            //Stop music
            SoundManager.Instance.StopAndClearMusicTrack();

            InitMenu();

            MainMenu.ResetSelectionStates();
        }

        public void Exit()
        {
            UITex = null;
            Font24px = null;
            Font21px = null;
            Font13px = null;
        }

        private void InitMenu()
        {
            MainMenu = new UIInputMenu(5, RenderingGlobals.BaseResolutionHalved);

            HeaderTextObj = UIHelpers.CreateCenterUIText(Font24px, HeaderText, Vector2.One, Color.SkyBlue, UITextOptions.Outline, UITextData.Standard);
            HeaderTextObj.Position = new Vector2(RenderingGlobals.BaseResWidthHalved, 50f);

            PromoTextObj = UIHelpers.CreateCenterUIText(Font13px, PromoText, Vector2.One, UITextOptions.Outline, UITextData.Standard);
            PromoTextObj.Position = HeaderTextObj.Position + new Vector2(0f, 60f);
            
            UIText newsletterOption = UIHelpers.CreateCenterUIText(Font21px, NewsletterText, Vector2.One, UITextOptions.Outline, UITextData.Standard);
            MainMenu.AddMenuElementRelative(newsletterOption, new Vector2(0f, -20f), new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, OnSelectNewsletter, null));

            UIText socialOption = UIHelpers.CreateCenterUIText(Font21px, SocialText, Vector2.One, UITextOptions.Outline, UITextData.Standard);
            MainMenu.AddMenuElementRelative(socialOption, new Vector2(0f, 30f), new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, OnSelectSocial, null));

            UIText backOption = UIHelpers.CreateCenterUIText(Font21px, BackText, Vector2.One, UITextOptions.Outline, UITextData.Standard);
            MainMenu.AddMenuElementRelative(backOption, new Vector2(0f, 160f), new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, OnSelectBack, null));

            UIHelpers.SetDefaultMenuSounds(MainMenu, false, false);

            base.Push(MainMenu);
        }

        public void Update()
        {
            base.UpdateMenu();
        }

        public void Render()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

            //Draw the BG texture
            //Ceiling so we can fit more in
            //const float scale = 30f;
            //int xAmount = (int)Math.Ceiling((RenderingGlobals.BaseResolution.X / BG.Width) / scale);
            //int yAmount = (int)Math.Ceiling((RenderingGlobals.BaseResolution.Y / BG.Height) / scale);
            //
            //int total = xAmount * yAmount;
            //Vector2 scaleVec = new Vector2(scale, scale);
            //
            //for (int i = 0; i < total; i++)
            //{
            //    Vector2 pos = new Vector2((BG.Width * scale) * (i % xAmount),
            //        (BG.Height * scale) * (i / xAmount));
            //
            //    RenderingManager.Instance.DrawSprite(BG, pos, null, BGColor, 0f, Vector2.Zero, scaleVec, SpriteEffects.None, .1f);
            //}

            RenderingManager.Instance.DrawSprite(UITex, Vector2.Zero, ContentGlobals.BoxRect, BGColor, 0f, Vector2.Zero,
                RenderingGlobals.BaseResolution, SpriteEffects.None, .1f);

            HeaderTextObj.Render();
            PromoTextObj.Render();
            //PromoText2Obj.Render();

            //Render the menu
            base.RenderMenu();

            RenderingManager.Instance.EndCurrentBatch();
        }

        private void OnSelectSocial()
        {
            UtilityGlobals.OpenURL(SocialLink);
        }

        private void OnSelectNewsletter()
        {
            UtilityGlobals.OpenURL(NewsletterLink);
        }

        private void OnSelectBack()
        {
            GameStateManager.Instance.ChangeGameState(new TitleState(TitleState.States.Title, false));
        }
    }
}