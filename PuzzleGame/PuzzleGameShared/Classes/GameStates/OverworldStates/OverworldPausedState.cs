/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class OverworldPausedState : UIMenuManager, IGameState
    {
        private OverworldState overworldState = null;
        private SpriteFont Font = null;
        private SpriteFont LevelsFont = null;
        private SpriteFont StatsFont = null;
        private Texture2D UITex = null;
        private UIRenderableContainer WorldInfoContainer = null;

        private readonly Color OutlineColor = Color.Black;

        private readonly Color FadeColor = Color.Black * .7f;

        private readonly Color TextColor = Color.White;//new Color(0, 140, 0, 255);

        private string WorldName = string.Empty;
        private string LevelCompletionText = string.Empty;
        private string TotalMovesText = string.Empty;
        private string TotalTimeText = string.Empty;

        public static readonly Color AllLevelsColor = new Color(0, 140, 0, 255);
        public static readonly Color AllLevelsCompleteColor = new Color(215, 175, 0, 255);
        public static readonly Color WorldStatsColor = Color.White;//new Color(0, 100, 255, 255);
        private Color LevelTextColor = AllLevelsColor;

        private const double FadeTime = 200d;
        private double ElapsedTime = 0d;

        private UIMenuOption.OptionSelected MenuOptionSelected = null;
        private UIMenuOption.OptionDeselected MenuOptionDeselected = null;

        private UIInputMenu InitMenu = null;
        private OptionsMenuNew OptionMenu = null;
        private TotalsMenu TotalsMenu = null;
        private ControlSelectMenu ControlScreen = null;
        //private ControlsMenu ControlsScreen = null;

        private MenuTransition.TransitionEnd OnTransitionEnd = null;

        public OverworldPausedState(OverworldState owState)
            : base()
        {
            overworldState = owState;

            OnTransitionEnd = ShowWorldInfoContainer;
        }

        private void SetUpInitMenu()
        {
            SetUpWorldInfo();

            MenuOptionSelected = OnOptionSelected;
            MenuOptionDeselected = OnOptionDeselected;

            InitMenu = new UIInputMenuPauseBack(5, RenderingGlobals.BaseResolutionHalved + new Vector2(0f, -40f));
            InitMenu.SetBackoutOption(MenuBackOut);

            Vector2 relativePos = new Vector2(-85f, 15f);
            Vector2 origin = new Vector2(0f, .5f);
            const float yDiff = 45;

            InitMenu.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Continue", origin, Vector2.One, UITextOptions.Outline, UITextData.Standard),
                relativePos, new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, Continue, null));
            InitMenu.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "World Totals", origin, Vector2.One, UITextOptions.Outline, UITextData.Standard),
                relativePos + new Vector2(0f, (yDiff * 1)), new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, Totals, null));
            InitMenu.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Options", origin, Vector2.One, UITextOptions.Outline, UITextData.Standard),
                relativePos + new Vector2(0f, (yDiff * 2)), new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, Options, null));
            InitMenu.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Controls", origin, Vector2.One, UITextOptions.Outline, UITextData.Standard),
                relativePos + new Vector2(0f, (yDiff * 3)), new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, Controls, null));
            InitMenu.AddMenuElementRelative(UIHelpers.CreateUIText(Font, "Exit to menu", origin, Vector2.One, UITextOptions.Outline, UITextData.Standard),
                relativePos + new Vector2(0f, (yDiff * 4)), new UIMenuOption(MenuOptionSelected, MenuOptionDeselected, ExitOverworld, null));

            MenuStack.Push(InitMenu);

            UIHelpers.SetDefaultMenuSounds(InitMenu, false, false);
            InitMenu.ResetSelectionStates();
        }

        private void OnOptionSelected(in int optionIndex)
        {
            UIInputMenu menu = MenuStack.Peek();

            menu.Elements[optionIndex].TintColor = TextColor;
        }

        private void OnOptionDeselected(in int optionIndex)
        {
            UIInputMenu menu = MenuStack.Peek();
            
            menu.Elements[optionIndex].TintColor = new Color(180, 180, 180, 255);
        }

        private void ShowWorldInfoContainer()
        {
            WorldInfoContainer.Visible = true;
        }

        public void Enter()
        {
            WorldName = "World " + (overworldState.WorldID + 1);
            LevelCompletionText = $"{overworldState.WorldInformation.LevelsCompleted}/{overworldState.WorldInformation.TotalLevels}";
            TotalMovesText = overworldState.WorldInformation.TotalMoves.ToString();
            TotalTimeText = LevelGlobals.GetFormattedLevelTimeString(overworldState.WorldInformation.TotalTime);

            if (overworldState.WorldInformation.LevelsCompleted == overworldState.WorldInformation.TotalLevels)
            {
                LevelTextColor = AllLevelsCompleteColor;
            }

            Font = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont21px);
            LevelsFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont12px);
            StatsFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont10px);
            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);

            SetUpInitMenu();

#if DEBUG
            Debug.AddCustomDebugCommand(DebugMoveUI);
#endif
        }

        public void Exit()
        {
            InitMenu?.CleanUp();
            OptionMenu?.CleanUp();
            TotalsMenu?.CleanUp();
            ControlScreen?.CleanUp();
#if DEBUG
            Debug.RemoveCustomDebugCommand(DebugMoveUI);
#endif
        }

        public void Update()
        {
            base.UpdateMenu();

            if (ElapsedTime >= FadeTime) return;

            //Lerp the color
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
            if (ElapsedTime >= FadeTime)
            {
                ElapsedTime = FadeTime;
            }
        }

        private void SetUpWorldInfo()
        {
            Vector2 containerSize = new Vector2(180f, 104f);

            NineSlicedSprite nineSliced = new NineSlicedSprite(UITex, ContentGlobals.PanelRect, 5, 5, 5, 5);

            Vector2 pos = new Vector2(RenderingGlobals.BaseResolutionWidth / 2, (containerSize.Y / 2) - nineSliced.BottomLine);

            Vector2 containerOrigin = (containerSize / 2);
            containerOrigin.Floor();
            Vector2 containerPos = pos - containerOrigin;

            WorldInfoContainer = new UIRenderableContainer(5, containerPos);

            UINineSlicedSprite uiNineSliced = new UINineSlicedSprite(nineSliced);
            uiNineSliced.Scale = containerSize;
            WorldInfoContainer.AddElementRelative(uiNineSliced, Vector2.Zero);

            UISprite movesIcon = new UISprite(new Sprite(UITex, ContentGlobals.MovesIconRectSmall, new Vector2(0f, .5f)));
            UISprite timeIcon = new UISprite(new Sprite(UITex, ContentGlobals.TimeIconRectSmall, new Vector2(0f, .5f)));

            WorldInfoContainer.AddElementRelative(movesIcon, new Vector2(containerOrigin.X - 32f, 68f));
            WorldInfoContainer.AddElementRelative(timeIcon, new Vector2(containerOrigin.X - 22f, 89f));

            WorldInfoContainer.AddElementRelative(UIHelpers.CreateUIText(StatsFont, TotalMovesText, new Vector2(0f, .5f),
                    Vector2.One, Color.Black), new Vector2(containerOrigin.X, 73f));

            WorldInfoContainer.AddElementRelative(UIHelpers.CreateUIText(StatsFont, TotalTimeText, new Vector2(0f, .5f),
                    Vector2.One, Color.Black), new Vector2(containerOrigin.X, 95f));

            WorldInfoContainer.AddElementRelative(UIHelpers.CreateCenterUIText(LevelsFont, LevelCompletionText,
                    Vector2.One, LevelTextColor), new Vector2(containerOrigin.X, 47f));

            WorldInfoContainer.AddElementRelative(UIHelpers.CreateCenterUIText(LevelsFont, WorldName,
                    Vector2.One, Color.Black), new Vector2(containerOrigin.X, 22f));
        }

        public void Render()
        {
            //Draw the overworld state
            overworldState.RenderOverworld();

            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

            RenderingManager.Instance.DrawSprite(UITex, Vector2.Zero, ContentGlobals.BoxRect, FadeColor * (float)(ElapsedTime / FadeTime),
                0f, Vector2.Zero, RenderingGlobals.BaseResolution, SpriteEffects.None, .3f);

            //Display world information
            if (WorldInfoContainer.Visible == true)
            {
                WorldInfoContainer.Render();
            }

            //Draw all the menu options
            base.RenderMenu();

            RenderingManager.Instance.EndCurrentBatch();
        }

        private void Continue()
        {
            overworldState.ChangeState(new OverworldSelectState(overworldState));
        }

        private void Totals()
        {
            if (TotalsMenu == null)
            {
                TotalsMenu = new TotalsMenu(RenderingGlobals.BaseResolutionHalved + new Vector2(RenderingGlobals.BaseResolutionWidth, 70f), overworldState.WorldID);
                TotalsMenu.SetBackoutOption(MenuBackOut);
                UIHelpers.SetDefaultMenuSounds(TotalsMenu, true, true);
            }

            WorldInfoContainer.Visible = false;

            PushWithTransition(TotalsMenu, MenuTransition.CreateTransitionDir(InitMenu, TotalsMenu, Direction.Right, 200d));
        }

        private void Options()
        {
            if (OptionMenu == null)
            {
                OptionMenu = new OptionsMenuNew(this, RenderingGlobals.BaseResolutionHalved + new Vector2(RenderingGlobals.BaseResolutionWidth, -150f),
                    new Vector2(0f, 35f), AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont15px));
                OptionMenu.SetBackoutOption(MenuBackOut);
                UIHelpers.SetDefaultMenuSounds(OptionMenu, true, true);
            }

            WorldInfoContainer.Visible = false;

            OptionMenu.ChangeSelection(-OptionMenu.CurSelection, false);
            OptionMenu.ResetSelectionStates();

            PushWithTransition(OptionMenu, MenuTransition.CreateTransitionDir(InitMenu, OptionMenu, Direction.Right, 200d));
        }

        private void Controls()
        {
            if (ControlScreen == null)
            {
                ControlScreen = new ControlSelectMenu(RenderingGlobals.BaseResolutionHalved + new Vector2(RenderingGlobals.BaseResolutionWidth, 0f), this);
                ControlScreen.SetBackoutOption(MenuBackOut);
                UIHelpers.SetDefaultMenuSounds(ControlScreen, true, true);
            }

            ControlScreen.ChangeSelection(-ControlScreen.CurSelection, false);
            ControlScreen.ResetSelectionStates();

            WorldInfoContainer.Visible = false;

            PushWithTransition(ControlScreen, MenuTransition.CreateTransitionDir(InitMenu, ControlScreen, Direction.Right, 200d));

            //if (ControlsScreen == null)
            //{
            //    ControlsScreen = new ControlsMenu(new Vector2(RenderingGlobals.BaseResolutionWidth, 0f), true);
            //    ControlsScreen.SetBackoutOption(MenuBackOut);
            //    ControlsScreen.SetSounds(null, AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), null, AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionBackSound),
            //        0f, 1f, 0f, .8f);
            //}
            //
            //WorldInfoContainer.Visible = false;
            //
            //PushWithTransition(ControlsScreen, MenuTransition.CreateTransitionDir(InitMenu, ControlsScreen, Direction.Right, 200d));
        }

        private void ExitOverworld()
        {
            //Unload all content when exiting back to the title screen
            SoundManager.Instance.ClearAllSounds();
            SoundManager.Instance.ClearMusicCache();

            AssetManager.Instance.UnloadLoadedContent();

            //Manually play the selection sound since it just got unloaded
            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false);

            GameStateManager.Instance.ChangeGameState(new TitleState(TitleState.States.Menu, false));
        }

        private void MenuBackOut()
        {
            if (MenuStack.Count > 1)
            {
                UIInputMenu prevMenu = MenuStack.Pop();
                UIInputMenu curMenu = MenuStack.Peek();

                MenuTransition transition = MenuTransition.CreateTransitionDir(prevMenu, curMenu, Direction.Left, 200d, OnTransitionEnd);

                SetTransition(transition);
            }
            else
            {
                Continue();
            }
        }

        private void DebugMoveUI()
        {
            if (KeyboardInput.GetKey(Keys.LeftShift, Debug.DebugKeyboard) == false)
            {
                return;
            }

            const float speed = 5f;

            Vector2 dir = Vector2.Zero;
            if (KeyboardInput.GetKey(Keys.Up, Debug.DebugKeyboard) == true)
            {
                dir.Y -= speed;
            }
            else if (KeyboardInput.GetKey(Keys.Down, Debug.DebugKeyboard) == true)
            {
                dir.Y += speed;
            }

            if (KeyboardInput.GetKey(Keys.Left, Debug.DebugKeyboard) == true)
            {
                dir.X -= speed;
            }
            else if (KeyboardInput.GetKey(Keys.Right, Debug.DebugKeyboard) == true)
            {
                dir.X += speed;
            }

            if (dir.X != 0f || dir.Y != 0f)
            {
                MenuStack.Peek().Position += dir;
            }
        }
    }
}