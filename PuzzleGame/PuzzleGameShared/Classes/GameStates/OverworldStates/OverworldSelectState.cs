/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class OverworldSelectState : IGameState
    {
        private OverworldState overworldState = null;

        //private SpriteFont Font = null;
        //private UIText PauseText = null;

        private const double MoveTime = 247d;

        public OverworldSelectState(OverworldState owState)
        {
            overworldState = owState;
        }

        public void Enter()
        {
            //Font = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont10px);
            //
            //PauseText = UIHelpers.CreateUIText(Font, "Esc: Pause", new Vector2(0f, .5f), Vector2.One, Color.White,
            //    UITextOptions.Outline, new UITextData(Color.Black * .8f, 1f, -.001f));
            //PauseText.Position = new Vector2(8f, 18f);
        }

        public void Exit()
        {
            
        }

        public void Update()
        {
            //Enter view map mode
            if (Input.GetButtonDown(InputActions.ViewMap) == true)
            {
                overworldState.ChangeState(new OverworldViewMapState(overworldState));
                return;
            }

            for (int i = 0; i < overworldState.Context.Blocks.Count; i++)
            {
                overworldState.Context.Blocks[i].Update();
            }

            for (int i = 0; i < overworldState.Context.RockShooters.Count; i++)
            {
                overworldState.Context.RockShooters[i].Update();
            }

            overworldState.Context.Player.AnimationManager.CurrentAnim.Update();

            for (int i = 0; i < overworldState.Portals.Count; i++)
            {
                overworldState.Portals[i].Update();
            }

            for (int i = 0; i < overworldState.Context.Warps.Count; i++)
            {
                overworldState.Context.Warps[i].Update();
            }

            for (int i = 0; i < overworldState.overworldMap.Nodes.Count; i++)
            {
                overworldState.overworldMap.Nodes[i].Update();
            }

            //Update the panel when the player isn't moving to avoid issues with it clipping
            //Also don't update it if the current node is empty
            if (overworldState.IsPlayerMoving == false && overworldState.CurNode.NodeType != MapNodeTypes.Empty)
                overworldState.LevelPanel.Update();

            //Pause the game if you press Escape
            if (Input.GetButtonDown(InputActions.Pause) == true)
            {
                overworldState.ChangeState(new OverworldPausedState(overworldState));
                return;
            }

            //Select the node
            if (Input.GetButtonDown(InputActions.Select) == true || Input.GetButtonDown(InputActions.Grab) == true)
            {
                overworldState.CurNode.OnNodeSelected(overworldState);
                return;
            }

            /* Player overworld movement */
            if (overworldState.IsPlayerMoving == true)
            {
                overworldState.ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
                if (overworldState.ElapsedTime >= MoveTime)
                {
                    overworldState.ElapsedTime = 0d;
                    overworldState.IsPlayerMoving = false;

                    overworldState.Context.Player.transform.Position = overworldState.PlayerDest;

                    overworldState.StartingNodeIndex = overworldState.CurrentNodeIndex;
                }
                else
                {
                    overworldState.Context.Player.transform.Position = Interpolation.Interpolate(overworldState.PlayerStart, overworldState.PlayerDest,
                        overworldState.ElapsedTime / MoveTime, Interpolation.InterpolationTypes.Linear);
                }

                overworldState.camera.LookAtAndClamp(overworldState.Context.Player.transform.Position, overworldState.overworldMap.MapBounds);
            }

            HandleMovement();
        }

        private void HandleMovement()
        {
            Direction? pressedDir = null;
            Direction? pressedDirVertical = null;

            //Navigation - Check for a path from this node
            //For fun, change the player's direction regardless if they can move there or not
            if (Input.GetButton(InputActions.Right) == true)
            {
                pressedDir = Direction.Right;
            }
            else if (Input.GetButton(InputActions.Left) == true)
            {
                pressedDir = Direction.Left;
            }
            if (Input.GetButton(InputActions.Up) == true)
            {
                if (pressedDir == null)
                    pressedDir = Direction.Up;
                pressedDirVertical = Direction.Up;
            }
            else if (Input.GetButton(InputActions.Down) == true)
            {
                if (pressedDir == null)
                    pressedDir = Direction.Down;
                pressedDirVertical = Direction.Down;
            }

            //If we pressed a direction, change the player's facing dir and see if we can move to another level in this direction
            if (pressedDir != null)
            {
                Direction dir = pressedDir.Value;
                int newNode = overworldState.CurrentNodeIndex;

            //Label used to try again with a vertical direction if one was pressed
            //This allows us to smooth overworld map movement if you hold both directions (but could be written better)
            Check:

                //Don't change direction if the player is already moving so the player doesn't change direction if they can't go this way
                if (overworldState.IsPlayerMoving == false)
                    overworldState.Context.Player.FacingDir = dir;

                int? destNode = overworldState.CurNode.GetNodeForDirection(dir);

                //Move in the appropriate direction
                if (destNode != null)
                {
                    int nodeVal = overworldState.GetNodeIndex(destNode.Value);

                    //Make sure the node is valid
                    if (nodeVal >= 0)
                    {
                        //Check if we can traverse to this node; it's traversable if the current level or that level has been completed
                        SaveData.GetLevelData(overworldState.WorldID, overworldState.CurNode.LevelID, out LevelData curLvlData);
                        SaveData.GetLevelData(overworldState.WorldID, destNode.Value, out LevelData destLvlData);

                        if (curLvlData?.Complete == true || destLvlData?.Complete == true)
                        {
                            //Move to this node if the player isn't already moving or it's the node the player started moving from
                            //This makes it so that you can only move between two nodes at a time with the ability to go back to the node you just left
                            if (overworldState.IsPlayerMoving == false || nodeVal == overworldState.StartingNodeIndex)
                            {
                                newNode = nodeVal;
                            }
                        }
                    }
                    else if (pressedDirVertical != null && pressedDirVertical.Value != dir)
                    {
                        dir = pressedDirVertical.Value;
                        goto Check;
                    }
                    //If we're moving, make sure we don't overshoot the level we're going to
                    //Only allow going between the original node and the destination
                    //if (overworldState.IsPlayerMoving == false || prevNode >= overworldState.StartingNode)
                    //{
                    //    newNode = prevNode;
                    //}
                    ////Use the vertical direction for smoother overworld movement if it's pressed and not the same as the current direction
                    //else if (pressedDirVertical != null && pressedDirVertical.Value != dir)
                    //{
                    //    dir = pressedDirVertical.Value;
                    //    goto Check;
                    //}
                }
                else
                {
                    //If no nodes are in the direction held, check if the player was holding a vertical direction and use with that direction if so
                    if (pressedDirVertical != null && pressedDirVertical.Value != dir)
                    {
                        dir = pressedDirVertical.Value;
                        goto Check;
                    }
                }

                //If the node we should go to isn't the current one, go to it if we've passed the requirements
                if (newNode != overworldState.CurrentNodeIndex)
                {
                    overworldState.Context.Player.FacingDir = dir;

                    Vector2 nodePos = overworldState.GetNode(newNode).Position;

                    overworldState.PlayerStart = overworldState.CurNode.Position;
                    overworldState.PlayerDest = nodePos;

                    //If the player is moving, invert the elapsed time to move towards the new destination from where they left off
                    if (overworldState.IsPlayerMoving == true)
                    {
                        overworldState.ElapsedTime = (MoveTime - overworldState.ElapsedTime);
                    }
                    else
                    {
                        //Get the player moving
                        overworldState.IsPlayerMoving = true;
                        overworldState.Context.Player.transform.Position = overworldState.PlayerStart;
                        overworldState.Context.Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.PlayerAnimations.Walk);
                        overworldState.ElapsedTime = 0d;
                    }

                    overworldState.StartingNodeIndex = overworldState.CurrentNodeIndex;

                    //Update the current node right away; this lets players enter the new level while the character is traveling
                    overworldState.CurrentNodeIndex = newNode;

                    overworldState.LevelPanel.UpdateLevelInfo(overworldState.CurNode);

                    SaveData.SetLastPlayedLevel(overworldState.CurNode.LevelID);
                }
                else
                {
                    if (overworldState.IsPlayerMoving == false)
                        overworldState.Context.Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.Idle);
                }
            }
            else
            {
                if (overworldState.IsPlayerMoving == false)
                    overworldState.Context.Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.Idle);
            }
        }

        public void Render()
        {
            overworldState.RenderOverworld();

            //RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
            //    SamplerState.PointClamp, null, null, null, null);
            //
            ////Show text saying how to pause
            //PauseText.Render();
            //
            //RenderingManager.Instance.EndCurrentBatch();
        }
    }
}