/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class OverworldEnterLevelState : IGameState
    {
        public delegate void OnFinish();
        private OnFinish OnEnterFinish = null;

        private OverworldState overworldState = null;

        private Texture2D Box = null;

        private Vector2 PlayerStartPos = Vector2.Zero;

        private Vector2 PlayerStartScale = Vector2.Zero;
        private Vector2 PlayerEndScale = Vector2.Zero;

        private readonly Direction[] ClockwiseDirections = new Direction[4] { Direction.Down, Direction.Left, Direction.Up, Direction.Right };

        private Direction CurDirection = Direction.Down;

        private const double TotalTime = 600d;
        private const double CharRotateTime = 74d;

        private double CurRotationTime = CharRotateTime;

        private double ElapsedTime = 0d;

        public OverworldEnterLevelState(OverworldState owState, OnFinish onEnterFinish)
        {
            overworldState = owState;
            OnEnterFinish = onEnterFinish;
        }

        public void Enter()
        {
            Box = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);

            overworldState.camera.LookAt(overworldState.CurNode.Position);
            overworldState.camera.ClampCamera(overworldState.overworldMap.MapBounds);

            PlayerStartPos = overworldState.CurNode.Position;
            PlayerStartScale = overworldState.Context.Player.transform.Scale;

            CurDirection = ClockwiseDirections[0];

            ElapsedTime = 0d;

            overworldState.Context.Player.transform.Position = PlayerStartPos;
            overworldState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Success);
            overworldState.Context.Player.FacingDir = CurDirection;
        }

        public void Exit()
        {
            Box = null;
            OnEnterFinish = null;
        }

        public void Update()
        {
            overworldState.Context.Player.AnimationManager.CurrentAnim.Update();

            for (int i = 0; i < overworldState.Portals.Count; i++)
            {
                overworldState.Portals[i].Update();
            }

            for (int i = 0; i < overworldState.Context.Warps.Count; i++)
            {
                overworldState.Context.Warps[i].Update();
            }

            for (int i = 0; i < overworldState.overworldMap.Nodes.Count; i++)
            {
                overworldState.overworldMap.Nodes[i].Update();
            }

            overworldState.LevelPanel.Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= TotalTime)
            {
                overworldState.Context.Player.transform.Scale = PlayerEndScale;
                CurDirection = ClockwiseDirections[0];
                overworldState.Context.Player.FacingDir = CurDirection;

                OnEnterFinish();
            }
            else
            {
                overworldState.Context.Player.transform.Scale = Interpolation.Interpolate(PlayerStartScale, PlayerEndScale,
                    ElapsedTime / TotalTime, Interpolation.InterpolationTypes.CubicIn);

                int fullRotations = (int)(ElapsedTime / (TotalTime / 2));
                CurRotationTime = CharRotateTime / (fullRotations + 1);

                CurDirection = ClockwiseDirections[(int)(ElapsedTime / CurRotationTime) % ClockwiseDirections.Length];
                overworldState.Context.Player.FacingDir = CurDirection;
            }
        }

        private void EnterLevel()
        {
            //Check if we should load a level or world based on the type of node
            OverworldMapNode node = overworldState.CurNode;

            node.OnNodeSelected(overworldState);
        }

        public void Render()
        {
            overworldState.RenderOverworld();

            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

            RenderingManager.Instance.DrawSprite(Box, Vector2.Zero, ContentGlobals.BoxRect, Color.Black * (float)(ElapsedTime / TotalTime), 0f, Vector2.Zero,
                RenderingGlobals.BaseResolution, SpriteEffects.None, .3f);

            RenderingManager.Instance.EndCurrentBatch();
        }
    }
}