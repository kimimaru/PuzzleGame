/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class OverworldWarpOutState : IGameState
    {
        private OverworldState overworldState = null;

        private Warp WarpUsed = null;

        private Texture2D Box = null;
        private bool UseFade = false;
        private Color StartFadeColor = Color.Transparent;
        private Color EndFadeColor = Color.Black;
        private bool SameWorld = false;
        private double FadeTime = 0d;
        private double ElapsedFadeTime = 0d;

        private bool FrameWait = false;

        public OverworldWarpOutState(OverworldState owState, Warp warpUsed, in bool sameWorld)
        {
            overworldState = owState;
            WarpUsed = warpUsed;
            SameWorld = sameWorld;
        }

        public OverworldWarpOutState(OverworldState owState, Warp warpUsed, in bool sameWorld, in double fadeTime, in Color startFadeColor, in Color endFadeColor)
            : this(owState, warpUsed, sameWorld)
        {
            UseFade = true;
            FadeTime = fadeTime;
            StartFadeColor = startFadeColor;
            EndFadeColor = endFadeColor;
        }

        public void Enter()
        {
            if (UseFade == true)
            {
                Box = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
                ElapsedFadeTime = 0d;
                FrameWait = true;
            }
            else
            {
                WarpUsed.PlayWarpOutSound();
            }

            WarpUsed.WarpedObjectOutEvent -= WarpOutFinished;
            WarpUsed.WarpedObjectOutEvent += WarpOutFinished;
        }

        public void Exit()
        {
            WarpUsed.WarpedObjectOutEvent -= WarpOutFinished;
            WarpUsed = null;
            Box = null;
        }

        public void Update()
        {
            if (FrameWait == true)
            {
                FrameWait = false;

                //Load the world's music track here - prevents issues when unloading music and sounds from prior states
                overworldState.LoadAndPlayOverworldMusic();

                WarpUsed.PlayWarpOutSound();

                return;
            }

            overworldState.Context.Player.AnimationManager.CurrentAnim.Update();
            
            for (int i = 0; i < overworldState.Portals.Count; i++)
            {
                overworldState.Portals[i].Update();
            }

            for (int i = 0; i < overworldState.Context.Warps.Count; i++)
            {
                overworldState.Context.Warps[i].Update();
            }

            for (int i = 0; i < overworldState.overworldMap.Nodes.Count; i++)
            {
                overworldState.overworldMap.Nodes[i].Update();
            }

            if (UseFade == true)
            {
                ElapsedFadeTime += Time.ElapsedTime.TotalMilliseconds;
            }
        }

        private void WarpOutFinished(ICollisionObj collisionObj)
        {
            overworldState.Context.Player.ChangeState(new BlankState());

            //If the world ID is the same as the current world, tell the overworld the new node the player is on
            if (SameWorld == true)
            {
                WorldMapNode worldNode = (WorldMapNode)overworldState.CurNode;

                int nodeIndex = overworldState.GetNodeIndex(worldNode.LandLevelID);

                overworldState.StartingNodeIndex = nodeIndex;
                overworldState.CurrentNodeIndex = nodeIndex;

                overworldState.LevelPanel.UpdateLevelInfo(overworldState.CurNode);
            }

            //Check for completion of the node we're on and surrounding nodes
            overworldState.CheckNodeCompletion(overworldState.CurrentNodeIndex);

            //If any level was just completed, show the complete animation
            //Otherwise, proceed like normal
            if (DataHandler.saveData.Misc.JustFinishedLevels.Count > 0)
            {
                overworldState.ChangeState(new OverworldShowCompleteState(overworldState, OverworldState.IncompleteLineColor, OverworldState.CompleteLineColor));
            }
            else
            {
                overworldState.ChangeState(new OverworldSelectState(overworldState));
            }
        }

        public void Render()
        {
            overworldState.RenderOverworld();

            if (UseFade == true)
            {
                RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

                Color color = Interpolation.Interpolate(StartFadeColor, EndFadeColor, ElapsedFadeTime / FadeTime, Interpolation.InterpolationTypes.Linear);
                RenderingManager.Instance.DrawSprite(Box, Vector2.Zero, ContentGlobals.BoxRect, color, 0f, Vector2.Zero,
                    RenderingGlobals.BaseResolution, SpriteEffects.None, .3f);

                RenderingManager.Instance.EndCurrentBatch();
            }
        }
    }
}