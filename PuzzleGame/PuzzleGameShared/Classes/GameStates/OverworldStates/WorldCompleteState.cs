/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class WorldCompleteState : LevelCompletionState
    {
        private OverworldState overworldState = null;

        #region Stat Counting

        private int NumLevels = 0;
        private string CompleteLevels = string.Empty;
        private Vector2 CompleteLevelsOrigin = Vector2.Zero;

        #endregion

        protected override Echidna Player => overworldState.Context.Player;

        public WorldCompleteState(OverworldState owState)
        {
            overworldState = owState;

            StatsStartPos = RenderingGlobals.BaseResolutionHalved + new Vector2(-80f, -2f);
            CompleteText = "WORLD COMPLETE!";

            InputTextStartPos = new Vector2(RenderingGlobals.BaseResolutionWidth / 2, RenderingGlobals.BaseResolutionHeight - 75f);

            MovesIconOffset = new Vector2(20f, 22f);
            TimeIconOffset = new Vector2(34f, 64f);
        }

        protected override void InitPlayerState()
        {
            //Set the player to the portal's position and make it face down
            overworldState.Context.Player.FacingDir = Direction.Down;
            overworldState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.Idle);

            OrigPlayerPos = overworldState.Context.Player.transform.Position;
        }

        protected override void LoadAssets()
        {
            Box = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            Font24px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont24px);
            Font21px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont21px);
            Font17px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont17px);
            FontGlyphs = Font24px.GetGlyphs();
            TallySound = AssetManager.Instance.LoadSound(ContentGlobals.TallySound);
        }

        protected override void InitData()
        {
            overworldState.LevelPanel.Reset();
        }

        public override void Enter()
        {
            base.Enter();

            //Start stats at 0
            UpdateStatsText(0, 0, 0d);
        }

        public override void Exit()
        {
            //Put the player back where it should be
            overworldState.Context.Player.transform.Position = OrigPlayerPos;
            overworldState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.Idle);

            //Stop the current music
            SoundManager.Instance.StopAndClearMusicTrack();

            //Load the world's music track
            Microsoft.Xna.Framework.Audio.SoundEffect musicTrack = null;

            try
            {
                musicTrack = AssetManager.Instance.LoadMusic(overworldState.overworldMap.MusicName);
            }
            catch (Microsoft.Xna.Framework.Content.ContentLoadException)
            {
                musicTrack = null;
            }

            if (musicTrack != null)
            {
                SoundManager.Instance.PlayMusic(musicTrack, true);
            }
        }

        private void UpdateStatsText(int numLevels, int moves, double time)
        {
            CompleteLevels = $"Levels: {numLevels}/{overworldState.WorldInformation.TotalLevels}";
            CompleteLevelsOrigin = Font17px.GetOrigin(CompleteLevels, 0f, .5f);

            base.UpdateStatsText(moves, time);
        }

        protected override void UpdateObjects()
        {
            for (int i = 0; i < overworldState.Portals.Count; i++)
            {
                overworldState.Portals[i].Update();
            }

            for (int i = 0; i < overworldState.Context.Warps.Count; i++)
            {
                overworldState.Context.Warps[i].Update();
            }

            for (int i = 0; i < overworldState.overworldMap.Nodes.Count; i++)
            {
                overworldState.overworldMap.Nodes[i].Update();
            }
        }

        /// <summary>
        /// Skips the world complete animation before the input.
        /// </summary>
        protected override void SkipAnimation()
        {
            ElapsedFadeTime = FadeTime;
            CurFadeColor = EndFadeColor;
            NumCharsPrinted = CompleteText.Length;

            Player.FacingDir = Direction.Down;
            Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Success);

            NumLevels = overworldState.WorldInformation.LevelsCompleted;
            NumMoves = overworldState.WorldInformation.TotalMoves;
            TimeVal = overworldState.WorldInformation.TotalTime;

            UpdateStatsText(NumLevels, NumMoves, TimeVal);

            Phase = CompletePhase.InputWait;
        }

        protected override void HandleStatCount()
        {
            ElapsedTallyTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTallyTime >= TallyTime)
            {
                SoundManager.Instance.PlaySound(TallySound, false, 1f);
                ElapsedTallyTime = 0d;
            }

            if (ElapsedTime >= CountTime)
            {
                ElapsedTime = 0d;
                Phase = CompletePhase.InputWait;

                NumLevels = overworldState.WorldInformation.LevelsCompleted;
                NumMoves = overworldState.WorldInformation.TotalMoves;
                TimeVal = overworldState.WorldInformation.TotalTime;

                UpdateStatsText(NumLevels, NumMoves, TimeVal);
            }
            else
            {
                double time = ElapsedTime / CountTime;
                NumLevels = Interpolation.Interpolate(0, overworldState.WorldInformation.LevelsCompleted, time, Interpolation.InterpolationTypes.Linear);
                NumMoves = Interpolation.Interpolate(0, overworldState.WorldInformation.TotalMoves, time, Interpolation.InterpolationTypes.Linear);
                TimeVal = Interpolation.Interpolate(0, overworldState.WorldInformation.TotalTime, time, Interpolation.InterpolationTypes.Linear);

                UpdateStatsText(NumLevels, NumMoves, TimeVal);
            }
        }

        protected override void HandlePressInput()
        {
            overworldState.ChangeState(new OverworldSelectState(overworldState));
        }

        protected override void HandleRestartInput()
        {
            
        }

        protected override void DrawMap()
        {
            overworldState.RenderOverworld();
        }

        protected override void DrawStatText()
        {
            Color levelsColor = StatColor;

            if (Phase == CompletePhase.InputWait)
            {
                if (NumLevels == overworldState.WorldInformation.TotalLevels)
                    levelsColor = OverworldPausedState.AllLevelsCompleteColor;
                else
                    levelsColor = OverworldPausedState.AllLevelsColor;
            }

            //Draw the world stats
            RenderingManager.Instance.spriteBatch.DrawStringOutline(2f, Color.Black, Font17px, CompleteLevels, StatsStartPos + new Vector2(0f, -1f), levelsColor, 0f,
                CompleteLevelsOrigin, StatsScale, SpriteEffects.None, .32f);
            RenderingManager.Instance.spriteBatch.DrawStringOutline(2f, Color.Black, Font17px, CompleteMoves, StatsStartPos + new Vector2(90f, 41f), StatColor, 0f,
                CompleteMovesOrigin, StatsScale, SpriteEffects.None, .32f);
            RenderingManager.Instance.spriteBatch.DrawStringOutline(2f, Color.Black, Font17px, CompleteTime, StatsStartPos + new Vector2(90f, 83f), StatColor, 0f,
                CompleteTimeOrigin, StatsScale, SpriteEffects.None, .32f);
        }
    }
}