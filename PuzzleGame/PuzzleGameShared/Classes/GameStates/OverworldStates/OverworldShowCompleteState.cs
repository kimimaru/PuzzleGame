/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class OverworldShowCompleteState : IGameState
    {
        private OverworldState overworldState = null;

        private Color LineIncompleteColor = Color.White;
        private Color LineCompleteColor = Color.White;

        private const double TotalTime = 1000d;
        private double ElapsedTime = 0d;

        private List<OverworldMapNode> NodesCompleted = null;

        private const double MinParticleLife = 300d;
        private const double MaxParticleLife = 300d;
        private const double ParticleStopTime = TotalTime - MaxParticleLife;

        private Texture2D LevelObjTex = null;
        private List<ParticleEngine> Particles = null;
        private bool StoppedParticles = false;

        public OverworldShowCompleteState(OverworldState owState, in Color lineIncompleteColor, in Color lineCompleteColor)
        {
            overworldState = owState;

            LineIncompleteColor = lineIncompleteColor;
            LineCompleteColor = lineCompleteColor;
        }

        public void Enter()
        {
            LevelObjTex = AssetManager.Instance.LoadTexture(ContentGlobals.LevelObjectsTex);

            //Initialize the list of nodes that were just completed
            NodesCompleted = new List<OverworldMapNode>(DataHandler.saveData.Misc.JustFinishedLevels.Count);

            for (int i = 0; i < DataHandler.saveData.Misc.JustFinishedLevels.Count; i++)
            {
                //Grab the index of the node just completed
                int nodeIndex = overworldState.GetNodeIndex(DataHandler.saveData.Misc.JustFinishedLevels[i]);
                if (nodeIndex < 0) continue;

                //Get the node based on this index
                OverworldMapNode completedNode = overworldState.GetNode(nodeIndex);
                if (completedNode == null) continue;

                NodesCompleted.Add(completedNode);
            }

            Particles = new List<ParticleEngine>(8);

            //Set the nodes and their lines to their incomplete states
            for (int i = 0; i < NodesCompleted.Count; i++)
            {
                OverworldMapNode node = NodesCompleted[i];

                ChangeLineColorAndSetupParticles(node.LevelID, node.UpLine, ref node.UplineColor, node.UpNode, LineIncompleteColor, true);
                ChangeLineColorAndSetupParticles(node.LevelID, node.DownLine, ref node.DownlineColor, node.DownNode, LineIncompleteColor, true);
                ChangeLineColorAndSetupParticles(node.LevelID, node.LeftLine, ref node.LeftlineColor, node.LeftNode, LineIncompleteColor, true);
                ChangeLineColorAndSetupParticles(node.LevelID, node.RightLine, ref node.RightlineColor, node.RightNode, LineIncompleteColor, true);

                node.NodeVisual.SetCompleteState(false);
                node.NodeVisual.SetVisualColorLerp(0d);
            }

            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.LevelUnlockedSound), false, .6f, 1f);
        }

        public void Exit()
        {
            LevelObjTex = null;

            NodesCompleted.Clear();
            NodesCompleted = null;

            Particles.Clear();
            Particles = null;
        }

        public void Update()
        {
            for (int i = 0; i < overworldState.Portals.Count; i++)
            {
                overworldState.Portals[i].Update();
            }

            for (int i = 0; i < overworldState.Context.Warps.Count; i++)
            {
                overworldState.Context.Warps[i].Update();
            }

            for (int i = 0; i < overworldState.overworldMap.Nodes.Count; i++)
            {
                overworldState.overworldMap.Nodes[i].Update();
            }

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= ParticleStopTime && StoppedParticles == false)
            {
                for (int i = 0; i < Particles.Count; i++)
                {
                    Particles[i].Stop(false);
                }

                StoppedParticles = true;
            }

            //Update particles
            for (int i = 0; i < Particles.Count; i++)
            {
                Particles[i].Update();
            }

            if (ElapsedTime >= TotalTime)
            {
                bool nextWorldUnlocked = false;

                for (int i = 0; i < NodesCompleted.Count; i++)
                {
                    OverworldMapNode node = NodesCompleted[i];

                    ChangeLineColorAndSetupParticles(node.LevelID, node.UpLine, ref node.UplineColor, node.UpNode, LineCompleteColor, false);
                    ChangeLineColorAndSetupParticles(node.LevelID, node.DownLine, ref node.DownlineColor, node.DownNode, LineCompleteColor, false);
                    ChangeLineColorAndSetupParticles(node.LevelID, node.LeftLine, ref node.LeftlineColor, node.LeftNode, LineCompleteColor, false);
                    ChangeLineColorAndSetupParticles(node.LevelID, node.RightLine, ref node.RightlineColor, node.RightNode, LineCompleteColor, false);

                    node.NodeVisual.SetCompleteState(true);
                    node.NodeVisual.SetVisualColorLerp(1d);

                    //Check if any of the nodes completed is a world node to the next world
                    if (node.NodeType == MapNodeTypes.World && node.WorldID > overworldState.WorldID)
                    {
                        nextWorldUnlocked = true;
                    }
                }

                //First, check if all levels are complete then play the game complete animation if so
                //Next, check if the next world is unlocked then play the world complete animation if so
                //Otherwise, proceed like normal
                if (CheckAllLevelsComplete() == true)
                {
                    overworldState.ChangeState(new AllLevelsCompleteState(overworldState));
                }
                else if (nextWorldUnlocked == true)
                {
                    overworldState.ChangeState(new WorldCompleteState(overworldState));
                }
                else
                {
                    overworldState.ChangeState(new OverworldSelectState(overworldState));
                }

                //Clear the list
                DataHandler.saveData.Misc.JustFinishedLevels.Clear();

                overworldState.UpdateLevelNodeUnlocked();
            }
            else
            {
                double time = ElapsedTime / TotalTime;
                Color lineColor = Interpolation.Interpolate(LineIncompleteColor, LineCompleteColor, time, Interpolation.InterpolationTypes.Linear);

                for (int i = 0; i < NodesCompleted.Count; i++)
                {
                    OverworldMapNode node = NodesCompleted[i];

                    ChangeLineColorAndSetupParticles(node.LevelID, node.UpLine, ref node.UplineColor, node.UpNode, lineColor, false);
                    ChangeLineColorAndSetupParticles(node.LevelID, node.DownLine, ref node.DownlineColor, node.DownNode, lineColor, false);
                    ChangeLineColorAndSetupParticles(node.LevelID, node.LeftLine, ref node.LeftlineColor, node.LeftNode, lineColor, false);
                    ChangeLineColorAndSetupParticles(node.LevelID, node.RightLine, ref node.RightlineColor, node.RightNode, lineColor, false);

                    node.NodeVisual.SetVisualColorLerp(time);
                }
            }
        }

        private void ChangeLineColorAndSetupParticles(in int levelID, Line? line, ref Color origLineColor, int? nodeDir, in Color newColor, in bool shouldAddParticles)
        {
            OverworldMapNode mapNode = overworldState.GetNodeAtDirection(nodeDir);
            if (mapNode == null) return;

            //Only change line colors for incomplete nodes or ones that haven't subsequently been completed - for example, World or Empty nodes
            if (overworldState.IsLevelComplete(mapNode.LevelID) == true && NodesCompleted.ContainsNB(mapNode) == false)
            {
                return;
            }

            //If the line at this node is not null, simply change its color
            if (line != null)
            {
                origLineColor = newColor;

                //Add particles if we should
                if (shouldAddParticles == true)
                {
                    OverworldMapNode thisNode = overworldState.GetNodeAtDirection(levelID);
                    AddParticleEngine(thisNode, mapNode, thisNode.GetDirectionToNode(nodeDir).Value);
                }

                return;
            }

            //Find the direction that leads to this node, if any
            Direction? dirr = mapNode.GetDirectionToNode(levelID);
            if (dirr == null) return;

            Direction dir = dirr.Value;

            //Find the line at this direction, if it's defined
            Line? newLine = mapNode.GetLineForDirection(dir);
            if (newLine == null) return;

            //If that line isn't null, then change its colr
            ref Color newLineColor = ref mapNode.GetLineColorForDirection(dir);
            newLineColor = newColor;

            //Add particles if we should
            if (shouldAddParticles == true)
            {
                AddParticleEngine(mapNode, overworldState.GetNodeAtDirection(levelID), dir);
            }
        }

        private void AddParticleEngine(OverworldMapNode fromNode, OverworldMapNode toNode, in Direction dirTo)
        {
            //Get the halfway point between the two nodes and use that as the position
            Vector2 diff = Interpolation.Interpolate(toNode.Position, fromNode.Position, .5d, Interpolation.InterpolationTypes.Linear);

            //Get the length between the two nodes
            float length = (fromNode.Position - toNode.Position).Length();

            //Use the length as the spread for all the particles
            //A little variation is set for the perpendicular direction (Ex. if the line is vertical, the particles won't spawn fully straight)
            const float perpVariation = 5f;
            Vector2 spread = (dirTo == Direction.Left || dirTo == Direction.Right) ? new Vector2(length / 2, perpVariation) : new Vector2(perpVariation, length / 2);

            Sprite particleSprite = new Sprite(LevelObjTex, ContentGlobals.PortalParticleRect);
            ParticleEngine pathParticles = new ParticleEngine(50, diff, particleSprite, LevelGlobals.BasePortalRenderDepth + .02f, 200d, 300d);
            pathParticles.MinPositionOffset = new Vector2(-spread.X, -spread.Y);
            pathParticles.MaxPositionOffset = spread;
            pathParticles.MinAngularVelocity = pathParticles.MaxAngularVelocity = 0f;
            pathParticles.MinVelocity = new Vector2(0f, -.7f * 60f);
            pathParticles.MaxVelocity = new Vector2(0f, -.9f * 60f);
            pathParticles.MinParticleLife = MinParticleLife;
            pathParticles.MaxParticleLife = MaxParticleLife;
            pathParticles.MinInitScale = Vector2.One * 2f;
            pathParticles.MaxInitScale = Vector2.One * 2f;
            pathParticles.EmissionRate = 5d;
            pathParticles.ParticleColor = Color.White;
            pathParticles.UseColorOverTime = true;
            pathParticles.ColorOverTime = Color.White * 0f;
            pathParticles.PrewarmParticles();

            Particles.Add(pathParticles);
        }

        /// <summary>
        /// Checks if all levels are complete to bring up the game complete screen.
        /// </summary>
        /// <returns></returns>
        private bool CheckAllLevelsComplete()
        {
            foreach (KeyValuePair<int, WorldDescriptionData> kvPair in DataHandler.levelDescData.WorldDescriptions)
            {
                WorldDescriptionData worldDesc = kvPair.Value;
                int worldID = worldDesc.WorldID;

                //If the world ID isn't found, not all levels are complete
                if (SaveData.GetWorldData(worldID, out WorldData worldData) == false)
                {
                    return false;
                }

                for (int i = 0; i < worldDesc.LvlDescriptions.Length; i++)
                {
                    LevelDescriptionData lvlDescData = worldDesc.LvlDescriptions[i];

                    //Get save data for this level; if it doesn't exist, not all levels are complete
                    if (SaveData.GetLevelData(worldID, lvlDescData.LevelID, out LevelData lvlData) == false)
                    {
                        return false;
                    }

                    //Check if the level isn't complete and exit early if so
                    if (lvlData.Complete == false)
                    {
                        return false;
                    }
                }
            }

            //All levels are complete
            return true;
        }

        public void Render()
        {
            //We need a custom render order to fit in the particles

            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, overworldState.camera.TransformMatrix);

            overworldState.DrawMap();

            overworldState.DrawNodes();

            overworldState.DrawSwitches();
            overworldState.DrawWarps();
            overworldState.DrawPortals();

            //Render these particles right above the portals
            for (int i = 0; i < Particles.Count; i++)
            {
                Particles[i].Render();
            }

            for (int i = 0; i < overworldState.Context.RockShooters.Count; i++)
            {
                overworldState.Context.RockShooters[i].Render();
            }

            for (int i = 0; i < overworldState.Context.Blocks.Count; i++)
            {
                overworldState.Context.Blocks[i].Render();
            }

            overworldState.LevelPanel.Render();
            overworldState.Context.Player.Render();

            RenderingManager.Instance.EndCurrentBatch();
        }
    }
}