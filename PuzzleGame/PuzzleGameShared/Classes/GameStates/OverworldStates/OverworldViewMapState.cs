/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class OverworldViewMapState : IGameState
    {
        private OverworldState overworldState = null;

        private Sprite ArrowSprite = null;
        private Vector2 ArrowScale = new Vector2(3f, 3f);
        private const float ArrowScreenOffset = 26f;

        private Vector2 ExitCameraPosition = Vector2.Zero;
        private Vector2 CameraSpeed = new Vector2(5f * 60f, 5f * 60f);

        private const double ArrowBlinkRate = 380d;
        private double ElapsedBlinkTime = 0d;
        private double ElapsedExitTime = 0d;

        private const double ExitTime = 300d;
        private bool Exiting = false;

        public OverworldViewMapState(OverworldState owState)
        {
            overworldState = owState;
        }

        public void Enter()
        {
            ArrowSprite = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.UITex), ContentGlobals.ArrowRect);
            ElapsedExitTime = 0d;
            ElapsedBlinkTime = 0d;

            overworldState.LevelPanel.Reset();
        }

        public void Exit()
        {

        }

        public void Update()
        {
            //Exit view map mode
            if (Exiting == false && (Input.GetButtonDown(InputActions.ViewMap) == true || Input.GetButtonDown(InputActions.BackOut) == true))
            {
                Exiting = true;
                ElapsedExitTime = 0d;
                ExitCameraPosition = overworldState.camera.Position;
            }

            overworldState.Context.Player.AnimationManager.CurrentAnim.Update();

            for (int i = 0; i < overworldState.Portals.Count; i++)
            {
                overworldState.Portals[i].Update();
            }

            for (int i = 0; i < overworldState.Context.Warps.Count; i++)
            {
                overworldState.Context.Warps[i].Update();
            }

            for (int i = 0; i < overworldState.overworldMap.Nodes.Count; i++)
            {
                overworldState.overworldMap.Nodes[i].Update();
            }

            ElapsedBlinkTime = UtilityGlobals.Wrap(ElapsedBlinkTime + Time.ElapsedTime.TotalMilliseconds, 0d, ArrowBlinkRate);

            if (Exiting == false)
            {
                HandleInput();
            }
            else
            {
                ElapsedExitTime += Time.ElapsedTime.TotalMilliseconds;

                if (ElapsedExitTime >= ExitTime)
                {
                    overworldState.camera.LookAtAndClamp(overworldState.Context.Player.transform.Position, overworldState.overworldMap.MapBounds);
                    overworldState.ChangeState(new OverworldSelectState(overworldState));
                }
                else
                {
                    Vector2 pos = Interpolation.Interpolate(ExitCameraPosition, overworldState.Context.Player.transform.Position, ElapsedExitTime / ExitTime,
                        Interpolation.InterpolationTypes.Linear);
                    overworldState.camera.LookAtAndClamp(pos, overworldState.overworldMap.MapBounds);
                }
            }
        }

        private void HandleInput()
        {
            Vector2 speed = Vector2.Zero;

            if (Input.GetButton(InputActions.Up) == true)
            {
                speed.Y = -CameraSpeed.Y;
            }
            if (Input.GetButton(InputActions.Down) == true)
            {
                speed.Y = CameraSpeed.Y;
            }

            if (Input.GetButton(InputActions.Left) == true)
            {
                speed.X = -CameraSpeed.X;
            }
            if (Input.GetButton(InputActions.Right) == true)
            {
                speed.X = CameraSpeed.X;
            }

            speed *= (float)Time.ElapsedTime.TotalSeconds;

            if (speed != Vector2.Zero)
            {
                overworldState.camera.LookAtAndClamp(overworldState.camera.Position + speed, overworldState.overworldMap.MapBounds);
            }
        }

        public void Render()
        {
            overworldState.RenderOverworld();

            //Draw arrows on all 4 sides of the screen if we should
            double visibleTime = ElapsedBlinkTime / (ArrowBlinkRate / 2d);
            if (visibleTime < 1)
            {
                float offset = ArrowScreenOffset;

                //Subtle arrow movement
                //offset -= (int)(ElapsedTime / (ArrowBlinkRate / 4d));

                RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

                Vector2 topPos = new Vector2(RenderingGlobals.BaseResolutionWidth / 2, offset);
                Vector2 bottomPos = new Vector2(topPos.X, RenderingGlobals.BaseResolutionHeight - offset);
                Vector2 leftPos = new Vector2(offset, RenderingGlobals.BaseResolutionHeight / 2);
                Vector2 rightPos = new Vector2(RenderingGlobals.BaseResolutionWidth - offset, leftPos.Y);

                RenderingManager.Instance.DrawSprite(ArrowSprite.Tex, topPos, ArrowSprite.SourceRect, Color.White, 0f, ArrowSprite.GetOrigin(),
                    ArrowScale, SpriteEffects.None, .1f);
                RenderingManager.Instance.DrawSprite(ArrowSprite.Tex, bottomPos, ArrowSprite.SourceRect, Color.White, (float)UtilityGlobals.ToRadians(180d), ArrowSprite.GetOrigin(),
                    ArrowScale, SpriteEffects.None, .1f);
                RenderingManager.Instance.DrawSprite(ArrowSprite.Tex, leftPos, ArrowSprite.SourceRect, Color.White, (float)UtilityGlobals.ToRadians(-90d),
                    ArrowSprite.GetOrigin(), ArrowScale, SpriteEffects.None, .1f);
                RenderingManager.Instance.DrawSprite(ArrowSprite.Tex, rightPos, ArrowSprite.SourceRect, Color.White, (float)UtilityGlobals.ToRadians(90d),
                    ArrowSprite.GetOrigin(), ArrowScale, SpriteEffects.None, .1f);

                RenderingManager.Instance.EndCurrentBatch();
            }
        }
    }
}