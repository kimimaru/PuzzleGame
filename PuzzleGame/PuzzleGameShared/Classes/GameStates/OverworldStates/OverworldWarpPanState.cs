/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class OverworldWarpPanState : IGameState
    {
        private OverworldState overworldState = null;

        private Warp WarpUsed = null;

        private Vector2 PanStartPosition = Vector2.Zero;
        private Vector2 PanEndPosition = Vector2.Zero;

        private double PanTime = 0d;
        private double ElapsedTime = 0d;

        public OverworldWarpPanState(OverworldState owState, Warp warpUsed, in Vector2 panStartPosition, in Vector2 panEndPosition, in double panTime)
        {
            overworldState = owState;
            WarpUsed = warpUsed;

            PanStartPosition = panStartPosition;
            PanEndPosition = panEndPosition;
            PanTime = panTime;
        }

        public void Enter()
        {
            overworldState.LevelPanel.Reset();
        }

        public void Exit()
        {
            WarpUsed.HaltWarpUpdate = false;
            WarpUsed.HasSound = true;
            WarpUsed = null;
        }

        public void Update()
        {
            overworldState.Context.Player.AnimationManager.CurrentAnim.Update();
            
            for (int i = 0; i < overworldState.Portals.Count; i++)
            {
                overworldState.Portals[i].Update();
            }

            for (int i = 0; i < overworldState.Context.Warps.Count; i++)
            {
                overworldState.Context.Warps[i].Update();
            }

            for (int i = 0; i < overworldState.overworldMap.Nodes.Count; i++)
            {
                overworldState.overworldMap.Nodes[i].Update();
            }

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= PanTime)
            {
                overworldState.camera.LookAtAndClamp(PanEndPosition, overworldState.overworldMap.MapBounds);

                overworldState.ChangeState(new OverworldWarpOutState(overworldState, WarpUsed, true));
            }
            else
            {
                Vector2 pos = Interpolation.Interpolate(PanStartPosition, PanEndPosition, ElapsedTime / PanTime, Interpolation.InterpolationTypes.Linear);
                overworldState.camera.LookAtAndClamp(pos, overworldState.overworldMap.MapBounds);
            }
        }

        public void Render()
        {
            overworldState.RenderOverworld();
        }
    }
}