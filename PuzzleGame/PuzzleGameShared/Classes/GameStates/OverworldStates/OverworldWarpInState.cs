/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class OverworldWarpInState : IGameState
    {
        private const double CameraPanTime = 600d;

        private OverworldState overworldState = null;

        private Warp WarpUsed = null;

        private Texture2D Box = null;
        private bool UseFade = false;
        private Color StartFadeColor = Color.Transparent;
        private Color EndFadeColor = Color.Black;
        private double FadeTime = 0d;
        private double ElapsedFadeTime = 0d;

        public OverworldWarpInState(OverworldState owState, Warp warpUsed)
        {
            overworldState = owState;
            WarpUsed = warpUsed;

            UseFade = false;
        }

        public OverworldWarpInState(OverworldState owState, Warp warpUsed, in double fadeTime, in Color startFadeColor, in Color endFadeColor)
            : this(owState, warpUsed)
        {
            UseFade = true;
            FadeTime = fadeTime;
            StartFadeColor = startFadeColor;
            EndFadeColor = endFadeColor;
        }

        public void Enter()
        {
            if (UseFade == true)
            {
                Box = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
                ElapsedFadeTime = 0d;
            }

            WarpUsed.WarpedObjectInEvent -= WarpInFinished;
            WarpUsed.WarpedObjectInEvent += WarpInFinished;

            RectangleF collisionRect = WarpUsed.GetCollisionRectAtWarpPos(overworldState.Context.Player.CollisionRect);

            WarpUsed.WarpObject(overworldState.Context.Player, collisionRect);
        }

        public void Exit()
        {
            WarpUsed.WarpedObjectInEvent -= WarpInFinished;
            WarpUsed = null;
            Box = null;
        }

        public void Update()
        {
            overworldState.Context.Player.AnimationManager.CurrentAnim.Update();

            overworldState.LevelPanel.Update();

            for (int i = 0; i < overworldState.Portals.Count; i++)
            {
                overworldState.Portals[i].Update();
            }

            for (int i = 0; i < overworldState.Context.Warps.Count; i++)
            {
                overworldState.Context.Warps[i].Update();
            }

            for (int i = 0; i < overworldState.overworldMap.Nodes.Count; i++)
            {
                overworldState.overworldMap.Nodes[i].Update();
            }

            if (UseFade == true)
            {
                ElapsedFadeTime += Time.ElapsedTime.TotalMilliseconds;
            }
        }

        private void WarpInFinished(ICollisionObj collisionObj)
        {
            WarpUsed.HaltWarpUpdate = true;
            WarpUsed.HasSound = false;

            //Change to the next overworld if the world IDs dont match up
            if (overworldState.CurNode.WorldID != overworldState.WorldID)
            {
                //Unload all content when entering a new world
                SoundManager.Instance.ClearAllSounds();
                SoundManager.Instance.ClearMusicCache();

                AssetManager.Instance.UnloadLoadedContent();

                TiledMap overworld = AssetManager.Instance.LoadTiledMap(overworldState.CurNode.LevelName);

                //Change to the next overworld
                GameStateManager.Instance.ChangeGameState(new OverworldState(overworld));
            }
            //Otherwise pan the camera over to where the world node takes us
            else
            {
                overworldState.ChangeState(new OverworldWarpPanState(overworldState, WarpUsed, overworldState.CurNode.Position, WarpUsed.WarpPos, CameraPanTime));
            }
        }

        public void Render()
        {
            overworldState.RenderOverworld();

            if (UseFade == true)
            {
                RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

                Color color = Interpolation.Interpolate(StartFadeColor, EndFadeColor, ElapsedFadeTime / FadeTime, Interpolation.InterpolationTypes.Linear);
                RenderingManager.Instance.DrawSprite(Box, Vector2.Zero, ContentGlobals.BoxRect, color, 0f, Vector2.Zero,
                    RenderingGlobals.BaseResolution, SpriteEffects.None, .3f);

                RenderingManager.Instance.EndCurrentBatch();
            }
        }
    }
}