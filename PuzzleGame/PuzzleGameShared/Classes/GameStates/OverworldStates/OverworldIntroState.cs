/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class OverworldIntroState : IGameState
    {
        private OverworldState overworldState = null;

        private Texture2D Box = null;

        private Vector2 PlayerStartPos = Vector2.Zero;

        private Vector2 PlayerStartScale = Vector2.Zero;
        private Vector2 PlayerEndScale = Vector2.Zero;

        private const double CharRotateTime = 37d;

        private double CurRotationTime = CharRotateTime;

        private readonly Direction[] ClockwiseDirections = new Direction[4] { Direction.Down, Direction.Left, Direction.Up, Direction.Right };

        private Direction CurDirection = Direction.Down;

        private const double TotalTime = 600d;
        private double ElapsedTime = 0d;

        /// <summary>
        /// A single frame used to wait out a longer update loop in case the overworld map takes longer than a frame to load.
        /// This allows the intro to play smoothly regardless of how long the load takes.
        /// </summary>
        private bool FrameWait = false;

        public OverworldIntroState(OverworldState owState)
        {
            overworldState = owState;
        }

        public void Enter()
        {
            Box = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);

            PlayerEndScale = overworldState.Context.Player.transform.Scale;

            PlayerStartPos = overworldState.CurNode.Position;

            overworldState.Context.Player.transform.Scale = PlayerStartScale;
            overworldState.Context.Player.transform.Position = PlayerStartPos;
            overworldState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Success);

            CurDirection = ClockwiseDirections[0];

            overworldState.Context.Player.FacingDir = CurDirection;
        }

        public void Exit()
        {
            Box = null;
            overworldState.Context.Player.transform.Scale = PlayerEndScale;
        }

        public void Update()
        {
            //Wait out the frame
            if (FrameWait == false)
            {
                FrameWait = true;

                //Load the world's music track here - prevents issues when unloading music and sounds from prior states
                overworldState.LoadAndPlayOverworldMusic();

                return;
            }

            overworldState.Context.Player.AnimationManager.CurrentAnim.Update();

            for (int i = 0; i < overworldState.Portals.Count; i++)
            {
                overworldState.Portals[i].Update();
            }

            for (int i = 0; i < overworldState.Context.Warps.Count; i++)
            {
                overworldState.Context.Warps[i].Update();
            }

            for (int i = 0; i < overworldState.overworldMap.Nodes.Count; i++)
            {
                overworldState.overworldMap.Nodes[i].Update();
            }

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= TotalTime)
            {
                ElapsedTime = TotalTime;

                overworldState.Context.Player.transform.Scale = PlayerEndScale;
                CurDirection = ClockwiseDirections[0];
                overworldState.Context.Player.FacingDir = CurDirection;
                overworldState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.Idle);

                //If any level was just completed, show the complete animation
                //Otherwise, proceed like normal
                if (DataHandler.saveData.Misc.JustFinishedLevels.Count > 0)
                {
                    overworldState.ChangeState(new OverworldShowCompleteState(overworldState, OverworldState.IncompleteLineColor, OverworldState.CompleteLineColor));
                }
                else
                {
                    overworldState.ChangeState(new OverworldSelectState(overworldState));
                }
            }
            else
            {
                overworldState.Context.Player.transform.Scale = Interpolation.Interpolate(PlayerStartScale, PlayerEndScale,
                    ElapsedTime / TotalTime, Interpolation.InterpolationTypes.CubicOut);

                int fullRotations = (int)(ElapsedTime / (TotalTime / 2));
                CurRotationTime = CharRotateTime * (fullRotations + 1);

                CurDirection = ClockwiseDirections[(int)(ElapsedTime / CurRotationTime) % ClockwiseDirections.Length];
                overworldState.Context.Player.FacingDir = CurDirection;
            }
        }

        public void Render()
        {
            overworldState.RenderOverworld();

            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);

            float boxAlpha = (1f - (float)(ElapsedTime / TotalTime));

            RenderingManager.Instance.DrawSprite(Box, Vector2.Zero, ContentGlobals.BoxRect, Color.Black * boxAlpha, 0f, Vector2.Zero,
                RenderingGlobals.BaseResolution, SpriteEffects.None, .3f);

            RenderingManager.Instance.EndCurrentBatch();
        }
    }
}