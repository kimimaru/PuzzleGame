﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public class KeyboardControlConfigMenu : UIInputMenu
    {
        private readonly Dictionary<string, string> InputNamesReadable = new Dictionary<string, string>(3)
        {
            { InputActions.BackOut, "Back" },
            { InputActions.Select, "Select" },
            { InputActions.ViewMap, "View Map" }
        };

        private readonly string[] InputActionOrder = new string[11]
        {
            InputActions.Up, InputActions.Down, InputActions.Left, InputActions.Right, InputActions.Grab,
            InputActions.Pause, InputActions.Restart, InputActions.Undo, InputActions.BackOut, InputActions.Select, InputActions.ViewMap
        };

        private readonly string[] TimeStrings = new string[5] { "..1", "..2", "..3", "..4", "..5" };

        private readonly int MaxKeyboardIndex = 1;
        private const int StartInputMapIndex = 1;
        private const int NumInputRows = 6;
        private readonly int LastInputGridIndex = 0;

        private SpriteFont HeaderFont = null;
        private SpriteFont ButtonFont = null;
        private SpriteFont ButtonNameFont = null;

        private Texture2D UITex = null;

        private UIMenuOption.OptionSelected OptionSelected = null;
        private UIMenuOption.OptionDeselected OptionDeselected = null;
        private UIMenuOption.OptionChosen ButtonInputChosen = null;
        private UIMenuOption.OptionChanged ButtonInputOptionChanged = null;
        private UIMenuOption.OptionChanged FooterInputOptionChanged = null;

        private readonly Color TextColor = Color.White;
        private readonly Color DeselectedTextColor = new Color(180, 180, 180, 255);
        private readonly Color ButtonColor = new Color(210, 210, 235, 255);
        private readonly Color UnavailableTextColor = new Color(130, 130, 130, 255);

        private readonly Vector2 HeaderPos = new Vector2(0f, -191f);

        private const double ArrowFadeTime = 600d;
        private readonly Vector2 ArrowDist = new Vector2(130f, -3f);
        private readonly Vector2 ArrowScale = new Vector2(2f, 2f);
        private readonly Color ArrowColorStart = Color.White;
        private readonly Color ArrowColorEnd = Color.White * .3f;
        private Color ArrowColor = Color.White;

        private KeyboardMapper KBMapper = null;

        /// <summary>
        /// Temporary config used to store all the data.
        /// </summary>
        private InputConfigData.KeyboardMapData TempConfig = null;

        private UISprite LeftArrow = null;
        private UISprite RightArrow = null;

        private InputHandler KBInputHandler = null;

        private UIText KeyboardIndexText = null;
        private UIText KeyboardNameText = null;

        private List<UIText> InputMapTextList = new List<UIText>(11);
        private List<UIText> ButtonMapTextList = new List<UIText>(11);

        private UIText SaveText = null;
        private UIText DefaultText = null;
        private UIText BackText = null;

        private const int StartingMappableIndex = 0;
        private int CurKeyboardMapIndex = 0;
        private bool IsCurKBIndexMappable = false;

        private readonly string[] KeyboardNames = new string[2] { "Main", "Alternative" };

        private int LastSelectedColumn = 0;

        private double ElapsedFadeTime = 0d;

        public KeyboardControlConfigMenu(in Vector2 position)
            : base(5, position)
        {
            LastInputGridIndex = StartInputMapIndex - 1 + InputActionOrder.Length;

            HeaderFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont24px);
            ButtonFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont15px);
            ButtonNameFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont13px);

            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);

            OptionSelected = OnOptionSelected;
            OptionDeselected = OnOptionDeselected;
            ButtonInputChosen = ChooseInput;
            ButtonInputOptionChanged = OnButtonInputOptionChanged;
            FooterInputOptionChanged = OnFooterOptionChanged;

            CurKeyboardMapIndex = 0;

            KBMapper = new KeyboardMapper(CurKeyboardMapIndex);
            KBMapper.InputRemapCancelledEvent -= OnInputRemapCancelled;
            KBMapper.InputRemapCancelledEvent += OnInputRemapCancelled;
            KBMapper.InputRemappedEvent -= OnInputRemapped;
            KBMapper.InputRemappedEvent += OnInputRemapped;

            KBInputHandler = Input.GetInputHandler(CurKeyboardMapIndex);

            //Copy input mappings to the temp data
            TempConfig = new InputConfigData.KeyboardMapData();
            KBInputHandler.CopyMappingsToDict(TempConfig.MappingData.ActionMappings);

            SetUpHeaders();
            SetUpButtons();

            SetUpRest();
            AddEntries();

            ChangeKeyboardIndex(0, true);
        }

        public override void CleanUp()
        {
            KBMapper.InputRemapCancelledEvent -= OnInputRemapCancelled;
            KBMapper.InputRemappedEvent -= OnInputRemapped;

            KBMapper.CleanUp();
        }

        private string GetKeyToName(in int key)
        {
            if (InputGlobals.KeyNameToText.TryGetValue(key, out string keyName) == false)
            {
                keyName = ((Keys)key).ToString();
            }

            return keyName;
        }

        private string GetReadableInputActionName(in string inputAction)
        {
            if (InputNamesReadable.TryGetValue(inputAction, out string readableName) == false)
            {
                return inputAction;
            }

            return readableName;
        }

        private void SetUpHeaders()
        {
            Rectangle spriteRect = ContentGlobals.ArrowRect;
            Sprite sprite = new Sprite(UITex, spriteRect);

            LeftArrow = new UISprite(sprite);
            LeftArrow.Position = Position + HeaderPos + new Vector2(-spriteRect.Height - ArrowDist.X, ArrowDist.Y);
            LeftArrow.Rotation = UtilityGlobals.ToRadians(-90f);
            LeftArrow.Scale = ArrowScale;

            RightArrow = new UISprite(sprite);
            RightArrow.Position = Position + HeaderPos + new Vector2(spriteRect.Height + ArrowDist.X, ArrowDist.Y);
            RightArrow.Rotation = UtilityGlobals.ToRadians(90f);
            RightArrow.Scale = ArrowScale;

            KeyboardIndexText = UIHelpers.CreateCenterUIText(HeaderFont, string.Empty, Vector2.One, ButtonColor, UITextOptions.Outline, UITextData.Standard);
            KeyboardIndexText.Position = Position + HeaderPos;

            KeyboardNameText = UIHelpers.CreateCenterUIText(ButtonNameFont, string.Empty, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            KeyboardNameText.Position = Position + HeaderPos + new Vector2(0f, 46f);
        }

        private void SetUpButtons()
        {
            Vector2 inputStartPos = Position + new Vector2(-230f, -100f);
            Vector2 mappingOffset = new Vector2(90f, 1f);
            const float mappingOffsetXDiff = 42f;
            Vector2 spacing = new Vector2(260f, 40f);

            for (int i = 0; i < InputActionOrder.Length; i++)
            {
                string action = InputActionOrder[i];
                Vector2 offset = new Vector2((i / NumInputRows) * spacing.X, (i % NumInputRows) * spacing.Y);

                UIText inputText = UIHelpers.CreateUIText(ButtonNameFont, GetReadableInputActionName(action), Vector2.Zero, Vector2.One,
                    UnavailableTextColor, UITextOptions.Outline, UITextData.Standard);
                inputText.Position = inputStartPos + offset;

                InputMapTextList.Add(inputText);

                UIText buttonMapText = UIHelpers.CreateUIText(ButtonNameFont, GetKeyToName(Input.GetInputHandler(CurKeyboardMapIndex).GetMappingVal(action).InputVal),
                    Vector2.Zero, Vector2.One, ButtonColor, UITextOptions.Outline, UITextData.Standard);
                buttonMapText.Position = inputStartPos + new Vector2(mappingOffset.X + ((i / NumInputRows) * mappingOffsetXDiff), mappingOffset.Y) + offset;

                ButtonMapTextList.Add(buttonMapText);
            }
        }

        private void SetUpRest()
        {
            SaveText = UIHelpers.CreateUIText(ButtonFont, "Save", new Vector2(1f, .5f), Vector2.One, DeselectedTextColor, UITextOptions.Outline, UITextData.Standard);
            SaveText.Position = Position + new Vector2(-20f, RenderingGlobals.BaseResHeightHalved - 80f);
            
            DefaultText = UIHelpers.CreateUIText(ButtonFont, "Defaults", new Vector2(0f, .5f), Vector2.One, DeselectedTextColor, UITextOptions.Outline, UITextData.Standard);
            DefaultText.Position = Position + new Vector2(20f, RenderingGlobals.BaseResHeightHalved - 80f);

            BackText = UIHelpers.CreateCenterUIText(ButtonFont, "Back", Vector2.One, DeselectedTextColor, UITextOptions.Outline, UITextData.Standard);
            BackText.Position = Position + new Vector2(0f, RenderingGlobals.BaseResHeightHalved - 40f);
        }

        private void AddEntries()
        {
            AddMenuElement(KeyboardIndexText, new UIMenuOption(OnHeaderOptionSelected, OptionDeselected, null, ChangeKeyboardIndexWrapper));

            for (int i = 0; i < InputMapTextList.Count; i++)
            {
                AddMenuElement(InputMapTextList[i], new UIMenuOption(OptionSelected, OptionDeselected, ButtonInputChosen, ButtonInputOptionChanged));
            }

            AddMenuElement(SaveText, new UIMenuOption(OptionSelected, OptionDeselected, SaveInputs, FooterInputOptionChanged));
            AddMenuElement(DefaultText, new UIMenuOption(OptionSelected, OptionDeselected, DefaultInputs, FooterInputOptionChanged));
            AddMenuElement(BackText, new UIMenuOption(OptionSelected, OptionDeselected, Return, null));

            for (int i = 0; i < ButtonMapTextList.Count; i++)
            {
                AddElement(ButtonMapTextList[i]);
            }

            AddElement(KeyboardNameText);

            AddElement(LeftArrow);
            AddElement(RightArrow);
        }

        private bool SelectionInInputGrid(in int selection)
        {
            return (selection >= StartInputMapIndex && selection <= LastInputGridIndex);
        }

        private void OnHeaderOptionSelected(in int optionIndex)
        {
            KeyboardIndexText.TextOptions = (UITextOptions)EnumUtility.AddEnumVal((long)KeyboardIndexText.TextOptions, (long)UITextOptions.Outline);
            KeyboardIndexText.TintColor = ButtonColor;

            ChangeKeyboardIndex(0, false);
        }

        private void OnOptionSelected(in int optionIndex)
        {
            Elements[optionIndex].TintColor = TextColor;
        }

        private void OnOptionDeselected(in int optionIndex)
        {
            Elements[optionIndex].TintColor = DeselectedTextColor;

            if (optionIndex == 0)
            {
                ElapsedFadeTime = 0d;
                LeftArrow.Visible = false;
                RightArrow.Visible = false;
            }
        }

        private void ChangeKeyboardIndexWrapper(in int amount)
        {
            ChangeKeyboardIndex(amount, false);
        }

        private void ChangeKeyboardIndex(in int amount, in bool forceUpdate)
        {
            int prevKBIndex = CurKeyboardMapIndex;

            CurKeyboardMapIndex = UtilityGlobals.Clamp(CurKeyboardMapIndex + amount, 0, MaxKeyboardIndex);

            KeyboardIndexText.Text = "Keyboard " + (CurKeyboardMapIndex + 1);

            KBInputHandler = Input.GetInputHandler(CurKeyboardMapIndex);
            KeyboardNameText.Text = KeyboardNames[CurKeyboardMapIndex];
            KeyboardNameText.TintColor = TextColor;

            //If the index is different, copy the mapped data to the temp config
            if (prevKBIndex != CurKeyboardMapIndex || forceUpdate == true)
            {
                if (DataHandler.inputConfigData.KBMapData.TryGetValue(CurKeyboardMapIndex, out InputConfigData.KeyboardMapData kbMapData) == true)
                {
                    TempConfig.CopyFrom(kbMapData);
                }
                else
                {
                    InputHandler inputHandler = Input.GetInputHandler(CurKeyboardMapIndex);
                    if (inputHandler != null)
                    {
                        inputHandler.CopyMappingsToDict(TempConfig.MappingData.ActionMappings);
                    }
                    //If there's somehow no input handler at that index, use defaults
                    else
                    {
                        TempConfig.MappingData.ActionMappings.CopyDictionaryData(InputGlobals.DefaultMappings[InputHandler.InputTypes.Keyboard]);
                    }
                }

                //Also update all the buttons
                for (int i = 0; i < ButtonMapTextList.Count; i++)
                {
                    ButtonMapTextList[i].Text = GetKeyToName(TempConfig.MappingData.ActionMappings[InputActionOrder[i]]);
                }
            }

            //Check if this index is mappable
            IsCurKBIndexMappable = (CurKeyboardMapIndex >= StartingMappableIndex);

            //Update the keyboard index
            TempConfig.KeyboardIndex = CurKeyboardMapIndex;

            if (IsCurKBIndexMappable == false)
            {
                //Go back to the top if this index isn't mappable - disallow mapping new inputs
                if (CurSelection != 0 && CurSelection != MaxSelections - 1)
                {
                    SetSelection(0, false);
                }

                for (int i = 0; i < InputMapTextList.Count; i++)
                {
                    InputMapTextList[i].TintColor = UnavailableTextColor;
                }

                SaveText.TintColor = UnavailableTextColor;
                DefaultText.TintColor = UnavailableTextColor;
            }
            else
            {
                //Update text colors
                for (int i = 0; i < InputMapTextList.Count; i++)
                {
                    InputMapTextList[i].TintColor = DeselectedTextColor;
                }

                SaveText.TintColor = DeselectedTextColor;
                DefaultText.TintColor = DeselectedTextColor;

                if (CurSelection > 0)
                {
                    Elements[CurSelection].TintColor = TextColor;
                }
            }

            LeftArrow.Visible = true;
            RightArrow.Visible = true;

            if (CurKeyboardMapIndex == 0)
            {
                LeftArrow.Visible = false;
            }
            if (CurKeyboardMapIndex == MaxKeyboardIndex)
            {
                RightArrow.Visible = false;
            }
        }

        private void OnButtonInputOptionChanged(in int amount)
        {
            if (amount == 0)
                return;

            //Find how many columns there are
            int columns = (int)(InputActionOrder.Length / NumInputRows);

            //Find out which column this selection is on
            int curColumn = (CurSelection - StartInputMapIndex) / NumInputRows;

            if (amount < 0)
            {
                //Wrap around to the last column if it's the first column
                if (curColumn == 0)
                {
                    int newSelection = UtilityGlobals.Clamp(CurSelection + (columns * NumInputRows), StartInputMapIndex, LastInputGridIndex);
                    SetSelection(newSelection, false);
                }
                else
                {
                    SetSelection(CurSelection - NumInputRows, false);
                }
            }
            else if (amount > 0)
            {
                //Wrap to the first column if it's the last column
                if (curColumn == columns)
                {
                    int newSelection = UtilityGlobals.Clamp(CurSelection - (columns * NumInputRows), StartInputMapIndex, LastInputGridIndex);
                    SetSelection(newSelection, false);
                }
                else
                {
                    SetSelection(UtilityGlobals.Clamp(CurSelection + NumInputRows, StartInputMapIndex, LastInputGridIndex), false);
                }
            }

            LastSelectedColumn = (CurSelection - StartInputMapIndex) / NumInputRows;
        }

        private void OnFooterOptionChanged(in int amount)
        {
            if (amount == 0)
                return;

            int newSelection = UtilityGlobals.Wrap(CurSelection + amount, LastInputGridIndex + 1, MaxSelections - 2);
            SetSelection(newSelection, false);

            LastSelectedColumn = ((CurSelection - LastInputGridIndex - StartInputMapIndex) * NumInputRows) / NumInputRows; //(CurSelection == (MaxSelections - 2)) ? 1 : 0;
        }

        private void ChooseInput()
        {
            KBMapper.StartMapInput(InputActionOrder[CurSelection - StartInputMapIndex], CurKeyboardMapIndex);
        }

        private void SaveInputs()
        {
            KBInputHandler.CopyMappingsFromDict(TempConfig.MappingData.ActionMappings);
            DataHandler.inputConfigData.AddOrChangeKeyboardMappings(CurKeyboardMapIndex, TempConfig);
            DataHandler.SaveInputConfigData();
        }

        private void DefaultInputs()
        {
            TempConfig.MappingData.ActionMappings.CopyDictionaryData(InputGlobals.DefaultMappings[InputHandler.InputTypes.Keyboard]);
        
            for (int i = 0; i < ButtonMapTextList.Count; i++)
            {
                ButtonMapTextList[i].Text = GetKeyToName(TempConfig.MappingData.ActionMappings[InputActionOrder[i]]);
            }
        }

        private void Return()
        {
            BackOut();
        }

        protected override void HandleSelectionInput()
        {
            if (Input.GetButtonDown(InputActions.Select) == true)
            {
                UIMenuOption option = MenuOptions[CurSelection];
                if (option.OnOptionChosen != null)
                {
                    option.OnOptionChosen();

                    if (OptionChosenSound?.IsDisposed == false)
                    {
                        bool playSound = true;

                        //Check if within the grid and don't play the sound if the keyboard at this index isn't mappable
                        if (SelectionInInputGrid(CurSelection) == true)
                        {
                            playSound = IsCurKBIndexMappable;
                        }

                        if (playSound == true)
                        {
                            SoundManager.Instance.PlaySound(OptionChosenSound, false, OptionChosenVolume);
                        }
                    }
                }
            }
            else if (Input.GetButtonDown(InputActions.BackOut) == true)
            {
                BackOut();

                if (OptionBackSound?.IsDisposed == false)
                {
                    SoundManager.Instance.PlaySound(OptionBackSound, false, OptionBackVolume);
                }
            }
        }

        protected override void HandleCursorInput()
        {
            if (Input.GetButtonDown(InputActions.Left) == true)
            {
                ChangeOption(-OptionChangeAmount, true);
            }
            else if (Input.GetButtonDown(InputActions.Right) == true)
            {
                ChangeOption(OptionChangeAmount, true);
            }
            else if (Input.GetButtonDown(InputActions.Down) == true)
            {
                ChangeSelectionWrapper(OptionSelectionAmount, true);
            }
            else if (Input.GetButtonDown(InputActions.Up) == true)
            {
                ChangeSelectionWrapper(-OptionSelectionAmount, true);
            }
        }

        private void ChangeSelectionWrapper(in int amount, in bool playSound)
        {
            if (amount < 0)
            {
                //Disallow wrapping around
                if (CurSelection == 0)
                {
                    return;
                }

                //Go to the top if this keyboard isn't mappable
                if (CurSelection == (MaxSelections - 1) && IsCurKBIndexMappable == false)
                {
                    ChangeSelection(-CurSelection, playSound);
                    return;
                }

                if (SelectionInInputGrid(CurSelection) == true)
                {
                    int row = (CurSelection - StartInputMapIndex) % NumInputRows;

                    //Go to the top if on the top row
                    if (row == 0)
                    {
                        ChangeSelection(-CurSelection, playSound);
                        return;
                    }
                }
                //Go to the last selected column on the final row
                else if (CurSelection < (MaxSelections - 1))
                {
                    int newSelection = (StartInputMapIndex + (NumInputRows - 1)) + (LastSelectedColumn * NumInputRows);

                    //If the new selection isn't in the input grid, go to the last input selection
                    if (SelectionInInputGrid(newSelection) == false)
                    {
                        newSelection = LastInputGridIndex;
                    }

                    SetSelection(newSelection, true);

                    return;
                }
                else if (CurSelection == (MaxSelections - 1))
                {
                    int newSelection = UtilityGlobals.Clamp(LastInputGridIndex + 1 + LastSelectedColumn, LastInputGridIndex + 1, MaxSelections - 2);

                    SetSelection(newSelection, playSound);
                    return;
                }
            }
            else if (amount > 0)
            {
                //Disallow wrapping from back to the keyboard
                if (CurSelection == MaxSelections - 1)
                {
                    return;
                }

                //Go to the bottom if this keyboard isn't mappable
                if (CurSelection == 0 && IsCurKBIndexMappable == false)
                {
                    ChangeSelection(MaxSelections - 1, playSound);
                    return;
                }

                if (SelectionInInputGrid(CurSelection) == true)
                {
                    int row = (CurSelection - StartInputMapIndex) % NumInputRows;

                    //Go to the bottom if on the bottom row or last selection in the input grid
                    if (row == (NumInputRows - 1) || CurSelection == LastInputGridIndex)
                    {
                        int selection = UtilityGlobals.Clamp(LastInputGridIndex + 1 + LastSelectedColumn, LastInputGridIndex + 1, MaxSelections - 2);

                        SetSelection(selection, playSound);
                        return;
                    }
                }
                //Go to the last selected column on the first row
                else if (CurSelection == 0)
                {
                    int newSelection = StartInputMapIndex + (LastSelectedColumn * NumInputRows);
                    SetSelection(newSelection, playSound);
                    return;
                }
                else if (CurSelection >= (LastInputGridIndex + 1) && CurSelection < (MaxSelections - 1))
                {
                    SetSelection(MaxSelections - 1, playSound);
                    return;
                }
            }

            ChangeSelection(amount, playSound);
        }

        public override void TransitionUpdate()
        {
            base.TransitionUpdate();

            UpdateScreen();
        }

        protected override void OnMenuBack()
        {
            base.OnMenuBack();

            LastSelectedColumn = 0;

            //When going back, reset all unsaved inputs
            if (DataHandler.inputConfigData.KBMapData.TryGetValue(CurKeyboardMapIndex, out InputConfigData.KeyboardMapData kbMapData) == true)
            {
                TempConfig.CopyFrom(kbMapData);
            }
            else
            {
                InputHandler inputHandler = Input.GetInputHandler(CurKeyboardMapIndex);

                if (inputHandler != null)
                {
                    inputHandler.CopyMappingsToDict(TempConfig.MappingData.ActionMappings);
                }
                else
                {
                    TempConfig.MappingData.ActionMappings.CopyDictionaryData(InputGlobals.DefaultMappings[InputHandler.InputTypes.Keyboard]);
                }
            }

            //Update all the buttons
            for (int i = 0; i < ButtonMapTextList.Count; i++)
            {
                ButtonMapTextList[i].Text = GetKeyToName(TempConfig.MappingData.ActionMappings[InputActionOrder[i]]);
            }
        }

        private void OnInputRemapCancelled(in string inputAction)
        {
            ButtonMapTextList[CurSelection - StartInputMapIndex].Text = GetKeyToName(TempConfig.MappingData.ActionMappings[inputAction]);
        }

        private void OnInputRemapped(in string actionRemapped, in int buttonMapped)
        {
            TempConfig.MappingData.ActionMappings[actionRemapped] = buttonMapped;

            ButtonMapTextList[CurSelection - StartInputMapIndex].Text = GetKeyToName(TempConfig.MappingData.ActionMappings[actionRemapped]);
        }

        private void UpdateScreen()
        {
            //Update arrows
            if (CurSelection == 0)
            {
                ElapsedFadeTime += Time.ElapsedTime.TotalMilliseconds;

                double time = UtilityGlobals.PingPong(ElapsedFadeTime / (ArrowFadeTime / 2), 0d, 1d);
                ArrowColor = Interpolation.Interpolate(ArrowColorStart, ArrowColorEnd, time, Interpolation.InterpolationTypes.Linear);

                LeftArrow.TintColor = ArrowColor;
                RightArrow.TintColor = ArrowColor;
            }
        }

        public override void Update()
        {
            if (KBMapper.IsActive == true)
            {
                int timeIndex = UtilityGlobals.Clamp((int)(KBMapper.InputTimeRemaining / Time.MsPerS), 0, TimeStrings.Length - 1);
            
                ButtonMapTextList[CurSelection - StartInputMapIndex].Text = TimeStrings[timeIndex];
            
                KBMapper.Update();
            }
            else
            {
                base.Update();

                UpdateScreen();
            }
        }

        public override void Render()
        {
            if (LeftArrow.Visible == true)
                LeftArrow.Render();
            if (RightArrow.Visible == true)
                RightArrow.Render();

            KeyboardIndexText.Render();
            KeyboardNameText.Render();

            for (int i = 0; i < InputMapTextList.Count; i++)
                InputMapTextList[i].Render();

            for (int i = 0; i < ButtonMapTextList.Count; i++)
                ButtonMapTextList[i].Render();

            SaveText.Render();
            DefaultText.Render();
            BackText.Render();
        }
    }
}
