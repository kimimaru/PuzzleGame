﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public class OptionsMenuNew : UIInputMenu
    {
        /// <summary>
        /// The graphics options for resolution.
        /// This is static to allow fetching and setting the available resolutions from anywhere.
        /// </summary>
        public static readonly UIMenuOption.OptionChosen[] GraphicsOptions = new UIMenuOption.OptionChosen[3];
        public static readonly UIMenuOption.OptionChosen FullscreenOption = null;
        private readonly string[] GraphicsOptionText = new string[] { "100%", "200%", "300%" };
        private int CurGraphicsOption = 0;

        private SpriteFont Font;

        private UIMenuOption.OptionSelected OptionSelected = null;
        private UIMenuOption.OptionDeselected OptionDeselected = null;

        private readonly Color TextColor = Color.White;// new Color(0, 140, 0, 255);
        private readonly Color DeselectedTextColor = new Color(180, 180, 180, 255);

        private const int SettingTextIndexOffset = 10;

        //private ControlConfigMenu ControlMenu = null;
        private UIMenuManager MenuManager = null;

        static OptionsMenuNew()
        {
            GraphicsOptions[0] = Res1x;
            GraphicsOptions[1] = Res2x;
            GraphicsOptions[2] = Res3x;

            FullscreenOption = ResFullscreen;
        }

        public OptionsMenuNew(UIMenuManager menuManager, in Vector2 position, in Vector2 spacing, SpriteFont font)
            : base(5, position)
        {
            MenuManager = menuManager;
            Font = font;

            CurGraphicsOption = UtilityGlobals.Clamp(DataHandler.saveData.Settings.ResOption, 0, GraphicsOptions.Length - 1);

            OptionSelected = OnOptionSelected;
            OptionDeselected = OnOptionDeselected;
            
            InitMenuOptions(spacing, font);

            //Manually call these to set the strings
            OnGraphicsOptionChanged(0);
            OnFullscreenOptionChanged(0);
            OnTimestepChanged(0);
            OnVSyncChanged(0);
            OnMusicVolumeChanged(0);
            OnSoundVolumeChanged(0);
            OnGrabOptionChanged(0);
            OnInputOptionChanged(0);
            OnHUDOptionChanged(0);
        }

        private void InitMenuOptions(in Vector2 spacing, SpriteFont font)
        {
            Vector2 origin = new Vector2(0f, .5f);
            Vector2 initOffset = new Vector2(-160f, 0f);

            AddMenuElementRelative(UIHelpers.CreateUIText(font, "Music Volume", origin, Vector2.One, UITextOptions.Outline,
                UITextData.Standard), (spacing * 0f) + initOffset, new UIMenuOption(OptionSelected, OptionDeselected, null, OnMusicVolumeChanged));
            AddMenuElementRelative(UIHelpers.CreateUIText(font, "Sound Volume", origin, Vector2.One, UITextOptions.Outline,
                UITextData.Standard), (spacing * 1f) + initOffset, new UIMenuOption(OptionSelected, OptionDeselected, null, OnSoundVolumeChanged));
            AddMenuElementRelative(UIHelpers.CreateUIText(font, "Block Grab", origin, Vector2.One, UITextOptions.Outline,
                UITextData.Standard), (spacing * 2f) + initOffset, new UIMenuOption(OptionSelected, OptionDeselected, null, OnGrabOptionChanged));
            AddMenuElementRelative(UIHelpers.CreateUIText(font, "Input Buffer", origin, Vector2.One, UITextOptions.Outline,
                UITextData.Standard), (spacing * 3f) + initOffset, new UIMenuOption(OptionSelected, OptionDeselected, null, OnInputOptionChanged));
            AddMenuElementRelative(UIHelpers.CreateUIText(font, "HUD Display", origin, Vector2.One, UITextOptions.Outline,
                UITextData.Standard), (spacing * 4f) + initOffset, new UIMenuOption(OptionSelected, OptionDeselected, null, OnHUDOptionChanged));

            Vector2 offset = new Vector2(0f, 10f) + initOffset;

            AddMenuElementRelative(UIHelpers.CreateUIText(font, "Resolution", origin, Vector2.One, UITextOptions.Outline,
                UITextData.Standard), (spacing * 5f) + offset, new UIMenuOption(OptionSelected, OptionDeselected, null, OnGraphicsOptionChanged));
            AddMenuElementRelative(UIHelpers.CreateUIText(font, "Fullscreen", origin, Vector2.One, UITextOptions.Outline,
                UITextData.Standard), (spacing * 6f) + offset, new UIMenuOption(OptionSelected, OptionDeselected, null, OnFullscreenOptionChanged));
            AddMenuElementRelative(UIHelpers.CreateUIText(font, "Framerate", origin, Vector2.One, UITextOptions.Outline,
                UITextData.Standard), (spacing * 7f) + offset, new UIMenuOption(OptionSelected, OptionDeselected, null, OnTimestepChanged));
            AddMenuElementRelative(UIHelpers.CreateUIText(font, "VSync", origin, Vector2.One, UITextOptions.Outline,
                UITextData.Standard), (spacing * 8f) + offset, new UIMenuOption(OptionSelected, OptionDeselected, null, OnVSyncChanged));

            AddMenuElementRelative(UIHelpers.CreateUIText(font, "Back", origin, Vector2.One, UITextOptions.Outline,
                UITextData.Standard), (spacing * 9f) + offset + new Vector2(0f, 10f), new UIMenuOption(OptionSelected, OptionDeselected, Return, null));

            Vector2 optionOffset = new Vector2(240f, 0f);

            UIText musicVolText = UIHelpers.CreateUIText(font, string.Empty, origin, Vector2.One, UITextOptions.Outline, UITextData.Standard);
            AddElement(musicVolText, Elements[0].Position + optionOffset);
            UIText soundVolText = UIHelpers.CreateUIText(font, string.Empty, origin, Vector2.One, UITextOptions.Outline, UITextData.Standard);
            AddElement(soundVolText, Elements[1].Position + optionOffset);
            UIText grabSettingText = UIHelpers.CreateUIText(font, string.Empty, origin, Vector2.One, UITextOptions.Outline, UITextData.Standard);
            AddElement(grabSettingText, Elements[2].Position + optionOffset);

            UIText inputSettingText = UIHelpers.CreateUIText(font, string.Empty, origin, Vector2.One, UITextOptions.Outline, UITextData.Standard);
            AddElement(inputSettingText, Elements[3].Position + optionOffset);

            UIText hudOption = UIHelpers.CreateUIText(font, string.Empty, origin, Vector2.One, UITextOptions.Outline, UITextData.Standard);
            AddElement(hudOption, Elements[4].Position + optionOffset);

            UIText gfxRes = UIHelpers.CreateUIText(font, string.Empty, origin, Vector2.One, UITextOptions.Outline, UITextData.Standard);
            AddElement(gfxRes, Elements[5].Position + optionOffset);

            UIText fullscreenRes = UIHelpers.CreateUIText(font, string.Empty, origin, Vector2.One, UITextOptions.Outline, UITextData.Standard);
            AddElement(fullscreenRes, Elements[6].Position + optionOffset);

            UIText gfxTimestep = UIHelpers.CreateUIText(font, string.Empty, origin, Vector2.One, UITextOptions.Outline, UITextData.Standard);
            AddElement(gfxTimestep, Elements[7].Position + optionOffset);

            UIText gfxVSync = UIHelpers.CreateUIText(font, string.Empty, origin, Vector2.One, UITextOptions.Outline, UITextData.Standard);
            AddElement(gfxVSync, Elements[8].Position + optionOffset);
        }

        private void OnGraphicsOptionChanged(in int amount)
        {
            CurGraphicsOption = UtilityGlobals.Clamp(CurGraphicsOption + amount, 0, GraphicsOptions.Length - 1);

            bool callMethod = (DataHandler.saveData.Settings.ResOption != CurGraphicsOption);
            DataHandler.saveData.Settings.ChangeResOption(CurGraphicsOption);

            ((UIText)Elements[5 + SettingTextIndexOffset]).Text = GraphicsOptionText[CurGraphicsOption];

            if (callMethod == true)
            {
                //If any of these are called, set fullscreen off if it isn't already
                //This will invoke the correct graphics option, since fullscreen handles doing so when turned off
                if (DataHandler.saveData.Settings.FullscreenOption == (int)FullscreenSettings.On)
                {
                    OnFullscreenOptionChanged(-1);
                }
                else
                {
                    GraphicsOptions[CurGraphicsOption].Invoke();
                }
            }
        }

        private void OnFullscreenOptionChanged(in int amount)
        {
            int fullscreen = DataHandler.saveData.Settings.FullscreenOption;

            fullscreen = UtilityGlobals.Clamp(fullscreen + amount, 0, (int)FullscreenSettings.On);

            bool callMethod = (DataHandler.saveData.Settings.FullscreenOption != fullscreen);
            DataHandler.saveData.Settings.ChangeFullscreenOption(fullscreen);

            ((UIText)Elements[6 + SettingTextIndexOffset]).Text = ((FullscreenSettings)DataHandler.saveData.Settings.FullscreenOption).ToString();

            if (callMethod == true)
            {
                //If fullscreen is on, invoke it, otherwise invoke the current resolution option
                if (DataHandler.saveData.Settings.FullscreenOption == (int)FullscreenSettings.On)
                {
                    FullscreenOption.Invoke();
                }
                else
                {
                    GraphicsOptions[CurGraphicsOption].Invoke();
                }
            }
        }

        private void OnTimestepChanged(in int amount)
        {
            int timeStep = DataHandler.saveData.Settings.Timestep;
            if (amount > 0)
            {
                timeStep = (int)TimestepSettings.Variable;
            }
            else if (amount < 0)
            {
                timeStep = (int)TimestepSettings.Fixed;
            }

            Time.TimeStep = (TimestepSettings)timeStep;
            DataHandler.saveData.Settings.ChangeTimestep(timeStep);

            ((UIText)Elements[7 + SettingTextIndexOffset]).Text = ((TimestepSettings)DataHandler.saveData.Settings.Timestep).ToString();
        }

        private void OnVSyncChanged(in int amount)
        {
            int vSyncSetting = DataHandler.saveData.Settings.VSync;

            if (amount > 0)
            {
                vSyncSetting = (int)VSyncSettings.Disabled;
            }
            else if (amount < 0)
            {
                vSyncSetting = (int)VSyncSettings.Enabled;
            }

            Time.VSyncSetting = (VSyncSettings)vSyncSetting;
            DataHandler.saveData.Settings.ChangeVSync(vSyncSetting);

            ((UIText)Elements[8 + SettingTextIndexOffset]).Text = ((VSyncSettings)DataHandler.saveData.Settings.VSync).ToString();
        }

        private void OnMusicVolumeChanged(in int amount)
        {
            SoundManager.Instance.MusicVolume += (.1f * amount);

            SoundManager.Instance.MusicVolume = (float)Math.Round(SoundManager.Instance.MusicVolume, 1);

            if (UtilityGlobals.IsApproximate(SoundManager.Instance.MusicVolume, DataHandler.saveData.Settings.MusicVolume, .01f) == false)
            {
                DataHandler.saveData.Settings.ChangeMusicVolume(SoundManager.Instance.MusicVolume);
            }

            ((UIText)Elements[0 + SettingTextIndexOffset]).Text = ((int)Math.Round(SoundManager.Instance.MusicVolume * 10f)).ToString();
        }

        private void OnSoundVolumeChanged(in int amount)
        {
            SoundManager.Instance.SoundVolume += (.1f * amount);

            SoundManager.Instance.SoundVolume = (float)Math.Round(SoundManager.Instance.SoundVolume, 1);

            if (UtilityGlobals.IsApproximate(SoundManager.Instance.SoundVolume, DataHandler.saveData.Settings.SoundVolume, .01f) == false)
            {
                DataHandler.saveData.Settings.ChangeSoundVolume(SoundManager.Instance.SoundVolume);
            }

            ((UIText)Elements[1 + SettingTextIndexOffset]).Text = ((int)Math.Round(SoundManager.Instance.SoundVolume * 10f)).ToString();
        }

        private void OnGrabOptionChanged(in int amount)
        {
            int grabSetting = DataHandler.saveData.Settings.GrabOption;

            if (amount > 0)
            {
                grabSetting = UtilityGlobals.Clamp(grabSetting + amount, 0, (int)GrabSettings.Auto);
            }
            else if (amount < 0)
            {
                grabSetting = UtilityGlobals.Clamp(grabSetting + amount, 0, (int)GrabSettings.Auto);
            }

            DataHandler.saveData.Settings.ChangeGrabOption(grabSetting);

            ((UIText)Elements[2 + SettingTextIndexOffset]).Text = ((GrabSettings)DataHandler.saveData.Settings.GrabOption).ToString();
        }

        private void OnInputOptionChanged(in int amount)
        {
            int inputSetting = DataHandler.saveData.Settings.InputOption;

            if (amount != 0)
            {
                inputSetting = UtilityGlobals.Clamp(inputSetting + amount, 0, (int)PlayerInputPriorityOptions.Buffered);
            }

            DataHandler.saveData.Settings.ChangeInputOption(inputSetting);

            string val = "Off";
            if (DataHandler.saveData.Settings.InputOption == (int)PlayerInputPriorityOptions.Buffered)
            {
                val = "On";
            }

            ((UIText)Elements[3 + SettingTextIndexOffset]).Text = val;
        }

        private void OnHUDOptionChanged(in int amount)
        {
            int hudSetting = DataHandler.saveData.Settings.HUDDisplaySetting;

            if (amount != 0)
            {
                hudSetting = UtilityGlobals.Clamp(hudSetting + amount, 0, (int)HUDDisplayOptions.All);
            }

            DataHandler.saveData.Settings.ChangeHUDSetting(hudSetting);

            ((UIText)Elements[4 + SettingTextIndexOffset]).Text = ((HUDDisplayOptions)DataHandler.saveData.Settings.HUDDisplaySetting).ToString();
        }

        private void OnOptionSelected(in int optionIndex)
        {
            Elements[optionIndex].TintColor = TextColor;

            int settingTextIndex = optionIndex + SettingTextIndexOffset;

            if (settingTextIndex < Elements.Count)
            {
                UIText settingText = (UIText)Elements[settingTextIndex];
                settingText.TextOptions = (UITextOptions)EnumUtility.AddEnumVal((long)settingText.TextOptions, (long)UITextOptions.Outline);
                settingText.TintColor = TextColor;
            }
        }

        private void OnOptionDeselected(in int optionIndex)
        {
            Elements[optionIndex].TintColor = DeselectedTextColor;

            int settingTextIndex = optionIndex + SettingTextIndexOffset;

            if (settingTextIndex < Elements.Count)
            {
                UIText settingText = (UIText)Elements[settingTextIndex];
                settingText.TextOptions = (UITextOptions)EnumUtility.AddEnumVal((long)settingText.TextOptions, (long)UITextOptions.Outline);
                settingText.TintColor = DeselectedTextColor;
            }
        }

        private void PlaySelectSound()
        {
            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionChangeSound), false);
        }

        private void OnBackOutConfig()
        {
            UIInputMenu prevMenu = MenuManager.Pop();
            UIInputMenu curMenu = MenuManager.Peek();

            MenuTransition transition = MenuTransition.CreateTransitionDir(prevMenu, curMenu, Direction.Left, 200d);

            MenuManager.SetTransition(transition);
        }

        private void Return()
        {
            BackOut();
        }

        private static void Res1x()
        {
            RenderingManager.Instance.SetFullScreen(false);
            RenderingManager.Instance.SetWindowBorderless(false);

            RenderingManager.Instance.ResizeWindow(RenderingGlobals.BaseResolution, true);
            RenderingManager.Instance.CenterWindow();
        }

        private static void Res2x()
        {
            RenderingManager.Instance.SetFullScreen(false);
            RenderingManager.Instance.SetWindowBorderless(false);

            RenderingManager.Instance.ResizeWindow(RenderingGlobals.BaseResolution * 2, true);
            RenderingManager.Instance.CenterWindow();
        }

        private static void Res3x()
        {
            RenderingManager.Instance.SetFullScreen(false);
            RenderingManager.Instance.SetWindowBorderless(false);

            RenderingManager.Instance.ResizeWindow(RenderingGlobals.BaseResolution * 3, true);
            RenderingManager.Instance.CenterWindow();
        }

        private static void ResFullscreen()
        {
            RenderingManager.Instance.SetFullScreen(true);
            RenderingManager.Instance.SetWindowBorderless(true);

            RenderingManager.Instance.ResizeWindow(RenderingManager.Instance.ScreenSize, true);
            RenderingManager.Instance.CenterWindow();
        }
    }
}
