﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public class GamepadControlConfigMenu : UIInputMenu
    {
        private readonly Dictionary<string, string> InputNamesReadable = new Dictionary<string, string>(3)
        {
            { InputActions.BackOut, "Back" },
            { InputActions.Select, "Select" },
            { InputActions.ViewMap, "View Map" }
        };

        private readonly string[] InputActionOrder = new string[11]
        {
            InputActions.Up, InputActions.Down, InputActions.Left, InputActions.Right, InputActions.Grab,
            InputActions.Pause, InputActions.Restart, InputActions.Undo, InputActions.BackOut, InputActions.Select, InputActions.ViewMap
        };

        private readonly string[] TimeStrings = new string[5] { "..1", "..2", "..3", "..4", "..5" };

        private readonly int MaxGamePadIndex = InputGlobals.MaxGamePadCount - 1;
        private const string ControllerUnavailableText = "Disconnected";
        private const string ControllerUnknownText = "Unknown";
        private const int StartInputMapIndex = 1;
        private const int NumInputRows = 6;
        private readonly int LastInputGridIndex = 0;

        private SpriteFont HeaderFont = null;
        private SpriteFont ButtonFont = null;
        private SpriteFont ButtonNameFont = null;

        private Texture2D UITex = null;

        private UIMenuOption.OptionSelected OptionSelected = null;
        private UIMenuOption.OptionDeselected OptionDeselected = null;
        private UIMenuOption.OptionChosen ButtonInputChosen = null;
        private UIMenuOption.OptionChanged ButtonInputOptionChanged = null;
        private UIMenuOption.OptionChanged FooterInputOptionChanged = null;

        private readonly Color TextColor = Color.White;
        private readonly Color DeselectedTextColor = new Color(180, 180, 180, 255);
        private readonly Color ButtonColor = new Color(210, 210, 235, 255);
        private readonly Color UnavailableTextColor = new Color(130, 130, 130, 255);

        private readonly Vector2 HeaderPos = new Vector2(0f, -191f);

        private const double ArrowFadeTime = 600d;
        private readonly Vector2 ArrowDist = new Vector2(130f, -3f);
        private readonly Vector2 ArrowScale = new Vector2(2f, 2f);
        private readonly Color ArrowColorStart = Color.White;
        private readonly Color ArrowColorEnd = Color.White * .3f;
        private Color ArrowColor = Color.White;

        private GamePadMapper GPMapper = null;

        /// <summary>
        /// Temporary config used to store all the data.
        /// </summary>
        private InputConfigData.GamepadMapData TempConfig = null;

        private UISprite LeftArrow = null;
        private UISprite RightArrow = null;

        private InputHandler GPInputHandler = null;

        private UIText GamePadIndexText = null;
        private UIText GamePadNameText = null;

        private List<UIText> InputMapTextList = new List<UIText>(11);
        private List<UIText> ButtonMapTextList = new List<UIText>(11);

        private UIText SaveText = null;
        private UIText DefaultText = null;
        private UIText BackText = null;

        private int CurControllerIndex = 0;
        private bool CurControllerConnected = false;
        private GamePadCapabilities CurGPCaps = default(GamePadCapabilities);

        private int LastSelectedColumn = 0;

        private double ElapsedFadeTime = 0d;

        public GamepadControlConfigMenu(in Vector2 position)
            : base(5, position)
        {
            LastInputGridIndex = StartInputMapIndex - 1 + InputActionOrder.Length;

            HeaderFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont24px);
            ButtonFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont15px);
            ButtonNameFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont13px);

            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);

            OptionSelected = OnOptionSelected;
            OptionDeselected = OnOptionDeselected;
            ButtonInputChosen = ChooseInput;
            ButtonInputOptionChanged = OnButtonInputOptionChanged;
            FooterInputOptionChanged = OnFooterOptionChanged;

            GPMapper = new GamePadMapper(InputGlobals.GamePadInputIndex + CurControllerIndex);
            GPMapper.InputRemapCancelledEvent -= OnInputRemapCancelled;
            GPMapper.InputRemapCancelledEvent += OnInputRemapCancelled;
            GPMapper.InputRemappedEvent -= OnInputRemapped;
            GPMapper.InputRemappedEvent += OnInputRemapped;

            CurControllerIndex = 0;
            
            GPInputHandler = Input.GetInputHandler(InputGlobals.GamePadInputIndex + CurControllerIndex);
            CurGPCaps = GamePad.GetCapabilities(CurControllerIndex);

            //Copy input mappings to the temp data
            TempConfig = new InputConfigData.GamepadMapData();
            TempConfig.MappingData.ActionMappings.CopyDictionaryData(InputGlobals.DefaultMappings[InputHandler.InputTypes.GamePad]);

            SetUpHeaders();
            SetUpButtons();

            SetUpRest();
            AddEntries();

            ChangeControllerIndex(0, true);
        }

        public override void CleanUp()
        {
            GPMapper.InputRemapCancelledEvent -= OnInputRemapCancelled;
            GPMapper.InputRemappedEvent -= OnInputRemapped;

            GPMapper.CleanUp();
        }

        private string GetButtonToName(in int button)
        {
            if (InputGlobals.ButtonNameToText.TryGetValue(button, out string buttonName) == false)
            {
                buttonName = button.ToString();
            }

            return buttonName;
        }

        private string GetReadableInputActionName(in string inputAction)
        {
            if (InputNamesReadable.TryGetValue(inputAction, out string readableName) == false)
            {
                return inputAction;
            }

            return readableName;
        }

        private void SetUpHeaders()
        {
            Rectangle spriteRect = ContentGlobals.ArrowRect;
            Sprite sprite = new Sprite(UITex, spriteRect);

            LeftArrow = new UISprite(sprite);
            LeftArrow.Position = Position + HeaderPos + new Vector2(-spriteRect.Height - ArrowDist.X, ArrowDist.Y);
            LeftArrow.Rotation = UtilityGlobals.ToRadians(-90f);
            LeftArrow.Scale = ArrowScale;

            RightArrow = new UISprite(sprite);
            RightArrow.Position = Position + HeaderPos + new Vector2(spriteRect.Height + ArrowDist.X, ArrowDist.Y);
            RightArrow.Rotation = UtilityGlobals.ToRadians(90f);
            RightArrow.Scale = ArrowScale;

            GamePadIndexText = UIHelpers.CreateCenterUIText(HeaderFont, string.Empty, Vector2.One, ButtonColor, UITextOptions.Outline, UITextData.Standard);
            GamePadIndexText.Position = Position + HeaderPos;

            GamePadNameText = UIHelpers.CreateCenterUIText(ButtonNameFont, string.Empty, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            GamePadNameText.Position = Position + HeaderPos + new Vector2(0f, 46f);
        }

        private void SetUpButtons()
        {
            Vector2 inputStartPos = Position + new Vector2(-230f, -100f);
            Vector2 mappingOffset = new Vector2(90f, 1f);
            const float mappingOffsetXDiff = 42f;
            Vector2 spacing = new Vector2(260f, 40f);

            for (int i = 0; i < InputActionOrder.Length; i++)
            {
                string action = InputActionOrder[i];
                Vector2 offset = new Vector2((i / NumInputRows) * spacing.X, (i % NumInputRows) * spacing.Y);

                UIText inputText = UIHelpers.CreateUIText(ButtonNameFont, GetReadableInputActionName(action), Vector2.Zero, Vector2.One,
                    DeselectedTextColor, UITextOptions.Outline, UITextData.Standard);
                inputText.Position = inputStartPos + offset;

                InputMapTextList.Add(inputText);

                UIText buttonMapText = UIHelpers.CreateUIText(ButtonNameFont, GetButtonToName(TempConfig.MappingData.ActionMappings[action]),
                    Vector2.Zero, Vector2.One, ButtonColor, UITextOptions.Outline, UITextData.Standard);
                buttonMapText.Position = inputStartPos + new Vector2(mappingOffset.X + ((i / NumInputRows) * mappingOffsetXDiff), mappingOffset.Y) + offset;

                ButtonMapTextList.Add(buttonMapText);
            }
        }

        private void SetUpRest()
        {
            SaveText = UIHelpers.CreateUIText(ButtonFont, "Save", new Vector2(1f, .5f), Vector2.One, DeselectedTextColor, UITextOptions.Outline, UITextData.Standard);
            SaveText.Position = Position + new Vector2(-20f, RenderingGlobals.BaseResHeightHalved - 80f);

            DefaultText = UIHelpers.CreateUIText(ButtonFont, "Defaults", new Vector2(0f, .5f), Vector2.One, DeselectedTextColor, UITextOptions.Outline, UITextData.Standard);
            DefaultText.Position = Position + new Vector2(20f, RenderingGlobals.BaseResHeightHalved - 80f);

            BackText = UIHelpers.CreateCenterUIText(ButtonFont, "Back", Vector2.One, DeselectedTextColor, UITextOptions.Outline, UITextData.Standard);
            BackText.Position = Position + new Vector2(0f, RenderingGlobals.BaseResHeightHalved - 40f);
        }

        private void AddEntries()
        {
            AddMenuElement(GamePadIndexText, new UIMenuOption(OnHeaderOptionSelected, OptionDeselected, null, ChangeControllerIndexWrapper));

            for (int i = 0; i < InputMapTextList.Count; i++)
            {
                AddMenuElement(InputMapTextList[i], new UIMenuOption(OptionSelected, OptionDeselected, ButtonInputChosen, ButtonInputOptionChanged));
            }

            AddMenuElement(SaveText, new UIMenuOption(OptionSelected, OptionDeselected, SaveInputs, FooterInputOptionChanged));
            AddMenuElement(DefaultText, new UIMenuOption(OptionSelected, OptionDeselected, DefaultInputs, FooterInputOptionChanged));
            AddMenuElement(BackText, new UIMenuOption(OptionSelected, OptionDeselected, Return, null));

            for (int i = 0; i < ButtonMapTextList.Count; i++)
            {
                AddElement(ButtonMapTextList[i]);
            }

            AddElement(GamePadNameText);

            AddElement(LeftArrow);
            AddElement(RightArrow);
        }

        private bool SelectionInInputGrid(in int selection)
        {
            return (selection >= StartInputMapIndex && selection <= LastInputGridIndex);
        }

        private void OnHeaderOptionSelected(in int optionIndex)
        {
            GamePadIndexText.TextOptions = (UITextOptions)EnumUtility.AddEnumVal((long)GamePadIndexText.TextOptions, (long)UITextOptions.Outline);
            GamePadIndexText.TintColor = ButtonColor;

            ChangeControllerIndex(0, false);
        }

        private void OnOptionSelected(in int optionIndex)
        {
            Elements[optionIndex].TintColor = TextColor;
        }

        private void OnOptionDeselected(in int optionIndex)
        {
            Elements[optionIndex].TintColor = DeselectedTextColor;

            if (optionIndex == 0)
            {
                ElapsedFadeTime = 0d;
                LeftArrow.Visible = false;
                RightArrow.Visible = false;
            }
        }

        private void ChangeControllerIndexWrapper(in int amount)
        {
            ChangeControllerIndex(amount, false);
        }

        private void ChangeControllerIndex(in int amount, in bool forceUpdate)
        {
            int prevControllerIndex = CurControllerIndex;

            CurControllerIndex = UtilityGlobals.Clamp(CurControllerIndex + amount, 0, MaxGamePadIndex);

            GPInputHandler = Input.GetInputHandler(InputGlobals.GamePadInputIndex + CurControllerIndex);
            CurGPCaps = GamePad.GetCapabilities(CurControllerIndex);

            //If the index is different, copy the mapped data to the temp config
            if (prevControllerIndex != CurControllerIndex || forceUpdate == true)
            {
                if (CurGPCaps.IsConnected == true
                    && string.IsNullOrEmpty(CurGPCaps.Identifier) == false
                    && DataHandler.inputConfigData.GPMapData.TryGetValue(CurGPCaps.Identifier, out InputConfigData.GamepadMapData gpMapData) == true)
                {
                    TempConfig.CopyFrom(gpMapData);
                }
                else
                {
                    TempConfig.MappingData.ActionMappings.CopyDictionaryData(InputGlobals.DefaultMappings[InputHandler.InputTypes.GamePad]);
                }

                //Also update all the buttons
                for (int i = 0; i < ButtonMapTextList.Count; i++)
                {
                    ButtonMapTextList[i].Text = GetButtonToName(TempConfig.MappingData.ActionMappings[InputActionOrder[i]]);
                }
            }

            GamePadIndexText.Text = "Gamepad " + (CurControllerIndex + 1);

            CurControllerConnected = CurGPCaps.IsConnected;

            //If the controller isn't connected, show unavailable text
            if (CurControllerConnected == false)
            {
                TempConfig.ControllerID = string.Empty;
                TempConfig.DisplayName = string.Empty;

                GamePadNameText.Text = ControllerUnavailableText;
                GamePadNameText.TintColor = DeselectedTextColor;

                //Go back to the top if the gamepad isn't connected - disallow mapping new inputs
                if (CurSelection != 0 && CurSelection != MaxSelections - 1)
                {
                    SetSelection(0, false);
                }

                for (int i = 0; i < InputMapTextList.Count; i++)
                {
                    InputMapTextList[i].TintColor = UnavailableTextColor;
                }

                SaveText.TintColor = UnavailableTextColor;
                DefaultText.TintColor = UnavailableTextColor;
            }
            else
            {
                //Update the ID if the controller at this index is connected
                TempConfig.ControllerID = CurGPCaps.Identifier;
                TempConfig.DisplayName = CurGPCaps.DisplayName;

                string displayName = CurGPCaps.DisplayName;
                
                //If the controller name isn't valid, display a fallback
                if (string.IsNullOrEmpty(displayName) == true)
                {
                    displayName = ControllerUnknownText;
                }

                GamePadNameText.Text = displayName;
                GamePadNameText.TintColor = TextColor;

                for (int i = 0; i < InputMapTextList.Count; i++)
                {
                    InputMapTextList[i].TintColor = DeselectedTextColor;
                }

                SaveText.TintColor = DeselectedTextColor;
                DefaultText.TintColor = DeselectedTextColor;

                if (CurSelection > 0)
                {
                    Elements[CurSelection].TintColor = TextColor;
                }
            }

            LeftArrow.Visible = (CurSelection == 0);
            RightArrow.Visible = (CurSelection == 0);

            if (CurControllerIndex == 0)
            {
                LeftArrow.Visible = false;
            }
            else if (CurControllerIndex == MaxGamePadIndex)
            {
                RightArrow.Visible = false;
            }
        }

        private void OnButtonInputOptionChanged(in int amount)
        {
            if (amount == 0)
                return;

            //Find how many columns there are
            int columns = (int)(InputActionOrder.Length / NumInputRows);

            //Find out which column this selection is on
            int curColumn = (CurSelection - StartInputMapIndex) / NumInputRows;

            if (amount < 0)
            {
                //Wrap around to the last column if it's the first column
                if (curColumn == 0)
                {
                    int newSelection = UtilityGlobals.Clamp(CurSelection + (columns * NumInputRows), StartInputMapIndex, LastInputGridIndex);
                    SetSelection(newSelection, false);
                }
                else
                {
                    SetSelection(CurSelection - NumInputRows, false);
                }
            }
            else if (amount > 0)
            {
                //Wrap to the first column if it's the last column
                if (curColumn == columns)
                {
                    int newSelection = UtilityGlobals.Clamp(CurSelection - (columns * NumInputRows), StartInputMapIndex, LastInputGridIndex);
                    SetSelection(newSelection, false);
                }
                else
                {
                    SetSelection(UtilityGlobals.Clamp(CurSelection + NumInputRows, StartInputMapIndex, LastInputGridIndex), false);
                }
            }

            LastSelectedColumn = (CurSelection - StartInputMapIndex) / NumInputRows;
        }

        private void OnFooterOptionChanged(in int amount)
        {
            if (amount == 0)
                return;

            int newSelection = UtilityGlobals.Wrap(CurSelection + amount, LastInputGridIndex + 1, MaxSelections - 2);
            SetSelection(newSelection, false);

            LastSelectedColumn = ((CurSelection - LastInputGridIndex - StartInputMapIndex) * NumInputRows) / NumInputRows;
        }

        private void ChooseInput()
        {
            GamePadState gpState = GamePadInput.GetGamePadState(CurControllerIndex);
            if (gpState.IsConnected == true)
            {
                GPMapper.StartMapInput(InputActionOrder[CurSelection - StartInputMapIndex], CurControllerIndex);
            }
        }

        private void SaveInputs()
        {
            if (string.IsNullOrEmpty(CurGPCaps.Identifier) == true)
            {
                return;
            }

            GPInputHandler.CopyMappingsFromDict(TempConfig.MappingData.ActionMappings);
            DataHandler.inputConfigData.AddOrChangeGamepadMappings(CurGPCaps.Identifier, TempConfig);
            DataHandler.SaveInputConfigData();
        }

        private void DefaultInputs()
        {
            TempConfig.MappingData.ActionMappings.CopyDictionaryData(InputGlobals.DefaultMappings[InputHandler.InputTypes.GamePad]);

            for (int i = 0; i < ButtonMapTextList.Count; i++)
            {
                ButtonMapTextList[i].Text = GetButtonToName(TempConfig.MappingData.ActionMappings[InputActionOrder[i]]);
            }
        }

        private void Return()
        {
            BackOut();
        }

        protected override void HandleSelectionInput()
        {
            if (Input.GetButtonDown(InputActions.Select) == true)
            {
                UIMenuOption option = MenuOptions[CurSelection];
                if (option.OnOptionChosen != null)
                {
                    option.OnOptionChosen();

                    if (OptionChosenSound?.IsDisposed == false)
                    {
                        bool playSound = true;

                        //Check if within the grid and don't play the sound if the controller at this index isn't connected
                        if (SelectionInInputGrid(CurSelection) == true)
                        {
                            playSound = GamePadInput.IsGamePadConnected(CurControllerIndex);
                        }

                        if (playSound == true)
                        {
                            SoundManager.Instance.PlaySound(OptionChosenSound, false, OptionChosenVolume);
                        }
                    }
                }
            }
            else if (Input.GetButtonDown(InputActions.BackOut) == true)
            {
                BackOut();

                if (OptionBackSound?.IsDisposed == false)
                {
                    SoundManager.Instance.PlaySound(OptionBackSound, false, OptionBackVolume);
                }
            }
        }

        protected override void HandleCursorInput()
        {
            if (Input.GetButtonDown(InputActions.Left) == true)
            {
                ChangeOption(-OptionChangeAmount, true);
            }
            else if (Input.GetButtonDown(InputActions.Right) == true)
            {
                ChangeOption(OptionChangeAmount, true);
            }
            else if (Input.GetButtonDown(InputActions.Down) == true)
            {
                ChangeSelectionWrapper(OptionSelectionAmount, true);
            }
            else if (Input.GetButtonDown(InputActions.Up) == true)
            {
                ChangeSelectionWrapper(-OptionSelectionAmount, true);
            }
        }

        private void ChangeSelectionWrapper(in int amount, in bool playSound)
        {
            if (amount < 0)
            {
                //Disallow wrapping around
                if (CurSelection == 0)
                {
                    return;
                }

                //Go to the top if no controller is connected
                if (CurSelection == (MaxSelections - 1) && CurControllerConnected == false)
                {
                    ChangeSelection(-CurSelection, playSound);
                    return;
                }

                if (SelectionInInputGrid(CurSelection) == true)
                {
                    int row = (CurSelection - StartInputMapIndex) % NumInputRows;

                    //Go to the top if on the top row
                    if (row == 0)
                    {
                        ChangeSelection(-CurSelection, playSound);
                        return;
                    }
                }
                //Go to the last selected column on the final row
                else if (CurSelection < (MaxSelections - 1))
                {
                    int newSelection = (StartInputMapIndex + (NumInputRows - 1)) + (LastSelectedColumn * NumInputRows);

                    //If the new selection isn't in the input grid, go to the last input selection
                    if (SelectionInInputGrid(newSelection) == false)
                    {
                        newSelection = LastInputGridIndex;
                    }

                    SetSelection(newSelection, true);

                    return;
                }
                else if (CurSelection == (MaxSelections - 1))
                {
                    int newSelection = UtilityGlobals.Clamp(LastInputGridIndex + 1 + LastSelectedColumn, LastInputGridIndex + 1, MaxSelections - 2);

                    SetSelection(newSelection, playSound);
                    return;
                }
            }
            else if (amount > 0)
            {
                //Disallow wrapping from back to the controller
                if (CurSelection == MaxSelections - 1)
                {
                    return;
                }

                //Go to the bottom if no controller is connected
                if (CurSelection == 0 && CurControllerConnected == false)
                {
                    ChangeSelection(MaxSelections - 1, playSound);
                    return;
                }

                if (SelectionInInputGrid(CurSelection) == true)
                {
                    int row = (CurSelection - StartInputMapIndex) % NumInputRows;

                    //Go to the bottom if on the bottom row or last selection in the input grid
                    if (row == (NumInputRows - 1) || CurSelection == LastInputGridIndex)
                    {
                        int selection = UtilityGlobals.Clamp(LastInputGridIndex + 1 + LastSelectedColumn, LastInputGridIndex + 1, MaxSelections - 2);

                        SetSelection(selection, playSound);
                        return;
                    }
                }
                //Go to the last selected column on the first row
                else if (CurSelection == 0)
                {
                    int newSelection = StartInputMapIndex + (LastSelectedColumn * NumInputRows);
                    SetSelection(newSelection, playSound);
                    return;
                }
                else if (CurSelection >= (LastInputGridIndex + 1) && CurSelection < (MaxSelections - 1))
                {
                    SetSelection(MaxSelections - 1, playSound);
                    return;
                }
            }

            ChangeSelection(amount, playSound);
        }

        public override void TransitionUpdate()
        {
            base.TransitionUpdate();

            UpdateScreen();
        }

        protected override void OnMenuBack()
        {
            base.OnMenuBack();

            LastSelectedColumn = 0;

            //When going back, reset all unsaved inputs
            if (string.IsNullOrEmpty(CurGPCaps.Identifier) == false
                && DataHandler.inputConfigData.GPMapData.TryGetValue(CurGPCaps.Identifier, out InputConfigData.GamepadMapData gpMapData) == true)
            {
                TempConfig.CopyFrom(gpMapData);
            }
            else
            {
                TempConfig.ControllerID = string.Empty;
                TempConfig.DisplayName = string.Empty;
                TempConfig.MappingData.ActionMappings.CopyDictionaryData(InputGlobals.DefaultMappings[InputHandler.InputTypes.GamePad]);
            }

            //Update all the buttons
            for (int i = 0; i < ButtonMapTextList.Count; i++)
            {
                ButtonMapTextList[i].Text = GetButtonToName(TempConfig.MappingData.ActionMappings[InputActionOrder[i]]);
            }
        }

        private void OnInputRemapCancelled(in string inputAction)
        {
            ButtonMapTextList[CurSelection - StartInputMapIndex].Text = GetButtonToName(TempConfig.MappingData.ActionMappings[inputAction]);
        }

        private void OnInputRemapped(in string actionRemapped, in int buttonMapped)
        {
            TempConfig.MappingData.ActionMappings[actionRemapped] = buttonMapped;

            ButtonMapTextList[CurSelection - StartInputMapIndex].Text = GetButtonToName(TempConfig.MappingData.ActionMappings[actionRemapped]);
        }

        private void UpdateScreen()
        {
            //Update arrows
            if (CurSelection == 0)
            {
                ElapsedFadeTime += Time.ElapsedTime.TotalMilliseconds;

                double time = UtilityGlobals.PingPong(ElapsedFadeTime / (ArrowFadeTime / 2), 0d, 1d);
                ArrowColor = Interpolation.Interpolate(ArrowColorStart, ArrowColorEnd, time, Interpolation.InterpolationTypes.Linear);

                LeftArrow.TintColor = ArrowColor;
                RightArrow.TintColor = ArrowColor;
            }

            bool controllerConnectionState = GamePadInput.IsGamePadConnected(CurControllerIndex);

            //Poll the state of current controller to see if its connection state changes
            if (controllerConnectionState != CurControllerConnected)
            {
                ChangeControllerIndex(0, true);
            }
        }

        public override void Update()
        {
            if (GPMapper.IsActive == true)
            {
                int timeIndex = UtilityGlobals.Clamp((int)(GPMapper.InputTimeRemaining / Time.MsPerS), 0, TimeStrings.Length - 1);

                ButtonMapTextList[CurSelection - StartInputMapIndex].Text = TimeStrings[timeIndex];

                GPMapper.Update();
            }
            else
            {
                base.Update();

                UpdateScreen();
            }
        }

        public override void Render()
        {
            if (LeftArrow.Visible == true)
                LeftArrow.Render();
            if (RightArrow.Visible == true)
                RightArrow.Render();

            GamePadIndexText.Render();
            GamePadNameText.Render();

            for (int i = 0; i < InputMapTextList.Count; i++)
                InputMapTextList[i].Render();

            for (int i = 0; i < ButtonMapTextList.Count; i++)
                ButtonMapTextList[i].Render();

            SaveText.Render();
            DefaultText.Render();
            BackText.Render();
        }
    }
}
