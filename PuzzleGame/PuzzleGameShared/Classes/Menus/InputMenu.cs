﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// The base class for input menus.
    /// </summary>
    public class InputMenu
    {
        public delegate void MenuOptionSelected();
        public delegate void MenuRenderingRoutine();

        protected MenuState MenuGameState = null;

        protected Vector2 StartPos = RenderingGlobals.BaseResolutionHalved;
        public int CurSelection { get; protected set; } = 0;

        public readonly List<InputMenuOption> MenuOptions = new List<InputMenuOption>(8);

        public float YOffset = 0f;
        public float YSpacing = 50f;
        public float TextScale = 1f;

        public MenuRenderingRoutine RenderingRoutine = null;

        protected MenuOptionSelected BackOutOption = null;

        protected InputMenu()
        {

        }

        public InputMenu(params InputMenuOption[] menuOptions)
        {
            if (menuOptions != null && menuOptions.Length > 0)
                MenuOptions.AddRange(menuOptions);
        }

        public void SetBackOutOption(in MenuOptionSelected backOutOption)
        {
            BackOutOption = backOutOption;
        }

        public void AddMenuOption(in InputMenuOption menuOption)
        {
            MenuOptions.Add(menuOption);
        }

        /// <summary>
        /// Sets the menu state for this input menu.
        /// </summary>
        /// <param name="menuGameState">The menu state.</param>
        public void SetMenuState(in MenuState menuGameState)
        {
            MenuGameState = menuGameState;
        }

        /// <summary>
        /// Changes the current selection.
        /// </summary>
        /// <param name="amount">The amount to change the selection.</param>
        protected void ChangeSelection(int amount)
        {
            CurSelection = UtilityGlobals.Wrap(CurSelection + amount, 0, MenuOptions.Count - 1);

            OnSelectionChanged(CurSelection);
        }

        /// <summary>
        /// What happens when the selection is changed.
        /// </summary>
        /// <param name="newSelection">The new selection.</param>
        protected virtual void OnSelectionChanged(in int newSelection)
        {
            //Don't play the sound if there are 1 or fewer options since there's nothing to wrap
            if (MenuOptions.Count > 1)
            {
                SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionChangeSound), false);
            }
        }

        /// <summary>
        /// Handles input for the menu cursor.
        /// </summary>
        protected virtual void HandleCursorInput()
        {
            if (Input.GetButtonDown(InputActions.Down) == true) ChangeSelection(1);
            else if (Input.GetButtonDown(InputActions.Up) == true) ChangeSelection(-1);
        }

        /// <summary>
        /// Handles input for selecting and backing out of the menu.
        /// </summary>
        protected virtual void HandleSelectionInput()
        {
            if (Input.GetButtonDown(InputActions.BackOut) == true) OnBackOut();
            else if (Input.GetButtonDown(InputActions.Select) == true) OnConfirm();
        }

        /// <summary>
        /// What occurs when the menu is backed out of.
        /// </summary>
        protected void OnBackOut()
        {
            BackOutOption?.Invoke();
        }

        /// <summary>
        /// What occurs when an item is confirmed on the menu.
        /// </summary>
        protected void OnConfirm()
        {
            InputMenuOption menuOption = MenuOptions[CurSelection];

            if (menuOption.OptionSelected != null)
            {
                SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);
                menuOption.OptionSelected.Invoke();
            }
        }

        public void Update()
        {
            HandleCursorInput();
            HandleSelectionInput();
        }

        public struct InputMenuOption
        {
            public string Text;
            public MenuOptionSelected OptionSelected;

            public InputMenuOption(in string text, in MenuOptionSelected optionSelected)
            {
                Text = text;
                OptionSelected = optionSelected;
            }
        }
    }
}
