/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace PuzzleGame
{
    public class MenuTransition : IUpdateable
    {
        public delegate void TransitionEnd();

        private UIInputMenu CurMenu = null;
        private UIInputMenu NextMenu = null;

        private Vector2 CurMenuStart = Vector2.Zero;
        private Vector2 NextMenuStart = Vector2.Zero;

        private Vector2 CurMenuDest = Vector2.Zero;
        private Vector2 NextMenuDest = Vector2.Zero;

        private double TransitionTime = 0d;
        private double ElapsedTime = 0d;

        public bool InProgress { get; private set; } = false;

        public TransitionEnd OnTransitionEnd = null;

        public MenuTransition(UIInputMenu curMenu, UIInputMenu nextMenu, in Vector2 curMenuDest, in Vector2 nextMenuDest, in double transitionTime)
        {
            CurMenu = curMenu;
            NextMenu = nextMenu;
            CurMenuDest = curMenuDest;
            NextMenuDest = nextMenuDest;
            TransitionTime = transitionTime;
        }

        public MenuTransition(UIInputMenu curMenu, UIInputMenu nextMenu, in Vector2 curMenuDest, in Vector2 nextMenuDest, in double transitionTime,
            TransitionEnd onTransitionEnd)
            : this(curMenu, nextMenu, curMenuDest, nextMenuDest, transitionTime)
        {
            OnTransitionEnd = onTransitionEnd;
        }

        public void StartTransition()
        {
            CurMenuStart = CurMenu.Position;
            NextMenuStart = NextMenu.Position;
            ElapsedTime = 0d;

            InProgress = true;
        }

        public void EndTransition()
        {
            //NOTE: Floating point inaccuracies sometimes cause nested elements to be barely off their mark
            CurMenu.Position = CurMenuDest;
            NextMenu.Position = NextMenuDest;

            InProgress = false;

            if (OnTransitionEnd != null)
            {
                OnTransitionEnd.Invoke();
                OnTransitionEnd = null;
            }
        }

        public void Update()
        {
            if (InProgress == false)
                return;

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= TransitionTime)
            {
                EndTransition();
            }
            else
            {
                CurMenu.Position = Interpolation.Interpolate(CurMenuStart, CurMenuDest, ElapsedTime / TransitionTime, Interpolation.InterpolationTypes.Linear);
                NextMenu.Position = Interpolation.Interpolate(NextMenuStart, NextMenuDest, ElapsedTime / TransitionTime, Interpolation.InterpolationTypes.Linear);
            }

            CurMenu.TransitionUpdate();
            NextMenu.TransitionUpdate();
        }

        public void RenderMenus()
        {
            CurMenu.Render();
            NextMenu.Render();
        }

        #region Helper Creation Methods

        public static MenuTransition CreateTransitionDir(UIInputMenu curMenu, UIInputMenu nextMenu, in Direction direction, in double transitionTime)
        {
            Vector2 modifier = DirectionUtil.GetVector2ForDir(direction) * RenderingGlobals.BaseResolution;
            
            Vector2 curMenuDest = curMenu.Position - modifier;
            Vector2 nextMenuDest = nextMenu.Position - modifier;


            return new MenuTransition(curMenu, nextMenu, curMenuDest, nextMenuDest, transitionTime);

            //curMenu.Position = RenderingGlobals.BaseResolutionHalved;
            //
            //Vector2 curMenuDest = curMenu.Position;
            //Vector2 nextMenuDest = RenderingGlobals.BaseResolutionHalved;
            //
            //Vector2 modifier = DirectionUtil.GetVector2ForDir(direction) * RenderingGlobals.BaseResolution;
            //
            //curMenuDest += -modifier;
            //nextMenu.Position = RenderingGlobals.BaseResolutionHalved + modifier;
            //
            //return new MenuTransition(curMenu, nextMenu, curMenuDest, nextMenuDest, transitionTime);
        }

        public static MenuTransition CreateTransitionDir(UIInputMenu curMenu, UIInputMenu nextMenu, in Direction direction, in double transitionTime,
            TransitionEnd onTransitionEnd)
        {
            MenuTransition transition = CreateTransitionDir(curMenu, nextMenu, direction, transitionTime);
            transition.OnTransitionEnd = onTransitionEnd;

            return transition;
        }

        #endregion
    }
}