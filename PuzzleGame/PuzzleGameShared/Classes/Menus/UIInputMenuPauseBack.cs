﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// An input menu that also goes back when pressing the pause button.
    /// </summary>
    public class UIInputMenuPauseBack : UIInputMenu
    {
        public UIInputMenuPauseBack()
            : base(4, Vector2.Zero)
        {
            
        }

        public UIInputMenuPauseBack(in int initialElementCapacity, in Vector2 pos)
            : base(initialElementCapacity, pos)
        {

        }

        protected override void HandleSelectionInput()
        {
            if (Input.GetButtonDown(InputActions.Select) == true)
            {
                UIMenuOption option = MenuOptions[CurSelection];
                if (option.OnOptionChosen != null)
                {
                    option.OnOptionChosen();

                    if (OptionChosenSound?.IsDisposed == false)
                    {
                        SoundManager.Instance.PlaySound(OptionChosenSound, false, OptionChosenVolume);
                    }
                }
            }
            else if (Input.GetButtonDown(InputActions.BackOut) == true || Input.GetButtonDown(InputActions.Pause) == true)
            {
                BackOut();

                if (OptionBackSound?.IsDisposed == false)
                {
                    SoundManager.Instance.PlaySound(OptionBackSound, false, OptionBackVolume);
                }
            }
        }
    }
}
