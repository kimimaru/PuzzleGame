﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// Shows information about each puzzle element.
    /// </summary>
    public sealed class PuzzleInfoMenu : UIInputMenu
    {
        private enum DemonstrationTypes
        {
            Walk, Grab, GrabMove, Undo, Spot, Switch, Pipes, Directions, Warps, Ice, Mud, Paired 
        }

        private const string LockedString = "???";

        private Texture2D UITex = null;
        private NineSlicedSprite Panel = null;
        private SpriteFont Font = null;
        private SpriteFont EntryFont = null;

        private readonly Color TextColor = Color.White;//new Color(0, 140, 0, 255);

        private readonly List<DemonstrationBase> Demonstrations = new List<DemonstrationBase>(12);

        private List<UINineSlicedSprite> DemoBackings = new List<UINineSlicedSprite>();
        private List<UIText> DemoHeaders = new List<UIText>(12);

        private UIText HeaderText = null;
        private UIText ReturnText = null;
        private UISprite UpArrow = null;
        private UISprite DownArrow = null;

        private const float ArrowYDistance = 13f;
        private const double ArrowFadeTime = 600d;
        private static readonly Color ArrowColorStart = Color.White;
        private static readonly Color ArrowColorEnd = Color.White * .3f;

        private readonly Vector2 PanelSize = new Vector2(300, 80);

        private Vector2 OrigOffset = Vector2.Zero;

        private Vector2 Spacing = Vector2.Zero;

        /// <summary>
        /// The order of the demonstrations.
        /// </summary>
        private DemonstrationEntry[] EntryOrder = null;

        private RectangleF VisibleDemoRegion = new RectangleF(0, 217f, RenderingGlobals.BaseResolutionWidth, 260f);

        private Camera2D Camera = null;
        private UIMenuOption Callbacks = default(UIMenuOption);

        /// <summary>
        /// The grid used for demonstrations.
        /// </summary>
        //NOTE: Try to replace this with static methods to avoid additional allocations since we need this only for positioning
        private Grid DemoGrid = null;

        private UIRenderableContainer GridContainer = null;

        /// <summary>
        /// Indicates to ignore new entries after the first locked one has been added; this is added as a convenience.
        /// </summary>
        private bool IgnoreNewEntries = false;

        private int PrevColumnSelection = 0;

        private double ElapsedFadeTime = 0d;
        private Color ArrowColor = ArrowColorStart;

        private int TotalSelections => Demonstrations.Count + 1;

        public PuzzleInfoMenu(in Vector2 position, in Vector2 spacing, SpriteFont font, SpriteFont entryFont)
            : base(13, position)
        {
            Spacing = spacing;
            Font = font;
            EntryFont = entryFont;
            OptionSelectionAmount = 2;

            Initialize();
        }

        private void OnOptionChanged(in int amount)
        {
            //Don't do anything if return is selected
            if (CurSelection == (TotalSelections - 1)) return;
            
            /* Clamp the column to the min/max number
             * If we're on column 0, go to column 1, and vice versa for a two-column grid
             * First find which column we're on using the current index
             */
            DemoGrid.GetColumnRowFromIndex(CurSelection, out int column, out int row);

            //Wrap the column index
            int newColumn = UtilityGlobals.Wrap(column + amount, 0, DemoGrid.Columns - 1);

            //Get the new index using our new column value
            int newIndex = DemoGrid.GetIndex(newColumn, row);

            if (newIndex == (TotalSelections - 1))
                newIndex = CurSelection;

            SetSelection(newIndex, true);
        }

        private void OnOptionSelected(in int optionIndex)
        {
            //Store the current column
            DemoGrid.GetColumnRowFromIndex(optionIndex, out PrevColumnSelection, out int row);

            float diff = (Position.Y + OrigOffset.Y) - DemoBackings[(optionIndex / DemoGrid.Rows) * DemoGrid.Rows].Position.Y;
            GridContainer.Position = new Vector2(GridContainer.Position.X, GridContainer.Position.Y + diff);

            Camera.SetTranslation(new Vector2((RenderingGlobals.BaseResolutionWidth + RenderingGlobals.BaseResWidthHalved) - Position.X, Camera.Position.Y - diff));

            Color col = Color.White;
            if (Demonstrations[optionIndex] == null)
            {
                col = new Color(200, 200, 200);
            }

            DemoBackings[optionIndex].TintColor = col;
        }

        private void OnOptionDeselected(in int optionIndex)
        {
            DemoBackings[optionIndex].TintColor = new Color(100, 100, 100, 255);
        }

        private void OnTextOptionSelected(in int optionIndex)
        {
            ReturnText.TintColor = TextColor;
        }

        private void OnTextOptionDeselected(in int optionIndex)
        {
            ReturnText.TintColor = new Color(180, 180, 180, 255);
        }

        private void Initialize()
        {
            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            Panel = new NineSlicedSprite(UITex, ContentGlobals.PanelRect, 5, 5, 5, 5);

            Callbacks = new UIMenuOption(OnOptionSelected, OnOptionDeselected, null, OnOptionChanged);

            Camera = new Camera2D();
            Camera.SetBounds(new Rectangle(0, 0, RenderingGlobals.BaseResolutionWidth, RenderingGlobals.BaseResolutionHeight));
            Camera.TranslationScale = 0f;
            CameraFollowMenuPosition();

            //Use a grid to align everything
            //Upper center starts the grid from the top, while bottom center starts the items from the bottom, pushing them up
            DemoGrid = new Grid(2, 6, PanelSize);
            DemoGrid.ChangeElementPivot(Grid.GridPivots.BottomCenter);
            DemoGrid.ChangeGridPivot(Grid.GridPivots.UpperCenter);
            DemoGrid.Spacing = Spacing;
            DemoGrid.Position = Position;
            DemoGrid.ChangeGridPadding(0, (int)PanelSize.X / 2, 0, 0);

            GridContainer = new UIRenderableContainer(12, Position);
            AddElement(GridContainer);

            //Push the original position up more so it doesn't center the grid on the screen
            OrigOffset.Y -= PanelSize.Y + (int)(Spacing.Y / 2);

            //Add 1 to offset up by 1
            //We want the space, discounting the perimeter of the rectangle, to be the size of the grid
            //To do that, in addition to the 1 offset, we'll also need to add 2 to the height
            VisibleDemoRegion.Y = (Position.Y + OrigOffset.Y) - ((int)(Spacing.Y / 2) + 1);
            VisibleDemoRegion.Height = ((DemoGrid.Rows / 2) * (PanelSize.Y + Spacing.Y)) + 2;

            //Set up the order
            EntryOrder = new DemonstrationEntry[]
            {
                new DemonstrationEntry(DataHandler.saveData.DemoData.HasWalk, "Walk", DemonstrationTypes.Walk, Vector2.Zero),
                new DemonstrationEntry(DataHandler.saveData.DemoData.HasGrab, "Grab", DemonstrationTypes.Grab, new Vector2(-16f, 0f)),
                new DemonstrationEntry(DataHandler.saveData.DemoData.HasGrabMove, "Push/Pull", DemonstrationTypes.GrabMove, new Vector2(-16f, 0f)),
                new DemonstrationEntry(DataHandler.saveData.DemoData.HasUndo, "Undo", DemonstrationTypes.Undo, new Vector2(-16f, 0f)),
                new DemonstrationEntry(DataHandler.saveData.DemoData.HasSpot, "Spots", DemonstrationTypes.Spot, new Vector2(-16f, 0f)),
                new DemonstrationEntry(DataHandler.saveData.DemoData.HasPipes, "Pipe Blocks", DemonstrationTypes.Pipes, new Vector2(-48f, 0f)),
                new DemonstrationEntry(DataHandler.saveData.DemoData.HasSwitch, "Switches", DemonstrationTypes.Switch, new Vector2(-48, 0f)),
                new DemonstrationEntry(DataHandler.saveData.DemoData.HasDirections, "Directional Blocks", DemonstrationTypes.Directions, new Vector2(-16f, 0f)),
                new DemonstrationEntry(DataHandler.saveData.DemoData.HasWarps, "Warps", DemonstrationTypes.Warps, new Vector2(-16f, 0f)),
                new DemonstrationEntry(DataHandler.saveData.DemoData.HasIce, "Ice", DemonstrationTypes.Ice, new Vector2(-48f, 0f)),
                new DemonstrationEntry(DataHandler.saveData.DemoData.HasMud, "Mud", DemonstrationTypes.Mud, new Vector2(-16f, 0f)),
                new DemonstrationEntry(DataHandler.saveData.DemoData.HasPaired, "Paired Blocks", DemonstrationTypes.Paired, new Vector2(-48f, 0f))
            };

            //Check the save data for which entries are available and create them
            //Add null entries for unavailable ones
            for (int i = 0; i < EntryOrder.Length; i++)
            {
                MakeEntry(EntryOrder[i], i);

                //Break out if we should ignore new entries
                if (IgnoreNewEntries == true)
                {
                    break;
                }
            }

            HeaderText = UIHelpers.CreateUIText(Font, "Glossary", new Vector2(.5f, .5f), Vector2.One, UITextOptions.Outline, UITextData.Standard);
            HeaderText.Position = new Vector2(Position.X, VisibleDemoRegion.Y - 70f);
            HeaderText.TintColor = TextColor;
            AddElement(HeaderText);

            ReturnText = UIHelpers.CreateUIText(Font, "Back", new Vector2(.5f, 0f), Vector2.One, UITextOptions.Outline, UITextData.Standard);
            ReturnText.RenderDepth = .7f;

            Vector2 returnPos = new Vector2(Position.X, VisibleDemoRegion.Bottom + (ArrowYDistance * 4f) + (int)ReturnText.TextOriginVal.Y);

            AddMenuElement(ReturnText, returnPos, new UIMenuOption(OnTextOptionSelected, OnTextOptionDeselected, BackOut, null));

            Sprite arrow = new Sprite(UITex, ContentGlobals.ArrowRect);

            UpArrow = new UISprite(arrow);
            UpArrow.Position = new Vector2(Position.X, VisibleDemoRegion.Y - ArrowYDistance);
            UpArrow.Scale = new Vector2(2f, 2f);

            DownArrow = new UISprite(arrow);
            DownArrow.Position = new Vector2(Position.X, VisibleDemoRegion.Bottom + ArrowYDistance);
            DownArrow.Rotation = DirectionUtil.GetRotationForDir(Direction.Down);
            DownArrow.Scale = new Vector2(2f, 2f);

            AddElement(UpArrow);
            AddElement(DownArrow);
        }

        private void MakeEntry(in DemonstrationEntry demoEntry, in int index)
        {
            if (demoEntry.Unlocked == true)
            {
                UIRenderableContainer container = MakeUnlockedEntry(demoEntry.HeaderText);
                GridContainer.AddElementRelative(container, Vector2.Zero);
                AddMenuElement(Callbacks);

                DemoGrid.AddGridElement(container);
                Demonstrations.Add(GetDemonstrationForValue(demoEntry.DemoType, new Vector2(container.Position.X + (int)(PanelSize.X / 2f),
                    container.Position.Y + (int)(PanelSize.Y / 2f) + 5f) + demoEntry.PositionOffset));
            }
            else
            {
                UIRenderableContainer container = MakeLockedEntry();
                GridContainer.AddElementRelative(container, Vector2.Zero);
                AddMenuElement(Callbacks);

                DemoGrid.AddGridElement(container);
                Demonstrations.Add(null);

                //Check if any other entries are unlocked
                //If none are, tell it to stop here

                for (int i = index + 1; i < EntryOrder.Length; i++)
                {
                    //We found an unlocked entry later, so return
                    if (EntryOrder[i].Unlocked == true)
                    {
                        return;
                    }
                }

                //Nothing afterwards is unlocked, so tell it to stop here
                IgnoreNewEntries = true;
            }
        }

        private UIRenderableContainer MakeUnlockedEntry(in string headerText)
        {
            UIRenderableContainer container = new UIRenderableContainer(2, Vector2.Zero);

            UINineSlicedSprite backing = new UINineSlicedSprite(Panel);
            backing.Scale = PanelSize;
            Vector2 panelOrigin = PanelSize / 2f;
            DemoBackings.Add(backing);

            container.AddElementRelative(backing, Vector2.Zero);
            UIText text = UIHelpers.CreateUIText(EntryFont, headerText, new Vector2(.5f, .5f), Vector2.One, Color.Black);
            text.RenderDepth = .7f;
            container.AddElementRelative(text, new Vector2(panelOrigin.X, 18f));
            DemoHeaders.Add(text);

            return container;
        }

        private UIRenderableContainer MakeLockedEntry()
        {
            UIRenderableContainer container = new UIRenderableContainer(2, Vector2.Zero);

            UINineSlicedSprite backing = new UINineSlicedSprite(Panel);
            backing.Scale = PanelSize;
            Vector2 panelOrigin = PanelSize / 2f;
            DemoBackings.Add(backing);

            container.AddElementRelative(backing, Vector2.Zero);
            UIText text = UIHelpers.CreateUIText(Font, LockedString, new Vector2(.5f, .5f), Vector2.One, Color.Black);
            text.RenderDepth = .7f;
            container.AddElementRelative(text, panelOrigin + new Vector2(1f, 7f));
            DemoHeaders.Add(text);

            return container;
        }

        protected override void HandleCursorInput()
        {
            //This is in place to prevent playing the sound twice
            bool shouldPlay = true;

            if (Input.GetButtonDown(InputActions.Left) == true)
            {
                ChangeOption(-OptionChangeAmount, shouldPlay);
                shouldPlay = false;
            }
            else if (Input.GetButtonDown(InputActions.Right) == true)
            {
                ChangeOption(OptionChangeAmount, shouldPlay);
                shouldPlay = false;
            }

            if (Input.GetButtonDown(InputActions.Down) == true)
            {
                //Find the amount to change the selection
                SelectionChangeWrapper(OptionSelectionAmount, shouldPlay);
            }
            else if (Input.GetButtonDown(InputActions.Up) == true)
            {
                SelectionChangeWrapper(-OptionSelectionAmount, shouldPlay);
            }
        }

        private void SelectionChangeWrapper(in int amount, in bool playSound)
        {
            /*
             * If we go down from the Return option, go up to the previous column on the first row
             * If we go up from the Return option, go to the previous column on the last row
             * Clamp selections to the Return option
            */
            
            int last = TotalSelections - 1;
            int newVal = CurSelection + amount;

            if (CurSelection != last && newVal > last)
            {
                SetSelection(last, playSound);
                return;
            }

            //Disable vertical wrapping - uncomment to enable it again
            if (newVal < 0 || (CurSelection == last && newVal > last))
            {
                return;
            }

            if (CurSelection == last && newVal != CurSelection)
            {
                int newSelection = CurSelection;
                if (newVal > last)
                {
                    newSelection = DemoGrid.GetIndex(PrevColumnSelection, 0);
                }
                else if (newVal < last)
                {
                    int lowestRowIndex = (DemoGrid.Columns == 0) ? 0 : ((TotalSelections - 2) / DemoGrid.Columns);
                
                    newSelection = DemoGrid.GetIndex(PrevColumnSelection, lowestRowIndex);
                    if (newSelection == last)
                    {
                        newSelection = UtilityGlobals.Clamp(last - 1, 0, last);
                    }
                }

                SetSelection(newSelection, playSound);
            }
            else
            {
                ChangeSelection(amount, playSound);
            }
        }

        private void CameraFollowMenuPosition()
        {
            Camera.SetTranslation(new Vector2((RenderingGlobals.BaseResolutionWidth + RenderingGlobals.BaseResWidthHalved) - Position.X, Camera.Position.Y));
        }

        public override void TransitionUpdate()
        {
            base.TransitionUpdate();

            UpdateScreen();
        }

        public override void Update()
        {
            base.Update();

            UpdateScreen();
        }

        private void UpdateScreen()
        {
            //Update only the selected demonstration
            if (CurSelection >= 0 && CurSelection < Demonstrations.Count)
            {
                Demonstrations[CurSelection]?.Update();
            }

            ElapsedFadeTime += Time.ElapsedTime.TotalMilliseconds;

            double time = UtilityGlobals.PingPong(ElapsedFadeTime / (ArrowFadeTime / 2), 0d, 1d);
            ArrowColor = Interpolation.Interpolate(ArrowColorStart, ArrowColorEnd, time, Interpolation.InterpolationTypes.Linear);

            UpArrow.TintColor = ArrowColor;
            DownArrow.TintColor = ArrowColor;

            VisibleDemoRegion.X = (Position.X + OrigOffset.X) - RenderingGlobals.BaseResWidthHalved;
            VisibleDemoRegion.Y = (Position.Y + OrigOffset.Y) - ((int)(Spacing.Y / 2) + 1);

            CameraFollowMenuPosition();
        }

        public override void Render()
        {
            //DemoGrid.DrawGridBounds();
            for (int i = 0; i < DemoBackings.Count; i++)
            {
                //Render only what's in the visible demo region
                if (VisibleDemoRegion.Contains(DemoBackings[i].Position) == true)
                {
                    DemoBackings[i].Render();
                }
            }

            //Draw up and down arrows
            if (VisibleDemoRegion.Contains(DemoBackings[0].Position) == false)
            {
                UpArrow.Render();
            }

            if (VisibleDemoRegion.Contains(DemoBackings[DemoBackings.Count - 1].Position) == false)
            {
                DownArrow.Render();
            }

            HeaderText.Render();

            for (int i = 0; i < DemoHeaders.Count; i++)
            {
                if (VisibleDemoRegion.Contains(DemoBackings[i].Position) == true)
                {
                    DemoHeaders[i].Render();
                }
            }

            ReturnText.Render();

            //Debug
            //RenderingManager.Instance.spriteBatch.DrawHollowRect(UITex, ContentGlobals.BoxRect, VisibleDemoRegion, Color.White, .5f, 1);
            //for (int i = 0; i < DemoBackings.Count; i++)
            //{
            //    UINineSlicedSprite backing = DemoBackings[i];
            //    RenderingManager.Instance.spriteBatch.DrawHollowRect(UITex, new RectangleF(backing.Position, backing.Scale), Color.Green,
            //        .6f, 1);
            //}

            RenderingManager.Instance.EndCurrentBatch();

            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, Camera.TransformMatrix);

            for (int i = 0; i < Demonstrations.Count; i++)
            {
                if (Demonstrations[i] != null && VisibleDemoRegion.Contains(DemoBackings[i].Position) == true)
                {
                    Demonstrations[i].Render();
                }
            }

            RenderingManager.Instance.EndCurrentBatch();

            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, null, null);
        }

        private DemonstrationBase GetDemonstrationForValue(in DemonstrationTypes demonstrationType, in Vector2 position)
        {
            switch(demonstrationType)
            {
                default:
                case DemonstrationTypes.Walk: return new WalkDemonstration(position);
                case DemonstrationTypes.Grab: return new GrabDemonstration(position);
                case DemonstrationTypes.GrabMove: return new PushPullDemonstration(position);
                case DemonstrationTypes.Undo: return new UndoDemonstration(position, BlockPatterns.None, null);
                case DemonstrationTypes.Spot: return new SpotDemonstration(position);
                case DemonstrationTypes.Switch: return new SwitchDemonstration(position);
                case DemonstrationTypes.Pipes: return new PipeDemonstration(position);
                case DemonstrationTypes.Directions: return new UndoDemonstration(position, BlockPatterns.None, Direction.Right);
                case DemonstrationTypes.Warps: return new WarpDemonstration(position);
                case DemonstrationTypes.Ice: return new IceDemonstration(position);
                case DemonstrationTypes.Mud: return new MudDemonstration(position);
                case DemonstrationTypes.Paired: return new PairedDemonstration(position);
            }
        }

        /// <summary>
        /// Describes a demonstration entry.
        /// </summary>
        private struct DemonstrationEntry
        {
            /// <summary>
            /// Whether the demonstration is unlocked.
            /// </summary>
            public bool Unlocked;
            
            /// <summary>
            /// The header text that describes the demonstration.
            /// </summary>
            public string HeaderText;

            /// <summary>
            /// The type of demonstration being presented.
            /// </summary>
            public DemonstrationTypes DemoType;

            /// <summary>
            /// The position offset of the demonstration.
            /// </summary>
            public Vector2 PositionOffset;

            public DemonstrationEntry(in bool unlocked, in string headerText, in DemonstrationTypes demoType, in Vector2 posOffset)
            {
                Unlocked = unlocked;
                HeaderText = headerText;
                DemoType = demoType;
                PositionOffset = posOffset;
            }
        }
    }
}
