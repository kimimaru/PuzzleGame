﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public class OptionsMenu : InputMenu
    {
        /// <summary>
        /// The graphics options for resolution.
        /// This is static to allow fetching and setting the available resolutions from anywhere.
        /// </summary>
        public static InputMenuOption[] GraphicsOptions { get; private set; } = new InputMenuOption[3];
        private int CurGraphicsOption = 0;

        static OptionsMenu()
        {
            GraphicsOptions[0] = new InputMenuOption("Resolution 100%", Res1x);
            GraphicsOptions[1] = new InputMenuOption("Resolution 200%", Res2x);
            GraphicsOptions[2] = new InputMenuOption("Resolution 300%", Res3x);
        }

        public OptionsMenu(in MenuState menustate)
        {
            MenuGameState = menustate;

            CurGraphicsOption = UtilityGlobals.Clamp(Engine.saveData.Settings.ResOption, 0, GraphicsOptions.Length - 1);

            AddMenuOption(new InputMenuOption(GraphicsOptions[CurGraphicsOption].Text, null));
            AddMenuOption(new InputMenuOption("Music Volume: " + (int)Math.Round(SoundManager.Instance.MusicVolume * 10), null));
            AddMenuOption(new InputMenuOption("Sound Volume: " + (int)Math.Round(SoundManager.Instance.SoundVolume * 10), null));
            AddMenuOption(new InputMenuOption("Block Grab: " + (GrabSettings)Engine.saveData.Settings.GrabOption, null));
            AddMenuOption(new InputMenuOption("Return", Return));
        }

        protected override void HandleCursorInput()
        {
            base.HandleCursorInput();

            if (CurSelection == 0)
            {
                if (Input.GetButtonDown(InputActions.Left) == true)
                {
                    PlaySelectSound();

                    CurGraphicsOption = UtilityGlobals.Clamp(CurGraphicsOption - 1, 0, GraphicsOptions.Length - 1);

                    if (CurGraphicsOption != Engine.saveData.Settings.ResOption)
                    {
                        Engine.saveData.Settings.ResOption = CurGraphicsOption;
                    }

                    MenuOptions[CurSelection] = new InputMenuOption(GraphicsOptions[CurGraphicsOption].Text, null);
                    GraphicsOptions[CurGraphicsOption].OptionSelected?.Invoke();
                }
                else if (Input.GetButtonDown(InputActions.Right) == true)
                {
                    PlaySelectSound();

                    CurGraphicsOption = UtilityGlobals.Clamp(CurGraphicsOption + 1, 0, GraphicsOptions.Length - 1);

                    if (CurGraphicsOption != Engine.saveData.Settings.ResOption)
                    {
                        Engine.saveData.Settings.ResOption = CurGraphicsOption;
                    }

                    MenuOptions[CurSelection] = new InputMenuOption(GraphicsOptions[CurGraphicsOption].Text, null);
                    GraphicsOptions[CurGraphicsOption].OptionSelected?.Invoke();
                }
            }
            else if (CurSelection == 1)
            {
                if (Input.GetButtonDown(InputActions.Left) == true)
                {
                    PlaySelectSound();

                    SoundManager.Instance.MusicVolume -= .1f;

                    SoundManager.Instance.MusicVolume = (float)Math.Round(SoundManager.Instance.MusicVolume, 1);

                    if (UtilityGlobals.IsApproximate(SoundManager.Instance.MusicVolume, Engine.saveData.Settings.MusicVolume, .01f) == false)
                    {
                        Engine.saveData.Settings.MusicVolume = SoundManager.Instance.MusicVolume;
                    }

                    InputMenuOption option = MenuOptions[CurSelection];
                    option.Text = "Music Volume: " + (int)Math.Round(SoundManager.Instance.MusicVolume * 10f);

                    MenuOptions[CurSelection] = option;
                }
                else if (Input.GetButtonDown(InputActions.Right) == true)
                {
                    PlaySelectSound();

                    SoundManager.Instance.MusicVolume += .1f;

                    SoundManager.Instance.MusicVolume = (float)Math.Round(SoundManager.Instance.MusicVolume, 1);

                    if (UtilityGlobals.IsApproximate(SoundManager.Instance.MusicVolume, Engine.saveData.Settings.MusicVolume, .01f) == false)
                    {
                        Engine.saveData.Settings.MusicVolume = SoundManager.Instance.MusicVolume;
                    }

                    InputMenuOption option = MenuOptions[CurSelection];
                    option.Text = "Music Volume: " + (int)Math.Round(SoundManager.Instance.MusicVolume * 10f);

                    MenuOptions[CurSelection] = option;
                }
            }
            else if (CurSelection == 2)
            {
                if (Input.GetButtonDown(InputActions.Left) == true)
                {
                    PlaySelectSound();

                    SoundManager.Instance.SoundVolume -= .1f;

                    SoundManager.Instance.SoundVolume = (float)Math.Round(SoundManager.Instance.SoundVolume, 1);

                    if (UtilityGlobals.IsApproximate(SoundManager.Instance.SoundVolume, Engine.saveData.Settings.SoundVolume, .01f) == false)
                    {
                        Engine.saveData.Settings.SoundVolume = SoundManager.Instance.SoundVolume;
                    }

                    InputMenuOption option = MenuOptions[CurSelection];
                    option.Text = "Sound Volume: " + (int)Math.Round(SoundManager.Instance.SoundVolume * 10f);

                    MenuOptions[CurSelection] = option;
                }
                else if (Input.GetButtonDown(InputActions.Right) == true)
                {
                    PlaySelectSound();

                    SoundManager.Instance.SoundVolume += .1f;

                    SoundManager.Instance.SoundVolume = (float)Math.Round(SoundManager.Instance.SoundVolume, 1);

                    if (UtilityGlobals.IsApproximate(SoundManager.Instance.SoundVolume, Engine.saveData.Settings.SoundVolume, .01f) == false)
                    {
                        Engine.saveData.Settings.SoundVolume = SoundManager.Instance.SoundVolume;
                    }

                    InputMenuOption option = MenuOptions[CurSelection];
                    option.Text = "Sound Volume: " + (int)Math.Round(SoundManager.Instance.SoundVolume * 10f);

                    MenuOptions[CurSelection] = option;
                }
            }
            else
            {
                if (Input.GetButtonDown(InputActions.Left) == true)
                {
                    PlaySelectSound();

                    Engine.saveData.Settings.GrabOption = (int)GrabSettings.Hold;

                    InputMenuOption option = MenuOptions[CurSelection];
                    option.Text = "Block Grab: " + (GrabSettings)Engine.saveData.Settings.GrabOption;

                    MenuOptions[CurSelection] = option;
                }
                else if (Input.GetButtonDown(InputActions.Right) == true)
                {
                    PlaySelectSound();

                    Engine.saveData.Settings.GrabOption = (int)GrabSettings.Press;

                    InputMenuOption option = MenuOptions[CurSelection];
                    option.Text = "Block Grab: " + (GrabSettings)Engine.saveData.Settings.GrabOption;

                    MenuOptions[CurSelection] = option;
                }
            }
        }

        private void PlaySelectSound()
        {
            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionChangeSound), false);
        }

        private void Return()
        {
            MenuGameState.PopMenu();
        }

        private static void Res1x()
        {
            RenderingManager.Instance.ResizeWindow(RenderingGlobals.BaseResolution, true);
        }

        private static void Res2x()
        {
            RenderingManager.Instance.ResizeWindow(RenderingGlobals.BaseResolution * 2, true);
        }

        private static void Res3x()
        {
            RenderingManager.Instance.ResizeWindow(RenderingGlobals.BaseResolution * 3, true);
        }
    }
}
