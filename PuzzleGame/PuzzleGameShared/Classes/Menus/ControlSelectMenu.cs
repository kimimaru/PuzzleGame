﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public class ControlSelectMenu : UIInputMenu
    {
        private SpriteFont Font = null;

        private GamepadControlConfigMenu GPControlMenu = null;
        private KeyboardControlConfigMenu KBControlMenu = null;

        private UIMenuManager MenuManager = null;

        private UIMenuOption.OptionSelected OptionSelected = null;
        private UIMenuOption.OptionDeselected OptionDeselected = null;

        private readonly Color TextColor = Color.White;
        private readonly Color DeselectedTextColor = new Color(180, 180, 180, 255);

        public ControlSelectMenu(in Vector2 position, UIMenuManager uiMenuManager)
            : base(3, position)
        {
            Font = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont21px);

            MenuManager = uiMenuManager;

            OptionSelected = OnOptionSelected;
            OptionDeselected = OnOptionDeselected;

            InitMenu();
        }

        public override void CleanUp()
        {
            base.CleanUp();

            GPControlMenu?.CleanUp();
            KBControlMenu?.CleanUp();
        }

        private void InitMenu()
        {
            UIText controllerText = UIHelpers.CreateCenterUIText(Font, "Controller", Vector2.One, UITextOptions.Outline, UITextData.Standard);
            AddMenuElementRelative(controllerText, new Vector2(0f, -60f), new UIMenuOption(OptionSelected, OptionDeselected, GamepadControlConfig, null));

            UIText keyboardText = UIHelpers.CreateCenterUIText(Font, "Keyboard", Vector2.One, UITextOptions.Outline, UITextData.Standard);
            AddMenuElementRelative(keyboardText, new Vector2(0f, 0f), new UIMenuOption(OptionSelected, OptionDeselected, KeyboardControlConfig, null));
            
            UIText backText = UIHelpers.CreateCenterUIText(Font, "Back", Vector2.One, UITextOptions.Outline, UITextData.Standard);
            AddMenuElementRelative(backText, new Vector2(0f, 60f), new UIMenuOption(OptionSelected, OptionDeselected, BackOut, null));
        }

        public void GamepadControlConfig()
        {
            if (GPControlMenu == null)
            {
                GPControlMenu = new GamepadControlConfigMenu(RenderingGlobals.BaseResolutionHalved + new Vector2(RenderingGlobals.BaseResolutionWidth, 0f));
                GPControlMenu.SetBackoutOption(OnBackOutConfig);
                UIHelpers.SetDefaultMenuSounds(GPControlMenu, true, true);
            }

            GPControlMenu.ChangeSelection(-GPControlMenu.CurSelection, false);
            GPControlMenu.ResetSelectionStates();

            MenuManager.PushWithTransition(GPControlMenu, MenuTransition.CreateTransitionDir(this, GPControlMenu, Direction.Right, 200d));
        }

        private void KeyboardControlConfig()
        {
            if (KBControlMenu == null)
            {
                KBControlMenu = new KeyboardControlConfigMenu(RenderingGlobals.BaseResolutionHalved + new Vector2(RenderingGlobals.BaseResolutionWidth, 0f));
                KBControlMenu.SetBackoutOption(OnBackOutConfig);
                UIHelpers.SetDefaultMenuSounds(KBControlMenu, true, true);
            }

            KBControlMenu.ChangeSelection(-KBControlMenu.CurSelection, false);
            KBControlMenu.ResetSelectionStates();

            MenuManager.PushWithTransition(KBControlMenu, MenuTransition.CreateTransitionDir(this, KBControlMenu, Direction.Right, 200d));
        }

        private void Return()
        {
            BackOut();
        }

        private void OnBackOutConfig()
        {
            UIInputMenu prevMenu = MenuManager.Pop();
            UIInputMenu curMenu = MenuManager.Peek();

            MenuTransition transition = MenuTransition.CreateTransitionDir(prevMenu, curMenu, Direction.Left, 200d);

            MenuManager.SetTransition(transition);
        }

        private void OnOptionSelected(in int optionIndex)
        {
            Elements[optionIndex].TintColor = TextColor;
        }

        private void OnOptionDeselected(in int optionIndex)
        {
            Elements[optionIndex].TintColor = DeselectedTextColor;
        }
    }
}
