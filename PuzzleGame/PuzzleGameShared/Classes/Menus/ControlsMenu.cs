/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public class ControlsMenu : UIInputMenu
    {
        private readonly Color ColumnHeaderColor = new Color(240, 240, 240, 255);
        private readonly Color TextColor = new Color(210, 210, 210, 255);
        private readonly Color InputColumnColor = new Color(240, 240, 255, 255);
        private readonly Color InputColumnTextColor = new Color(210, 210, 235, 255);

        private readonly float ColumnHeaderYDist = 45f;
        private readonly float ColumnYDist = 32f;
        private readonly float ColumnHeaderYStart = 115f;

        private readonly float InputColumnXStart = 88f;
        private readonly float EqualsColumnXStart = 225f;
        private readonly float MenuColumnXStart = 508f;
        private readonly float MapColumnXStart = 456f;
        private readonly float LevelColumnXStart = 288f;

        private SpriteFont FontHeader = null;
        private SpriteFont FontColumnHeader = null;
        private SpriteFont FontControls = null;

        private List<UIText> ColumnHeaders = new List<UIText>(4);

        public ControlsMenu(in Vector2 position, in bool includeHeader)
        {
            Position = position;

            FontColumnHeader = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont13px);
            FontControls = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont10px);

            if (includeHeader == true)
            {
                FontHeader = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont24px);
                SetUpHeader();
            }

            SetUpInputColumn();
            SetUpEqualsColumn();
            //SetUpMenuColumn();
            SetUpMapColumn();
            SetUpLevelColumn();

            //Move the column headers to the end so all the text using other fonts gets rendered first to avoid redundant texture switches
            for (int i = 0; i < ColumnHeaders.Count; i++)
            {
                AddElement(ColumnHeaders[i]);
            }

            ColumnHeaders.Clear();
            ColumnHeaders = null;

            AddMenuElement(new UIMenuOption(null, null, BackOut, null));
        }

        private void SetUpHeader()
        {
            UIText header = UIHelpers.CreateCenterUIText(FontHeader, "Controls", Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            header.Position = new Vector2(RenderingGlobals.BaseResolutionWidth / 2, 46);

            AddElementRelative(header, new Vector2(RenderingGlobals.BaseResolutionWidth / 2, 46));
        }

        private void SetUpInputColumn()
        {
            float x = InputColumnXStart;

            UIText inputHeader = UIHelpers.CreateUIText(FontColumnHeader, "Input", Vector2.Zero, Vector2.One, InputColumnColor, UITextOptions.Outline, UITextData.Standard);
            inputHeader.Position = Position + new Vector2(x, ColumnHeaderYStart);
            ColumnHeaders.Add(inputHeader);

            UIText wasdText = UIHelpers.CreateUIText(FontControls, "WASD", Vector2.Zero, Vector2.One, InputColumnTextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(wasdText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist));

            UIText arrowsText = UIHelpers.CreateUIText(FontControls, "Arrow Keys", Vector2.Zero, Vector2.One, InputColumnTextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(arrowsText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 1)));

            UIText enterText = UIHelpers.CreateUIText(FontControls, "Enter", Vector2.Zero, Vector2.One, InputColumnTextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(enterText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 2)));

            UIText spaceText = UIHelpers.CreateUIText(FontControls, "Space", Vector2.Zero, Vector2.One, InputColumnTextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(spaceText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 3)));

            UIText xText = UIHelpers.CreateUIText(FontControls, "X", Vector2.Zero, Vector2.One, InputColumnTextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(xText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 4)));

            UIText eText = UIHelpers.CreateUIText(FontControls, "E", Vector2.Zero, Vector2.One, InputColumnTextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(eText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 5)));

            UIText rText = UIHelpers.CreateUIText(FontControls, "R", Vector2.Zero, Vector2.One, InputColumnTextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(rText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 6)));

            UIText uText = UIHelpers.CreateUIText(FontControls, "U", Vector2.Zero, Vector2.One, InputColumnTextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(uText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 7)));

            UIText escText = UIHelpers.CreateUIText(FontControls, "Esc", Vector2.Zero, Vector2.One, InputColumnTextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(escText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 8)));
        }

        private void SetUpEqualsColumn()
        {
            float x = EqualsColumnXStart;
            const string equals = "=";

            float y = ColumnHeaderYStart - 1f;

            //UIText inputHeader = UIHelpers.CreateUIText(FontColumnHeader, equals, Vector2.Zero, Vector2.One, ColumnHeaderColor, UITextOptions.Outline, UITextData.Standard);
            //AddElementRelative(inputHeader, new Vector2(x, ColumnHeaderYStart));

            UIText wasdText = UIHelpers.CreateUIText(FontControls, equals, Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(wasdText, new Vector2(x, y + ColumnHeaderYDist));

            UIText arrowsText = UIHelpers.CreateUIText(FontControls, equals, Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(arrowsText, new Vector2(x, y + ColumnHeaderYDist + (ColumnYDist * 1)));

            UIText enterText = UIHelpers.CreateUIText(FontControls, equals, Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(enterText, new Vector2(x, y + ColumnHeaderYDist + (ColumnYDist * 2)));

            UIText spaceText = UIHelpers.CreateUIText(FontControls, equals, Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(spaceText, new Vector2(x, y + ColumnHeaderYDist + (ColumnYDist * 3)));

            UIText xText = UIHelpers.CreateUIText(FontControls, equals, Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(xText, new Vector2(x, y + ColumnHeaderYDist + (ColumnYDist * 4)));

            UIText eText = UIHelpers.CreateUIText(FontControls, equals, Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(eText, new Vector2(x, y + ColumnHeaderYDist + (ColumnYDist * 5)));

            UIText rText = UIHelpers.CreateUIText(FontControls, equals, Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(rText, new Vector2(x, y + ColumnHeaderYDist + (ColumnYDist * 6)));

            UIText uText = UIHelpers.CreateUIText(FontControls, equals, Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(uText, new Vector2(x, y + ColumnHeaderYDist + (ColumnYDist * 7)));

            UIText escText = UIHelpers.CreateUIText(FontControls, equals, Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(escText, new Vector2(x, y + ColumnHeaderYDist + (ColumnYDist * 8)));
        }

        private void SetUpMenuColumn()
        {
            float x = MenuColumnXStart;

            UIText menuHeader = UIHelpers.CreateUIText(FontColumnHeader, "Menu", Vector2.Zero, Vector2.One, ColumnHeaderColor, UITextOptions.Outline, UITextData.Standard);
            menuHeader.Position = Position + new Vector2(x, ColumnHeaderYStart);
            ColumnHeaders.Add(menuHeader);

            UIText wasdText = UIHelpers.CreateUIText(FontControls, "Move", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(wasdText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist));

            UIText arrowsText = UIHelpers.CreateUIText(FontControls, "Move", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(arrowsText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 1)));

            UIText enterText = UIHelpers.CreateUIText(FontControls, "Menu Select", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(enterText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 2)));

            UIText spaceText = UIHelpers.CreateUIText(FontControls, "Menu Back", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(spaceText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 3)));

            UIText xText = UIHelpers.CreateUIText(FontControls, "Menu Back", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(xText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 4)));

            //UIText eText = UIHelpers.CreateUIText(FontControls, "E", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            //AddElementRelative(eText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 5)));

            //UIText rText = UIHelpers.CreateUIText(FontControls, "R", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            //AddElementRelative(rText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 6)));

            //UIText uText = UIHelpers.CreateUIText(FontControls, "U", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            //AddElementRelative(uText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 7)));

            UIText escText = UIHelpers.CreateUIText(FontControls, "Menu Back", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(escText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 8)));
        }

        private void SetUpMapColumn()
        {
            float x = MapColumnXStart;

            UIText mapHeader = UIHelpers.CreateUIText(FontColumnHeader, "Map", Vector2.Zero, Vector2.One, ColumnHeaderColor, UITextOptions.Outline, UITextData.Standard);
            mapHeader.Position = Position + new Vector2(x, ColumnHeaderYStart);
            ColumnHeaders.Add(mapHeader);

            UIText wasdText = UIHelpers.CreateUIText(FontControls, "Move", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(wasdText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist));

            UIText arrowsText = UIHelpers.CreateUIText(FontControls, "Move", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(arrowsText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 1)));

            UIText enterText = UIHelpers.CreateUIText(FontControls, "Enter Level", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(enterText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 2)));

            UIText spaceText = UIHelpers.CreateUIText(FontControls, "Enter Level", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(spaceText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 3)));

            UIText xText = UIHelpers.CreateUIText(FontControls, "Enter Level", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(xText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 4)));

            UIText eText = UIHelpers.CreateUIText(FontControls, "View Map", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(eText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 5)));

            UIText rText = UIHelpers.CreateUIText(FontControls, "n/a", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(rText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 6)));

            UIText uText = UIHelpers.CreateUIText(FontControls, "n/a", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(uText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 7)));

            UIText escText = UIHelpers.CreateUIText(FontControls, "Pause", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(escText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 8)));
        }

        private void SetUpLevelColumn()
        {
            float x = LevelColumnXStart;

            UIText levelHeader = UIHelpers.CreateUIText(FontColumnHeader, "Level", Vector2.Zero, Vector2.One, ColumnHeaderColor, UITextOptions.Outline, UITextData.Standard);
            levelHeader.Position = Position + new Vector2(x, ColumnHeaderYStart);
            ColumnHeaders.Add(levelHeader);

            UIText wasdText = UIHelpers.CreateUIText(FontControls, "Move", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(wasdText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist));

            UIText arrowsText = UIHelpers.CreateUIText(FontControls, "Move", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(arrowsText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 1)));

            UIText enterText = UIHelpers.CreateUIText(FontControls, "n/a", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(enterText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 2)));

            UIText spaceText = UIHelpers.CreateUIText(FontControls, "Grab Block", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(spaceText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 3)));

            UIText xText = UIHelpers.CreateUIText(FontControls, "Grab Block", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(xText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 4)));

            UIText eText = UIHelpers.CreateUIText(FontControls, "n/a", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(eText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 5)));

            UIText rText = UIHelpers.CreateUIText(FontControls, "Restart(hold)", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(rText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 6)));

            UIText uText = UIHelpers.CreateUIText(FontControls, "Undo", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(uText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 7)));

            UIText escText = UIHelpers.CreateUIText(FontControls, "Pause", Vector2.Zero, Vector2.One, TextColor, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(escText, new Vector2(x, ColumnHeaderYStart + ColumnHeaderYDist + (ColumnYDist * 8)));
        }
    }
}