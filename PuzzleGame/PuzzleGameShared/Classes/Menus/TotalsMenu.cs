﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public class TotalsMenu : UIInputMenu
    {
        private static readonly Color IncompleteLevelColor = new Color(115, 115, 115, 255);

        private const double ArrowFadeTime = 600d;
        private static readonly Color ArrowColorStart = Color.White;
        private static readonly Color ArrowColorEnd = Color.White * .3f;

        private const float PanelYOffset = -120f;
        private static readonly Vector2 PanelSize = new Vector2(440f, 250f);

        private const float StatsYOffset = 23f;

        private static readonly Vector2 LevelHeaderPosOffset = new Vector2(-204f, -111f);
        private const float LevelHeaderXDiff = 219f;
        private const float LevelHeaderYDiff = 49f;
        private const int Columns = 2;

        private readonly int LevelsInPanel = (int)(PanelSize.Y / LevelHeaderYDiff) * Columns;

        private const float DividerYOffset = 5f;
        private readonly Vector2 DividerSize = new Vector2(2f, PanelSize.Y - 10f);
        private readonly Color DividerColor = new Color(167, 167, 167, 255);

        private readonly Color TextColor = new Color(0, 110, 0, 255);
        private readonly Color TravelToWorldTextColor = new Color(103, 143, 255, 255);
        private readonly Color TravelToWorldTextColorDisabled = new Color(180, 180, 180, 255) * .6f;
        //private readonly Color TravelToWorldArrowColor = Color.White;
        //private readonly Color TravelToWorldArrowColorDisabled = new Color(180, 180, 180, 255) * .6f;

        private UIMenuOption.OptionSelected OptionSelected = null;
        private UIMenuOption.OptionDeselected OptionDeselected = null;
        private UIMenuOption.OptionChanged OptionChanged = null;

        private UIRenderableContainer WorldInfoContainer = null;
        private List<UIRenderableContainer> IndividualWorldContainers = null;

        //These are here so we can batch all the sprites and text together for performance
        private List<List<UISprite>> IndividualStatSprites = null;
        private List<List<UIText>> IndividualStatText = null;

        private SpriteFont Font = null;
        private SpriteFont WorldStatsFont = null;
        private SpriteFont EntryFont = null;
        private Texture2D UITex = null;
        private Sprite MovesIconSmall = null;
        private Sprite TimeIconSmall = null;

        private UISprite MovesIconBig = null;
        private UISprite TimeIconBig = null;

        private UIText WorldName = null;
        private UIText WorldLevels = null;
        private UIText WorldMoves = null;
        private UIText WorldTime = null;

        private UISprite UpArrow = null;
        private UISprite DownArrow = null;
        private UISprite LeftArrow = null;
        private UISprite RightArrow = null;

        private UIText TravelToWorld = null;
        //private UISprite TravelArrow = null;
        //private readonly Vector2 TravelArrowOffset = new Vector2(0f, 40f);
        private const double TravelTime = 600d;
        private double ElapsedTravelTime = 0d;
        private readonly Vector2 TravelTextStartPosRelative = new Vector2(-175f, -236f);
        private readonly Vector2 TravelTextStartBob = new Vector2(0f, 2f);
        private readonly Vector2 TravelTextEndBob = new Vector2(0f, -2f);

        /// <summary>
        /// The offsets of the vertical selections for each world on the menu.
        /// For example, if the player scrolled down once, the value at that selection would be 1.
        /// </summary>
        private int[] WorldSelectionOffsets = null;
        private List<WorldInfo> WorldInfoList = null;

        private double ElapsedFadeTime = 0d;
        private Color ArrowColor = ArrowColorStart;

        private int StartingWorldID = 0;

        public TotalsMenu(in Vector2 position, in int startingWorldID)
            : base(5, position)
        {
            OptionChangeAmount = Columns;

            Font = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont18px);
            WorldStatsFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont13px);
            EntryFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont10px);
            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);

            MovesIconSmall = new Sprite(UITex, ContentGlobals.MovesIconRectSmall, Vector2.Zero);
            TimeIconSmall = new Sprite(UITex, ContentGlobals.TimeIconRectSmall, Vector2.Zero);

            OptionSelected = OnOptionSelected;
            OptionDeselected = OnOptionDeselected;
            OptionChanged = OnOptionChanged;

            SetUpUI();

            //Initialize world travel text
            OnKeyboardMappingsChanged(0);

            StartingWorldID = startingWorldID;
            CurSelection = startingWorldID;

            //Find the index of the world starting at this ID
            for (int i = 0; i < WorldInfoList.Count; i++)
            {
                if (WorldInfoList[i].WorldID == startingWorldID)
                {
                    CurSelection = i;
                    break;
                }
            }

            OnOptionSelected(CurSelection);

            DataHandler.inputConfigData.KeyboardMappingsChangedEvent -= OnKeyboardMappingsChanged;
            DataHandler.inputConfigData.KeyboardMappingsChangedEvent += OnKeyboardMappingsChanged;
        }

        public override void CleanUp()
        {
            base.CleanUp();

            DataHandler.inputConfigData.KeyboardMappingsChangedEvent -= OnKeyboardMappingsChanged;
        }

        private void SetUpUI()
        {
            SetUpHeaders();
            SetUpArrows();

            WorldInfoContainer = new UIRenderableContainer(DataHandler.saveData.WorldSaves.Keys.Count, Vector2.Zero);
            AddElementRelative(WorldInfoContainer, Vector2.Zero);

            IndividualWorldContainers = new List<UIRenderableContainer>(DataHandler.levelDescData.WorldDescriptions.Count);

            WorldInfoList = new List<WorldInfo>(DataHandler.levelDescData.WorldDescriptions.Count);

            IndividualStatSprites = new List<List<UISprite>>(DataHandler.levelDescData.WorldDescriptions.Count);
            IndividualStatText = new List<List<UIText>>(DataHandler.levelDescData.WorldDescriptions.Count);

            foreach (KeyValuePair<int, WorldDescriptionData> worldDesc in DataHandler.levelDescData.WorldDescriptions)
            {
                int worldID = worldDesc.Key;

                //If we don't have save data for this world, ignore it
                if (SaveData.GetWorldData(worldID, out WorldData worldData) == false)
                {
                    continue;
                }

                WorldDescriptionData worldDescData = worldDesc.Value;

                WorldInfo worldInfo = new WorldInfo(worldDescData.WorldID, 0, worldDescData.LevelCount, 0, 0d);

                SetUpWorldEntry(WorldInfoList.Count, worldID, worldDescData, ref worldInfo);

                WorldInfoList.Add(worldInfo);
            }

            WorldSelectionOffsets = new int[WorldInfoList.Count];
        }

        private void SetUpHeaders()
        {
            Vector2 origin = new Vector2(0f, .5f);
            
            WorldName = UIHelpers.CreateUIText(Font, "Test", origin, Vector2.One, Color.Green, UITextOptions.Outline,
                UITextData.Standard);
            AddElementRelative(WorldName, new Vector2(-57f, -280));

            WorldLevels = UIHelpers.CreateUIText(WorldStatsFont, "Test", origin, Vector2.One, Color.White, UITextOptions.Outline,
                UITextData.Standard);
            AddElementRelative(WorldLevels, new Vector2(-30f, -249f));

            MovesIconBig = new UISprite(new Sprite(UITex, ContentGlobals.MovesIconRectLarge));
            AddElementRelative(MovesIconBig, new Vector2(-33f, -218f));

            WorldMoves = UIHelpers.CreateUIText(WorldStatsFont, "Test", origin, Vector2.One, OverworldPausedState.WorldStatsColor, UITextOptions.Outline,
                UITextData.Standard);
            AddElementRelative(WorldMoves, new Vector2(8f, -214f));

            TimeIconBig = new UISprite(new Sprite(UITex, ContentGlobals.TimeIconRectLarge));
            AddElementRelative(TimeIconBig, new Vector2(-26f, -180f));

            WorldTime = UIHelpers.CreateUIText(WorldStatsFont, "Test", origin, Vector2.One, OverworldPausedState.WorldStatsColor, UITextOptions.Outline,
                UITextData.Standard);
            AddElementRelative(WorldTime, new Vector2(8f, -175f));

            TravelToWorld = UIHelpers.CreateUIText(WorldStatsFont, string.Empty, new Vector2(.5f, 0f), Vector2.One,
                TravelToWorldTextColorDisabled, UITextOptions.Outline, UITextData.Standard);
            AddElementRelative(TravelToWorld, TravelTextStartPosRelative + TravelTextStartBob);

            //TravelArrow = new UISprite(new Sprite(UITex, ContentGlobals.PinkArrowRect), new Vector2(2f, 2f));
            //TravelArrow.Position = TravelToWorld.Position + TravelArrowOffset;
            //TravelArrow.Rotation = UtilityGlobals.ToRadians(90f);
            //AddElement(TravelArrow);
        }

        private void SetUpArrows()
        {
            Rectangle spriteRect = ContentGlobals.ArrowRect;
            Sprite sprite = new Sprite(UITex, spriteRect);
            Vector2 arrowDist = new Vector2(4f, 4f);

            UpArrow = new UISprite(sprite);
            DownArrow = new UISprite(sprite);
            LeftArrow = new UISprite(sprite);
            RightArrow = new UISprite(sprite);

            Vector2 arrowScale = new Vector2(2f, 2f);
            UpArrow.Scale = arrowScale;
            DownArrow.Scale = arrowScale;
            LeftArrow.Scale = arrowScale;
            RightArrow.Scale = arrowScale;

            DownArrow.Rotation = UtilityGlobals.ToRadians(180f);
            LeftArrow.Rotation = UtilityGlobals.ToRadians(-90f);
            RightArrow.Rotation = UtilityGlobals.ToRadians(90f);

            Vector2 panelPos = Position + new Vector2(0f, PanelYOffset);

            UpArrow.Position = panelPos + new Vector2(0f, -spriteRect.Height - arrowDist.Y);
            DownArrow.Position = panelPos + new Vector2(0f, PanelSize.Y + spriteRect.Height + arrowDist.Y);
            LeftArrow.Position = panelPos + new Vector2((-PanelSize.X / 2f) - spriteRect.Height - arrowDist.X, PanelSize.Y / 2f);
            RightArrow.Position = panelPos + new Vector2((PanelSize.X / 2f) + spriteRect.Height + arrowDist.X, PanelSize.Y / 2f);

            AddElement(UpArrow);
            AddElement(DownArrow);
            AddElement(LeftArrow);
            AddElement(RightArrow);
        }

        private void SetUpWorldEntry(in int index, in int worldID, WorldDescriptionData worldDesc, ref WorldInfo worldInfo)
        {
            //Set up the container for the world
            UIRenderableContainer container = new UIRenderableContainer(1 + (worldDesc.LevelCount * 2), Position + new Vector2(RenderingGlobals.BaseResolutionWidth * index, 0));

            NineSlicedSprite nineSliced = new NineSlicedSprite(UITex, ContentGlobals.PanelRect, 5, 5, 5, 5);

            UINineSlicedSprite panel = new UINineSlicedSprite(nineSliced);
            panel.Scale = PanelSize;
            panel.TintColor = Color.White;

            container.AddElementRelative(panel, new Vector2(-panel.Scale.X / 2f, PanelYOffset));

            UISprite divider = new UISprite(new Sprite(UITex, ContentGlobals.BoxRect), DividerSize);
            divider.TintColor = DividerColor;
            container.AddElementRelative(divider, new Vector2((int)(-DividerSize.X / 2f), PanelYOffset + DividerYOffset));

            IndividualStatSprites.Add(new List<UISprite>((worldDesc.LevelCount * 2) + 1));
            IndividualStatText.Add(new List<UIText>((worldDesc.LevelCount * 3)));

            IndividualStatSprites[index].Add(panel);
            IndividualStatSprites[index].Add(divider);

            int lvlIndex = 0;

            for (int i = 0; i < worldDesc.LvlDescriptions.Length; i++)
            {
                LevelDescriptionData lvlDescData = worldDesc.LvlDescriptions[i];

                //Get save data for this level; if it doesn't exist, use defaults
                if (SaveData.GetLevelData(worldID, lvlDescData.LevelID, out LevelData lvlData) == false)
                {
                    lvlData = new LevelData();
                }

                if (lvlData.Complete == true)
                {
                    worldInfo.LevelsCompleted++;
                    worldInfo.TotalMoves += lvlData.LvlStats.NumMoves;
                    worldInfo.TotalTime += lvlData.LvlStats.LevelTime;
                }

                //Set up the entry for each level
                SetUpLevelEntry(lvlData, lvlDescData, container, lvlIndex, index);

                lvlIndex++;
            }

            AddMenuElement(new UIMenuOption(OptionSelected, null, null, OptionChanged));

            WorldInfoContainer.AddElement(container);
            IndividualWorldContainers.Add(container);
        }

        private void SetUpLevelEntry(in LevelData levelData, in LevelDescriptionData levelDescData,
            UIRenderableContainer container, in int index, in int worldIndex)
        {
            LevelStats lvlStats = levelData.LvlStats;

            Vector2 headerScale = Vector2.One;
            Vector2 statsScale = Vector2.One;

            string headerText = $"{levelDescData.LevelDisplayName} {levelDescData.LevelDescription}";

            UIText levelHeader = UIHelpers.CreateUIText(EntryFont, headerText, Vector2.Zero, headerScale, TextColor);
            //levelHeader.TintColor = (levelData.Complete == true) ? TextColor : IncompleteLevelColor;

            Vector2 headerPos = container.Position + LevelHeaderPosOffset + new Vector2(LevelHeaderXDiff * (index % Columns), LevelHeaderYDiff * (index / Columns));
            Vector2 movesPos = headerPos + new Vector2(0f, StatsYOffset);
            Vector2 timePos = movesPos + new Vector2(70f, 0f);

            //Create a container to hold all the level information
            int levelContainerCapacity = levelData.Complete == true ? 5 : 2;
            UIRenderableContainer levelContainer = new UIRenderableContainer(levelContainerCapacity, headerPos);
            container.AddElement(levelContainer);

            levelContainer.AddElement(levelHeader, headerPos);

            IndividualStatText[worldIndex].Add(levelHeader);

            if (levelData.Complete == true)
            {
                UISprite movesIcon = new UISprite(MovesIconSmall);
                UIText movesStats = UIHelpers.CreateUIText(EntryFont, lvlStats.NumMoves.ToString(), Vector2.Zero, statsScale,
                    Color.Black);

                UISprite timeIcon = new UISprite(TimeIconSmall);
                UIText timeStats = UIHelpers.CreateUIText(EntryFont, LevelGlobals.GetFormattedLevelTimeString(lvlStats.LevelTime),
                    Vector2.Zero, statsScale, Color.Black);

                levelContainer.AddElement(movesStats, movesPos + new Vector2(30f, 0));
                levelContainer.AddElement(timeStats, timePos + new Vector2(20f, 0f));

                IndividualStatText[worldIndex].Add(movesStats);
                IndividualStatText[worldIndex].Add(timeStats);

                levelContainer.AddElement(movesIcon, movesPos + -Vector2.UnitY);
                levelContainer.AddElement(timeIcon, timePos + -Vector2.UnitY);

                IndividualStatSprites[worldIndex].Add(movesIcon);
                IndividualStatSprites[worldIndex].Add(timeIcon);
            }
            else
            {
                UIText incompleteStatsText = UIHelpers.CreateUIText(EntryFont, "Incomplete", Vector2.Zero, statsScale, IncompleteLevelColor);
                levelContainer.AddElement(incompleteStatsText, movesPos + new Vector2(10f, 0f));

                IndividualStatText[worldIndex].Add(incompleteStatsText);
            }
        }

        private void PopulateWorldInfo(int index)
        {
            WorldInfo worldInfo = WorldInfoList[index];

            WorldName.Text = $"World {worldInfo.WorldID + 1}";
            WorldLevels.Text = $"{worldInfo.LevelsCompleted}/{worldInfo.TotalLevels}";

            WorldLevels.TintColor = (worldInfo.LevelsCompleted == worldInfo.TotalLevels)
                ? OverworldPausedState.AllLevelsCompleteColor : OverworldPausedState.AllLevelsColor;

            WorldMoves.Text = worldInfo.TotalMoves.ToString();
            WorldTime.Text = LevelGlobals.GetFormattedLevelTimeString(worldInfo.TotalTime);

            WorldInfoContainer.Position = Position - new Vector2(index * RenderingGlobals.BaseResolutionWidth, 0f);

            if (worldInfo.WorldID != StartingWorldID)
            {
                TravelToWorld.TintColor = TravelToWorldTextColor;
                //TravelArrow.TintColor = TravelToWorldArrowColor;
            }
            else
            {
                TravelToWorld.TintColor = TravelToWorldTextColorDisabled;
                //TravelArrow.TintColor = TravelToWorldArrowColorDisabled;
            }
        }

        private void OnOptionSelected(in int optionIndex)
        {
            PopulateWorldInfo(CurSelection);
            OnOptionChanged(0);

            LeftArrow.Visible = CurSelection > 0;
            RightArrow.Visible = (CurSelection < (MenuOptions.Count - 1));
        }

        private void OnOptionDeselected(in int optionIndex)
        {
            
        }

        private void OnOptionChanged(in int amount)
        {
            int newVal = WorldSelectionOffsets[CurSelection] + amount;
            WorldSelectionOffsets[CurSelection] = newVal;

            UIRenderableContainer worldContainer = IndividualWorldContainers[CurSelection];

            int levelCount = WorldInfoList[CurSelection].TotalLevels;

            //Starting at 2 is due to the first element being the panel and the 2nd being the divider
            int elementCount = worldContainer.Elements.Count;
            for (int i = 2; i < elementCount; i++)
            {
                worldContainer.Elements[i].Position -= new Vector2(0f, amount * (LevelHeaderYDiff / Columns));

                int index = (i - 2);

                //Only set entries in the panel range to be visible
                bool visible = (index >= newVal && (index - newVal) < LevelsInPanel);

                //Set all elements in the container invisible
                //The sprites and text are in separate lists and rendered individually to increase performance
                UIRenderableContainer container = (UIRenderableContainer)worldContainer.Elements[i];

                for (int j = 0; j < container.Elements.Count; j++)
                {
                    UIRenderable element = container.Elements[j];
                    element.Visible = visible;
                }
            }

            UpArrow.Visible = newVal > 0;
            DownArrow.Visible = (newVal + LevelsInPanel) < levelCount;
        }

        protected override void HandleCursorInput()
        {
            if (Input.GetButtonDown(InputActions.Left) == true)
            {
                if (CurSelection > 0)
                    ChangeSelection(-OptionSelectionAmount, true);
            }
            else if (Input.GetButtonDown(InputActions.Right) == true)
            {
                if (CurSelection < (MenuOptions.Count - 1))
                    ChangeSelection(OptionSelectionAmount, true);
            }
            else if (Input.GetButtonDown(InputActions.Down) == true)
            {
                if ((WorldSelectionOffsets[CurSelection] + LevelsInPanel) < WorldInfoList[CurSelection].TotalLevels)
                    ChangeOption(OptionChangeAmount, true);
            }
            else if (Input.GetButtonDown(InputActions.Up) == true)
            {
                if (WorldSelectionOffsets[CurSelection] > 0)
                    ChangeOption(-OptionChangeAmount, true);
            }

            if (Input.GetButtonDown(InputActions.Select) == true)
            {
                int selectionWorldID = WorldInfoList[CurSelection].WorldID;

                //Allow traveling to a world that's not this one as a shortcut
                if (selectionWorldID != StartingWorldID)
                {
                    //Unload all content when going to a different world
                    SoundManager.Instance.ClearAllSounds();
                    SoundManager.Instance.ClearMusicCache();
                    
                    AssetManager.Instance.UnloadLoadedContent();

                    string worldToLoad = DataHandler.levelDescData.WorldDescriptions[WorldInfoList[CurSelection].WorldID].MapToLoad;
                    MonoGame.Extended.Tiled.TiledMap tiledMap = AssetManager.Instance.LoadTiledMap(worldToLoad);

                    //Start at the beginning of each world
                    DataHandler.saveData.LastPlayedLevel = int.MinValue;

                    SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false);

                    GameStateManager.Instance.ChangeGameState(new OverworldState(tiledMap));
                }
            }
        }

        public override void TransitionUpdate()
        {
            base.TransitionUpdate();

            UpdateArrowColor();

            //TravelArrow.Position = TravelToWorld.Position + TravelArrowOffset;

            if (StartingWorldID != WorldInfoList[CurSelection].WorldID)
            {
                UpdateTravelTextHover();
            }
        }

        public override void Update()
        {
            base.Update();

            UpdateArrowColor();

            //TravelArrow.Position = TravelToWorld.Position + TravelArrowOffset;

            if (StartingWorldID != WorldInfoList[CurSelection].WorldID)
            {
                UpdateTravelTextHover();
            }
        }

        private void UpdateArrowColor()
        {
            ElapsedFadeTime += Time.ElapsedTime.TotalMilliseconds;

            double time = UtilityGlobals.PingPong(ElapsedFadeTime / (ArrowFadeTime / 2), 0d, 1d);
            ArrowColor = Interpolation.Interpolate(ArrowColorStart, ArrowColorEnd, time, Interpolation.InterpolationTypes.Linear);

            UpArrow.TintColor = ArrowColor;
            DownArrow.TintColor = ArrowColor;
            LeftArrow.TintColor = ArrowColor;
            RightArrow.TintColor = ArrowColor;
        }

        private void UpdateTravelTextHover()
        {
            ElapsedTravelTime += Time.ElapsedTime.TotalMilliseconds;
            double time = UtilityGlobals.PingPong(ElapsedTravelTime / TravelTime, 0d, 1d);
            TravelToWorld.Position = Interpolation.Interpolate(Position + TravelTextStartPosRelative + TravelTextStartBob, Position + TravelTextStartPosRelative + TravelTextEndBob,
                time, Interpolation.InterpolationTypes.QuadInOut);
            TravelToWorld.Position.Round();
        }

        private void OnKeyboardMappingsChanged(in int kbIndex)
        {
            InputHandler inputHandler = Input.GetInputHandler(kbIndex);
            if (inputHandler == null)
            {
                return;
            }

            string selectStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.Select);
            //string backOutStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.BackOut);

            TravelToWorld.Text = $"Warp ({selectStr})";///{backOutStr}";
        }

        public override void Render()
        {
            if (UpArrow.Visible == true)
                UpArrow.Render();
            if (DownArrow.Visible == true)
                DownArrow.Render();
            if (LeftArrow.Visible == true)
                LeftArrow.Render();
            if (RightArrow.Visible == true)
                RightArrow.Render();

            //if (TravelArrow.Visible == true)
            //    TravelArrow.Render();

            MovesIconBig.Render();
            TimeIconBig.Render();

            for (int i = 0; i < IndividualStatSprites[CurSelection].Count; i++)
            {
                UISprite sprite = IndividualStatSprites[CurSelection][i];
                if (sprite.Visible == true)
                    sprite.Render();
            }

            for (int i = 0; i < IndividualStatText[CurSelection].Count; i++)
            {
                UIText text = IndividualStatText[CurSelection][i];
                if (text.Visible == true)
                    text.Render();
            }

            //WorldInfoContainer.Elements[CurSelection].Render();

            WorldName.Render();
            WorldLevels.Render();
            WorldMoves.Render();
            WorldTime.Render();

            if (TravelToWorld.Visible == true)
                TravelToWorld.Render();
        }
    }
}
