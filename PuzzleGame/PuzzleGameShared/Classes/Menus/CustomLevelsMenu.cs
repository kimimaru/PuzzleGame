﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Tiled;
using MonoGame.Extended.Content.Pipeline.Tiled;

namespace PuzzleGame
{
    public class CustomLevelsMenu : InputMenu
    {
        public CustomLevelsMenu(in MenuState menustate)
        {
            MenuGameState = menustate;
            TextScale = .6f;
            YOffset = -50f;
            YSpacing = 35f;

            AddMenuOption(new InputMenuOption("Return", Return));

            LoadCustomLevels();
        }

        private void Return()
        {
            MenuGameState.PopMenu();
        }

        private void LoadCustomLevels()
        {
            if (Directory.Exists(ContentGlobals.CustomLevelPath) == false)
            {
                Debug.LogError($"Path {ContentGlobals.CustomLevelPath} doesn't exist!");
                return;
            }

            string[] tmxFiles = Directory.GetFiles(ContentGlobals.CustomLevelPath, "*.tmx", SearchOption.TopDirectoryOnly);

            for (int i = 0; i < tmxFiles.Length; i++)
            {
                string fileName = tmxFiles[i].Remove(0, ContentGlobals.CustomLevelPath.Length).Replace(".tmx", string.Empty);
                AddMenuOption(new InputMenuOption(fileName, LoadLevel));
            }
        }

        private void LoadLevel()
        {
            //Try to load the level
            string levelName = MenuOptions[CurSelection].Text;
            TiledMapContent level = CustomLevelLoader.LoadLevel(levelName);
            if (level == null)
            {
                Debug.LogError($"Error loading level {levelName}");
                return;
            }
            
            TiledMap map = AssetManager.Instance.LoadAsset<TiledMap>("CustomLevels/" + levelName);
            
            Engine.LoadInGameContent();
            GameStateManager.Instance.ChangeGameState(new IngameState(map, LevelEventTypes.Base));
        }
    }
}
