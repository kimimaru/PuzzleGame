﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// Class for global values dealing with game configuration.
    /// </summary>
    public static class ConfigGlobals
    {
        public const string ApplicationDataFolderRoot = "Kimimaru";
        public const string ApplicationDataFolderName = Engine.GameName;
        private static string ApplicationDataPath = string.Empty;

        public const string ConfigRoot = "Config";
        private const string ControllerMappingFileName = "ControllerMappings.txt";
        public static readonly string ControllerMappingFile = ConfigRoot + "/" + ControllerMappingFileName;

        public const string InputConfigFileName = "InputConfig.txt";

        private static string InputConfigFolderPath = string.Empty;
        private static string InputConfigFullPath = string.Empty;

        private static string InstallDataFolderPath = string.Empty;

        public static string GetInstallDataPath()
        {
            if (string.IsNullOrEmpty(InstallDataFolderPath) == true)
            {
                InstallDataFolderPath = AppDomain.CurrentDomain.BaseDirectory;

#if MACOS && !WINDOWS && !LINUX
                try
                {
                    string resourcesPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..", "Resources");

                    if (Directory.Exists(resourcesPath) == true)
                    {
                        InstallDataFolderPath = resourcesPath;
                    }
                }
                catch (Exception e) when (e is ArgumentException || e is PlatformNotSupportedException || e is ArgumentNullException)
                {
                    Debug.LogError($"Error combining paths for install data folder; resorting to executable's local directory. Message: {e.Message}");
                    InstallDataFolderPath = AppDomain.CurrentDomain.BaseDirectory;
                }
#endif
            }

            return InstallDataFolderPath;
        }

        /// <summary>
        /// Gets the path where the game stores its data.
        /// </summary>
        /// <returns>A string representing the path the game stores its data.</returns>
        public static string GetApplicationDataPath()
        {
            if (string.IsNullOrEmpty(ApplicationDataPath))
            {
                try
                {
                    //macOS typically has local application data in "~/Library/Application Support", while Mono would place it in "~/.local/share"
#if MACOS && !WINDOWS && !LINUX
                    string applicationDataPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Library", "Application Support");
#else
                    string applicationDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
#endif
                    ApplicationDataPath = System.IO.Path.Combine(applicationDataPath, ApplicationDataFolderRoot, ApplicationDataFolderName);
                }
                catch (Exception e) when (e is ArgumentException || e is PlatformNotSupportedException || e is ArgumentNullException)
                {
                    Debug.LogError($"Error retrieving application data folder path; resorting to local game folder as path. Message: {e.Message}");
                    ApplicationDataPath = string.Empty;
                }
            }

            return ApplicationDataPath;
        }

        /// <summary>
        /// Loads custom controller mappings from the config file.
        /// </summary>
        /// <returns>An int representing how many controller mappings were added from the config file.</returns>
        public static int LoadControllerMappingsFromConfig()
        {
            string controllerMappingPath = ControllerMappingFile;// GetInputConfigFolderPath();

            try
            {
                controllerMappingPath = Path.Combine(GetApplicationDataPath(), ControllerMappingFile);
            }
            catch (Exception e) when (e is ArgumentException || e is PlatformNotSupportedException || e is ArgumentNullException)
            {
                Debug.LogError($"Error retrieving controller mapping file path; resorting to local game folder. Message: {e.Message}");
                controllerMappingPath = ControllerMappingFile;
            }

            if (File.Exists(controllerMappingPath) == false)
            {
                Debug.Log("Controller mapping file does not exist; no additional mappings loaded");
                return 0;
            }

            int mappingsAdded = 0;

            try
            {
                mappingsAdded = NativeGlobals.AddGameControllerMappings(controllerMappingPath);
            }
            catch (Exception e)
            {
                Debug.LogError($"Encountered problem adding controller mappings: {e.Message}");
                mappingsAdded = 0;
            }

            return mappingsAdded;
        }

        /// <summary>
        /// Returns the full path for the input config folder.
        /// </summary>
        public static string GetInputConfigFolderPath()
        {
            if (string.IsNullOrEmpty(InputConfigFolderPath) == true)
            {
                try
                {
                    string applicationDataPath = ConfigGlobals.GetApplicationDataPath();
                    InputConfigFolderPath = Path.Combine(applicationDataPath, ConfigRoot);
                }
                catch (Exception e) when (e is ArgumentException || e is ArgumentNullException)
                {
                    Debug.LogError($"Error retrieving input config data folder path. Resorting to local game folder as path. Message: {e.Message}");
                    InputConfigFolderPath = ConfigRoot;
                }
            }

            return InputConfigFolderPath;
        }

        /// <summary>
        /// Returns the full path for the input config data file.
        /// </summary>
        public static string GetInputConfigDataPath()
        {
            if (string.IsNullOrEmpty(InputConfigFullPath) == true)
            {
                try
                {
                    string folderPath = GetInputConfigFolderPath();
                    InputConfigFullPath = Path.Combine(folderPath, InputConfigFileName);
                }
                catch (Exception e) when (e is ArgumentException || e is ArgumentNullException)
                {
                    Debug.LogError($"Error retrieving input config data file path. Resorting to local game folder. Message: {e.Message}");
                    InputConfigFullPath = InputConfigFileName;
                }
            }

            return InputConfigFullPath;
        }
    }
}
