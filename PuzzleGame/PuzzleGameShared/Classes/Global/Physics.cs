﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// Static class handling anything physics related.
    /// </summary>
    public static class Physics
    {
        /// <summary>
        /// Gravity value.
        /// </summary>
        public static Vector2 Gravity = new Vector2(0f, .2f);
    }
}
