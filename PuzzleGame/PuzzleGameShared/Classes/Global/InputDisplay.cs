﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// Displays which inputs are being pressed.
    /// </summary>
    public sealed class InputDisplay : IUpdateable
    {
        public SpriteFont Font = null;
        private string[] InputStrings = null;
        private List<int> IndicesPressed = null;
        private Vector2 StartDrawPos = Vector2.Zero;
        private Vector2 IndexDrawPosDiff = Vector2.Zero;
        private Vector2 TextOrigin = Vector2.Zero;

        public bool IsDebug = false;

        /// <summary>
        /// The amount of time, in milliseconds, to wait before updating the input display.
        /// </summary>
        private double UpdateInterval = 5d;
        private double PrevUpdateVal = 0d;

        public InputDisplay(SpriteFont font, string[] inputStrings, in double updateInterval,
            in Vector2 startDrawPos, in Vector2 indexDrawPosDiff, in Vector2 textOrigin, in bool isDebug)
        {
            Font = font;
            InputStrings = inputStrings;
            UpdateInterval = updateInterval;
            StartDrawPos = startDrawPos;
            IndexDrawPosDiff = indexDrawPosDiff;
            TextOrigin = textOrigin;
            IsDebug = isDebug;

            IndicesPressed = new List<int>(InputStrings.Length);
        }

        public void SetFont(SpriteFont font)
        {
            Font = font;
        }

        public void Update()
        {
            PrevUpdateVal += Time.ElapsedTime.TotalMilliseconds;

            if (PrevUpdateVal < UpdateInterval)
            {
                return;
            }

            IndicesPressed.Clear();
            for (int i = 0; i < InputStrings.Length; i++)
            {
                if (Input.GetButton(InputStrings[i]) == true)
                {
                    IndicesPressed.Add(i);
                }
            }

            PrevUpdateVal = 0d;
        }

        public void Draw()
        {
            for (int i = 0; i < IndicesPressed.Count; i++)
            {
                int val = IndicesPressed[i];
                string inputStr = InputStrings[val];
                Vector2 origin = Font.GetOrigin(inputStr, TextOrigin.X, TextOrigin.Y);

                SpriteBatch batch = RenderingManager.Instance.spriteBatch;
                if (IsDebug == true)
                {
                    batch = Debug.DebugUIBatch;
                }

                batch.DrawString(Font, inputStr, StartDrawPos + (IndexDrawPosDiff * val), Color.White, 0f, origin,
                    1f, SpriteEffects.None, .5f);
            }
        }
    }
}
