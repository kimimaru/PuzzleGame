﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// A Transform. It has a position, rotation, and scale.
    /// </summary>
    public class Transform : IPosition, IRotatable, IScalable
    {
        [Recordable(RecordableFlags.None)]
        public Vector2 Position { get; set; } = Vector2.Zero;
        [Recordable(RecordableFlags.None)]
        public float Rotation { get; set; } = 0f;
        [Recordable(RecordableFlags.None)]
        public Vector2 Scale { get; set; } = Vector2.One;

        public Transform()
        {

        }

        public Transform(in Vector2 position, in float rotation, in Vector2 scale)
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;
        }
    }
}
