﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// Class for global values dealing with rendering
    /// </summary>
    public static class RenderingGlobals
    {
        public const int BaseResolutionWidth = 640;
        public const int BaseResolutionHeight = 480;

        public const int BaseResWidthHalved = BaseResolutionWidth / 2;
        public const int BaseResHeightHalved = BaseResolutionHeight / 2;

        private static readonly Vector2 Resolution = new Vector2(BaseResolutionWidth, BaseResolutionHeight);
        private static readonly Vector2 ResolutionHalf = Resolution / 2;

        public static ref readonly Vector2 BaseResolution => ref Resolution;
        public static ref readonly Vector2 BaseResolutionHalved => ref ResolutionHalf;

        /// <summary>
        /// Resizes a RenderTarget by disposing it and pointing it to a new RenderTarget instance with the desired size.
        /// <para>If the RenderTarget is already the new size, nothing happens.
        /// If the RenderTarget is null, it will be created.</para>
        /// </summary>
        /// <param name="renderTarget">The RenderTarget to resize.</param>
        /// <param name="newSize">The new size of the RenderTarget.</param>
        public static void ResizeRenderTarget(ref RenderTarget2D renderTarget, in Vector2 newSize)
        {
            int newWidth = (int)newSize.X;
            int newHeight = (int)newSize.Y;

            //Check if the RenderTarget isn't null nor disposed
            //If it's either null or disposed, it's invalid and a new one should be created regardless
            if (renderTarget != null && renderTarget.IsDisposed == false)
            {
                //Return if the RenderTarget is already this size
                if (renderTarget.Width == newWidth && renderTarget.Height == newHeight)
                    return;

                //Dispose the current RenderTarget, as they're not resizable
                renderTarget.Dispose();
            }

            //Point the reference to a new RenderTarget with the new size
            renderTarget = new RenderTarget2D(RenderingManager.Instance.graphicsDevice, newWidth, newHeight);
        }

        public static void DrawRenderTargetToOther(RenderTarget2D targetToDrawOn, RenderTarget2D targetToDraw, Effect shader)
        {
            RenderingManager.Instance.graphicsDevice.SetRenderTarget(targetToDrawOn);
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred, BlendState.AlphaBlend,
                SamplerState.PointClamp, null, null, shader, null);
            RenderingManager.Instance.DrawSprite(targetToDraw, new Rectangle(0, 0, targetToDraw.Width, targetToDraw.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0f);
            RenderingManager.Instance.EndCurrentBatch();
        }
    }
}
