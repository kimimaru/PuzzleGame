﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// Lightweight data representing a Transform.
    /// </summary>
    public struct TransformData
    {
        public Vector2 Position;
        public float Rotation;
        public Vector2 Scale;

        public TransformData(in Vector2 position, in float rotation, in Vector2 scale)
        {
            Position = position;
            Rotation = rotation;
            Scale = scale;
        }

        public TransformData(in Transform transform) : this(transform.Position, transform.Rotation, transform.Scale)
        {

        }

        public override bool Equals(object obj)
        {
            if (obj is TransformData transformData)
            {
                return (this == transformData);
            }

            return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 23;

                hash = (hash * 29) + Position.GetHashCode();
                hash = (hash * 29) + Rotation.GetHashCode();
                hash = (hash * 29) + Scale.GetHashCode();

                return hash;
            }
        }

        public static bool operator ==(TransformData a, TransformData b)
        {
            return (a.Position == b.Position && a.Rotation == b.Rotation && a.Scale == b.Scale);
        }

        public static bool operator !=(TransformData a, TransformData b)
        {
            return !(a == b);
        }
    }
}
