﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// Class for global values dealing with loading and unloading content
    /// </summary>
    public static class ContentGlobals
    {
        public const string ProjectRoot = ContentRoot + "../";

        public const string ContentRoot = "Content/";
        public const string AudioRoot = "Audio/";
        public const string SFXRoot = AudioRoot + "SFX/";
        public const string MusicRoot = AudioRoot + "Music/";
        public const string SpriteRoot = "Sprites/";
        public const string TilesetRoot = SpriteRoot + "Tilesets/";
        public const string UIRoot = "UI/";
        public const string ShaderRoot = "Shaders/";
        public const string ShaderTextureRoot = ShaderRoot + "ShaderTextures/";
        public const string FontRoot = "Fonts/";
        public const string MapRoot = "TiledMaps/";
        public const string CustomLevelPath = ContentRoot + "CustomLevels/";

#if DEBUG
        private const string SaveDataFileName = "SaveData.txt";
#else
        private const string SaveDataFileName = "SaveData.sav";
#endif
        private static string SaveDataFullPath = string.Empty;

        /// <summary>
        /// Returns the full path for the save data file.
        /// </summary>
        public static string GetSaveDataPath()
        {
            if (string.IsNullOrEmpty(SaveDataFullPath) == true)
            {
                try
                {
                    string folderPath = ConfigGlobals.GetApplicationDataPath();
                    SaveDataFullPath = System.IO.Path.Combine(folderPath, SaveDataFileName);
                }
                catch (Exception e) when (e is ArgumentException || e is ArgumentNullException)
                {
                    Debug.LogError($"Error retrieving save data file path. Resorting to local game folder as path. Message: {e.Message}");
                    SaveDataFullPath = SaveDataFileName;
                }
            }

            //Debug.Log($"Save Data Path: {SaveDataFullPath}");
            return SaveDataFullPath;
        }

        #region Specific Content

        public const string EchidnaTex = "Echidna";
        public const string LevelObjectsTex = "LevelObjects_v2";
        public const string UITex = "UI";
        public const string LevelTileset = TilesetRoot + "Tileset";
        public const string BGIcons = "BGIcons";

        public const string TitleMusic = "Puzzle-Caves";
        public const string DefaultLevelMusic = "Puzzle-Dreams";
        public const string DefaultOverworldMusic = "Digital-Dreaming";
        public const string LevelCompleteMusic = "Level-Complete";
        public const string CreditsMusic = "Mellow-Puzzler";

        public const string MenuOptionChangeSound = "50561__broumbroum__sf3-sfx-menu-select";
        public const string MenuOptionSelectSound = "328120__kianda__powerup";
        public const string MenuOptionBackSound = "plasterbrain_cartoon-ui-back-cancel";

        public const string UndoSound = "lloydevans09_warping";
        public const string SwitchPressedSound = "164549__adam-n__switch-click-4";
        public const string CorrectSpotSound = "404359__kagateni__success2";
        public const string IncorrectSpotSound = "249618__vincentm400__invalid";
        public const string WarpSound = "richerlandtv__gametinywarp";
        public const string ExclamationSound = "wjoojoo_tink01";
        public const string QuestionSound = "plasterbrain_8bit-question-mark";
        public const string RumbleSound = "terry93d_8-bit-rumbling";
        public const string GameOverSound = "myfox14__game-over-arcade";
        public const string RockBreakSound = "wim__stonebreak01";
        public const string RockShootSound = "leszek_szary-shoot";
        public const string RockEnterPipeSound = "RockEnterPipe";
        public const string RockHitHardSound = "RockHitHard";
        public const string ConfirmSound = "completetask_0";
        public const string TallySound = "Tally";
        public const string BlockMoveSound = "Block_Move2";
        public const string BlockIceMoveSound = "Block_Move";
        public const float BlockMoveSoundPitch = .3f;
        public const float BlockIceMoveSoundPitch = 1f;
        public const float BlockMoveSoundVolume = .8f;
        public const float BlockIceMoveSoundVolume = .8f;
        public const int BlockMoveSoundLimit = 1;
        public const int BlockIceMoveSoundLimit = 1;
        public const int BlockCorrectSpotSoundLimit = 1;
        public const int BlockIncorrectSpotSoundLimit = 1;
        public const string LevelUnlockedSound = "jingle-win-synth-00";

        public const string DefaultFont24px = "PixelFont";
        public const string DefaultFont18px = "PixelFont2";
        public const string DefaultFont10px = "PixelFont3";
        public const string DefaultFont13px = "PixelFont4";
        public const string DefaultFont21px = "PixelFont5";
        public const string DefaultFont12px = "PixelFont6";
        public const string DefaultFont17px = "PixelFont7";
        public const string DefaultFont15px = "PixelFont8";

        public const string SpecialFont19px = "PixelFontSpecial";
        public const string SpecialFont13px = "PixelFontSpecial2";
        public const string DebugFont = "Font";

        public const string DistortShader = "Distort";

        #region Title and Logo

        public static readonly Rectangle TitleBGRect = new Rectangle(0, 0, 8, 8);
        public static readonly Rectangle MBLogoRect = new Rectangle(37, 3, 212, 107);

        #endregion

        #region UI

        public static readonly Rectangle BoxRect = new Rectangle(0, 0, 1, 1);
        public static readonly Rectangle PanelRect = new Rectangle(1, 0, 32, 32);
        public static readonly Rectangle ArrowRect = new Rectangle(34, 18, 16, 14);
        public static readonly Rectangle PinkArrowRect = new Rectangle(34, 1, 13, 15);

        public static readonly Rectangle MovesIconRectSmall = new Rectangle(1, 33, 27, 16);
        public static readonly Rectangle MovesIconRectLarge = new Rectangle(79, 27, 47, 32);

        public static readonly Rectangle TimeIconRectSmall = new Rectangle(34, 33, 17, 17);
        public static readonly Rectangle TimeIconRectLarge = new Rectangle(94, 61, 33, 33);

        public static readonly Rectangle MonoGameLogoRect = new Rectangle(6, 120, 128, 128);

        #endregion

        #region Cutscene

        public static readonly Vector2 CSBubbleOffset = new Vector2(18f, -24f);
        public static readonly Rectangle CSBubbleExclamationRect = new Rectangle(81, 1, 22, 22);
        public static readonly Rectangle CSBubbleQuestionRect = new Rectangle(104, 1, 22, 22);
        public static readonly Rectangle CSExclamationRect = new Rectangle(59, 1, 6, 17);
        public static readonly Rectangle CSQuestionRect = new Rectangle(67, 1, 12, 17);

        public const string CSJumpSound = "lefty-studios_jumping";
        public const string CSPulseSound = "ambient_techno1";

        #endregion

        #region Cutscene Levels

        public const string TitleLevel = "Title";
        public const string TitleDemoLevel = "TitleDemo";
        public const string IntroCutscene1Level = "IC1";
        public const string IntroCutscene2Level = "IC2";
        public const string EndingCutsceneLevel = "EC";
        public const string CreditsCutsceneLevel = "CC";
        public const string BonusUnlockLevel = "UB";

        public const string PostBoss1CutsceneLevel = "PB1C";
        public const string PostBoss2CutsceneLevel = "PB2C";
        public const string PostBoss3CutsceneLevel = "PB3C";
        public const string PostBoss4CutsceneLevel = "PB4C";
        public const string PostBoss5CutsceneLevel = "PB5C";
        public const string PostBoss6CutsceneLevel = "PB6C";

        #endregion

        #region Object Animations

        public static readonly Rectangle BlockNormalRect = new Rectangle(9, 16, 32, 32);
        public static readonly Rectangle BlockPairedRect = new Rectangle(41, 16, 32, 32);
        public static readonly Rectangle BlockPipeStartRect = new Rectangle(109, 16, 32, 32);
        public static readonly Rectangle BlockUngrabbableRect = new Rectangle(73, 16, 32, 32);
        public const int BlockPipeUngrabbableYOffset = 112;

        public static readonly Rectangle BlockPatternStartRect = new Rectangle(203, 95, 32, 32);

        public static readonly Rectangle BlockCorrectAnim1Rect = new Rectangle(107, 95, 32, 32);
        public static readonly Rectangle BlockCorrectAnim2Rect = new Rectangle(139, 95, 32, 32);
        public static readonly Rectangle BlockCorrectAnim3Rect = new Rectangle(171, 95, 32, 32);

        public static readonly Rectangle BlockIncorrectAnim1Rect = new Rectangle(9, 128, 32, 32);
        public static readonly Rectangle BlockIncorrectAnim2Rect = new Rectangle(41, 128, 32, 32);
        public static readonly Rectangle BlockIncorrectAnim3Rect = new Rectangle(73, 128, 32, 32);

        public static readonly Rectangle MudDecalRect = new Rectangle(41, 1, 32, 8);
        public static readonly Rectangle IceDecalRect = new Rectangle(9, 1, 32, 8);

        public static readonly Rectangle PortalAnim1Rect = new Rectangle(9, 61, 21, 21);
        public static readonly Rectangle PortalAnim2Rect = new Rectangle(30, 61, 21, 21);
        public static readonly Rectangle PortalAnim3Rect = new Rectangle(51, 61, 21, 21);

        public static readonly Rectangle PortalActiveAnim1Rect = new Rectangle(77, 51, 21, 31);
        public static readonly Rectangle PortalActiveAnim2Rect = new Rectangle(98, 51, 21, 31);
        public static readonly Rectangle PortalActiveAnim3Rect = new Rectangle(119, 51, 21, 31);

        public static readonly Rectangle PortalBossAnim1Rect = new Rectangle(9, 173, 21, 21);
        public static readonly Rectangle PortalBossAnim2Rect = new Rectangle(30, 173, 21, 21);
        public static readonly Rectangle PortalBossAnim3Rect = new Rectangle(51, 173, 21, 21);

        public static readonly Rectangle PortalBossActiveAnim1Rect = new Rectangle(77, 163, 21, 31);
        public static readonly Rectangle PortalBossActiveAnim2Rect = new Rectangle(98, 163, 21, 31);
        public static readonly Rectangle PortalBossActiveAnim3Rect = new Rectangle(119, 163, 21, 31);

        public static readonly Rectangle SpotStartRect = new Rectangle(9, 95, 32, 32);

        public static readonly Rectangle SwitchSpriteRect = new Rectangle(355, 56, 26, 26);
        public static readonly Rectangle SwitchBlueSpriteRect = new Rectangle(381, 56, 26, 26);

        public static readonly Rectangle WarpAnim1Rect = new Rectangle(146, 61, 21, 21);
        public static readonly Rectangle WarpAnim2Rect = new Rectangle(167, 61, 21, 21);
        public static readonly Rectangle WarpAnim3Rect = new Rectangle(188, 61, 21, 21);

        public static readonly Rectangle WarpWorldAnim2Rect = new Rectangle(211, 61, 21, 21);
        public static readonly Rectangle WarpWorldAnim3Rect = new Rectangle(232, 61, 21, 21);

        public static readonly Rectangle WarpDestSpriteRect = new Rectangle(255, 61, 21, 21);

        public static readonly Rectangle BlockParticleRect = new Rectangle(91, 5, 4, 4);
        public static readonly Rectangle PortalParticleRect = new Rectangle(77, 4, 5, 5);
        public static readonly Rectangle PortalBossParticleRect = new Rectangle(84, 4, 5, 5);
        public static readonly Rectangle WarpParticleRect = new Rectangle(435, 59, 21, 21);
        public static readonly Rectangle WarpWorldParticleRect = new Rectangle(457, 59, 21, 21);

        #region Mole

        public static readonly Rectangle MoleIdleAnimURect = new Rectangle(9, 349, 24, 27); //new Rectangle(9, 204, 12, 14);
        public static readonly Rectangle MoleIdleAnimDRect = new Rectangle(9, 382, 24, 28); //new Rectangle(9, 221, 12, 14);
        public static readonly Rectangle MoleIdleAnimLRect = new Rectangle(10, 416, 23, 28); //new Rectangle(10, 238, 11, 14);
        public static readonly Rectangle MoleIdleAnimRRect = new Rectangle(10, 450, 23, 28); //new Rectangle(10, 255, 11, 14);

        public static readonly Rectangle MoleThrowAnimU1Rect = new Rectangle(41, 349, 24, 27); //new Rectangle(25, 204, 12, 15);
        public static readonly Rectangle MoleThrowAnimU2Rect = new Rectangle(73, 349, 24, 27); //new Rectangle(41, 204, 12, 14);
        public static readonly Rectangle MoleThrowAnimU3Rect = new Rectangle(103, 349, 28, 27); //new Rectangle(56, 205, 14, 13);

        public static readonly Rectangle MoleThrowAnimD1Rect = new Rectangle(41, 382, 24, 28); //new Rectangle(25, 221, 12, 14);
        public static readonly Rectangle MoleThrowAnimD2Rect = new Rectangle(73, 382, 24, 28); //new Rectangle(41, 221, 12, 14);
        public static readonly Rectangle MoleThrowAnimD3Rect = new Rectangle(105, 382, 24, 28); //new Rectangle(57, 221, 12, 14);

        public static readonly Rectangle MoleThrowAnimL1Rect = new Rectangle(36, 416, 27, 28); //new Rectangle(23, 238, 13, 14);
        public static readonly Rectangle MoleThrowAnimL2Rect = new Rectangle(70, 416, 23, 28); //new Rectangle(40, 238, 11, 14);
        public static readonly Rectangle MoleThrowAnimL3Rect = new Rectangle(99, 416, 28, 28); //new Rectangle(54, 238, 15, 14);

        public static readonly Rectangle MoleThrowAnimR1Rect = new Rectangle(40, 450, 27, 28); //new Rectangle(25, 255, 13, 14);
        public static readonly Rectangle MoleThrowAnimR2Rect = new Rectangle(70, 450, 23, 28); //new Rectangle(40, 255, 11, 14);
        public static readonly Rectangle MoleThrowAnimR3Rect = new Rectangle(95, 450, 29, 28); //new Rectangle(52, 255, 15, 14);

        public static readonly Rectangle MoleHurtAnimU1Rect = new Rectangle(134, 349, 27, 27); //new Rectangle(72, 205, 13, 13);
        public static readonly Rectangle MoleHurtAnimU2Rect = new Rectangle(166, 349, 31, 27); //new Rectangle(88, 205, 15, 13);

        public static readonly Rectangle MoleHurtAnimD1Rect = new Rectangle(137, 381, 25, 27); //new Rectangle(73, 221, 12, 13);
        public static readonly Rectangle MoleHurtAnimD2Rect = new Rectangle(163, 382, 32, 32); //new Rectangle(86, 221, 16, 16);

        public static readonly Rectangle MoleHurtAnimL1Rect = new Rectangle(132, 416, 27, 28); //new Rectangle(71, 238, 13, 14);
        public static readonly Rectangle MoleHurtAnimL2Rect = new Rectangle(163, 418, 32, 28); //new Rectangle(86, 239, 16, 14);

        public static readonly Rectangle MoleHurtAnimR1Rect = new Rectangle(132, 450, 27, 28); //new Rectangle(71, 255, 13, 14);
        public static readonly Rectangle MoleHurtAnimR2Rect = new Rectangle(163, 452, 32, 28); //new Rectangle(86, 256, 16, 14);

        #endregion

        #region Rock

        public static readonly Rectangle RockArrowAnim1Rect = new Rectangle(125, 277, 13, 14);
        public static readonly Rectangle RockArrowAnim2Rect = new Rectangle(141, 277, 13, 14);

        public static readonly Rectangle RockBreakAnim1Rect = new Rectangle(298, 349, 24, 24); //new Rectangle(178, 204, 16, 16);
        public static readonly Rectangle RockBreakAnim2Rect = new Rectangle(325, 349, 24, 24); //new Rectangle(196, 204, 16, 16);
        public static readonly Rectangle RockBreakAnim3Rect = new Rectangle(352, 349, 24, 24); //new Rectangle(214, 204, 16, 16);

        public static readonly Rectangle RockRollAnimU1Rect = new Rectangle(217, 349, 24, 24); //new Rectangle(124, 204, 16, 16);
        public static readonly Rectangle RockRollAnimU2Rect = new Rectangle(244, 349, 24, 24); //new Rectangle(142, 204, 16, 16);
        public static readonly Rectangle RockRollAnimU3Rect = new Rectangle(271, 349, 24, 24); //new Rectangle(160, 204, 16, 16);

        public static readonly Rectangle RockRollAnimD1Rect = new Rectangle(217, 376, 24, 24); //new Rectangle(124, 222, 16, 16);
        public static readonly Rectangle RockRollAnimD2Rect = new Rectangle(244, 376, 24, 24); //new Rectangle(142, 222, 16, 16);
        public static readonly Rectangle RockRollAnimD3Rect = new Rectangle(271, 376, 24, 24); //new Rectangle(160, 222, 16, 16);

        public static readonly Rectangle RockRollAnimL1Rect = new Rectangle(217, 403, 24, 24); //new Rectangle(124, 240, 16, 16);
        public static readonly Rectangle RockRollAnimL2Rect = new Rectangle(244, 403, 24, 24); //new Rectangle(142, 240, 16, 16);
        public static readonly Rectangle RockRollAnimL3Rect = new Rectangle(271, 403, 24, 24); //new Rectangle(160, 240, 16, 16);

        public static readonly Rectangle RockRollAnimR1Rect = new Rectangle(217, 430, 24, 24); //new Rectangle(124, 258, 16, 16);
        public static readonly Rectangle RockRollAnimR2Rect = new Rectangle(244, 430, 24, 24); //new Rectangle(142, 258, 16, 16);
        public static readonly Rectangle RockRollAnimR3Rect = new Rectangle(271, 430, 24, 24); //new Rectangle(160, 258, 16, 16);

        #endregion

        #region Echidna

        public static readonly Rectangle EchidnaIdleAnimURect = new Rectangle(28, 23, 14, 17);
        //public static readonly Rectangle EchidnaIdleAnimURect = new Rectangle(163, 5, 30, 28);
        public static readonly Rectangle EchidnaIdleAnimDRect = new Rectangle(28, 5, 14, 15);
        //public static readonly Rectangle EchidnaIdleAnimDRect = new Rectangle(3, 3, 30, 31);
        public static readonly Rectangle EchidnaIdleAnimLRect = new Rectangle(28, 63, 14, 16);
        //public static readonly Rectangle EchidnaIdleAnimLRect = new Rectangle(37, 199, 25, 28);
        public static readonly Rectangle EchidnaIdleAnimRRect = new Rectangle(28, 43, 14, 16);
        //public static readonly Rectangle EchidnaIdleAnimRRect = new Rectangle(69, 38, 26, 29);

        public static readonly Rectangle EchidnaWalkAnimU1Rect = new Rectangle(9, 23, 14, 18);
        //public static readonly Rectangle EchidnaWalkAnimU1Rect = new Rectangle(131, 6, 30, 29);
        public static readonly Rectangle EchidnaWalkAnimU2Rect = EchidnaIdleAnimURect;//new Rectangle(28, 23, 14, 17);
        //public static readonly Rectangle EchidnaWalkAnimU2Rect = EchidnaIdleAnimURect;
        public static readonly Rectangle EchidnaWalkAnimU3Rect = new Rectangle(47, 23, 14, 18);
        //public static readonly Rectangle EchidnaWalkAnimU3Rect = new Rectangle(3, 37, 30, 29);

        public static readonly Rectangle EchidnaWalkAnimD1Rect = new Rectangle(9, 5, 14, 16);
        //public static readonly Rectangle EchidnaWalkAnimD1Rect = new Rectangle(35, 3, 30, 34);
        public static readonly Rectangle EchidnaWalkAnimD2Rect = new Rectangle(28, 5, 14, 15);
        //public static readonly Rectangle EchidnaWalkAnimD2Rect = new Rectangle(3, 3, 30, 31);
        public static readonly Rectangle EchidnaWalkAnimD3Rect = new Rectangle(47, 5, 14, 16);
        //public static readonly Rectangle EchidnaWalkAnimD3Rect = new Rectangle(99, 3, 30, 34);

        public static readonly Rectangle EchidnaWalkAnimL1Rect = new Rectangle(9, 63, 14, 18);
        //public static readonly Rectangle EchidnaWalkAnimL1Rect = new Rectangle(69, 199, 25, 30);
        public static readonly Rectangle EchidnaWalkAnimL2Rect = EchidnaIdleAnimLRect;//new Rectangle(28, 63, 14, 16);
        //public static readonly Rectangle EchidnaWalkAnimL2Rect = EchidnaIdleAnimLRect;
        public static readonly Rectangle EchidnaWalkAnimL3Rect = new Rectangle(47, 63, 14, 18);
        //public static readonly Rectangle EchidnaWalkAnimL3Rect = new Rectangle(5, 199, 25, 30);

        public static readonly Rectangle EchidnaWalkAnimR1Rect = new Rectangle(9, 43, 14, 18);
        //public static readonly Rectangle EchidnaWalkAnimR1Rect = new Rectangle(101, 38, 26, 30);
        public static readonly Rectangle EchidnaWalkAnimR2Rect = EchidnaIdleAnimRRect;//new Rectangle(28, 43, 14, 16);
        //public static readonly Rectangle EchidnaWalkAnimR2Rect = EchidnaIdleAnimRRect;//new Rectangle(69, 38, 25, 28);
        public static readonly Rectangle EchidnaWalkAnimR3Rect = new Rectangle(47, 43, 14, 18);
        //public static readonly Rectangle EchidnaWalkAnimR3Rect = new Rectangle(36, 39, 27, 30);

        public static readonly Rectangle EchidnaGrabAnimURect = new Rectangle(85, 23, 14, 17);
        public static readonly Rectangle EchidnaGrabAnimDRect = new Rectangle(85, 4, 14, 16);
        public static readonly Rectangle EchidnaGrabAnimLRect = new Rectangle(85, 63, 16, 16);
        public static readonly Rectangle EchidnaGrabAnimRRect = new Rectangle(85, 43, 15, 16);

        public static readonly Rectangle EchidnaGrabWalkAnimU1Rect = new Rectangle(66, 23, 14, 18);
        public static readonly Rectangle EchidnaGrabWalkAnimU2Rect = new Rectangle(85, 23, 14, 17);
        public static readonly Rectangle EchidnaGrabWalkAnimU3Rect = new Rectangle(104, 23, 14, 18);

        public static readonly Rectangle EchidnaGrabWalkAnimD1Rect = new Rectangle(66, 5, 14, 15);
        public static readonly Rectangle EchidnaGrabWalkAnimD2Rect = new Rectangle(85, 4, 14, 16);
        public static readonly Rectangle EchidnaGrabWalkAnimD3Rect = new Rectangle(104, 5, 14, 15);

        public static readonly Rectangle EchidnaGrabWalkAnimL1Rect = new Rectangle(66, 63, 16, 16);
        public static readonly Rectangle EchidnaGrabWalkAnimL2Rect = new Rectangle(85, 63, 16, 16);
        public static readonly Rectangle EchidnaGrabWalkAnimL3Rect = new Rectangle(104, 63, 16, 16);

        public static readonly Rectangle EchidnaGrabWalkAnimR1Rect = new Rectangle(66, 43, 15, 16);
        public static readonly Rectangle EchidnaGrabWalkAnimR2Rect = new Rectangle(85, 43, 15, 16);
        public static readonly Rectangle EchidnaGrabWalkAnimR3Rect = new Rectangle(104, 43, 15, 16);

        public static readonly Rectangle EchidnaSuccessAnimURect = new Rectangle(28, 82, 14, 17);
        public static readonly Rectangle EchidnaSuccessAnimDRect = new Rectangle(9, 83, 14, 15);
        public static readonly Rectangle EchidnaSuccessAnimLRect = new Rectangle(67, 82, 14, 16);
        public static readonly Rectangle EchidnaSuccessAnimRRect = new Rectangle(47, 82, 14, 16);

        public static readonly Rectangle EchidnaSleepAnimU1Rect = new Rectangle(28, 23, 14, 17);
        //public static readonly Rectangle EchidnaSleepAnimU1Rect = new Rectangle(35, 134, 30, 28);
        public static readonly Rectangle EchidnaSleepAnimU2Rect = new Rectangle(66, 101, 14, 17);
        //public static readonly Rectangle EchidnaSleepAnimU2Rect = new Rectangle(67, 134, 30, 28);
        public static readonly Rectangle EchidnaSleepAnimU3Rect = new Rectangle(85, 101, 14, 17);
        //public static readonly Rectangle EchidnaSleepAnimU3Rect = new Rectangle(99, 134, 30, 28);

        public static readonly Rectangle EchidnaSleepAnimD1Rect = new Rectangle(9, 102, 14, 15);
        //public static readonly Rectangle EchidnaSleepAnimD1Rect = new Rectangle(131, 99, 30, 31);
        public static readonly Rectangle EchidnaSleepAnimD2Rect = new Rectangle(28, 102, 14, 15);
        //public static readonly Rectangle EchidnaSleepAnimD2Rect = new Rectangle(163, 99, 30, 31);
        public static readonly Rectangle EchidnaSleepAnimD3Rect = new Rectangle(47, 102, 14, 15);
        //public static readonly Rectangle EchidnaSleepAnimD3Rect = new Rectangle(3, 131, 30, 31);

        public static readonly Rectangle EchidnaSleepAnimL1Rect = new Rectangle(65, 121, 14, 16);
        public static readonly Rectangle EchidnaSleepAnimL2Rect = new Rectangle(84, 121, 14, 16);
        public static readonly Rectangle EchidnaSleepAnimL3Rect = new Rectangle(102, 121, 14, 16);

        public static readonly Rectangle EchidnaSleepAnimR1Rect = new Rectangle(9, 121, 14, 16);
        //public static readonly Rectangle EchidnaSleepAnimR1Rect = new Rectangle(133, 134, 25, 28);
        public static readonly Rectangle EchidnaSleepAnimR2Rect = new Rectangle(28, 121, 14, 16);
        //public static readonly Rectangle EchidnaSleepAnimR2Rect = new Rectangle(165, 134, 25, 28);
        public static readonly Rectangle EchidnaSleepAnimR3Rect = new Rectangle(47, 121, 14, 16);
        //public static readonly Rectangle EchidnaSleepAnimR3Rect = new Rectangle(5, 166, 25, 28);

        #endregion


        #endregion

        #endregion
    }
}
