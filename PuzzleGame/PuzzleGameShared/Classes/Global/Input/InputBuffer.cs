﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// Handles buffers for inputs.
    /// </summary>
    public sealed class InputBuffer : IUpdateable
    {
        /// <summary>
        /// The amount of time inputs are retained in the buffer.
        /// </summary>
        public double BufferTime { get; private set; } = 51d;

        private List<Direction> LastDirectionsHeld = new List<Direction>(4);
        public Direction? LastDirectionHeld { get; private set; } = null;
        public string LastInputPressed => RecentPressedDirs.Count > 0 ? RecentPressedDirs.Peek().InputAction : string.Empty;

        private Queue<ActionBuffer> RecentPressedDirs = new Queue<ActionBuffer>(16);

        private double ElapsedTime = 0d;

        private Dictionary<string, double> LastInputsHeld = new Dictionary<string, double>(5)
        {
            { InputActions.Left, 0d },
            { InputActions.Right, 0d },
            { InputActions.Down, 0d },
            { InputActions.Up, 0d },
            { InputActions.Grab, 0d },
        };

        private Dictionary<string, double> LastInputsPressed = new Dictionary<string, double>(5)
        {
            { InputActions.Left, 0d },
            { InputActions.Right, 0d },
            { InputActions.Down, 0d },
            { InputActions.Up, 0d },
            { InputActions.Grab, 0d },
        };

        public InputBuffer(in double bufferTime)
        {
            BufferTime = bufferTime;

            ElapsedTime = BufferTime;
        }

        public void Update()
        {
            UpdateHeldPressedInputs();
        }

        private void CheckInput(string inputAction)
        {
            bool shouldAdd = Input.GetButtonDown(inputAction);

            //The input is held; update its time
            if (Input.GetButton(inputAction) == true)
            {
                LastInputsHeld[inputAction] = ElapsedTime;
            }

            if (shouldAdd == true)
            {
                LastInputsPressed[inputAction] = ElapsedTime;

                RecentPressedDirs.Enqueue(new ActionBuffer(inputAction, ElapsedTime));
            }
        }

        private void CheckInputDir(string inputAction, in Direction dir)
        {
            bool shouldAdd = Input.GetButtonDown(inputAction);

            //The input is held; add it if it's not already in the list
            if (Input.GetButton(inputAction) == true)
            {
                LastInputsHeld[inputAction] = ElapsedTime;
                LastDirectionsHeld.AddIfNotIn(dir);
            }
            //The input is released; remove from the list
            else
            {
                LastDirectionsHeld.Remove(dir);
            }

            if (shouldAdd == true)
            {
                LastInputsPressed[inputAction] = ElapsedTime;

                RecentPressedDirs.Enqueue(new ActionBuffer(inputAction, ElapsedTime));
            }
        }

        public bool CheckAndConsumeInput(string inputAction)
        {
            if (RecentPressedDirs.Count > 0)
            {
                if (RecentPressedDirs.Peek().InputAction == inputAction)
                {
                    RecentPressedDirs.Dequeue();
                    return true;
                }
            }

            return false;
        }

        public void ConsumeLastInput()
        {
            if (RecentPressedDirs.Count > 0)
            {
                RecentPressedDirs.Dequeue();
            }
        }

        private void UpdateHeldPressedInputs()
        {
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            CheckInputDir(InputActions.Left, Direction.Left);
            CheckInputDir(InputActions.Right, Direction.Right);
            CheckInputDir(InputActions.Up, Direction.Up);
            CheckInputDir(InputActions.Down, Direction.Down);
            CheckInput(InputActions.Grab);

            //If no directions are held, clear the last direction held
            if (LastDirectionsHeld.Count == 0)
            {
                LastDirectionHeld = null;
            }
            //Use the most recently pressed direction
            else
            {
                LastDirectionHeld = LastDirectionsHeld[LastDirectionsHeld.Count - 1];
            }

            //We loop so that multiple inputs pressed on the same frame expire on the same frame
            while (RecentPressedDirs.Count > 0)
            {
                ActionBuffer ab = RecentPressedDirs.Peek();
                double diff = ElapsedTime - ab.TimePressed;

                if (diff >= BufferTime)
                {
                    RecentPressedDirs.Dequeue();
                }
                else
                {
                    break;
                }
            }
        }

        private struct ActionBuffer
        {
            public string InputAction;
            public double TimePressed;

            public ActionBuffer(string inputAction, in double timePressed)
            {
                InputAction = inputAction;
                TimePressed = timePressed;
            }

            public override bool Equals(object obj)
            {
                if (obj is ActionBuffer ab)
                {
                    return (this == ab);
                }

                return false;
            }

            public override int GetHashCode()
            {
                int hash = 17;
                hash = (hash * 33) + (InputAction == null ? 0 : InputAction.GetHashCode());
                hash = (hash * 33) + TimePressed.GetHashCode();

                return hash;
            }

            public static bool operator ==(ActionBuffer ab1, ActionBuffer ab2)
            {
                return (ab1.InputAction == ab2.InputAction && ab1.TimePressed == ab2.TimePressed);
            }

            public static bool operator !=(ActionBuffer ab1, ActionBuffer ab2)
            {
                return !(ab1 == ab2);
            }

            public override string ToString()
            {
                return "Action: " + InputAction + " | TimePressed: " + TimePressed;
            }
        }
    }
}
