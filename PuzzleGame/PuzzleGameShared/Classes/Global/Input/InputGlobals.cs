﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// Class for global values dealing with input.
    /// </summary>
    public static class InputGlobals
    {
        public const int MinJoystickAxisValue = short.MinValue;
        public const int MaxJoystickAxisValue = short.MaxValue;

        /// <summary>
        /// The starting input index used for gamepads.
        /// </summary>
        public const int GamePadInputIndex = 2;

        /// <summary>
        /// The max number of gamepads the game allows.
        /// </summary>
        public static readonly int MaxGamePadCount = GamePad.MaximumGamePadCount;

        #region Default Input Mappings

        public static readonly Dictionary<InputHandler.InputTypes, Dictionary<string, int>> DefaultMappings = new Dictionary<InputHandler.InputTypes, Dictionary<string, int>>()
        {
            {
                InputHandler.InputTypes.Keyboard, new Dictionary<string, int>(11)
                {
                    { InputActions.Right, (int)Keys.Right },
                    { InputActions.Left, (int)Keys.Left },
                    { InputActions.Down, (int)Keys.Down },
                    { InputActions.Up, (int)Keys.Up },

                    { InputActions.Grab, (int)Keys.Space },
                    { InputActions.Pause, (int)Keys.Escape },
                    { InputActions.Restart, (int)Keys.R },
                    { InputActions.Undo, (int)Keys.Back },

                    { InputActions.BackOut, (int)Keys.Escape },
                    { InputActions.Select, (int)Keys.Enter },
                    { InputActions.ViewMap, (int)Keys.E },
                }
            },
            {
                InputHandler.InputTypes.GamePad, new Dictionary<string, int>(11)
                {
                    { InputActions.Right, (int)Buttons.DPadRight },
                    { InputActions.Left, (int)Buttons.DPadLeft },
                    { InputActions.Down, (int)Buttons.DPadDown },
                    { InputActions.Up, (int)Buttons.DPadUp },

                    { InputActions.Grab, (int)Buttons.A },
                    { InputActions.Pause, (int)Buttons.Start },
                    { InputActions.Restart, (int)Buttons.RightShoulder },
                    { InputActions.Undo, (int)Buttons.Y },

                    { InputActions.BackOut, (int)Buttons.X },
                    { InputActions.Select, (int)Buttons.A },
                    { InputActions.ViewMap, (int)Buttons.Y },
                }
            },
            {
                InputHandler.InputTypes.Joystick, new Dictionary<string, int>(11)
                {
                    { InputActions.Up, 0 },
                    { InputActions.Down, 1 },
                    { InputActions.Left, 2 },
                    { InputActions.Right, 3 },

                    { InputActions.Grab, 4 },
                    { InputActions.Pause, 5 },
                    { InputActions.Restart, 6 },
                    { InputActions.Undo, 7 },

                    { InputActions.BackOut, 8 },
                    { InputActions.Select, 9 },
                    { InputActions.ViewMap, 10 },
                }
            }
        };

        #endregion

        #region Input To Readable Names

        public static readonly Dictionary<int, string> KeyNameToText = new Dictionary<int, string>(77)
        {
            { (int)Keys.Escape, "Esc" },
            { (int)Keys.LeftShift, "LShift" },
            { (int)Keys.RightShift, "RShift" },
            { (int)Keys.LeftWindows, "LSuper" },
            { (int)Keys.RightWindows, "RSuper" },
            { (int)Keys.LeftControl, "LCtrl" },
            { (int)Keys.RightControl, "RCtrl" },
            { (int)Keys.LeftAlt, "LAlt" },
            { (int)Keys.RightAlt, "RAlt" },
            { (int)Keys.D0, "0" },
            { (int)Keys.D1, "1" },
            { (int)Keys.D2, "2" },
            { (int)Keys.D3, "3" },
            { (int)Keys.D4, "4" },
            { (int)Keys.D5, "5" },
            { (int)Keys.D6, "6" },
            { (int)Keys.D7, "7" },
            { (int)Keys.D8, "8" },
            { (int)Keys.D9, "9" },
            { (int)Keys.NumPad0, "Pad0" },
            { (int)Keys.NumPad1, "Pad1" },
            { (int)Keys.NumPad2, "Pad2" },
            { (int)Keys.NumPad3, "Pad3" },
            { (int)Keys.NumPad4, "Pad4" },
            { (int)Keys.NumPad5, "Pad5" },
            { (int)Keys.NumPad6, "Pad6" },
            { (int)Keys.NumPad7, "Pad7" },
            { (int)Keys.NumPad8, "Pad8" },
            { (int)Keys.NumPad9, "Pad9" },
            { (int)Keys.OemCopy, "Copy" },
            { (int)Keys.OemClear, "Clear" },
            { (int)Keys.OemPipe, "Pipe" },
            { (int)Keys.OemPeriod, "Period" },
            { (int)Keys.OemComma, "Comma" },
            { (int)Keys.OemEnlW, "EnlW" },
            { (int)Keys.OemMinus, "Minus" },
            { (int)Keys.OemPlus, "Plus" },
            { (int)Keys.OemQuestion, "Qstn" },
            { (int)Keys.OemQuotes, "Quote" },
            { (int)Keys.OemSemicolon, "SColon" },
            { (int)Keys.OemTilde, "Tilde" },
            { (int)Keys.OemOpenBrackets, "OBrkt" },
            { (int)Keys.OemCloseBrackets, "CBrkt" },
            { (int)Keys.Oem8, "Oem8" },
            { (int)Keys.OemAuto, "Auto" },
            { (int)Keys.OemBackslash, "BSlash" },
            { (int)Keys.PageDown, "PgDown" },
            { (int)Keys.PageUp, "PgUp" },
            { (int)Keys.BrowserBack, "BrBack" },
            { (int)Keys.BrowserFavorites, "BrFav" },
            { (int)Keys.BrowserForward, "BrFwd" },
            { (int)Keys.BrowserHome, "BrHome" },
            { (int)Keys.BrowserRefresh, "BrRfrsh" },
            { (int)Keys.BrowserSearch, "BrSrch" },
            { (int)Keys.BrowserStop, "BrStop" },
            { (int)Keys.ChatPadGreen, "CPGreen" },
            { (int)Keys.ChatPadOrange, "CPOrng" },
            { (int)Keys.EraseEof, "ErsEOF" },
            { (int)Keys.ImeConvert, "ImeCvrt" },
            { (int)Keys.ImeNoConvert, "ImeNCvrt" },
            { (int)Keys.LaunchApplication1, "LApp1" },
            { (int)Keys.LaunchApplication2, "LApp2" },
            { (int)Keys.LaunchMail, "LMail" },
            { (int)Keys.MediaNextTrack, "MNTrack" },
            { (int)Keys.MediaPlayPause, "MPPause" },
            { (int)Keys.MediaPreviousTrack, "MPTrack" },
            { (int)Keys.MediaStop, "MStop" },
            { (int)Keys.SelectMedia, "SMedia" },
            { (int)Keys.Separator, "Sep" },
            { (int)Keys.VolumeDown, "VDown" },
            { (int)Keys.VolumeUp, "VUp" },
            { (int)Keys.VolumeMute, "VMute" },
            { (int)Keys.PrintScreen, "PrtScrn" },
            { (int)Keys.ProcessKey, "PrcsKey" },
            { (int)Keys.NumLock, "NmLck" },
            { (int)Keys.CapsLock, "CpsLck" },
            { (int)Keys.Back, "Backspc" }
        };

        public static readonly Dictionary<int, string> ButtonNameToText = new Dictionary<int, string>(25)
        {
            { (int)Buttons.LeftThumbstickLeft, "LStickLeft" },
            { (int)Buttons.LeftThumbstickRight, "LStickRight" },
            { (int)Buttons.LeftThumbstickUp, "LStickUp" },
            { (int)Buttons.LeftThumbstickDown, "LStickDown" },
            { (int)Buttons.LeftStick, "LStick" },
            { (int)Buttons.RightThumbstickLeft, "RStickLeft" },
            { (int)Buttons.RightThumbstickRight, "RStickRight" },
            { (int)Buttons.RightThumbstickUp, "RStickUp" },
            { (int)Buttons.RightThumbstickDown, "RStickDown" },
            { (int)Buttons.RightStick, "RStick" },
            { (int)Buttons.A, "A" },
            { (int)Buttons.B, "B" },
            { (int)Buttons.X, "X" },
            { (int)Buttons.Y, "Y" },
            { (int)Buttons.Start, "Start" },
            { (int)Buttons.Back, "Back" },
            { (int)Buttons.BigButton, "Main" },
            { (int)Buttons.DPadLeft, "DLeft" },
            { (int)Buttons.DPadRight, "DRight" },
            { (int)Buttons.DPadUp, "DUp" },
            { (int)Buttons.DPadDown, "DDown" },
            { (int)Buttons.LeftShoulder, "LShoulder" },
            { (int)Buttons.RightShoulder, "RShoulder" },
            { (int)Buttons.LeftTrigger, "LTrigger" },
            { (int)Buttons.RightTrigger, "RTrigger" },
        };

        #endregion

        public static void GetAndLoadInputsForIndex(in InputConfigData inputConfigData, in int gamePadIndex)
        {
            GamePadCapabilities gamePadCaps = GamePad.GetCapabilities(gamePadIndex);

            if (string.IsNullOrEmpty(gamePadCaps.Identifier) == true)
            {
                return;
            }

            if (inputConfigData.GPMapData.TryGetValue(gamePadCaps.Identifier, out InputConfigData.GamepadMapData gpMapData) == true)
            {
                InputHandler inputHandler = Input.GetInputHandler(gamePadIndex + InputGlobals.GamePadInputIndex);

                //Return if this is an invalid input handler
                if (inputHandler == null)
                {
                    return;
                }

                //Copy the inputs saved
                inputHandler.CopyMappingsFromDict(gpMapData.MappingData.ActionMappings);
            }
        }

        public static void LoadInputsFromConfigData(in InputConfigData inputConfigData)
        {
            //Load keyboard mappings
            for (int i = 0; i < InputGlobals.GamePadInputIndex; i++)
            {
                //Check if we have input data for this keyboard index
                if (inputConfigData.KBMapData.TryGetValue(i, out InputConfigData.KeyboardMapData kbMapData) == true)
                {
                    InputHandler inputHandler = Input.GetInputHandler(i);

                    //Continue if this is an invalid input handler
                    if (inputHandler == null)
                    {
                        continue;
                    }

                    //Copy the inputs saved
                    inputHandler.CopyMappingsFromDict(kbMapData.MappingData.ActionMappings);
                }
            }

            //Load gamepad mappings
            for (int i = 0; i < InputGlobals.MaxGamePadCount; i++)
            {
                GamePadCapabilities gamePadCaps = GamePad.GetCapabilities(i);

                if (string.IsNullOrEmpty(gamePadCaps.Identifier) == true)
                {
                    continue;
                }

                //Check if we have input data for this controller
                if (inputConfigData.GPMapData.TryGetValue(gamePadCaps.Identifier, out InputConfigData.GamepadMapData gpMapData) == true)
                {
                    InputHandler inputHandler = Input.GetInputHandler(i + InputGlobals.GamePadInputIndex);

                    //Continue if this is an invalid input handler
                    if (inputHandler == null)
                    {
                        continue;
                    }

                    //Copy the inputs saved
                    inputHandler.CopyMappingsFromDict(gpMapData.MappingData.ActionMappings);
                }
            }
        }

        /// <summary>
        /// Find an index for a connected controller given the ID of the controller.
        /// </summary>
        /// <param name="controllerID">The ID of the controller.</param>
        /// <returns>The index of the connected controller if found, otherwise -1.</returns>
        public static int FindControllerIndexForID(in string controllerID)
        {
            for (int i = 0; i < InputGlobals.MaxGamePadCount; i++)
            {
                GamePadCapabilities gamePadCaps = GamePad.GetCapabilities(i);
                if (gamePadCaps.Identifier == controllerID)
                {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Find the closest index for a connected controller given the ID of the controller and the previous index used.
        /// </summary>
        /// <param name="controllerID">The ID of the controller.</param>
        /// <param name="index">The index to prioritize a connected controller for.</param>
        /// <returns>The index of the connected controller closest to the given index if found, otherwise -1.</returns>
        public static int FindControllerIndexForIDClosestTo(in string controllerID, in int index)
        {
            int prevDiff = InputGlobals.MaxGamePadCount;
            int closestIndex = -1;
            for (int i = 0; i < InputGlobals.MaxGamePadCount; i++)
            {
                GamePadCapabilities gamePadCaps = GamePad.GetCapabilities(i);
                if (gamePadCaps.Identifier == controllerID)
                {
                    int diff = Math.Abs(index - i);
                    if (diff < prevDiff)
                    {
                        prevDiff = diff;
                        closestIndex = i;

                        if (closestIndex == index)
                            break;
                    }
                }
            }

            return closestIndex;
        }
    }

    /// <summary>
    /// The types of actions you can perform in the game.
    /// </summary>
    public static class InputActions
    {
        public const string Left = "Left";
        public const string Right = "Right";
        public const string Up = "Up";
        public const string Down = "Down";
        public const string Grab = "Grab";
        public const string Pause = "Pause";
        public const string Restart = "Restart";
        public const string Undo = "Undo";

        public const string BackOut = "BackOut";
        public const string Select = "Select";
        public const string ViewMap = "ViewMap";
    }
}
