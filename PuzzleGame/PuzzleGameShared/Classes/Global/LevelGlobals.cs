﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Global values relating to levels.
    /// </summary>
    public static class LevelGlobals
    {
        public delegate bool CollisionFilter(ICollisionObj collisionObj);

        public const string CollisionLayerName = "Collision";
        public const string BlockLayerName = "Blocks";
        public const string PortalLayerName = "Portal";
        public const string SpotLayerName = "Spots";
        public const string SwitchLayerName = "Switches";
        public const string WarpLayerName = "Warps";
        public const string PlayerStartLayerName = "PlayerStart";
        public const string TextLayerName = "Text";
        public const string SurfaceLayerName = "Surfaces";
        public const string RockShooterLayerName = "RockShooters";
        public const string CutsceneTriggerLayerName = "CutsceneTriggers";
        public const string CameraFocusLayerName = "CameraFocus";

        //Used for cutscenes
        public const string SpawnPosLayerName = "SpawnPos";

        public const string BlockPatternKey = "blockpattern";
        public const string IDKey = "id";
        public const string DirectionKey = "direction";
        public const string ScaleKey = "scale";
        public const string SwitchAmountKey = "amount";
        public const string SwitchTimeKey = "time";
        public const string SwitchToModifierKey = "tomod";
        public const string SwitchFromModifierKey = "frommod";
        public const string WarpTileXKey = "tilex";
        public const string WarpTileYKey = "tiley";
        public const string WarpAltAnimKey = "altanim";
        public const string PortalBossAnimKey = "bossanim";
        public const string CollisionLayerKey = "collisionlayer";
        public const string TextKey = "text";
        public const string SurfaceTypeKey = "surfacetype";
        public const string GrabbableKey = "grabbable";
        public const string ShowIDKey = "showid";
        public const string HasSoundKey = "hassound";

        public const string PairedBlockIDKey = "pairedid";
        public const string PipeBlockEntranceDirKey = "entrancedir";
        public const string PipeBlockExitDirKey = "exitdir";

        public const string RockShooterHurtKey = "hurt";
        public const string RockShooterThrowRockKey = "throwrock";

        public const string AllowsUndoKey = "allowundo";
        public const string AllowsRestartKey = "allowrestart";
        public const string AllowsExitingKey = "allowexiting";
        public const string MusicKey = "music";
        public const string CameraFollowKey = "camerafollow";
        public const string ShowHUDKey = "showhud";

        public const string TextFontKey = "font";
        public const string TextRedKey = "r";
        public const string TextGreenKey = "g";
        public const string TextBlueKey = "b";
        public const string TextAlphaKey = "alpha";

        public const string CutsceneIDKey = "cutsceneid";

        /// <summary>
        /// A level property to unlock bonus levels.
        /// Used only on the last level to unlock bonuses as soon as it's complete.
        /// </summary>
        public const string UnlocksBonusKey = "unlocksbonus";

        #region Overworld

        public const string NodeLayerName = "Nodes";

        public const string LevelNameKey = "level";
        public const string UpDirKey = "map_up";
        public const string DownDirKey = "map_down";
        public const string LeftDirKey = "map_left";
        public const string RightDirKey = "map_right";
        public const string DescriptionKey = "description";
        public const string NodeTypeKey = "nodetype";
        public const string LandLevelIDKey = "landlevelid";
        public const string WorldIDKey = "worldid";
        public const string AutoCompleteKey = "autocomplete";
        public const string DisplayIDKey = "displayid";
        public const string UnlockTypeKey = "unlocktype";
        public const string DisplayOverrideKey = "displayoverride";
        public const string NodeAltAnimKey = "nodealtanim";

        public const string WorldNumKey = "worldnum";
        public const string WorldNameKey = "worldname";

        #endregion

        #region Object Types

        public const string PairedBlockType = "paired";
        public const string BlockSwitchType = "blockswitch";
        public const string PipeBlockType = "pipe";

        #endregion

        #region Object Render Depths

        public const float BasePlayerRenderDepth = .8f;
        public const float BaseBlockRenderDepth = .7f;
        public const float BaseRockRenderDepth = .75f;
        public const float BaseRockShooterRenderDepth = .5f;
        public const float BasePortalRenderDepth = .4f;
        public const float BaseWarpRenderDepth = .3f;
        public const float BaseSwitchRenderDepth = .2f;
        public const float BaseSpotRenderDepth = .1f;
        public const float BaseTextRenderDepth = .05f;

        public const float BaseTileLayerDepth = 0f;
        public const float TileLayerDepthIncrease = .01f;

        #endregion

        #region Collision Checks

        public static T CheckCollision<T>(in Vector2 speed, in RectangleF collisionRect, in ObjectTypes objType,
            in CollisionTypes collType, in CollisionLayers collLayer, List<T> collision) where T : ICollisionObj
        {
            RectangleF rect = collisionRect;
            rect.X += speed.X;
            rect.Y += speed.Y;

            for (int i = 0; i < collision.Count; i++)
            {
                T collObj = collision[i];

                if (collObj.ObjectType != objType) continue;
                if (EnumUtility.HasEnumVal((long)collObj.CollisionType, (long)collType) == false) continue;
                if (EnumUtility.HasEnumVal((long)collObj.CollisionLayer, (long)collLayer) == false) continue;

                RectangleF coll = collObj.CollisionRect;

                if (rect.Intersects(coll) == true)
                {
                    return collObj;
                }
            }

            return default;
        }

        public static T CheckCollision<T>(in Vector2 speed, in RectangleF collisionRect,
            in CollisionTypes collType, in CollisionLayers collLayer, List<T> collision) where T: ICollisionObj
        {
            RectangleF rect = collisionRect;
            rect.X += speed.X;
            rect.Y += speed.Y;

            for (int i = 0; i < collision.Count; i++)
            {
                T collObj = collision[i];

                if (EnumUtility.HasEnumVal((long)collObj.CollisionType, (long)collType) == false) continue;
                if (EnumUtility.HasEnumVal((long)collObj.CollisionLayer, (long)collLayer) == false) continue;

                RectangleF coll = collObj.CollisionRect;

                if (rect.Intersects(coll) == true)
                {
                    return collObj;
                }
            }

            return default;
        }

        public static T CheckCollision<T>(in Vector2 speed, in T collisionObject,
            in CollisionTypes collType, in CollisionLayers collLayer, List<T> collision) where T: class, ICollisionObj
        {
            RectangleF rect = collisionObject.CollisionRect;
            rect.X += speed.X;
            rect.Y += speed.Y;

            for (int i = 0; i < collision.Count; i++)
            {
                T collObj = collision[i];
                if (collObj == collisionObject) continue;

                if (EnumUtility.HasEnumVal((long)collObj.CollisionType, (long)collType) == false) continue;
                if (EnumUtility.HasEnumVal((long)collObj.CollisionLayer, (long)collLayer) == false) continue;

                RectangleF coll = collObj.CollisionRect;

                if (rect.Intersects(coll) == true)
                {
                    return collObj;
                }
            }

            return default;
        }

        public static T CheckCollision<T>(in Vector2 speed, in RectangleF collisionRect, List<T> collision,
            CollisionFilter collisionFilter) where T: ICollisionObj
        {
            RectangleF rect = collisionRect;
            rect.X += speed.X;
            rect.Y += speed.Y;

            for (int i = 0; i < collision.Count; i++)
            {
                T collObj = collision[i];

                RectangleF coll = collObj.CollisionRect;

                if (rect.Intersects(coll) == true && collisionFilter(collObj) == true)
                {
                    return collObj;
                }
            }

            return default;
        }

        public static T CheckClosestCollision<T>(in Vector2 speed, in RectangleF collisionRect, List<T> collision,
            CollisionFilter collisionFilter) where T : ICollisionObj
        {
            RectangleF rect = collisionRect;
            rect.X += speed.X;
            rect.Y += speed.Y;

            Vector2 rectCenter = rect.Center;

            T collisionObj = default;
            double distance = double.MaxValue;

            for (int i = 0; i < collision.Count; i++)
            {
                T collObj = collision[i];

                RectangleF coll = collObj.CollisionRect;

                if (rect.Intersects(coll) == true && collisionFilter(collObj) == true)
                {
                    //Use the squared distance from the collision rectangle instead of just the distance from its center
                    //This makes it accurate with objects of all sizes
                    //For example, a large block blocking half of a large switch-controlled block won't be considered much further from it
                    //despite its center being further
                    double dist = UtilityGlobals.SquaredDistanceToPointFromRectangle(coll.TopLeft, coll.BottomRight, rectCenter);
                    if (dist < distance)
                    {
                        collisionObj = collObj;
                        distance = dist;
                    }
                }
            }

            return collisionObj;
        }

        #endregion

        #region Level Object Creation

        public static ICollisionObj CreateObject(PlayableContext context, TiledMapObject mapObj, in ObjectTypes objType)
        {
            return CreateObject(context, (ObjectInitInfo)mapObj, objType);
        }

        public static ICollisionObj CreateObject(PlayableContext context, in ObjectInitInfo initInfo, in ObjectTypes objType)
        {
            switch(objType)
            {
                case ObjectTypes.Wall: return CreateWall(context, initInfo);
                case ObjectTypes.Block: return CreateBlock(context, initInfo);
                case ObjectTypes.Spot: return CreateSpot(context, initInfo);
                case ObjectTypes.Portal: return CreatePortal(context, initInfo);
                case ObjectTypes.Switch: return CreateSwitch(context, initInfo);
                case ObjectTypes.Warp: return CreateWarp(context, initInfo);
                case ObjectTypes.RockShooter: return CreateRockShooter(context, initInfo);
                default: return null;
            }
        }

        public static CollisionObject CreateWall(PlayableContext context, TiledMapObject mapObj)
        {
            return CreateWall(context, (ObjectInitInfo)mapObj);
        }

        public static CollisionObject CreateWall(PlayableContext context, in ObjectInitInfo initInfo)
        {
            CollisionObject collObj = new CollisionObject();
            collObj.Initialize(context, initInfo);

            return collObj;
        }

        public static Block CreateBlock(PlayableContext context, TiledMapObject mapObj)
        {
            return CreateBlock(context, (ObjectInitInfo)mapObj);
        }

        public static Block CreateBlock(PlayableContext context, in ObjectInitInfo initInfo)
        {
            Block block = null;

            if (initInfo.Type == LevelGlobals.PairedBlockType)
            {
                block = new PairedBlock();
            }
            else if (initInfo.Type == LevelGlobals.PipeBlockType)
            {
                block = new PipeBlock();
            }
            else
            {
                block = new Block();
            }

            block.Initialize(context, initInfo);

            return block;
        }

        public static Spot CreateSpot(PlayableContext context, TiledMapObject mapObj)
        {
            return CreateSpot(context, (ObjectInitInfo)mapObj);
        }

        public static Spot CreateSpot(PlayableContext context, in ObjectInitInfo initInfo)
        {
            Spot spot = new Spot();
            spot.Initialize(context, initInfo);

            return spot;
        }

        public static Portal CreatePortal(PlayableContext context, TiledMapObject mapObj)
        {
            return CreatePortal(context, (ObjectInitInfo)mapObj);
        }

        public static Portal CreatePortal(PlayableContext context, in ObjectInitInfo initInfo)
        {
            Portal portal = new Portal(initInfo.Position);
            portal.Initialize(context, initInfo);

            return portal;
        }

        public static Switch CreateSwitch(PlayableContext context, TiledMapObject mapObj)
        {
            return CreateSwitch(context, (ObjectInitInfo)mapObj);
        }

        public static Switch CreateSwitch(PlayableContext context, in ObjectInitInfo initInfo)
        {
            Switch switchObj = null;

            if (initInfo.Type == LevelGlobals.BlockSwitchType)
            {
                switchObj = new BlockSwitch();
            }
            else
            {
                switchObj = new Switch();
            }

            switchObj.Initialize(context, initInfo);

            return switchObj;
        }

        public static Warp CreateWarp(PlayableContext context, TiledMapObject mapObj)
        {
            return CreateWarp(context, (ObjectInitInfo)mapObj);
        }

        public static Warp CreateWarp(PlayableContext context, in ObjectInitInfo initInfo)
        {
            Warp warp = new Warp();
            warp.Initialize(context, initInfo);

            return warp;
        }

        public static RockShooter CreateRockShooter(PlayableContext context, TiledMapObject mapObj)
        {
            return CreateRockShooter(context, (ObjectInitInfo)mapObj);
        }

        public static RockShooter CreateRockShooter(PlayableContext context, in ObjectInitInfo initInfo)
        {
            RockShooter rockShooter = new RockShooter();
            rockShooter.Initialize(context, initInfo);

            return rockShooter;
        }

        public static InLevelText CreateLevelText(PlayableContext context, TiledMapObject mapObj)
        {
            return CreateLevelText(context, (ObjectInitInfo)mapObj);
        }

        public static InLevelText CreateLevelText(PlayableContext context, in ObjectInitInfo initInfo)
        {
            InLevelText levelText = new InLevelText();
            levelText.Initialize(context, initInfo);

            return levelText;
        }

        #endregion

        #region Stats

        /// <summary>
        /// The maximum number of registered moves in a level.
        /// </summary>
        public const int MaxLevelMoves = 999;

        /// <summary>
        /// The maximum amount of registered time in a level.
        /// </summary>
        public const double MaxLevelTime = (60d * Time.MsPerS * 100d) - 1d;

        /// <summary>
        /// Returns a string formatted in the level time, given a value in milliseconds.
        /// </summary>
        /// <param name="milliseconds">The number of milliseconds.</param>
        /// <returns>A string formatted in the level time.</returns>
        public static string GetFormattedLevelTimeString(in double milliseconds)
        {
            //Print how many minutes and seconds the level took
            long ms = (long)milliseconds;
            const long minutes = 60 * (long)Time.MsPerS;
            const long seconds = (long)Time.MsPerS;

            long minute = ms / minutes;
            ms -= (minute * minutes);

            long second = ms / seconds;
            ms -= (second * seconds);

            //double msRepresentation = (int)((ms / 100d));

            return $"{minute.ToString("0")}:{second.ToString("00")}";//.{msRepresentation.ToString("0")}";
        }

        #endregion

        #region Misc Effects

        public static readonly Vector2[] StandardCameraShakes = new Vector2[]
        {
            new Vector2(0, 1), new Vector2(-1, -1), new Vector2(-1, 1), new Vector2(1, 0), new Vector2(0, -1), new Vector2(1, -1),
            new Vector2(-1, 0), new Vector2(1, 1)
        };

        #endregion
    }
}
