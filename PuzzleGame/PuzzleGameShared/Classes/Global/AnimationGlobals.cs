﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    public static class AnimationGlobals
    {
        public const string Idle = "Idle";

        public static class PlayerAnimations
        {
            public const string Walk = "Walk";
            public const string Grab = "Grab";
            public const string GrabWalk = "GrabWalk";
            public const string Success = "Success";
            public const string Sleep = "Sleep";
        }

        public static class MoleAnimations
        {
            public const string Throw = "Throw";
            public const string Hurt = "Hurt";
        }

        public static class RockAnimations
        {
            public const string Roll = "Roll";
            public const string Break = "Break";
        }
    }
}
