﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// Helps handle calls to native code.
    /// </summary>
    public static class NativeGlobals
    {
        //NOTE: A lot of this code is copied directly from MonoGame, so we may need to update it from time to time
        //Either that, or have some of the native classes public in our fork so we can use them directly

        public static IntPtr NativeLibrary = GetNativeLibrary();

        private static IntPtr GetNativeLibrary()
        {
            IntPtr ret = IntPtr.Zero;

            // Load bundled library
            var assemblyLocation = Path.GetDirectoryName(typeof(Game).Assembly.Location) ?? "./";

#if WINDOWS
            if (Environment.Is64BitProcess)
                ret = FuncLoader.LoadLibrary(Path.Combine(assemblyLocation, "x64/SDL2.dll"));
            else
                ret = FuncLoader.LoadLibrary(Path.Combine(assemblyLocation, "x86/SDL2.dll"));
#elif LINUX && !MACOS
            if (Environment.Is64BitProcess)
                ret = FuncLoader.LoadLibrary(Path.Combine(assemblyLocation, "x64/libSDL2-2.0.so.0"));
            else
                ret = FuncLoader.LoadLibrary(Path.Combine(assemblyLocation, "x86/libSDL2-2.0.so.0"));
#else
            ret = FuncLoader.LoadLibrary(Path.Combine(assemblyLocation, "libSDL2-2.0.0.dylib"));

            //Look in Frameworks for .app bundles
            if (ret == IntPtr.Zero)
                ret = FuncLoader.LoadLibrary(Path.Combine(assemblyLocation, "..", "Frameworks", "libSDL2-2.0.0.dylib"));
#endif

            // Load system library
            if (ret == IntPtr.Zero)
            {
#if WINDOWS
                ret = FuncLoader.LoadLibrary("SDL2.dll");
#elif LINUX && !MACOS
                ret = FuncLoader.LoadLibrary("libSDL2-2.0.so.0");
#else
                ret = FuncLoader.LoadLibrary("libSDL2-2.0.0.dylib");
#endif
            }

            // Try extra locations for Windows because of .NET Core rids
#if WINDOWS
            {
                var rid = Environment.Is64BitProcess ? "win-x64" : "win-x86";

                if (ret == IntPtr.Zero)
                    ret = FuncLoader.LoadLibrary(Path.Combine(assemblyLocation, "../../runtimes", rid, "native/SDL2.dll"));

                if (ret == IntPtr.Zero)
                    ret = FuncLoader.LoadLibrary(Path.Combine(assemblyLocation, "runtimes", rid, "native/SDL2.dll"));
            }
#endif

            // Welp, all failed, PANIC!!!
            if (ret == IntPtr.Zero)
                throw new Exception("Failed to load SDL library.");

            return ret;
        }

        public static class SDLNative
        {
            public enum SDL_MessageBoxFlags : uint
            {
                SDL_MESSAGEBOX_ERROR = 0x00000010,   /**< error dialog */
                SDL_MESSAGEBOX_WARNING = 0x00000020,   /**< warning dialog */
                SDL_MESSAGEBOX_INFORMATION = 0x00000040,   /**< informational dialog */
                SDL_MESSAGEBOX_BUTTONS_LEFT_TO_RIGHT = 0x00000080,   /**< buttons placed left to right */
                SDL_MESSAGEBOX_BUTTONS_RIGHT_TO_LEFT = 0x00000100    /**< buttons placed right to left */
            }

            [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
            public delegate IntPtr d_sdl_rwfrommem(byte[] mem, int size);
            public static d_sdl_rwfrommem SDL_RWFromMem = FuncLoader.LoadFunction<d_sdl_rwfrommem>(NativeLibrary, "SDL_RWFromMem");

            [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
            public delegate int d_sdl_gamecontrolleraddmappingsfromrw(IntPtr rw, int freew);
            public static d_sdl_gamecontrolleraddmappingsfromrw AddMappingFromRw = FuncLoader.LoadFunction<d_sdl_gamecontrolleraddmappingsfromrw>(NativeLibrary, "SDL_GameControllerAddMappingsFromRW");

            [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
            public delegate int d_sdl_showsimplemessagebox(uint flags, string title, string message, IntPtr window);
            public static d_sdl_showsimplemessagebox ShowSimpleMessageBox = FuncLoader.LoadFunction<d_sdl_showsimplemessagebox>(NativeLibrary, "SDL_ShowSimpleMessageBox");
        }

        public static int AddGameControllerMappings(string filepath)
        {
            int numMappingsAdded = 0;

            using (var stream = File.OpenRead(filepath))
            {
                if (stream != null)
                {
                    using (var reader = new BinaryReader(stream))
                    {
                        var src = SDLNative.SDL_RWFromMem(reader.ReadBytes((int)stream.Length), (int)stream.Length);
                        numMappingsAdded = SDLNative.AddMappingFromRw(src, 1);
                        //Debug.Log($"Added {numMappingsAdded} controller mappings from config file!");
                    }
                }
            }

            return numMappingsAdded;
        }

        /// <summary>
        /// Shows a simple message box.
        /// </summary>
        /// <param name="flags">The flags for the message box.</param>
        /// <param name="title">The title of the message box.</param>
        /// <param name="message">The message of the message box.</param>
        /// <param name="gameWindow">The GameWindow to serve as the parent of the message box. Can be null for no parent window.</param>
        public static void ShowMessageBox(SDLNative.SDL_MessageBoxFlags flags, string title, string message, GameWindow gameWindow)
        {
            IntPtr windowHandle = (gameWindow != null) ? gameWindow.Handle : IntPtr.Zero;

            int success = SDLNative.ShowSimpleMessageBox((uint)flags, title, message, windowHandle);

            if (success < 0)
            {
                Debug.LogError($"Error displaying message box. Code: {success}");
            }
        }
    }
}
