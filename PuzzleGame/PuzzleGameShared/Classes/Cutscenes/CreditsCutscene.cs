/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The credits cutscene.
    /// </summary>
    public sealed class CreditsCutscene : Cutscene
    {
        public override void Start()
        {
            IngameState creditsScene = new IngameState(-1, AssetManager.Instance.LoadTiledMap(ContentGlobals.CreditsCutsceneLevel),
                LevelEventTypes.Custom, "Credits", "End Credits", string.Empty, string.Empty);
            GameStateManager.Instance.ChangeGameState(creditsScene);

            creditsScene.ChangeLevelState(new CreditsCutsceneState(creditsScene));
        }
    }
}