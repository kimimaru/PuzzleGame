/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The third part of the intro cutscene.
    /// </summary>
    public sealed class IntroCutscenePt3 : Cutscene
    {
        public override void Start()
        {
            IngameState state = (IngameState)GameStateManager.Instance.CurrentState;
            
            state.ChangeLevelState(new IntroSceneAmbushedState(state));
        }
    }
}