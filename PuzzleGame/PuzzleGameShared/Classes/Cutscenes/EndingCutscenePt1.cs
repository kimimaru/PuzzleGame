/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The first part of the ending cutscene.
    /// </summary>
    public sealed class EndingCutscenePt1 : Cutscene
    {
        public override void Start()
        {
            IngameState endingScene = new IngameState(-1, AssetManager.Instance.LoadTiledMap(ContentGlobals.EndingCutsceneLevel),
                LevelEventTypes.Custom, "Ending", LevelDescription, string.Empty, string.Empty);
            GameStateManager.Instance.ChangeGameState(endingScene);

            endingScene.ChangeLevelState(new EndingCutsceneStatePt1(endingScene));
        }
    }
}