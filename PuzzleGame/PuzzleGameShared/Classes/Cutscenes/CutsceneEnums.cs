/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    public enum CutsceneEnums
    {
        None, Intro, IntroPt2, IntroPt3,
        PostBoss1Load, PostBoss1, PostBoss2Load, PostBoss2, PostBoss3Load, PostBoss3, PostBoss4Load, PostBoss4, PostBoss5Load, PostBoss5, PostBoss6Load, PostBoss6,
        EndingPt1, EndingPt2, Credits, DemoEnd, TitleDemo
    }
}