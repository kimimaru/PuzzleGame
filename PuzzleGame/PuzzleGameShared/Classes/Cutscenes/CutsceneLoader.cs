/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// Helps handle loading cutscenes.
    /// </summary>
    public static class CutsceneLoader
    {
        /// <summary>
        /// Loads a cutscene given a cutscene ID.
        /// </summary>
        /// <param name="cutsceneEnum">The ID of the cutscene to load.</param>
        /// <returns>A Cutscene instance.</returns>
        public static Cutscene LoadCutscene(in CutsceneEnums cutsceneEnum)
        {
            switch(cutsceneEnum)
            {
                default:
                case CutsceneEnums.Intro: return new IntroCutscene();
                case CutsceneEnums.IntroPt2: return new IntroCutscenePt2();
                case CutsceneEnums.IntroPt3: return new IntroCutscenePt3();
                case CutsceneEnums.PostBoss1Load: return new PostBossCutsceneLoad(ContentGlobals.PostBoss1CutsceneLevel);
                case CutsceneEnums.PostBoss2Load: return new PostBossCutsceneLoad(ContentGlobals.PostBoss2CutsceneLevel);
                case CutsceneEnums.PostBoss3Load: return new PostBossCutsceneLoad(ContentGlobals.PostBoss3CutsceneLevel);
                case CutsceneEnums.PostBoss4Load: return new PostBossCutsceneLoad(ContentGlobals.PostBoss4CutsceneLevel);
                case CutsceneEnums.PostBoss5Load: return new PostBossCutsceneLoad(ContentGlobals.PostBoss5CutsceneLevel);
                case CutsceneEnums.PostBoss6Load: return new PostBossCutsceneLoad(ContentGlobals.PostBoss6CutsceneLevel);
                case CutsceneEnums.PostBoss1: return new PostBoss1Cutscene();
                case CutsceneEnums.PostBoss2: return new PostBoss2Cutscene();
                case CutsceneEnums.PostBoss3: return new PostBoss3Cutscene();
                case CutsceneEnums.PostBoss4: return new PostBoss4Cutscene();
                case CutsceneEnums.PostBoss5: return new PostBoss5Cutscene();
                case CutsceneEnums.PostBoss6: return new PostBoss6Cutscene();
                case CutsceneEnums.EndingPt1: return new EndingCutscenePt1();
                case CutsceneEnums.EndingPt2: return new EndingCutscenePt2();
                case CutsceneEnums.Credits: return new CreditsCutscene();
                case CutsceneEnums.DemoEnd: return new DemoEndCutscene();
                case CutsceneEnums.TitleDemo: return new TitleDemoCutscene();
            }
        }
    }
}