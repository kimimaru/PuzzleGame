/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// A cutscene trigger. When the player touches it, a cutscene will play.
    /// </summary>
    public sealed class CutsceneTrigger : CollisionObject
    {
        /// <summary>
        /// The ID of the cutscene to play.
        /// </summary>
        private CutsceneEnums CutsceneID = CutsceneEnums.Intro;

        public override void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            base.Initialize(context, initInfo);

            CollisionType = CollisionTypes.Trigger;
            TriggerQuery = TriggerQueries.Constant;

            if (initInfo.Properties.TryGetValue(LevelGlobals.CutsceneIDKey, out string cutsceneIDVal) == true)
            {
                Enum.TryParse(cutsceneIDVal, true, out CutsceneID);
            }
        }

        public override bool QueryTrigger(List<ICollisionObj> collisions)
        {
            if (Context.Player.PlayerState == PlayerStates.Idle && Context.Player.CollisionRect.Intersects(CollisionRect) == true)
            {
                Context.RemoveObject(this);
                Cutscene cutscene = CutsceneLoader.LoadCutscene(CutsceneID);
                cutscene.Start();
                return true;
            }

            return false;
        }
    }
}