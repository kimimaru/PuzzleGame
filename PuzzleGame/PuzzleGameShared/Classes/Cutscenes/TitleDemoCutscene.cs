/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The cutscene for the title demo.
    /// </summary>
    public sealed class TitleDemoCutscene : Cutscene
    {
        public override void Start()
        {
            IngameState InGameState = new IngameState(-1, AssetManager.Instance.LoadTiledMap(ContentGlobals.TitleDemoLevel), LevelEventTypes.Custom,
                "M-B", "Title Demo", "1337", "13:37");
            InGameState.ChangeLevelState(new TitleDemoState(InGameState));

            GameStateManager.Instance.ChangeGameState(InGameState);
        }
    }
}