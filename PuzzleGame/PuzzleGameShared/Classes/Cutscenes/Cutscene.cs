/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// Represents a cutscene.
    /// </summary>
    public abstract class Cutscene
    {
        public string LevelName = string.Empty;
        public string LevelDescription = string.Empty;
        public int LevelID = 0;

        /// <summary>
        /// Starts the cutscene.
        /// </summary>
        public abstract void Start();
    }
}