/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The cutscene after the fifth boss level.
    /// </summary>
    public sealed class PostBoss5Cutscene : Cutscene
    {
        public override void Start()
        {
            IngameState curIngameState = (IngameState)GameStateManager.Instance.CurrentState;

            curIngameState.ChangeLevelState(new PostBoss5SceneState(curIngameState));
        }
    }
}