/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The intro cutscene.
    /// </summary>
    public sealed class IntroCutscene : Cutscene
    {
        public override void Start()
        {
            TiledMap map = AssetManager.Instance.LoadTiledMap(LevelName);

            IngameState introScene = new IngameState(LevelID, map, LevelEventTypes.Custom, "Cutscene", LevelDescription, string.Empty, string.Empty);
            GameStateManager.Instance.ChangeGameState(introScene);

            introScene.ChangeLevelState(new IntroSceneStartState(introScene));
        }
    }
}