/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The cutscene after a boss level.
    /// </summary>
    public sealed class PostBossCutsceneLoad : Cutscene
    {
        private string CutsceneLevelName = string.Empty;

        public PostBossCutsceneLoad(string cutsceneLevelName)
        {
            CutsceneLevelName = cutsceneLevelName;
        }

        public override void Start()
        {
            IngameState curIngameState = (IngameState)GameStateManager.Instance.CurrentState;
            string levelName = curIngameState.DisplayLevelName;

            IngameState InGameState = new IngameState(-1, AssetManager.Instance.LoadTiledMap(CutsceneLevelName),
                LevelEventTypes.Custom, "Cutscene", levelName, string.Empty, string.Empty);
            InGameState.ChangeLevelState(new LevelIntroState(InGameState, InGameState.Context.Player.transform.Position));

            GameStateManager.Instance.ChangeGameState(InGameState);
        }
    }
}