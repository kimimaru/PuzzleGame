/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// Part 1 of the ending cutscene.
    /// </summary>
    public class EndingCutsceneStatePt1 : BaseCutsceneState
    {
        private enum EndingPhases
        {
            InitWait, FadeIn, Rumble, RumbleEnd, PostRumbleFadeIn, PortalPulse
        }

        private IngameState inGameState = null;

        private Texture2D UITex = null;
        private SoundEffect RumbleSound = null;
        private SoundManager.SoundInstanceHolder RumbleSoundInstance = null;
        private SoundEffect ExclamationSound = null;
        private SoundEffect SpotPlacedSound = null;

        private const float RumbleMaxVolume = 1f;
        private const float RumbleMinVolume = 0f;

        private const double InitWaitTime = 500d;

        private readonly Color InitOverlayColor = Color.Black;
        private readonly Color InitOverlayFadeOutColor = Color.Black * .5f;
        private readonly Color OverlayFadeOutColor = Color.Transparent;
        private readonly Color OverlayPulseOutColor = Color.White * 0f;
        private readonly Color OverlayPulseInColor = Color.White * .5f; 
        private Color CurOverlayColor = Color.Black;
        private const double FadeInTime = 800d;
        private const double PostRumbleFadeInTime = 700d;
        private const double PulseWhiteTime = 1200d;

        private readonly Rectangle BubbleRect = ContentGlobals.CSBubbleExclamationRect;
        private readonly Rectangle ExclamationRect = ContentGlobals.CSExclamationRect;

        private readonly Vector2 BubbleOffset = ContentGlobals.CSBubbleOffset;

        private const double IconAppearTime = 200d;

        private const double ExclamationTime = 1500d;
        private const float ExclamationScale = 1f;
        private double ElapsedExclamationTime = 0d;

        private Vector2[] CamShakes = null;
        private int CamShakeIndex = 0;
        private const double StartShakeChangeTime = 25d;
        private const double EndShakeChangeTime = 102d;
        private double ShakeChangeTime = StartShakeChangeTime;
        private double ElapsedShakeTime = 0d;

        private readonly Direction[] TurnDirections = new Direction[3] { Direction.Left, Direction.Right, Direction.Up };
        private int TurnDirectionIndex = 0;
        private const double TurnDirectionTime = 800d;
        private double ElapsedTurnDirectionTime = 0d;

        private const double RumbleEndTime = 2000d;

        private const double PortalPulseWait = 1200d;
        private const double LastPortalPulseDelay = 2000d;
        private const int MaxPortalPulses = 4;
        private int CurPortalPulses = 0;

        private EndingPhases CurPhase = EndingPhases.InitWait;
        private double ElapsedTime = 0d;

        public EndingCutsceneStatePt1(IngameState ingamestate)
        {
            inGameState = ingamestate;
        }

        protected override void DerivedEnter()
        {
            //Upon entering, check if bonus levels are unlocked by seeing if the bonus unlock screen should be played
            //Bonus levels are unlocked immediately upon completing the last level so this flag shouldn't change in between
            if (DataHandler.saveData.Misc.ShowBonusUnlock == true)
            {
                //Disable pausing and exiting the cutscene the first time around
                inGameState.LevelMap.AllowsPause = false;
                IsSkippable = false;
            }

            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            RumbleSound = AssetManager.Instance.LoadSound(ContentGlobals.RumbleSound);
            ExclamationSound = AssetManager.Instance.LoadSound(ContentGlobals.ExclamationSound);
            SpotPlacedSound = AssetManager.Instance.LoadSound(ContentGlobals.CorrectSpotSound);

            SoundManager.Instance.StopAndClearMusicTrack();

            CamShakes = LevelGlobals.StandardCameraShakes;
            CamShakeIndex = 0;

            inGameState.camera.DefaultPosition = inGameState.camera.Position;

            inGameState.Context.Player.FacingDir = Direction.Up;
            inGameState.Context.Player.ChangeState(new BlankState(true));

            CurOverlayColor = InitOverlayColor;

#if DEBUG
            Debug.AddCustomDebugCommand(DebugSkip);
#endif
        }

        public override void Exit()
        {
            SoundManager.Instance.StopAllSounds(RumbleSound);

            RumbleSound = null;
            RumbleSoundInstance = null;

            UITex = null;
            ExclamationSound = null;
            SpotPlacedSound = null;

#if DEBUG
            Debug.RemoveCustomDebugCommand(DebugSkip);
#endif
        }

        protected override void DerivedUpdate()
        {
            inGameState.Context.Player.Update();
            inGameState.Context.LevelPortal.Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (CurPhase == EndingPhases.InitWait)
            {
                if (ElapsedTime >= InitWaitTime)
                {
                    CurPhase = EndingPhases.FadeIn;
                    ElapsedTime = 0d;
                }
                return;
            }

            if (CurPhase == EndingPhases.FadeIn)
            {
                if (ElapsedTime >= FadeInTime)
                {
                    CurOverlayColor = InitOverlayFadeOutColor;
                    CurPhase = EndingPhases.Rumble;
                    ElapsedTime = 0d;

                    RumbleSoundInstance = SoundManager.Instance.PlaySound(RumbleSound, true, RumbleMaxVolume);
                    SoundManager.Instance.PlaySound(ExclamationSound, false, 1f);
                }
                else
                {
                    CurOverlayColor = Interpolation.Interpolate(InitOverlayColor, InitOverlayFadeOutColor, ElapsedTime / FadeInTime, Interpolation.InterpolationTypes.Linear);
                }
                return;
            }

            if (CurPhase == EndingPhases.Rumble)
            {
                ElapsedExclamationTime += Time.ElapsedTime.TotalMilliseconds;

                HandleCameraShake();

                if (TurnDirectionIndex < TurnDirections.Length)
                {
                    ElapsedTurnDirectionTime += Time.ElapsedTime.TotalMilliseconds;
                    if (ElapsedTurnDirectionTime >= TurnDirectionTime)
                    {
                        inGameState.Context.Player.FacingDir = TurnDirections[TurnDirectionIndex];
                        TurnDirectionIndex++;
                        ElapsedTurnDirectionTime = 0d;
                    }
                }
                else
                {
                    CurPhase = EndingPhases.RumbleEnd;
                    ElapsedTime = 0d;
                }

                return;
            }

            if (CurPhase == EndingPhases.RumbleEnd)
            {
                HandleCameraShake();

                if (ElapsedTime >= RumbleEndTime)
                {
                    SoundManager.Instance.StopAllSounds(RumbleSound);
                    inGameState.camera.SetTranslation(inGameState.camera.DefaultPosition);

                    CurPhase = EndingPhases.PostRumbleFadeIn;
                    ElapsedTime = 0d;
                }
                else
                {
                    if (ElapsedShakeTime == 0d)
                    {
                        ShakeChangeTime = Interpolation.Interpolate(StartShakeChangeTime, EndShakeChangeTime, ElapsedTime / RumbleEndTime, Interpolation.InterpolationTypes.Linear);
                    }

                    //This can be null if playing without an audio device
                    if (RumbleSoundInstance != null)
                    {
                        RumbleSoundInstance.Volume = Interpolation.Interpolate(RumbleMaxVolume, RumbleMinVolume, ElapsedTime / RumbleEndTime, Interpolation.InterpolationTypes.Linear);
                        RumbleSoundInstance.SoundInstance.Volume = SoundManager.Instance.CalculateSoundVolume(RumbleSoundInstance.Volume);
                    }
                }

                return;
            }

            if (CurPhase == EndingPhases.PostRumbleFadeIn)
            {
                if (ElapsedTime >= PostRumbleFadeInTime)
                {
                    CurOverlayColor = Color.Transparent;
                    CurPhase = EndingPhases.PortalPulse;
                    ElapsedTime = 0d;

                    HandlePortalPulse();
                }
                else
                {
                    CurOverlayColor = Interpolation.Interpolate(InitOverlayFadeOutColor, OverlayFadeOutColor, ElapsedTime / PostRumbleFadeInTime, Interpolation.InterpolationTypes.Linear);
                }

                return;
            }

            if (CurPhase == EndingPhases.PortalPulse)
            {
                if (CurPortalPulses < (MaxPortalPulses - 1))
                {
                    if (ElapsedTime >= PortalPulseWait)
                    {
                        HandlePortalPulse();
                        ElapsedTime = 0d;
                    }
                }
                else
                {
                    if (ElapsedTime >= LastPortalPulseDelay)
                    {
                        ElapsedTime = 0d;
                        SoundManager.Instance.PlaySound(SpotPlacedSound, false, .5f);

                        //Start the next part of the cutscene
                        Cutscene nextCutscene = CutsceneLoader.LoadCutscene(CutsceneEnums.EndingPt2);
                        nextCutscene.Start();
                    }
                }

                return;
            }
        }

        private void HandlePortalPulse()
        {
            inGameState.Context.IncrementPortalUnlock(1);
            CurPortalPulses++;
            SoundManager.Instance.PlaySound(SpotPlacedSound, false, .5f);
        }

        private void HandleCameraShake()
        {
            inGameState.camera.SetTranslation(inGameState.camera.DefaultPosition + CamShakes[CamShakeIndex]);

            ElapsedShakeTime += Time.ElapsedTime.TotalMilliseconds;
            if (ElapsedShakeTime >= ShakeChangeTime)
            {
                CamShakeIndex = UtilityGlobals.Wrap(CamShakeIndex + 1, 0, CamShakes.Length - 1);
                ElapsedShakeTime = 0d;
            }
        }

        protected override void DerivedRender()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();

            if (CurPhase == EndingPhases.Rumble && ElapsedExclamationTime < ExclamationTime)
            {
                RenderExclamation();
            }

            RenderingManager.Instance.EndCurrentBatch();

            if (CurOverlayColor.A > 0)
            {
                RenderOverlay();
            }
        }

        private void RenderExclamation()
        {
            Vector2 bubblePos = inGameState.Context.Player.transform.Position + BubbleOffset;

            float timeVal = (float)UtilityGlobals.Clamp(ElapsedExclamationTime / IconAppearTime, 0d, 1d);

            float scale = UtilityGlobals.Clamp(timeVal, 0f, 1f) * ExclamationScale;

            RenderingManager.Instance.DrawSprite(UITex, bubblePos, BubbleRect, Color.White, 0f,
                BubbleRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .05f);

            RenderingManager.Instance.DrawSprite(UITex, bubblePos, ExclamationRect, Color.White, 0f,
                ExclamationRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .06f);
        }

        private void RenderOverlay()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

            RenderingManager.Instance.DrawSprite(UITex, Vector2.Zero, ContentGlobals.BoxRect, CurOverlayColor, 0f, Vector2.Zero, RenderingGlobals.BaseResolution,
                SpriteEffects.None, .1f);

            RenderingManager.Instance.EndCurrentBatch();
        }

        protected override void SkipCutscene()
        {
            //Skip the entire cutscene, including the credits
            //The way this cutscene is handled doesn't serve it well to cancel specific parts
            //This brings players back to the game as quick as possible since skipping this is only possible on subsequent playthroughs
            GameStateManager.Instance.ChangeGameState(new OverworldState(DataHandler.saveData.LastPlayedWorld));
        }

        private void DebugSkip()
        {
            if (KeyboardInput.GetKey(Keys.Tab) == false)
            {
                return;
            }

            if (KeyboardInput.GetKeyDown(Keys.S) == false)
            {
                return;
            }

            Cutscene nextCutscene = CutsceneLoader.LoadCutscene(CutsceneEnums.EndingPt2);
            nextCutscene.Start();
        }
    }
}