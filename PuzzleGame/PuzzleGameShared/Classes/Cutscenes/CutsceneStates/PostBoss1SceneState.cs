/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// The cutscene after the first boss level.
    /// </summary>
    public class PostBoss1SceneState : BaseCutsceneState
    {
        private enum PostBoss1Phases
        {
            Exclaim, Disappear, Wait
        }

        private IngameState inGameState = null;

        private Texture2D UITex = null;
        private SoundEffect ExclamationSound = null;
        private SoundEffect WarpSound = null;
        private readonly Rectangle BubbleRect = ContentGlobals.CSBubbleExclamationRect;
        private readonly Rectangle ExclamationRect = ContentGlobals.CSExclamationRect;

        private readonly Vector2 BubbleOffset = ContentGlobals.CSBubbleOffset;

        private const double IconAppearTime = 200d;

        private const double ExclamationTime = 1500d;
        private const float ExclamationScale = 1f;

        private const double DisappearTime = 1000d;
        private const double BlinkRate = 30d;
        private double ElapsedBlinkRate = 0d;

        private const double ExitLevelFadeTime = 1000d;
        private const double ExitLevelFlyTime = 1000d;

        private const double WaitTime = 500d;

        private PostBoss1Phases CurPhase = PostBoss1Phases.Exclaim;
        private double ElapsedTime = 0d;

        public PostBoss1SceneState(IngameState ingamestate)
        {
            inGameState = ingamestate;
        }

        protected override void DerivedEnter()
        {
            inGameState.Context.Player.FacingDir = Direction.Right;
            inGameState.Context.Player.ChangeState(new BlankState(true));

            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            ExclamationSound = AssetManager.Instance.LoadSound(ContentGlobals.ExclamationSound);
            WarpSound = AssetManager.Instance.LoadSound(ContentGlobals.WarpSound);

            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent -= OnPlayerEnterPortal;
            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent += OnPlayerEnterPortal;

            SoundManager.Instance.PlaySound(ExclamationSound, false, 1f);

#if DEBUG
            Debug.AddCustomDebugCommand(DebugSkip);
#endif
        }

        public override void Exit()
        {
            UITex = null;
            ExclamationSound = null;

#if DEBUG
            Debug.RemoveCustomDebugCommand(DebugSkip);
#endif
        }

        //Use a static method because this state will be gone when the player regains control, and we don't want the portal holding onto it via the handler
        private static void OnPlayerEnterPortal()
        {
            IngameState ingamestate = (IngameState)GameStateManager.Instance.CurrentState;
            ingamestate.ChangeLevelState(new LevelFlyOutState(ingamestate, Color.Transparent, Color.Black, ExitLevelFadeTime, ExitLevelFlyTime, true));
            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);
        }

        protected override void DerivedUpdate()
        {
            inGameState.Context.Player.Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (CurPhase == PostBoss1Phases.Exclaim)
            {
                if (ElapsedTime >= ExclamationTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss1Phases.Disappear;

                    //Clear all particles so it looks consistent while the mole is blinking
                    inGameState.Context.LevelPortal.ActivatedParticles.ClearParticles();
                }
                return;
            }

            if (CurPhase == PostBoss1Phases.Disappear)
            {
                if (ElapsedTime >= DisappearTime)
                {
                    ElapsedBlinkRate = 0d;
                    ElapsedTime = 0d;

                    inGameState.Context.RemoveObject(inGameState.Context.RockShooters[0]);
                    SoundManager.Instance.PlaySound(WarpSound, false, 1f);

                    CurPhase = PostBoss1Phases.Wait;
                }
                else
                {
                    ElapsedBlinkRate += Time.ElapsedTime.TotalMilliseconds;

                    if (ElapsedBlinkRate >= BlinkRate)
                    {
                        inGameState.Context.RockShooters[0].TintColor = inGameState.Context.RockShooters[0].TintColor == Color.Transparent ? Color.White : Color.Transparent;

                        ElapsedBlinkRate = 0d;
                    }
                }
                return;
            }

            if (CurPhase == PostBoss1Phases.Wait)
            {
                if (ElapsedTime >= WaitTime)
                {
                    ElapsedTime = 0d;

                    inGameState.Context.Player.ChangeState(new IdleState());
                    inGameState.ChangeLevelState(new PlayingState(inGameState));
                }
            }
        }

        protected override void DerivedRender()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();

            if (CurPhase == PostBoss1Phases.Exclaim)
            {
                Vector2 bubblePos = inGameState.Context.Player.transform.Position + BubbleOffset;

                float timeVal = (float)(ElapsedTime / IconAppearTime);

                float scale = UtilityGlobals.Clamp(timeVal, 0f, 1f) * ExclamationScale;

                RenderingManager.Instance.DrawSprite(UITex, bubblePos, BubbleRect, Color.White, 0f,
                    BubbleRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .05f);

                RenderingManager.Instance.DrawSprite(UITex, bubblePos, ExclamationRect, Color.White, 0f,
                    ExclamationRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .06f);
            }

            RenderingManager.Instance.EndCurrentBatch();
        }

        protected override void SkipCutscene()
        {
            if (inGameState.Context.RockShooters.Count > 0)
            {
                inGameState.Context.RemoveObject(inGameState.Context.RockShooters[0]);
            }
            inGameState.Context.Player.ChangeState(new IdleState());
            inGameState.ChangeLevelState(new PlayingState(inGameState));
        }

        private void DebugSkip()
        {
            if (KeyboardInput.GetKey(Keys.Tab, Debug.DebugKeyboard) == false) return;

            if (KeyboardInput.GetKeyDown(Keys.S, Debug.DebugKeyboard) == true)
            {
                CurPhase = PostBoss1Phases.Disappear;
                ElapsedTime = DisappearTime;
            }
        }
    }
}