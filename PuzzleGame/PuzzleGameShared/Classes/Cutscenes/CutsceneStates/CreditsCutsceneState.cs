/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// The credits cutscene.
    /// </summary>
    public class CreditsCutsceneState : BaseCutsceneState
    {
        private enum CreditsPhases
        {
            InitFade, Burrowing, Surface, Jump1, Jump2, Wait, CreditsScroll, LogoFadeIn, TheEndScale, PromptFade, PromptInput, FinalFadeOut, FinalWait
        }

        private IngameState inGameState = null;

        private Texture2D UITex = null;
        private Texture2D LogoTex = null;
        private SpriteFont Font = null;
        private SpriteFont PromptFont = null;

        private readonly Color OverlayStartColor = Color.White;
        private readonly Color OverlayEndColor = Color.White * 0f;
        private Color CurOverlayColor = Color.White;

        private const double InitFadeTime = 700d;
        private const float StartingMusicVolume = 0f;
        private float OrigMusicVolume = .5f;

        private Vector2 OrigPlayerPos = Vector2.Zero;
        private Rectangle OrigPlayerSourceRect = Rectangle.Empty;
        private Vector2 PlayerBurrowStartPos = Vector2.Zero;
        private Vector2 PlayerBurrowEndPos = Vector2.Zero;

        private ParticleEngine BurrowingParticles = null;

        private const double BurrowingTime = 800d;
        private const double SurfaceTime = 800d;

        private const double JumpTime = 500d;
        private const float PlayerJumpEndY = -50f;
        private Vector2 PlayerJumpEndPos = Vector2.Zero;

        private const double WaitTime = 500d;

        private const double HopTime = 150d;
        private const float HopYOffset = -10f;
        private double ElapsedHopTime = 0d;

        private CreditsDisplay Credits = null;

        private const double LogoFadeTime = 1500d;
        private readonly Color LogoStartColor = Color.Transparent;
        private readonly Color LogoEndColor = Color.White;
        private readonly Vector2 LogoSpritePos = new Vector2(RenderingGlobals.BaseResWidthHalved, 65f);
        private UISprite LogoSprite = null;

        private const string TheEndText = "THE END";
        //private const string PromptText = "Press Select/Grab";
        private UIText TheEndTextObj = null;
        private UIText PromptTextObj = null;
        private readonly Vector2 TheEndTextPos = new Vector2(RenderingGlobals.BaseResWidthHalved, 170f);
        private readonly Vector2 PromptTextPos = new Vector2(RenderingGlobals.BaseResWidthHalved, RenderingGlobals.BaseResHeightHalved + 125f);
        private readonly Color TheEndColor = Color.White;
        private readonly Vector2 TheEndScaleStart = Vector2.Zero;
        private readonly Vector2 TheEndScaleEnd = Vector2.One;
        private Color PromptColorStart = Color.White * 0f;
        private readonly Color PromptColorEnd = new Color(143, 135, 255, 255);
        private readonly Color OutlineColorStart = Color.Black * 0f;
        private readonly Color OutlineColorEnd = Color.Black;

        private const double TheEndTime = 1500d;
        private const double PromptTime = 400d;
        private const double PromptBobTime = 1200d;//150d;
        private const float PromptBobYRange = -3f;
        private readonly Vector2 PromptStartBob = new Vector2(0f, 5f);
        private readonly Vector2 PromptEndBob = new Vector2(0f, -5f);
        private double ElapsedPromptBobTime = 0d;

        private const double FinalFadeTime = 3000d;
        private readonly Color FinalFadeStart = Color.Black * 0f;
        private readonly Color FinalFadeEnd = Color.Black;

        private const double FinalWaitTime = 400d;

        private CreditsPhases CurPhase = CreditsPhases.InitFade;
        private double ElapsedTime = 0d;

        public CreditsCutsceneState(IngameState ingamestate)
        {
            inGameState = ingamestate;

            //The credits scene is unskippable
            IsSkippable = false;
        }

        protected override void DerivedEnter()
        {
            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            LogoTex = AssetManager.Instance.LoadTexture(ContentGlobals.BGIcons);
            Font = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont24px);
            PromptFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont21px);

            //Ease the music in by starting it off at 0 then going to the set value
            OrigMusicVolume = SoundManager.Instance.MusicVolume;
            SoundManager.Instance.MusicVolume = 0f;

            string creditsMusic = inGameState.LevelMap.MusicName;
            if (string.IsNullOrEmpty(creditsMusic) == true)
            {
                creditsMusic = ContentGlobals.CreditsMusic;
            }

            SoundManager.Instance.PlayMusic(AssetManager.Instance.LoadMusic(creditsMusic), true, true);
            inGameState.Context.Player.FacingDir = Direction.Down;
            inGameState.Context.Player.ChangeState(new BlankState(true));
            
            //Update the current animation once to ensure the direction changed
            inGameState.Context.Player.AnimationManager.CurrentAnim.Update();

            OrigPlayerPos = inGameState.Context.Player.transform.Position;
            
            OrigPlayerSourceRect = inGameState.Context.Player.sprite.SourceRect.Value;

            CurOverlayColor = OverlayStartColor;

            PlayerBurrowStartPos = OrigPlayerPos + new Vector2(0f, 32f);
            PlayerBurrowEndPos = PlayerBurrowStartPos + new Vector2(0f, -OrigPlayerSourceRect.Height);

            PlayerJumpEndPos = OrigPlayerPos + new Vector2(0f, PlayerJumpEndY);

            //Set up credits and end text objects
            Credits = new CreditsDisplay(false);

            LogoSprite = new UISprite(new Sprite(LogoTex, ContentGlobals.MBLogoRect));
            LogoSprite.TintColor = LogoStartColor;
            LogoSprite.Position = LogoSpritePos;

            TheEndTextObj = UIHelpers.CreateCenterUIText(Font, TheEndText, TheEndScaleStart, TheEndColor, UITextOptions.Outline, UITextData.Standard);
            TheEndTextObj.Position = TheEndTextPos;

            string selectStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.Select);
            //string backOutStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.BackOut);

            string promptStr = $"Press {selectStr}";

            PromptTextObj = UIHelpers.CreateCenterUIText(PromptFont, promptStr, Vector2.One, PromptColorStart, UITextOptions.Outline, UITextData.Standard);
            PromptTextObj.Position = PromptTextPos + PromptStartBob;

            TheEndTextObj.TextData.OutlineColor = OutlineColorEnd;
            PromptTextObj.TextData.OutlineColor = OutlineColorStart;

            PromptColorStart = PromptColorEnd * 0f;

#if DEBUG
            Debug.AddCustomDebugCommand(DebugSkipCreditsScroll);
#endif
        }

        public override void Exit()
        {
            SoundManager.Instance.StopAndClearMusicTrack();
            SoundManager.Instance.MusicVolume = OrigMusicVolume;

            UITex = null;
            LogoTex = null;
            Font = null;
            PromptFont = null;

#if DEBUG
            Debug.RemoveCustomDebugCommand(DebugSkipCreditsScroll);
#endif
        }

        protected override void DerivedUpdate()
        {
            inGameState.Context.Player.Update();

            if (CurPhase == CreditsPhases.InitFade || CurPhase == CreditsPhases.Burrowing)
            {
                //Set the source rectangle's height to 0 to make the Echidna invisible until it surfaces
                Rectangle rect = OrigPlayerSourceRect;
                rect.Height = 0;
                inGameState.Context.Player.sprite.SourceRect = rect;
            }

            if (CurPhase == CreditsPhases.InitFade)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
                if (ElapsedTime >= InitFadeTime)
                {
                    SoundManager.Instance.MusicVolume = OrigMusicVolume;
                    CurOverlayColor = OverlayEndColor;

                    ElapsedTime = 0d;
                    CurPhase = CreditsPhases.Burrowing;

                    //Start the particles
                    InitializeParticleEngine();
                    BurrowingParticles.Resume();
                }
                else
                {
                    double time = ElapsedTime / InitFadeTime;

                    SoundManager.Instance.MusicVolume = Interpolation.Interpolate(StartingMusicVolume, OrigMusicVolume, time, Interpolation.InterpolationTypes.Linear);
                    CurOverlayColor = Interpolation.Interpolate(OverlayStartColor, OverlayEndColor, time, Interpolation.InterpolationTypes.Linear);
                }

                return;
            }

            if (CurPhase == CreditsPhases.Burrowing)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                BurrowingParticles.Update();

                if (ElapsedTime >= BurrowingTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = CreditsPhases.Surface;
                }

                return;
            }

            if (CurPhase == CreditsPhases.Surface)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                BurrowingParticles.Update();

                if (ElapsedTime >= SurfaceTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = CreditsPhases.Jump1;

                    inGameState.Context.Player.transform.Position = PlayerBurrowEndPos;
                    inGameState.Context.Player.sprite.SourceRect = OrigPlayerSourceRect;
                    BurrowingParticles.Stop(true);

                    inGameState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Success);
                    SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.CSJumpSound), false, 1f);
                }
                else
                {
                    double time = ElapsedTime / SurfaceTime;

                    inGameState.Context.Player.transform.Position = Interpolation.Interpolate(PlayerBurrowStartPos, PlayerBurrowEndPos, time, Interpolation.InterpolationTypes.Linear);

                    Rectangle rect = inGameState.Context.Player.sprite.SourceRect.Value;
                    rect.Height = Interpolation.Interpolate(0, OrigPlayerSourceRect.Height, time, Interpolation.InterpolationTypes.Linear);
                    inGameState.Context.Player.sprite.SourceRect = rect;
                }

                return;
            }

            if (CurPhase == CreditsPhases.Jump1)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                if (ElapsedTime >= JumpTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = CreditsPhases.Jump2;

                    inGameState.Context.Player.transform.Position = PlayerJumpEndPos;
                }
                else
                {
                    inGameState.Context.Player.transform.Position = Interpolation.Interpolate(PlayerBurrowEndPos, PlayerJumpEndPos, ElapsedTime / JumpTime, Interpolation.InterpolationTypes.CubicOut);
                }

                return;
            }

            if (CurPhase == CreditsPhases.Jump2)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                if (ElapsedTime >= JumpTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = CreditsPhases.Wait;

                    inGameState.Context.Player.transform.Position = OrigPlayerPos;
                }
                else
                {
                    inGameState.Context.Player.transform.Position = Interpolation.Interpolate(PlayerJumpEndPos, OrigPlayerPos, ElapsedTime / JumpTime, Interpolation.InterpolationTypes.QuarticIn);
                }

                return;
            }

            if (CurPhase == CreditsPhases.Wait)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                if (ElapsedTime >= WaitTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = CreditsPhases.CreditsScroll;
                }

                return;
            }
            
            //Update player hopping from hereon
            UpdatePlayerHopping();

            if (CurPhase == CreditsPhases.CreditsScroll)
            {
                Credits.Update();

                //Check if we're done scrolling
                if (UtilityGlobals.IsApproximate(Credits.YScroll, CreditsDisplay.ScrollEndY, .01f) == true)
                {
                    ElapsedTime = 0d;
                    CurPhase = CreditsPhases.LogoFadeIn;
                }

                return;
            }

            if (CurPhase == CreditsPhases.LogoFadeIn)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                if (ElapsedTime >= LogoFadeTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = CreditsPhases.TheEndScale;

                    LogoSprite.TintColor = LogoEndColor;
                }
                else
                {
                    LogoSprite.TintColor = Interpolation.Interpolate(LogoStartColor, LogoEndColor, ElapsedTime / LogoFadeTime, Interpolation.InterpolationTypes.CubicIn);
                }

                return;
            }

            if (CurPhase == CreditsPhases.TheEndScale)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                if (ElapsedTime >= TheEndTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = CreditsPhases.PromptFade;

                    TheEndTextObj.Scale = TheEndScaleEnd;
                }
                else
                {
                    double time = ElapsedTime / TheEndTime;

                    TheEndTextObj.Scale = Interpolation.Interpolate(TheEndScaleStart, TheEndScaleEnd, time, Interpolation.InterpolationTypes.QuarticIn);
                }

                return;
            }

            if (CurPhase == CreditsPhases.PromptFade)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
                //UpdatePromptBob();

                if (ElapsedTime >= PromptTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = CreditsPhases.PromptInput;
                    PromptTextObj.TintColor = PromptColorEnd;
                    PromptTextObj.TextData.OutlineColor = OutlineColorEnd;

                    SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.ConfirmSound), false, 1f);
                }
                else
                {
                    //double time = ElapsedTime / PromptTime;
                    //
                    //PromptTextObj.TintColor = Interpolation.Interpolate(PromptColorStart, PromptColorEnd, time, Interpolation.InterpolationTypes.Linear);
                    //PromptTextObj.TextData.OutlineColor = Interpolation.Interpolate(OutlineColorStart, OutlineColorEnd, time, Interpolation.InterpolationTypes.QuinticIn);
                }

                return;
            }

            if (CurPhase == CreditsPhases.PromptInput)
            {
                UpdatePromptBob();

                if (Input.GetButtonDown(InputActions.Select) == true)
                {
                    ElapsedTime = 0d;
                    CurPhase = CreditsPhases.FinalFadeOut;
                    
                    //Make the prompt text invisible
                    PromptTextObj.TintColor = Color.Transparent;

                    SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);
                }

                return;
            }

            if (CurPhase == CreditsPhases.FinalFadeOut)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                if (ElapsedTime >= FinalFadeTime)
                {
                    CurOverlayColor = FinalFadeEnd;
                    ElapsedTime = 0d;
                    CurPhase = CreditsPhases.FinalWait;
                }
                else
                {
                    double time = ElapsedTime / FinalFadeTime;
                    SoundManager.Instance.MusicVolume = Interpolation.Interpolate(OrigMusicVolume, StartingMusicVolume, time, Interpolation.InterpolationTypes.Linear);
                    CurOverlayColor = Interpolation.Interpolate(FinalFadeStart, FinalFadeEnd, time, Interpolation.InterpolationTypes.Linear);
                }

                return;
            }

            if (CurPhase == CreditsPhases.FinalWait)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                if (ElapsedTime >= FinalWaitTime)
                {
                    //END GAME
                    SoundManager.Instance.StopAndClearMusicTrack();
                    SoundManager.Instance.MusicVolume = OrigMusicVolume;

                    //If we should show the bonus unlock screen, do so, otherwise go back to the title screen
                    if (DataHandler.saveData.Misc.ShowBonusUnlock == true)
                    {
                        GameStateManager.Instance.ChangeGameState(new BonusUnlockState());
                    }
                    else
                    {
                        //Unload all content
                        SoundManager.Instance.ClearAllSounds();
                        SoundManager.Instance.ClearMusicCache();

                        AssetManager.Instance.UnloadLoadedContent();

                        GameStateManager.Instance.ChangeGameState(new TitleState(TitleState.States.Title, false));
                    }
                }

                return;
            }
        }

        private void InitializeParticleEngine()
        {
            Sprite particleSprite = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.LevelObjectsTex), ContentGlobals.BlockParticleRect);
            BurrowingParticles = new ParticleEngine(25, PlayerBurrowStartPos, particleSprite, LevelGlobals.BasePlayerRenderDepth - .01f, 300d, 800d);
            BurrowingParticles.EmissionRate = 16d;
            BurrowingParticles.Position = PlayerBurrowStartPos;
            BurrowingParticles.MinAngularVelocity = UtilityGlobals.ToRadians(0f);
            BurrowingParticles.MaxAngularVelocity = UtilityGlobals.ToRadians(0f);
            BurrowingParticles.MinPositionOffset = new Vector2(-6f, -6f);
            BurrowingParticles.MaxPositionOffset = new Vector2(6f, 6f);
            BurrowingParticles.MinVelocity = new Vector2(-.1f * 60f, -.1f * 60f);
            BurrowingParticles.MaxVelocity = new Vector2(.1f * 60f, .1f * 60f);
            BurrowingParticles.MinParticleLife = 101d;
            BurrowingParticles.MaxParticleLife = 167d;
            BurrowingParticles.MinInitScale = new Vector2(2f, 1f);
            BurrowingParticles.MaxInitScale = new Vector2(3f, 2f);
            BurrowingParticles.MinAcceleration = new Vector2(-.02f * 60f, -.02f * 60f);
            BurrowingParticles.MaxAcceleration = new Vector2(.02f * 60f, .02f * 60f);
            
            BurrowingParticles.ParticleColor = Block.GetParticleColorForSurface(SurfaceTypes.Ground);
            BurrowingParticles.Stop(true);
        }

        private void UpdatePlayerHopping()
        {
            ElapsedHopTime += Time.ElapsedTime.TotalMilliseconds;

            double time = ElapsedHopTime / HopTime;

            float sin = UtilityGlobals.Clamp((float)Math.Sin(time), 0f, 1f);
            Vector2 vec = new Vector2(0f, (HopYOffset * sin));

            inGameState.Context.Player.transform.Position = OrigPlayerPos + vec;
        }

        private void UpdatePromptBob()
        {
            ElapsedPromptBobTime += Time.ElapsedTime.TotalMilliseconds;

            double time = UtilityGlobals.PingPong(ElapsedPromptBobTime / (PromptBobTime / 2), 0d, 1d);
            PromptTextObj.Position = Interpolation.Interpolate(PromptTextPos + PromptStartBob, PromptTextPos + PromptEndBob,
                time, Interpolation.InterpolationTypes.CubicInOut);

            //float posOffset = (float)Math.Sin(ElapsedPromptBobTime / PromptBobTime) * PromptBobYRange;
            //PromptTextObj.Position = PromptTextPos + new Vector2(0f, posOffset);
        }

        protected override void DerivedRender()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();
            if (CurPhase == CreditsPhases.Burrowing || CurPhase == CreditsPhases.Surface)
            {
                BurrowingParticles.Render();
            }

            RenderingManager.Instance.EndCurrentBatch();

            //Render anything that needs to render on top of the level in screen space
            bool startDeferredBatch = (CurPhase == CreditsPhases.CreditsScroll || CurPhase == CreditsPhases.LogoFadeIn
                || CurPhase == CreditsPhases.TheEndScale || CurPhase == CreditsPhases.PromptFade
                || CurPhase == CreditsPhases.PromptInput || CurPhase == CreditsPhases.FinalFadeOut
                || CurPhase == CreditsPhases.FinalWait || CurOverlayColor.A > 0 || PromptTextObj.TintColor.A > 0);

            if (startDeferredBatch == true)
            {
                RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                    BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

                if (CurPhase == CreditsPhases.CreditsScroll)
                {
                    Credits.RenderCredits();
                }

                if (CurPhase >= CreditsPhases.LogoFadeIn)
                {
                    LogoSprite.Render();
                }

                if (CurPhase == CreditsPhases.TheEndScale || CurPhase == CreditsPhases.PromptFade
                    || CurPhase == CreditsPhases.PromptInput || CurPhase == CreditsPhases.FinalFadeOut || CurPhase == CreditsPhases.FinalWait)
                {
                    TheEndTextObj.Render();
                }

                if (PromptTextObj.TintColor.A > 0)
                {
                    PromptTextObj.Render();
                }

                if (CurOverlayColor.A > 0)
                {
                    RenderingManager.Instance.DrawSprite(UITex, Vector2.Zero, ContentGlobals.BoxRect, CurOverlayColor, 0f, Vector2.Zero,
                        RenderingGlobals.BaseResolution, SpriteEffects.None, .1f);
                }

                RenderingManager.Instance.EndCurrentBatch();
            }
        }

        protected override void SkipCutscene()
        {
            //The credits can't be skipped
        }

        private void DebugSkipCreditsScroll()
        {
            if (KeyboardInput.GetKey(Keys.Tab) == false)
            {
                return;
            }

            if (KeyboardInput.GetKeyDown(Keys.S) == false)
            {
                return;
            }

            ElapsedTime = 0d;
            CurPhase = CreditsPhases.LogoFadeIn;
        }
    }
}