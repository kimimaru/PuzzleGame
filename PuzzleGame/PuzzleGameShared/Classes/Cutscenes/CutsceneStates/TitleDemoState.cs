/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The title demo cutscene.
    /// </summary>
    public class TitleDemoState : BaseCutsceneState
    {
        private enum TitleDemoPhases
        {
            WaitRight, PushRight, PushRightWait, WaitLeft, PushLeft, PushLeftWait, WaitDown, PushDown, PushDownWait, FadeOut
        }

        private IngameState inGameState = null;

        private Texture2D UITex = null;

        private const double WaitOneTime = 400d;
        private const double WaitTwoTime = 1200d;
        private const double WrongSpotWaitTime = 500d;
        private const double WaitTime = 350d;

        private const double JumpTime = 150d;
        private double ElapsedJumpTime = 0d;
        private Vector2 PlayerEndPos = Vector2.Zero;

        private const double FadeOutTime = 1500d;
        private readonly Color StartFadeColor = Color.Transparent;
        private readonly Color EndFadeColor = Color.White;
        private Color CurFadeColor = Color.Transparent;

        private int CurBlockIndex = -1;
        private Vector2[] PlayerSpawnPositions = null;
        private Direction[] PlayerFacingDirs = new Direction[3] { Direction.Right, Direction.Left, Direction.Down };

        private TitleDemoPhases CurPhase = TitleDemoPhases.WaitRight;
        private double ElapsedTime = 0d;

        public TitleDemoState(IngameState ingamestate)
        {
            inGameState = ingamestate;

            //The title demo isn't skippable the standard way, as it has its own inputs for skipping
            //We also don't want to show the skip text
            IsSkippable = false;
        }

        protected override void DerivedEnter()
        {
            SoundManager.Instance.StopAndClearMusicTrack();

            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);

            CurFadeColor = StartFadeColor;

            //All blocks aside from the first start out invisible
            for (int i = 1; i < inGameState.Context.Blocks.Count; i++)
                inGameState.Context.Blocks[i].TintColor = Color.Transparent;

            TiledMapObjectLayer spawnPosLayer = inGameState.LevelMap.Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.SpawnPosLayerName);

            //The first position is the player's
            PlayerSpawnPositions = new Vector2[spawnPosLayer.Objects.Length + 1];
            PlayerSpawnPositions[0] = inGameState.Context.Player.transform.Position;

            for (int i = 0; i < spawnPosLayer.Objects.Length; i++)
            {
                TiledMapObject obj = spawnPosLayer.Objects[i];
                PlayerSpawnPositions[i + 1] = obj.Position + inGameState.Context.LevelSpace.TileSizeHalf;
            }

            inGameState.Context.Player.FacingDir = PlayerFacingDirs[0];
            inGameState.Context.Player.ChangeState(new BlankState(false));
            inGameState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Grab);

            inGameState.Context.Player.StateChangedEvent -= OnPlayerStateChanged;
            inGameState.Context.Player.StateChangedEvent += OnPlayerStateChanged;

            CurBlockIndex = -1;
#if DEBUG
            Debug.AddCustomDebugCommand(DebugSkip);
#endif
        }

        private void OnPlayerStateChanged(PlayerStateMachine newState)
        {
            if (inGameState.Context.Player.PlayerState == PlayerStates.Grab)
            {
                //Check for activating the spot
                inGameState.Context.Spots[0].QueryTrigger(inGameState.Context.LevelCollision);
                inGameState.Context.Player.ChangeState(new BlankState(false));

                CurPhase++;

                if (CurPhase == TitleDemoPhases.PushDownWait)
                {
                    PlayerEndPos = inGameState.Context.Player.transform.Position;
                    inGameState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Success);
                }
                else
                {
                    inGameState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Grab);
                }
            }
        }

        public override void Exit()
        {
            UITex = null;

            inGameState.Context.Player.StateChangedEvent -= OnPlayerStateChanged;

#if DEBUG
            Debug.RemoveCustomDebugCommand(DebugSkip);
#endif
        }

        protected override void DerivedUpdate()
        {
            //At any time, if the player presses these buttons, end the demo
            if (Input.GetButtonDown(InputActions.Pause) || Input.GetButtonDown(InputActions.Select) == true)
            {
                SkipCutscene();
                return;
            }

            inGameState.Context.LevelPortal.Update();

            for (int i = 0; i < inGameState.Context.Blocks.Count; i++)
            {
                inGameState.Context.Blocks[i].Update();
            }

            inGameState.Context.Player.Update();

            //States 
            if (CurPhase == TitleDemoPhases.WaitRight || CurPhase == TitleDemoPhases.WaitLeft || CurPhase == TitleDemoPhases.WaitDown)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                if (ElapsedTime >= WaitTime)
                {
                    ElapsedTime = 0d;
                    CurPhase++;
                    
                    CurBlockIndex++;

                    Block curBlock = inGameState.Context.Blocks[CurBlockIndex];
                    curBlock.TintColor = Color.White;

                    inGameState.Context.Player.transform.Position = PlayerSpawnPositions[CurBlockIndex];
                    inGameState.Context.Player.TintColor = Color.White;
                    inGameState.Context.Player.FacingDir = PlayerFacingDirs[CurBlockIndex];

                    Vector2 moveAmt = DirectionUtil.GetVector2ForDir(inGameState.Context.Player.FacingDir) * inGameState.Context.LevelSpace.TileSize;

                    curBlock.ChangeState(new BlockMoveState(curBlock, moveAmt, inGameState.Context.Player.MoveTime, false));
                    inGameState.Context.Player.ChangeState(new PlayerBlockMoveState(curBlock, moveAmt, inGameState.Context.Player.MoveTime));
                }

                return;
            }

            if (CurPhase == TitleDemoPhases.PushRightWait || CurPhase == TitleDemoPhases.PushLeftWait || CurPhase == TitleDemoPhases.PushDownWait)
            {
                if (CurPhase == TitleDemoPhases.PushDownWait)
                {
                    UpdatePlayerHopping();
                }

                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                if (ElapsedTime >= WrongSpotWaitTime)
                {
                    ElapsedTime = 0d;

                    CurPhase++;

                    if (CurPhase != TitleDemoPhases.FadeOut)
                    {
                        inGameState.Context.Player.TintColor = Color.Transparent;
                        inGameState.Context.Blocks[CurBlockIndex].TintColor = Color.Transparent;
                        inGameState.Context.Blocks[CurBlockIndex].transform.Position = inGameState.Context.LevelSpace.TileSizeHalf;

                        inGameState.Context.Spots[0].QueryTrigger(inGameState.Context.LevelCollision);
                    }
                }

                return;
            }

            if (CurPhase == TitleDemoPhases.FadeOut)
            {
                UpdatePlayerHopping();

                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                if (ElapsedTime >= FadeOutTime)
                {
                    CurFadeColor = EndFadeColor;
                    SkipCutscene();
                }
                else
                {
                    CurFadeColor = Interpolation.Interpolate(StartFadeColor, EndFadeColor, ElapsedTime / FadeOutTime, Interpolation.InterpolationTypes.CubicIn);
                }

                return;
            }
        }

        private void UpdatePlayerHopping()
        {
            ElapsedJumpTime += Time.ElapsedTime.TotalMilliseconds;

            double time = ElapsedJumpTime / JumpTime;

            float sin = UtilityGlobals.Clamp((float)Math.Sin(time), 0f, 1f);
            Vector2 vec = new Vector2(0f, (-10f * sin));

            inGameState.Context.Player.transform.Position = PlayerEndPos + vec;
        }

        protected override void DerivedRender()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();

            RenderingManager.Instance.EndCurrentBatch();

            if (CurPhase == TitleDemoPhases.FadeOut)
            {
                if (CurFadeColor.A > 0f)
                {
                    RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                        BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

                    RenderingManager.Instance.DrawSprite(UITex, Vector2.Zero, ContentGlobals.BoxRect, CurFadeColor, 0f,
                        Vector2.Zero, RenderingGlobals.BaseResolution, SpriteEffects.None, 1f);

                    RenderingManager.Instance.EndCurrentBatch();
                }
            }
        }

        protected override void SkipCutscene()
        {
            GameStateManager.Instance.ChangeGameState(new TitleState(TitleState.States.Title, true));
        }

        private void DebugSkip()
        {
            if (KeyboardInput.GetKey(Keys.Tab, Debug.DebugKeyboard) == false) return;

            if (KeyboardInput.GetKeyDown(Keys.S, Debug.DebugKeyboard) == true)
            {
                SkipCutscene();
                return;
            }

            if (KeyboardInput.GetKeyDown(Keys.R, Debug.DebugKeyboard) == true)
            {
                CutsceneLoader.LoadCutscene(CutsceneEnums.TitleDemo).Start();
                return;
            }
        }
    }
}