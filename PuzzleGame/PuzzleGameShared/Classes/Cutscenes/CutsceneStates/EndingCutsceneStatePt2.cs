/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// The ending cutscene.
    /// </summary>
    public class EndingCutsceneStatePt2 : BaseCutsceneState
    {
        private enum EndingPhasesPt2
        {
            Movement, PulseDieDown
        }

        private IngameState inGameState = null;

        private Texture2D UITex = null;
        private SoundEffect PulseSound = null;

        private readonly Color OverlayPulseOutColor = Color.White * 0f;
        private readonly Color OverlayPulseInColor = Color.White * .5f; 
        private Color CurOverlayColor = Color.White * 0f;
        private const double FadeInTime = 800d;
        private const double PostRumbleFadeInTime = 700d;
        private const double PulseWhiteTime = 1200d;

        private const double PulseDieDownFactor = 1f;
        private double PulseDieDownTime = 0d;
        private Color PulseOverlayColor = Color.White;

        private readonly Color OverlayFadeOutStartColor = Color.White * 0f;
        private readonly Color OverlayFadeOutEndColor = Color.White;
        private const double FadeOutTime = 3000d;

        private double ElapsedOverlayTime = 0d;

        private EndingPhasesPt2 CurPhase = EndingPhasesPt2.Movement;
        private double ElapsedTime = 0d;
        private double ElapsedPulseTime = PulseWhiteTime;

        public EndingCutsceneStatePt2(IngameState ingamestate)
        {
            inGameState = ingamestate;
        }

        protected override void DerivedEnter()
        {
            //If bonus levels were just unlocked, disallow skipping
            if (DataHandler.saveData.Misc.ShowBonusUnlock == true)
            {
                inGameState.LevelMap.AllowsPause = false;
                IsSkippable = false;
            }

            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            PulseSound = AssetManager.Instance.LoadSound(ContentGlobals.CSPulseSound);

            CurOverlayColor = OverlayPulseOutColor;

            //Activate the portal
            inGameState.Context.LevelPortal.Activated = true;

            //Set up the event
            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent -= OnPlayerEnterPortal;
            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent += OnPlayerEnterPortal;

            //Give control to the player
            //We can't use the PlayingState here because it won't allow us to render the white overlay
            inGameState.Context.Player.ChangeState(new IdleState());
        }

        public override void Exit()
        {
            if (PulseSound != null)
            {
                SoundManager.Instance.StopAllSounds(PulseSound);
            }

            UITex = null;
            PulseSound = null;

            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent -= OnPlayerEnterPortal;
        }

        protected override void DerivedUpdate()
        {
            inGameState.Context.Player.Update();
            inGameState.Context.LevelPortal.Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            ElapsedPulseTime += Time.ElapsedTime.TotalMilliseconds;
            if (ElapsedPulseTime >= PulseWhiteTime)
            {
                ElapsedPulseTime = 0d;
                SoundManager.Instance.PlaySound(PulseSound, false, 1f);
            }

            if (CurPhase == EndingPhasesPt2.Movement)
            {
                ElapsedOverlayTime += Time.ElapsedTime.TotalMilliseconds;

                double time = UtilityGlobals.PingPong(ElapsedOverlayTime / PulseWhiteTime, 0d, 1d);
                CurOverlayColor = Interpolation.Interpolate(OverlayPulseOutColor, OverlayPulseInColor, time, Interpolation.InterpolationTypes.Linear);

                //Manually check for the player entering the portal
                inGameState.Context.LevelPortal.QueryTrigger(inGameState.Context.LevelCollision);

                return;
            }

            if (CurPhase == EndingPhasesPt2.PulseDieDown)
            {
                if (ElapsedTime >= PulseDieDownTime)
                {
                    ElapsedTime = 0d;
                    CurOverlayColor = OverlayPulseOutColor;

                    //CurPhase = EndingPhasesPt2.FadeOut;

                    //Exit the level like normal with the specified fade
                    //The level should have a cutscene defined so it loads it directly after this
                    LevelFlyOutState flyOut = new LevelFlyOutState(inGameState, OverlayFadeOutStartColor, OverlayFadeOutEndColor, FadeOutTime, FadeOutTime, false);
                    inGameState.ChangeLevelState(flyOut);
                }
                else
                {
                    CurOverlayColor = Interpolation.Interpolate(PulseOverlayColor, OverlayPulseOutColor, ElapsedTime / PulseDieDownTime, Interpolation.InterpolationTypes.Linear);
                }

                return;
            }
        }

        private void OnPlayerEnterPortal()
        {
            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent -= OnPlayerEnterPortal;

            inGameState.Context.Player.FacingDir = Direction.Down;
            inGameState.Context.Player.ChangeState(new BlankState());
            inGameState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Success);

            //Move onto the rest of the cutscene
            ElapsedTime = 0d;
            CurPhase = EndingPhasesPt2.PulseDieDown;

            //Calculate how far along the fade is so we can make it die down faster
            //Multiply by 2 since the PingPong used for the color influences how the final time will be
            double time = Interpolation.InverseLerp(0d, PulseWhiteTime * 2, ElapsedOverlayTime);

            PulseOverlayColor = CurOverlayColor;

            //Subtract any remaining whole numbers (Ex. 1 from 1.66667 to obtain just .66667)
            int timeInt = (int)time;
            PulseDieDownTime = ((time - timeInt) * PulseWhiteTime) * PulseDieDownFactor;

            ElapsedOverlayTime = 0d;

            //SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);
        }

        protected override void DerivedRender()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();

            RenderingManager.Instance.EndCurrentBatch();

            if (CurOverlayColor.A > 0)
            {
                RenderOverlay();
            }
        }

        private void RenderOverlay()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

            RenderingManager.Instance.DrawSprite(UITex, Vector2.Zero, ContentGlobals.BoxRect, CurOverlayColor, 0f, Vector2.Zero, RenderingGlobals.BaseResolution,
                SpriteEffects.None, .1f);

            RenderingManager.Instance.EndCurrentBatch();
        }

        protected override void SkipCutscene()
        {
            //Skip the entire cutscene, including the credits
            //The way this cutscene is handled doesn't serve it well to cancel specific parts
            //This brings players back to the game as quick as possible since skipping this is only possible on subsequent playthroughs
            GameStateManager.Instance.ChangeGameState(new OverworldState(DataHandler.saveData.LastPlayedWorld));
        }
    }
}