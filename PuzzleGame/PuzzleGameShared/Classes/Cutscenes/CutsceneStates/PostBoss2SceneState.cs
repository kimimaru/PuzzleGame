/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// The cutscene after the second boss level.
    /// </summary>
    public class PostBoss2SceneState : BaseCutsceneState
    {
        private enum PostBoss2Phases
        {
            Question, LookUpOne, LookDownOne, Exclaim, LookUpTwo, LookDownTwo, Disappear, WaitTwo
        }

        private IngameState inGameState = null;

        private Texture2D UITex = null;
        private SoundEffect ExclamationSound = null;
        private SoundEffect QuestionSound = null;
        private SoundEffect WarpSound = null;
        private readonly Rectangle BubbleRect = ContentGlobals.CSBubbleExclamationRect;
        private readonly Rectangle ExclamationRect = ContentGlobals.CSExclamationRect;

        private readonly Vector2 BubbleOffset = ContentGlobals.CSBubbleOffset;

        private const double IconAppearTime = 200d;

        private const double LookUpOneTime = 700d;
        private const double LookDownOneTime = 400d;
        private const double LookUpTwoTime = 900d;
        private const double LookDownTwoTime = 400d;

        private const double ExclamationTime = 1500d;
        private const float ExclamationScale = 1f;

        private const double DisappearTime = 1000d;
        private const double BlinkRate = 30d;
        private double ElapsedBlinkRate = 0d;

        private const double ExitLevelFadeTime = 1000d;
        private const double ExitLevelFlyTime = 1000d;

        private const double QuestionTime = 1500d;
        private const double WaitTwoTime = 500d;

        private PostBoss2Phases CurPhase = PostBoss2Phases.Question;
        private double ElapsedTime = 0d;

        public PostBoss2SceneState(IngameState ingamestate)
        {
            inGameState = ingamestate;
        }

        protected override void DerivedEnter()
        {
            inGameState.Context.Player.FacingDir = Direction.Down;
            inGameState.Context.Player.ChangeState(new BlankState(true));

            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            ExclamationSound = AssetManager.Instance.LoadSound(ContentGlobals.ExclamationSound);
            QuestionSound = AssetManager.Instance.LoadSound(ContentGlobals.QuestionSound);
            WarpSound = AssetManager.Instance.LoadSound(ContentGlobals.WarpSound);

            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent -= OnPlayerEnterPortal;
            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent += OnPlayerEnterPortal;

            SoundManager.Instance.PlaySound(QuestionSound, false, 1f);

#if DEBUG
            Debug.AddCustomDebugCommand(DebugSkip);
#endif
        }

        public override void Exit()
        {
            UITex = null;
            ExclamationSound = null;
            QuestionSound = null;
#if DEBUG
            Debug.RemoveCustomDebugCommand(DebugSkip);
#endif
        }

        //Use a static method because this state will be gone when the player regains control, and we don't want the portal holding onto it via the handler
        private static void OnPlayerEnterPortal()
        {
            IngameState ingamestate = (IngameState)GameStateManager.Instance.CurrentState;
            ingamestate.ChangeLevelState(new LevelFlyOutState(ingamestate, Color.Transparent, Color.Black, ExitLevelFadeTime, ExitLevelFlyTime, true));
            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);
        }

        protected override void DerivedUpdate()
        {
            inGameState.Context.LevelPortal.Update();

            for (int i = 0; i < inGameState.Context.Blocks.Count; i++)
            {
                inGameState.Context.Blocks[i].Update();
            }

            inGameState.Context.Player.Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (CurPhase == PostBoss2Phases.Question)
            {
                if (ElapsedTime >= QuestionTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss2Phases.LookUpOne;

                    inGameState.Context.RockShooters[0].FacingDir = Direction.Up;
                    inGameState.Context.RockShooters[0].Update();
                }
                return;
            }

            if (CurPhase == PostBoss2Phases.LookUpOne)
            {
                if (ElapsedTime >= LookUpOneTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss2Phases.LookDownOne;

                    inGameState.Context.RockShooters[0].FacingDir = Direction.Down;
                    inGameState.Context.RockShooters[0].Update();
                }
                return;
            }

            if (CurPhase == PostBoss2Phases.LookDownOne)
            {
                if (ElapsedTime >= LookDownOneTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss2Phases.Exclaim;

                    SoundManager.Instance.PlaySound(ExclamationSound, false, 1f);
                }
                return;
            }

            if (CurPhase == PostBoss2Phases.Exclaim)
            {
                if (ElapsedTime >= ExclamationTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss2Phases.LookUpTwo;

                    inGameState.Context.RockShooters[0].FacingDir = Direction.Up;
                    inGameState.Context.RockShooters[0].Update();
                }
                return;
            }

            if (CurPhase == PostBoss2Phases.LookUpTwo)
            {
                if (ElapsedTime >= LookUpTwoTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss2Phases.LookDownTwo;

                    inGameState.Context.RockShooters[0].FacingDir = Direction.Down;
                    inGameState.Context.RockShooters[0].Update();
                }
                return;
            }

            if (CurPhase == PostBoss2Phases.LookDownTwo)
            {
                if (ElapsedTime >= LookDownTwoTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss2Phases.Disappear;
                }
                return;
            }

            if (CurPhase == PostBoss2Phases.Disappear)
            {
                if (ElapsedTime >= DisappearTime)
                {
                    ElapsedBlinkRate = 0d;
                    ElapsedTime = 0d;

                    inGameState.Context.RemoveObject(inGameState.Context.RockShooters[0]);
                    SoundManager.Instance.PlaySound(WarpSound, false, 1f);

                    CurPhase = PostBoss2Phases.WaitTwo;
                }
                else
                {
                    ElapsedBlinkRate += Time.ElapsedTime.TotalMilliseconds;

                    if (ElapsedBlinkRate >= BlinkRate)
                    {
                        inGameState.Context.RockShooters[0].TintColor = inGameState.Context.RockShooters[0].TintColor == Color.Transparent ? Color.White : Color.Transparent;

                        ElapsedBlinkRate = 0d;
                    }
                }
                return;
            }

            if (CurPhase == PostBoss2Phases.WaitTwo)
            {
                if (ElapsedTime >= WaitTwoTime)
                {
                    ElapsedTime = 0d;

                    inGameState.Context.Player.ChangeState(new IdleState());
                    inGameState.ChangeLevelState(new PlayingState(inGameState));
                }
            }
        }

        protected override void DerivedRender()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();

            if (CurPhase == PostBoss2Phases.Exclaim)
            {
                Vector2 bubblePos = inGameState.Context.RockShooters[0].transform.Position + BubbleOffset;

                float timeVal = (float)(ElapsedTime / IconAppearTime);

                float scale = UtilityGlobals.Clamp(timeVal, 0f, 1f) * ExclamationScale;

                RenderingManager.Instance.DrawSprite(UITex, bubblePos, BubbleRect, Color.White, 0f,
                    BubbleRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .05f);

                RenderingManager.Instance.DrawSprite(UITex, bubblePos, ExclamationRect, Color.White, 0f,
                    ExclamationRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .06f);
            }
            else if (CurPhase == PostBoss2Phases.Question)
            {
                Vector2 bubblePos = inGameState.Context.RockShooters[0].transform.Position + BubbleOffset;

                float timeVal = (float)(ElapsedTime / IconAppearTime);

                float scale = UtilityGlobals.Clamp(timeVal, 0f, 1f) * ExclamationScale;

                RenderingManager.Instance.DrawSprite(UITex, bubblePos, ContentGlobals.CSBubbleQuestionRect, Color.White, 0f,
                    ContentGlobals.CSBubbleQuestionRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .05f);

                RenderingManager.Instance.DrawSprite(UITex, bubblePos, ContentGlobals.CSQuestionRect, Color.White, 0f,
                    ContentGlobals.CSQuestionRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .06f);
            }

            RenderingManager.Instance.EndCurrentBatch();
        }

        protected override void SkipCutscene()
        {
            if (inGameState.Context.RockShooters.Count > 0)
            {
                inGameState.Context.RemoveObject(inGameState.Context.RockShooters[0]);
            }
            inGameState.Context.Player.ChangeState(new IdleState());
            inGameState.ChangeLevelState(new PlayingState(inGameState));
        }

        private void DebugSkip()
        {
            if (KeyboardInput.GetKey(Keys.Tab, Debug.DebugKeyboard) == false) return;

            if (KeyboardInput.GetKeyDown(Keys.S, Debug.DebugKeyboard) == true)
            {
                CurPhase = PostBoss2Phases.WaitTwo;
                ElapsedTime = WaitTwoTime;
            }
        }
    }
}