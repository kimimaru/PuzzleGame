/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// The ambushed sequence of the intro cutscene.
    /// </summary>
    public class IntroSceneAmbushedState : BaseCutsceneState
    {
        private enum IntroAmbushedPhases
        {
            InitWait, WalkDown, Exclamation, MolesAppear, LookAround, RockThrow, FadeOut, FadeIn, LookLeft, LookRight, Question
        }

        private IngameState inGameState = null;

        private Texture2D UITex = null;
        private SoundEffect ExclamationSound = null;
        private SoundEffect QuestionSound = null;
        private readonly Rectangle BubbleRect = ContentGlobals.CSBubbleExclamationRect;
        private readonly Rectangle BubbleQuestionRect = ContentGlobals.CSBubbleQuestionRect;
        private readonly Rectangle ExclamationRect = ContentGlobals.CSExclamationRect;
        private readonly Rectangle QuestionRect = ContentGlobals.CSQuestionRect;

        private const double TextChangeColorTime = 500d;

        private readonly Vector2 InitBubbleOffset = ContentGlobals.CSBubbleOffset;

        private const double LookDownWaitTime = 500d;

        private const double IconAppearTime = 200d;

        private const double ExclamationTime = 1500d;
        private const float ExclamationScale = 1f;

        private const double MolesAppearTime = 800d;

        private readonly Direction[] TurnDirections = new Direction[3] { Direction.Right, Direction.Up, Direction.Down };
        private int CurTurn = 0;
        private const double TurnTime = 400d;

        private const double StunTime = 1020d;
        private const double FadeOutTime = 1100d;
        private readonly Color FadeInColor = Color.Transparent;
        private readonly Color FadeOutColor = Color.Black;
        private Color CurFadeColor = Color.Black;

        private const double LookLeftRightTime = 600d;

        private const double QuestionTime = 1500d;

        private Vector2 DestPos = Vector2.Zero;
        private IntroAmbushedPhases CurPhase = IntroAmbushedPhases.InitWait;
        private double ElapsedTime = 0d;

        private const double ExitLevelFadeTime = 1000d;
        private const double ExitLevelFlyTime = 1000d;

        public IntroSceneAmbushedState(IngameState ingameState)
        {
            inGameState = ingameState;
        }

        protected override void DerivedEnter()
        {
            SoundManager.Instance.StopAndClearMusicTrack();

            inGameState.Context.Player.FacingDir = Direction.Down;
            inGameState.Context.Player.ChangeState(new BlankState(true));

            inGameState.Context.Player.StateChangedEvent -= OnPlayerWalkDown;
            inGameState.Context.Player.StateChangedEvent += OnPlayerWalkDown;

            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent -= OnPlayerEnterPortal;
            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent += OnPlayerEnterPortal;

            DestPos = inGameState.Context.Player.transform.Position + new Vector2(0f, inGameState.Context.LevelSpace.TileSize.Y * 3f);

            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            ExclamationSound = AssetManager.Instance.LoadSound(ContentGlobals.ExclamationSound);
            QuestionSound = AssetManager.Instance.LoadSound(ContentGlobals.QuestionSound);

            for (int i = 0; i < inGameState.Context.RockShooters.Count; i++)
            {
                inGameState.Context.RockShooters[i].HaltThrow = true;
            }

            for (int i = 0; i < inGameState.Context.LevelText.Count; i++)
            {
                inGameState.Context.LevelText[i].ChangeColor(Color.White, Color.Transparent, TextChangeColorTime);
            }

#if DEBUG
            Debug.AddCustomDebugCommand(DebugSkip);
#endif
        }

        public override void Exit()
        {
            UITex = null;
            ExclamationSound = null;
            QuestionSound = null;

            inGameState.Context.Player.StateChangedEvent -= OnPlayerWalkDown;
            inGameState.Context.Player.StateChangedEvent -= OnPlayerStunned;

#if DEBUG
            Debug.RemoveCustomDebugCommand(DebugSkip);
#endif
        }

        private void OnPlayerWalkDown(PlayerStateMachine newState)
        {
            if (CurPhase == IntroAmbushedPhases.WalkDown && inGameState.Context.Player.PlayerState == PlayerStates.Idle)
            {
                if (UtilityGlobals.IsApproximate(inGameState.Context.Player.transform.Position, DestPos, .01f) == false)
                {
                    inGameState.Context.Player.ChangeState(new PlayerMoveState(new Vector2(0f, inGameState.Context.LevelSpace.TileSize.Y),
                        inGameState.Context.Player.MoveTime));
                }
                else
                {
                    inGameState.Context.Player.StateChangedEvent -= OnPlayerWalkDown;
                    inGameState.Context.Player.ChangeState(new BlankState(true));
                    inGameState.Context.Player.FacingDir = Direction.Left;
                    ElapsedTime = 0d;
                    CurPhase = IntroAmbushedPhases.Exclamation;
                    SoundManager.Instance.PlaySound(ExclamationSound, false, 1f);
                    inGameState.camera.LookAtAndClamp(inGameState.Context.Player.transform.Position, inGameState.LevelMap.MapBounds);
                    inGameState.camera.DefaultPosition = inGameState.camera.Position;
                }
            }
        }

        private void OnPlayerStunned(PlayerStateMachine newState)
        {
            if (CurPhase == IntroAmbushedPhases.RockThrow && inGameState.Context.Player.PlayerState == PlayerStates.Stun)
            {
                inGameState.Context.Player.StateChangedEvent -= OnPlayerStunned;

                //Reapply stun with a longer duration
                inGameState.Context.Player.ChangeState(new PlayerStunnedState(Direction.Left, new BlankState(true), StunTime));
                CurPhase = IntroAmbushedPhases.FadeOut;
                ElapsedTime = 0d;

                for (int i = 0; i < inGameState.Context.RockShooters.Count; i++)
                {
                    inGameState.Context.RockShooters[i].HaltThrow = true;
                }
            }
        }

        protected override void DerivedUpdate()
        {
            inGameState.Context.Player.Update();
            for (int i = 0; i < inGameState.Context.RockShooters.Count; i++)
            {
                inGameState.Context.RockShooters[i].Update();
            }

            for (int i = 0; i < inGameState.Context.LevelText.Count; i++)
            {
                inGameState.Context.LevelText[i].Update();
            }

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (CurPhase == IntroAmbushedPhases.InitWait)
            {
                if (ElapsedTime >= LookDownWaitTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = IntroAmbushedPhases.WalkDown;
                    inGameState.Context.Player.ChangeState(new PlayerMoveState(new Vector2(0f, inGameState.Context.LevelSpace.TileSize.Y),
                        inGameState.Context.Player.MoveTime));
                }
                return;
            }

            if (CurPhase == IntroAmbushedPhases.WalkDown)
            {
                inGameState.camera.LookAtAndClamp(inGameState.Context.Player.transform.Position, inGameState.LevelMap.MapBounds);
                inGameState.camera.DefaultPosition = inGameState.camera.Position;
                return;
            }

            if (CurPhase == IntroAmbushedPhases.Exclamation)
            {
                if (ElapsedTime >= ExclamationTime)
                {
                    ElapsedTime = 0d;
                    inGameState.Context.RockShooters[0].FacingDir = Direction.Right;
                    CurPhase = IntroAmbushedPhases.MolesAppear;
                }
                return;
            }

            if (CurPhase == IntroAmbushedPhases.MolesAppear)
            {
                if (ElapsedTime >= MolesAppearTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = IntroAmbushedPhases.LookAround;
                    for (int i = 1; i < inGameState.Context.RockShooters.Count; i++)
                    {
                        RockShooter mole = inGameState.Context.RockShooters[i];
                        int offsetIndex = i - 1;
                        mole.TintColor = Color.White;
                    }
                }
                else
                {
                    for (int i = 1; i < inGameState.Context.RockShooters.Count; i++)
                    {
                        RockShooter mole = inGameState.Context.RockShooters[i];
                        int offsetIndex = i - 1;
                        mole.TintColor = Interpolation.Interpolate(Color.Transparent, Color.White,
                            ElapsedTime / MolesAppearTime, Interpolation.InterpolationTypes.CubicIn);
                    }
                }
                return;
            }

            if (CurPhase == IntroAmbushedPhases.LookAround)
            {
                if (ElapsedTime >= TurnTime)
                {
                    if (CurTurn == (TurnDirections.Length - 1))
                    {
                        inGameState.Context.Player.FacingDir = TurnDirections[CurTurn];
                        ElapsedTime = 0d;
                        CurPhase = IntroAmbushedPhases.RockThrow;

                        //Unhalt the moles from throwing rocks and handle the step after the player gets hit by a rock
                        for (int i = 0; i < inGameState.Context.RockShooters.Count; i++)
                        {
                            inGameState.Context.RockShooters[i].HaltThrow = false;
                        }

                        inGameState.Context.Player.StateChangedEvent -= OnPlayerStunned;
                        inGameState.Context.Player.StateChangedEvent += OnPlayerStunned;
                    }
                    else
                    {
                        inGameState.Context.Player.FacingDir = TurnDirections[CurTurn];
                        CurTurn++;
                        ElapsedTime = 0d;
                    }
                }
                return;
            }

            if (CurPhase == IntroAmbushedPhases.FadeOut)
            {
                if (ElapsedTime >= FadeOutTime)
                {
                    CurFadeColor = FadeOutColor;
                    CurPhase = IntroAmbushedPhases.FadeIn;
                    ElapsedTime = 0d;

                    //Remove all moles then show the portal, block, and spot
                    for (int i = inGameState.Context.RockShooters.Count - 1; i >= 0; i--)
                    {
                        inGameState.Context.RemoveObject(inGameState.Context.RockShooters[i]);
                    }

                    //Stop halting the portal's color from changing and call a manual update to make it update its color
                    inGameState.Context.LevelPortal.HaltColorUpdate = false;
                    inGameState.Context.LevelPortal.Update();

                    for (int i = 0; i < inGameState.Context.Blocks.Count; i++)
                    {
                        Block b = inGameState.Context.Blocks[i];
                        if (b.BlockPattern != BlockPatterns.None)
                        {
                            b.TintColor = Color.White;
                        }
                    }

                    inGameState.Context.Spots[0].TintColor = Color.White;
                }
                else
                {
                    CurFadeColor = Interpolation.Interpolate(FadeInColor, FadeOutColor, ElapsedTime / FadeOutTime, Interpolation.InterpolationTypes.Linear);
                }
                return;
            }

            if (CurPhase == IntroAmbushedPhases.FadeIn)
            {
                if (ElapsedTime >= FadeOutTime)
                {
                    CurFadeColor = FadeOutColor;
                    CurPhase = IntroAmbushedPhases.LookLeft;
                    ElapsedTime = 0d;
                    inGameState.Context.Player.FacingDir = Direction.Left;
                }
                else
                {
                    CurFadeColor = Interpolation.Interpolate(FadeOutColor, FadeInColor, ElapsedTime / FadeOutTime, Interpolation.InterpolationTypes.Linear);
                }
                return;
            }

            if (CurPhase == IntroAmbushedPhases.LookLeft)
            {
                if (ElapsedTime >= LookLeftRightTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = IntroAmbushedPhases.LookRight;
                    inGameState.Context.Player.FacingDir = Direction.Right;
                }

                return;
            }

            if (CurPhase == IntroAmbushedPhases.LookRight)
            {
                if (ElapsedTime >= LookLeftRightTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = IntroAmbushedPhases.Question;

                    inGameState.Context.Player.FacingDir = Direction.Down;
                    SoundManager.Instance.PlaySound(QuestionSound, false, 1f);
                }
                return;
            }

            if (CurPhase == IntroAmbushedPhases.Question)
            {
                inGameState.camera.LookAtAndClamp(inGameState.Context.Player.transform.Position, inGameState.LevelMap.MapBounds);
                if (ElapsedTime >= QuestionTime)
                {
                    inGameState.Context.Player.ChangeState(new IdleState());
                    inGameState.ChangeLevelState(new PlayingState(inGameState));
                }
            }
        }

        protected override void DerivedRender()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();

            if (CurPhase == IntroAmbushedPhases.Exclamation || CurPhase == IntroAmbushedPhases.Question)
            {
                Vector2 bubblePos = Vector2.Zero;
                if (CurPhase == IntroAmbushedPhases.Exclamation)
                {
                    bubblePos = inGameState.Context.RockShooters[0].transform.Position + InitBubbleOffset;
                }
                else
                {
                    bubblePos = inGameState.Context.Player.transform.Position + InitBubbleOffset;
                }

                Rectangle rect = (CurPhase == IntroAmbushedPhases.Exclamation) ? BubbleRect : BubbleQuestionRect;

                float timeVal = (float)(ElapsedTime / IconAppearTime);

                float scale = UtilityGlobals.Clamp(timeVal, 0f, 1f) * ExclamationScale;
                RenderingManager.Instance.DrawSprite(UITex, bubblePos, rect, Color.White, 0f,
                    rect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BaseRockShooterRenderDepth + .05f);

                if (CurPhase == IntroAmbushedPhases.Exclamation)
                {
                    RenderingManager.Instance.DrawSprite(UITex, bubblePos, ExclamationRect, Color.White, 0f,
                            ExclamationRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BaseRockShooterRenderDepth + .06f);
                }
                else
                {
                    RenderingManager.Instance.DrawSprite(UITex, bubblePos, QuestionRect, Color.White, 0f,
                            QuestionRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BaseRockShooterRenderDepth + .06f);
                }
            }

            RenderingManager.Instance.EndCurrentBatch();

            if (CurPhase == IntroAmbushedPhases.FadeOut || CurPhase == IntroAmbushedPhases.FadeIn)
            {
                DrawFade();
            }
        }

        private void DrawFade()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                    BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

            RenderingManager.Instance.DrawSprite(UITex, Vector2.Zero, ContentGlobals.BoxRect, CurFadeColor, 0f, Vector2.Zero,
                RenderingGlobals.BaseResolution, SpriteEffects.None, .5f);

            RenderingManager.Instance.EndCurrentBatch();
        }

        //Use a static method because this state will be gone when the player regains control, and we don't want the portal holding onto it via the handler
        private static void OnPlayerEnterPortal()
        {
            IngameState ingamestate = (IngameState)GameStateManager.Instance.CurrentState;
            ingamestate.ChangeLevelState(new LevelFlyOutState(ingamestate, Color.Transparent, Color.Black, ExitLevelFadeTime, ExitLevelFlyTime, true));
            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);
        }

        protected override void SkipCutscene()
        {
            inGameState.Context.Player.StateChangedEvent -= OnPlayerWalkDown;
            inGameState.Context.Player.StateChangedEvent -= OnPlayerStunned;

            inGameState.Context.Player.transform.Position = DestPos;
            inGameState.Context.Player.FacingDir = Direction.Down;

            //Remove all moles then show the portal, block, and spot
            for (int i = inGameState.Context.RockShooters.Count - 1; i >= 0; i--)
            {
                inGameState.Context.RemoveObject(inGameState.Context.RockShooters[i]);
            }

            for (int i = 0; i < inGameState.Context.LevelText.Count; i++)
            {
                inGameState.Context.LevelText[i].ChangeColor(Color.White, Color.Transparent, TextChangeColorTime);
            }

            //Stop halting the portal's color from changing and call a manual update to make it update its color
            inGameState.Context.LevelPortal.HaltColorUpdate = false;
            inGameState.Context.LevelPortal.Update();

            for (int i = 0; i < inGameState.Context.Blocks.Count; i++)
            {
                Block b = inGameState.Context.Blocks[i];
                if (b.BlockPattern != BlockPatterns.None)
                {
                    b.TintColor = Color.White;
                }
            }

            inGameState.Context.Spots[0].TintColor = Color.White;

            inGameState.Context.Player.ChangeState(new IdleState());
            inGameState.ChangeLevelState(new PlayingState(inGameState));
        }

        private void DebugSkip()
        {
            if (KeyboardInput.GetKey(Keys.Tab, Debug.DebugKeyboard) == false) return;

            if (KeyboardInput.GetKeyDown(Keys.S, Debug.DebugKeyboard) == true)
            {
                CurPhase = IntroAmbushedPhases.RockThrow;
                inGameState.Context.Player.ChangeState(new PlayerStunnedState(Direction.Down, new BlankState()));
                OnPlayerStunned(inGameState.Context.Player.CurState);

                CurPhase = IntroAmbushedPhases.Question;
                ElapsedTime = QuestionTime;

                //Remove all moles then show the portal, block, and spot
                for (int i = inGameState.Context.RockShooters.Count - 1; i >= 0; i--)
                {
                    inGameState.Context.RemoveObject(inGameState.Context.RockShooters[i]);
                }

                //Stop halting the portal's color from changing and call a manual update to make it update its color
                inGameState.Context.LevelPortal.HaltColorUpdate = false;
                inGameState.Context.LevelPortal.Update();

                for (int i = 0; i < inGameState.Context.Blocks.Count; i++)
                {
                    Block b = inGameState.Context.Blocks[i];
                    if (b.BlockPattern != BlockPatterns.None)
                    {
                        b.TintColor = Color.White;
                    }
                }

                inGameState.Context.Spots[0].TintColor = Color.White;
            }
        }
    }
}