/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The title screen sequence playing in the background.
    /// This essentially acts like a cutscene but isn't one in the traditional way.
    /// </summary>
    public class TitleScreenCutsceneState : IGameState
    {
        /* This will be managed by several different states. The high level overview of what happens is this:
         * -The echidna places a block at a designated location then leaves
         * -If a block is placed, the echidna can then go from another side and push it through to the opposite side or pull it back
         * 
         * With that out of the way, the states will be like so:
         * 
         * - The main state handles moving the echidna a certain number of tiles
         *   If the echidna should have a block, it'll move the block as well and play the block movement animation 
         * - After each state, the echidna lets go of the block and plays the idle animation; this works regardless if it has a block or not
         * - Depending on whether the echidna left a block or not, the possibilities are narrowed:
         *   - If a block is left, only pushing/pulling through on the same side  as the block (left or right) are possible
         * - After each state, wait a certain amount of time before moving onto the next
         */

        /// <summary>
        /// The state representing the level.
        /// This is used to handle loading everything.
        /// </summary>
        private IngameState inGameState = null;

        private BlockPatterns[] BlockPatternSprites = null;

        private MoveSequenceState[][] LPlaceBlockSequences = null;
        private MoveSequenceState[][] LTakeBlockSequences = null;

        private MoveSequenceState[][] RPlaceBlockSequences = null;
        private MoveSequenceState[][] RTakeBlockSequences = null;

        private bool IsPlaceSequence = true;
        private bool IsLeftSequence = false;

        private MoveSequenceState[] CurMvSequence = null;
        private int CurSequenceIndex = 0;

        private const double WaitTime = 500d;
        private double ElapsedTime = 0d;

        public TitleScreenCutsceneState(IngameState ingameState)
        {
            inGameState = ingameState;
        }

        public void Enter()
        {
            BlockPatternSprites = EnumUtility.GetValues<BlockPatterns>.EnumValues;

            //Use a list to avoid choosing the same spot pattern twice
            //Don't allow empty Spot patterns, so start at 1
            List<int> randInts = new List<int>(BlockPatternSprites.Length - 1);
            for (int i = 1; i < BlockPatternSprites.Length; i++)
            {
                randInts.Add(i);
            }

            //Randomize the patterns of the spots
            for (int i = 0; i < inGameState.Context.Spots.Count; i++)
            {
                //Choose a random pattern out of the ones available in the list
                int index = RandomGlobals.Randomizer.Next(0, randInts.Count);

                inGameState.Context.Spots[i].ChangeSpotPattern(BlockPatternSprites[randInts[index]]);

                randInts.RemoveAt(index);
            }

            randInts = null;

            ChooseRandomBlockPattern();
            SetupSequences();

            //Decide whether to start on the left or right
            int randNum = RandomGlobals.Randomizer.Next(0, 2);
            IsLeftSequence = (randNum == 0);

            DecideNextSequence();
            
            CurSequenceIndex = 0;

            CurMvSequence[CurSequenceIndex].Start();
        }

        public void Exit()
        {
            
        }

        private MoveSequenceState CreateSeqHelper(in Vector2 startPos, in Direction moveDir, in Direction facingDir, in bool hasBlock, in int numTiles)
        {
            return new MoveSequenceState(inGameState.Context.LevelSpace.TileSize, startPos, moveDir, facingDir,
                inGameState.Context.Player, (hasBlock == true) ? inGameState.Context.Blocks[0] : null, numTiles);
        }

        private void SetupSequences()
        {
            TiledMapObjectLayer spawnPosLayer = inGameState.LevelMap.Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.SpawnPosLayerName);

            Vector2 LtopEdgePos = spawnPosLayer.Objects[0].Position + inGameState.Context.LevelSpace.TileSizeHalf;
            Vector2 LtopMiddlePos = spawnPosLayer.Objects[1].Position + inGameState.Context.LevelSpace.TileSizeHalf;
            Vector2 leftEdgePos = spawnPosLayer.Objects[2].Position + inGameState.Context.LevelSpace.TileSizeHalf;
            Vector2 leftMiddlePos = spawnPosLayer.Objects[3].Position + inGameState.Context.LevelSpace.TileSizeHalf;
            Vector2 LbotEdgePos = spawnPosLayer.Objects[4].Position + inGameState.Context.LevelSpace.TileSizeHalf;
            Vector2 LbotMiddlePos = spawnPosLayer.Objects[5].Position + inGameState.Context.LevelSpace.TileSizeHalf;

            Vector2 RtopEdgePos = spawnPosLayer.Objects[6].Position + inGameState.Context.LevelSpace.TileSizeHalf;
            Vector2 RtopMiddlePos = spawnPosLayer.Objects[7].Position + inGameState.Context.LevelSpace.TileSizeHalf;
            Vector2 rightEdgePos = spawnPosLayer.Objects[8].Position + inGameState.Context.LevelSpace.TileSizeHalf;
            Vector2 rightMiddlePos = spawnPosLayer.Objects[9].Position + inGameState.Context.LevelSpace.TileSizeHalf;
            Vector2 RbotEdgePos = spawnPosLayer.Objects[10].Position + inGameState.Context.LevelSpace.TileSizeHalf;
            Vector2 RbotMiddlePos = spawnPosLayer.Objects[11].Position + inGameState.Context.LevelSpace.TileSizeHalf;

            Block block = inGameState.Context.Blocks[0];

            //Left side

            //Left place sequences
            MoveSequenceState[] LBlockPushDownMoveUpSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(LtopEdgePos, Direction.Down, Direction.Down, true, 8),
                CreateSeqHelper(LtopMiddlePos, Direction.Up, Direction.Up, false, 8)
            };
            MoveSequenceState[] LBlockPushRightMoveLeftSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(leftEdgePos, Direction.Right, Direction.Right, true, 4),
                CreateSeqHelper(leftMiddlePos, Direction.Left, Direction.Left, false, 4)
            };
            MoveSequenceState[] LBlockPushUpMoveDownSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(LbotEdgePos, Direction.Up, Direction.Up, true, 8),
                CreateSeqHelper(LbotMiddlePos, Direction.Down, Direction.Down, false, 8)
            };

            //Left take sequences
            MoveSequenceState[] LBlockMoveRightPullLeftSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(leftEdgePos, Direction.Right, Direction.Right, false, 4),
                CreateSeqHelper(leftMiddlePos, Direction.Left, Direction.Right, true, 4)
            };
            MoveSequenceState[] LBlockMoveUpPushUpSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(LbotEdgePos, Direction.Up, Direction.Up, false, 8),
                CreateSeqHelper(LbotMiddlePos, Direction.Up, Direction.Up, true, 10)
            };
            MoveSequenceState[] LBlockMoveUpPullDownSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(LbotEdgePos, Direction.Up, Direction.Up, false, 8),
                CreateSeqHelper(LbotMiddlePos, Direction.Down, Direction.Up, true, 8)
            };
            MoveSequenceState[] LBlockMoveDownPushDownSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(LtopEdgePos, Direction.Down, Direction.Down, false, 8),
                CreateSeqHelper(LtopMiddlePos, Direction.Down, Direction.Down, true, 10)
            };
            MoveSequenceState[] LBlockMoveDownPullUpSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(LtopEdgePos, Direction.Down, Direction.Down, false, 8),
                CreateSeqHelper(LtopMiddlePos, Direction.Up, Direction.Down, true, 8)
            };

            //Set up the left arrays
            LPlaceBlockSequences = new MoveSequenceState[][] { LBlockPushDownMoveUpSequence, LBlockPushRightMoveLeftSequence, LBlockPushUpMoveDownSequence };
            LTakeBlockSequences = new MoveSequenceState[][] {  LBlockMoveRightPullLeftSequence, LBlockMoveUpPushUpSequence, LBlockMoveUpPullDownSequence, LBlockMoveDownPushDownSequence, LBlockMoveDownPullUpSequence };

            //Right side

            //Right place sequences
            MoveSequenceState[] RBlockPushDownMoveUpSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(RtopEdgePos, Direction.Down, Direction.Down, true, 8),
                CreateSeqHelper(RtopMiddlePos, Direction.Up, Direction.Up, false, 8)
            };
            MoveSequenceState[] RBlockPushLeftMoveRightSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(rightEdgePos, Direction.Left, Direction.Left, true, 4),
                CreateSeqHelper(rightMiddlePos, Direction.Right, Direction.Right, false, 4)
            };
            MoveSequenceState[] RBlockPushUpMoveDownSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(RbotEdgePos, Direction.Up, Direction.Up, true, 8),
                CreateSeqHelper(RbotMiddlePos, Direction.Down, Direction.Down, false, 8)
            };

            //Right take sequences
            MoveSequenceState[] RBlockMoveLeftPullRightSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(rightEdgePos, Direction.Left, Direction.Left, false, 4),
                CreateSeqHelper(rightMiddlePos, Direction.Right, Direction.Left, true, 4)
            };
            MoveSequenceState[] RBlockMoveUpPushUpSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(RbotEdgePos, Direction.Up, Direction.Up, false, 8),
                CreateSeqHelper(RbotMiddlePos, Direction.Up, Direction.Up, true, 10)
            };
            MoveSequenceState[] RBlockMoveUpPullDownSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(RbotEdgePos, Direction.Up, Direction.Up, false, 8),
                CreateSeqHelper(RbotMiddlePos, Direction.Down, Direction.Up, true, 8)
            };
            MoveSequenceState[] RBlockMoveDownPushDownSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(RtopEdgePos, Direction.Down, Direction.Down, false, 8),
                CreateSeqHelper(RtopMiddlePos, Direction.Down, Direction.Down, true, 10)
            };
            MoveSequenceState[] RBlockMoveDownPullUpSequence = new MoveSequenceState[]
            {
                CreateSeqHelper(RtopEdgePos, Direction.Down, Direction.Down, false, 8),
                CreateSeqHelper(RtopMiddlePos, Direction.Up, Direction.Down, true, 8)
            };

            //Set up the right arrays
            RPlaceBlockSequences = new MoveSequenceState[][] { RBlockPushDownMoveUpSequence, RBlockPushLeftMoveRightSequence, RBlockPushUpMoveDownSequence };
            RTakeBlockSequences = new MoveSequenceState[][] { RBlockMoveLeftPullRightSequence, RBlockMoveUpPushUpSequence, RBlockMoveUpPullDownSequence, RBlockMoveDownPushDownSequence, RBlockMoveDownPullUpSequence };
        }

        public void Update()
        {
            //Update all objects
            UpdateObjects();

            if (CurMvSequence[CurSequenceIndex].Complete == false)
            {
                return;
            }

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime < WaitTime)
            {
                return;
            }

            ElapsedTime = 0d;
            CurSequenceIndex++;

            if (CurSequenceIndex >= CurMvSequence.Length)
            {
                CurSequenceIndex = 0;
                IsPlaceSequence = !IsPlaceSequence;

                //Choose a new random block pattern for a place sequence
                if (IsPlaceSequence == true)
                {
                    ChooseRandomBlockPattern();
                    
                    //Flip the sides to keep things fresh
                    IsLeftSequence = !IsLeftSequence;
                }

                DecideNextSequence();
            }

            CurMvSequence[CurSequenceIndex].Start();
        }

        private void DecideNextSequence()
        {
            MoveSequenceState[][] moveSequenceOptions = null;

            if (IsPlaceSequence == true)
            {
                moveSequenceOptions = (IsLeftSequence == true) ? LPlaceBlockSequences : RPlaceBlockSequences;
            }
            else
            {
                moveSequenceOptions = (IsLeftSequence == true) ? LTakeBlockSequences : RTakeBlockSequences;
            }

            CurMvSequence = moveSequenceOptions[RandomGlobals.Randomizer.Next(0, moveSequenceOptions.Length)];
        }

        private void ChooseRandomBlockPattern()
        {
            Block block = inGameState.Context.Blocks[0];
            BlockPatterns blockPattern = BlockPatternSprites[RandomGlobals.Randomizer.Next(0, BlockPatternSprites.Length)];

            block.BlockPattern = blockPattern;
            block.PatternSprite.SourceRect = Block.GetPatternSourceRect(block.BlockPattern, block.BlockDirection);
        }

        private void UpdateObjects()
        {
            for (int i = 0; i < inGameState.Context.Spots.Count; i++)
            {
                inGameState.Context.Spots[i].QueryTrigger(inGameState.Context.LevelCollision);
            }

            inGameState.Context.LevelPortal.Update();

            for (int i = 0; i < inGameState.Context.Blocks.Count; i++)
            {
                inGameState.Context.Blocks[i].Update();
            }

            inGameState.Context.Player.Update();

            for (int i = 0; i < inGameState.Context.Warps.Count; i++)
            {
                inGameState.Context.Warps[i].Update();
            }

            for (int i = 0; i < inGameState.Context.Switches.Count; i++)
            {
                inGameState.Context.Switches[i].Update();
            }

            for (int i = 0; i < inGameState.Context.RockShooters.Count; i++)
            {
                inGameState.Context.RockShooters[i].Update();
            }
        }

        public void Render()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();

            RenderingManager.Instance.EndCurrentBatch();
        }

        /// <summary>
        /// Handles the basics of moving the player and a block a certain number of tiles.
        /// </summary>
        private class MoveSequenceState
        {
            private Vector2 StartPosition = Vector2.Zero;
            private Vector2 TileSize = Vector2.Zero;
            private Direction MoveDir = Direction.Up;
            private Direction FacingDir = Direction.Up;
            private Echidna Player = null;
            private Block BlockMoved = null;
            private int NumTiles = 1;

            private int CurTilesMoved = 0;

            public bool Complete => (CurTilesMoved >= NumTiles);

            public MoveSequenceState(in Vector2 tileSize, in Vector2 startPosition, in Direction moveDir, in Direction facingDir,
                Echidna player, Block blockMoved, in int numTiles)
            {
                TileSize = tileSize;
                StartPosition = startPosition;
                MoveDir = moveDir;
                FacingDir = facingDir;
                Player = player;
                BlockMoved = blockMoved;
                NumTiles = numTiles;
            }

            public void Start()
            {
                CurTilesMoved = 0;

                Player.StateChangedEvent -= OnPlayerStateChanged;
                Player.StateChangedEvent += OnPlayerStateChanged;

                //Set player starting position
                Player.transform.Position = StartPosition;
                Player.FacingDir = FacingDir;

                //Set block starting position, if available
                if (BlockMoved != null)
                {
                    Vector2 moveVec = DirectionUtil.GetVector2ForDir(FacingDir);
                    BlockMoved.transform.Position = StartPosition + (moveVec * TileSize);
                }

                HandleMove();
            }

            public void End()
            {
                CurTilesMoved = NumTiles;

                Player.StateChangedEvent -= OnPlayerStateChanged;
                Player.ChangeState(new BlankState(true));
                BlockMoved?.ChangeState(null);
            }

            private void HandleMove()
            {
                Vector2 moveVec = DirectionUtil.GetVector2ForDir(MoveDir);
                if (BlockMoved != null)
                {
                    Player.ChangeState(new PlayerBlockMoveState(BlockMoved, moveVec * TileSize, Player.MoveTime));
                    BlockMoved.ChangeState(new BlockMoveState(BlockMoved, moveVec * TileSize, Player.MoveTime, true));
                }
                else
                {
                    Player.ChangeState(new PlayerMoveState(moveVec * TileSize, Player.MoveTime));
                }
            }

            private void OnPlayerStateChanged(PlayerStateMachine newState)
            {
                if (Player.PlayerState == PlayerStates.Grab || Player.PlayerState == PlayerStates.Idle)
                {
                    CurTilesMoved++;

                    if (Complete == true)
                    {
                        End();
                        return;
                    }

                    //Continue moving
                    HandleMove();
                }
            }
        }
    }
}