/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// The cutscene after the fifth boss level.
    /// </summary>
    public class PostBoss5SceneState : BaseCutsceneState
    {
        //In this cutscene, the moles notice the echidna after it makes its way over to them
        //They then try to attack the echidna but ultimately end up hitting one another after the echidna moves out of the way
        //This opens up warps the echidna can take that lead to the portal

        private enum PostBoss5Phases
        {
            Exclaim, WaitOne, Move, WaitTwo, Question
        }

        private IngameState inGameState = null;

        private Texture2D UITex = null;
        private SoundEffect ExclamationSound = null;
        private SoundEffect QuestionSound = null;

        private readonly Rectangle ExclamationBubbleRect = ContentGlobals.CSBubbleExclamationRect;
        private readonly Rectangle ExclamationRect = ContentGlobals.CSExclamationRect;
        private readonly Rectangle QuestionBubbleRect = ContentGlobals.CSBubbleQuestionRect;
        private readonly Rectangle QuestionRect = ContentGlobals.CSQuestionRect;

        private readonly Vector2 BubbleOffset = ContentGlobals.CSBubbleOffset;

        private const double IconAppearTime = 200d;

        private const float ExpressionScale = 1f;

        private const double ExitLevelFadeTime = 1000d;
        private const double ExitLevelFlyTime = 1000d;

        private const double MoleShootTime = 700d;
        private const double ExclamationTime = 1500d;
        private const double QuestionTime = 1500d;
        private const double WaitOneTime = 600d;
        private const double WaitTwoTime = 1400d;

        private Vector2 PlayerEndMoveDest = Vector2.Zero;

        private PostBoss5Phases CurPhase = PostBoss5Phases.Exclaim;
        private double ElapsedTime = 0d;

        public PostBoss5SceneState(IngameState ingamestate)
        {
            inGameState = ingamestate;
        }

        protected override void DerivedEnter()
        {
            inGameState.Context.Player.FacingDir = Direction.Left;
            inGameState.Context.Player.ChangeState(new BlankState(true));

            PlayerEndMoveDest = inGameState.Context.Player.transform.Position + new Vector2(inGameState.Context.LevelSpace.TileSize.X, 0f);

            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            ExclamationSound = AssetManager.Instance.LoadSound(ContentGlobals.ExclamationSound);
            QuestionSound = AssetManager.Instance.LoadSound(ContentGlobals.QuestionSound);

            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent -= OnPlayerEnterPortal;
            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent += OnPlayerEnterPortal;

            SoundManager.Instance.PlaySound(ExclamationSound, false, 1f);
            
#if DEBUG
            Debug.AddCustomDebugCommand(DebugSkip);
#endif
        }

        public override void Exit()
        {
            UITex = null;
            ExclamationSound = null;
            QuestionSound = null;

            inGameState.Context.Player.StateChangedEvent -= OnPlayerStateChanged;

#if DEBUG
            Debug.RemoveCustomDebugCommand(DebugSkip);
#endif
        }

        //Use a static method because this state will be gone when the player regains control, and we don't want the portal holding onto it via the handler
        private static void OnPlayerEnterPortal()
        {
            IngameState ingamestate = (IngameState)GameStateManager.Instance.CurrentState;
            ingamestate.ChangeLevelState(new LevelFlyOutState(ingamestate, Color.Transparent, Color.Black, ExitLevelFadeTime, ExitLevelFlyTime, true));
            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);
        }

        private void OnPlayerStateChanged(PlayerStateMachine newState)
        {
            //Once back in idle, switch to the next phase
            if (inGameState.Context.Player.PlayerState == PlayerStates.Idle)
            {
                inGameState.Context.Player.FacingDir = Direction.Left;
                inGameState.Context.Player.ChangeState(new BlankState(true));

                inGameState.Context.Player.StateChangedEvent -= OnPlayerStateChanged;

                ElapsedTime = 0d;
                CurPhase = PostBoss5Phases.WaitTwo;
            }
        }

        protected override void DerivedUpdate()
        {
            //Have warps query triggers for the block that gets pushed
            for (int i = 0; i < inGameState.Context.Warps.Count; i++)
            {
                inGameState.Context.Warps[i].QueryTrigger(inGameState.Context.LevelCollision);
            }

            inGameState.Context.LevelPortal.Update();

            for (int i = 0; i < inGameState.Context.Blocks.Count; i++)
            {
                inGameState.Context.Blocks[i].Update();
            }

            inGameState.Context.Player.Update();

            for (int i = 0; i < inGameState.Context.Warps.Count; i++)
            {
                inGameState.Context.Warps[i].QueryTrigger(inGameState.Context.LevelCollision);
                inGameState.Context.Warps[i].Update();
            }

            for (int i = 0; i < inGameState.Context.RockShooters.Count; i++)
            {
                inGameState.Context.RockShooters[i].Update();
            }

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (CurPhase == PostBoss5Phases.Exclaim)
            {
                if (ElapsedTime >= ExclamationTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss5Phases.WaitOne;

                    //Have the moles throw rocks now, and reduce their shoot time so they don't wait as long
                    for (int i = 0; i < inGameState.Context.RockShooters.Count; i++)
                    {
                        inGameState.Context.RockShooters[i].ShootTime = MoleShootTime;
                        inGameState.Context.RockShooters[i].HaltThrow = false;
                    }
                }

                return;
            }

            if (CurPhase == PostBoss5Phases.WaitOne)
            {
                if (ElapsedTime >= WaitOneTime)
                {
                    ElapsedTime = 0;
                    CurPhase = PostBoss5Phases.Move;

                    inGameState.Context.Player.StateChangedEvent -= OnPlayerStateChanged;
                    inGameState.Context.Player.StateChangedEvent += OnPlayerStateChanged;

                    inGameState.Context.Player.FacingDir = Direction.Right;

                    inGameState.Context.Player.ChangeState(new PlayerMoveState(new Vector2(inGameState.Context.LevelSpace.TileSize.X, 0f), inGameState.Context.Player.MoveTime));
                }

                return;
            }

            if (CurPhase == PostBoss5Phases.Move)
            {
                //Wait on the player to finish moving
                return;
            }

            if (CurPhase == PostBoss5Phases.WaitTwo)
            {
                if (ElapsedTime >= WaitTwoTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss5Phases.Question;

                    SoundManager.Instance.PlaySound(QuestionSound, false, 1f);
                }

                return;
            }

            if (CurPhase == PostBoss5Phases.Question)
            {
                if (ElapsedTime >= QuestionTime)
                {
                    ElapsedTime = 0d;

                    inGameState.Context.Player.ChangeState(new IdleState());
                    inGameState.ChangeLevelState(new PlayingState(inGameState));
                }

                return;
            }
        }

        protected override void DerivedRender()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();

            if (CurPhase == PostBoss5Phases.Exclaim)
            {
                for (int i = 0; i < inGameState.Context.RockShooters.Count; i++)
                {
                    Vector2 bubblePos = inGameState.Context.RockShooters[i].transform.Position + BubbleOffset;

                    float timeVal = (float)(ElapsedTime / IconAppearTime);

                    float scale = UtilityGlobals.Clamp(timeVal, 0f, 1f) * ExpressionScale;

                    RenderingManager.Instance.DrawSprite(UITex, bubblePos, ExclamationBubbleRect, Color.White, 0f,
                        ExclamationBubbleRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .05f);

                    RenderingManager.Instance.DrawSprite(UITex, bubblePos, ExclamationRect, Color.White, 0f,
                        ExclamationRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .06f);
                }
            }
            else if (CurPhase == PostBoss5Phases.Question)
            {
                Vector2 bubblePos = inGameState.Context.Player.transform.Position + BubbleOffset;

                float timeVal = (float)(ElapsedTime / IconAppearTime);

                float scale = UtilityGlobals.Clamp(timeVal, 0f, 1f) * ExpressionScale;

                RenderingManager.Instance.DrawSprite(UITex, bubblePos, QuestionBubbleRect, Color.White, 0f,
                    QuestionBubbleRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .05f);

                RenderingManager.Instance.DrawSprite(UITex, bubblePos, QuestionRect, Color.White, 0f,
                    QuestionRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .06f);
            }

            RenderingManager.Instance.EndCurrentBatch();
        }

        protected override void SkipCutscene()
        {
            for (int i = inGameState.Context.RockShooters.Count - 1; i >= 0; i--)
            {
                inGameState.Context.RemoveObject(inGameState.Context.RockShooters[i]);
            }

            CurPhase = PostBoss5Phases.Exclaim;

            inGameState.Context.Player.StateChangedEvent -= OnPlayerStateChanged;

            if (inGameState.Context.LevelCamera != null)
            {
                inGameState.Context.LevelCamera.SetTranslation(inGameState.Context.LevelCamera.DefaultPosition);
            }

            inGameState.Context.Player.transform.Position = PlayerEndMoveDest;
            inGameState.Context.Player.ChangeState(new IdleState());
            inGameState.ChangeLevelState(new PlayingState(inGameState));
        }

        private void DebugSkip()
        {
            if (KeyboardInput.GetKey(Keys.Tab, Debug.DebugKeyboard) == false) return;

            if (KeyboardInput.GetKeyDown(Keys.S, Debug.DebugKeyboard) == true)
            {
                for (int i = inGameState.Context.RockShooters.Count - 1; i >= 0; i--)
                {
                    inGameState.Context.RemoveObject(inGameState.Context.RockShooters[i]);
                }

                CurPhase = PostBoss5Phases.WaitOne;
                ElapsedTime = WaitTwoTime;
            }
        }
    }
}