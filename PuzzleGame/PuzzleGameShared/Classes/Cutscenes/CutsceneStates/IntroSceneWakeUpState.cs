/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// The wake up sequence of the intro cutscene.
    /// </summary>
    public class IntroSceneWakeUpState : BaseCutsceneState
    {
        private enum IntroWakeUpPhases
        {
            FadeIn, Wait, WakeUp, LookAround, LookDownWait, WalkDown, Question
        }

        private IngameState inGameState = null;

        private Texture2D UITex = null;
        private SpriteFont SleepFont = null;
        private SoundEffect ExclamationSound = null;
        private SoundEffect QuestionSound = null;
        private readonly Rectangle BubbleRect = ContentGlobals.CSBubbleExclamationRect;
        private readonly Rectangle BubbleQuestionRect = ContentGlobals.CSBubbleQuestionRect;
        private readonly Rectangle ExclamationRect = ContentGlobals.CSExclamationRect;
        private readonly Rectangle QuestionRect = ContentGlobals.CSQuestionRect;

        private const double TextChangeColorTime = 500d;

        private readonly Vector2 InitSleepZOffset = new Vector2(6f, -8f);
        private readonly Vector2 SleepZOffset = new Vector2(5, -12);
        private const float SleepZScale = 1f;
        private const int SleepZCount = 3;
        private const double SleepAlphaTimeOffset = 200d;
        private const double SleepAlphaTime = 300d;
        private const double SleepPosTime = 91d;
        private const double SleepPosOffsetTime = 34d;
        private const float SleepMaxPosOffset = 2f;
        private Vector2 SleepTextOrigin = Vector2.Zero;
        private string SleepText = "Z";
        private double ElapsedSleepTime = 0d;

        private const double FadeInTime = 1500d;
        private readonly Color FadeInColor = Color.Transparent;
        private readonly Color FadeOutColor = Color.Black;
        private Color CurFadeColor = Color.Black;

        private const double WaitTime = 1000d;
        private const double LookDownWaitTime = 500d;

        private const double IconAppearTime = 200d;

        private Vector2 OrigPlayerPos = Vector2.Zero;
        private const double HopTime = 400d;
        private const double ExclamationTime = 1500d;
        private const float ExclamationScale = 1f;

        private int PrevTurn = 0;
        private const double TotalTurnTime = 1600d;

        private Vector2 DestPos = Vector2.Zero;
        private const double QuestionTime = 1500d;

        private IntroWakeUpPhases CurPhase = IntroWakeUpPhases.FadeIn;
        private double ElapsedTime = 0d;

        public IntroSceneWakeUpState(IngameState ingamestate)
        {
            inGameState = ingamestate;
        }

        protected override void DerivedEnter()
        {
            SoundManager.Instance.StopAndClearMusicTrack();

            inGameState.Context.Player.FacingDir = Direction.Down;
            inGameState.Context.Player.ChangeState(new BlankState(true));
            inGameState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Sleep);

            inGameState.Context.Player.StateChangedEvent -= OnPlayerStateChange;
            inGameState.Context.Player.StateChangedEvent += OnPlayerStateChange;

            DestPos = inGameState.Context.Player.transform.Position + new Vector2(0f, inGameState.Context.LevelSpace.TileSize.Y * 6f);

            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            SleepFont = AssetManager.Instance.LoadFont(ContentGlobals.SpecialFont13px);
            ExclamationSound = AssetManager.Instance.LoadSound(ContentGlobals.ExclamationSound);
            QuestionSound = AssetManager.Instance.LoadSound(ContentGlobals.QuestionSound);

            SleepTextOrigin = SleepFont.GetOrigin(SleepText, 0, 1);

            inGameState.Context.LevelPortal.HaltColorUpdate = true;
            inGameState.Context.LevelPortal.TintColor = Color.Transparent;

            for (int i = 0; i < inGameState.Context.Blocks.Count; i++)
            {
                Block b = inGameState.Context.Blocks[i];
                if (b.BlockPattern != BlockPatterns.None)
                {
                    b.TintColor = Color.Transparent;
                }
            }

            inGameState.Context.Spots[0].TintColor = Color.Transparent;

            inGameState.Context.RockShooters[0].FacingDir = Direction.Left;
            for (int i = 0; i < inGameState.Context.RockShooters.Count; i++)
            {
                if (i > 0)
                {
                    inGameState.Context.RockShooters[i].TintColor = Color.Transparent;
                }
                inGameState.Context.RockShooters[i].HaltThrow = true;

                //Do a single update to update any direction changes
                inGameState.Context.RockShooters[i].Update();
            }

            for (int i = 0; i < inGameState.Context.LevelText.Count; i++)
            {
                inGameState.Context.LevelText[i].ChangeColor(Color.Transparent, Color.White, TextChangeColorTime);
            }

            //IntroControlTextObj introControlTextObj = new IntroControlTextObj();
            //inGameState.Context.AddObject(introControlTextObj);
            //
            //introControlTextObj.Initialize(inGameState.Context, new ObjectInitInfo());
            //introControlTextObj.PostInitialize(inGameState.Context);

#if DEBUG
            Debug.AddCustomDebugCommand(DebugSkip);
#endif
        }

        public override void Exit()
        {
            UITex = null;
            SleepFont = null;
            ExclamationSound = null;
            QuestionSound = null;

            inGameState.Context.Player.StateChangedEvent -= OnPlayerStateChange;

#if DEBUG
            Debug.RemoveCustomDebugCommand(DebugSkip);
#endif
        }

        private void OnPlayerStateChange(PlayerStateMachine newState)
        {
            if (CurPhase == IntroWakeUpPhases.WalkDown && inGameState.Context.Player.PlayerState == PlayerStates.Idle)
            {
                if (UtilityGlobals.IsApproximate(inGameState.Context.Player.transform.Position, DestPos, .01f) == false)
                {
                    inGameState.Context.Player.ChangeState(new PlayerMoveState(new Vector2(0f, inGameState.Context.LevelSpace.TileSize.Y),
                        inGameState.Context.Player.MoveTime));
                }
                else
                {
                    inGameState.Context.Player.StateChangedEvent -= OnPlayerStateChange;
                    inGameState.Context.Player.ChangeState(new BlankState(true));
                    inGameState.Context.Player.FacingDir = Direction.Right;
                    ElapsedTime = 0d;
                    CurPhase = IntroWakeUpPhases.Question;
                    SoundManager.Instance.PlaySound(QuestionSound, false, 1f);
                }
            }
        }

        protected override void DerivedUpdate()
        {
            inGameState.Context.Player.Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (CurPhase == IntroWakeUpPhases.FadeIn)
            {
                ElapsedSleepTime += Time.ElapsedTime.TotalMilliseconds;
                if (ElapsedTime >= FadeInTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = IntroWakeUpPhases.Wait;
                    CurFadeColor = FadeInColor;
                }
                else
                {
                    CurFadeColor = Interpolation.Interpolate(FadeOutColor, FadeInColor, ElapsedTime / FadeInTime, Interpolation.InterpolationTypes.Linear);
                }
                return;
            }

            if (CurPhase == IntroWakeUpPhases.Wait && ElapsedSleepTime >= WaitTime)
            {
                ElapsedSleepTime += Time.ElapsedTime.TotalMilliseconds;
                if (ElapsedTime >= WaitTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = IntroWakeUpPhases.WakeUp;
                    
                    //Disable camera following so it doesn't move when the player hops
                    inGameState.LevelMap.CameraFollow = false;
                    OrigPlayerPos = inGameState.Context.Player.transform.Position;
                    inGameState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.Idle);
                    SoundManager.Instance.PlaySound(ExclamationSound, false, 1f);
                }
                return;
            }

            if (CurPhase == IntroWakeUpPhases.WakeUp)
            {
                //Show a hop
                if (ElapsedTime < HopTime)
                {
                    HandlePlayerHop();
                }
                else
                {
                    inGameState.Context.Player.transform.Position = OrigPlayerPos;
                }

                if (ElapsedTime >= ExclamationTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = IntroWakeUpPhases.LookAround;
                    inGameState.LevelMap.CameraFollow = true;
                    PrevTurn = 0;
                }
                return;
            }

            if (CurPhase == IntroWakeUpPhases.LookAround)
            {
                if (ElapsedTime >= TotalTurnTime)
                {
                    ElapsedTime = 0;
                    inGameState.Context.Player.FacingDir = Direction.Down;
                    CurPhase = IntroWakeUpPhases.LookDownWait;
                }
                else
                {
                    double eachTurn = TotalTurnTime / 4;
                    int val = (int)(ElapsedTime / eachTurn);
                    if (val != PrevTurn)
                    {
                        PrevTurn = val;
                        inGameState.Context.Player.FacingDir = DirectionUtil.GetClockwiseDirection(inGameState.Context.Player.FacingDir);
                    }
                }
                return;
            }

            if (CurPhase == IntroWakeUpPhases.LookDownWait)
            {
                if (ElapsedTime >= LookDownWaitTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = IntroWakeUpPhases.WalkDown;
                    inGameState.Context.Player.ChangeState(new PlayerMoveState(new Vector2(0f, inGameState.Context.LevelSpace.TileSize.Y),
                        inGameState.Context.Player.MoveTime));
                }
                return;
            }

            if (CurPhase == IntroWakeUpPhases.WalkDown)
            {
                inGameState.camera.LookAtAndClamp(inGameState.Context.Player.transform.Position, inGameState.LevelMap.MapBounds);
                return;
            }

            if (CurPhase == IntroWakeUpPhases.Question)
            {
                inGameState.camera.LookAtAndClamp(inGameState.Context.Player.transform.Position, inGameState.LevelMap.MapBounds);
                if (ElapsedTime >= QuestionTime)
                {
                    inGameState.Context.Player.ChangeState(new IdleState());
                    inGameState.ChangeLevelState(new PlayingState(inGameState));
                }
            }
        }

        private void HandlePlayerHop()
        {
            double time = UtilityGlobals.PingPong(ElapsedTime / (HopTime / 2), 0d, 1d);

            Vector2 vec = Interpolation.Interpolate(OrigPlayerPos, OrigPlayerPos + new Vector2(0f, -10f), time, Interpolation.InterpolationTypes.SineInOut);

            inGameState.Context.Player.transform.Position = vec;
        }

        protected override void DerivedRender()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();

            if (CurPhase == IntroWakeUpPhases.FadeIn || CurPhase == IntroWakeUpPhases.Wait)
            {
                //Show the "Z"s of the Echidna sleeping
                Vector2 sleepZPos = inGameState.Context.Player.transform.Position + InitSleepZOffset;

                for (int i = 0; i < SleepZCount; i++)
                {
                    Color col = Color.White * (float)Math.Sin((-ElapsedSleepTime + (i * SleepAlphaTimeOffset)) / SleepAlphaTime);
                    Vector2 newPos = sleepZPos + (i * SleepZOffset);
                    int xOffset = (int)(Math.Sin((ElapsedSleepTime + (i * SleepPosOffsetTime)) / SleepPosTime) * SleepMaxPosOffset);
                    newPos.X += xOffset;

                    RenderingManager.Instance.spriteBatch.DrawString(SleepFont, SleepText, newPos, col,
                        0f, SleepTextOrigin, SleepZScale, SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .05f);
                }
            }

            if (CurPhase == IntroWakeUpPhases.WakeUp || CurPhase == IntroWakeUpPhases.Question)
            {
                Vector2 bubblePos = inGameState.Context.Player.transform.Position + ContentGlobals.CSBubbleOffset;

                float timeVal = (float)(ElapsedTime / IconAppearTime);

                Rectangle rect = (CurPhase == IntroWakeUpPhases.WakeUp) ? BubbleRect : BubbleQuestionRect;

                float scale = UtilityGlobals.Clamp(timeVal, 0f, 1f) * ExclamationScale;
                //RenderingManager.Instance.spriteBatch.DrawString(SleepFont, "!", bubblePos, Color.Red, 0f, SleepFont.GetOrigin("!", .5f, 1f),
                //    scale, SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .05f);
                RenderingManager.Instance.DrawSprite(UITex, bubblePos, rect, Color.White, 0f,
                    rect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .05f);

                if (CurPhase == IntroWakeUpPhases.WakeUp)
                {
                    RenderingManager.Instance.DrawSprite(UITex, bubblePos, ExclamationRect, Color.White, 0f,
                        ExclamationRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .06f);
                }
                else
                {
                    RenderingManager.Instance.DrawSprite(UITex, bubblePos, QuestionRect, Color.White, 0f,
                        QuestionRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .06f);
                }
            }

            RenderingManager.Instance.EndCurrentBatch();

            if (CurPhase == IntroWakeUpPhases.FadeIn)
            {
                RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                    BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

                RenderingManager.Instance.DrawSprite(UITex, Vector2.Zero, ContentGlobals.BoxRect, CurFadeColor, 0f, Vector2.Zero,
                    RenderingGlobals.BaseResolution, SpriteEffects.None, .5f);

                RenderingManager.Instance.EndCurrentBatch();
            }
        }

        protected override void SkipCutscene()
        {
            inGameState.Context.Player.StateChangedEvent -= OnPlayerStateChange;
            inGameState.Context.Player.transform.Position = DestPos;
            inGameState.Context.Player.FacingDir = Direction.Right;

            inGameState.LevelMap.CameraFollow = true;

            inGameState.Context.Player.ChangeState(new IdleState());
            inGameState.ChangeLevelState(new PlayingState(inGameState));
        }

        private void DebugSkip()
        {
            if (KeyboardInput.GetKey(Keys.Tab, Debug.DebugKeyboard) == false) return;

            if (KeyboardInput.GetKeyDown(Keys.S, Debug.DebugKeyboard) == true)
            {
                CurPhase = IntroWakeUpPhases.Question;
                ElapsedTime = QuestionTime;

                //if (CurPhase == IntroWakeUpPhases.FadeIn)
                //{
                //    ElapsedTime = FadeInTime;
                //}
                //else if (CurPhase == IntroWakeUpPhases.Wait)
                //{
                //    ElapsedSleepTime = WaitTime;
                //    ElapsedTime = WaitTime;
                //}
                //else if (CurPhase == IntroWakeUpPhases.WakeUp)
                //{
                //    ElapsedTime = ExclamationTime;
                //}
                //else if (CurPhase == IntroWakeUpPhases.LookAround)
                //{
                //    ElapsedTime = TotalTurnTime;
                //}
                //else if (CurPhase == IntroWakeUpPhases.LookDownWait)
                //{
                //    ElapsedTime = LookDownWaitTime;
                //}
                //else if (CurPhase == IntroWakeUpPhases.WalkDown)
                //{
                //
                //}
            }
        }
    }
}