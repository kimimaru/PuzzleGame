/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// The cutscene after the sixth boss level.
    /// </summary>
    public class PostBoss6SceneState : BaseCutsceneState
    {
        //In this cutscene, the mole closest to the echidna hears something and questions it
        //The second mole next to that one turns towards it and questions it as well
        //That second mole throws a rock for safety, then they turn back towards the portal and vanish (fade in + out)

        private enum PostBoss6Phases
        {
            QuestionOne, WaitOne, QuestionTwo, ThrowRock, WaitTwo, WaitThree, FadeOut, FadeIn, WaitFour
        }

        private IngameState inGameState = null;

        private Texture2D UITex = null;
        private SoundEffect QuestionSound = null;

        private readonly Rectangle QuestionBubbleRect = ContentGlobals.CSBubbleQuestionRect;
        private readonly Rectangle QuestionRect = ContentGlobals.CSQuestionRect;

        private readonly Vector2 BubbleOffset = ContentGlobals.CSBubbleOffset;

        private readonly Color StartColor = Color.Transparent;
        private readonly Color EndColor = Color.Black;
        private Color CurColor = Color.Transparent;

        private const double FadeOutTime = 500d;
        private const double FadeInTime = 500d;

        private const double IconAppearTime = 200d;

        private const float ExpressionScale = 1f;

        private const double ExitLevelFadeTime = 1000d;
        private const double ExitLevelFlyTime = 1000d;

        private const double MoleShootTime = 500d;
        private const double ThrowRockWaitTime = 1000d;
        private const double QuestionTime = 1500d;
        private const double WaitOneTime = 500d;
        private const double WaitTwoTime = 1000d;
        private const double WaitThreeTime = 500d;
        private const double WaitFourTime = 400d;

        private PostBoss6Phases CurPhase = PostBoss6Phases.QuestionOne;
        private double ElapsedTime = 0d;

        public PostBoss6SceneState(IngameState ingamestate)
        {
            inGameState = ingamestate;
        }

        protected override void DerivedEnter()
        {
            inGameState.Context.Player.FacingDir = Direction.Left;
            inGameState.Context.Player.ChangeState(new BlankState(true));

            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            QuestionSound = AssetManager.Instance.LoadSound(ContentGlobals.QuestionSound);

            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent -= OnPlayerEnterPortal;
            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent += OnPlayerEnterPortal;

            inGameState.Context.Player.FacingDir = Direction.Up;

            inGameState.Context.RockShooters[0].FacingDir = Direction.Down;

            SoundManager.Instance.PlaySound(QuestionSound, false, 1f);
            
#if DEBUG
            Debug.AddCustomDebugCommand(DebugSkip);
#endif
        }

        public override void Exit()
        {
            UITex = null;
            QuestionSound = null;

#if DEBUG
            Debug.RemoveCustomDebugCommand(DebugSkip);
#endif
        }

        //Use a static method because this state will be gone when the player regains control, and we don't want the portal holding onto it via the handler
        private static void OnPlayerEnterPortal()
        {
            IngameState ingamestate = (IngameState)GameStateManager.Instance.CurrentState;
            ingamestate.ChangeLevelState(new LevelFlyOutState(ingamestate, Color.Transparent, Color.Black, ExitLevelFadeTime, ExitLevelFlyTime, true));
            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);
        }

        protected override void DerivedUpdate()
        {
            //Have warps query triggers for the block that gets warped
            for (int i = 0; i < inGameState.Context.Warps.Count; i++)
            {
                inGameState.Context.Warps[i].QueryTrigger(inGameState.Context.LevelCollision);
            }

            inGameState.Context.LevelPortal.Update();

            for (int i = 0; i < inGameState.Context.Blocks.Count; i++)
            {
                inGameState.Context.Blocks[i].Update();
            }

            inGameState.Context.Player.Update();

            for (int i = 0; i < inGameState.Context.Warps.Count; i++)
            {
                inGameState.Context.Warps[i].QueryTrigger(inGameState.Context.LevelCollision);
                inGameState.Context.Warps[i].Update();
            }

            for (int i = 0; i < inGameState.Context.RockShooters.Count; i++)
            {
                inGameState.Context.RockShooters[i].Update();
            }

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (CurPhase == PostBoss6Phases.QuestionOne)
            {
                if (ElapsedTime >= QuestionTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss6Phases.WaitOne;
                }

                return;
            }

            if (CurPhase == PostBoss6Phases.WaitOne)
            {
                if (ElapsedTime >= WaitOneTime)
                {
                    ElapsedTime = 0;
                    CurPhase = PostBoss6Phases.QuestionTwo;

                    //Make the mole face left and set its throw direction so the rock moves in that direction when thrown
                    inGameState.Context.RockShooters[1].FacingDir = Direction.Left;
                    inGameState.Context.RockShooters[1].SetThrowDir(Direction.Left);

                    SoundManager.Instance.PlaySound(QuestionSound, false, 1f);
                }

                return;
            }

            if (CurPhase == PostBoss6Phases.QuestionTwo)
            {
                if (ElapsedTime >= QuestionTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss6Phases.ThrowRock;

                    inGameState.Context.RockShooters[1].ShootTime = MoleShootTime;
                    inGameState.Context.RockShooters[1].HaltThrow = false;

                    inGameState.Context.RockShooters[2].FacingDir = Direction.Left;
                }

                return;
            }

            if (CurPhase == PostBoss6Phases.ThrowRock)
            {
                if (ElapsedTime >= ThrowRockWaitTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss6Phases.WaitTwo;

                    inGameState.Context.RockShooters[1].HaltThrow = true;
                    inGameState.Context.RockShooters[1].FacingDir = Direction.Right;
                    inGameState.Context.RockShooters[2].FacingDir = Direction.Right;
                }

                return;
            }

            if (CurPhase == PostBoss6Phases.WaitTwo)
            {
                if (ElapsedTime >= WaitTwoTime)
                {
                    ElapsedTime = 0;
                    CurPhase = PostBoss6Phases.WaitThree;

                    inGameState.Context.RockShooters[0].FacingDir = Direction.Right;
                }

                return;
            }

            if (CurPhase == PostBoss6Phases.WaitThree)
            {
                if (ElapsedTime >= WaitThreeTime)
                {
                    ElapsedTime = 0;
                    CurPhase = PostBoss6Phases.FadeOut;

                    CurColor = StartColor;
                }

                return;
            }

            if (CurPhase == PostBoss6Phases.FadeOut)
            {
                if (ElapsedTime >= FadeOutTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss6Phases.FadeIn;

                    //Make all moles disappear by removing them after the fade
                    for (int i = inGameState.Context.RockShooters.Count - 1; i >= 0; i--)
                    {
                        inGameState.Context.RemoveObject(inGameState.Context.RockShooters[i]);
                    }

                    CurColor = EndColor;
                }
                else
                {
                    CurColor = Interpolation.Interpolate(StartColor, EndColor, ElapsedTime / FadeOutTime, Interpolation.InterpolationTypes.Linear);
                }

                return;
            }

            if (CurPhase == PostBoss6Phases.FadeIn)
            {
                if (ElapsedTime >= FadeInTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss6Phases.WaitFour;

                    CurColor = StartColor;
                }
                else
                {
                    CurColor = Interpolation.Interpolate(EndColor, StartColor, ElapsedTime / FadeOutTime, Interpolation.InterpolationTypes.Linear);
                }

                return;
            }

            if (CurPhase == PostBoss6Phases.WaitFour)
            {
                if (ElapsedTime >= WaitFourTime)
                {
                    ElapsedTime = 0d;

                    inGameState.Context.Player.ChangeState(new IdleState());
                    inGameState.ChangeLevelState(new PlayingState(inGameState));
                }

                return;
            }
        }

        protected override void DerivedRender()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();

            if (CurPhase == PostBoss6Phases.QuestionOne || CurPhase == PostBoss6Phases.QuestionTwo)
            {
                Vector2 bubblePos = BubbleOffset;
                if (CurPhase == PostBoss6Phases.QuestionOne)
                    bubblePos += inGameState.Context.RockShooters[0].transform.Position;
                else if (CurPhase == PostBoss6Phases.QuestionTwo)
                    bubblePos += inGameState.Context.RockShooters[1].transform.Position;

                float timeVal = (float)(ElapsedTime / IconAppearTime);

                float scale = UtilityGlobals.Clamp(timeVal, 0f, 1f) * ExpressionScale;

                RenderingManager.Instance.DrawSprite(UITex, bubblePos, QuestionBubbleRect, Color.White, 0f,
                    QuestionBubbleRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .05f);

                RenderingManager.Instance.DrawSprite(UITex, bubblePos, QuestionRect, Color.White, 0f,
                    QuestionRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .06f);
            }

            RenderingManager.Instance.EndCurrentBatch();

            if ((CurPhase == PostBoss6Phases.FadeIn || CurPhase == PostBoss6Phases.FadeOut) && CurColor.A > 0)
            {
                RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                    BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

                RenderingManager.Instance.DrawSprite(UITex, Vector2.Zero, ContentGlobals.BoxRect, CurColor, 0f, Vector2.Zero,
                    RenderingGlobals.BaseResolution, SpriteEffects.None, 1f);

                RenderingManager.Instance.EndCurrentBatch();
            }
        }

        protected override void SkipCutscene()
        {
            for (int i = inGameState.Context.RockShooters.Count - 1; i >= 0; i--)
            {
                inGameState.Context.RemoveObject(inGameState.Context.RockShooters[i]);
            }

            CurPhase = PostBoss6Phases.WaitFour;

            inGameState.Context.Player.ChangeState(new IdleState());
            inGameState.ChangeLevelState(new PlayingState(inGameState));
        }

        private void DebugSkip()
        {
            if (KeyboardInput.GetKey(Keys.Tab, Debug.DebugKeyboard) == false) return;

            if (KeyboardInput.GetKeyDown(Keys.S, Debug.DebugKeyboard) == true)
            {
                for (int i = inGameState.Context.RockShooters.Count - 1; i >= 0; i--)
                {
                    inGameState.Context.RemoveObject(inGameState.Context.RockShooters[i]);
                }

                CurPhase = PostBoss6Phases.WaitFour;
                ElapsedTime = WaitThreeTime;
            }
        }
    }
}