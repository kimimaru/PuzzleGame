/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// The cutscene after the third boss level.
    /// </summary>
    public class PostBoss3SceneState : BaseCutsceneState
    {
        private enum PostBoss3Phases
        {
            Exclaim, FadeOut, FadeIn, Wait, LookLeft, LookRight, Question
        }

        private IngameState inGameState = null;

        private Texture2D UITex = null;
        private SoundEffect ExclamationSound = null;
        private SoundEffect QuestionSound = null;
        private readonly Rectangle BubbleRect = ContentGlobals.CSBubbleExclamationRect;
        private readonly Rectangle ExclamationRect = ContentGlobals.CSExclamationRect;

        private readonly Rectangle QuestionBubbleRect = ContentGlobals.CSBubbleQuestionRect;
        private readonly Rectangle QuestionRect = ContentGlobals.CSQuestionRect;

        private readonly Vector2 BubbleOffset = ContentGlobals.CSBubbleOffset;

        private readonly Color StartColor = Color.Transparent;
        private readonly Color EndColor = Color.Black;
        private Color CurColor = Color.Transparent;

        private const double IconAppearTime = 200d;

        private const double ExclamationTime = 1500d;
        private const float ExclamationScale = 1f;
        private const float QuestionScale = 1f;

        private const double ExitLevelFadeTime = 1000d;
        private const double ExitLevelFlyTime = 1000d;

        private const double QuestionTime = 1500d;
        private const double FadeOutTime = 500d;
        private const double FadeInTime = 500d;
        private const double LookTime = 500d;
        private const double WaitTime = 500d;

        private PostBoss3Phases CurPhase = PostBoss3Phases.Exclaim;
        private double ElapsedTime = 0d;

        public PostBoss3SceneState(IngameState ingamestate)
        {
            inGameState = ingamestate;
        }

        protected override void DerivedEnter()
        {
            inGameState.Context.Player.FacingDir = Direction.Right;
            inGameState.Context.Player.ChangeState(new BlankState(true));

            UITex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            ExclamationSound = AssetManager.Instance.LoadSound(ContentGlobals.ExclamationSound);
            QuestionSound = AssetManager.Instance.LoadSound(ContentGlobals.QuestionSound);

            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent -= OnPlayerEnterPortal;
            inGameState.Context.LevelPortal.PlayerEnteredPortalEvent += OnPlayerEnterPortal;

            CurColor = StartColor;

            inGameState.Context.RockShooters[0].FacingDir = Direction.Left;
            inGameState.Context.RockShooters[1].FacingDir = Direction.Up;

            for (int i = 0; i < inGameState.Context.RockShooters.Count; i++)
                inGameState.Context.RockShooters[i].Update();

            SoundManager.Instance.PlaySound(ExclamationSound, false, 1f);

#if DEBUG
            Debug.AddCustomDebugCommand(DebugSkip);
#endif
        }

        public override void Exit()
        {
            UITex = null;
            ExclamationSound = null;
            QuestionSound = null;
#if DEBUG
            Debug.RemoveCustomDebugCommand(DebugSkip);
#endif
        }

        //Use a static method because this state will be gone when the player regains control, and we don't want the portal holding onto it via the handler
        private static void OnPlayerEnterPortal()
        {
            IngameState ingamestate = (IngameState)GameStateManager.Instance.CurrentState;
            ingamestate.ChangeLevelState(new LevelFlyOutState(ingamestate, Color.Transparent, Color.Black, ExitLevelFadeTime, ExitLevelFlyTime, true));
            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);
        }

        protected override void DerivedUpdate()
        {
            inGameState.Context.LevelPortal.Update();

            inGameState.Context.Player.Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (CurPhase == PostBoss3Phases.Exclaim)
            {
                if (ElapsedTime >= ExclamationTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss3Phases.FadeOut;
                }
                return;
            }

            if (CurPhase == PostBoss3Phases.FadeOut)
            {
                if (ElapsedTime >= FadeOutTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = PostBoss3Phases.FadeIn;

                    CurColor = EndColor;

                    //Remove all moles once the screen is black
                    for (int i = inGameState.Context.RockShooters.Count - 1; i >= 0; i--)
                    {
                        inGameState.Context.RemoveObject(inGameState.Context.RockShooters[i]);
                    }
                }
                else
                {
                    CurColor = Interpolation.Interpolate(StartColor, EndColor, ElapsedTime / FadeOutTime, Interpolation.InterpolationTypes.Linear);
                }

                return;
            }

            if (CurPhase == PostBoss3Phases.FadeIn)
            {
                if (ElapsedTime >= FadeInTime)
                {
                    ElapsedTime = 0d;
                    CurColor = StartColor;

                    CurPhase = PostBoss3Phases.Wait;
                }
                else
                {
                    CurColor = Interpolation.Interpolate(EndColor, StartColor, ElapsedTime / FadeInTime, Interpolation.InterpolationTypes.Linear);
                }

                return;
            }

            if (CurPhase == PostBoss3Phases.Wait)
            {
                if (ElapsedTime >= WaitTime)
                {
                    ElapsedTime = 0d;

                    CurPhase = PostBoss3Phases.LookLeft;
                    inGameState.Context.Player.FacingDir = Direction.Left;
                }

                return;
            }

            if (CurPhase == PostBoss3Phases.LookLeft)
            {
                if (ElapsedTime >= LookTime)
                {
                    ElapsedTime = 0d;

                    CurPhase = PostBoss3Phases.LookRight;
                    inGameState.Context.Player.FacingDir = Direction.Right;
                }

                return;
            }

            if (CurPhase == PostBoss3Phases.LookRight)
            {
                if (ElapsedTime >= LookTime)
                {
                    ElapsedTime = 0d;

                    CurPhase = PostBoss3Phases.Question;

                    SoundManager.Instance.PlaySound(QuestionSound, false, 1f);
                }

                return;
            }

            if (CurPhase == PostBoss3Phases.Question)
            {
                if (ElapsedTime >= QuestionTime)
                {
                    ElapsedTime = 0d;

                    inGameState.Context.Player.ChangeState(new IdleState());
                    inGameState.ChangeLevelState(new PlayingState(inGameState));
                }
            }
        }

        protected override void DerivedRender()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();

            if (CurPhase == PostBoss3Phases.Exclaim)
            {
                for (int i = 0; i < inGameState.Context.RockShooters.Count; i++)
                {
                    Vector2 bubblePos = inGameState.Context.RockShooters[i].transform.Position + BubbleOffset;

                    float timeVal = (float)(ElapsedTime / IconAppearTime);

                    float scale = UtilityGlobals.Clamp(timeVal, 0f, 1f) * ExclamationScale;

                    RenderingManager.Instance.DrawSprite(UITex, bubblePos, BubbleRect, Color.White, 0f,
                        BubbleRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .05f);

                    RenderingManager.Instance.DrawSprite(UITex, bubblePos, ExclamationRect, Color.White, 0f,
                        ExclamationRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .06f);
                }
            }
            else if (CurPhase == PostBoss3Phases.Question)
            {
                Vector2 bubblePos = inGameState.Context.Player.transform.Position + BubbleOffset;

                float timeVal = (float)(ElapsedTime / IconAppearTime);

                float scale = UtilityGlobals.Clamp(timeVal, 0f, 1f) * QuestionScale;

                RenderingManager.Instance.DrawSprite(UITex, bubblePos, QuestionBubbleRect, Color.White, 0f,
                    QuestionBubbleRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .05f);

                RenderingManager.Instance.DrawSprite(UITex, bubblePos, QuestionRect, Color.White, 0f,
                    QuestionRect.GetCenterOrigin(), new Vector2(scale, scale), SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .06f);
            }

            RenderingManager.Instance.EndCurrentBatch();

            if ((CurPhase == PostBoss3Phases.FadeIn || CurPhase == PostBoss3Phases.FadeOut) && CurColor.A > 0)
            {
                RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                    BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

                RenderingManager.Instance.DrawSprite(UITex, Vector2.Zero, ContentGlobals.BoxRect, CurColor, 0f, Vector2.Zero,
                    RenderingGlobals.BaseResolution, SpriteEffects.None, 1f);

                RenderingManager.Instance.EndCurrentBatch();
            }
        }

        protected override void SkipCutscene()
        {
            for (int i = inGameState.Context.RockShooters.Count - 1; i >= 0; i--)
            {
                inGameState.Context.RemoveObject(inGameState.Context.RockShooters[i]);
            }

            CurPhase = PostBoss3Phases.Question;

            inGameState.Context.Player.ChangeState(new IdleState());
            inGameState.ChangeLevelState(new PlayingState(inGameState));
        }

        private void DebugSkip()
        {
            if (KeyboardInput.GetKey(Keys.Tab, Debug.DebugKeyboard) == false) return;

            if (KeyboardInput.GetKeyDown(Keys.S, Debug.DebugKeyboard) == true)
            {
                for (int i = inGameState.Context.RockShooters.Count - 1; i >= 0; i--)
                {
                    inGameState.Context.RemoveObject(inGameState.Context.RockShooters[i]);
                }

                CurPhase = PostBoss3Phases.Question;
                ElapsedTime = QuestionTime;
            }
        }
    }
}