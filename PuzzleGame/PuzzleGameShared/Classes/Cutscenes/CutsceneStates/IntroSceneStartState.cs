/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// The starting sequence of the intro cutscene.
    /// </summary>
    public class IntroSceneStartState : BaseCutsceneState
    {
        private enum IntroStartPhases
        {
            FadeIn, Wait, CameraShake, FadeOut
        }

        private IngameState inGameState = null;

        private Texture2D Box = null;
        private SoundEffect RumbleSound = null;
        private SoundManager.SoundInstanceHolder RumbleSoundInstance = null;
        private SpriteFont SleepFont = null;

        private const float RumbleMaxVolume = 1f;
        private const float RumbleMinVolume = 0f;

        private readonly Vector2 InitSleepZOffset = new Vector2(6f, -8f);
        private readonly Vector2 SleepZOffset = new Vector2(5, -12);
        private const float SleepZScale = 1f;
        private const int SleepZCount = 3;
        private const double SleepAlphaTimeOffset = 200d;
        private const double SleepAlphaTime = 300d;
        private const double SleepPosTime = 91d;
        private const double SleepPosOffsetTime = 34d;
        private const float SleepMaxPosOffset = 2f;
        private Vector2 SleepTextOrigin = Vector2.Zero;
        private string SleepText = "Z";
        private double ElapsedSleepTime = 0d;

        private const double FadeInTime = 1500d;
        private readonly Color FadeInColor = Color.Black * .5f;
        private readonly Color FadeOutColor = Color.Black;
        private Color CurFadeColor = Color.Black;

        private const double WaitTime = 2000d;

        private Vector2[] CamShakes = null;
        private int CamShakeIndex = 0;
        private const double ShakeChangeTime = 25d;
        private double ElapsedShakeTime = 0d;
        private const double ShakeWaitTime = 3500d;

        private const double FadeOutTime = 1500d;

        private IntroStartPhases CurPhase = IntroStartPhases.FadeIn;
        private double ElapsedTime = 0d;

        public IntroSceneStartState(IngameState ingamestate)
        {
            inGameState = ingamestate;
        }

        protected override void DerivedEnter()
        {
            SoundManager.Instance.StopAndClearMusicTrack();

            inGameState.Context.Player.FacingDir = Direction.Down;
            inGameState.Context.Player.ChangeState(new BlankState(true));
            inGameState.Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Sleep);

            Box = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            RumbleSound = AssetManager.Instance.LoadSound(ContentGlobals.RumbleSound);
            SleepFont = AssetManager.Instance.LoadFont(ContentGlobals.SpecialFont13px);

            CamShakes = LevelGlobals.StandardCameraShakes;

            inGameState.camera.DefaultPosition = inGameState.camera.Position;

            SleepTextOrigin = SleepFont.GetOrigin(SleepText, 0, 1);

#if DEBUG
            Debug.AddCustomDebugCommand(DebugSkip);
#endif
        }

        public override void Exit()
        {
            Box = null;

            SoundManager.Instance.StopAllSounds(RumbleSound);

            RumbleSound = null;
            RumbleSoundInstance = null;
            SleepFont = null;

#if DEBUG
            Debug.RemoveCustomDebugCommand(DebugSkip);
#endif
        }

        protected override void DerivedUpdate()
        {
            inGameState.Context.Player.Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
            ElapsedSleepTime += Time.ElapsedTime.TotalMilliseconds;

            if (CurPhase == IntroStartPhases.FadeIn)
            {
                if (ElapsedTime >= FadeInTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = IntroStartPhases.Wait;
                    CurFadeColor = FadeInColor;
                }
                else
                {
                    CurFadeColor = Interpolation.Interpolate(FadeOutColor, FadeInColor, ElapsedTime / FadeInTime, Interpolation.InterpolationTypes.Linear);
                }
                return;
            }

            if (CurPhase == IntroStartPhases.Wait)
            {
                if (ElapsedTime >= WaitTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = IntroStartPhases.CameraShake;
                    ElapsedShakeTime = 0d;

                    RumbleSoundInstance = SoundManager.Instance.PlaySound(RumbleSound, true, RumbleMaxVolume);
                }
                return;
            }

            if (CurPhase == IntroStartPhases.CameraShake || CurPhase == IntroStartPhases.FadeOut)
            {
                inGameState.camera.SetTranslation(inGameState.camera.DefaultPosition + CamShakes[CamShakeIndex]);

                ElapsedShakeTime += Time.ElapsedTime.TotalMilliseconds;
                if (ElapsedShakeTime >= ShakeChangeTime)
                {
                    CamShakeIndex = UtilityGlobals.Wrap(CamShakeIndex + 1, 0, CamShakes.Length - 1);
                    ElapsedShakeTime = 0d;
                }

                if (CurPhase == IntroStartPhases.CameraShake && ElapsedTime >= ShakeWaitTime)
                {
                    ElapsedTime = 0d;
                    CurPhase = IntroStartPhases.FadeOut;
                }
            }

            if (CurPhase == IntroStartPhases.FadeOut)
            {
                if (ElapsedTime >= FadeOutTime)
                {
                    CurFadeColor = FadeOutColor;

                    //Stop the sound
                    SoundManager.Instance.StopAllSounds(RumbleSound);

                    //Load next cutscene
                    Cutscene nextCutscene = CutsceneLoader.LoadCutscene(CutsceneEnums.IntroPt2);
                    nextCutscene.LevelID = inGameState.LevelNum;
                    nextCutscene.LevelName = ContentGlobals.IntroCutscene2Level;
                    nextCutscene.LevelDescription = inGameState.DisplayLevelName;

                    nextCutscene.Start();
                }
                else
                {
                    CurFadeColor = Interpolation.Interpolate(FadeInColor, FadeOutColor, ElapsedTime / FadeOutTime, Interpolation.InterpolationTypes.Linear);

                    //This can be null if playing without an audio device
                    if (RumbleSoundInstance != null)
                    {
                        RumbleSoundInstance.Volume = Interpolation.Interpolate(RumbleMaxVolume, RumbleMinVolume, ElapsedTime / FadeOutTime, Interpolation.InterpolationTypes.CubicIn);
                        RumbleSoundInstance.SoundInstance.Volume = SoundManager.Instance.CalculateSoundVolume(RumbleSoundInstance.Volume);
                    }
                }
            }
        }

        protected override void DerivedRender()
        {
            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, inGameState.camera.TransformMatrix);

            inGameState.RenderLevelNoBatch();

            //Show the "Z"s of the Echidna sleeping
            Vector2 sleepZPos = inGameState.Context.Player.transform.Position + InitSleepZOffset;

            for (int i = 0; i < SleepZCount; i++)
            {
                Color col = Color.White * (float)Math.Sin((-ElapsedSleepTime + (i * SleepAlphaTimeOffset)) / SleepAlphaTime);
                Vector2 newPos = sleepZPos + (i * SleepZOffset);
                int xOffset = (int)(Math.Sin((ElapsedSleepTime + (i * SleepPosOffsetTime)) / SleepPosTime) * SleepMaxPosOffset);
                newPos.X += xOffset;

                RenderingManager.Instance.spriteBatch.DrawString(SleepFont, SleepText, newPos, col,
                    0f, SleepTextOrigin, SleepZScale, SpriteEffects.None, LevelGlobals.BasePlayerRenderDepth + .05f);
            }

            RenderingManager.Instance.EndCurrentBatch();

            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

            RenderingManager.Instance.DrawSprite(Box, Vector2.Zero, ContentGlobals.BoxRect, CurFadeColor, 0f, Vector2.Zero,
                RenderingGlobals.BaseResolution, SpriteEffects.None, .5f);

            RenderingManager.Instance.EndCurrentBatch();
        }

        protected override void SkipCutscene()
        {
            CurPhase = IntroStartPhases.FadeOut;
            ElapsedTime = FadeOutTime;
        }

        private void DebugSkip()
        {
            if (KeyboardInput.GetKey(Keys.Tab, Debug.DebugKeyboard) == false) return;

            if (KeyboardInput.GetKeyDown(Keys.S, Debug.DebugKeyboard) == true)
            {
                CurPhase = IntroStartPhases.FadeOut;
                ElapsedTime = FadeOutTime;
            }
        }
    }
}