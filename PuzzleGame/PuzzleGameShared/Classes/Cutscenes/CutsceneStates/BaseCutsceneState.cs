/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public abstract  class BaseCutsceneState : IGameState
    {
        /// <summary>
        /// Tells if the cutscene is skippable.
        /// </summary>
        protected bool IsSkippable = true;

        private UIText SkipText = null;

        public void Enter()
        {
            if (IsSkippable == true && SkipText == null)
            {
                string pauseStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.Pause);

                SpriteFont font = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont10px);
                SkipText = UIHelpers.CreateUIText(font, $"{pauseStr}: Skip", new Vector2(0f, .5f), Vector2.One, Color.White,
                UITextOptions.Outline, new UITextData(Color.Black * .8f, 1f, -.001f));
                SkipText.Position = new Vector2(8f, 18f);
            }

            DerivedEnter();
        }

        protected abstract void DerivedEnter();

        public abstract void Exit();

        public void Render()
        {
            DerivedRender();

            if (IsSkippable == false)
            {
                return;
            }

            RenderingManager.Instance.StartBatch(RenderingManager.Instance.spriteBatch, SpriteSortMode.Deferred,
                BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, null);

            SkipText.Render();

            RenderingManager.Instance.EndCurrentBatch();
        }

        protected abstract void DerivedRender();

        public void Update()
        {
            if (IsSkippable == true && Input.GetButtonDown(InputActions.Pause) == true)
            {
                SkipCutscene();
                return;
            }

            DerivedUpdate();
        }

        protected abstract void DerivedUpdate();

        protected abstract void SkipCutscene();
    }
}