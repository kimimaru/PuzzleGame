/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// The sequence on the title screen.
    /// </summary>
    public sealed class TitleSequence : IUpdateable
    {
        public enum TitleSequenceStates
        {
            Up, Down
        }

        private readonly BlockPatterns[] BlockSprites = new BlockPatterns[] { BlockPatterns.Circle, BlockPatterns.Square, BlockPatterns.Diamond,
            BlockPatterns.Triangle };

        private double MoveTime = 7000d;

        private Echidna Player = null;

        private Block BlockPushed = null;
        private float BlockCollisionHeight = 0f;

        private Vector2 BlockStart = Vector2.Zero;
        private Vector2 BlockEnd = Vector2.Zero;

        private Vector2 StartPos = Vector2.Zero;
        private Vector2 EndPos = Vector2.Zero;

        private TitleSequenceStates State = TitleSequenceStates.Up;

        private SequenceData[] SeqData = null;
        private List<int> IndicesToChoose = null;
        private int LastChosen = -1;

        private double ElapsedTime = 0d;

        public TitleSequence(SequenceData[] sequenceData, PlayableContext context)
        {
            SeqData = sequenceData;

            //Make sure it's not fully random, and play each one at least once before resetting
            IndicesToChoose = new List<int>(SeqData.Length);

            Initialize(context);
            Reset();
        }

        private void Initialize(PlayableContext context)
        {
            Player = new Echidna(new BlankState());
            Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.GrabWalk);
            //Player.InputPriorityOptions = PlayerInputPriorityOptions.None;
            context.AddObject(Player);

            BlockPushed = new Block(Vector2.Zero, BlockPatterns.Triangle, null);
            BlockCollisionHeight = BlockPushed.CollisionRect.Height;
            context.AddObject(BlockPushed);

            ObjectInitInfo objInitInfo = new ObjectInitInfo();
            objInitInfo.Properties = new Dictionary<string, string>(1);
            BlockPushed.Initialize(context, objInitInfo);
        }

        public void Update()
        {
            //BlockPushed.Update();
            //Player.Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= MoveTime)
            {
                Reset();
            }
            else
            {
                BlockPushed.transform.Position = Interpolation.Interpolate(BlockStart, BlockEnd, ElapsedTime / MoveTime, Interpolation.InterpolationTypes.Linear);
            }

            float playerY = 0f;
            if (State == TitleSequenceStates.Up)
            {
                Vector2 clamped = UtilityGlobals.ClampRectToOther(Direction.Up, Player.transform.Position, Player.CollisionRect, BlockPushed.CollisionRect);

                playerY = clamped.Y;
            }
            else
            {
                Vector2 clamped = UtilityGlobals.ClampRectToOther(Direction.Down, Player.transform.Position, Player.CollisionRect, BlockPushed.CollisionRect);

                playerY = clamped.Y;
            }

            Player.transform.Position = new Vector2(BlockPushed.transform.Position.X, playerY);
        }

        private void UpdateBlockTypeAndSprite()
        {
            BlockPatterns blockPattern = BlockSprites[RandomGlobals.Randomizer.Next(0, BlockSprites.Length)];

            BlockPushed.BlockPattern = blockPattern;
            BlockPushed.PatternSprite.SourceRect = Block.GetPatternSourceRect(BlockPushed.BlockPattern, BlockPushed.BlockDirection);
        }

        private void SetBlockPos(in TitleSequenceStates state)
        {
            BlockPushed.transform.Position = StartPos;

            if (state == TitleSequenceStates.Down)
            {
                BlockPushed.transform.Position += new Vector2(0f, (BlockCollisionHeight / 2));

                Player.FacingDir = Direction.Down;

                BlockStart = BlockPushed.transform.Position;
                BlockEnd.Y = EndPos.Y - BlockCollisionHeight - 32;
                BlockEnd.X = EndPos.X;
                Player.transform.Position = BlockStart;
            }
            else
            {
                BlockPushed.transform.Position += new Vector2(0f, -(BlockCollisionHeight / 2));

                Player.FacingDir = Direction.Up;

                BlockStart = BlockPushed.transform.Position;
                BlockEnd.Y = EndPos.Y + BlockCollisionHeight + 32;
                BlockEnd.X = EndPos.X;
                Player.transform.Position = BlockStart;
            }
        }

        public void RenderBlock()
        {
            BlockPushed.Render();
        }

        public void RenderPlayer()
        {
            Player.Render();
        }

        public void Reset()
        {
            int lastChosenSaved = -1;

            if (IndicesToChoose.Count == 0)
            {
                //Refill which are chosen
                for (int i = 0; i < SeqData.Length; i++)
                    IndicesToChoose.Add(i);

                //Make sure the next one is unique if the list was just finished
                //Remove the last chosen index from the list and add it back after choosing the next one
                if (LastChosen >= 0)
                {
                    lastChosenSaved = LastChosen;
                    IndicesToChoose.Remove(LastChosen);
                }
            }

            int randVal = RandomGlobals.Randomizer.Next(0, IndicesToChoose.Count);
            LastChosen = IndicesToChoose[randVal];
            IndicesToChoose.RemoveAt(randVal);

            //Add back the last chosen if we should
            if (lastChosenSaved >= 0)
            {
                IndicesToChoose.Add(lastChosenSaved);
            }

            ref SequenceData data = ref SeqData[LastChosen];

            State = data.State;
            StartPos = data.StartPos;
            EndPos = data.EndPos;
            MoveTime = data.MoveTime;

            ElapsedTime = 0d;
            UpdateBlockTypeAndSprite();
            SetBlockPos(State);

            BlockPushed.particleEngine.Stop(false);
            BlockPushed.UpdateData();
            BlockPushed.particleEngine.Resume();
        }

        public struct SequenceData
        {
            public Vector2 StartPos;
            public Vector2 EndPos;
            public TitleSequenceStates State;
            public double MoveTime;

            public SequenceData(in Vector2 startPos, in Vector2 endPos, in TitleSequenceStates state, in double moveTime)
            {
                StartPos = startPos;
                EndPos = endPos;
                State = state;
                MoveTime = moveTime;
            }
        }
    }
}