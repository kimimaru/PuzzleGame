/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace PuzzleGame
{
    /// <summary>
    /// A derived <see cref="ContentManager"/> that provides extra functionality.
    /// </summary>
    public sealed class MBContentManager : ContentManager
    {
        public MBContentManager(IServiceProvider serviceProvider) : base(serviceProvider)
        {

        }

        public MBContentManager(IServiceProvider serviceProvider, string rootDirectory) : base(serviceProvider, rootDirectory)
        {

        }

        /// <summary>
        /// Tells how many assets are currently loaded.
        /// </summary>
        /// <returns>An integer representing how many assets are loaded.</returns>
        public int LoadedAssetCount => LoadedAssets.Count;

        /// <summary>
        /// Tells if an asset with a particular name is loaded.
        /// </summary>
        /// <param name="assetName">The name of the asset.</param>
        /// <returns>true if the asset is loaded, otherwise false.</returns>
        public bool IsAssetLoaded(in string assetName)
        {
            return LoadedAssets.ContainsKey(assetName);
        }
    }
}