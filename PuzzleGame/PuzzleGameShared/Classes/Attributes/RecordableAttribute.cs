/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// An attribute used to indicate a recordable field or property on an object.
    /// <para>For recordable events, use the explicit syntax.
    /// It's possible the backing field has the same name as the event, but it's likely not guaranteed
    /// and increases the complexity of finding the backing field.</para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class RecordableAttribute : Attribute
    {
        /// <summary>
        /// The recordable flags of this attribute.
        /// </summary>
        public RecordableFlags RecordedFlags { get; private set; } = RecordableFlags.None;

        public RecordableAttribute(RecordableFlags recordableFlags)
        {
            RecordedFlags = recordableFlags;
        }
    }
}