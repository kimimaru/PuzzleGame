﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public sealed class LevelMap
    {
        public TiledMap Map { get; private set; } = null;
        public RectangleF MapBounds { get; private set; } = RectangleF.Empty;

        public TiledMapObjectLayer CollisionLayer { get; private set; } = null;
        public TiledMapObjectLayer BlockLayer { get; private set; } = null;
        public TiledMapObjectLayer SpotLayer { get; private set; } = null;
        public TiledMapObjectLayer PortalLayer { get; private set; } = null;
        public TiledMapObjectLayer SwitchLayer { get; private set; } = null;
        public TiledMapObjectLayer WarpLayer { get; private set; } = null;
        public TiledMapObjectLayer TextLayer { get; private set; } = null;
        public TiledMapObjectLayer SurfaceLayer { get; private set; } = null;
        public TiledMapObjectLayer RockShooterLayer { get; private set; } = null;

        public Vector2 TileSize { get; private set; } = Vector2.Zero;
        public Vector2 TileSizeHalf => (TileSize / 2);

        public TiledMapObject PlayerStartTile { get; private set; } = null;
        public TiledMapObject PortalTile { get; private set; } = null;

        public TiledMapObject CameraFocusTile { get; private set; } = null;

        public SurfaceTileEngine SurfaceTiles = null;

        public bool AllowsUndo { get; private set; } = true;
        public bool AllowsRestart { get; private set; } = true;
        public bool AllowsExiting = true;
        public bool AllowsPause = true;
        public string MusicName { get; private set; } = string.Empty;
        public bool CameraFollow = false;
        public bool ShowHUD = true;
        public bool UnlocksBonus { get; private set; } = false;
        public CutsceneEnums CutsceneID { get; private set; } = CutsceneEnums.None;

        public LevelMap(TiledMap tiledMap)
        {
            Map = tiledMap;
            Initialize();
        }

        private void Initialize()
        {
            TileSize = new Vector2(Map.TileWidth, Map.TileHeight);
            MapBounds = new RectangleF(0f, 0f, Map.WidthInPixels, Map.HeightInPixels);

            if (Map.Properties.TryGetValue(LevelGlobals.AllowsUndoKey, out string allowsUndo) == true)
            {
                if (bool.TryParse(allowsUndo, out bool allowUndo) == true)
                {
                    AllowsUndo = allowUndo;
                }
            }

            if (Map.Properties.TryGetValue(LevelGlobals.AllowsRestartKey, out string allowsRestart) == true)
            {
                if (bool.TryParse(allowsRestart, out bool allowRestart) == true)
                {
                    AllowsRestart = allowRestart;
                }
            }

            if (Map.Properties.TryGetValue(LevelGlobals.AllowsExitingKey, out string allowsExiting) == true)
            {
                if (bool.TryParse(allowsExiting, out bool allowExiting) == true)
                {
                    AllowsExiting = allowExiting;
                }
            }

            if (Map.Properties.TryGetValue(LevelGlobals.ShowHUDKey, out string showHUD) == true)
            {
                if (bool.TryParse(showHUD, out bool displayHUD) == true)
                {
                    ShowHUD = displayHUD;
                }
            }

            if (Map.Properties.TryGetValue(LevelGlobals.MusicKey, out string musicName) == false)
            {
                musicName = ContentGlobals.DefaultLevelMusic;
            }

            if (Map.Properties.TryGetValue(LevelGlobals.CutsceneIDKey, out string cutsceneIDStr) == true)
            {
                if (Enum.TryParse(cutsceneIDStr, true, out CutsceneEnums cutsceneID) == true)
                {
                    CutsceneID = cutsceneID;
                }
            }

            if (Map.Properties.TryGetValue(LevelGlobals.UnlocksBonusKey, out string unlocksBonusStr) == true)
            {
                if (bool.TryParse(unlocksBonusStr, out bool unlocksBonus) == true)
                {
                    UnlocksBonus = unlocksBonus;
                }
            }

            //By default, if the level is larger than can be seen, make the camera follow
            //if (MapBounds.Width > RenderingGlobals.BaseResolutionWidth || MapBounds.Height > RenderingGlobals.BaseResolutionHeight)
            //{
            //    CameraFollow = true;
            //}

            //Override camera follow if we should
            if (Map.Properties.TryGetValue(LevelGlobals.CameraFollowKey, out string cameraFollowStr) == true)
            {
                if (bool.TryParse(cameraFollowStr, out bool cameraFollow) == true)
                {
                    CameraFollow = cameraFollow;
                }
            }

            MusicName = musicName;

            TiledMapObjectLayer playerStart = Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.PlayerStartLayerName);
            if (playerStart == null || playerStart.Objects.Length == 0)
            {
                throw new Exception("Either \"PlayerStart\" layer doesn't exist or an object designating the start tile does not exist in the TiledMap!");
            }
            
            PlayerStartTile = playerStart.Objects[0];

            CollisionLayer = Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.CollisionLayerName);
            BlockLayer = Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.BlockLayerName);
            SpotLayer = Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.SpotLayerName);
            PortalLayer = Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.PortalLayerName);
            SwitchLayer = Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.SwitchLayerName);
            WarpLayer = Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.WarpLayerName);
            TextLayer = Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.TextLayerName);
            SurfaceLayer = Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.SurfaceLayerName);
            RockShooterLayer = Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.RockShooterLayerName);

            TiledMapObjectLayer cameraFocusLayer = Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.CameraFocusLayerName);
            if (cameraFocusLayer != null && cameraFocusLayer.Objects != null && cameraFocusLayer.Objects.Length > 0)
            {
                CameraFocusTile = cameraFocusLayer.Objects[0];
            }

            PortalTile = PortalLayer.Objects[0];
        }

        public Vector2 GetPosFromColRow(in int x, in int y)
        {
            return new Vector2(x * TileSize.X, y * TileSize.Y);
        }

        public void GetTileColRol(in Vector2 position, out int x, out int y)
        {
            x = (int)Math.Floor(position.X / TileSize.X);
            y = (int)Math.Floor(position.Y / TileSize.Y);
        }

        public int GetTileIndex(in Vector2 position)
        {
            GetTileColRol(position, out int x, out int y);

            int index = (y * Map.Width) + x;
            
            return index;
        }

        public bool IsOutOfBounds(in Vector2 position)
        {
            GetTileColRol(position, out int xTileIndex, out int yTileIndex);

            return (xTileIndex < 0 || xTileIndex >= Map.Width
                    || yTileIndex >= Map.Height);
        }

        public void DrawMap()
        {
            //Layers are ordered by depth from top to bottom in the .tmx file
            for (int i = 0; i < Map.TileLayers.Count; i++)
            {
                TiledMapTileLayer layer = Map.TileLayers[i];
            
                //Draw all tiles in each layer
                for (int j = 0; j < layer.Tiles.Count; j++)
                {
                    //If the tile is blank, ignore it
                    if (layer.Tiles[j].IsBlank == true) continue;
            
                    TiledMapTile tile = layer.Tiles[j];
                    
                    //Find which tileset this tile belongs to
                    TiledMapTileset tileSet = Map.GetTilesetByTileGlobalIdentifier(tile.GlobalIdentifier);
            
                    //Continue if the tileset wasn't found or its texture is null
                    if (tileSet == null || tileSet.Texture == null) continue;
            
                    //Subtract the tileset's first global identifier from the tile's global identifier to get the index of the region
                    Rectangle getRegion = tileSet.GetTileRegion(tile.GlobalIdentifier - tileSet.FirstGlobalIdentifier);

                    Vector2 pos = new Vector2(((j % Map.Width) * Map.TileWidth), (j / Map.Width) * Map.TileHeight);

                    SpriteEffects flip = SpriteEffects.None;

                    if (tile.IsFlippedHorizontally == true)
                    {
                        flip |= SpriteEffects.FlipHorizontally;
                    }

                    if (tile.IsFlippedVertically == true)
                    {
                        flip |= SpriteEffects.FlipVertically;
                    }

                    RenderingManager.Instance.DrawSprite(tileSet.Texture, pos, getRegion, Color.White * layer.Opacity, 0f, Vector2.Zero,
                        Vector2.One, flip, LevelGlobals.BaseTileLayerDepth + (LevelGlobals.TileLayerDepthIncrease * i));
                }
            }
        }
    }
}
