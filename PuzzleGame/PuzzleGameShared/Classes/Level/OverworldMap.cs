﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public sealed class OverworldMap
    {
        public TiledMap Map { get; private set; } = null;
        public RectangleF MapBounds { get; private set; } = RectangleF.Empty;

        public TiledMapObjectLayer NodeLayer { get; private set; } = null;

        public Vector2 TileSize { get; private set; } = Vector2.Zero;
        public Vector2 TileSizeHalf => (TileSize / 2);

        public List<OverworldMapNode> Nodes = null;

        public string MusicName { get; private set; } = string.Empty;
        public int WorldID { get; private set; } = 0;
        public string WorldName { get; private set; } = string.Empty;

        public OverworldMap(TiledMap tiledMap)
        {
            Map = tiledMap;
            Initialize();
        }

        private void Initialize()
        {
            TileSize = new Vector2(Map.TileWidth, Map.TileHeight);
            MapBounds = new RectangleF(0f, 0f, Map.WidthInPixels, Map.HeightInPixels);

            if (Map.Properties.TryGetValue(LevelGlobals.MusicKey, out string musicName) == false)
            {
                musicName = ContentGlobals.DefaultOverworldMusic;
            }

            if (Map.Properties.TryGetValue(LevelGlobals.WorldNumKey, out string worldNumStr) == true)
            {
                if (int.TryParse(worldNumStr, out int worldNum) == true)
                {
                    WorldID = worldNum;
                }
            }

            if (Map.Properties.TryGetValue(LevelGlobals.WorldNameKey, out string worldName) == true)
            {
                WorldName = worldName;
            }

            if (WorldName == null)
            {
                WorldName = string.Empty;
            }

            MusicName = musicName;

            NodeLayer = Map.GetLayer<TiledMapObjectLayer>(LevelGlobals.NodeLayerName);

            Nodes = new List<OverworldMapNode>(NodeLayer.Objects.Length);

            for (int i = 0; i < NodeLayer.Objects.Length; i++)
            {
                TiledMapObject node = NodeLayer.Objects[i];

                //Continue if a node is disabled
                if (node.IsVisible == false) continue;

                MapNodeTypes nodeType = MapNodeTypes.Level;
                NodeUnlockTypes unlockType = NodeUnlockTypes.Normal;

                if (node.Properties.TryGetValue(LevelGlobals.NodeTypeKey, out string nodeTypeStr) == true)
                {
                    Enum.TryParse(nodeTypeStr, true, out nodeType);
                }

                if (node.Properties.TryGetValue(LevelGlobals.UnlockTypeKey, out string nodeUnlockStr) == true)
                {
                    Enum.TryParse(nodeUnlockStr, true, out unlockType);
                }

                //Ignore bonus levels if they haven't been unlocked
                if (unlockType == NodeUnlockTypes.Bonus && DataHandler.saveData.CompleteData.UnlockBonus == false)
                {
                    continue;
                }

                ObjectInitInfo objInitInfo = (ObjectInitInfo)node;
                objInitInfo.Position += TileSizeHalf;

                OverworldMapNode overworldNode = OverworldMapNode.CreateMapNodeForType(nodeType);
                overworldNode.Initialize(WorldID, objInitInfo);

                Nodes.Add(overworldNode);
            }
        }

        public Vector2 GetPosFromColRow(in int x, in int y)
        {
            return new Vector2(x * TileSize.X, y * TileSize.Y);
        }

        public void GetTileColRol(in Vector2 position, out int x, out int y)
        {
            x = (int)Math.Floor(position.X / TileSize.X);
            y = (int)Math.Floor(position.Y / TileSize.Y);
        }

        public int GetTileIndex(in Vector2 position)
        {
            GetTileColRol(position, out int x, out int y);

            int index = (y * Map.Width) + x;
            
            return index;
        }

        public bool IsOutOfBounds(in Vector2 position)
        {
            GetTileColRol(position, out int xTileIndex, out int yTileIndex);

            return (xTileIndex < 0 || xTileIndex >= Map.Width
                    || yTileIndex >= Map.Height);
        }

        public void DrawMap()
        {
            //Layers are ordered by depth from top to bottom in the .tmx file
            for (int i = 0; i < Map.TileLayers.Count; i++)
            {
                TiledMapTileLayer layer = Map.TileLayers[i];
            
                //Draw all tiles in each layer
                for (int j = 0; j < layer.Tiles.Count; j++)
                {
                    //If the tile is blank, ignore it
                    if (layer.Tiles[j].IsBlank == true) continue;
            
                    TiledMapTile tile = layer.Tiles[j];
                    
                    //Find which tileset this tile belongs to
                    TiledMapTileset tileSet = Map.GetTilesetByTileGlobalIdentifier(tile.GlobalIdentifier);
            
                    //Continue if the tileset wasn't found or its texture is null
                    if (tileSet == null || tileSet.Texture == null) continue;

                    //Subtract the tileset's first global identifier from the tile's global identifier to get the index of the region
                    Rectangle getRegion = tileSet.GetTileRegion(tile.GlobalIdentifier - tileSet.FirstGlobalIdentifier);
            
                    Vector2 pos = new Vector2(((j % Map.Width) * Map.TileWidth), (j / Map.Width) * Map.TileHeight);

                    SpriteEffects flip = SpriteEffects.None;

                    if (tile.IsFlippedHorizontally == true)
                    {
                        flip |= SpriteEffects.FlipHorizontally;
                    }

                    if (tile.IsFlippedVertically == true)
                    {
                        flip |= SpriteEffects.FlipVertically;
                    }

                    RenderingManager.Instance.DrawSprite(tileSet.Texture, pos, getRegion, Color.White * layer.Opacity, 0f, Vector2.Zero,
                        Vector2.One, flip, .9f);
                }
            }
        }
    }
}
