/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Represents a node on the overworld map.
    /// </summary>
    public sealed class WorldMapNode : OverworldMapNode
    {
        public int LandLevelID = -1;

        public WorldMapNode()
        {
            NodeType = MapNodeTypes.World;
        }
        
        public override void Initialize(in int initWorldNum, in ObjectInitInfo objectInitInfo)
        {
            base.Initialize(initWorldNum, objectInitInfo);

            if (objectInitInfo.Properties.TryGetValue(LevelGlobals.LandLevelIDKey, out string landLevelIDStr) == true)
            {
                int.TryParse(landLevelIDStr, out LandLevelID);
            }

            DisplayName = "To";
            Description = $"World {WorldID + 1}";
        }

        public override void OnNodeSelected(OverworldState overworldState)
        {
            //SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);

            if (overworldState.IsPlayerMoving == true)
            {
                overworldState.IsPlayerMoving = false;
                overworldState.Context.Player.transform.Position = overworldState.CurNode.Position;
                overworldState.camera.LookAtAndClamp(overworldState.Context.Player.transform.Position, overworldState.overworldMap.MapBounds);
            }

            SaveData.SetLastPlayedLevel(LandLevelID);

            //Check if we have save data for this world
            //If not, add it and save the data
            if (SaveData.HasWorldData(WorldID) == false)
            {
                DataHandler.saveData.WorldSaves.Add(WorldID, new WorldData());
                DataHandler.SaveSaveData();
            }

            WorldNodeVisual worldVisual = (WorldNodeVisual)NodeVisual;

            if (WorldID != overworldState.WorldID)
            {
                overworldState.ChangeState(new OverworldWarpInState(overworldState, worldVisual.WarpVisual,
                    worldVisual.WarpVisual.WarpDuration, Color.Transparent, Color.Black));
            }
            else
            {
                overworldState.ChangeState(new OverworldWarpInState(overworldState, worldVisual.WarpVisual));
            }
            //overworldState.ChangeState(new OverworldEnterLevelState(overworldState, LoadLevel));
        }

        //private void LoadLevel()
        //{
        //    SaveData.SetLastPlayedLevel(LandLevelID);
        //    SaveData.SetLastPlayedWorld(WorldID);
        //
        //    //Check if we have save data for this world
        //    //If not, add it and save the data
        //    if (SaveData.HasWorldData(WorldID) == false)
        //    {
        //        DataHandler.saveData.WorldSaves.Add(WorldID, new WorldData());
        //        DataHandler.SaveSaveData();
        //    }
        //
        //    TiledMap overworld = AssetManager.Instance.LoadTiledMap(LevelName);
        //
        //    //Change to the next overworld
        //    GameStateManager.Instance.ChangeGameState(new OverworldState(overworld));
        //}
    }
}