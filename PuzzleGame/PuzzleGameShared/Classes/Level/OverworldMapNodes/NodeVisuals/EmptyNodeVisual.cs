/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public sealed class EmptyNodeVisual : INodeVisual, ITintable
    {
        public Color TintColor { get; set; } = Color.White;

        private Circle RenderCircle = default;
        private Texture2D NodeTex = null;
        private Rectangle? NodeRect = default;
        private Color IncompleteColor = Color.White;
        private Color CompleteColor = Color.White;

        public EmptyNodeVisual(in Circle renderCircle, Texture2D nodeTex, in Rectangle? nodeRect, in Color incompleteColor, in Color completeColor)
        {
            RenderCircle = renderCircle;
            NodeTex = nodeTex;
            NodeRect = nodeRect;

            IncompleteColor = incompleteColor;
            CompleteColor = completeColor;
        }

        public void SetVisualColorLerp(in double time)
        {
            TintColor = Interpolation.Interpolate(IncompleteColor, CompleteColor, time, Interpolation.InterpolationTypes.Linear);
        }

        public void SetCompleteState(in bool complete)
        {
            TintColor = (complete == true) ? CompleteColor : IncompleteColor;
        }

        public void Render()
        {
            RenderingManager.Instance.spriteBatch.DrawCircle(NodeTex, NodeRect, RenderCircle, TintColor, .3f);
        }
    }
}