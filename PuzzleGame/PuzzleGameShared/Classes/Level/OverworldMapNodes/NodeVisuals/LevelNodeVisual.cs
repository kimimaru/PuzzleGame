/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public sealed class LevelNodeVisual : INodeVisual
    {
        private Portal PortalVisual = null;

        public LevelNodeVisual(Portal portalVisual)
        {
            PortalVisual = portalVisual;
        }

        public void SetVisualColorLerp(in double time)
        {
            PortalVisual.TintColor = Interpolation.Interpolate(Portal.DeactivatedColor, Portal.ActivatedColor, time, Interpolation.InterpolationTypes.Linear);
        }

        public void SetCompleteState(in bool complete)
        {
            PortalVisual.Activated = complete;
        }

        public void Render()
        {
            PortalVisual.Render();
        }
    }
}