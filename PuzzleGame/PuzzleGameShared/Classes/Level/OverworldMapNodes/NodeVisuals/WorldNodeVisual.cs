/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public sealed class WorldNodeVisual : INodeVisual
    {
        public Warp WarpVisual { get; private set; } = null;

        public WorldNodeVisual(Warp warpVisual)
        {
            WarpVisual = warpVisual;
            WarpVisual.DrawWarpDest = false;
            WarpVisual.DrawWarpDestID = false;
            WarpVisual.WarpParticles.Stop(false);
        }

        public void SetVisualColorLerp(in double time)
        {

        }

        public void SetCompleteState(in bool complete)
        {
            WarpVisual.WarpDestBlocked = !complete;
            if (WarpVisual.WarpDestBlocked == true)
            {
                WarpVisual.WarpParticles.Stop(false);
            }
            else
            {
                WarpVisual.WarpParticles.Resume();
            }
        }

        public void Render()
        {
            WarpVisual.Render();
        }
    }
}