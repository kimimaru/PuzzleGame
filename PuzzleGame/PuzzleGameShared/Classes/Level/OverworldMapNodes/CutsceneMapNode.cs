/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// Represents a cutscene node on the overworld map.
    /// </summary>
    public sealed class CutsceneMapNode : OverworldMapNode
    {
        public CutsceneEnums CutsceneID { get; private set; } = CutsceneEnums.Intro;

        public CutsceneMapNode()
        {
            NodeType = MapNodeTypes.Cutscene;
        }

        public override void Initialize(in int initWorldNum, in ObjectInitInfo objectInitInfo)
        {
            base.Initialize(initWorldNum, objectInitInfo);

            CutsceneEnums cutsceneID = CutsceneEnums.Intro;

            if (objectInitInfo.Properties.TryGetValue(LevelGlobals.CutsceneIDKey, out string cutsceneIDStr) == true)
            {
                Enum.TryParse(cutsceneIDStr, true, out cutsceneID);
            }

            CutsceneID = cutsceneID;

            DisplayName = "Cutscene";
        }

        public override void OnNodeSelected(OverworldState overworldState)
        {
            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);

            if (overworldState.IsPlayerMoving == true)
            {
                overworldState.IsPlayerMoving = false;
                overworldState.Context.Player.transform.Position = overworldState.CurNode.Position;
                overworldState.camera.LookAtAndClamp(overworldState.Context.Player.transform.Position, overworldState.overworldMap.MapBounds);
            }

            overworldState.ChangeState(new OverworldEnterLevelState(overworldState, LoadCutscene));
        }

        private void LoadCutscene()
        {
            //Load the cutscene and set its information
            Cutscene cutscene = CutsceneLoader.LoadCutscene(CutsceneID);
            cutscene.LevelID = LevelID;
            cutscene.LevelName = LevelName;
            cutscene.LevelDescription = Description;

            //Start the cutscene
            cutscene.Start();
        }
    }
}