/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// The types of overworld map nodes.
    /// </summary>
    public enum MapNodeTypes
    {
        Level, World, Cutscene, Empty
    }

    /// <summary>
    /// The unlock types for overworld map nodes.
    /// </summary>
    public enum NodeUnlockTypes
    {
        Normal, Bonus
    }

    /// <summary>
    /// Represents a node on the overworld map.
    /// </summary>
    public abstract class OverworldMapNode
    {
        public int WorldID = 0;
        public int LevelID = 0;
        public Vector2 Position = Vector2.Zero;
        public string LevelName = string.Empty;
        public string Description = string.Empty;
        public int? UpNode = null;
        public int? DownNode = null;
        public int? LeftNode = null;
        public int? RightNode = null;
        public MapNodeTypes NodeType { get; protected set; } = MapNodeTypes.Level;
        public bool AutoComplete = false;
        public string DisplayID = string.Empty;
        public string DisplayName = string.Empty;
        public NodeUnlockTypes UnlockType = NodeUnlockTypes.Normal;

        /// <summary>
        /// Overrides what is shown as the node's level number; this can be any string.
        /// </summary>
        public string DisplayOverride = string.Empty;

        #region Visuals

        public INodeVisual NodeVisual = null;

        public Line? UpLine = null;
        public Color UplineColor = Color.White;

        public Line? DownLine = null;
        public Color DownlineColor = Color.White;

        public Line? LeftLine = null;
        public Color LeftlineColor = Color.White;

        public Line? RightLine = null;
        public Color RightlineColor = Color.White;

        public bool HasAltAnim = false;

        #endregion

        public OverworldMapNode()
        {

        }

        /// <summary>
        /// What happens when the node is selected on the overworld map.
        /// </summary>
        public abstract void OnNodeSelected(OverworldState overworldState);

        public virtual void Initialize(in int initWorldNum, in ObjectInitInfo objectInitInfo)
        {
            Position = objectInitInfo.Position;

            //Get the level name; empty nodes don't need one
            objectInitInfo.Properties.TryGetValue(LevelGlobals.LevelNameKey, out LevelName);

            if (objectInitInfo.Properties.TryGetValue(LevelGlobals.IDKey, out string levelID) == true)
            {
                int.TryParse(levelID, out LevelID);
            }
            
            if (objectInitInfo.Properties.TryGetValue(LevelGlobals.UpDirKey, out string upDirStr) == true)
            {
                if (int.TryParse(upDirStr, out int up) == true)
                {
                    UpNode = up;
                }
            }

            if (objectInitInfo.Properties.TryGetValue(LevelGlobals.DownDirKey, out string downDirStr) == true)
            {
                if (int.TryParse(downDirStr, out int down) == true)
                {
                    DownNode = down;
                }
            }

            if (objectInitInfo.Properties.TryGetValue(LevelGlobals.LeftDirKey, out string leftDirStr) == true)
            {
                if (int.TryParse(leftDirStr, out int left) == true)
                {
                    LeftNode = left;
                }
            }

            if (objectInitInfo.Properties.TryGetValue(LevelGlobals.RightDirKey, out string rightDirStr) == true)
            {
                if (int.TryParse(rightDirStr, out int right) == true)
                {
                    RightNode = right;
                }
            }

            objectInitInfo.Properties.TryGetValue(LevelGlobals.DescriptionKey, out Description);
            if (Description == null) Description = string.Empty;

            WorldID = initWorldNum;

            if (objectInitInfo.Properties.TryGetValue(LevelGlobals.WorldIDKey, out string worldIDStr) == true)
            {
                if (int.TryParse(worldIDStr, out WorldID) == false)
                {
                    WorldID = initWorldNum;
                }
            }

            if (objectInitInfo.Properties.TryGetValue(LevelGlobals.AutoCompleteKey, out string autoCompleteStr) == true)
            {
                bool.TryParse(autoCompleteStr, out AutoComplete);
            }

            objectInitInfo.Properties.TryGetValue(LevelGlobals.DisplayIDKey, out DisplayID);
            if (DisplayID == null) DisplayID = string.Empty;

            if (objectInitInfo.Properties.TryGetValue(LevelGlobals.UnlockTypeKey, out string unlockTypeStr) == true)
            {
                if (Enum.TryParse(unlockTypeStr, true, out NodeUnlockTypes unlockType) == true)
                {
                    UnlockType = unlockType;
                }
            }

            objectInitInfo.Properties.TryGetValue(LevelGlobals.DisplayOverrideKey, out DisplayOverride);
            if (DisplayOverride == null) DisplayOverride = string.Empty;

            if (objectInitInfo.Properties.TryGetValue(LevelGlobals.NodeAltAnimKey, out string hasAltAnimStr) == true)
            {
                if (bool.TryParse(hasAltAnimStr, out bool altAnim) == true)
                {
                    HasAltAnim = altAnim;
                }
            }
        }

        public virtual void Update()
        {

        }

        public int? GetNodeForDirection(in Direction direction)
        {
            switch (direction)
            {
                case Direction.Up: return UpNode;
                case Direction.Down:
                default: return DownNode;
                case Direction.Left: return LeftNode;
                case Direction.Right: return RightNode;
            }
        }

        public Line? GetLineForDirection(in Direction direction)
        {
            switch (direction)
            {
                case Direction.Up: return UpLine;
                case Direction.Down:
                default: return DownLine;
                case Direction.Left: return LeftLine;
                case Direction.Right: return RightLine;
            }
        }

        public ref Color GetLineColorForDirection(in Direction direction)
        {
            switch (direction)
            {
                case Direction.Up: return ref UplineColor;
                case Direction.Down:
                default: return ref DownlineColor;
                case Direction.Left: return ref LeftlineColor;
                case Direction.Right: return ref RightlineColor;
            }
        }

        public Direction? GetDirectionToNode(int? nodeDir)
        {
            if (nodeDir == null) return null;

            if (UpNode == nodeDir) return Direction.Up;
            else if (DownNode == nodeDir) return Direction.Down;
            else if (LeftNode == nodeDir) return Direction.Left;
            else if (RightNode == nodeDir) return Direction.Right;

            return null;
        }

        public Direction? GetDirectionFromNode(int? nodeDir)
        {
            Direction? dir = GetDirectionToNode(nodeDir);
            if (dir != null)
            {
                return DirectionUtil.GetOppositeDirection(dir.Value);
            }

            return null;
        }

        /// <summary>
        /// Creates a map node, given the type of map node to create.
        /// </summary>
        /// <param name="mapNodeType">The type of map node to create.</param>
        /// <returns>An OverworldMapNode associated with the given map node type.</returns>
        public static OverworldMapNode CreateMapNodeForType(in MapNodeTypes mapNodeType)
        {
            switch(mapNodeType)
            {
                default:
                case MapNodeTypes.Level: return new LevelMapNode();
                case MapNodeTypes.World: return new WorldMapNode();
                case MapNodeTypes.Cutscene: return new CutsceneMapNode();
                case MapNodeTypes.Empty: return new EmptyMapNode();
            }
        }
    }
}