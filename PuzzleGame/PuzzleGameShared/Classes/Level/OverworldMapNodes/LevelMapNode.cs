/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Represents a level node on the overworld map.
    /// </summary>
    public sealed class LevelMapNode : OverworldMapNode
    {
        /// <summary>
        /// The number of the level in the world based on the DisplayID, or the LevelID if the DisplayID is not defined.
        /// This is mainly for display purposes.
        /// </summary>
        public int NumberInWorld { get; private set; } = 0;

        /// <summary>
        /// Whether the level is unlocked, meaning a path to it is available, but not completed.
        /// This is used to visually indicate levels that have yet to be complete.
        /// </summary>
        public bool Unlocked { get; private set; } = false;

        private double ElapsedTime = 0d;

        public LevelMapNode()
        {
            NodeType = MapNodeTypes.Level;
        }

        public override void Initialize(in int initWorldNum, in ObjectInitInfo objectInitInfo)
        {
            base.Initialize(initWorldNum, objectInitInfo);

            //If the display ID is empty, use the level ID
            if (string.IsNullOrEmpty(DisplayID) == true)
            {
                NumberInWorld = LevelID + 1;
            }
            //Otherwise, use the display ID
            else
            {
                if (int.TryParse(DisplayID, NumberStyles.Any, CultureInfo.InvariantCulture, out int numInWorld) == true)
                {
                    NumberInWorld = numInWorld;
                }
                //Unable to parse - fallback to the level ID for safety
                else
                {
                    NumberInWorld = LevelID + 1;
                }
            }

            //Now that we have the number in the world, decide what should actually be displayed (Ex. "1-1")
            //By default, use the display override
            string displayNum = DisplayOverride;

            //If there's no display override, use the number
            if (string.IsNullOrEmpty(displayNum) == true)
            {
                displayNum = NumberInWorld.ToString();
            }

            DisplayName = $"{WorldID + 1}-{displayNum}";
        }

        public void SetUnlocked(in bool unlocked)
        {
            Unlocked = unlocked;
        }

        public override void Update()
        {
            base.Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (Unlocked == false)
            {
                return;
            }

            //If unlocked, change the visual
            double time = UtilityGlobals.PingPong(ElapsedTime / Portal.FadeTime, 0d, 1d);
            NodeVisual.SetVisualColorLerp(time);
        }

        public override void OnNodeSelected(OverworldState overworldState)
        {
            SoundManager.Instance.PlaySound(AssetManager.Instance.LoadSound(ContentGlobals.MenuOptionSelectSound), false, 1f);

            if (overworldState.IsPlayerMoving == true)
            {
                overworldState.IsPlayerMoving = false;
                overworldState.Context.Player.transform.Position = overworldState.CurNode.Position;
                overworldState.camera.LookAtAndClamp(overworldState.Context.Player.transform.Position, overworldState.overworldMap.MapBounds);
            }

            overworldState.ChangeState(new OverworldEnterLevelState(overworldState, LoadLevel));
        }

        private void LoadLevel()
        {
            //Unload everything when entering a new level
            SoundManager.Instance.ClearAllSounds();
            SoundManager.Instance.ClearMusicCache();

            AssetManager.Instance.UnloadLoadedContent();

            AssetManager.Instance.LoadInGameContent();

            int levelNum = LevelID;
            TiledMap level = AssetManager.Instance.LoadTiledMap(LevelName);

            //Send data for this level
            string levelName = Description;
            string bestLevelMoves = string.Empty;
            string bestLevelTime = string.Empty;

            //This check prevents crashes when debugging - this data should always exist normally if the level is completed
            if (SaveData.GetLevelData(WorldID, LevelID, out LevelData lvlData) == true)
            {
                //Only show this information if the level is complete
                if (lvlData.Complete == true)
                {
                    bestLevelMoves = lvlData.LvlStats.NumMoves.ToString();
                    bestLevelTime = LevelGlobals.GetFormattedLevelTimeString(lvlData.LvlStats.LevelTime);
                }
            }

            IngameState ingameState = new IngameState(levelNum, level, LevelEventTypes.Base, DisplayName, levelName, bestLevelMoves, bestLevelTime);
            ingameState.ChangeLevelState(new LevelIntroState(ingameState, ingameState.Context.Player.transform.Position));
            GameStateManager.Instance.ChangeGameState(ingameState);
        }
    }
}