﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// A panel showing information about a level on the overworld.
    /// </summary>
    public sealed class OverworldLevelPanel : IUpdateable, IPosition
    {
        private enum LevelPanelStates
        {
            FadingIn, Still
        }

        private const float BaseDescriptionScale = 1f;
        private const float PanelMargin = 4f;
        private const float LevelTextScale = 1f;

        private const float StartContentYOffset = 12f;

        private static readonly Vector2 BasePanelSize = new Vector2(150f, 100f);
        private static readonly Vector2 BaseRelativeStartPos = new Vector2(0f, -150f);
        private static readonly Vector2 BaseRelativeEndPos = new Vector2(0f, 50f);

        private Vector2 PanelSize = BasePanelSize;
        private Vector2 RelativeStartPos = BaseRelativeStartPos;

        private SpriteFont LevelTextFont = null;

        private NineSlicedSprite PanelSprite = null;
        private Texture2D PanelTex = null;
        private SpriteFont PanelFont = null;
        private SpriteFont PanelFontLarge = null;

        private Sprite MovesIcon = null;
        private Sprite TimeIcon = null;

        public Vector2 Position { get; set; }
        private Vector2 StartPos = Vector2.Zero;
        private Vector2 EndPos = Vector2.Zero;

        private string LevelText = string.Empty;
        private string LevelDescriptionText = string.Empty;
        private string NumMovesText = string.Empty;
        private string TimeCompleteText = string.Empty;
        private bool Complete = false;
        private float DescriptionScale = 1f;

        private Color DescriptionColor = Color.Blue;

        private LevelPanelStates State = LevelPanelStates.FadingIn;

        private const double FadeTime = 400d;
        private double ElapsedTime = 0d;

        public OverworldLevelPanel()
        {
            PanelTex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);
            PanelFont = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont10px);
            PanelFontLarge = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont12px);

            PanelSprite = new NineSlicedSprite(PanelTex, ContentGlobals.PanelRect, 5, 5, 5, 5);

            MovesIcon = new Sprite(PanelTex, ContentGlobals.MovesIconRectSmall);
            TimeIcon = new Sprite(PanelTex, ContentGlobals.TimeIconRectSmall);

            Reset();
        }

        public void Reset()
        {
            ElapsedTime = 0d;
            State = LevelPanelStates.FadingIn;
        }

        public void Update()
        {
            if (State == LevelPanelStates.FadingIn)
            {
                UpdateFadingIn();
            }
        }

        private void UpdateFadingIn()
        {
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= FadeTime)
            {
                State = LevelPanelStates.Still;
                ElapsedTime = FadeTime;
                Position = EndPos;
            }
            else
            {
                Position = Interpolation.Interpolate(StartPos, EndPos, ElapsedTime / FadeTime, Interpolation.InterpolationTypes.CubicOut);
            }
        }

        public void UpdateLevelInfo(OverworldMapNode levelNode)
        {
            Reset();

            //Don't update anything if the node is empty
            if (levelNode.NodeType == MapNodeTypes.Empty)
            {
                return;
            }

            RelativeStartPos = BaseRelativeStartPos;

            PanelSize = BasePanelSize;

            LevelTextFont = PanelFontLarge;

            NumMovesText = string.Empty;
            TimeCompleteText = string.Empty;

            LevelText = levelNode.DisplayName;
            LevelDescriptionText = levelNode.Description;

            if (levelNode.NodeType == MapNodeTypes.Level)
            {
                SaveData.GetLevelData(DataHandler.saveData.LastPlayedWorld, levelNode.LevelID, out LevelData lvlData);

                Complete = (lvlData?.Complete == true);
                if (Complete == true)
                {
                    NumMovesText = lvlData.LvlStats.NumMoves.ToString();
                    TimeCompleteText = LevelGlobals.GetFormattedLevelTimeString(lvlData.LvlStats.LevelTime);
                }
                
                DescriptionColor = Color.Blue;
            }
            else if (levelNode.NodeType == MapNodeTypes.World)
            {
                LevelTextFont = PanelFont;
                DescriptionColor = Color.Black;
            }

            if (string.IsNullOrEmpty(NumMovesText) == true || string.IsNullOrEmpty(LevelText) == true)
            {
                PanelSize.Y -= 44f;
                RelativeStartPos.Y += 22f;
            }

            StartPos = levelNode.Position + RelativeStartPos;
            EndPos = StartPos + BaseRelativeEndPos;

            Position = StartPos;

            //Measure the level description to see if we should scale the text to fit in the level panel
            Vector2 size = PanelFont.MeasureString(LevelDescriptionText);

            //Use the scaled size that we draw this at for accuracy
            Vector2 scaledSize = size * BaseDescriptionScale;
            
            //Subtract double the margin, which accounts for both the left and the right
            Vector2 panelSize = PanelSize - new Vector2(PanelMargin * 2f, 0f);

            DescriptionScale = 1f;

            //If the text is larger than the panel, then we should scale the text accordingly
            if (scaledSize.X > panelSize.X)
            {
                DescriptionScale = panelSize.X / scaledSize.X;
            }
        }

        public void Render()
        {
            float alpha = (float)(ElapsedTime / FadeTime);

            //Don't render if the alpha is 0
            if (alpha == 0f)
            {
                return;
            }

            Vector2 panelOrigin = (PanelSize / 2);
            panelOrigin.Floor();
            Vector2 panelPos = Position - panelOrigin;

            //This value helps position the elements contained to be relative to the top of the panel
            Vector2 panelPosCenter = Position - new Vector2(0f, panelOrigin.Y);

            Color completeColor = Color.Black;
            if (Complete == false)
                completeColor = Color.Gray;

            Rectangle panelRect = new Rectangle(panelPos.ToPoint(), PanelSize.ToPoint());

            //Draw the panel
            for (int i = 0; i < PanelSprite.Slices; i++)
            {
                Rectangle nineRect = PanelSprite.GetRectForIndex(panelRect, i);

                RenderingManager.Instance.DrawSprite(PanelTex, nineRect, PanelSprite.Regions[i],
                    Color.White * alpha, 0f, Vector2.Zero, SpriteEffects.None, .3f);
            }

            //Draw the moves icon if we should
            if (string.IsNullOrEmpty(NumMovesText) == false)
            {
                RenderingManager.Instance.DrawSprite(MovesIcon.Tex, panelPosCenter + new Vector2(-13f, 52f + StartContentYOffset),
                    MovesIcon.SourceRect, Color.White * alpha, 0f, MovesIcon.GetOrigin(), Vector2.One, SpriteEffects.None, .3f);
            }

            //Draw the time icon if we should
            if (string.IsNullOrEmpty(TimeCompleteText) == false)
            {
                RenderingManager.Instance.DrawSprite(TimeIcon.Tex, panelPosCenter + new Vector2(-8f, 73f + StartContentYOffset),
                    TimeIcon.SourceRect, Color.White * alpha, 0f, TimeIcon.GetOrigin(), Vector2.One, SpriteEffects.None, .3f);
            }

            Vector2 lvlTextOrigin = LevelTextFont.GetCenterOrigin(LevelText);

            RenderingManager.Instance.spriteBatch.DrawString(LevelTextFont, LevelText, panelPosCenter + new Vector2(0f, StartContentYOffset + 3 + (int)(lvlTextOrigin.Y / 2)),
                Color.Black * alpha, 0f, lvlTextOrigin, LevelTextScale, SpriteEffects.None, .3f);

            Vector2 descriptionTextOrigin = PanelFont.GetCenterOrigin(LevelDescriptionText);

            RenderingManager.Instance.spriteBatch.DrawString(PanelFont, LevelDescriptionText, panelPosCenter + new Vector2(0f, 26f + StartContentYOffset)
                + new Vector2(0f, (int)(descriptionTextOrigin.Y / 2)), DescriptionColor * alpha, 0f, descriptionTextOrigin, BaseDescriptionScale * DescriptionScale,
                SpriteEffects.None, .3f);

            //Draw best moves text if we should
            if (string.IsNullOrEmpty(NumMovesText) == false)
            {
                Vector2 movesOrigin = PanelFont.GetOrigin(NumMovesText, 0f, .5f);

                RenderingManager.Instance.spriteBatch.DrawString(PanelFont, NumMovesText, panelPosCenter + new Vector2(6f, 51f + StartContentYOffset)
                    + new Vector2(0f, (int)(movesOrigin.Y / 2)), completeColor * alpha, 0f, movesOrigin, 1f, SpriteEffects.None, .3f);
            }

            //Draw best time text if we should
            if (string.IsNullOrEmpty(TimeCompleteText) == false)
            {
                Vector2 timeOrigin = PanelFont.GetOrigin(TimeCompleteText, 0f, .5f);

                RenderingManager.Instance.spriteBatch.DrawString(PanelFont, TimeCompleteText, panelPosCenter + new Vector2(6f, 73f + StartContentYOffset)
                    + new Vector2(0f, (int)(timeOrigin.Y / 2)), completeColor * alpha, 0f, timeOrigin, 1f, SpriteEffects.None, .3f);
            }
        }
    }
}
