/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// Contains information regarding a world.
    /// </summary>
    public struct WorldInfo
    {
        public int WorldID;
        public int LevelsCompleted;
        public int TotalLevels;
        public int TotalMoves;
        public double TotalTime;

        public WorldInfo(in int worldID, in int levelsCompleted, in int totalLevels, in int totalMoves, in double totalTime)
        {
            WorldID = worldID;
            LevelsCompleted = levelsCompleted;
            TotalLevels = totalLevels;
            TotalMoves = totalMoves;
            TotalTime = totalTime;
        }
    }
}