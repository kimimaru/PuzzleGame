/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// The types of level events used.
    /// </summary>
    public enum LevelEventTypes
    {
        /// <summary>
        /// Indicates the base events are used in the level.
        /// <para>This includes events such as completing the level when touching the portal.</para>
        /// </summary>
        Base,

        /// <summary>
        /// Indicates that custom events are used in the level. The base events will not be initialized.
        /// </summary>
        Custom
    }
}