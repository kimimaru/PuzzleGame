/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// The types of surfaces.
    /// </summary>
    public enum SurfaceTypes
    {
        /// <summary>
        /// Standard ground.
        /// </summary>
        Ground = 0,
       
        /// <summary>
        /// Mud surface - object movement speed is decreased.
        /// Blocks on this surface cannot be pushed by the player.
        /// </summary>
        Mud,
        
        /// <summary>
        /// Ice surface - objects moving on here slide in a single direction until hitting something or another surface.
        /// Blocks on this surface cannot be pulled by the player.
        /// </summary>
        Ice
    }
}