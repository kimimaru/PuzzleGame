/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// Information about the playable space.
    /// </summary>
    public struct PlayableSpace
    {
        /// <summary>
        /// The size of each tile in the space.
        /// </summary>
        public Vector2 TileSize;

        /// <summary>
        /// Half the size of each tile in the space.
        /// </summary>
        public Vector2 TileSizeHalf => (TileSize / 2);

        /// <summary>
        /// The width of the space in pixels.
        /// </summary>
        public int WidthInPixels;

        /// <summary>
        /// The height of the space in pixels.
        /// </summary>
        public int HeightInPixels;

        /// <summary>
        /// The bounds of the space.
        /// </summary>
        public RectangleF Bounds => new RectangleF(0f, 0f, WidthInPixels, HeightInPixels);

        public PlayableSpace(in Vector2 tileSize, in int widthInPixels, in int heightInPixels)
        {
            TileSize = tileSize;
            WidthInPixels = widthInPixels;
            HeightInPixels = heightInPixels;
        }

        /// <summary>
        /// Obtains a position of a tile given a column and row number.
        /// </summary>
        /// <param name="column">The column number of the tile.</param>
        /// <param name="row">The row number of the tile.</param>
        /// <returns>A <see cref="Vector2"/> representing the position of the tile at this column and row.</returns>
        public Vector2 GetPosFromColRow(in int column, in int row)
        {
            return new Vector2(column * TileSize.X, row * TileSize.Y);
        }

        /// <summary>
        /// Obtains a tile's column and row numbers, given a position.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="column">The returned column number.</param>
        /// <param name="row">The returned row number.</param>
        public void GetTileColRol(in Vector2 position, out int column, out int row)
        {
            column = (int)Math.Floor(position.X / TileSize.X);
            row = (int)Math.Floor(position.Y / TileSize.Y);
        }
    }
}