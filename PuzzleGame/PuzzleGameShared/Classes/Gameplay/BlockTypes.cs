/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// The types of blocks.
    /// </summary>
    public enum BlockTypes
    {
        Normal,
        Paired,
        Pipe
    }

    /// <summary>
    /// The types of block patterns.
    /// </summary>
    public enum BlockPatterns
    {
        /// <summary>
        /// None is reserved for grabbable objects that shouldn't go into spots.
        /// Some of these may include neutral blocks and movable switches.
        /// </summary>
        None,
        Square,
        Circle,
        Diamond,
        Triangle
    }
}