/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class Switch : ITransformable, ICollisionObj, IUpdateable, IRenderDepth
    {
        public static readonly Color IDColor = new Color(55, 55, 55, 255);

        public string ID { get; set; } = string.Empty;

        public PlayableContext Context { get; set; } = null;
        public Transform transform { get; private set; } = new Transform();
        public float RenderDepth { get; set; } = LevelGlobals.BaseSwitchRenderDepth;

        public Sprite sprite { get; private set; } = null;

        public ObjectTypes ObjectType => ObjectTypes.Switch;

        public CollisionTypes CollisionType { get; set; } = CollisionTypes.Trigger;

        public CollisionLayers CollisionLayer { get; set; } = CollisionLayers.Switch;

        public TriggerQueries TriggerQuery { get; private set; } = TriggerQueries.Constant;

        //protected Vector2 SpriteTopRight
        //{
        //    get
        //    {
        //        Rectangle spriteRect = sprite.SourceRect.Value;
        //        return new Vector2(transform.Position.X + ((spriteRect.Width / 2) * transform.Scale.X), transform.Position.Y - ((spriteRect.Height / 2) * transform.Scale.Y));
        //    }
        //}

        public virtual RectangleF CollisionRect
        {
            get
            {
                //return new RectangleF(transform.Position.X, transform.Position.Y, 1, 1);
                Rectangle spriteRect = sprite.SourceRect.Value;
                return new RectangleF(transform.Position.X - ((spriteRect.Width / 2) * transform.Scale.X),
                                      transform.Position.Y - ((spriteRect.Height / 2) * transform.Scale.Y),
                                      spriteRect.Width * transform.Scale.X, spriteRect.Height * transform.Scale.Y);
            }
        }

        public SwitchTypes SwitchType { get; protected set; } = SwitchTypes.Standard;

        public Direction PairedObjMoveDir { get; private set; } = Direction.Down;
        public int PairedObjMoveAmount { get; private set; } = 1;
        private Vector2 TileSize = Vector2.Zero;

        [Recordable(RecordableFlags.None)]
        protected bool Pressed = false;

        private double ToModifier = 1d;
        private double FromModifier = -1d;

        [Recordable(RecordableFlags.None)]
        public double Modifier = -1d;

        public double MoveTime = 300d;

        public bool DrawID = true;
        protected SpriteFont IDFont = null;
        protected Vector2 IDOrigin = Vector2.Zero;

        protected SoundEffect PressedSound = null;

        public Switch()
        {
            sprite = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.LevelObjectsTex), ContentGlobals.SwitchSpriteRect);
        }

        public Switch(in Vector2 position, in Vector2 objPosition, in Direction objMoveDir, in int objMoveAmount, in Vector2 tileSize,
            in double moveTime, in double toMod, in double fromMod)
            : this()
        {
            transform.Position = position;

            PairedObjMoveDir = objMoveDir;
            PairedObjMoveAmount = objMoveAmount;
            TileSize = tileSize;
            MoveTime = moveTime;
            ToModifier = toMod;
            FromModifier = fromMod;
        }

        public virtual void CleanUp()
        {

        }

        public virtual void Update()
        {
            
        }

        public virtual bool QueryTrigger(List<ICollisionObj> collisions)
        {
            RectangleF collRect = CollisionRect;

            //If collision and not pressed, press
            if (Pressed == false)
            {
                for (int i = 0; i < collisions.Count; i++)
                {
                    ICollisionObj coll = collisions[i];

                    if (coll != this && coll.ObjectType != ObjectTypes.Spot && coll.CollisionRect.Intersects(collRect) == true)
                    {
                        Pressed = true;
                        OnPressed();
                        return false;
                    }
                }
            }
            //If not collision and pressed, release
            else
            {
                for (int i = 0; i < collisions.Count; i++)
                {
                    ICollisionObj coll = collisions[i];

                    if (coll != this && coll.ObjectType != ObjectTypes.Spot && coll.CollisionRect.Intersects(collRect) == true)
                    {
                        return false;
                    }
                }

                Pressed = false;
                OnReleased();
            }

            return false;
        }

        public virtual void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            TileSize = context.LevelSpace.TileSize;

            if (initInfo.Properties.TryGetValue(LevelGlobals.IDKey, out string idVal) == true)
            {
                ID = idVal;

                IDFont = AssetManager.Instance.LoadFont(ContentGlobals.SpecialFont13px);
                IDOrigin = IDFont.GetCenterOrigin(ID);
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.ShowIDKey, out string showID) == true)
            {
                bool.TryParse(showID, out DrawID);
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.DirectionKey, out string directionStr) == true)
            {
                if (Enum.TryParse(directionStr, true, out Direction dir) == true)
                {
                    PairedObjMoveDir = dir;
                }
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.SwitchAmountKey, out string moveAmtStr) == true)
            {
                if (int.TryParse(moveAmtStr, out int moveAmt) == true)
                {
                    PairedObjMoveAmount = moveAmt;
                }
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.SwitchTimeKey, out string timeStr) == true)
            {
                if (double.TryParse(timeStr, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture,
                    out double moveTime) == true)
                {
                    MoveTime = moveTime;
                }
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.SwitchToModifierKey, out string toModStr) == true)
            {
                if (double.TryParse(toModStr, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture,
                    out double toMod) == true)
                {
                    ToModifier = toMod;
                }
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.SwitchFromModifierKey, out string fromModStr) == true)
            {
                if (double.TryParse(fromModStr, System.Globalization.NumberStyles.Any,
                    System.Globalization.CultureInfo.InvariantCulture, out double fromMod) == true)
                {
                    FromModifier = fromMod;
                }
            }

            //Check for loading the sound
            if (PressedSound == null)
            {
                bool loadSound = true;

                if (initInfo.Properties.TryGetValue(LevelGlobals.HasSoundKey, out string noSoundStr) == true)
                {
                    bool.TryParse(noSoundStr, out loadSound);
                }

                if (loadSound == true)
                {
                    PressedSound = AssetManager.Instance.LoadSound(ContentGlobals.SwitchPressedSound);
                }
            }

            transform.Position = initInfo.Position + context.LevelSpace.TileSizeHalf;
        }

        public virtual void PostInitialize(PlayableContext context)
        {

        }

        public virtual void OnPressed()
        {
            Modifier = ToModifier;

            if (PressedSound != null)
            {
                SoundManager.Instance.PlaySound(PressedSound, false, .6f);
            }
        }

        public virtual void OnReleased()
        {
            Modifier = FromModifier;
        }

        public void Render()
        {
            RenderingManager.Instance.DrawSprite(sprite.Tex, transform.Position, sprite.SourceRect, Color.White, transform.Rotation, sprite.GetOrigin(), transform.Scale, SpriteEffects.None, RenderDepth);
            //RenderingManager.Instance.spriteBatch.DrawHollowRect(AssetManager.Instance.LoadTexture(ContentGlobals.UITex), ContentGlobals.BoxRect, 
            //    CollisionRect, Color.Blue, RenderDepth + .01f, 1);

            //Don't draw the ID if we shouldn't
            if (DrawID == true)
            {
                RenderingManager.Instance.spriteBatch.DrawStringOutline(1f, Color.White * .8f, -.001f, -.002f, -.003f, -.004f, 
                    IDFont, ID, CollisionRect.TopRight, IDColor, 0f, IDOrigin, 1f, SpriteEffects.None, RenderDepth + .02f);
            }
        }
    }
}