/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class PipeBlock : Block
    {
        /// <summary>
        /// The direction pairs for Pipe Blocks.
        /// </summary>
        private enum PipeDirectionPairs
        {
            UpDown, LeftRight, UpRight, UpLeft, DownLeft, DownRight
        }

        public Direction EntranceDir { get; private set; } = Direction.Up;
        public Direction ExitDir { get; private set; } = Direction.Down;

        public override RectangleF CollisionRect
        {
            get
            {
                const int width = 32;
                const int height = 32;
                return new RectangleF(transform.Position.X - ((width / 2) * CollisionScale.X),
                                      transform.Position.Y - ((height / 2) * CollisionScale.Y),
                                      width * CollisionScale.X, height * CollisionScale.Y);
            }
        }

        private PipeDirectionPairs PipeDirPair = PipeDirectionPairs.UpDown;

        private Sprite FilledSprite = null;

        [Recordable(RecordableFlags.None)]
        public bool Filled = false;

        public PipeBlock()
        {
            BlockType = BlockTypes.Pipe;
        }

        public override void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            base.Initialize(context, initInfo);

            if (initInfo.Properties.TryGetValue(LevelGlobals.PipeBlockEntranceDirKey, out string entrancedirstr) == true)
            {
                if (Enum.TryParse(entrancedirstr, true, out Direction entrancedir) == true)
                {
                    EntranceDir = entrancedir;
                }
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.PipeBlockExitDirKey, out string exitdirstr) == true)
            {
                if (Enum.TryParse(exitdirstr, true, out Direction exitdir) == true)
                {
                    ExitDir = exitdir;
                }
            }

            string entranceExitStr = EntranceDir.ToString() + ExitDir.ToString();

            //Try to get the pipe pair type by the directions
            if (Enum.TryParse(entranceExitStr, true, out PipeDirPair) == false)
            {
                //If we didn't parse successfully, flip the exit and entrance directions and see if that does it
                string exitEntranceStr = ExitDir.ToString() + EntranceDir.ToString();

                if (Enum.TryParse(exitEntranceStr, true, out PipeDirPair) == false)
                {
                    //Default to this if we didn't succeed
                    PipeDirPair = PipeDirectionPairs.UpDown;
                }
            }

            sprite.SourceRect = GetBlockSourceRect(BlockType, InitGrabbable);
            FilledSprite = new Sprite(sprite.Tex, GetFilledSourceRect());
        }

        protected override void RenderBlock(in Color color)
        {
            Sprite spriteUsed = (Filled == false) ? sprite : FilledSprite;
            Vector2 origin = spriteUsed.GetOrigin();

            RenderingManager.Instance.DrawSprite(spriteUsed.Tex, transform.Position, spriteUsed.SourceRect, color, transform.Rotation,
                origin, transform.Scale, SpriteEffects.None, RenderDepth);
        }

        public override Rectangle GetBlockSourceRect(in BlockTypes blockType, in bool initGrabbable)
        {
            Rectangle rect = ContentGlobals.BlockPipeStartRect;
            rect.X += ((int)PipeDirPair * 32);

            if (initGrabbable == false)
            {
                rect.Y += ContentGlobals.BlockPipeUngrabbableYOffset;
            }

            return rect;
        }

        private Rectangle GetFilledSourceRect()
        {
            Rectangle rect = GetBlockSourceRect(BlockType, InitGrabbable);
            int enumLength = EnumUtility.GetValues<PipeDirectionPairs>.EnumValues.Length;

            rect.X += (enumLength * 32);
            return rect;
            //switch (PipeDirPair)
            //{
            //    default:
            //    case PipeDirectionPairs.UpDown: return new Rectangle(288, 96, 38, 32);
            //    case PipeDirectionPairs.UpRight: return new Rectangle(326, 96, 38, 32);
            //    case PipeDirectionPairs.UpLeft: return new Rectangle(364, 96, 38, 32);
            //    case PipeDirectionPairs.DownRight: return new Rectangle(402, 96, 38, 32);
            //    case PipeDirectionPairs.DownLeft: return new Rectangle(440, 96, 38, 32);
            //    case PipeDirectionPairs.LeftRight: return new Rectangle(480, 90, 32, 38);
            //}
        }
    }
}