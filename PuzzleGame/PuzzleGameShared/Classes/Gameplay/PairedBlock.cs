/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class PairedBlock : Block
    {
        public static readonly Color PairedIDColor = new Color(0, 0, 100, 255);

        private LevelGlobals.CollisionFilter BlockCollFilter = null;

        /// <summary>
        /// The paired ID of this paired block. This determines which paired blocks are linked.
        /// </summary>
        public string PairedID { get; private set; } = string.Empty;

        /// <summary>
        /// The block paired with this one.
        /// </summary>
        public PairedBlock PairedObj { get; private set; } = null;

        private bool ShowPairedID = true;
        private Vector2 PairedIDOrigin = Vector2.Zero;

        public PairedBlock()
        {
            BlockType = BlockTypes.Paired;
        }

        public override void CleanUp()
        {
            base.CleanUp();

            BlockCollFilter = null;
            IDFont = null;

            if (PairedObj != null)
            {
                Context.Player.BlockMovedEvent -= PairedObj.OnPlayerBlockPushed;
            }
        }

        protected override void OnGrab()
        {
            //When this block is grabbed, subscribe the paired block to this one's event
            if (PairedObj != null)
            {
                Context.Player.BlockMovedEvent -= PairedObj.OnPlayerBlockPushed;
                Context.Player.BlockMovedEvent += PairedObj.OnPlayerBlockPushed;
            }
        }

        protected override void OnRelease()
        {
            if (PairedObj != null)
            {
                Context.Player.BlockMovedEvent -= PairedObj.OnPlayerBlockPushed;
            }
        }

        private void OnPlayerBlockPushed(Block blockGrabbed, in Vector2 moveAmount, in double moveTime)
        {
            if (blockGrabbed == PairedObj)
            {
                //Don't move if this paired block is currently in another state
                if (BlockFSM != null)
                {
                    return;
                }

                //If this block is inactive, don't do anything
                if (Active == false)
                {
                    return;
                }

                //Check to see if you can move
                ICollisionObj coll = LevelGlobals.CheckCollision(moveAmount, CollisionRect, Context.LevelCollision, BlockCollFilter);

                if (coll == null)
                {
                    //Get the surface this block is on
                    SurfaceTypes surface = Context.SurfaceTiles.GetTileInRegion(CollisionRect);

                    //If on Ice, change to the ice movement state
                    if (surface == SurfaceTypes.Ice)
                    {
                        Vector2 moveDir = moveAmount;
                        moveDir.Normalize();

                        ChangeState(new BlockIceMoveState(this, moveDir));
                    }
                    //Otherwise move like normal
                    else
                    {
                        ChangeState(new BlockMoveState(this, moveAmount, moveTime, false));

                        if (EnumUtility.HasEnumVal((long)Context.Player.MoveSmoothingOptions, (long)PlayerSmoothingOptions.BlockMove) == true)
                        {
                            BlockFSM.Update();
                        }
                    }
                }
            }
        }

        public override void Update()
        {
            base.Update();
        }

        public override void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            base.Initialize(context, initInfo);

            if (initInfo.Properties.TryGetValue(LevelGlobals.PairedBlockIDKey, out string pairedid) == true)
            {
                PairedID = pairedid;
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.ShowIDKey, out string showpairedid) == true)
            {
                bool.TryParse(showpairedid, out ShowPairedID);
            }

            //Load the font if we should show the paired ID
            if (ShowPairedID == true)
            {
                IDFont = AssetManager.Instance.LoadFont(ContentGlobals.SpecialFont13px);
                PairedIDOrigin = IDFont.GetOrigin(PairedID, 1f, .5f);
            }
        }

        public override void PostInitialize(PlayableContext context)
        {
            base.PostInitialize(context);

            BlockCollFilter = BlockCollisionFilter;

            if (string.IsNullOrEmpty(PairedID) == true)
                return;

            //Find a block that should be paired with this one
            for (int i = 0; i < context.Blocks.Count; i++)
            {
                Block b = context.Blocks[i];
                if (b == this) continue;

                PairedBlock pBlock = b as PairedBlock;

                if (pBlock != null && PairedID == pBlock.PairedID)
                {
                    PairedObj = pBlock;
                    break;
                }
            }
        }

        public override void Render()
        {
            base.Render();

            //Display the Paired Block's ID on the upper-left if we should
            if (ShowPairedID == true)
            {
                RenderingManager.Instance.spriteBatch.DrawStringOutline(1f, Color.White * .8f, -.001f, -.002f, -.003f, -.004f,
                IDFont, PairedID, CollisionRect.TopRight, PairedIDColor, 0f, PairedIDOrigin, 1f, SpriteEffects.None, RenderDepth + .02f);
            }
        }

        protected override void RenderOverlay()
        {
            if (BlockState == BlockStates.CorrectSpot || BlockState == BlockStates.WrongSpot
                || (PairedObj != null && (Grabbed == true || PairedObj.Grabbed == true)))
            {
                Color overlayColor = BaseSpotOverlayColor;
                Sprite sprite = SpotOverlay.SpriteToChange;

                if (BlockState == BlockStates.CorrectSpot)
                {
                    overlayColor = BaseSpotOverlayColor;
                }
                else if (BlockState == BlockStates.WrongSpot)
                {
                    overlayColor = BaseSpotOverlayColor;
                    sprite = SpotOverlayIncorrect.SpriteToChange;
                }
                else if (PairedObj != null && (PairedObj.Grabbed == true || Grabbed == true))
                {
                    overlayColor = Color.DeepSkyBlue;
                }

                overlayColor *= SpotOverlayAlpha;

                RenderingManager.Instance.DrawSprite(sprite.Tex, transform.Position, sprite.SourceRect, overlayColor, 0f, sprite.GetOrigin(),
                    transform.Scale, SpriteEffects.None, RenderDepth + .01f);
            }
        }

        private bool BlockCollisionFilter(ICollisionObj collisionObj)
        {
            //Don't check for the paired object for the paired movement
            //This allows the block to move when adjacent to the paired one
            return (collisionObj != this && collisionObj != PairedObj
                && collisionObj.CollisionType == CollisionTypes.Blocking && collisionObj.ObjectType != ObjectTypes.Player
                && collisionObj.CollisionLayer != CollisionLayers.BlockExcept);
        }

        public override Rectangle GetBlockSourceRect(in BlockTypes blockType, in bool initGrabbable)
        {
            if (initGrabbable == false)
            {
                return ContentGlobals.BlockUngrabbableRect;
            }

            return ContentGlobals.BlockPairedRect;
        }
    }
}