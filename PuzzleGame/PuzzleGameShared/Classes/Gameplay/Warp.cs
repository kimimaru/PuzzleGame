/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class Warp : ITransformable, ICollisionObj, IUpdateable, IRenderDepth
    {
        public delegate void OnWarpedObjectOut(ICollisionObj collisionObj);
        public event OnWarpedObjectOut WarpedObjectOutEvent = null;

        public delegate void OnWarpedObjectIn(ICollisionObj collisionObj);
        public event OnWarpedObjectIn WarpedObjectInEvent = null;

        private LevelGlobals.CollisionFilter WarpCollisionFilter = null;

        public enum WarpStates
        {
            In, Out
        }

        public static readonly Color WarpUnblockedColor = Color.White;
        public static readonly Color WarpBlockedColor = new Color(160, 160, 160, 255);

        public static readonly Color IDColor = new Color(80, 80, 80, 255);

        public string ID { get; set; } = string.Empty;

        public PlayableContext Context { get; set; } = null;
        public Transform transform { get; private set; } = new Transform();
        public float RenderDepth { get; set; } = LevelGlobals.BaseWarpRenderDepth;

        private SimpleAnimation WarpAnim = null;
        public Sprite sprite { get; private set; } = null;

        private Sprite WarpPosSprite = null;
        //private SimpleAnimation WarpPosAnim = null;

        public ObjectTypes ObjectType => ObjectTypes.Warp;

        public CollisionTypes CollisionType { get; set; } = CollisionTypes.Trigger;

        public CollisionLayers CollisionLayer { get; set; } = CollisionLayers.Warp;

        public TriggerQueries TriggerQuery { get; private set; } = TriggerQueries.Constant;

        public RectangleF CollisionRect
        {
            get
            {
                Rectangle spriteRect = sprite.SourceRect.Value;
                return new RectangleF(transform.Position.X - ((spriteRect.Width / 2) * transform.Scale.X),
                                      transform.Position.Y - ((spriteRect.Height / 2) * transform.Scale.Y),
                                      spriteRect.Width * transform.Scale.X, spriteRect.Height * transform.Scale.Y);
            }
        }

        public RectangleF WarpDestRect
        {
            get
            {
                Rectangle spriteRect = sprite.SourceRect.Value;
                return new RectangleF(WarpPos.X - ((spriteRect.Width / 2) * transform.Scale.X),
                                      WarpPos.Y - ((spriteRect.Height / 2) * transform.Scale.Y),
                                      spriteRect.Width * transform.Scale.X, spriteRect.Height * transform.Scale.Y);
            }
        }

        /// <summary>
        /// The time it takes to warp an object.
        /// </summary>
        public double WarpDuration => WarpTime;

        public Vector2 WarpPos { get; private set; } = Vector2.Zero;

        [Recordable(RecordableFlags.None)]
        private ICollisionObj WarpingObject = null;
        [Recordable(RecordableFlags.None)]
        private CollisionTypes OrigWarpingObjType = CollisionTypes.Trigger;
        [Recordable(RecordableFlags.None)]
        private WarpStates WarpState = WarpStates.In;
        [Recordable(RecordableFlags.None)]
        private CollisionObject TemporaryCollision = null;

        [Recordable(RecordableFlags.None)]
        private Vector2 OrigWarpingObjScale = Vector2.Zero;

        [Recordable(RecordableFlags.None)]
        private Vector2 WarpingObjNewScale = Vector2.Zero;

        [Recordable(RecordableFlags.None)]
        private double ElapsedTime = 0d;

        [Recordable(RecordableFlags.None)]
        private double ColorElapsedTime = 0d;

        private const double WarpColorTime = 300d;
        private const double WarpTime = 400d;

        private SoundEffect WarpUpSound = null;
        private SoundEffect WarpDownSound = null;

        private Color WarpPosColor = Color.White;

        /// <summary>
        /// Tells if the warp's destination is blocked.
        /// </summary>
        [Recordable(RecordableFlags.None)]
        public bool WarpDestBlocked { get; set; } = false;

        /// <summary>
        /// Tells whether to draw the destination warp.
        /// This is used as an optimization to ensure destinations aren't drawn over each other multiple times.
        /// </summary>
        public bool DrawWarpDest = true;

        /// <summary>
        /// Tells whether to draw the ID over the destination warp.
        /// This is used as an optimization to ensure the same IDs aren't drawn over each other multiple times.
        /// </summary>
        public bool DrawWarpDestID = true;

        /// <summary>
        /// Tells whether to halt updates regarding warping the current object.
        /// </summary>
        public bool HaltWarpUpdate = false;

        public bool HasSound = true;

        private SpriteFont IDFont = null;
        private Vector2 IDOrigin = Vector2.Zero;

        [Recordable(RecordableFlags.RecordFields | RecordableFlags.AvoidSet)]
        public ParticleEngine WarpParticles { get; private set; } = null;

        //public SimpleAnimation WarpVeilAnim = null;

        public Warp()
        {
            
        }

        public Warp(in Vector2 position, in Vector2 warpPos) : this()
        {
            transform.Position = position;

            SetWarpDestination(warpPos);
        }

        public void CleanUp()
        {
            WarpCollisionFilter = null;
            WarpedObjectOutEvent = null;
            WarpedObjectInEvent = null;
        }

        public void Update()
        {
            ColorElapsedTime = UtilityGlobals.Clamp(ColorElapsedTime + ((WarpDestBlocked == true) ? Time.ElapsedTime.TotalMilliseconds
                : -Time.ElapsedTime.TotalMilliseconds), 0d, WarpColorTime);

            WarpAnim.Update();
            //WarpPosAnim.Update();

            WarpParticles.Position = transform.Position;
            WarpParticles.Update();
            //WarpVeilAnim.Update();

            if (WarpingObject == null || HaltWarpUpdate == true)
            {
                return;
            }

            //Handle warping the object
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (WarpState == WarpStates.In)
            {
                WarpingObject.transform.Scale = Interpolation.Interpolate(OrigWarpingObjScale, WarpingObjNewScale, ElapsedTime / WarpDuration,
                    Interpolation.InterpolationTypes.CubicIn);
                //WarpingObject.transform.Position = Interpolation.Interpolate(transform.Position, WarpInEndPos, ElapsedTime / WarpDuration,
                //    Interpolation.InterpolationTypes.CubicIn);

                if (ElapsedTime >= WarpDuration)
                {
                    WarpingObject.transform.Position = WarpPos;
                    WarpingObject.transform.Scale = WarpingObjNewScale;
                    ElapsedTime = 0d;
                    SetWarpState(WarpStates.Out);

                    //Invoke the event
                    WarpedObjectInEvent?.Invoke(WarpingObject);

                    if (HasSound == true)
                    {
                        PlayWarpOutSound();
                    }
                }
            }
            else if (WarpState == WarpStates.Out)
            {
                WarpingObject.transform.Scale = Interpolation.Interpolate(WarpingObjNewScale, OrigWarpingObjScale, ElapsedTime / WarpDuration,
                    Interpolation.InterpolationTypes.CubicOut);
                //WarpingObject.transform.Position = Interpolation.Interpolate(WarpOutStartPos, WarpPos, ElapsedTime / WarpDuration,
                //    Interpolation.InterpolationTypes.CubicOut);

                if (ElapsedTime >= WarpDuration)
                {
                    WarpingObject.transform.Scale = OrigWarpingObjScale;
                    //WarpingObject.transform.Position = WarpPos;
                    ElapsedTime = 0d;
                    SetWarpState(WarpStates.In);

                    //Take the player out of the warp state if the player is the one being warped
                    if (WarpingObject.ObjectType == ObjectTypes.Player)
                    {
                        Echidna player = (Echidna)WarpingObject;
                        player.ChangeState(new IdleState());
                    }
                    else if (WarpingObject.ObjectType == ObjectTypes.Block)
                    {
                        //Take the block out of the warp state if it's the one being warped
                        Block block = (Block)WarpingObject;
                        block.ChangeState(null);
                        block.UpdateData();
                    }
                    
                    //Remove the temporary collision if it was added
                    if (TemporaryCollision != null)
                    {
                        Context.RemoveObject(TemporaryCollision);
                        TemporaryCollision = null;
                    }

                    //Change back to the original collision type
                    WarpingObject.CollisionType = OrigWarpingObjType;

                    //Invoke the event
                    WarpedObjectOutEvent?.Invoke(WarpingObject);

                    WarpingObject = null;
                }
            }
        }

        public bool QueryTrigger(List<ICollisionObj> collisions)
        {
            //Don't query if we're already warping something
            if (WarpingObject != null) return false;

            //If the warp isn't blocked
            if (WarpDestBlocked == false)
            {
                //Check if the destination is blocked
                for (int i = 0; i < collisions.Count; i++)
                {
                    ICollisionObj collision = collisions[i];
                    if (collision.CollisionType != CollisionTypes.Blocking && collision.ObjectType != ObjectTypes.Player) continue;

                    RectangleF objRect = collision.CollisionRect;

                    //The destination is blocked; return
                    if (objRect.Intersects(WarpDestRect) == true)
                    {
                        WarpDestBlocked = true;
                        WarpParticles.Stop(false);
                        return false;
                    }
                }

                //The destination isn't blocked, so see if anything touches the warp
                for (int i = 0; i < collisions.Count; i++)
                {
                    ICollisionObj collision = collisions[i];
                    if (collision.CollisionType != CollisionTypes.Blocking && collision.ObjectType != ObjectTypes.Player) continue;

                    //Make sure any objects are within a certain distance before warping them
                    if (UtilityGlobals.IsApproximate(collision.transform.Position, transform.Position, .01f) == false)
                    {
                        continue;
                    }

                    //Make sure any non-player objects are the same size or smaller than the warp before warping them
                    /* NOTE: Make this apply only to blocks - it still applies to other objects the warp can teleport, such as moles
                     * If warps shouldn't teleport these objects, exclude them from the collision check */
                    Vector2 scaleDiff = transform.Scale - collision.transform.Scale;
                    if (collision.ObjectType != ObjectTypes.Player &&
                        scaleDiff.X < 0f && scaleDiff.Y < 0f)
                    {
                        continue;
                    }

                    //One more check - see if this object would collide with anything at the warp point; if so, don't warp
                    //Use the collision rectangle at the warp point's position
                    RectangleF objRect = GetCollisionRectAtWarpPos(collision.CollisionRect);

                    //Check for collisions at the warp point
                    ICollisionObj coll = LevelGlobals.CheckCollision(Vector2.Zero, objRect, collisions, WarpCollisionFilter);

                    //No collision, so we're good to warp
                    if (coll == null)
                    {
                        WarpObject(collision, objRect);

                        break;
                    }
                }
            }
            //Don't query if we're already warping something
            else if (WarpingObject == null)
            {
                //Check if the destination is no longer blocked
                for (int i = 0; i < collisions.Count; i++)
                {
                    ICollisionObj collision = collisions[i];
                    if (collision.CollisionType != CollisionTypes.Blocking && collision.ObjectType != ObjectTypes.Player) continue;

                    RectangleF objRect = collision.CollisionRect;

                    //The destination is blocked; return
                    if (objRect.Intersects(WarpDestRect) == true)
                    {
                        return false;
                    }
                }

                WarpDestBlocked = false;
                WarpParticles.Resume();
                return false;
            }

            return false;
        }

        public void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            int warpX = 0;
            int warpY = 0;

            if (initInfo.Properties.TryGetValue(LevelGlobals.IDKey, out string idVal) == true)
            {
                ID = idVal;
            }

            //Specify tile X and tile Y
            if (initInfo.Properties.TryGetValue(LevelGlobals.WarpTileXKey, out string tileXVal) == true)
            {
                int.TryParse(tileXVal, out warpX);
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.WarpTileYKey, out string tileYVal) == true)
            {
                int.TryParse(tileYVal, out warpY);
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.ScaleKey, out string scaleStr) == true)
            {
                if (float.TryParse(scaleStr, out float scale) == true)
                {
                    transform.Scale = new Vector2(scale);
                }
            }

            //Check for loading the sounds
            if (initInfo.Properties.TryGetValue(LevelGlobals.HasSoundKey, out string noSoundStr) == true)
            {
                bool.TryParse(noSoundStr, out HasSound);
            }

            bool useAltAnim = false;
            if (initInfo.Properties.TryGetValue(LevelGlobals.WarpAltAnimKey, out string worldAltAnimStr) == true)
            {
                bool.TryParse(worldAltAnimStr, out useAltAnim);
            }

            WarpUpSound = AssetManager.Instance.LoadSound(ContentGlobals.WarpSound);
            WarpDownSound = WarpUpSound;

            SetWarpDestination(context.LevelSpace.GetPosFromColRow(warpX, warpY) + (context.LevelSpace.TileSizeHalf * transform.Scale));

            transform.Position = initInfo.Position + (context.LevelSpace.TileSizeHalf * transform.Scale);

            WarpCollisionFilter = CheckCollisionFilter;

            DefineAnimations(useAltAnim);

            Sprite particleSprite = new Sprite(sprite.Tex, GetParticleRect(useAltAnim));
            WarpParticles = new ParticleEngine(7, transform.Position, particleSprite, RenderDepth + .005f, 200d, 300d);
            WarpParticles.MinPositionOffset = Vector2.Zero;
            WarpParticles.MaxPositionOffset = Vector2.Zero;
            WarpParticles.MinAngularVelocity = WarpParticles.MaxAngularVelocity = 0f;
            WarpParticles.MinVelocity = new Vector2(0f, -.5f * 60f);
            WarpParticles.MaxVelocity = new Vector2(0f, -.5f * 60f);
            WarpParticles.MinParticleLife = 600d;
            WarpParticles.MaxParticleLife = 600d;
            WarpParticles.MinInitScale = transform.Scale;
            WarpParticles.MaxInitScale = transform.Scale;
            WarpParticles.EmissionRate = 134d;
            WarpParticles.ParticleColor = Color.White;
            WarpParticles.UseColorOverTime = true;
            WarpParticles.UseFloatOrigin = false;
            WarpParticles.ColorOverTime = Color.White * 0f;
            WarpParticles.PrewarmParticles();
        }

        public void PostInitialize(PlayableContext context)
        {
            int selfIndex = context.Warps.Count;

            //Check to see if any other warps have the same ID; if so, don't draw the destination ID for this one
            for (int i = 0; i < context.Warps.Count; i++)
            {
                Warp warp = context.Warps[i];

                if (warp == this)
                {
                    selfIndex = i;
                    continue;
                }

                //If this warp has the same destination and is after, tell this warp not to draw its destination or ID
                if (warp.WarpPos == WarpPos && i > selfIndex)
                {
                    DrawWarpDest = false;
                    DrawWarpDestID = false;
                    break;
                }

                //If this warp has the same ID and is after, tell this warp not to draw its ID at the destination
                if (string.IsNullOrEmpty(ID) == false && warp.ID == ID && i > selfIndex)
                {
                    DrawWarpDestID = false;
                    break;
                }
            }

            IDFont = AssetManager.Instance.LoadFont(ContentGlobals.SpecialFont13px);
            IDOrigin = IDFont.GetCenterOrigin(ID);
        }

        private void DefineAnimations(in bool altAnim)
        {
            Rectangle animFrame2Rect = ContentGlobals.WarpAnim2Rect;
            Rectangle animFrame3Rect = ContentGlobals.WarpAnim3Rect;

            if (altAnim == true)
            {
                animFrame2Rect = ContentGlobals.WarpWorldAnim2Rect;
                animFrame3Rect = ContentGlobals.WarpWorldAnim3Rect;
            }

            sprite = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.LevelObjectsTex), ContentGlobals.WarpAnim1Rect);
            WarpAnim = new SimpleAnimation(sprite, AnimTypes.Looping,
                new AnimationFrame(ContentGlobals.WarpAnim1Rect, 80d),
                new AnimationFrame(animFrame2Rect, 80d),
                new AnimationFrame(animFrame3Rect, 80d),
                new AnimationFrame(animFrame2Rect, 80d));

            WarpPosSprite = new Sprite(sprite.Tex, ContentGlobals.WarpDestSpriteRect);
            //WarpPosAnim = new SimpleAnimation(new Sprite(sprite.Tex, ContentGlobals.WarpDestSpriteRect), AnimTypes.Looping,
            //    new AnimationFrame(ContentGlobals.WarpDestSpriteRect, 80d),
            //    new AnimationFrame(new Rectangle(27, 94, 21, 21), 80d));

            //WarpVeilAnim = new SimpleAnimation(new Sprite(sprite.Tex, new Rectangle(275, 278, 21, 30), new Vector2(.5f, 1f)), AnimTypes.Looping,
            //    new AnimationFrame(new Rectangle(275, 278, 21, 30), 106.67d, new Vector2(.5f, 1f)),
            //    new AnimationFrame(new Rectangle(296, 278, 21, 30), 106.67d, new Vector2(.5f, 1f)),
            //    new AnimationFrame(new Rectangle(317, 278, 21, 30), 106.67d, new Vector2(.5f, 1f)),
            //    new AnimationFrame(new Rectangle(296, 278, 21, 30), 106.67d, new Vector2(.5f, 1f)));
        }

        private Rectangle GetParticleRect(in bool altAnim)
        {
            if (altAnim == false)
            {
                return ContentGlobals.WarpParticleRect;
            }

            return ContentGlobals.WarpWorldParticleRect;
        }

        /// <summary>
        /// Sets the position of the warp's destination.
        /// </summary>
        /// <param name="newWarpPos">The new position of the warp's destination.</param>
        public void SetWarpDestination(in Vector2 newWarpPos)
        {
            WarpPos = newWarpPos;
        }

        /// <summary>
        /// Retrieves the collision rectangle an object would have at the warp's destination.
        /// </summary>
        /// <param name="origObjectRect">The original object's collision rectangle.</param>
        /// <returns>A collision rectangle at the warp's destination.</returns>
        public RectangleF GetCollisionRectAtWarpPos(in RectangleF origObjectRect)
        {
            RectangleF objRect = origObjectRect;
            objRect.X = WarpPos.X - (objRect.Width / 2);
            objRect.Y = WarpPos.Y - (objRect.Height / 2);

            return objRect;
        }

        public void PlayWarpInSound()
        {
            SoundManager.Instance.PlaySound(WarpUpSound, false, .5f);
        }

        public void PlayWarpOutSound()
        {
            SoundManager.Instance.PlaySound(WarpDownSound, false, .5f);
        }

        /// <summary>
        /// Sets the warp state of the warp.
        /// </summary>
        /// <param name="warpState">The warp state.</param>
        public void SetWarpState(in WarpStates warpState)
        {
            WarpState = warpState;
        }

        /// <summary>
        /// Sets the progress as a percentage for warping an object in the warp's current warp state.
        /// </summary>
        /// <param name="percentage">The percentage of the warp progress.</param>
        public void SetWarpProgress(in double percentage)
        {
            ElapsedTime = UtilityGlobals.Clamp(WarpDuration * percentage, 0d, WarpDuration);
        }

        /// <summary>
        /// Manually tells the warp to warp an object. Use this only when manually controlling warps.
        /// </summary>
        /// <param name="collisionObj">The object to warp.</param>
        public void WarpObject(ICollisionObj collisionObj, RectangleF objRect)
        {
            //Warp
            WarpingObject = collisionObj;
            WarpingObject.transform.Position = transform.Position;

            OrigWarpingObjScale = WarpingObject.transform.Scale;
            WarpingObjNewScale = new Vector2(0f, OrigWarpingObjScale.Y + 2f);

            //If a block is being warped and is not grabbable, make it grabbable
            //This makes blocks linked to switches grabbable
            if (WarpingObject.ObjectType == ObjectTypes.Block)
            {
                Block b = (Block)WarpingObject;

                //Release the block and put it in a blank state
                b.Release();

                if (b.Grabbable == false)
                {
                    b.Grabbable = true;
                    b.UpdateData();
                }

                b.ChangeState(new BlockBlankState(b));
            }

            OrigWarpingObjType = WarpingObject.CollisionType;

            //Turn to a trigger to disable collisions while it's moving
            WarpingObject.CollisionType = CollisionTypes.Trigger;

            SetWarpState(WarpStates.In);
            ElapsedTime = 0d;

            //If this is the player, put the player in a warping state
            if (WarpingObject == Context.Player)
            {
                Context.Player.ChangeState(new WarpState());
            }
            
            //Add temporary collision where the warp is to block the warp until the object arrives at the destination
            TemporaryCollision = new CollisionObject(objRect.TopLeft, objRect.Size, CollisionTypes.Blocking, CollisionLayers.Wall);
            Context.AddObject(TemporaryCollision);

            if (HasSound == true)
            {
                PlayWarpInSound();
            }
        }

        public void Render()
        {
            Rectangle? sourceRect = sprite.SourceRect;
            //If the destination is blocked, show only the first frame of the warp's animation
            //This keeps the timer consistent since the animation will still be updated
            if (WarpDestBlocked == true)
            {
                sourceRect = WarpAnim.GetFrame(0).DrawRegion;
            }

            Color warpColor = Interpolation.Interpolate(WarpUnblockedColor, WarpBlockedColor, ColorElapsedTime / WarpColorTime, Interpolation.InterpolationTypes.Linear);

            //Render warp
            RenderingManager.Instance.DrawSprite(sprite.Tex, transform.Position, sourceRect, warpColor, transform.Rotation, sprite.GetOrigin(),
                transform.Scale, SpriteEffects.None, RenderDepth);

            //Render warp destination
            if (DrawWarpDest == true)
            {
                //Rectangle warpPosRect = WarpPosAnim.GetFrame(0).DrawRegion;
                //If an object is being warped, show more frames of the animation
                //if (WarpingObject != null)
                //{
                //    warpPosRect = WarpPosAnim.CurFrame.DrawRegion;
                //}

                //Show where the warp leads
                RenderingManager.Instance.DrawSprite(WarpPosSprite.Tex, WarpPos, WarpPosSprite.SourceRect, WarpPosColor, 0f,
                    WarpPosSprite.GetOrigin(), transform.Scale, SpriteEffects.None, RenderDepth - .01f);
                //RenderingManager.Instance.DrawSprite(WarpPosAnim.SpriteToChange.Tex, WarpPos, warpPosRect, WarpPosColor, 0f, WarpPosAnim.SpriteToChange.GetOrigin(),
                //    transform.Scale, SpriteEffects.None, RenderDepth - .01f);
            }

            WarpParticles.Render();

            //float alpha = (float)((1d - (ColorElapsedTime / WarpColorTime)) * .4d);
            //
            //if (alpha > 0f)
            //{
            //    Sprite wSprite = WarpVeilAnim.SpriteToChange;
            //    RenderingManager.Instance.DrawSprite(wSprite.Tex, new Vector2(transform.Position.X, CollisionRect.Bottom), wSprite.SourceRect,
            //        Color.White * alpha, 0f, wSprite.GetOrigin(), transform.Scale, SpriteEffects.None, RenderDepth + .006f);
            //}

            //If the ID is defined, render text for it next to the warp and destination to show where each one leads
            if (string.IsNullOrEmpty(ID) == false)
            {
                Vector2 topRight = CollisionRect.TopRight;
                Vector2 offset = topRight - transform.Position;

                //Show a small number by the warp and warp exit
                RenderingManager.Instance.spriteBatch.DrawStringOutline(1f, Color.White * .8f, -.001f, -.002f, -.003f, -.004f,
                    IDFont, ID, topRight, IDColor, 0f, IDOrigin, 1f, SpriteEffects.None, RenderDepth + .01f);

                //Don't draw the destination ID if we shouldn't
                if (DrawWarpDestID == true)
                {
                    RenderingManager.Instance.spriteBatch.DrawStringOutline(1f, Color.White * .8f, -.001f, -.002f, -.003f, -.004f, 
                        IDFont, ID, WarpPos + offset, IDColor, 0f, IDOrigin, 1f, SpriteEffects.None, RenderDepth + .01f);
                }
            }

            //RenderingManager.Instance.spriteBatch.DrawHollowRect(AssetManager.Instance.LoadTexture(ContentGlobals.UITex), ContentGlobals.BoxRect,
            //    CollisionRect, Color.Blue, RenderDepth + .04f, 1);
        }

        private bool CheckCollisionFilter(ICollisionObj collisionObj)
        {
            return (collisionObj != WarpingObject &&
                (collisionObj.CollisionType == CollisionTypes.Blocking || collisionObj.ObjectType == ObjectTypes.Player));
        }
    }
}