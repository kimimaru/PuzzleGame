/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Moles that throw rocks in a direction. The rock trajectories can be manipulated through Pipe Blocks.
    /// </summary>
    public class RockShooter : ICollisionObj, IUpdateable, IRenderDepth, IDirectional, ITintable
    {
        public string ID { get; set; } = string.Empty;

        public PlayableContext Context { get; set; } = null;
        public Transform transform { get; private set; } = new Transform();

        public ObjectTypes ObjectType { get; private set; } = ObjectTypes.RockShooter;

        public CollisionTypes CollisionType { get; set; } = CollisionTypes.Blocking;

        public CollisionLayers CollisionLayer { get; private set; } = CollisionLayers.Wall;

        public TriggerQueries TriggerQuery { get; private set; } = TriggerQueries.None;

        public RectangleF CollisionRect
        {
            get
            {
                Rectangle spriteRect = sprite.SourceRect.Value;
                return new RectangleF(transform.Position.X - (12 * transform.Scale.X),
                                      transform.Position.Y - (12 * transform.Scale.Y),
                                      24 * transform.Scale.X, 24 * transform.Scale.Y);
            }
        }

        public float RenderDepth { get; set; } = LevelGlobals.BaseRockShooterRenderDepth;

        [Recordable(RecordableFlags.None)]
        public Color TintColor { get; set; } = Color.White;

        public Sprite sprite { get; private set; } = null;

        [Recordable(RecordableFlags.RecordFields | RecordableFlags.AvoidSet)]
        public AnimManager AnimationManager { get; private set; } = null;

        public Direction FacingDir { get; set; } = Direction.Down;

        [Recordable(RecordableFlags.RecordFields | RecordableFlags.AvoidSet)]
        public Rock RockShot { get; private set; } = null;

        [Recordable(RecordableFlags.None)]
        private bool LaunchedRock = false;

        [Recordable(RecordableFlags.None)]
        private bool HitByRock = false;

        private const double FadeTime = 600d;
        private const double ThrowAnimTimeOffset = 240d;
        private const double WaitPlayThrowAnimTime = WaitShootTime - ThrowAnimTimeOffset;
        private const double WaitShootTime = 1500d;

        private double shootTime = WaitShootTime;
        private double ThrowAnimWaitTime = WaitPlayThrowAnimTime;

        public double ShootTime
        {
            get => shootTime;
            set
            {
                shootTime = value;
                ThrowAnimWaitTime = shootTime - ThrowAnimTimeOffset;
            }
        }

        [Recordable(RecordableFlags.None)]
        private double ElapsedTime = 0d;

        private const double RedBlinkTime = 34d;

        public bool HaltThrow = false;

        private const double ShakeChangeTime = 25d;
        private const double ShakeTime = 300d;

        public RockShooter()
        {

        }

        public void CleanUp()
        {
            RockShot.HitCollisionEvent -= OnRockHitCollision;
            RockShot.CleanUp();
        }

        public void Update()
        {
            AnimationManager.CurrentAnim.Update();

            RockShot.Update();

            if (HitByRock == true)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
                
                if (ElapsedTime >= FadeTime)
                {
                    TintColor = Color.Transparent;

                    //Reset the camera
                    if (Context.LevelCamera != null)
                    {
                        Context.LevelCamera.SetTranslation(Context.LevelCamera.DefaultPosition);
                    }

                    //Remove the rock shooter and clean it up
                    Context.RemoveObject(this);

                    CleanUp();
                }
                else
                {
                    //Make the mole blink red while fading out
                    float alpha = Interpolation.Interpolate(1f, 0f, ElapsedTime / FadeTime, Interpolation.InterpolationTypes.Linear);

                    double time = UtilityGlobals.PingPong(ElapsedTime / RedBlinkTime, 0d, 1d);
                    Color color = Interpolation.Interpolate(Color.White, Color.Red, time, Interpolation.InterpolationTypes.Linear);

                    TintColor = color * alpha;
                    
                    //Shake the camera when the rock shooter is hit
                    //The index used is based on the time
                    if (Context.LevelCamera != null)
                    {
                        if (ElapsedTime < ShakeTime)
                        {
                            int camIndex = (int)(ElapsedTime / ShakeChangeTime) % LevelGlobals.StandardCameraShakes.Length;

                            Context.LevelCamera.LookAtAndClamp(Context.LevelCamera.DefaultPosition + LevelGlobals.StandardCameraShakes[camIndex], Context.LevelSpace.Bounds);
                        }
                        //Reset the camera's position for one shake after the last shake is done
                        //Multiply the shake change time by 2 here in case the elapsed frame time is over the shake change time
                        else if (ElapsedTime >= ShakeTime && ElapsedTime < (ShakeTime + (ShakeChangeTime * 2d)))
                        {
                            Context.LevelCamera.SetTranslation(Context.LevelCamera.DefaultPosition);
                        }
                    }
                }
            }
            else if (LaunchedRock == false && HaltThrow == false)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                if (ElapsedTime >= ThrowAnimWaitTime && RockShot.PipeInside == null)
                {
                    AnimationManager.PlayAnimationIfDiff(AnimationGlobals.MoleAnimations.Throw);
                }

                if (ElapsedTime >= ShootTime)
                {
                    ElapsedTime = ShootTime;
                    LaunchedRock = true;
                    AnimationManager.PlayAnimation(AnimationGlobals.Idle);

                    RockShot.Launch();
                }
            }
        }

        public void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            if (initInfo.Properties.TryGetValue(LevelGlobals.DirectionKey, out string directionStr) == true)
            {
                if (Enum.TryParse(directionStr, true, out Direction shootDir) == true)
                {
                    FacingDir = shootDir;
                }
            }

            bool hurtsPlayer = false;

            if (initInfo.Properties.TryGetValue(LevelGlobals.RockShooterHurtKey, out string hurtStr) == true)
            {
                bool.TryParse(hurtStr, out hurtsPlayer);
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.RockShooterThrowRockKey, out string throwRockStr) == true)
            {
                if (bool.TryParse(throwRockStr, out bool throwsRock) == true)
                {
                    HaltThrow = !throwsRock;
                }
            }

            transform.Position = initInfo.Position + (context.LevelSpace.TileSizeHalf * transform.Scale);
            transform.Scale = new Vector2(1f, 1f);

            sprite = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.LevelObjectsTex), ContentGlobals.MoleIdleAnimURect);

            AnimationManager = new AnimManager(sprite, 3);
            DefineAnimations();
            AnimationManager.CurrentAnim.Update();

            Vector2 moveDir = DirectionUtil.GetVector2ForDir(FacingDir);

            RockShot = new Rock(moveDir, hurtsPlayer);

            SetThrowDir(FacingDir);

            RockShot.Context = context;
            RockShot.Initialize(context, initInfo);

            RockShot.HitCollisionEvent -= OnRockHitCollision;
            RockShot.HitCollisionEvent += OnRockHitCollision;
        }

        /// <summary>
        /// Sets the direction the mole throws the rock.
        /// This also adjusts where its rock initially spawns after hitting collision.
        /// </summary>
        /// <param name="direction">The direction to shoot in.</param>
        public void SetThrowDir(in Direction direction)
        {
            Vector2 moveDir = DirectionUtil.GetVector2ForDir(direction);

            RockShot.SetStartingMoveDir(moveDir);

            RectangleF rect = RockShot.CollisionRect;

            Vector2 startingPos = Vector2.Zero;

            if (moveDir.X > 0f)
            {
                startingPos = UtilityGlobals.ClampRectToOther(Direction.Left, RockShot.transform.Position, rect, CollisionRect);
                startingPos.Y = CollisionRect.Center.Y;
            }
            else if (moveDir.X < 0f)
            {
                startingPos = UtilityGlobals.ClampRectToOther(Direction.Right, RockShot.transform.Position, rect, CollisionRect);
                startingPos.Y = CollisionRect.Center.Y;
            }
            else if (moveDir.Y > 0f)
            {
                startingPos = UtilityGlobals.ClampRectToOther(Direction.Up, RockShot.transform.Position, rect, CollisionRect);
                startingPos.X = CollisionRect.Center.X;
            }
            else if (moveDir.Y < 0f)
            {
                startingPos = UtilityGlobals.ClampRectToOther(Direction.Down, RockShot.transform.Position, rect, CollisionRect);
                startingPos.X = CollisionRect.Center.X;
            }

            RockShot.StartingPos = startingPos;

            //If not launched, update its direction
            if (RockShot.RockState == Rock.RockStates.WaitingLaunch || RockShot.RockState == Rock.RockStates.Destroyed)
            {
                RockShot.SetNextDirection(direction);
            }
        }

        public void PostInitialize(PlayableContext context)
        {
            RockShot.PostInitialize(context);
        }

        private void DefineAnimations()
        {
            AnimationManager.AddAnimation(AnimationGlobals.Idle, new DirectionalAnimation(null, this, AnimTypes.Normal,
                new DirAnimationFrame(80d, ContentGlobals.MoleIdleAnimURect, ContentGlobals.MoleIdleAnimDRect,
                ContentGlobals.MoleIdleAnimLRect, ContentGlobals.MoleIdleAnimRRect)));

            AnimationManager.AddAnimation(AnimationGlobals.MoleAnimations.Throw, new DirectionalAnimation(null, this, AnimTypes.Normal,
                new DirAnimationFrame(80d, ContentGlobals.MoleThrowAnimU1Rect, ContentGlobals.MoleThrowAnimD1Rect,
                    ContentGlobals.MoleThrowAnimL1Rect, ContentGlobals.MoleThrowAnimR1Rect),
                new DirAnimationFrame(80d, ContentGlobals.MoleThrowAnimU2Rect, ContentGlobals.MoleThrowAnimD2Rect,
                    ContentGlobals.MoleThrowAnimL2Rect, ContentGlobals.MoleThrowAnimR2Rect),
                new DirAnimationFrame(80d, ContentGlobals.MoleThrowAnimU3Rect, ContentGlobals.MoleThrowAnimD3Rect,
                    ContentGlobals.MoleThrowAnimL3Rect, ContentGlobals.MoleThrowAnimR3Rect)));

            AnimationManager.AddAnimation(AnimationGlobals.MoleAnimations.Hurt, new DirectionalAnimation(null, this, AnimTypes.Normal,
                new DirAnimationFrame(200d, ContentGlobals.MoleHurtAnimU1Rect, ContentGlobals.MoleHurtAnimD1Rect,
                    ContentGlobals.MoleHurtAnimL1Rect, ContentGlobals.MoleHurtAnimR1Rect),
                new DirAnimationFrame(200d, ContentGlobals.MoleHurtAnimU2Rect, ContentGlobals.MoleHurtAnimD2Rect,
                    ContentGlobals.MoleHurtAnimL2Rect, ContentGlobals.MoleHurtAnimR2Rect)));
        }

        public bool QueryTrigger(List<ICollisionObj> collisions)
        {
            return false;
        }

        public void Render()
        {
            RenderingManager.Instance.DrawSprite(sprite.Tex, transform.Position, sprite.SourceRect, TintColor, transform.Rotation,
                sprite.GetOrigin(), transform.Scale, SpriteEffects.None, RenderDepth);
            //RenderingManager.Instance.spriteBatch.DrawHollowRect(AssetManager.Instance.LoadTexture(ContentGlobals.UITex), ContentGlobals.BoxRect,
            //    CollisionRect, Color.Orange, RenderDepth + .001f, 1);

            //if (LaunchedRock == true)
            RockShot.Render();
        }

        private void OnRockHitCollision()
        {
            if (HitByRock == true) return;

            LaunchedRock = false;
            ElapsedTime = 0d;
        }

        public void OnHitByRock()
        {
            if (HitByRock == true) return;

            HitByRock = true;
            ElapsedTime = 0d;

            AnimationManager.PlayAnimation(AnimationGlobals.MoleAnimations.Hurt);
        }
    }
}