/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// A rock shot from a Rock Shooter. It moves in a certain direction, which can be manipulated with Pipe Blocks.
    /// </summary>
    public class Rock : ICollisionObj, IUpdateable, IDirectional, IRenderDepth, ITintable
    {
        public delegate void HitCollision();

        [Recordable(RecordableFlags.None)]
        private HitCollision HitCollisionHandler = null;

        public event HitCollision HitCollisionEvent
        {
            add
            {
                lock (this)
                {
                    HitCollisionHandler += value;
                }
            }
            remove
            {
                lock (this)
                {
                    HitCollisionHandler -= value;
                }
            }
        }

        public enum RockStates
        {
            WaitingLaunch, Launched, Destroyed
        }

        private LevelGlobals.CollisionFilter RockCollFilter = null;

        public string ID { get; set; } = string.Empty;

        public PlayableContext Context { get; set; } = null;
        public Transform transform { get; private set; } = new Transform();

        public ObjectTypes ObjectType { get; private set; } = ObjectTypes.Rock;

        public CollisionTypes CollisionType { get; set; } = CollisionTypes.Trigger;

        public CollisionLayers CollisionLayer { get; private set; } = CollisionLayers.Wall;

        public TriggerQueries TriggerQuery { get; private set; } = TriggerQueries.None;

        public RectangleF CollisionRect
        {
            get
            {
                return new RectangleF(transform.Position.X - 2, transform.Position.Y - 2, 5, 5);
            }
        }

        public float RenderDepth { get; set; } = LevelGlobals.BaseRockRenderDepth;

        [Recordable(RecordableFlags.None)]
        public Color TintColor { get; set; } = Color.White;

        public Sprite sprite { get; private set; } = null;

        private SimpleAnimation ArrowAnim = null;
        private readonly Vector2 ArrowPosOffset = new Vector2(-4f, -4f);

        [Recordable(RecordableFlags.RecordFields | RecordableFlags.AvoidSet)]
        public AnimManager AnimationManager { get; private set; } = null;

        [Recordable(RecordableFlags.None)]
        public Direction FacingDir { get; private set; }

        /// <summary>
        /// The speed the rock moves. This value is in pixels per second.
        /// </summary>
        public Vector2 RockSpeed { get; private set; } = new Vector2(150f, 150f);

        public Vector2 StartingPos = Vector2.Zero;
        private Vector2 StartingMoveDir = Vector2.Zero;

        [Recordable(RecordableFlags.None)]
        private Vector2 CurMoveDir = Vector2.Zero;

        [Recordable(RecordableFlags.None)]
        public PipeBlock PipeInside { get; private set; } = null;

        private SoundEffect BreakSound = null;
        private SoundEffect ShootSound = null;
        private SoundEffect EnterPipeSound = null;
        private SoundEffect HitHardSound = null;

        [Recordable(RecordableFlags.None)]
        private Vector2 DestroyedPos = Vector2.Zero;
        private const double TotalDestroyedTime = 600d;
        private const double DestroyedBlinkInterval = 34d;

        [Recordable(RecordableFlags.None)]
        private double ElapsedDestroyedTime = 0d;

        [Recordable(RecordableFlags.None)]
        public RockStates RockState = RockStates.WaitingLaunch;
        public bool HurtsPlayer { get; private set; } = false;

        [Recordable(RecordableFlags.RecordFields | RecordableFlags.AvoidSet)]
        private ParticleEngine AirParticles = null;

        public Rock(in Vector2 startingMoveDir, in bool hurtsPlayer)
        {
            StartingMoveDir = startingMoveDir;
            HurtsPlayer = hurtsPlayer;

            sprite = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.LevelObjectsTex), Rectangle.Empty);
            AnimationManager = new AnimManager(sprite, 2);
            DefineAnimations();
            AnimationManager.CurrentAnim.Update();

            ArrowAnim = new SimpleAnimation(new Sprite(sprite.Tex, ContentGlobals.RockArrowAnim1Rect), AnimTypes.Looping,
                new AnimationFrame(ContentGlobals.RockArrowAnim1Rect, 80d),
                new AnimationFrame(ContentGlobals.RockArrowAnim2Rect, 80d));

            transform.Scale = new Vector2(1f, 1f);
        }

        private void DefineAnimations()
        {
            AnimationManager.AddAnimation(AnimationGlobals.RockAnimations.Roll, new DirectionalAnimation(null, this, AnimTypes.Looping,
                new DirAnimationFrame(36d, ContentGlobals.RockRollAnimU1Rect, ContentGlobals.RockRollAnimD1Rect, 
                    ContentGlobals.RockRollAnimL1Rect, ContentGlobals.RockRollAnimR1Rect),
                new DirAnimationFrame(36d, ContentGlobals.RockRollAnimU2Rect, ContentGlobals.RockRollAnimD2Rect,
                    ContentGlobals.RockRollAnimL2Rect, ContentGlobals.RockRollAnimR2Rect),
                new DirAnimationFrame(36d, ContentGlobals.RockRollAnimU3Rect, ContentGlobals.RockRollAnimD3Rect,
                    ContentGlobals.RockRollAnimL3Rect, ContentGlobals.RockRollAnimR3Rect)));

            Rectangle breakFrame1 = ContentGlobals.RockBreakAnim1Rect;
            Rectangle breakFrame2 = ContentGlobals.RockBreakAnim2Rect;
            Rectangle breakFrame3 = ContentGlobals.RockBreakAnim3Rect;

            AnimationManager.AddAnimation(AnimationGlobals.RockAnimations.Break, new DirectionalAnimation(null, this, AnimTypes.Normal,
                new DirAnimationFrame(80d, breakFrame1, breakFrame1, breakFrame1, breakFrame1),
                new DirAnimationFrame(80d, breakFrame2, breakFrame2, breakFrame2, breakFrame2),
                new DirAnimationFrame(80d, breakFrame3, breakFrame3, breakFrame3, breakFrame3)));
        }

        public void CleanUp()
        {
            if (PipeInside != null)
            {
                PipeInside.Filled = false;
                PipeInside = null;
            }
            HitCollisionHandler = null;
            //BreakSound = null;
            //ShootSound = null;
        }

        public void Update()
        {
            AnimationManager.CurrentAnim.Update();

            if (PipeInside != null)
            {
                ArrowAnim.Update();
            }

            if (RockState == RockStates.Launched)
            {
                //Move in the direction it's shot
                Vector2 moveAmt = CurMoveDir * (RockSpeed * (float)Time.ElapsedTime.TotalSeconds);
                transform.Position += moveAmt;

                CheckAndHandleCollision();
            }
            else
            {
                ElapsedDestroyedTime = UtilityGlobals.Clamp(ElapsedDestroyedTime + Time.ElapsedTime.TotalMilliseconds, 0d, TotalDestroyedTime);
            }

            AirParticles.Position = transform.Position;
            AirParticles.Update();
        }

        private float GetRockSoundVolume()
        {
            float volume = 1f;

            //If the rock isn't in the camera's view, set the volume to 0
            if (Context.LevelCamera != null && Context.LevelCamera.IsInCameraView((Rectangle)CollisionRect) == false)
            {
                return 0f;
            }

            int moleCount = Context.RockShooters.Count;

            //Scale the volume by the number of Rock Shooters
            if (moleCount > 0)
            {
                volume = .8f;
                if (moleCount > 3)
                {
                    volume = .6f;

                    if (moleCount > 5)
                    {
                        volume = .4f;

                        if (moleCount > 8)
                        {
                            volume = .2f;
                        }
                    }
                }
            }

            return volume;
        }

        private void CheckAndHandleCollision()
        {
            ICollisionObj coll = LevelGlobals.CheckCollision(Vector2.Zero, CollisionRect, Context.LevelCollision, RockCollFilter);

            if (coll == null) return;

            //Check what we hit
            if (coll.ObjectType == ObjectTypes.Block)
            {
                //If we hit a Block and it's not a Pipe Block, reset
                Block b = (Block)coll;
                if (b.BlockType != BlockTypes.Pipe)
                {
                    Reset();
                    PlayBreakSound(GetRockSoundVolume());
                }
                //We hit a Pipe Block - see if we should be redirected
                else
                {
                    PipeBlock pipe = (PipeBlock)b;

                    //If there's another rock in the pipe, ignore
                    if (pipe.Filled == true)
                    {
                        Reset();
                        PlayBreakSound(GetRockSoundVolume());
                        return;
                    }

                    Direction curMoveDir = DirectionUtil.GetDirectionForVector2(CurMoveDir);
                    Direction opposite = DirectionUtil.GetOppositeDirection(curMoveDir);

                    //Check if we can enter the pipe
                    if (pipe.EntranceDir == opposite)
                    {
                        SetNextDirection(pipe.ExitDir);

                        PipeInside = pipe;
                        PipeInside.Filled = true;
                        RockState = RockStates.WaitingLaunch;
                        AirParticles.Stop(false);

                        ArrowAnim.Play();

                        PlayEnterPipeSound(GetRockSoundVolume());

                        HitCollisionHandler?.Invoke();
                    }
                    else if (pipe.ExitDir == opposite)
                    {
                        SetNextDirection(pipe.EntranceDir);

                        PipeInside = pipe;
                        PipeInside.Filled = true;
                        RockState = RockStates.WaitingLaunch;
                        AirParticles.Stop(false);

                        ArrowAnim.Play();

                        PlayEnterPipeSound(GetRockSoundVolume());

                        HitCollisionHandler?.Invoke();
                    }
                    else
                    {
                        Reset();
                        PlayBreakSound(GetRockSoundVolume());
                    }
                }
            }
            else if (coll.ObjectType == ObjectTypes.Player)
            {
                //Make sure the player isn't warping
                Echidna player = (Echidna)coll;

                if (player.PlayerState != PlayerStates.Warp)
                {
                    //If we hit the player and we should hurt it, game over and restart the level
                    if (HurtsPlayer == true)
                    {
                        Context.Player.InvokeHitByStrongRock();
                        PlayHitHardSound(1f);
                    }
                    else
                    {
                        //Otherwise, if the player is in a valid state to get stunned, stun it briefly
                        if (Context.Player.PlayerState == PlayerStates.Idle || Context.Player.PlayerState == PlayerStates.Walk
                            || Context.Player.PlayerState == PlayerStates.Other)
                        {
                            Context.Player.ChangeState(new PlayerStunnedState(DirectionUtil.GetDirectionForVector2(CurMoveDir), Context.Player.CurState));
                        }

                        Reset();
                        PlayBreakSound(GetRockSoundVolume());
                    }
                }
            }
            //If we hit a rock shooter, destroy it
            else if (coll.ObjectType == ObjectTypes.RockShooter)
            {
                RockShooter rs = (RockShooter)coll;

                Reset();

                //Play at full volume
                PlayHitHardSound(1f);

                rs.OnHitByRock();
            }
            //If not anything else, reset
            else
            {
                Reset();
                PlayBreakSound(GetRockSoundVolume());
            }
        }

        private void PlayBreakSound(in float volume)
        {
            if (BreakSound != null && volume > 0f)
            {
                SoundManager.Instance.PlaySound(BreakSound, false, volume);
            }
        }

        private void PlayEnterPipeSound(in float volume)
        {
            if (EnterPipeSound != null && volume > 0f)
            {
                SoundManager.Instance.PlaySound(EnterPipeSound, false, volume);
            }
        }

        private void PlayHitHardSound(in float volume)
        {
            if (HitHardSound != null && volume > 0f)
            {
                SoundManager.Instance.PlaySound(HitHardSound, false, volume);
            }
        }

        public void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            if (HurtsPlayer == true)
            {
                TintColor = Color.Gray;
            }

            //Check for loading the sounds
            if (BreakSound == null || ShootSound == null)
            {
                bool loadSound = true;

                if (initInfo.Properties.TryGetValue(LevelGlobals.HasSoundKey, out string noSoundStr) == true)
                {
                    bool.TryParse(noSoundStr, out loadSound);
                }

                if (loadSound == true)
                {
                    BreakSound = AssetManager.Instance.LoadSound(ContentGlobals.RockBreakSound);
                    ShootSound = AssetManager.Instance.LoadSound(ContentGlobals.RockShootSound);
                    EnterPipeSound = AssetManager.Instance.LoadSound(ContentGlobals.RockEnterPipeSound);
                    HitHardSound = AssetManager.Instance.LoadSound(ContentGlobals.RockHitHardSound);
                }
            }

            Sprite particleSprite = new Sprite(sprite.Tex, ContentGlobals.BlockParticleRect);
            AirParticles = new ParticleEngine(10, transform.Position, particleSprite, RenderDepth - .01f, 300d, 800d);
            AirParticles.EmissionRate = 16d;
            AirParticles.MinAngularVelocity = UtilityGlobals.ToRadians(90f);
            AirParticles.MaxAngularVelocity = UtilityGlobals.ToRadians(90f);
            AirParticles.MinParticleLife = 101d;
            AirParticles.MaxParticleLife = 151d;
            AirParticles.ColorOverTime = Color.White * .7f;
            AirParticles.ParticleColor = Color.White;
            AirParticles.UseColorOverTime = true;
            AirParticles.PrewarmParticles();
            AdjustParticleDirection(Direction.Up);
            AirParticles.Stop(true);
        }

        private void AdjustParticleDirection(in Direction moveDirection)
        {
            //Not initialized yet; ignore
            if (AirParticles == null)
            {
                return;
            }

            RectangleF collisionRect = CollisionRect;
            const float accelConst = .1f * 60f;
            Vector2 scaleMin = new Vector2(.7f, .7f);
            Vector2 scaleMax = new Vector2(2f, 2f);
            Vector2 velocityMin = new Vector2(0f, .3f) * 60f;
            Vector2 velocityMax = new Vector2(0f, .5f) * 60f;

            if (moveDirection == Direction.Up)
            {
                int width = (int)collisionRect.Width;

                AirParticles.MinPositionOffset = new Vector2(-width, collisionRect.Height);
                AirParticles.MaxPositionOffset = new Vector2(width, AirParticles.MinPositionOffset.Y);
                AirParticles.MinVelocity = velocityMin;
                AirParticles.MaxVelocity = velocityMax;
                AirParticles.MinInitScale = scaleMin;
                AirParticles.MaxInitScale = scaleMax;
                AirParticles.MinAcceleration = new Vector2(-accelConst, 0f);
                AirParticles.MaxAcceleration = new Vector2(accelConst, 0f);
            }
            else if (moveDirection == Direction.Down)
            {
                int width = (int)collisionRect.Width;

                AirParticles.MinPositionOffset = new Vector2(-width, -collisionRect.Height);
                AirParticles.MaxPositionOffset = new Vector2(width, AirParticles.MinPositionOffset.Y);
                AirParticles.MinVelocity = -velocityMax;
                AirParticles.MaxVelocity = -velocityMin;
                AirParticles.MinInitScale = scaleMin;
                AirParticles.MaxInitScale = scaleMax;
                AirParticles.MinAcceleration = new Vector2(-accelConst, 0f);
                AirParticles.MaxAcceleration = new Vector2(accelConst, 0f);
            }
            else if (moveDirection == Direction.Left)
            {
                int height = (int)collisionRect.Height;

                AirParticles.MinPositionOffset = new Vector2(collisionRect.Width, -height);
                AirParticles.MaxPositionOffset = new Vector2(AirParticles.MinPositionOffset.X, height);
                AirParticles.MinVelocity = velocityMin.SwapXY();
                AirParticles.MaxVelocity = velocityMax.SwapXY();
                AirParticles.MinInitScale = scaleMin;
                AirParticles.MaxInitScale = scaleMax;
                AirParticles.MinAcceleration = new Vector2(0f, -accelConst);
                AirParticles.MaxAcceleration = new Vector2(0f, accelConst);
            }
            else
            {
                int height = (int)collisionRect.Height;

                AirParticles.MinPositionOffset = new Vector2(-collisionRect.Width, -height);
                AirParticles.MaxPositionOffset = new Vector2(AirParticles.MinPositionOffset.X, height);
                AirParticles.MinVelocity = -velocityMax.SwapXY();
                AirParticles.MaxVelocity = -velocityMin.SwapXY();
                AirParticles.MinInitScale = scaleMin;
                AirParticles.MaxInitScale = scaleMax;
                AirParticles.MinAcceleration = new Vector2(0f, -accelConst);
                AirParticles.MaxAcceleration = new Vector2(0f, accelConst);
            }
        }

        public void PostInitialize(PlayableContext context)
        {
            RockCollFilter = CheckRockCollision;

            Reset();
            RockState = RockStates.WaitingLaunch;
        }

        public bool QueryTrigger(List<ICollisionObj> collisions)
        {
            return false;
        }

        public void Reset()
        {
            DestroyedPos = transform.Position;
            transform.Position = StartingPos;
            transform.Rotation = 0f;
            CurMoveDir = StartingMoveDir;
            RockState = RockStates.Destroyed;
            ElapsedDestroyedTime = 0d;

            FacingDir = DirectionUtil.GetDirectionForVector2(CurMoveDir);
            AdjustParticleDirection(FacingDir);

            AnimationManager.PlayAnimation(AnimationGlobals.RockAnimations.Break);
            AirParticles.Stop(false);

            HitCollisionHandler?.Invoke();
        }

        public void Launch()
        {
            AnimationManager.PlayAnimation(AnimationGlobals.RockAnimations.Roll);
            AirParticles.Resume();

            if (PipeInside == null)
            {
                transform.Position = StartingPos;
            }
            else
            {
                RectangleF rect = PipeInside.CollisionRect;

                if (CurMoveDir.X > 0f)
                {
                    Vector2 pos = UtilityGlobals.ClampRectToOther(Direction.Left, transform.Position, CollisionRect, rect);
                    pos.Y = rect.Center.Y;
                    transform.Position = pos;
                }
                else if (CurMoveDir.X < 0f)
                {
                    Vector2 pos = UtilityGlobals.ClampRectToOther(Direction.Right, transform.Position, CollisionRect, rect);
                    pos.Y = rect.Center.Y;
                    transform.Position = pos;
                }
                else if (CurMoveDir.Y > 0f)
                {
                    Vector2 pos = UtilityGlobals.ClampRectToOther(Direction.Up, transform.Position, CollisionRect, rect);
                    pos.X = rect.Center.X;
                    transform.Position = pos;
                }
                else if (CurMoveDir.Y < 0f)
                {
                    Vector2 pos = UtilityGlobals.ClampRectToOther(Direction.Down, transform.Position, CollisionRect, rect);
                    pos.X = rect.Center.X;
                    transform.Position = pos;
                }

                PipeInside.Filled = false;
                PipeInside = null;
            }

            if (ShootSound != null)
            {
                float volume = GetRockSoundVolume();

                if (volume > 0f)
                    SoundManager.Instance.PlaySound(ShootSound, false, volume);
            }
            RockState = RockStates.Launched;
        }

        public void SetStartingMoveDir(in Vector2 moveDir)
        {
            StartingMoveDir = moveDir;
        }

        public void SetNextDirection(in Direction direction)
        {
            CurMoveDir = DirectionUtil.GetVector2ForDir(direction);
            
            FacingDir = direction;
            AdjustParticleDirection(FacingDir);
        }

        public void Render()
        {
            AirParticles.Render();

            if (RockState == RockStates.Launched)
            {
                RenderingManager.Instance.DrawSprite(sprite.Tex, transform.Position, sprite.SourceRect, TintColor, transform.Rotation,
                    sprite.GetOrigin(), transform.Scale, SpriteEffects.None, RenderDepth);
                //RenderingManager.Instance.spriteBatch.DrawHollowRect(AssetManager.Instance.LoadTexture(ContentGlobals.UITex), ContentGlobals.BoxRect, CollisionRect, Color.White, RenderDepth + .01f, 1);
            }
            else if (RockState == RockStates.Destroyed && ElapsedDestroyedTime < TotalDestroyedTime)
            {
                Color col = ((int)(ElapsedDestroyedTime / DestroyedBlinkInterval) % 2) == 0 ? Color.Transparent : TintColor;
                RenderingManager.Instance.DrawSprite(sprite.Tex, DestroyedPos, sprite.SourceRect, col, transform.Rotation, sprite.GetOrigin(),
                    transform.Scale, SpriteEffects.None, RenderDepth);
            }

            //Render the arrow animation
            if (PipeInside != null)
            {
                //Get the position based on the current direction of the rock
                Vector2 pos = PipeInside.transform.Position + (CurMoveDir * (PipeInside.CollisionRect.Size + ArrowPosOffset));
                float rotation = DirectionUtil.GetRotationForDir(FacingDir);

                RenderingManager.Instance.DrawSprite(ArrowAnim.SpriteToChange.Tex, pos, ArrowAnim.SpriteToChange.SourceRect,
                    Color.White, rotation, ArrowAnim.SpriteToChange.GetOrigin(), Vector2.One, SpriteEffects.None, RenderDepth - .001f);
            }
        }

        private bool CheckRockCollision(ICollisionObj collisionObj)
        {
            return ((collisionObj.CollisionType == CollisionTypes.Blocking || collisionObj.ObjectType == ObjectTypes.Player));
        }
    }
}