/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// Settings regarding timesteps.
    /// </summary>
    public enum TimestepSettings
    {
        /// <summary>
        /// The game will run at a fixed interval to hit the target framerate.
        /// </summary>
        Fixed,

        /// <summary>
        /// The game will run at a variable interval and run as fast as allowed by the graphics card.
        /// </summary>
        Variable
    }

    /// <summary>
    /// Settings regarding VSync.
    /// </summary>
    public enum VSyncSettings
    {
        Enabled, Disabled
    }
}