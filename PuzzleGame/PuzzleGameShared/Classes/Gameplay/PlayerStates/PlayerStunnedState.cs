﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// A state that temporarily stuns the player. The player is put into this when getting hit by a non-damaging rock.
    /// </summary>
    public class PlayerStunnedState : PlayerStateMachine
    {
        private const double BaseStunTime = 500d;

        private Vector2 PlayerForwardMoveAmount = Vector2.Zero;
        private Vector2 PlayerBackwardMoveAmount = Vector2.Zero;
        private Vector2 OrigPlayerDrawOffset = Vector2.Zero;

        private bool MoveForward = true;

        private const double ChangeTime = 32d;
        private double FrameTime = 0d;

        private double StunTime = BaseStunTime;
        private double ElapsedTime = 0d;
        private PlayerStateMachine PrevPlayerState = null;

        public PlayerStunnedState(in Direction hitDirection, PlayerStateMachine prevState)
            : this(hitDirection, prevState, BaseStunTime)
        {
            
        }

        public PlayerStunnedState(in Direction hitDirection, PlayerStateMachine prevState, in double stunTime)
        {
            PlayerForwardMoveAmount = DirectionUtil.GetVector2ForDir(hitDirection);
            PlayerBackwardMoveAmount = -PlayerForwardMoveAmount;

            PrevPlayerState = prevState;
            StunTime = stunTime;
        }

        public override void Enter()
        {
            OrigPlayerDrawOffset = Vector2.Zero;

            Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.Idle);
            Player.PlayerState = PlayerStates.Stun;
        }

        public override void Exit()
        {
            Player.DrawOffset = OrigPlayerDrawOffset;
        }

        public override void Update()
        {
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= StunTime)
            {
                Player.DrawOffset = OrigPlayerDrawOffset;

                Player.ChangeState(PrevPlayerState);
            }
            else
            {
                if (MoveForward == true)
                {
                    Player.DrawOffset = PlayerForwardMoveAmount;
                }
                else
                {
                    Player.DrawOffset = PlayerBackwardMoveAmount;
                }

                FrameTime += Time.ElapsedTime.TotalMilliseconds;

                if (FrameTime >= ChangeTime)
                {
                    MoveForward = !MoveForward;
                    FrameTime = 0d;
                }
            }
        }
    }
}
