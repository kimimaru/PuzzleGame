﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public class PlayerBlockMoveState : PlayerStateMachine
    {
        private Block BlockGrabbed = null;

        private Vector2 MoveAmount = Vector2.Zero;
        private double MoveTime = 0d;
        private double ElapsedTime = 0d;

        private Vector2 PlayerStartPos = Vector2.Zero;
        private Vector2 PlayerEndPos = Vector2.Zero;

        /// <summary>
        /// Whether to buffer releasing the block if using the press grab setting.
        /// </summary>
        private bool BufferRelease = false;

        public PlayerBlockMoveState(Block blockGrabbed, in Vector2 moveAmt, in double moveTime)
        {
            BlockGrabbed = blockGrabbed;

            MoveAmount = moveAmt;
            MoveTime = moveTime;
        }

        public override void Enter()
        {
            PlayerStartPos = Player.transform.Position;
            PlayerEndPos = PlayerStartPos + MoveAmount;

            Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.PlayerAnimations.GrabWalk);

            BlockGrabbed.Grab();
            Player.PlayerState = PlayerStates.GrabMove;

            BufferRelease = false;
        }

        public override void Exit()
        {
            BlockGrabbed.Release();
            BlockGrabbed = null;
        }

        public override void Update()
        {
            //Check for buffering the release for the press grab setting
            //if (BufferRelease == false && Input.GetButtonDown(InputActions.Grab) == true)
            //{
            //    BufferRelease = true;
            //}

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= MoveTime)
            {
                Player.transform.Position = PlayerEndPos;

                Vector2 dir = MoveAmount;
                dir.Normalize();

                //If the block was pushed, check if it's now on ice
                //A block is pushed if it goes in the same direction the player is facing
                if (DirectionUtil.GetDirectionForVector2(dir) == Player.FacingDir)
                {
                    //Check if the block is now on ice; if so, the block will slide automatically after moving, so put the player in idle
                    SurfaceTypes surface = Player.Context.SurfaceTiles.GetTileInRegion(BlockGrabbed.CollisionRect);

                    if (surface == SurfaceTypes.Ice)
                    {
                        //Make sure nothing is in front of the block; if there is, act like normal since it's not sliding
                        ICollisionObj coll = LevelGlobals.CheckCollision(dir * Player.Context.LevelSpace.TileSize,
                            BlockGrabbed.CollisionRect, Player.Context.LevelCollision, CustomBlockIceFilter);

                        if (coll == null)
                        {
                            BlockGrabbed.Release();

                            Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.Idle);

                            //Set the player back to idle
                            Player.ChangeState(new IdleState());
                            return;
                        }
                    }
                }

                //If the block is released at this point (Ex. it took a Warp), go to idle
                //This fixes a bug where you can move the block out of a warp if it already started warping
                //This happen if the distance threshold for Warps is high enough to cause a block to warp before the player is done moving
                //due to the player going back into the grabbed state
                if (BlockGrabbed.Grabbed == false)
                {
                    Player.ChangeState(new IdleState());
                    return;
                }

                bool shouldRelease = (BufferRelease == true && DataHandler.saveData.Settings.GrabOption == (int)GrabSettings.Press);

                //Go to the block grab state
                Player.ChangeState(new BlockGrabState(BlockGrabbed, false, shouldRelease));
            }
            else
            {
                Player.transform.Position = Interpolation.Interpolate(PlayerStartPos, PlayerEndPos, ElapsedTime / MoveTime, Interpolation.InterpolationTypes.Linear);
            }
        }

        private bool CustomBlockIceFilter(ICollisionObj collisionObj)
        {
            return (collisionObj != BlockGrabbed && collisionObj.CollisionType == CollisionTypes.Blocking
                && collisionObj.CollisionLayer != CollisionLayers.BlockExcept);
        }
    }
}
