﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// A general-purpose blank state that disables player input.
    /// </summary>
    public class BlankState : PlayerStateMachine
    {
        public bool PlayIdleAnim = true;

        public BlankState()
        {

        }

        public BlankState(in bool playIdleAnim)
        {
            PlayIdleAnim = playIdleAnim;
        }

        public override void Enter()
        {
            if (PlayIdleAnim == true)
            {
                Player.AnimationManager.PlayAnimation(AnimationGlobals.Idle);
            }
            Player.PlayerState = PlayerStates.Other;
        }

        public override void Exit()
        {
            
        }

        public override void Update()
        {
            
        }
    }
}
