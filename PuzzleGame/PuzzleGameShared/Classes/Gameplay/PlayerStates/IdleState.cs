﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class IdleState : PlayerStateMachine
    {
        private LevelGlobals.CollisionFilter GrabBlockFilter = null;

        #region Idle Wait Animation

        private const double PlayIdleAnimTime = 20000d;

        private const double SleepTime = 200d;
        private readonly Vector2 InitSleepZOffset = new Vector2(6, -8);
        private readonly Vector2 SleepZOffset = new Vector2(5, -12);

        private const string SleepText = "Z";

        private const float SleepZScale = 1f;
        private const int SleepZCount = 3;
        private const double SleepAlphaTimeOffset = 200d;
        private const double SleepAlphaTime = 300d;

        private const double SleepPosTime = 91d;
        private const double SleepPosOffsetTime = 34d;
        private const float SleepMaxPosOffset = 2f;

        private SpriteFont SleepFont = null;
        private Vector2 SleepTextOrigin = Vector2.Zero;

        private double ElapsedIdleAnimTime = 0d;
        private double ElapsedTimeAgainstBlock = 0d;

        #endregion

        public override void Enter()
        {
            GrabBlockFilter = CheckGrabbableBlock;

            SleepFont = AssetManager.Instance.LoadFont(ContentGlobals.SpecialFont13px);
            SleepTextOrigin = SleepFont.GetOrigin(SleepText, 0f, 1f);

            Player.PlayerState = PlayerStates.Idle;
            ElapsedIdleAnimTime = 0d;
        }

        public override void Exit()
        {
            GrabBlockFilter = null;
            ElapsedIdleAnimTime = 0d;
            ElapsedTimeAgainstBlock = 0d;

            SleepFont = null;
        }

        public override void Update()
        {
            ElapsedIdleAnimTime += Time.ElapsedTime.TotalMilliseconds;

            if (DataHandler.saveData.Settings.GrabOption != (int)GrabSettings.Auto)
            {
                ElapsedTimeAgainstBlock = 0d;
            }

            //Handle grabbing the block
            if (ShouldGrabBlock() == true)
            {
                Vector2 dir = DirectionUtil.GetVector2ForDir(Player.FacingDir);

                //Check if there's collision here and if the collision is a grabbable Block
                Block block = LevelGlobals.CheckCollision(dir * Player.Context.LevelSpace.TileSize, Player.GrabRect, Player.Context.Blocks,
                    GrabBlockFilter);

                //Grab the block
                if (block != null)
                {
                    Player.ChangeState(new BlockGrabState(block, true, false));
                    return;
                }
            }

            HandleMove();
        }

        private bool ShouldGrabBlock()
        {
            if (DataHandler.saveData.Settings.GrabOption == (int)GrabSettings.Auto && ElapsedTimeAgainstBlock >= Echidna.AutoBlockGrabTime)
            {
                return true;
            }

            if (DataHandler.saveData.Settings.GrabOption == (int)GrabSettings.Hold || DataHandler.saveData.Settings.GrabOption == (int)GrabSettings.Auto)
            {
                if (DataHandler.saveData.Settings.InputOption != (int)PlayerInputPriorityOptions.Buffered)
                {
                    return Input.GetButton(InputActions.Grab) == true;
                }
                else
                {
                    if (Player.BufferedInputs.CheckAndConsumeInput(InputActions.Grab) == true)
                    {
                        return true;
                    }
                    else
                    {
                        return Input.GetButton(InputActions.Grab) == true
                            && (Player.BufferedInputs.LastInputPressed == InputActions.Grab
                            || string.IsNullOrEmpty(Player.BufferedInputs.LastInputPressed) == true);
                    }
                }
            }
            else
            {
                if (DataHandler.saveData.Settings.InputOption != (int)PlayerInputPriorityOptions.Buffered)
                {
                    return Input.GetButtonDown(InputActions.Grab) == true;
                }
                else return Player.BufferedInputs.CheckAndConsumeInput(InputActions.Grab);
            }
        }

        private bool HandleMove()
        {
            Vector2 moveDir = Vector2.Zero;

            //If input priority is set to be buffered, check the value using the buffer
            if (DataHandler.saveData.Settings.InputOption == (int)PlayerInputPriorityOptions.Buffered)
            {
                Direction? lastDir = DirectionUtil.GetDirectionForAction(Player.BufferedInputs.LastInputPressed);
                //Check the most recently pressed input - we don't check if the input was held to avoid moving farther than intended
                if (lastDir != null)
                {
                    Player.BufferedInputs.ConsumeLastInput();
                    moveDir = DirectionUtil.GetVector2ForDir(lastDir.Value);
                }
                //Resort to the last direction held
                else if (Player.BufferedInputs.LastDirectionHeld != null)
                {
                    moveDir = DirectionUtil.GetVector2ForDir(Player.BufferedInputs.LastDirectionHeld.Value);
                }
            }
            //Resort to the last direction held
            else if (Player.BufferedInputs.LastDirectionHeld != null)
            {
                moveDir = DirectionUtil.GetVector2ForDir(Player.BufferedInputs.LastDirectionHeld.Value);
            }
            //}
            //If the input priority is set to read the most recent direction, simply check the value of the last held input the player recorded
            //else if (EnumUtility.HasEnumVal((long)Player.InputPriorityOptions, (long)PlayerInputPriorityOptions.MostRecent) == true)
            //{
            //    if (Player.LastInputHeld != null)
            //    {
            //        moveDir = DirectionUtil.GetVector2ForDir(Player.LastInputHeld.Value);
            //    }
            //}
            //Otherwise obtain input here to get the movement direction
            //else
            //{
            //    if (Input.GetButton(InputActions.Left) == true)
            //    {
            //        moveDir.X = -1f;
            //    }
            //    else if (Input.GetButton(InputActions.Right) == true)
            //    {
            //        moveDir.X = 1f;
            //    }
            //    else if (Input.GetButton(InputActions.Up) == true)
            //    {
            //        moveDir.Y = -1f;
            //    }
            //    else if (Input.GetButton(InputActions.Down) == true)
            //    {
            //        moveDir.Y = 1f;
            //    }
            //}

            if (moveDir == Vector2.Zero)
            {
                if (ElapsedIdleAnimTime >= PlayIdleAnimTime)
                {
                    Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.PlayerAnimations.Sleep);
                }
                else
                {
                    Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.Idle);
                }

                ElapsedTimeAgainstBlock = 0d;
                return false;
            }

            //Face the direction you moved in
            Direction prevDir = Player.FacingDir;
            Player.FacingDir = DirectionUtil.GetDirectionForVector2(moveDir);

            ICollisionObj coll = CheckCollision(moveDir * Player.Context.LevelSpace.TileSize, CollisionTypes.Blocking, CollisionLayers.AllExceptPlayer);

            //Return if there's something blocking the player
            if (coll != null)
            {
                //Reset idle time if the player just changed direction
                if (prevDir != Player.FacingDir)
                {
                    ElapsedIdleAnimTime = 0d;
                    Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.Idle);
                }
                else if (ElapsedIdleAnimTime >= PlayIdleAnimTime)
                {
                    Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.PlayerAnimations.Sleep);
                }
                else
                {
                    Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.Idle);
                }

                //If the grab option is Auto, check if it's a grabbable block and how long the player is pressing against it
                if (DataHandler.saveData.Settings.GrabOption == (int)GrabSettings.Auto && coll.ObjectType == ObjectTypes.Block)
                {
                    Block b = (Block)coll;
                    if (b.Grabbable == true)
                    {
                        ElapsedTimeAgainstBlock += Time.ElapsedTime.TotalMilliseconds;
                    }
                    else
                    {
                        ElapsedTimeAgainstBlock = 0d;
                    }
                }
                else
                {
                    ElapsedTimeAgainstBlock = 0d;
                }

                return false;
            }

            ElapsedTimeAgainstBlock = 0d;

            double time = Player.MoveTime;

            SurfaceTypes playerSurface = Player.Context.SurfaceTiles.GetTile(Player.SurfaceCheckPos);
            if (playerSurface == SurfaceTypes.Mud)
            {
                time = Player.GetMoveTimeWithModifier(Player.MudTimeModifier);
            }
            //Slide if on Ice
            else if (playerSurface == SurfaceTypes.Ice)
            {
                Player.ChangeState(new IceMoveState(moveDir));
                return true;
            }

            //Change to the walk state
            Player.ChangeState(new PlayerMoveState(moveDir * Player.Context.LevelSpace.TileSize, time));

            //NOTE: Maybe we can handle this in the state itself - this would keep things consolidated and make it easier to work with scripted sequences (Ex. demonstrations, cutscenes)
            if (EnumUtility.HasEnumVal((long)Player.MoveSmoothingOptions, (long)PlayerSmoothingOptions.Walk) == true)
            {
                Player.CurState.Update();
            }

            return true;
        }

        private bool CheckGrabbableBlock(ICollisionObj collisionObj)
        {
            //Make sure it's a block
            if (collisionObj.ObjectType != ObjectTypes.Block || collisionObj.CollisionType != CollisionTypes.Blocking) return false;

            Block block = collisionObj as Block;
            if (block == null) return false;

            //Return only grabbable blocks
            return block.Grabbable;
        }

        public override void Render()
        {
            base.Render();

            if (ElapsedIdleAnimTime < PlayIdleAnimTime) return;

            //Show the "Z"s of the Echidna sleeping
            Vector2 sleepZPos = Player.transform.Position + InitSleepZOffset;

            for (int i = 0; i < SleepZCount; i++)
            {
                float alpha = (float)Math.Sin((-ElapsedIdleAnimTime + (i * SleepAlphaTimeOffset)) / SleepAlphaTime);
                Color col = Color.White * alpha;
                Vector2 newPos = sleepZPos + (i * SleepZOffset);
                int xOffset = (int)(Math.Sin((ElapsedIdleAnimTime + (i * SleepPosOffsetTime)) / SleepPosTime) * SleepMaxPosOffset);
                newPos.X += xOffset;

                //Don't draw if there's no alpha
                if (alpha <= 0)
                    continue;

                RenderingManager.Instance.spriteBatch.DrawString(SleepFont, SleepText, newPos, col, 0f, SleepTextOrigin,
                    SleepZScale, SpriteEffects.None, Player.RenderDepth + .05f);
            }
        }
    }
}
