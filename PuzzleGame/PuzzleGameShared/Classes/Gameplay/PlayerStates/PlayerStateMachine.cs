﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// State machine for the player.
    /// </summary>
    public abstract class PlayerStateMachine : IFiniteStateMachine
    {
        protected Echidna Player { get; private set; } = null;

        protected PlayerStateMachine()
        {

        }

        public void SetPlayer(in Echidna player)
        {
            Player = player;
        }

        public abstract void Enter();

        public abstract void Exit();

        public abstract void Update();

        public virtual void Render()
        {

        }

        protected ICollisionObj CheckCollision(in Vector2 speed, in ObjectTypes objType, in CollisionTypes collType,
            in CollisionLayers collObjType)
        {
            return LevelGlobals.CheckCollision(speed, Player.CollisionRect, collType, collObjType, Player.Context.LevelCollision);
        }

        protected ICollisionObj CheckCollision(in Vector2 speed, in ObjectTypes objType, in CollisionTypes collType)
        {
            return LevelGlobals.CheckCollision(speed, Player.CollisionRect, objType, collType, CollisionLayers.All, Player.Context.LevelCollision);
        }

        protected ICollisionObj CheckCollision(in Vector2 speed, in CollisionTypes collType, in CollisionLayers collLayer)
        {
            return LevelGlobals.CheckCollision(speed, Player.CollisionRect, collType, collLayer, Player.Context.LevelCollision);
        }
    }
}
