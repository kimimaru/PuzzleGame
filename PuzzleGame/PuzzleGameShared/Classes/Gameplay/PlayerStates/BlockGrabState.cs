﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public class BlockGrabState : PlayerStateMachine
    {
        private LevelGlobals.CollisionFilter PlayerFilter = null;
        private LevelGlobals.CollisionFilter BlockFilter = null;

        private Block BlockGrabbed = null;

        private bool FromIdleState = false;
        private bool ForceRelease = false;
        private double AutoBlockReleaseTime = 0d;
        private double ElapsedTimeHoldingBlock = 0d;
        private double ElapsedBlockMoveDelay = 0d;

        public BlockGrabState(Block blockGrabbed, in bool fromIdleState, in bool forceRelease)
        {
            BlockGrabbed = blockGrabbed;
            FromIdleState = fromIdleState;
            ForceRelease = forceRelease;
        }

        public override void Enter()
        {
            if (FromIdleState == true)
            {
                Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Grab);
                AutoBlockReleaseTime = Echidna.AutoBlockReleasePostDelayTime;
            }
            else
            {
                AutoBlockReleaseTime = Echidna.AutoBlockReleaseTime;
            }

            BlockGrabbed.Grab();

            PlayerFilter = CustomPlayerFilter;
            BlockFilter = CustomBlockFilter;
            Player.PlayerState = PlayerStates.Grab;

            ElapsedTimeHoldingBlock = 0d;
        }

        public override void Exit()
        {
            BlockGrabbed.Release();
            BlockGrabbed = null;
            
            PlayerFilter = null;
            BlockFilter = null;

            ElapsedTimeHoldingBlock = 0d;
        }

        private bool ShouldReleaseBlock()
        {
            if (ForceRelease == true)
            {
                return true;
            }

            if (DataHandler.saveData.Settings.GrabOption == (int)GrabSettings.Auto)
            {
                bool released = (Input.GetButton(InputActions.Grab) == false && ElapsedTimeHoldingBlock >= AutoBlockReleaseTime) || Input.GetButtonUp(InputActions.Grab);

                //If the block is released and the input buffer is in use, consume any grab input that may have happened as a result of the release
                //This prevents regrabbing the block immediately after
                if (released == true && DataHandler.saveData.Settings.InputOption == (int)PlayerInputPriorityOptions.Buffered)
                {
                    Player.BufferedInputs.CheckAndConsumeInput(InputActions.Grab);
                }

                return released;
            }

            if (DataHandler.saveData.Settings.GrabOption == (int)GrabSettings.Hold)
            {
                return (Input.GetButton(InputActions.Grab) == false);
            }
            else
            {
                if (DataHandler.saveData.Settings.InputOption != (int)PlayerInputPriorityOptions.Buffered)
                {
                    return (Input.GetButtonDown(InputActions.Grab) == true);
                }
                else return Player.BufferedInputs.CheckAndConsumeInput(InputActions.Grab);
            }
        }

        public override void Update()
        {
            //Change to the idle state if you let go of the button or the block is already released (Ex. it entered a Warp)
            if (ShouldReleaseBlock() == true || BlockGrabbed.Grabbed == false)
            {
                BlockGrabbed.Release();
                Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.Idle);
                Player.ChangeState(new IdleState());
                return;
            }

            if (DataHandler.saveData.Settings.GrabOption == (int)GrabSettings.Auto)
            {
                //Increment the time holding the block only once the push delay time has expired or doesn't apply
                if (FromIdleState == false || ElapsedBlockMoveDelay >= Echidna.AutoBlockPushDelayTime)
                {
                    ElapsedTimeHoldingBlock += Time.ElapsedTime.TotalMilliseconds;
                }
                else
                {
                    ElapsedBlockMoveDelay += Time.ElapsedTime.TotalMilliseconds;
                }
            }

            //Check if the player presses a direction to move the block
            //The direction must be towards or away
            //Check for pushing
            if (ShouldPush() == true)
            {
                //If the direction isn't null, that means it can only move in a certain direction
                if (BlockGrabbed.BlockDirection != null)
                {
                    //Check if we're not pushing it in the same direction it can move
                    if (Player.FacingDir != BlockGrabbed.BlockDirection)
                    {
                        Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.PlayerAnimations.Grab);
                        return;
                    }
                }

                Vector2 rawDir = DirectionUtil.GetVector2ForDir(Player.FacingDir);
                Vector2 dir = rawDir * Player.Context.LevelSpace.TileSize;

                //Check if there's collision in the direction for the block
                ICollisionObj blockCollision = LevelGlobals.CheckCollision(dir, BlockGrabbed.CollisionRect, Player.Context.LevelCollision,
                        BlockFilter);

                //If there is collision, check if the block being pushed is a paired block and isn't colliding with its linked block
                //If the block is colliding with its linked block, check if anything is in the way of the linked block
                if (blockCollision == null || CheckPairedBlockMove(blockCollision, dir) == true)
                {
                    //Check if there's collision in the direction for the player that's not the block
                    ICollisionObj playerCollision = LevelGlobals.CheckCollision(dir, Player.CollisionRect, Player.Context.LevelCollision,
                        PlayerFilter);

                    //Don't do the paired block check here, since after we switched to
                    //tile-based movement, it's no longer possible for anything other than the block being grabbed to be in the player's way
                    //We technically don't need this check anymore, but we should keep it just in case
                    if (playerCollision == null)
                    {
                        //There's no collision that blocks, so push the block
                        //If the block is on Ice, only move the block and put the player back into idle
                        //Otherwise, move like normal
                        SurfaceTypes surface = Player.Context.SurfaceTiles.GetTileInRegion(BlockGrabbed.CollisionRect);

                        if (surface == SurfaceTypes.Ice)
                        {
                            //This isn't 100% accurate as it's an edge case, but it helps with anything that needs to move normally (Ex. Paired Blocks)
                            Player.InvokeBlockMoved(BlockGrabbed, dir, Player.MoveTime);

                            BlockGrabbed.ChangeState(new BlockIceMoveState(BlockGrabbed, rawDir));
                            BlockGrabbed.Release();

                            Player.ChangeState(new IdleState());
                        }
                        else
                        {
                            double time = Player.MoveTime;

                            Player.InvokeBlockMoved(BlockGrabbed, dir, time);

                            BlockGrabbed.ChangeState(new BlockMoveState(BlockGrabbed, dir, time, false));
                            if (EnumUtility.HasEnumVal((long)Player.MoveSmoothingOptions, (long)PlayerSmoothingOptions.BlockMove) == true)
                            {
                                BlockGrabbed.BlockFSM.Update();
                            }

                            Player.ChangeState(new PlayerBlockMoveState(BlockGrabbed, dir, time));
                            if (EnumUtility.HasEnumVal((long)Player.MoveSmoothingOptions, (long)PlayerSmoothingOptions.BlockMove) == true)
                            {
                                Player.CurState.Update();
                            }
                        }

                        return;
                    }
                    else
                    {
                        Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.PlayerAnimations.Grab);
                    }
                }
                else
                {
                    Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.PlayerAnimations.Grab);
                }
            }
            //Check for pulling
            else if (ShouldPull() == true)
            {
                //If the direction isn't null, that means it can only move in a certain direction
                if (BlockGrabbed.BlockDirection != null)
                {
                    Direction oppositeDir = DirectionUtil.GetOppositeDirection((Direction)BlockGrabbed.BlockDirection);
                    //Check if we're pulling it in the same direction it can move
                    if (Player.FacingDir != oppositeDir)
                    {
                        Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.PlayerAnimations.Grab);
                        return;
                    }
                }

                Vector2 dir = -DirectionUtil.GetVector2ForDir(Player.FacingDir) * Player.Context.LevelSpace.TileSize;

                //Check if there's collision in the direction for the player
                ICollisionObj playerCollision = LevelGlobals.CheckCollision(dir, Player.CollisionRect, CollisionTypes.Blocking, 
                    CollisionLayers.AllExceptPlayer, Player.Context.LevelCollision);

                //If there is collision, check if the block being pulled is a paired block and the player is colliding with its linked block
                //If the player is colliding with its linked block, check if anything is in the way of the linked block
                if (playerCollision == null || CheckPairedBlockMove(playerCollision, dir) == true)
                {
                    //Check if there's collision in the direction of the block that's not the player
                    ICollisionObj blockCollision = LevelGlobals.CheckCollision(dir, BlockGrabbed.CollisionRect,
                        Player.Context.LevelCollision, BlockFilter);

                    //Perform the paired block check for the block as well
                    //It's possible for both the player and the linked block to be in a larger paired block's way
                    if (blockCollision == null || CheckPairedBlockMove(blockCollision, dir) == true)
                    {
                        double time = Player.MoveTime;

                        //There's no collision, so pull the block
                        //If the block is in mud, pull it slower
                        SurfaceTypes playerSurface = Player.Context.SurfaceTiles.GetTileInRegion(BlockGrabbed.CollisionRect);
                        if (playerSurface == SurfaceTypes.Mud)
                        {
                            time = Player.GetMoveTimeWithModifier(Player.MudTimeModifier);
                        }

                        Player.InvokeBlockMoved(BlockGrabbed, dir, time);

                        BlockGrabbed.ChangeState(new BlockMoveState(BlockGrabbed, dir, time, true));
                        if (EnumUtility.HasEnumVal((long)Player.MoveSmoothingOptions, (long)PlayerSmoothingOptions.BlockMove) == true)
                        {
                            BlockGrabbed.BlockFSM.Update();
                        }

                        Player.ChangeState(new PlayerBlockMoveState(BlockGrabbed, dir, time));
                        if (EnumUtility.HasEnumVal((long)Player.MoveSmoothingOptions, (long)PlayerSmoothingOptions.BlockMove) == true)
                        {
                            Player.CurState.Update();
                        }
                        return;
                    }
                    else
                    {
                        Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.PlayerAnimations.Grab);
                    }
                }
                else
                {
                    Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.PlayerAnimations.Grab);
                }
            }
            else
            {
                Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.PlayerAnimations.Grab);
            }
        }

        private bool ShouldPull()
        {
            bool pressedDir = false;
            string inputAction = string.Empty;

            //Get the input action based on the direction the player is facing - in the case of pulling, it's the opposite direction
            switch (Player.FacingDir)
            {
                case Direction.Up: inputAction = InputActions.Down; break;
                case Direction.Down: inputAction = InputActions.Up; break;
                case Direction.Left: inputAction = InputActions.Right; break;
                case Direction.Right: inputAction = InputActions.Left; break;
                default: inputAction = string.Empty; break;
            }

            //Check the input
            if (string.IsNullOrEmpty(inputAction) == false)
            {
                if (DataHandler.saveData.Settings.InputOption == (int)PlayerInputPriorityOptions.Buffered
                    && Player.BufferedInputs.CheckAndConsumeInput(inputAction) == true)
                {
                    pressedDir = true;
                }
                else
                {
                    pressedDir = Input.GetButton(inputAction);
                }
            }

            //Return since the player didn't press anything to perform a pull
            if (pressedDir == false)
            {
                return false;
            }

            //Blocks cannot be pulled if the player is on ice
            SurfaceTypes playerSurface = Player.Context.SurfaceTiles.GetTile(Player.SurfaceCheckPos);
            if (playerSurface == SurfaceTypes.Ice)
            {
                return false;
            }

            return true;
        }

        private bool ShouldPush()
        {
            bool pressedDir = false;
            string inputAction = string.Empty;

            //Get the input action based on the direction the player is facing - in the case of pushing, it's the same direction
            switch (Player.FacingDir)
            {
                case Direction.Up: inputAction = InputActions.Up; break;
                case Direction.Down: inputAction = InputActions.Down; break;
                case Direction.Left: inputAction = InputActions.Left; break;
                case Direction.Right: inputAction = InputActions.Right; break;
                default: inputAction = string.Empty; break;
            }

            //Check the input
            if (string.IsNullOrEmpty(inputAction) == false)
            {
                if (DataHandler.saveData.Settings.InputOption == (int)PlayerInputPriorityOptions.Buffered
                    && Player.BufferedInputs.CheckAndConsumeInput(inputAction) == true)
                {
                    pressedDir = true;
                }
                else
                {
                    //If the Auto grab setting is used and the block delay hasn't passed, allow moving if the player just pressed the direction again
                    if (FromIdleState == true && DataHandler.saveData.Settings.GrabOption == (int)GrabSettings.Auto
                        && ElapsedBlockMoveDelay < Echidna.AutoBlockPushDelayTime)
                    {
                        pressedDir = Input.GetButtonDown(inputAction);
                    }
                    //Otherwise check for a hold
                    else
                    {
                        pressedDir = Input.GetButton(inputAction);
                    }
                }
            }

            //Return since the player didn't press anything to perform a push
            if (pressedDir == false)
            {
                return false;
            }
                
            //Blocks cannot be pushed if they're on mud surfaces
            SurfaceTypes blockSurface = Player.Context.SurfaceTiles.GetTileInRegion(BlockGrabbed.CollisionRect);
            if (blockSurface == SurfaceTypes.Mud)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks if the current block is a Paired Block and collided object is its linked block when attempting to move.
        /// If so, it checks if anything is in the way of the linked block.
        /// <para>This is in place to allow the player to move Paired Blocks in circumstances that wouldn't previously work.
        /// One example is if the player is sandwiched between two linked Paired Blocks and tries to pull one.</para>
        /// </summary>
        /// <param name="collisionHit">The initial collision hit.</param>
        /// <param name="moveDir">The direction to move in.</param>
        /// <returns>false if the collision hit isn't the current Paired Block's linked block,
        /// or it is and something is in the way of the linked block.</returns>
        private bool CheckPairedBlockMove(ICollisionObj collisionHit, in Vector2 moveDir)
        {
            if (BlockGrabbed.BlockType != BlockTypes.Paired || collisionHit.ObjectType != ObjectTypes.Block)
            {
                return false;
            }
            
            PairedBlock pairedBlock = (PairedBlock)BlockGrabbed;
            if (pairedBlock.PairedObj != collisionHit)
            {
                return false;
            }

            //Check for collision in this direction from the linked object
            ICollisionObj collision = LevelGlobals.CheckCollision(moveDir, collisionHit, CollisionTypes.Blocking,
                CollisionLayers.AllExceptPlayer & (~CollisionLayers.BlockExcept), Player.Context.LevelCollision);

            return collision == null;
        }

        private bool CustomBlockFilter(ICollisionObj collisionObj)
        {
            return (collisionObj != BlockGrabbed
                && collisionObj.CollisionType == CollisionTypes.Blocking && collisionObj != Player
                && collisionObj.CollisionLayer != CollisionLayers.BlockExcept);
        }

        private bool CustomPlayerFilter(ICollisionObj collisionObj)
        {
            return (collisionObj != BlockGrabbed
                && collisionObj.CollisionType == CollisionTypes.Blocking && collisionObj.CollisionLayer != CollisionLayers.Player);
        }
    }
}
