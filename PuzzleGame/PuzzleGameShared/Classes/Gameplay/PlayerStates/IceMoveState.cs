﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class IceMoveState : PlayerStateMachine
    {
        private Vector2 MoveDir = Vector2.Zero;

        private Vector2 PlayerStartPos = Vector2.Zero;
        private Vector2 PlayerEndPos = Vector2.Zero;

        private double MoveTime = 0d;
        private double ElapsedTime = 0d;

        public IceMoveState(in Vector2 moveDir)
        {
            MoveDir = moveDir;
        }

        public override void Enter()
        {
            MoveTime = Player.MoveTime;

            PlayerStartPos = Player.transform.Position;
            PlayerEndPos = PlayerStartPos + (MoveDir * Player.Context.LevelSpace.TileSize);

            Player.AnimationManager.PlayAnimation(AnimationGlobals.Idle);
            Player.PlayerState = PlayerStates.IceMove;
        }

        public override void Exit()
        {
            
        }

        public override void Update()
        {
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= MoveTime)
            {
                Player.transform.Position = PlayerEndPos;

                //Check which tile the player is on now after moving
                //If it's not ice, then go back to idle
                SurfaceTypes surface = Player.Context.SurfaceTiles.GetTile(Player.SurfaceCheckPos);
                if (surface != SurfaceTypes.Ice)
                {
                    Player.ChangeState(new IdleState());
                    return;
                }

                //Go back to idle if we hit something
                ICollisionObj coll = CheckCollision(MoveDir * Player.Context.LevelSpace.TileSize, CollisionTypes.Blocking, CollisionLayers.AllExceptPlayer);
                if (coll != null)
                {
                    Player.ChangeState(new IdleState());
                    return;
                }

                //Otherwise, continue sliding
                Player.ChangeState(new IceMoveState(MoveDir));
            }
            else
            {
                Player.transform.Position = Interpolation.Interpolate(PlayerStartPos, PlayerEndPos, ElapsedTime / MoveTime,
                    Interpolation.InterpolationTypes.Linear);
            }
        }
    }
}
