﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    public class PlayerMoveState : PlayerStateMachine
    {
        private Vector2 MoveAmount = Vector2.Zero;
        private double MoveTime = 0d;
        private double ElapsedTime = 0d;

        private Vector2 PlayerStartPos = Vector2.Zero;
        private Vector2 PlayerEndPos = Vector2.Zero;

        public PlayerMoveState(in Vector2 moveAmt, in double moveTime)
        {
            MoveAmount = moveAmt;
            MoveTime = moveTime;
        }

        public override void Enter()
        {
            //Initialize if zero; this is in place to work nicely with the stunned state
            if (PlayerStartPos == Vector2.Zero)
            {
                PlayerStartPos = Player.transform.Position;
                PlayerEndPos = PlayerStartPos + MoveAmount;
            }

            Player.PlayerState = PlayerStates.Walk;

            Player.AnimationManager.PlayAnimationIfDiff(AnimationGlobals.PlayerAnimations.Walk);
        }

        public override void Exit()
        {
            
        }

        public override void Update()
        {
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= MoveTime)
            {
                Player.transform.Position = PlayerEndPos;

                Vector2 dir = MoveAmount;
                dir.Normalize();

                //Check if the player is now on ice; if so, the player will slide automatically after moving
                SurfaceTypes surface = Player.Context.SurfaceTiles.GetTileInRegion(Player.CollisionRect);

                if (surface == SurfaceTypes.Ice)
                {
                    //Make sure nothing is in front of the player; if there is, act like normal since it's not sliding
                    ICollisionObj coll = CheckCollision(MoveAmount, CollisionTypes.Blocking, CollisionLayers.AllExceptPlayer);

                    if (coll == null)
                    {
                        //Make the player slide on ice
                        Player.ChangeState(new IceMoveState(dir));
                        return;
                    }
                }

                //Go to the idle state
                Player.ChangeState(new IdleState());
            }
            else
            {
                Player.transform.Position = Interpolation.Interpolate(PlayerStartPos, PlayerEndPos, ElapsedTime / MoveTime, Interpolation.InterpolationTypes.Linear);
            }
        }
    }
}
