/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class Block : ITransformable, IBlock, ICollisionObj, IUpdateable, ITintable, IRenderDepth
    {
        public static readonly Color IDColor = new Color(55, 55, 55, 255);

        private static readonly Color GroundParticleColor = new Color(237, 185, 137);
        private static readonly Color MudParticleColor = new Color(165, 139, 103);
        private static readonly Color IceParticleColor = Color.SkyBlue;

        public delegate void OnStateChanged(IFiniteStateMachine state);

        /// <summary>
        /// An event invoked when the <see cref="Block"/> changes state.
        /// </summary>
        public event OnStateChanged StateChangedEvent = null;

        public string ID { get; set; } = string.Empty;

        public PlayableContext Context { get; set; } = null;

        public Transform transform { get; private set; } = new Transform();

        public Sprite sprite { get; private set; } = null;

        //[Recordable(RecordableFlags.RecordFields)]
        public Sprite PatternSprite { get; private set; } = null;

        public ObjectTypes ObjectType => ObjectTypes.Block;

        [Recordable(RecordableFlags.None)]
        public CollisionTypes CollisionType { get; set; } = CollisionTypes.Blocking;

        public CollisionLayers CollisionLayer { get; set; } = CollisionLayers.Block;

        [Recordable(RecordableFlags.None)]
        public TriggerQueries TriggerQuery { get; private set; } = TriggerQueries.None;

        public BlockTypes BlockType { get; protected set; } = BlockTypes.Normal;

        public BlockPatterns BlockPattern { get; set; } = BlockPatterns.Square;

        public Direction? BlockDirection { get; set; } = null;

        public bool HasSound { get; protected set; } = true;

        [Recordable(RecordableFlags.None)]
        public BlockStates BlockState = BlockStates.None;

        [Recordable(RecordableFlags.Clone)]
        public IFiniteStateMachine BlockFSM { get; private set; } = null;

        [Recordable(RecordableFlags.None)]
        public Color TintColor { get; set; } = Color.White;

        [Recordable(RecordableFlags.None)]
        public float RenderDepth { get; set; } = LevelGlobals.BaseBlockRenderDepth;

        [Recordable(RecordableFlags.None)]
        public bool Grabbable { get; set; } = true;

        /// <summary>
        /// Tells if the block was initially grabbable upon spawning.
        /// </summary>
        //[Recordable(RecordableFlags.None)]
        public bool InitGrabbable { get; protected set; } = true;

        //[Recordable(RecordableFlags.None)]
        public bool Grabbed { get; private set; } = false;

        /// <summary>
        /// Whether the block is active or not.
        /// This is used by <see cref="BlockSwitch"/> to further indicate if they're gone or not.
        /// </summary>
        [Recordable(RecordableFlags.None)]
        public bool Active = true;

        private const float MinFade = .5f;
        private const double FadeTime = 400d;

        [Recordable(RecordableFlags.None)]
        private double ElapsedFadeTime = 0d;

        [Recordable(RecordableFlags.None)]
        private bool ObjInside = false;

        /// <summary>
        /// The collision scale of the Block.
        /// </summary>
        public Vector2 CollisionScale { get; protected set; } = Vector2.One;

        protected SimpleAnimation SpotOverlay = null;
        protected SimpleAnimation SpotOverlayIncorrect = null;
        protected const float SpotOverlayAlpha = .5f;
        protected static readonly Color BaseSpotOverlayColor = Color.White;

        [Recordable(RecordableFlags.None)]
        protected Color PatternColor = Color.White;

        [Recordable(RecordableFlags.None)]
        protected Rectangle? SurfaceDecalRect = null;

        protected SpriteFont IDFont = null;
        protected Vector2 IDOrigin = Vector2.Zero;

        [Recordable(RecordableFlags.None)]
        public bool DrawID = true;

        protected double TotalOverlayTime = 0d;

        [Recordable(RecordableFlags.RecordFields | RecordableFlags.AvoidSet)]
        public ParticleEngine particleEngine { get; private set; } = null;

        public virtual RectangleF CollisionRect
        {
            get
            {
                Rectangle spriteRect = sprite.SourceRect.Value;
                return new RectangleF(transform.Position.X - ((spriteRect.Width / 2) * CollisionScale.X),
                                      transform.Position.Y - ((spriteRect.Height / 2) * CollisionScale.Y),
                                      spriteRect.Width * CollisionScale.X, spriteRect.Height * CollisionScale.Y);
            }
        }

        public Block()
        {
            Texture2D tex = AssetManager.Instance.LoadTexture(ContentGlobals.LevelObjectsTex);
            Vector2 origin = new Vector2(.5f, .5f);
            SpotOverlay = new SimpleAnimation(new Sprite(tex, ContentGlobals.BlockCorrectAnim1Rect, origin), AnimTypes.Looping,
                new AnimationFrame(ContentGlobals.BlockCorrectAnim1Rect, 80d, origin),
                new AnimationFrame(ContentGlobals.BlockCorrectAnim2Rect, 80d, origin),
                new AnimationFrame(ContentGlobals.BlockCorrectAnim3Rect, 80d, origin),
                new AnimationFrame(ContentGlobals.BlockCorrectAnim2Rect, 80d, origin));
            SpotOverlayIncorrect = new SimpleAnimation(new Sprite(tex, ContentGlobals.BlockIncorrectAnim1Rect, origin), AnimTypes.Looping,
                new AnimationFrame(ContentGlobals.BlockIncorrectAnim1Rect, 80d, origin),
                new AnimationFrame(ContentGlobals.BlockIncorrectAnim2Rect, 80d, origin),
                new AnimationFrame(ContentGlobals.BlockIncorrectAnim3Rect, 80d, origin),
                new AnimationFrame(ContentGlobals.BlockIncorrectAnim2Rect, 80d, origin));
        }

        public Block(in Vector2 position, in BlockPatterns blockPattern, in Direction? blockDirection) : this()
        {
            transform.Position = position;

            BlockPattern = blockPattern;
            BlockDirection = blockDirection;

            Rectangle rect = GetPatternSourceRect(BlockPattern, BlockDirection);

            sprite = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.LevelObjectsTex), GetBlockSourceRect(BlockType, InitGrabbable));
            PatternSprite = new Sprite(sprite.Tex, rect);
        }

        public virtual void CleanUp()
        {
            StateChangedEvent = null;

            IDFont = null;
        }

        public void UpdateData()
        {
            TintColor = Color.White;
            PatternColor = Color.White;
            TriggerQuery = TriggerQueries.None;

            if (Grabbable == false)
            {
                TintColor = Color.DarkGray;
                PatternColor = Color.LightGray;
            }

            if (CollisionLayer == CollisionLayers.Player)
            {
                TintColor = Color.LightGreen;
                PatternColor = Color.LightGreen;
                TriggerQuery = TriggerQueries.Constant;
            }
            else if (CollisionLayer == CollisionLayers.BlockExcept)
            {
                TintColor = Color.Orange;
                PatternColor = Color.Orange;
                TriggerQuery = TriggerQueries.Constant;
            }

            SurfaceTypes surface = Context.SurfaceTiles.GetTileInRegion(CollisionRect);

            if (surface == SurfaceTypes.Mud)
            {
                SurfaceDecalRect = ContentGlobals.MudDecalRect;
            }
            else if (surface == SurfaceTypes.Ice)
            {
                SurfaceDecalRect = ContentGlobals.IceDecalRect;
            }
            else
            {
                SurfaceDecalRect = null;
            }

            SetParticleColorForSurface(surface);
        }

        public void Grab()
        {
            if (Grabbed == false)
            {
                Grabbed = true;
                OnGrab();
            }
        }

        protected virtual void OnGrab()
        {

        }

        public void Release()
        {
            if (Grabbed == true)
            {
                Grabbed = false;
                OnRelease();
            }
        }

        protected virtual void OnRelease()
        {

        }

        public virtual void Update()
        {
            TotalOverlayTime += Time.ElapsedTime.TotalMilliseconds;

            SpotOverlay.Update();
            SpotOverlayIncorrect.Update();

            //SpotOverlayAlpha = GetFadeTime(TotalOverlayTime);
            if (ObjInside == true)
            {
                ElapsedFadeTime = UtilityGlobals.Clamp(ElapsedFadeTime + Time.ElapsedTime.TotalMilliseconds, 0d, FadeTime);
            }
            else
            {
                ElapsedFadeTime = UtilityGlobals.Clamp(ElapsedFadeTime - Time.ElapsedTime.TotalMilliseconds, 0d, FadeTime);
            }

            if (ElapsedFadeTime <= 0d)
            {
                RenderDepth = LevelGlobals.BaseBlockRenderDepth;
            }
            else
            {
                if (CollisionLayer == CollisionLayers.Player)
                    RenderDepth = LevelGlobals.BasePlayerRenderDepth + .1f;
                else
                    RenderDepth = LevelGlobals.BaseBlockRenderDepth + .05f;
            }

            BlockFSM?.Update();

            particleEngine.Position = transform.Position;
            particleEngine.Update();
        }

        public void ChangeState(IFiniteStateMachine newState)
        {
            BlockFSM?.Exit();
            BlockFSM = newState;
            BlockFSM?.Enter();

            StateChangedEvent?.Invoke(newState);
        }

        public bool QueryTrigger(List<ICollisionObj> collisions)
        {
            if (CollisionLayer == CollisionLayers.Player)
            {
                ObjInside = Context.Player.CollisionRect.Intersects(CollisionRect);
            }
            else if (CollisionLayer == CollisionLayers.BlockExcept)
            {
                ObjInside = false;

                for (int i = 0; i < collisions.Count; i++)
                {
                    ICollisionObj coll = collisions[i];
                    if (coll == this || coll.CollisionType == CollisionTypes.Trigger || coll.CollisionLayer != CollisionLayers.Block)
                        continue;

                    if (coll.CollisionRect.Intersects(CollisionRect) == true)
                    {
                        ObjInside = true;
                        break;
                    }
                }
            }

            return false;
        }

        public virtual void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            if (initInfo.Properties.TryGetValue(LevelGlobals.BlockPatternKey, out string value) == true)
            {
                if (Enum.TryParse(value, true, out BlockPatterns blockPattern) == true)
                {
                    BlockPattern = blockPattern;
                }
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.ScaleKey, out string scaleStr) == true)
            {
                if (float.TryParse(scaleStr, out float scale) == true)
                {
                    transform.Scale = new Vector2(scale);
                }
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.DirectionKey, out string dirVal) == true)
            {
                if (Enum.TryParse(dirVal, true, out Direction blockDir) == true)
                {
                    BlockDirection = blockDir;
                }
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.IDKey, out string idVal) == true)
            {
                ID = idVal;

                IDFont = AssetManager.Instance.LoadFont(ContentGlobals.SpecialFont13px);
                IDOrigin = IDFont.GetCenterOrigin(ID);
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.ShowIDKey, out string showID) == true)
            {
                bool.TryParse(showID, out DrawID);
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.CollisionLayerKey, out string typeVal) == true)
            {
                if (Enum.TryParse(typeVal, true, out CollisionLayers collisionLayer) == true)
                {
                    CollisionLayer = collisionLayer;
                }
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.GrabbableKey, out string grabVal) == true)
            {
                if (bool.TryParse(grabVal, out bool grabbable) == true)
                {
                    Grabbable = grabbable;
                }
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.HasSoundKey, out string hasSoundVal) == true)
            {
                if (bool.TryParse(hasSoundVal, out bool hasSound) == true)
                {
                    HasSound = hasSound;
                }
            }

            InitGrabbable = Grabbable;

            CollisionScale = transform.Scale;
            transform.Position = initInfo.Position + (context.LevelSpace.TileSizeHalf * transform.Scale);

            sprite = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.LevelObjectsTex), GetBlockSourceRect(BlockType, InitGrabbable));
            if (BlockType != BlockTypes.Pipe && (BlockPattern != BlockPatterns.None || BlockDirection != null))
            {
                Rectangle rect = GetPatternSourceRect(BlockPattern, BlockDirection);
                PatternSprite = new Sprite(sprite.Tex, rect);
            }

            Sprite particleSprite = new Sprite(sprite.Tex, ContentGlobals.BlockParticleRect);
            particleEngine = new ParticleEngine(5 * (int)transform.Scale.X, transform.Position, particleSprite, RenderDepth - .01f, 300d, 800d);
            particleEngine.EmissionRate = 16d;
            particleEngine.MinAngularVelocity = UtilityGlobals.ToRadians(90f) * 60f;
            particleEngine.MaxAngularVelocity = UtilityGlobals.ToRadians(90f) * 60f;
            particleEngine.MinParticleLife = 84d;
            particleEngine.MaxParticleLife = 101d;
            particleEngine.PrewarmParticles();
            AdjustParticleDirection(Direction.Up);
            particleEngine.Stop(true);
        }

        public void AdjustParticleDirection(in Direction moveDirection)
        {
            RectangleF collisionRect = CollisionRect;
            const float accelConst = .1f * 60f;
            Vector2 scaleMin = new Vector2(3f, 2f);
            Vector2 scaleMax = new Vector2(4f, 3f);

            if (moveDirection == Direction.Up)
            {
                int width = (int)(collisionRect.Width / 2);

                particleEngine.MinPositionOffset = new Vector2(-width + 2 + scaleMax.X, collisionRect.Height / 2);
                particleEngine.MaxPositionOffset = new Vector2(width - 2 - scaleMax.X, particleEngine.MinPositionOffset.Y);
                particleEngine.MinVelocity = new Vector2(0f, .025f) * 60f;
                particleEngine.MaxVelocity = new Vector2(0f, .05f) * 60f;
                particleEngine.MinInitScale = scaleMin;
                particleEngine.MaxInitScale = scaleMax;
                particleEngine.MinAcceleration = new Vector2(-accelConst, 0f);
                particleEngine.MaxAcceleration = new Vector2(accelConst, 0f);
            }
            else if (moveDirection == Direction.Down)
            {
                int width = (int)(collisionRect.Width / 2);

                particleEngine.MinPositionOffset = new Vector2(-width + 2 + scaleMax.X, -collisionRect.Height / 2);
                particleEngine.MaxPositionOffset = new Vector2(width - 2 - scaleMax.X, particleEngine.MinPositionOffset.Y);
                particleEngine.MinVelocity = new Vector2(0f, -.05f) * 60f;
                particleEngine.MaxVelocity = new Vector2(0f, -.025f) * 60f;
                particleEngine.MinInitScale = scaleMin;
                particleEngine.MaxInitScale = scaleMax;
                particleEngine.MinAcceleration = new Vector2(-accelConst, 0f);
                particleEngine.MaxAcceleration = new Vector2(accelConst, 0f);
            }
            else if (moveDirection == Direction.Left)
            {
                int height = (int)(collisionRect.Height / 2);

                particleEngine.MinPositionOffset = new Vector2(collisionRect.Width / 2, -height + 2 + scaleMax.Y);
                particleEngine.MaxPositionOffset = new Vector2(particleEngine.MinPositionOffset.X, height - 2 - scaleMax.Y);
                particleEngine.MinVelocity = new Vector2(.025f, 0f) * 60f;
                particleEngine.MaxVelocity = new Vector2(.05f, 0f) * 60f;
                particleEngine.MinInitScale = scaleMin.SwapXY();
                particleEngine.MaxInitScale = scaleMax.SwapXY();
                particleEngine.MinAcceleration = new Vector2(0f, -accelConst);
                particleEngine.MaxAcceleration = new Vector2(0f, accelConst);
            }
            else
            {
                int height = (int)(collisionRect.Height / 2);

                particleEngine.MinPositionOffset = new Vector2(-collisionRect.Width / 2, -height + 2 + scaleMax.Y);
                particleEngine.MaxPositionOffset = new Vector2(particleEngine.MinPositionOffset.X, height - 2 - scaleMax.Y);
                particleEngine.MinVelocity = new Vector2(-.05f, 0f) * 60f;
                particleEngine.MaxVelocity = new Vector2(-.025f, 0f) * 60f;
                particleEngine.MinInitScale = scaleMin.SwapXY();
                particleEngine.MaxInitScale = scaleMax.SwapXY();
                particleEngine.MinAcceleration = new Vector2(0f, -accelConst);
                particleEngine.MaxAcceleration = new Vector2(0f, accelConst);
            }
        }

        public static Color GetParticleColorForSurface(in SurfaceTypes surface)
        {
            switch (surface)
            {
                default:
                case SurfaceTypes.Ground: return GroundParticleColor;
                case SurfaceTypes.Mud: return MudParticleColor;
                case SurfaceTypes.Ice: return IceParticleColor;
            }
        }

        public void SetParticleColorForSurface(in SurfaceTypes surface)
        {
            particleEngine.ParticleColor = GetParticleColorForSurface(surface);
        }

        public virtual void PostInitialize(PlayableContext context)
        {
            UpdateData();

            if (string.IsNullOrEmpty(ID) == true) return;

            for (int i = 0; i < context.Switches.Count; i++)
            {
                if (context.Switches[i].ID == ID && context.Switches[i].SwitchType == SwitchTypes.Standard)
                {
                    ChangeState(new BlockSwitchState(this, Context.Switches[i]));

                    break;
                }
            }
        }

        public virtual void Render()
        {
            float alpha = (1f - (float)((ElapsedFadeTime * MinFade) / FadeTime));
            Color color = TintColor * alpha;
            RenderBlock(color);

            if (SurfaceDecalRect != null)
            {
                Rectangle rect = SurfaceDecalRect.Value;
                Rectangle spriteRect = sprite.SourceRect.Value;
                //Instead of using the bottom of the block's collision box, we manually calculate the position from the bottom of the sprite
                //This makes the decal render in the correct location while the block is being visually scaled (Ex. warping)
                RenderingManager.Instance.DrawSprite(sprite.Tex, new Vector2(transform.Position.X, transform.Position.Y + ((spriteRect.Height / 2) * transform.Scale.Y)), rect, Color.White, 0f,
                    rect.GetOrigin(.5f, 1f), transform.Scale, SpriteEffects.None, RenderDepth + .0005f);
            }

            if (PatternSprite != null && (BlockPattern != BlockPatterns.None || BlockDirection != null))
            {
                //Draw a shadow for the pattern so it's more easily shown on blocks
                //RenderingManager.Instance.DrawSprite(PatternSprite.Tex, transform.Position + new Vector2(1f, 1f), PatternSprite.SourceRect,
                //    Color.Black * (float)(color.A / (float)byte.MaxValue) * .75f, transform.Rotation, PatternSprite.GetOrigin(), transform.Scale, SpriteEffects.None, RenderDepth + .001f);

                float patternAlpha = alpha * (float)(TintColor.A / (float)byte.MaxValue);

                RenderingManager.Instance.DrawSprite(PatternSprite.Tex, transform.Position, PatternSprite.SourceRect, PatternColor * patternAlpha, transform.Rotation,
                    PatternSprite.GetOrigin(), transform.Scale, SpriteEffects.None, RenderDepth + .002f);
            }

            //RenderingManager.Instance.spriteBatch.DrawHollowRect(AssetManager.Instance.LoadTexture(ContentGlobals.UITex),
            //    ContentGlobals.BoxRect, CollisionRect, Color.White, RenderDepth + .002f, 1);

            RenderOverlay();

            //Don't draw the ID if we shouldn't
            if (DrawID == true && string.IsNullOrEmpty(ID) == false)
            {
                RenderingManager.Instance.spriteBatch.DrawStringOutline(1f, Color.White * .8f, -.001f, -.002f, -.003f, -.004f,
                    IDFont, ID, CollisionRect.TopRight, IDColor, 0f, IDOrigin, 1f, SpriteEffects.None, RenderDepth + .02f);
            }

            particleEngine?.Render();
        }

        protected virtual void RenderBlock(in Color color)
        {
            RenderingManager.Instance.DrawSprite(sprite.Tex, transform.Position, sprite.SourceRect, color, transform.Rotation,
                sprite.GetOrigin(), transform.Scale, SpriteEffects.None, RenderDepth);
        }

        protected virtual void RenderOverlay()
        {
            if (BlockState == BlockStates.CorrectSpot || BlockState == BlockStates.WrongSpot)
            {
                Color overlayColor = BaseSpotOverlayColor * SpotOverlayAlpha;
                Sprite sprite = (BlockState == BlockStates.CorrectSpot) ? SpotOverlay.SpriteToChange : SpotOverlayIncorrect.SpriteToChange;
                RenderingManager.Instance.DrawSprite(sprite.Tex, transform.Position, sprite.SourceRect, overlayColor, 0f, sprite.GetOrigin(),
                    transform.Scale, SpriteEffects.None, RenderDepth + .01f);
            }
        }

        //public static float GetFadeTime(in double totalAlphaTime)
        //{
        //    return (float)UtilityGlobals.PingPong(totalAlphaTime / 400d, 0f, 1f);
        //}

        public virtual Rectangle GetBlockSourceRect(in BlockTypes blockType, in bool initGrabbable)
        {
            if (initGrabbable == false)
            {
                return ContentGlobals.BlockUngrabbableRect;
            }

            return ContentGlobals.BlockNormalRect;
        }

        public static Rectangle GetPatternSourceRect(in BlockPatterns blockPattern, in Direction? blockDirection)
        {
            Rectangle rect = ContentGlobals.BlockPatternStartRect;
            if (blockPattern == BlockPatterns.None)
            {
                if (blockDirection != null)
                {
                    rect.X += ((EnumUtility.GetValues<BlockPatterns>.EnumValues.Length - 1) + (int)blockDirection) * 32;
                }
            }
            else
            {
                rect.X += ((int)blockPattern - 1) * 32;
            }

            return rect;
        }
    }
}