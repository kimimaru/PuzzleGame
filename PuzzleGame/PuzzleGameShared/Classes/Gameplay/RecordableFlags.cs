/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// The flags for recording objects.
    /// <para>This is a bitwise field.</para>
    /// </summary>
    [Flags]
    public enum RecordableFlags
    {
        /// <summary>
        /// No special properties - the current value will be recorded.
        /// </summary>
        None = 0,

        /// <summary>
        /// A clone of the value will be recorded.
        /// </summary>
        Clone = 1 << 0,

        /// <summary>
        /// The fields and properties of the value will be recorded.
        /// </summary>
        RecordFields = 1 << 1,

        /// <summary>
        /// The value will not be set to the field and property of the object.
        /// This is a minor performance gain for reference types that are not cloned but have their fields and properties recorded.
        /// </summary>
        AvoidSet = 1 << 2,

        /// <summary>
        /// The value will be added to the main object list that's processed.
        /// </summary>
        AddToList = 1 << 3,

        /// <summary>
        /// The value will not be recorded in the data of the object it belongs to.
        /// </summary>
        DontRecordInBelongingObject = 1 << 4
    }
}