﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The main protagonist of the game.
    /// </summary>
    public sealed class Echidna : ICollisionObj, ITintable, IUpdateable, IDirectional, IRenderDepth
    {
        public delegate void OnBlockMoved(Block blockMoved, in Vector2 moveAmount, in double moveTime);
        public event OnBlockMoved BlockMovedEvent = null;

        public delegate void OnHitByStrongRock();
        public event OnHitByStrongRock HitByStrongRockEvent = null;

        public delegate void OnStateChanged(PlayerStateMachine newState);
        public event OnStateChanged StateChangedEvent = null;

        public Transform transform { get; private set; } = new Transform();
        public Color TintColor { get; set; } = Color.White;
        public float RenderDepth { get; set; } = LevelGlobals.BasePlayerRenderDepth;

        [Recordable(RecordableFlags.None)]
        public Direction FacingDir { get; set; } = Direction.Down;

        public SpriteEffects SpriteFlip = SpriteEffects.None;

        public TriggerQueries TriggerQuery { get; private set; } = TriggerQueries.None;

        public Sprite sprite { get; private set; } = null;

        /// <summary>
        /// The base time it takes the player to move one tile.
        /// </summary>
        public const double BaseMoveTime = 214d;

        /// <summary>
        /// The time it takes the player to move one tile.
        /// </summary>
        public double MoveTime = BaseMoveTime;

        /// <summary>
        /// The time modifier for moving in mud.
        /// </summary>
        public readonly float MudTimeModifier = 1.1f;

        /// <summary>
        /// How long the player should be hold against a block to grab it if <see cref="GrabSettings.Auto"/> is used.
        /// </summary>
        public const double AutoBlockGrabTime = 1d;

        /// <summary>
        /// How long the player should wait to release a block if <see cref="GrabSettings.Auto"/> is used.
        /// </summary>
        public const double AutoBlockReleaseTime = 350d;

        /// <summary>
        /// How long the player should wait to release a block if <see cref="GrabSettings.Auto"/> is used after the <see cref="AutoBlockPushDelayTime"/>.
        /// </summary>
        public const double AutoBlockReleasePostDelayTime = 150d;

        /// <summary>
        /// How long the player must wait before being able to push a block from idle if <see cref="GrabSettings.Auto"/> is used.
        /// </summary>
        public const double AutoBlockPushDelayTime = 300d;

        /// <summary>
        /// The player's movement smoothing options.
        /// </summary>
        public PlayerSmoothingOptions MoveSmoothingOptions = PlayerSmoothingOptions.Walk | PlayerSmoothingOptions.BlockMove;

        public string ID { get; set; } = "Player";
        public PlayableContext Context { get; set; } = null;
        public ObjectTypes ObjectType => ObjectTypes.Player;
        public CollisionTypes CollisionType { get; set; } = CollisionTypes.Trigger;
        public CollisionLayers CollisionLayer { get; private set; } = CollisionLayers.Player;

        /// <summary>
        /// The rectangle used for world collision (Ex. walls).
        /// </summary>
        public RectangleF CollisionRect
        {
            get
            {
                const int size = 32;
                return new RectangleF(transform.Position.X - ((size / 2) * 1f/*transform.Scale.X*/),
                                      (transform.Position.Y - ((size / 2) * 1f/*transform.Scale.Y*/)),
                                      size * 1f/*transform.Scale.X*/, size * 1f/*transform.Scale.Y*/);
            }
        }

        /// <summary>
        /// The point used for checking collision with the tile engine for surfaces.
        /// </summary>
        public Vector2 SurfaceCheckPos => CollisionRect.Center;

        /// <summary>
        /// The rectangle used for grabbing blocks.
        /// A rectangle for grabbing in either the X or Y direction is returned depending on the direction the player is facing.
        /// </summary>
        public RectangleF GrabRect
        {
            get
            {
                switch(FacingDir)
                {
                    case Direction.Left:
                    case Direction.Right: return GrabRectX;
                    case Direction.Down:
                    case Direction.Up:
                    default: return GrabRectY;
                }
            }
        }

        /// <summary>
        /// The rectangle used for grabbing blocks in the X direction.
        /// </summary>
        private RectangleF GrabRectX
        {
            get
            {
                RectangleF rect = CollisionRect;
                return new RectangleF(rect.X, rect.Center.Y, rect.Width, 1);
            }
        }

        /// <summary>
        /// The rectangle used for grabbing blocks in the Y direction.
        /// </summary>
        private RectangleF GrabRectY
        {
            get
            {
                RectangleF rect = CollisionRect;
                return new RectangleF(rect.Center.X, rect.Y, 1, rect.Height);
            }
        }

        public PlayerStateMachine CurState { get; private set; } = null;

        public AnimManager AnimationManager { get; private set; } = null;

        /// <summary>
        /// Tells the state the player is in.
        /// </summary>
        public PlayerStates PlayerState = PlayerStates.Idle;

        /// <summary>
        /// An offset to draw the player at.
        /// </summary>
        public Vector2 DrawOffset = Vector2.Zero;

        private const double InputBufferTime = (((1d / 60d) * 1000d) * 12d) + 1d;
        public readonly InputBuffer BufferedInputs = null;

        public Echidna() : this(new IdleState())
        {
            
        }

        public Echidna(PlayerStateMachine startingState)
        {
            sprite = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.EchidnaTex), ContentGlobals.EchidnaIdleAnimURect);

            transform.Scale = new Vector2(2f);

            AnimationManager = new AnimManager(sprite, 4);
            DefineAnimations();

            BufferedInputs = new InputBuffer(InputBufferTime);

            ChangeState(startingState);
        }

        public void CleanUp()
        {
            BlockMovedEvent = null;
            HitByStrongRockEvent = null;
            StateChangedEvent = null;
        }

        public void InvokeHitByStrongRock()
        {
            HitByStrongRockEvent?.Invoke();
        }

        public void InvokeBlockMoved(Block blockGrabbed, in Vector2 moveAmount, in double moveTime)
        {
            BlockMovedEvent?.Invoke(blockGrabbed, moveAmount, moveTime);
        }

        public void ChangeState(PlayerStateMachine newState)
        {
            CurState?.Exit();

            CurState = newState;
            CurState.SetPlayer(this);

            CurState.Enter();

            StateChangedEvent?.Invoke(newState);
        }

        public void Update()
        {
            //Always update the input buffer so its behavior is consistent when toggled on and off
            BufferedInputs.Update();

            CurState.Update();
            AnimationManager.CurrentAnim.Update();
        }

        public void Render()
        {
            RenderingManager.Instance.DrawSprite(sprite.Tex, transform.Position + DrawOffset, sprite.SourceRect, TintColor, transform.Rotation, sprite.GetOrigin(), transform.Scale, SpriteFlip, RenderDepth);
            //RenderingManager.Instance.spriteBatch.DrawHollowRect(AssetManager.Instance.LoadTexture(ContentGlobals.UITex), ContentGlobals.BoxRect, CollisionRect, Color.Blue, RenderDepth + .02f, 1);
            //RenderingManager.Instance.DrawSprite(AssetManager.Instance.LoadTexture(ContentGlobals.UITex), SurfaceCheckPos, ContentGlobals.BoxRect, Color.White, 0f, Vector2.Zero, Vector2.One, SpriteEffects.None, RenderDepth + .01f);
            //RenderingManager.Instance.spriteBatch.DrawHollowRect(AssetManager.Instance.LoadTexture(ContentGlobals.UITex), ContentGlobals.BoxRect, GrabRect, Color.Green, RenderDepth + .03f, 1);
            CurState.Render();
        }

        private void DefineAnimations()
        {
            AnimationManager.AddAnimation(AnimationGlobals.Idle, new DirectionalAnimation(sprite, this, AnimTypes.Normal,
                new DirAnimationFrame(0d, ContentGlobals.EchidnaIdleAnimURect, ContentGlobals.EchidnaIdleAnimDRect,
                    ContentGlobals.EchidnaIdleAnimLRect, ContentGlobals.EchidnaIdleAnimRRect)));
            
            AnimationManager.AddAnimation(AnimationGlobals.PlayerAnimations.Walk, new DirectionalAnimation(sprite, this, AnimTypes.Looping,
                new DirAnimationFrame(117d, ContentGlobals.EchidnaWalkAnimU1Rect, ContentGlobals.EchidnaWalkAnimD1Rect,
                    ContentGlobals.EchidnaWalkAnimL1Rect, ContentGlobals.EchidnaWalkAnimR1Rect),
                new DirAnimationFrame(117d, ContentGlobals.EchidnaWalkAnimU2Rect, ContentGlobals.EchidnaWalkAnimD2Rect,
                    ContentGlobals.EchidnaWalkAnimL2Rect, ContentGlobals.EchidnaWalkAnimR2Rect),
                new DirAnimationFrame(117d, ContentGlobals.EchidnaWalkAnimU3Rect, ContentGlobals.EchidnaWalkAnimD3Rect,
                    ContentGlobals.EchidnaWalkAnimL3Rect, ContentGlobals.EchidnaWalkAnimR3Rect),
                new DirAnimationFrame(117d, ContentGlobals.EchidnaWalkAnimU2Rect, ContentGlobals.EchidnaWalkAnimD2Rect,
                    ContentGlobals.EchidnaWalkAnimL2Rect, ContentGlobals.EchidnaWalkAnimR2Rect)));

            AnimationManager.AddAnimation(AnimationGlobals.PlayerAnimations.Grab, new DirectionalAnimation(sprite, this, AnimTypes.Normal,
                new DirAnimationFrame(0d, ContentGlobals.EchidnaGrabAnimURect, ContentGlobals.EchidnaGrabAnimDRect,
                    ContentGlobals.EchidnaGrabAnimLRect, ContentGlobals.EchidnaGrabAnimRRect)));

            AnimationManager.AddAnimation(AnimationGlobals.PlayerAnimations.GrabWalk, new DirectionalAnimation(sprite, this, AnimTypes.Looping,
                new DirAnimationFrame(117d, ContentGlobals.EchidnaGrabWalkAnimU1Rect, ContentGlobals.EchidnaGrabWalkAnimD1Rect,
                    ContentGlobals.EchidnaGrabWalkAnimL1Rect, ContentGlobals.EchidnaGrabWalkAnimR1Rect),
                new DirAnimationFrame(117d, ContentGlobals.EchidnaGrabWalkAnimU2Rect, ContentGlobals.EchidnaGrabWalkAnimD2Rect,
                    ContentGlobals.EchidnaGrabWalkAnimL2Rect, ContentGlobals.EchidnaGrabWalkAnimR2Rect),
                new DirAnimationFrame(117d, ContentGlobals.EchidnaGrabWalkAnimU3Rect, ContentGlobals.EchidnaGrabWalkAnimD3Rect,
                    ContentGlobals.EchidnaGrabWalkAnimL3Rect, ContentGlobals.EchidnaGrabWalkAnimR3Rect),
                new DirAnimationFrame(117d, ContentGlobals.EchidnaGrabWalkAnimU2Rect, ContentGlobals.EchidnaGrabWalkAnimD2Rect,
                    ContentGlobals.EchidnaGrabWalkAnimL2Rect, ContentGlobals.EchidnaGrabWalkAnimR2Rect)));

            AnimationManager.AddAnimation(AnimationGlobals.PlayerAnimations.Success, new DirectionalAnimation(sprite, this, AnimTypes.Normal,
                new DirAnimationFrame(0d, ContentGlobals.EchidnaSuccessAnimURect, ContentGlobals.EchidnaSuccessAnimDRect,
                ContentGlobals.EchidnaSuccessAnimLRect, ContentGlobals.EchidnaSuccessAnimRRect)));

            AnimationManager.AddAnimation(AnimationGlobals.PlayerAnimations.Sleep, new DirectionalAnimation(sprite, this, AnimTypes.Looping,
                new DirAnimationFrame(800d, ContentGlobals.EchidnaSleepAnimU1Rect, ContentGlobals.EchidnaSleepAnimD1Rect,
                    ContentGlobals.EchidnaSleepAnimL1Rect, ContentGlobals.EchidnaSleepAnimR1Rect),
                new DirAnimationFrame(150d, ContentGlobals.EchidnaSleepAnimU2Rect, ContentGlobals.EchidnaSleepAnimD2Rect,
                    ContentGlobals.EchidnaSleepAnimL2Rect, ContentGlobals.EchidnaSleepAnimR2Rect),
                new DirAnimationFrame(800d, ContentGlobals.EchidnaSleepAnimU3Rect, ContentGlobals.EchidnaSleepAnimD3Rect,
                    ContentGlobals.EchidnaSleepAnimL3Rect, ContentGlobals.EchidnaSleepAnimR3Rect),
                new DirAnimationFrame(150d, ContentGlobals.EchidnaSleepAnimU2Rect, ContentGlobals.EchidnaSleepAnimD2Rect,
                    ContentGlobals.EchidnaSleepAnimL2Rect, ContentGlobals.EchidnaSleepAnimR2Rect)));
        }

        public bool QueryTrigger(List<ICollisionObj> collisions)
        {
            return false;
        }

        public void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            transform.Position = initInfo.Position + context.LevelSpace.TileSizeHalf;
        }

        public void PostInitialize(PlayableContext context)
        {

        }

        /// <summary>
        /// Gets the player's total move time with a modifier applied.
        /// </summary>
        /// <param name="modifier">A modifier from 0 to 1 acting as a percentage.</param>
        /// <returns>The move time with the modifier applied.</returns>
        public double GetMoveTimeWithModifier(in double modifier)
        {
            return Math.Round(MoveTime * modifier);
        }
    }
}
