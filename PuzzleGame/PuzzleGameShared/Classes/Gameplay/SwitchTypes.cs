/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// The types of switches.
    /// </summary>
    public enum SwitchTypes
    {
        /// <summary>
        /// A standard switch that moves blocks.
        /// </summary>
        Standard,

        /// <summary>
        /// A toggle switch that makes blocks appear and disappear.
        /// </summary>
        Block
    }
}