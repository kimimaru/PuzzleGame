﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class BlockChangeCollisionState : IFiniteStateMachine
    {
        private Block BlockChanged = null;
        private CollisionLayers collisionObjType = CollisionLayers.Block;

        public BlockChangeCollisionState(Block blockChanged, in CollisionLayers collisionobjType)
        {
            BlockChanged = blockChanged;
            collisionObjType = collisionobjType;
        }

        public void Enter()
        {
            //Change the collision type then exit
            BlockChanged.CollisionLayer = collisionObjType;
            BlockChanged.UpdateData();

            BlockChanged.ChangeState(null);
        }

        public void Exit()
        {
            BlockChanged = null;
        }

        public void Update()
        {
            
        }
    }
}
