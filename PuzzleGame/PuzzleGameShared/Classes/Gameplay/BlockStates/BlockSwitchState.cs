﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace PuzzleGame
{
    public class BlockSwitchState : IFiniteStateMachine
    {
        private LevelGlobals.CollisionFilter SwitchCollisionFilter = null;

        private Block BlockMoved = null;

        private Switch SwitchAssociated = null;

        private SoundEffect MoveSound = null;
        private SoundEffect IceMoveSound = null;

        private Vector2 ObjStartPos = Vector2.Zero;
        private Vector2 ObjEndPos = Vector2.Zero;

        private const double MinMoveSoundPlayTime = 101d;
        private double MoveSoundPlayTime = 250d;

        private double ElapsedTime = 0d;
        private double ElapsedSoundTime = 0d;

        public BlockSwitchState(Block blockMoved, Switch switchAssociated)
        {
            BlockMoved = blockMoved;

            SwitchAssociated = switchAssociated;
        }

        public void Enter()
        {
            //Since the block is controlled by the switch, make it not grabbable
            BlockMoved.Grabbable = false;
            BlockMoved.sprite.SourceRect = BlockMoved.GetBlockSourceRect(BlockMoved.BlockType, BlockMoved.InitGrabbable);
            //if (BlockMoved.PatternSprite != null)
            //{
            //    BlockMoved.PatternSprite.SourceRect = Block.GetPatternSourceRect(BlockMoved.BlockPattern, BlockMoved.BlockDirection);
            //}

            BlockMoved.UpdateData();

            //Make the block draw its ID if its switch does
            BlockMoved.DrawID = SwitchAssociated.DrawID;

            ObjStartPos = BlockMoved.transform.Position;
            ObjEndPos = ObjStartPos + ((DirectionUtil.GetVector2ForDir(SwitchAssociated.PairedObjMoveDir) * BlockMoved.Context.LevelSpace.TileSize) * SwitchAssociated.PairedObjMoveAmount);

            SwitchCollisionFilter = CollisionCheck;

            MoveSound = AssetManager.Instance.LoadSound(ContentGlobals.BlockMoveSound);
            IceMoveSound = AssetManager.Instance.LoadSound(ContentGlobals.BlockIceMoveSound);

            if (SwitchAssociated.PairedObjMoveAmount == 0)
            {
                MoveSoundPlayTime = 0d;
            }
            else
            {
                //Play the move sound as often as the block moves tiles, within reason
                double playRate = SwitchAssociated.MoveTime / (double)SwitchAssociated.PairedObjMoveAmount;
                MoveSoundPlayTime = UtilityGlobals.Clamp(playRate, MinMoveSoundPlayTime, double.MaxValue);
            }
        }

        public void Exit()
        {
            BlockMoved.sprite.SourceRect = BlockMoved.GetBlockSourceRect(BlockMoved.BlockType, BlockMoved.InitGrabbable);
            //if (BlockMoved.PatternSprite != null)
            //{
            //    BlockMoved.PatternSprite.SourceRect = Block.GetPatternSourceRect(BlockMoved.BlockPattern, BlockMoved.BlockDirection);
            //}

            BlockMoved.DrawID = false;

            //BlockMoved = null;
            //SwitchAssociated = null;
            //
            //SwitchCollisionFilter = null;
        }

        public void Update()
        {
            //Store temp movement information for the paired object
            double newTime = ElapsedTime + (Time.ElapsedTime.TotalMilliseconds * SwitchAssociated.Modifier);

            Vector2 pos = Vector2.Zero;

            if (newTime >= SwitchAssociated.MoveTime)
            {
                newTime = SwitchAssociated.MoveTime;
                pos = ObjEndPos;
            }
            else if (newTime <= 0d)
            {
                newTime = 0d;
                pos = ObjStartPos;
            }
            else
            {
                pos = Interpolation.Interpolate(ObjStartPos, ObjEndPos, newTime / SwitchAssociated.MoveTime, Interpolation.InterpolationTypes.Linear);
            }

            //Check if there's any collision
            ICollisionObj collisionObj = null;

            //Optimization: for single-tile blocks, just check for any collision
            if (BlockMoved.transform.Scale == Vector2.One)
            {
                collisionObj = LevelGlobals.CheckCollision(pos - BlockMoved.transform.Position, BlockMoved.CollisionRect,
                    BlockMoved.Context.LevelCollision, SwitchCollisionFilter);
            }
            //Larger blocks need to check the closest collision
            //If they don't, they'll clamp to the first object they detect collision with regardless if another one is in the way, which leads to bugs
            else
            {
                collisionObj = LevelGlobals.CheckClosestCollision(pos - BlockMoved.transform.Position, BlockMoved.CollisionRect,
                    BlockMoved.Context.LevelCollision, SwitchCollisionFilter);
            }

            //Get the direction we're moving
            Direction moveDir = SwitchAssociated.PairedObjMoveDir;
            if (SwitchAssociated.Modifier < 0d)
            {
                moveDir = DirectionUtil.GetOppositeDirection(moveDir);
            }

            if (ElapsedTime != newTime)
            {
                BlockMoved.UpdateData();
                BlockMoved.particleEngine.Resume();
                BlockMoved.AdjustParticleDirection(moveDir);
            }
            else
            {
                if (BlockMoved.particleEngine.IsEmitting == true)
                {
                    BlockMoved.particleEngine.Stop(false);
                }
            }

            //If not, move the object and adjust the time
            //This prevents it from moving when there's an object in the way
            if (collisionObj == null)
            {
                //Update the sound time and play the block movement sounds only if the block is moving
                if (ElapsedTime != newTime)
                {
                    ElapsedSoundTime += Time.ElapsedTime.TotalMilliseconds;

                    //Don't play unless over the threshold
                    if (ElapsedSoundTime >= MoveSoundPlayTime)
                    {
                        //Don't bother doing any of this if the block shouldn't have sound
                        if (BlockMoved.HasSound == true)
                        {
                            //Check the surface the block is on to determine the sound played
                            SurfaceTypes surface = BlockMoved.Context.SurfaceTiles.GetTileInRegion(BlockMoved.CollisionRect);

                            if (surface == SurfaceTypes.Ice)
                            {
                                SoundManager.Instance.PlaySoundWithLimit(IceMoveSound, ContentGlobals.BlockIceMoveSoundLimit, false,
                                    ContentGlobals.BlockIceMoveSoundVolume, ContentGlobals.BlockIceMoveSoundPitch);
                            }
                            else
                            {
                                SoundManager.Instance.PlaySoundWithLimit(MoveSound, ContentGlobals.BlockMoveSoundLimit, false,
                                    ContentGlobals.BlockMoveSoundVolume, ContentGlobals.BlockMoveSoundPitch);
                            }
                        }

                        ElapsedSoundTime -= MoveSoundPlayTime;
                    }
                }

                ElapsedTime = newTime;
                BlockMoved.transform.Position = pos;
            }
            else
            {
                double prevElapsed = ElapsedTime;

                //Clamp to the object we hit
                Vector2 newPos = UtilityGlobals.ClampRectToOther(moveDir, BlockMoved.transform.Position, BlockMoved.CollisionRect,
                    collisionObj.CollisionRect);

                //Find the time we should be at at this position
                if (ObjStartPos.X != ObjEndPos.X)
                {
                    ElapsedTime = Interpolation.InverseLerp(ObjStartPos.X, ObjEndPos.X, newPos.X)
                        * SwitchAssociated.MoveTime;
                }
                else
                {
                    ElapsedTime = Interpolation.InverseLerp(ObjStartPos.Y, ObjEndPos.Y, newPos.Y)
                        * SwitchAssociated.MoveTime;
                }

                if (ElapsedTime != prevElapsed)
                {
                    BlockMoved.particleEngine.Resume();
                    BlockMoved.AdjustParticleDirection(moveDir);
                }
                else
                {
                    BlockMoved.particleEngine.Stop(false);
                }

                BlockMoved.transform.Position = Interpolation.Interpolate(ObjStartPos, ObjEndPos, ElapsedTime / SwitchAssociated.MoveTime, Interpolation.InterpolationTypes.Linear);
            }
        }

        private bool CollisionCheck(ICollisionObj collisionObj)
        {
            //The object is blocked by blocking collision and the player
            return (collisionObj != BlockMoved && (collisionObj.ObjectType == ObjectTypes.Player
                || (collisionObj.CollisionType == CollisionTypes.Blocking
                && collisionObj.CollisionLayer != CollisionLayers.BlockExcept)));
        }
    }
}
