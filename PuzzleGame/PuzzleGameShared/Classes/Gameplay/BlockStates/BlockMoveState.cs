﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class BlockMoveState : IFiniteStateMachine
    {
        private Block BlockMoved = null;

        public Vector2 MoveAmount = Vector2.Zero;

        private Vector2 BlockStartPos = Vector2.Zero;
        private Vector2 BlockEndPos = Vector2.Zero;

        /// <summary>
        /// Tells whether to ignore ice surfaces when moving.
        /// </summary>
        /// <remarks>
        /// This is true only when pulling blocks directly to avoid a redundant state change, as the player would be in the way.
        /// The state change also noticeably changes the color of the block for a frame, since blocks are ungrabbable while sliding on ice.
        /// </remarks>
        private bool IgnoreIce = false;

        public double MoveTime = 250d;
        private double ElapsedTime = 0d;

        public BlockMoveState(Block blockMoved, in Vector2 moveAmt, in double moveTime, in bool ignoreIce)
        {
            BlockMoved = blockMoved;

            MoveAmount = moveAmt;
            MoveTime = moveTime;

            IgnoreIce = ignoreIce;
        }

        public void Enter()
        {
            BlockStartPos = BlockMoved.transform.Position;
            BlockEndPos = BlockStartPos + MoveAmount;

            Vector2.Normalize(ref MoveAmount, out Vector2 dir);

            Direction moveDir = DirectionUtil.GetDirectionForVector2(dir);
            BlockMoved.AdjustParticleDirection(moveDir);

            BlockMoved.particleEngine.Resume();

            if (BlockMoved.HasSound == true)
            {
                SoundManager.Instance.PlaySoundWithLimit(AssetManager.Instance.LoadSound(ContentGlobals.BlockMoveSound),
                    ContentGlobals.BlockMoveSoundLimit, false, ContentGlobals.BlockMoveSoundVolume, ContentGlobals.BlockMoveSoundPitch);
            }
        }

        public void Exit()
        {
            BlockMoved.UpdateData();
            BlockMoved.particleEngine.Stop(false);
            //BlockMoved = null;
            //MoveAmount = Vector2.Zero;
        }

        public void Update()
        {
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= MoveTime)
            {
                BlockMoved.transform.Position = BlockEndPos;

                //Check if the block is now on ice; if so, the block should slide
                SurfaceTypes surface = BlockMoved.Context.SurfaceTiles.GetTileInRegion(BlockMoved.CollisionRect);

                if (surface == SurfaceTypes.Ice && IgnoreIce == false)
                {
                    Vector2 dir = MoveAmount;
                    dir.Normalize();

                    //Make sure nothing is in front of the block; if there is, stop
                    ICollisionObj coll = LevelGlobals.CheckCollision(dir * BlockMoved.Context.LevelSpace.TileSize,
                        BlockMoved.CollisionRect, BlockMoved.Context.LevelCollision, CustomBlockMoveFilter);

                    if (coll == null)
                    {
                        BlockMoved.ChangeState(new BlockIceMoveState(BlockMoved, dir));
                        return;
                    }
                }

                BlockMoved.ChangeState(null);
            }
            else
            {
                BlockMoved.transform.Position = Interpolation.Interpolate(BlockStartPos, BlockEndPos, ElapsedTime / MoveTime, Interpolation.InterpolationTypes.Linear);
            }
        }

        private bool CustomBlockMoveFilter(ICollisionObj collisionObj)
        {
            return (collisionObj != BlockMoved && collisionObj.CollisionType == CollisionTypes.Blocking
                && collisionObj.CollisionLayer != CollisionLayers.BlockExcept);
        }
    }
}
