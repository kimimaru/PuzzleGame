﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public class BlockIceMoveState : IFiniteStateMachine
    {
        private LevelGlobals.CollisionFilter BlockIceFilter = null;

        private Block BlockMoved = null;

        private Vector2 MoveDir = Vector2.Zero;

        private Vector2 BlockStartPos = Vector2.Zero;
        private Vector2 BlockEndPos = Vector2.Zero;

        private double MoveTime = 217d;
        private double ElapsedTime = 0d;

        public BlockIceMoveState(Block blockMoved, in Vector2 moveDir)
        {
            BlockMoved = blockMoved;

            MoveDir = moveDir;
        }

        public void Enter()
        {
            BlockStartPos = BlockMoved.transform.Position;
            BlockEndPos = BlockStartPos + (MoveDir * BlockMoved.Context.LevelSpace.TileSize);
            
            Direction moveDir = DirectionUtil.GetDirectionForVector2(MoveDir);
            BlockMoved.AdjustParticleDirection(moveDir);
            BlockMoved.particleEngine.Resume();

            //Mark it as non-grabbable
            BlockMoved.Grabbable = false;
            BlockMoved.UpdateData();

            //Change the time to be slightly lower than the player's to fix a bug that lets the player
            //push a block onto ice and step onto the first ice tile the block passes through without sliding
            MoveTime = BlockMoved.Context.Player.MoveTime - 34d;

            BlockIceFilter = CustomBlockIceFilter;

            if (BlockMoved.HasSound == true)
            {
                //Pitch of 1 works well for ice!
                SoundManager.Instance.PlaySoundWithLimit(AssetManager.Instance.LoadSound(ContentGlobals.BlockIceMoveSound),
                    ContentGlobals.BlockIceMoveSoundLimit, false, ContentGlobals.BlockIceMoveSoundVolume, ContentGlobals.BlockIceMoveSoundPitch);
            }
        }

        public void Exit()
        {
            BlockMoved.Grabbable = true;
            BlockMoved.UpdateData();
            BlockMoved.particleEngine.Stop(false);
            
            //
            //BlockMoved = null;
            //MoveDir = Vector2.Zero;
            //
            //BlockIceFilter = null;
        }

        public void Update()
        {
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= MoveTime)
            {
                BlockMoved.transform.Position = BlockEndPos;

                //If the block is going to collide with something the next tile over or is no longer on ice, stop
                if (BlockMoved.Context.SurfaceTiles.GetTileInRegion(BlockMoved.CollisionRect) != SurfaceTypes.Ice)
                {
                    BlockMoved.ChangeState(null);
                    return;
                }

                ICollisionObj coll = LevelGlobals.CheckCollision(MoveDir * BlockMoved.Context.LevelSpace.TileSize, BlockMoved.CollisionRect,
                    BlockMoved.Context.LevelCollision, BlockIceFilter);
                if (coll != null)
                {
                    BlockMoved.ChangeState(null);
                }
                else
                {
                    BlockMoved.ChangeState(new BlockIceMoveState(BlockMoved, MoveDir));
                }
            }
            else
            {
                BlockMoved.transform.Position = Interpolation.Interpolate(BlockStartPos, BlockEndPos, ElapsedTime / MoveTime, Interpolation.InterpolationTypes.Linear);

                //Check collision with the block
                ICollisionObj coll = LevelGlobals.CheckCollision(Vector2.Zero, BlockMoved.CollisionRect, BlockMoved.Context.LevelCollision, BlockIceFilter);
                if (coll != null)
                {
                    //If we found collision while moving, then reset back to the start position and exit
                    //This fixes issues with clipping through objects
                    BlockMoved.transform.Position = BlockStartPos;

                    BlockMoved.ChangeState(null);
                }
            }
        }

        private bool CustomBlockIceFilter(ICollisionObj collisionObj)
        {
            return (collisionObj != BlockMoved &&
                (collisionObj.CollisionType == CollisionTypes.Blocking || collisionObj.ObjectType == ObjectTypes.Player)
                && collisionObj.CollisionLayer != CollisionLayers.BlockExcept);
        }
    }
}
