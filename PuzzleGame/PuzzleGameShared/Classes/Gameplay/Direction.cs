﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    /// <summary>
    /// The possible directions to face.
    /// </summary>
    public enum Direction
    {
        Up, Down, Left, Right
    }

    public static class DirectionUtil
    {
        public static Direction GetOppositeDirection(in Direction direction)
        {
            switch(direction)
            {
                case Direction.Up: return Direction.Down;
                case Direction.Down: return Direction.Up;
                case Direction.Left: return Direction.Right;
                case Direction.Right:
                default:
                    return Direction.Left;
            }
        }

        public static Vector2 GetVector2ForDir(in Direction direction)
        {
            switch(direction)
            {
                case Direction.Up: return new Vector2(0f, -1f);
                case Direction.Down: return new Vector2(0f, 1f);
                case Direction.Left: return new Vector2(-1f, 0f);
                case Direction.Right: return new Vector2(1f, 0f);
                default: return Vector2.Zero;
            }
        }

        /// <summary>
        /// Retrieves a direction associated with a direction vector.
        /// </summary>
        /// <param name="vector2">A normalized direction vector with only one non-zero component.</param>
        /// <returns>The <see cref="Direction"/> associated with the direction vector. If none match, <see cref="Direction.Down"/>.</returns>
        public static Direction GetDirectionForVector2(in Vector2 vector2)
        {
            if (vector2 == -Vector2.UnitY) return Direction.Up;
            if (vector2 == Vector2.UnitY) return Direction.Down;
            if (vector2 == -Vector2.UnitX) return Direction.Left;
            if (vector2 == Vector2.UnitX) return Direction.Right;

            //Default
            return Direction.Down;
        }

        public static Direction GetClockwiseDirection(in Direction direction)
        {
            switch (direction)
            {
                case Direction.Up: return Direction.Right;
                case Direction.Right:
                default: return Direction.Down;
                case Direction.Down: return Direction.Left;
                case Direction.Left: return Direction.Up;
            }
        }

        public static Direction GetCounterClockwiseDirection(in Direction direction)
        {
            switch (direction)
            {
                case Direction.Up: return Direction.Left;
                case Direction.Left:
                default: return Direction.Down;
                case Direction.Down: return Direction.Right;
                case Direction.Right: return Direction.Up;
            }
        }

        public static float GetRotationForDir(in Direction direction)
        {
            switch (direction)
            {
                default:
                case Direction.Up: return 0f;
                case Direction.Down: return (float)UtilityGlobals.ToRadians(180d);
                case Direction.Left: return (float)UtilityGlobals.ToRadians(-90d);
                case Direction.Right: return (float)UtilityGlobals.ToRadians(90d);
            }
        }

        public static float GetRotationForVector2(in Vector2 vector2)
        {
            return GetRotationForDir(GetDirectionForVector2(vector2));
        }

        public static string GetActionForDirection(in Direction direction)
        {
            switch (direction)
            {
                default:
                case Direction.Left:  return InputActions.Left;
                case Direction.Right: return InputActions.Right;
                case Direction.Up: return InputActions.Up;
                case Direction.Down: return InputActions.Down;
            }
        }

        public static Direction? GetDirectionForAction(in string action)
        {
            switch (action)
            {
                default: return null;
                case InputActions.Left: return Direction.Left;
                case InputActions.Right: return Direction.Right;
                case InputActions.Up: return Direction.Up;
                case InputActions.Down: return Direction.Down;
            }
        }
    }
}
