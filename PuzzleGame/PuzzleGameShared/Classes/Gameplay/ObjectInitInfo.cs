/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Information regarding initializing an object.
    /// </summary>
    public struct ObjectInitInfo
    {
        public int Identifier;
        public string Name;
        public string Type;
        public Dictionary<string, string> Properties;
        public bool IsVisible;
        public float Opacity;
        public float Rotation;
        public Vector2 Position;
        public Vector2 Size;

        public static explicit operator ObjectInitInfo(TiledMapObject mapObj)
        {
            ObjectInitInfo initInfo = default;

            initInfo.Identifier = mapObj.Identifier;
            initInfo.Name = mapObj.Name;
            initInfo.Type = mapObj.Type;
            initInfo.Properties = mapObj.Properties;
            initInfo.IsVisible = mapObj.IsVisible;
            initInfo.Opacity = mapObj.Opacity;
            initInfo.Rotation = mapObj.Rotation;
            initInfo.Position = mapObj.Position;
            initInfo.Size = mapObj.Size;

            return initInfo;
        }
    }
}