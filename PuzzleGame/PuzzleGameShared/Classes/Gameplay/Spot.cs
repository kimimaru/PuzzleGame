/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class Spot : ITransformable, ICollisionObj, /*IUpdateable,*/ ITintable, IRenderDepth
    {
        public string ID { get; set; } = string.Empty;

        public PlayableContext Context { get; set; } = null;
        public Transform transform { get; private set; } = new Transform();
        public float RenderDepth { get; set; } = LevelGlobals.BaseSpotRenderDepth;

        public Sprite sprite { get; private set; } = null;

        private readonly Color GroundPatternColor = new Color(232, 236, 202, 255);
        private readonly Color MudPatternColor = new Color(182, 186, 152, 255);
        private readonly Color IcePatternColor = new Color(209, 247, 255, 255);
        private Sprite PatternSprite = null;
        private Color PatternColor = new Color(182, 186, 152, 255);

        public Color TintColor { get; set; } = Color.White;

        public ObjectTypes ObjectType => ObjectTypes.Spot;

        public CollisionTypes CollisionType { get; set; } = CollisionTypes.Trigger;

        public CollisionLayers CollisionLayer { get; set; } = CollisionLayers.Spot;

        public TriggerQueries TriggerQuery { get; private set; } = TriggerQueries.Constant;

        public BlockPatterns AssociatedBlockPattern { get; private set; } = BlockPatterns.Circle;

        private Block BlockPlaced = null;
        private bool Activated = false;

        public virtual RectangleF CollisionRect
        {
            get
            {
                Rectangle spriteRect = sprite.SourceRect.Value;
                return new RectangleF(transform.Position.X - ((spriteRect.Width / 2) * transform.Scale.X),
                                      transform.Position.Y - ((spriteRect.Height / 2) * transform.Scale.Y),
                                      spriteRect.Width * transform.Scale.X, spriteRect.Height * transform.Scale.Y);
            }
        }

        private SoundEffect PlacedSound = null;
        private SoundEffect IncorrectPlacedSound = null;

        //private SpotParticleEngine SpotParticles = null;

        //private SimpleAnimation SpotActiveAnim = null;

        //private double ElapsedTime = 0d;

        public Spot()
        {

        }

        public void CleanUp()
        {
            PlacedSound = null;
            IncorrectPlacedSound = null;
        }

        public bool QueryTrigger(List<ICollisionObj> collisions)
        {
            if (BlockPlaced == null)
            {
                //If collision is a block and is in the same position, activate spot
                for (int i = 0; i < collisions.Count; i++)
                {
                    ICollisionObj collision = collisions[i];
                    if (collision == this || collision.ObjectType != ObjectTypes.Block) continue;

                    if (UtilityGlobals.IsApproximate(collision.transform.Position, transform.Position, .001f) == false)
                    {
                        continue;
                    }

                    Block block = (Block)collision;

                    //Disallow any non-standard blocks from going into spots
                    if (block.BlockType != BlockTypes.Normal && block.BlockType != BlockTypes.Paired)
                    {
                        continue;
                    }

                    BlockPlaced = block;

                    //Ensure it's the same type of block and that the scales are equal
                    //If the block type for this spot is None, then any block will fit
                    if ((AssociatedBlockPattern == BlockPatterns.None || block.BlockPattern == AssociatedBlockPattern)
                        && UtilityGlobals.IsApproximate(transform.Scale, block.transform.Scale, .01f) == true)
                    {
                        BlockPlaced.BlockState = BlockStates.CorrectSpot;
                        Context.IncrementPortalUnlock(1);

                        Activated = true;

                        //SpotParticles.Resume();

                        //AnimIndex = 0;
                        //ElapsedAnimTime = 0d;

                        if (PlacedSound != null)
                        {
                            //Limit how many sounds are played so there's no overlap if multiple blocks are placed
                            SoundManager.Instance.PlaySoundWithLimit(PlacedSound, ContentGlobals.BlockCorrectSpotSoundLimit, false, .8f);
                        }

                        return false;
                    }
                    //It's in the wrong spot, so indicate that
                    else
                    {
                        BlockPlaced.BlockState = BlockStates.WrongSpot;
                        Activated = false;
                        //SpotParticles.Stop(false);

                        if (IncorrectPlacedSound != null)
                        {
                            //Limit how many sounds are played so there's no overlap if multiple blocks are placed
                            SoundManager.Instance.PlaySoundWithLimit(IncorrectPlacedSound, ContentGlobals.BlockIncorrectSpotSoundLimit, false, .8f);
                        }
                        return false;
                    }
                }
            }
            else
            {
                //Check to see if the block was removed
                if (UtilityGlobals.IsApproximate(BlockPlaced.transform.Position, transform.Position, .001f) == false || BlockPlaced.Active == false)
                {
                    BlockPlaced.BlockState = BlockStates.None;
                    BlockPlaced = null;

                    //Decrement when it's removed if it didn't activate previously
                    if (Activated == true)
                    {
                        Context.IncrementPortalUnlock(-1);
                        Activated = false;

                        //SpotParticles.Stop(false);
                    }

                    return false;
                }
                //Confirm that the block has the correct state
                //This fixes a bug where the the block changes from one spot to another in a single frame, possible via undoing
                //The spot it just got off of would overwrite its state and display incorrectly
                else
                {
                    if (AssociatedBlockPattern == BlockPatterns.None || BlockPlaced.BlockPattern == AssociatedBlockPattern)
                    {
                        if (BlockPlaced.BlockState != BlockStates.CorrectSpot)
                            BlockPlaced.BlockState = BlockStates.CorrectSpot;
                    }
                    else if (BlockPlaced.BlockState != BlockStates.WrongSpot)
                    {
                        BlockPlaced.BlockState = BlockStates.WrongSpot;
                    }
                }
            }

            return false;
        }

        public void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            if (initInfo.Properties.TryGetValue(LevelGlobals.BlockPatternKey, out string value) == true)
            {
                if (Enum.TryParse(value, true, out BlockPatterns blockPattern) == true)
                {
                    AssociatedBlockPattern = blockPattern;
                }
            }

            if (initInfo.Properties.TryGetValue(LevelGlobals.ScaleKey, out string scaleStr) == true)
            {
                if (float.TryParse(scaleStr, out float scale) == true)
                {
                    transform.Scale = new Vector2(scale);
                }
            }

            transform.Position = initInfo.Position + (context.LevelSpace.TileSizeHalf * transform.Scale);

            if (initInfo.Properties.TryGetValue(LevelGlobals.IDKey, out string idVal) == true)
            {
                ID = idVal;
            }

            SurfaceTypes surfaceOn = context.SurfaceTiles.GetTile(transform.Position);
            sprite = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.LevelObjectsTex), GetSourceRectForSurface(surfaceOn));

            if (surfaceOn == SurfaceTypes.Mud)
            {
                PatternColor = MudPatternColor;
            }
            else if (surfaceOn == SurfaceTypes.Ice)
            {
                PatternColor = IcePatternColor;
            }
            else
            {
                PatternColor = GroundPatternColor;
            }

            if (AssociatedBlockPattern != BlockPatterns.None)
            {
                Rectangle rect = Block.GetPatternSourceRect(AssociatedBlockPattern, null);

                PatternSprite = new Sprite(sprite.Tex, rect);
            }

            //Check for loading the sounds
            if (PlacedSound == null || IncorrectPlacedSound == null)
            {
                bool loadSound = true;

                if (initInfo.Properties.TryGetValue(LevelGlobals.HasSoundKey, out string noSoundStr) == true)
                {
                    bool.TryParse(noSoundStr, out loadSound);
                }

                if (loadSound == true)
                {
                    PlacedSound = AssetManager.Instance.LoadSound(ContentGlobals.CorrectSpotSound);
                    IncorrectPlacedSound = AssetManager.Instance.LoadSound(ContentGlobals.IncorrectSpotSound);
                }
            }

            //SetupParticleEngine();

            //SpotActiveAnim = new SimpleAnimation(new Sprite(sprite.Tex, new Rectangle(101, 320, 32, 33), new Vector2(.5f, 1f)), AnimTypes.Looping,
            //    new AnimationFrame(new Rectangle(101, 310, 32, 43), 85d, new Vector2(.5f, 1f)),
            //    new AnimationFrame(new Rectangle(133, 310, 32, 43), 85d, new Vector2(.5f, 1f)),
            //    new AnimationFrame(new Rectangle(165, 310, 32, 43), 85d, new Vector2(.5f, 1f)),
            //    new AnimationFrame(new Rectangle(133, 310, 32, 43), 85d, new Vector2(.5f, 1f)));
        }

        public void PostInitialize(PlayableContext context)
        {

        }

        /// <summary>
        /// Changes the Spot's pattern. Used only in cutscenes.
        /// </summary>
        public void ChangeSpotPattern(in BlockPatterns pattern)
        {
            AssociatedBlockPattern = pattern;
            if (AssociatedBlockPattern != BlockPatterns.None)
            {
                Rectangle rect = Block.GetPatternSourceRect(AssociatedBlockPattern, null);

                if (PatternSprite == null)
                {
                    PatternSprite = new Sprite(sprite.Tex, rect);
                }
                else
                {
                    PatternSprite.SourceRect = rect;
                }
            }
        }

        //private void SetupParticleEngine()
        //{
        //    Sprite particleSprite = new Sprite(sprite.Tex, new Rectangle(148, 81, 5, 5));
        //    SpotParticles = new SpotParticleEngine(8, transform.Position, particleSprite, LevelGlobals.BasePlayerRenderDepth - .001f, 1003d, 1003d);
        //    SpotParticles.MinPositionOffset = new Vector2(-5f * transform.Scale.X, -5f * transform.Scale.Y);
        //    SpotParticles.MaxPositionOffset = new Vector2(5f * transform.Scale.X, 7f * transform.Scale.Y);
        //    SpotParticles.MinVelocity = new Vector2(-.5f * 60f, -1f * 60f);
        //    SpotParticles.MaxVelocity = new Vector2(.5f * 60f, -1f * 60f);
        //    SpotParticles.MinAcceleration = new Vector2(0f, 3f * 60f);
        //    SpotParticles.MaxAcceleration = new Vector2(0f, 3f * 60f);
        //    SpotParticles.MinAngularVelocity = 0f;
        //    SpotParticles.MaxAngularVelocity = 0f;
        //    SpotParticles.MinInitScale = transform.Scale * 2f;
        //    SpotParticles.MaxInitScale = transform.Scale * 2f;
        //    SpotParticles.MinScaleOverTime = Vector2.Zero;
        //    SpotParticles.MaxScaleOverTime = Vector2.Zero;
        //    SpotParticles.EmissionRate = 0d;
        //    SpotParticles.ParticleColor = Color.White;
        //    SpotParticles.UseColorOverTime = true;
        //    SpotParticles.ColorOverTime = Color.White * 0f;
        //
        //    SpotParticles.PrewarmParticles();
        //    SpotParticles.Stop(true);
        //}

        //public void Update()
        //{
        //    ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
        //    //SpotParticles.Update();
        //    //SpotActiveAnim.Update();
        //}

        public void Render()
        {
            RenderingManager.Instance.DrawSprite(sprite.Tex, transform.Position, sprite.SourceRect, TintColor, transform.Rotation, sprite.GetOrigin(), transform.Scale, SpriteEffects.None, RenderDepth);
            if (PatternSprite != null && AssociatedBlockPattern != BlockPatterns.None)
            {
                RenderingManager.Instance.DrawSprite(PatternSprite.Tex, transform.Position, PatternSprite.SourceRect, PatternColor * (TintColor.A / 255f), transform.Rotation, PatternSprite.GetOrigin(), transform.Scale, SpriteEffects.None, RenderDepth + .001f);
            }
            //RenderingManager.Instance.spriteBatch.DrawHollowRect(AssetManager.Instance.LoadRawTexture2D($"{ContentGlobals.SpriteRoot}Box.png"),
            //    CollisionRect, Color.Blue, RenderDepth + .01f, 1);

            //SpotParticles.Render();

            //if (Activated == true)
            //{
            //    RenderingManager.Instance.DrawSprite(SpotActiveAnim.SpriteToChange.Tex, new Vector2(transform.Position.X, CollisionRect.Bottom),
            //        SpotActiveAnim.SpriteToChange.SourceRect, Color.White * .5f, 0f, SpotActiveAnim.SpriteToChange.GetOrigin(), transform.Scale, SpriteEffects.None, LevelGlobals.BaseBlockRenderDepth + .02f);
            //}
        }

        private Rectangle GetSourceRectForSurface(in SurfaceTypes surface)
        {
            Rectangle startRect = ContentGlobals.SpotStartRect;
            startRect.X += ((int)surface * 32);

            return startRect;
        }
    }
}