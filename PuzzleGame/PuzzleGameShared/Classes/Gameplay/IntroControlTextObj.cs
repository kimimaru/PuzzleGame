/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Microsoft.Xna.Framework;
//using MonoGame.Extended.Tiled;
//
//namespace PuzzleGame
//{
//    /// <summary>
//    /// Used in the intro to change the InLevelText to match the correct controls.
//    /// </summary>
//    public class IntroControlTextObj : ICollisionObj
//    {
//        public string ID { get; set; } = string.Empty;
//
//        public PlayableContext Context { get; set; } = null;
//
//        public Transform transform { get; private set; } = new Transform();
//
//        public ObjectTypes ObjectType { get; private set; } = ObjectTypes.Wall;
//
//        public CollisionTypes CollisionType { get; set; } = CollisionTypes.Trigger;
//
//        public CollisionLayers CollisionLayer { get; protected set; }
//
//        public TriggerQueries TriggerQuery { get; protected set; } = TriggerQueries.None;
//
//        public RectangleF CollisionRect => new RectangleF(transform.Position, transform.Scale);
//
//        public IntroControlTextObj()
//        {
//            transform.Scale = Vector2.Zero;
//        }
//
//        public void CleanUp()
//        {
//            DataHandler.inputConfigData.KeyboardMappingsChangedEvent -= OnKeyboardMappingsChanged;
//        }
//
//        public virtual void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
//        {
//            transform.Position = Vector2.Zero;
//            transform.Scale = Vector2.Zero;
//        }
//
//        public void PostInitialize(PlayableContext context)
//        {
//            OnKeyboardMappingsChanged(0);
//
//            //Subscribe to the event to change the objects
//            DataHandler.inputConfigData.KeyboardMappingsChangedEvent -= OnKeyboardMappingsChanged;
//            DataHandler.inputConfigData.KeyboardMappingsChangedEvent += OnKeyboardMappingsChanged;
//        }
//
//        public virtual bool QueryTrigger(List<ICollisionObj> collisions)
//        {
//            return false;
//        }
//
//        private void OnKeyboardMappingsChanged(in int kbIndex)
//        {
//            if (Context.LevelText.Count == 0)
//            {
//                return;
//            }
//
//            //Change the movement text
//            if (Context.LevelText[0] != null)
//            {
//                string upStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.Up);
//                string leftStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.Left);
//                string downStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.Down);
//                string rightStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.Right);
//
//                Context.LevelText[0].ChangeDisplayText($"{upStr}/{leftStr}/{downStr}/{rightStr} = Move");
//            }
//
//            //Change the grab text
//            if (Context.LevelText.Count > 1 && Context.LevelText[1] != null)
//            {
//                string grabStr = UtilityGlobals.GetKeyStrForInput(0, InputActions.Grab);
//
//                Context.LevelText[1].ChangeDisplayText($"{grabStr} = Grab");
//            }
//        }
//    }
//}