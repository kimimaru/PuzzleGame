/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    public class Portal : ITransformable, ICollisionObj, ITintable, IUpdateable, IRenderDepth
    {
        public delegate void OnPlayerEnteredPortal();
        public event OnPlayerEnteredPortal PlayerEnteredPortalEvent = null;

        private enum PortalColorStates
        {
            Still, Fading
        }

        public string ID { get; set; } = string.Empty;

        public PlayableContext Context { get; set; } = null;
        public Transform transform { get; private set; } = new Transform();
        public float RenderDepth { get; set; } = LevelGlobals.BasePortalRenderDepth;

        public Sprite sprite { get; private set; } = null;
        private SimpleAnimation PortalAnim = null;

        public ObjectTypes ObjectType => ObjectTypes.Portal;

        public CollisionTypes CollisionType { get; set; } = CollisionTypes.Trigger;

        public CollisionLayers CollisionLayer { get; set; } = CollisionLayers.Portal;

        public TriggerQueries TriggerQuery { get; private set; } = TriggerQueries.Constant;

        public virtual RectangleF CollisionRect
        {
            get
            {
                Rectangle spriteRect = sprite.SourceRect.Value;
                return new RectangleF(transform.Position.X - ((spriteRect.Width / 2) * transform.Scale.X),
                                      transform.Position.Y - ((spriteRect.Height / 2) * transform.Scale.Y),
                                      spriteRect.Width * transform.Scale.X, spriteRect.Height * transform.Scale.Y);
            }
        }

        public bool Activated
        {
            get => activated;
            set
            {
                //Reset the portal's animation when it's not activated
                activated = value;
                if (activated == false)
                {
                    PortalAnim.Play();
                    ActivatedParticles.Stop(false);
                }
                else
                {
                    ActivatedParticles.Resume();
                }
            }
        }

        public Color TintColor { get; set; } = DeactivatedColor;

        public static readonly Color DeactivatedColor = Color.DarkGray;
        public static readonly Color ActivatedColor = Color.White;

        public bool HaltColorUpdate = false;

        private bool activated = false;

        public const double FadeTime = 300d;
        private double ElapsedFadeTime = 0d;

        private double ElapsedAnimTime = 0d;
        private const double AnimTime = 250d;

        private PortalColorStates ColorState = PortalColorStates.Still;
        public ParticleEngine ActivatedParticles { get; private set; } = null;
        private SimpleAnimation PortalActivatedAnim = null;

        public Portal()
        {
            //sprite = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.LevelObjectsTex), ContentGlobals.PortalAnim1Rect);
            //PortalAnim = new SimpleAnimation(sprite, AnimTypes.Looping,
            //    new AnimationFrame(ContentGlobals.PortalAnim1Rect, 85d),
            //    new AnimationFrame(ContentGlobals.PortalAnim2Rect, 85d),
            //    new AnimationFrame(ContentGlobals.PortalAnim3Rect, 85d),
            //    new AnimationFrame(ContentGlobals.PortalAnim2Rect, 85d));
            //
            //Sprite animSprite = new Sprite(sprite.Tex, ContentGlobals.PortalActiveAnim1Rect, new Vector2(.5f, 1f));
            //PortalActivatedAnim = new SimpleAnimation(animSprite, AnimTypes.Looping,
            //    new AnimationFrame(ContentGlobals.PortalActiveAnim1Rect, 113.34d, new Vector2(.5f, 1f)),
            //    new AnimationFrame(ContentGlobals.PortalActiveAnim2Rect, 113.34d, new Vector2(.5f, 1f)),
            //    new AnimationFrame(ContentGlobals.PortalActiveAnim3Rect, 113.34d, new Vector2(.5f, 1f)));
            //    //new AnimationFrame(ContentGlobals.PortalActiveAnim2Rect, 85d, new Vector2(.5f, 1f)));
        }

        public Portal(in Vector2 position) : this()
        {
            transform.Position = position;
        }

        public void CleanUp()
        {
            PlayerEnteredPortalEvent = null;
        }

        public void OnCorrectSpotPlaced()
        {
            ColorState = PortalColorStates.Fading;
        }

        public void Update()
        {
            PortalAnim.Update();

            double factor = (ColorState == PortalColorStates.Fading || Activated == true) ? 1d : -1d;
            ElapsedFadeTime = UtilityGlobals.Clamp(ElapsedFadeTime + (Time.ElapsedTime.TotalMilliseconds * factor), 0d, FadeTime);
            
            ElapsedAnimTime = UtilityGlobals.Wrap(ElapsedAnimTime + Time.ElapsedTime.TotalMilliseconds, 0d, AnimTime);

            if (ElapsedFadeTime >= FadeTime)
            {
                ElapsedFadeTime = FadeTime;
                if (HaltColorUpdate == false)
                {
                    TintColor = ActivatedColor;
                }

                if (Activated == false) ColorState = PortalColorStates.Still;
            }
            else
            {
                if (HaltColorUpdate == false)
                {
                    TintColor = Interpolation.Interpolate(DeactivatedColor, ActivatedColor, ElapsedFadeTime / FadeTime, Interpolation.InterpolationTypes.Linear);
                }
            }

            ActivatedParticles.Position = transform.Position;
            ActivatedParticles.Update();
            PortalActivatedAnim.Update();
        }

        public bool QueryTrigger(List<ICollisionObj> collisions)
        {
            if (Activated == true)
            {
                //Finish the level if the player touched it when it's activated, provided the player isn't warping
                if (Context.Player.PlayerState != PlayerStates.Warp
                    && UtilityGlobals.IsApproximate(Context.Player.transform.Position, transform.Position, .001f) == true)
                {
                    PlayerEnteredPortalEvent?.Invoke();
                    return true;
                }
            }

            return false;
        }

        public void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            transform.Position = initInfo.Position + context.LevelSpace.TileSizeHalf;

            if (initInfo.Properties.TryGetValue(LevelGlobals.IDKey, out string portalIDVal) == true)
            {
                ID = portalIDVal;
            }

            bool useAlternateAnim = false;

            if (initInfo.Properties.TryGetValue(LevelGlobals.PortalBossAnimKey, out string bossAnim) == true)
            {
                if (bool.TryParse(bossAnim, out bool useAlternate) == true)
                {
                    useAlternateAnim = useAlternate;
                }
            }

            Rectangle animRect1 = ContentGlobals.PortalAnim1Rect;
            Rectangle animRect2 = ContentGlobals.PortalAnim2Rect;
            Rectangle animRect3 = ContentGlobals.PortalAnim3Rect;

            Rectangle activeAnimRect1 = ContentGlobals.PortalActiveAnim1Rect;
            Rectangle activeAnimRect2 = ContentGlobals.PortalActiveAnim2Rect;
            Rectangle activeAnimRect3 = ContentGlobals.PortalActiveAnim3Rect;

            if (useAlternateAnim == true)
            {
                animRect1 = ContentGlobals.PortalBossAnim1Rect;
                animRect2 = ContentGlobals.PortalBossAnim2Rect;
                animRect3 = ContentGlobals.PortalBossAnim3Rect;

                activeAnimRect1 = ContentGlobals.PortalBossActiveAnim1Rect;
                activeAnimRect2 = ContentGlobals.PortalBossActiveAnim2Rect;
                activeAnimRect3 = ContentGlobals.PortalBossActiveAnim3Rect;
            }

            sprite = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.LevelObjectsTex), animRect1);
            PortalAnim = new SimpleAnimation(sprite, AnimTypes.Looping,
                new AnimationFrame(animRect1, 85d),
                new AnimationFrame(animRect2, 85d),
                new AnimationFrame(animRect3, 85d),
                new AnimationFrame(animRect2, 85d));

            Sprite animSprite = new Sprite(sprite.Tex, activeAnimRect1, new Vector2(.5f, 1f));
            PortalActivatedAnim = new SimpleAnimation(animSprite, AnimTypes.Looping,
                new AnimationFrame(activeAnimRect1, 113.34d, new Vector2(.5f, 1f)),
                new AnimationFrame(activeAnimRect2, 113.34d, new Vector2(.5f, 1f)),
                new AnimationFrame(activeAnimRect3, 113.34d, new Vector2(.5f, 1f)));
            //new AnimationFrame(ContentGlobals.PortalActiveAnim2Rect, 85d, new Vector2(.5f, 1f)));

            InitializeParticleEngine(transform.Position, useAlternateAnim);
        }

        private void InitializeParticleEngine(in Vector2 particlePos, in bool alternateAnim)
        {
            if (ActivatedParticles != null)
            {
                return;
            }

            Rectangle particleRect = (alternateAnim == true) ? ContentGlobals.PortalBossParticleRect : ContentGlobals.PortalParticleRect;

            Sprite particleSprite = new Sprite(sprite.Tex, particleRect);
            ActivatedParticles = new ParticleEngine(20, particlePos, particleSprite, RenderDepth + .02f, 200d, 300d);
            ActivatedParticles.MinPositionOffset = new Vector2((int)(-CollisionRect.Width / 2) + 2, (int)(-CollisionRect.Height / 4));
            ActivatedParticles.MaxPositionOffset = new Vector2((int)(CollisionRect.Width / 2) - 2, (int)(CollisionRect.Height / 4));
            ActivatedParticles.MinAngularVelocity = ActivatedParticles.MaxAngularVelocity = 0f;
            ActivatedParticles.MinVelocity = new Vector2(0f, -.7f * 60f);
            ActivatedParticles.MaxVelocity = new Vector2(0f, -.9f * 60f);
            ActivatedParticles.MinParticleLife = 300d;
            ActivatedParticles.MaxParticleLife = 800d;
            ActivatedParticles.MinInitScale = Vector2.One;//new Vector2(1f * transform.Scale.X, 3f * transform.Scale.Y);
            ActivatedParticles.MaxInitScale = Vector2.One;//new Vector2(1f * transform.Scale.X, 7f * transform.Scale.Y);
            //ActivatedParticles.MinScaleOverTime = new Vector2(0f, .7f);
            //ActivatedParticles.MaxScaleOverTime = new Vector2(0f, .8f);
            ActivatedParticles.EmissionRate = 16d;
            ActivatedParticles.ParticleColor = Color.White;
            ActivatedParticles.UseColorOverTime = true;
            ActivatedParticles.ColorOverTime = Color.White * 0f;
            ActivatedParticles.PrewarmParticles();
            ActivatedParticles.Stop(true);
        }

        public void PostInitialize(PlayableContext context)
        {

        }

        public void Render()
        {
            AnimationFrame frame = PortalAnim.CurFrame;
            if (Activated == false || ElapsedFadeTime < FadeTime)
            {
                frame = PortalAnim.GetFrame(0);
            }

            RenderingManager.Instance.DrawSprite(sprite.Tex, transform.Position, frame.DrawRegion, TintColor, transform.Rotation, sprite.GetOrigin(), transform.Scale, SpriteEffects.None, RenderDepth);

            ActivatedParticles.Render();

            float alpha = (float)((ElapsedFadeTime / FadeTime) * .4d);
            if (alpha > 0f)
            {
                Sprite animSprite = PortalActivatedAnim.SpriteToChange;
                RenderingManager.Instance.DrawSprite(animSprite.Tex, new Vector2(transform.Position.X, CollisionRect.Bottom), animSprite.SourceRect,
                    Color.White * alpha, 0f, animSprite.GetOrigin(), transform.Scale, SpriteEffects.None, RenderDepth + .01f);
            }
        }
    }
}