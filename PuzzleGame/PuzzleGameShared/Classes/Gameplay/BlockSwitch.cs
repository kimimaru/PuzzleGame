/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// A type of switch that toggles a block on and off when pressed.
    /// </summary>
    public class BlockSwitch : Switch
    {
        private LevelGlobals.CollisionFilter CollisionCheckFilter = null;

        private Block LinkedBlock = null;
        private bool Toggled = false;
        private bool WaitForBlockStop = false;

        public BlockSwitch()
        {
            Pressed = false;
            Toggled = false;

            sprite.SourceRect = ContentGlobals.SwitchBlueSpriteRect;

            SwitchType = SwitchTypes.Block;
        }

        public override void CleanUp()
        {
            base.CleanUp();

            CollisionCheckFilter = null;
        }

        public override void Update()
        {
            base.Update();

            //Wait for the block to get out any state before appearing or disappearing
            if (LinkedBlock != null && WaitForBlockStop == true && LinkedBlock.BlockFSM == null)
            {
                HandleAddRemoveBlock();
            }
        }

        public override bool QueryTrigger(List<ICollisionObj> collisions)
        {
            RectangleF collRect = CollisionRect;

            //If the switch hasn't been toggled, check for collision
            if (Toggled == false)
            {
                //If we're still waiting for the block to finish activating or deactivating, exit early
                if (WaitForBlockStop == true) return false;

                for (int i = 0; i < collisions.Count; i++)
                {
                    ICollisionObj coll = collisions[i];
                    
                    //Prevent the block from toggling itself
                    if (coll != this && coll != LinkedBlock && coll.CollisionRect.Intersects(collRect) == true)
                    {
                        Toggled = true;
                        HandlePress();
                        return false;
                    }
                }
            }
            //If it has been toggled, wait for everything to get off the switch
            else
            {
                for (int i = 0; i < collisions.Count; i++)
                {
                    ICollisionObj coll = collisions[i];

                    if (coll != this && coll.CollisionRect.Intersects(collRect) == true)
                    {
                        return false;
                    }
                }

                Toggled = false;
            }

            return false;
        }

        public override void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            base.Initialize(context, initInfo);

            transform.Position = initInfo.Position + context.LevelSpace.TileSizeHalf;
        }

        public override void PostInitialize(PlayableContext context)
        {
            CollisionCheckFilter = CollisionCheck;

            if (string.IsNullOrEmpty(ID) == true) return;

            for (int i = 0; i < context.Blocks.Count; i++)
            {
                Block block = context.Blocks[i];

                if (block.ID == ID)
                {
                    LinkedBlock = block;
                    break;
                }
            }
        }

        private void HandlePress()
        {
            if (Pressed == true)
            {
                Pressed = false;
                OnReleased();
            }
            else
            {
                Pressed = true;
                OnPressed();
            }
        }

        public override void OnPressed()
        {
            if (PressedSound != null)
            {
                SoundManager.Instance.PlaySound(PressedSound, false, .6f);
            }

            //Check for collision where the block is
            if (LinkedBlock != null)
            {
                //Wait for the block to be stopped
                WaitForBlockStop = true;
            }
        }

        public override void OnReleased()
        {
            if (PressedSound != null)
            {
                SoundManager.Instance.PlaySound(PressedSound, false, .6f);
            }

            if (LinkedBlock != null)
            {
                //Wait for the block to be stopped
                WaitForBlockStop = true;
            }
        }

        private void HandleAddRemoveBlock()
        {
            if (Pressed == false)
            {
                //Check for collision where the block is
                ICollisionObj coll = LevelGlobals.CheckCollision(Vector2.Zero, LinkedBlock.CollisionRect, Context.LevelCollision, CollisionCheckFilter);

                //If there's no collision, add the block
                if (coll == null)
                {
                    //Debug.Log("ADDED BLOCK");

                    Context.AddObject(LinkedBlock);
                    LinkedBlock.Active = true;
                    WaitForBlockStop = false;
                }
            }
            else
            {
                //Debug.Log("REMOVED BLOCK");

                //Remove the block
                Context.RemoveObject(LinkedBlock);
                LinkedBlock.Active = false;
                WaitForBlockStop = false;

                //If the block is grabbed, release it
                if (LinkedBlock.Grabbed == true)
                {
                    LinkedBlock.Release();
                    Context.Player.ChangeState(new IdleState());
                }
            }
        }

        private bool CollisionCheck(ICollisionObj collisionObj)
        {
            //The object is blocked by blocking collision and the player
            return (collisionObj != LinkedBlock && (collisionObj.ObjectType == ObjectTypes.Player
                || (collisionObj.CollisionType == CollisionTypes.Blocking
                && collisionObj.CollisionLayer != CollisionLayers.BlockExcept)));
        }
    }
}