/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// A context in which the core game can be played.
    /// </summary>
    public class PlayableContext
    {
        public Echidna Player = null;
        public readonly List<ICollisionObj> LevelCollision = new List<ICollisionObj>(16);

        public readonly List<Block> Blocks = new List<Block>(4);

        public readonly List<Spot> Spots = new List<Spot>(4);

        public readonly List<Switch> Switches = new List<Switch>(4);

        public readonly List<Warp> Warps = new List<Warp>(8);

        public readonly List<RockShooter> RockShooters = new List<RockShooter>(4);

        public readonly List<InLevelText> LevelText = new List<InLevelText>(4);

        public Portal LevelPortal { get; private set; } = null;

        public SurfaceTileEngine SurfaceTiles { get; private set; } = null;

        public int SpotsForPortal { get; private set; } = 0;

        public PlayableSpace LevelSpace = default(PlayableSpace);

        /// <summary>
        /// An optional camera for the context.
        /// </summary>
        public Camera2D LevelCamera { get; private set; } = null;

        public PlayableContext()
        {

        }

        public PlayableContext(in PlayableSpace levelSpace)
        {
            LevelSpace = levelSpace;
        }

        public void AddText(InLevelText levelText)
        {
            LevelText.Add(levelText);
        }

        public void AddObject(ICollisionObj collisionObj)
        {
            LevelCollision.Add(collisionObj);

            if (collisionObj.ObjectType == ObjectTypes.Player)
            {
                Player = (Echidna)collisionObj;
            }
            else if (collisionObj.ObjectType == ObjectTypes.Block)
            {
                Blocks.Add((Block)collisionObj);
            }
            else if (collisionObj.ObjectType == ObjectTypes.Spot)
            {
                Spots.Add((Spot)collisionObj);
            }
            else if (collisionObj.ObjectType == ObjectTypes.Switch)
            {
                Switches.Add((Switch)collisionObj);
            }
            else if (collisionObj.ObjectType == ObjectTypes.Portal)
            {
                LevelPortal = (Portal)collisionObj;
            }
            else if (collisionObj.ObjectType == ObjectTypes.Warp)
            {
                Warps.Add((Warp)collisionObj);
            }
            else if (collisionObj.ObjectType == ObjectTypes.RockShooter)
            {
                RockShooters.Add((RockShooter)collisionObj);
            }

            collisionObj.Context = this;
        }

        public void AddObjects(List<ICollisionObj> collisionObjs)
        {
            for (int i = 0; i < collisionObjs.Count; i++)
            {
                AddObject(collisionObjs[i]);
            }
        }

        private void RemoveSpecializedObject(ICollisionObj collisionObj)
        {
            if (collisionObj.ObjectType == ObjectTypes.Player)
            {
                Player = null;
            }
            else if (collisionObj.ObjectType == ObjectTypes.Block)
            {
                Blocks.Remove((Block)collisionObj);
            }
            else if (collisionObj.ObjectType == ObjectTypes.Spot)
            {
                Spots.Remove((Spot)collisionObj);
            }
            else if (collisionObj.ObjectType == ObjectTypes.Switch)
            {
                Switches.Remove((Switch)collisionObj);
            }
            else if (collisionObj.ObjectType == ObjectTypes.Portal)
            {
                LevelPortal = null;
            }
            else if (collisionObj.ObjectType == ObjectTypes.Warp)
            {
                Warps.Remove((Warp)collisionObj);
            }
            else if (collisionObj.ObjectType == ObjectTypes.RockShooter)
            {
                RockShooters.Remove((RockShooter)collisionObj);
            }

            collisionObj.Context = null;
        }

        public void RemoveObject(ICollisionObj collisionObj)
        {
            LevelCollision.Remove(collisionObj);

            RemoveSpecializedObject(collisionObj);
        }

        public void RemoveObjects(List<ICollisionObj> collisionObjs)
        {
            if (collisionObjs == LevelCollision)
            {
                Debug.LogError($"The {nameof(collisionObjs)} list is the same as this context's {nameof(LevelCollision)} list. This can corrupt the level's state, so please fix it.");
            }

            for (int i = 0; i < collisionObjs.Count; i++)
            {
                RemoveObject(collisionObjs[i]);
            }
        }

        public void ClearAllObjects()
        {
            //Remove specialized objects first so we don't remove from the total object list while doing so
            for (int i = LevelCollision.Count - 1; i >= 0; i--)
            {
                RemoveSpecializedObject(LevelCollision[i]);
            }

            LevelCollision.Clear();
        }

        public void ClearAllObjectsAndCamera()
        {
            ClearAllObjects();
            SetCamera(null);
        }

        public void SetSurfaceTiles(SurfaceTileEngine surfaceTileEngine)
        {
            SurfaceTiles = surfaceTileEngine;
        }

        public void SetCamera(Camera2D levelCamera)
        {
            LevelCamera = levelCamera;
        }

        public void ResetSpotsForPortal()
        {
            SpotsForPortal = 0;
        }

        public void IncrementPortalUnlock(in int amount)
        {
            SpotsForPortal += amount;
            LevelPortal.Activated = (SpotsForPortal >= Spots.Count);

            if (amount > 0)
            {
                LevelPortal.OnCorrectSpotPlaced();
            }
        }
    }
}