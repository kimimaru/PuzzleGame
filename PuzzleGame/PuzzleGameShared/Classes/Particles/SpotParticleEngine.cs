/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// A Spot's particle engine.
    /// </summary>
    public class SpotParticleEngine : ParticleEngine
    {
        private int Index = 0;

        public SpotParticleEngine(in int maxParticles, in Vector2 position, Sprite particleSprite, in float renderDepth,
            in double minParticleLife, in double maxParticleLife)
            : base(maxParticles, position, particleSprite, renderDepth, minParticleLife, maxParticleLife)
        {
            
        }

        protected override void RecycleParticle(Particle particle)
        {
            base.RecycleParticle(particle);

            Index = (Index + 1) % 4;

            RecycleForIndex(particle, Index);
        }

        private void RecycleForIndex(Particle particle, in int index)
        {
            particle.transform.Position = Position;

            switch (index)
            {
                default:
                case 0:
                    particle.transform.Position += MinPositionOffset;
                    particle.Velocity = MinVelocity;
                    break;
                case 1:
                    particle.transform.Position += new Vector2(MaxPositionOffset.X, MinPositionOffset.Y);
                    particle.Velocity = MaxVelocity;
                    break;
                case 2:
                    particle.transform.Position += new Vector2(MinPositionOffset.X, MaxPositionOffset.Y);
                    particle.Velocity = MinVelocity;
                    break;
                case 3:
                    particle.transform.Position += MaxPositionOffset;
                    particle.Velocity = MaxVelocity;
                    break;
            }
        }

        public override void Resume()
        {
            base.Resume();

            for (int i = 0; i < MaxParticles / 2; i++)
            {
                EmitParticle();
            }
        }

        public override void Update()
        {
            UpdateAliveParticles();
        }
    }
}