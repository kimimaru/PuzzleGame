/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// Represents a particle in a <see cref="ParticleEngine"/>.
    /// </summary>
    public class Particle : ITransformable, ITintable, IUpdateable, IRenderDepth
    {
        public Transform transform { get; private set; } = new Transform();
        public Sprite ParticleSprite = null;
        public Vector2 Velocity = Vector2.Zero;
        public Vector2 Acceleration = Vector2.Zero;
        public float AngularVelocity = 0f;
        public Color TintColor
        {
            get => tintColor;
            set
            {
                OrigColor = value;
                tintColor = value;
            }
        }
        private Color tintColor = Color.White;
        private Color OrigColor = Color.White;
        public double TimeToLive
        {
            get => timeToLive;
            set
            {
                OrigTimeToLive = value;
                timeToLive = value;
            }
        }
        private double timeToLive = 0d;
        public float RenderDepth { get; set; } = 0f;
        public Vector2 ScaleOverTime = Vector2.Zero;
        public Color ColorOverTime = Color.White;
        public bool UseColorOverTime = false;
        public bool UseFloatOrigin = false;

        private double OrigTimeToLive = 0d;

        public Particle()
        {

        }

        public void Update()
        {
            timeToLive -= Time.ElapsedTime.TotalMilliseconds;

            float elapsedSeconds = (float)Time.ElapsedTime.TotalSeconds;

            transform.Position += (Velocity * elapsedSeconds);
            transform.Rotation += (AngularVelocity * elapsedSeconds);
            transform.Scale += (ScaleOverTime * elapsedSeconds);

            Velocity += (Acceleration * elapsedSeconds);

            if (UseColorOverTime == true)
            {
                tintColor = Interpolation.Interpolate(OrigColor, ColorOverTime, 1d - (timeToLive / OrigTimeToLive), Interpolation.InterpolationTypes.Linear);
            }
        }

        public void Render()
        {
            Vector2 origin = (UseFloatOrigin == true) ? ParticleSprite.GetOriginFloat() : ParticleSprite.GetOrigin();
            
            RenderingManager.Instance.DrawSprite(ParticleSprite.Tex, transform.Position, ParticleSprite.SourceRect,
                tintColor, transform.Rotation, origin, transform.Scale, SpriteEffects.None, RenderDepth);
        }
    }
}