/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// A particle engine that manages particles.
    /// </summary>
    public class ParticleEngine : IPosition, IUpdateable, IRenderDepth
    {
        [Recordable(RecordableFlags.None)]
        public bool IsEmitting { get; protected set; } = true;
        public double EmissionRate = 16d;
        public Vector2 Position { get; set; } = Vector2.Zero;
        public List<Particle> AliveParticles = null;
        public List<Particle> DeadParticles = null;
        public Sprite ParticleSprite = null;
        public int MaxParticles = 0;
        public float RenderDepth { get; set; } = 0f;
        public double MinParticleLife = 0d;
        public double MaxParticleLife = 0d;

        [Recordable(RecordableFlags.None)]
        public Vector2 MinPositionOffset = Vector2.Zero;
        [Recordable(RecordableFlags.None)]
        public Vector2 MaxPositionOffset = Vector2.Zero;
        [Recordable(RecordableFlags.None)]
        public Vector2 MinVelocity = Vector2.Zero;
        [Recordable(RecordableFlags.None)]
        public Vector2 MaxVelocity = Vector2.Zero;
        [Recordable(RecordableFlags.None)]
        public Vector2 MinAcceleration = Vector2.Zero;
        [Recordable(RecordableFlags.None)]
        public Vector2 MaxAcceleration = Vector2.Zero;
        public float MinAngularVelocity = 0f;
        public float MaxAngularVelocity = 0f;
        [Recordable(RecordableFlags.None)]
        public Vector2 MinInitScale = Vector2.Zero;
        [Recordable(RecordableFlags.None)]
        public Vector2 MaxInitScale = Vector2.Zero;
        public Vector2 MinScaleOverTime = Vector2.Zero;
        public Vector2 MaxScaleOverTime = Vector2.Zero;
        [Recordable(RecordableFlags.None)]
        public Color ParticleColor = Color.White;
        [Recordable(RecordableFlags.None)]
        public Color ColorOverTime = Color.White;
        [Recordable(RecordableFlags.None)]
        public bool UseColorOverTime = false;
        public bool UseFloatOrigin = true;

        protected double ElapsedTime = 0d;

        public ParticleEngine(in int maxParticles, in Vector2 position, Sprite particleSprite, in float renderDepth,
            in double minParticleLife, in double maxParticleLife)
        {
            Position = position;
            MaxParticles = maxParticles;
            DeadParticles = new List<Particle>(MaxParticles);
            AliveParticles = new List<Particle>(MaxParticles);
            ParticleSprite = particleSprite;
            RenderDepth = renderDepth;
            MinParticleLife = minParticleLife;
            MaxParticleLife = maxParticleLife;
        }

        /// <summary>
        /// Prewarms the particle engine by populating particles.
        /// </summary>
        public void PrewarmParticles()
        {
            for (int i = AliveParticles.Count + DeadParticles.Count; i < MaxParticles; i++)
            {
                DeadParticles.Add(new Particle());
            }
        }

        protected virtual void RecycleParticle(Particle particle)
        {
            Vector2 position = Position + RandomGlobals.Randomizer.NextVector2(MinPositionOffset, MaxPositionOffset);

            Vector2 velocity = RandomGlobals.Randomizer.NextVector2(MinVelocity, MaxVelocity);
            float angle = 0f;
            float angularVelocity = (float)RandomGlobals.Randomizer.NextDouble(MinAngularVelocity, MaxAngularVelocity);
            Vector2 scale = RandomGlobals.Randomizer.NextVector2(MinInitScale, MaxInitScale);
            double ttl = RandomGlobals.Randomizer.NextDouble(MinParticleLife, MaxParticleLife);
            Vector2 scaleOverTime = RandomGlobals.Randomizer.NextVector2(MinScaleOverTime, MaxScaleOverTime);
            Vector2 acceleration = RandomGlobals.Randomizer.NextVector2(MinAcceleration, MaxAcceleration);

            particle.ParticleSprite = ParticleSprite;
            particle.transform.Position = position;
            particle.transform.Rotation = angle;
            particle.transform.Scale = scale;
            particle.Velocity = velocity;
            particle.Acceleration = acceleration;
            particle.AngularVelocity = angularVelocity;
            particle.ScaleOverTime = scaleOverTime;
            particle.TintColor = ParticleColor;
            particle.ColorOverTime = ColorOverTime;
            particle.UseColorOverTime = UseColorOverTime;
            particle.TimeToLive = ttl;
            particle.RenderDepth = RenderDepth;
            particle.UseFloatOrigin = UseFloatOrigin;

            AliveParticles.Add(particle);
        }

        /// <summary>
        /// Stops the particle engine.
        /// </summary>
        /// <param name="immediate">true to stop all currently alive particles, false to let them continue until their lifetimes expire.</param>
        public void Stop(in bool immediate)
        {
            IsEmitting = false;

            if (immediate == false)
            {
                return;
            }

            ClearParticles();
        }

        /// <summary>
        /// Resumes the particle engine.
        /// </summary>
        public virtual void Resume()
        {
            IsEmitting = true;
        }


        /// <summary>
        /// Immediately clears all active particles in the particle engine.
        /// </summary>
        public void ClearParticles()
        {
            for (int i = AliveParticles.Count - 1; i >= 0; i--)
            {
                DeadParticles.Add(AliveParticles[i]);
                AliveParticles.RemoveAt(i);
            }
        }

        protected void EmitParticle()
        {
            //If we have a dead particle, reuse it
            if (DeadParticles.Count > 0)
            {
                int index = DeadParticles.Count - 1;
                RecycleParticle(DeadParticles[index]);
                DeadParticles.RemoveAt(index);
            }
            //Otherwise if we're under the max, create a new particle
            else if (AliveParticles.Count < MaxParticles)
            {
                RecycleParticle(new Particle());
            }
        }

        protected void UpdateEmission()
        {
            if (IsEmitting == false)
            {
                return;
            }

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
            bool emitted = false;

            //Emit as many particles as it should in this allocated time
            //This makes emission framerate-independent
            while (ElapsedTime >= EmissionRate)
            {
                emitted = true;
                ElapsedTime -= EmissionRate;

                EmitParticle();

                //Break out if the emission rate is less than or equal to 0 to prevent an infinite loop
                if (EmissionRate <= 0d)
                {
                    break;
                }
            }

            //Reset to 0 if any particles were emitted
            if (emitted == true)
            {
                ElapsedTime = 0d;
            }
        }

        protected void UpdateAliveParticles()
        {
            //Update all alive particles
            for (int i = AliveParticles.Count - 1; i >= 0; i--)
            {
                Particle aliveParticle = AliveParticles[i];

                aliveParticle.Update();
                if (aliveParticle.TimeToLive <= 0)
                {
                    AliveParticles.RemoveAt(i);
                    DeadParticles.Add(aliveParticle);
                }
            }
        }

        public virtual void Update()
        {
            UpdateEmission();

            UpdateAliveParticles();
        }

        public void Render()
        {
            //Render only all alive particles
            for (int i = 0; i < AliveParticles.Count; i++)
            {
                AliveParticles[i].Render();
            }
        }
    }
}