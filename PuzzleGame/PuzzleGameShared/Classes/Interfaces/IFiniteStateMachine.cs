﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// An interface for finite state machines.
    /// </summary>
    public interface IFiniteStateMachine
    {
        /// <summary>
        /// What happens when entering the state.
        /// </summary>
        void Enter();

        /// <summary>
        /// What happens when exiting the state.
        /// </summary>
        void Exit();

        /// <summary>
        /// Updates the state.
        /// </summary>
        void Update();
    }
}
