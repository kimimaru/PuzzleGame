﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// An interface for objects that perform cleanup logic.
    /// This is where they can clear events and other data.
    /// </summary>
    public interface ICleanup
    {
        /// <summary>
        /// Performs clean up on the object, clearing events and other data.
        /// </summary>
        void CleanUp();
    }
}
