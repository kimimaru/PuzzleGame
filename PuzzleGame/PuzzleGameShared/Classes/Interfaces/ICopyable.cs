﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// An interface for objects that can be copied.
    /// </summary>
    /// <typeparam name="T">The type of the object to return when copied.</typeparam>
    public interface ICopyable<T>
    {
        /// <summary>
        /// Returns a copy of the object of type T.
        /// </summary>
        /// <returns>A copy of the object of type T.</returns>
        T Copy();
    }
}
