﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame
{
    /// <summary>
    /// An interface for objects that can be enabled and disabled.
    /// </summary>
    public interface IEnableable
    {
        /// <summary>
        /// Whether the object is enabled or not.
        /// </summary>
        bool Enabled { get; set; }

        // <summary>
        // What happens when the object is enabled.
        // </summary>
        //void OnEnabled();

        // <summary>
        // What happens when the object is disabled.
        // </summary>
        //void OnDisabled();
    }
}
