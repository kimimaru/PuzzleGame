﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PuzzleGame
{
    /// <summary>
    /// Contains data for input config settings.
    /// </summary>
    public sealed class InputConfigData
    {
        public delegate void KeyboardMappingsChanged(in int kbIndex);
        public event KeyboardMappingsChanged KeyboardMappingsChangedEvent = null;

        public delegate void GamepadMappingsChanged(string gpIdentifier);
        public event GamepadMappingsChanged GamepadMappingsChangedEvent = null;

        public Dictionary<int, KeyboardMapData> KBMapData = new Dictionary<int, KeyboardMapData>(InputGlobals.GamePadInputIndex);
        public Dictionary<string, GamepadMapData> GPMapData = new Dictionary<string, GamepadMapData>(InputGlobals.MaxGamePadCount);

        public InputConfigData()
        {

        }

        public void AddOrChangeKeyboardMappings(in int kbIndex, InputConfigData.KeyboardMapData newKBMapData)
        {
            if (KBMapData.TryGetValue(kbIndex, out InputConfigData.KeyboardMapData kbMapData) == false)
            {
                kbMapData = new InputConfigData.KeyboardMapData(newKBMapData);
                DataHandler.inputConfigData.KBMapData.Add(kbIndex, kbMapData);
            }

            DataHandler.inputConfigData.KBMapData[kbIndex].CopyFrom(newKBMapData);

            KeyboardMappingsChangedEvent?.Invoke(kbIndex);
        }

        public void AddOrChangeGamepadMappings(string gpIdentifier, InputConfigData.GamepadMapData newGPMapData)
        {
            if (GPMapData.TryGetValue(gpIdentifier, out InputConfigData.GamepadMapData gpMapData) == false)
            {
                gpMapData = new InputConfigData.GamepadMapData(newGPMapData);
                DataHandler.inputConfigData.GPMapData.Add(gpIdentifier, gpMapData);
            }

            DataHandler.inputConfigData.GPMapData[gpIdentifier].CopyFrom(newGPMapData);

            GamepadMappingsChangedEvent?.Invoke(gpIdentifier);
        }

        /// <summary>
        /// Validates input config data upon being loaded.
        /// </summary>
        /// <param name="inputConfigData">An existing InputConfigData reference.</param>
        /// <returns>A new InputConfigData object if the existing one is null, otherwise the existing object.</returns>
        public static InputConfigData ValidateConfigData(InputConfigData inputConfigData)
        {
            if (inputConfigData == null)
            {
                return new InputConfigData();
            }

            if (inputConfigData.KBMapData == null)
            {
                inputConfigData.KBMapData = new Dictionary<int, KeyboardMapData>(InputGlobals.GamePadInputIndex);
            }

            foreach (KeyValuePair<int, KeyboardMapData> kbMapData in inputConfigData.KBMapData)
            {
                //Ensure mapping data isn't null
                if (kbMapData.Value.MappingData == null)
                {
                    kbMapData.Value.MappingData = new InputMapData(InputHandler.InputTypes.Keyboard);
                }
            }

            if (inputConfigData.GPMapData == null)
            {
                inputConfigData.GPMapData = new Dictionary<string, GamepadMapData>(InputGlobals.MaxGamePadCount);
            }

            foreach (KeyValuePair<string, GamepadMapData> gpMapData in inputConfigData.GPMapData)
            {
                //Ensure mapping data isn't null
                if (gpMapData.Value.MappingData == null)
                {
                    gpMapData.Value.MappingData = new InputMapData(InputHandler.InputTypes.GamePad);
                }
            }

            return inputConfigData;
        }

        public sealed class KeyboardMapData
        {
            public int KeyboardIndex = 0;
            public InputMapData MappingData = new InputMapData(InputHandler.InputTypes.Keyboard);

            public KeyboardMapData()
            {

            }

            public KeyboardMapData(in KeyboardMapData copy)
            {
                CopyFrom(copy);
            }

            public void CopyFrom(in KeyboardMapData copy)
            {
                KeyboardIndex = copy.KeyboardIndex;
                MappingData.ActionMappings.CopyDictionaryData(copy.MappingData.ActionMappings);
            }
        }

        public sealed class GamepadMapData
        {
            public string ControllerID = string.Empty;
            public string DisplayName = string.Empty;
            public InputMapData MappingData = new InputMapData(InputHandler.InputTypes.GamePad);

            public GamepadMapData()
            {

            }

            public GamepadMapData(in GamepadMapData copy)
            {
                CopyFrom(copy);
            }

            public void CopyFrom(in GamepadMapData copy)
            {
                ControllerID = copy.ControllerID;
                DisplayName = copy.DisplayName;
                MappingData.ActionMappings.CopyDictionaryData(copy.MappingData.ActionMappings);
            }
        }

        public sealed class InputMapData
        {
            public readonly Dictionary<string, int> ActionMappings = null;

            public InputMapData(in InputHandler.InputTypes inputType)
            {
                ActionMappings = new Dictionary<string, int>(InputGlobals.DefaultMappings[inputType]);
            }
        }
    }
}
