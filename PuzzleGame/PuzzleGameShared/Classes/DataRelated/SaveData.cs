﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace PuzzleGame
{
    public sealed class SaveData
    {
        public string Version = "0.0";
        public CompletionData CompleteData = new CompletionData();
        public Dictionary<int, WorldData> WorldSaves;
        public int LastPlayedLevel = int.MinValue;
        public int LastPlayedWorld = 0;
        public DemonstrationData DemoData = new DemonstrationData();
        public SettingsData Settings = new SettingsData();
        public MiscData Misc = new MiscData();

        public SaveData()
        {
            WorldSaves = new Dictionary<int, WorldData>() { { 0, new WorldData() } };
        }

        /// <summary>
        /// Validates save data upon being loaded.
        /// </summary>
        /// <param name="saveData">An existing SaveData reference.</param>
        /// <returns>A new SaveData object if the existing one is null, otherwise the existing object.</returns>
        public static SaveData ValidateSaveData(SaveData saveData)
        {
            //Ensure we have save data
            if (saveData == null)
            {
                saveData = new SaveData();
                return saveData;
            }

            //Ensure we have world saves
            if (saveData.WorldSaves == null)
            {
                saveData.WorldSaves = new Dictionary<int, WorldData>() { { 0, new WorldData() } };
            }

            //Ensure the current world is valid
            if (saveData.WorldSaves.ContainsKey(saveData.LastPlayedWorld) == false)
            {
                saveData.LastPlayedWorld = 0;
            }

            //Ensure the last played level has save data
            WorldData worldData = saveData.WorldSaves[saveData.LastPlayedWorld];
            if (worldData.levelData.ContainsKey(saveData.LastPlayedLevel) == false)
            {
                //There's no data, so go back to the first possible level
                saveData.LastPlayedLevel = int.MinValue;
            }

            //Ensure we have various data available
            if (saveData.CompleteData == null)
            {
                saveData.CompleteData = new CompletionData();
            }

            if (saveData.DemoData == null)
            {
                saveData.DemoData = new DemonstrationData();
            }

            if (saveData.Settings == null)
            {
                saveData.Settings = new SettingsData();
            }

            if (saveData.Misc == null)
            {
                saveData.Misc = new MiscData();
            }

            return saveData;
        }

        public static bool HasWorldData(in int worldID)
        {
            return DataHandler.saveData.WorldSaves.ContainsKey(worldID);
        }

        public static WorldData GetWorldData(in int worldID)
        {
            return DataHandler.saveData.WorldSaves[worldID];
        }

        public static bool GetWorldData(in int worldID, out WorldData worldData)
        {
            return DataHandler.saveData.WorldSaves.TryGetValue(worldID, out worldData);
        }

        public static bool HasLevelData(in int worldID, in int levelID)
        {
            return DataHandler.saveData.WorldSaves[worldID].levelData.ContainsKey(levelID);
        }

        public static LevelData GetLevelData(in int worldID, in int levelID)
        {
            return DataHandler.saveData.WorldSaves[worldID].levelData[levelID];
        }

        public static bool GetLevelData(in int worldID, in int levelID, out LevelData lvlData)
        {
            return DataHandler.saveData.WorldSaves[worldID].levelData.TryGetValue(levelID, out lvlData);
        }

        public static void SetLastPlayedWorld(in int worldID)
        {
            DataHandler.saveData.LastPlayedWorld = worldID;
        }

        public static void SetLastPlayedLevel(in int levelID)
        {
            DataHandler.saveData.LastPlayedLevel = levelID;
        }
    }

    public sealed class WorldData
    {
        public Dictionary<int, LevelData> levelData = new Dictionary<int, LevelData>();
    }

    public sealed class LevelStats
    {
        /// <summary>
        /// The number of moves performed.
        /// </summary>
        public int NumMoves = 0;

        /// <summary>
        /// The amount of time spent playing the level.
        /// </summary>
        public double LevelTime = 0d;

        /// <summary>
        /// Copies data from another <see cref="LevelStats"/>.
        /// <para>If any part of the data is improved, use <see cref="CopyImprovedDataFrom(in LevelStats)"/> instead.
        /// This method should be used only when copying data upon the first completion of a level.</para>
        /// </summary>
        /// <param name="levelStats">The data to copy from.</param>
        public void CopyDataFrom(in LevelStats levelStats)
        {
            NumMoves = levelStats.NumMoves;
            LevelTime = levelStats.LevelTime;
        }

        /// <summary>
        /// Copies improved data from another <see cref="LevelStats"/>.
        /// </summary>
        /// <param name="levelStats">The data to copy from.</param>
        public void CopyImprovedDataFrom(in LevelStats levelStats)
        {
            if (IsImprovedMoves(levelStats, this) == true)
            {
                NumMoves = levelStats.NumMoves;
            }

            if (IsImprovedTime(levelStats, this) == true)
            {
                LevelTime = levelStats.LevelTime;
            }
        }

        /// <summary>
        /// Clears the level stats.
        /// </summary>
        public void Clear()
        {
            NumMoves = 0;
            LevelTime = 0d;
        }

        /// <summary>
        /// Checks if one set of <see cref="LevelStats"/> has improved <see cref="NumMoves"/> over another.
        /// </summary>
        /// <param name="statsToCheck">The stats to check.</param>
        /// <param name="statsToCompareWith">The stats to compare with.</param>
        /// <returns>true if <paramref name="statsToCheck"/> has <see cref="NumMoves"/> improvements over <paramref name="statsToCompareWith"/>, otherwise false.</returns>
        public static bool IsImprovedMoves(in LevelStats statsToCheck, in LevelStats statsToCompareWith)
        {
            if (statsToCheck == null)
                return false;

            if (statsToCheck != null && statsToCompareWith == null)
                return true;

            return (statsToCheck.NumMoves < statsToCompareWith.NumMoves);
        }

        /// <summary>
        /// Checks if one set of <see cref="LevelStats"/> has improved <see cref="LevelTime"/> over another.
        /// </summary>
        /// <param name="statsToCheck">The stats to check.</param>
        /// <param name="statsToCompareWith">The stats to compare with.</param>
        /// <returns>true if <paramref name="statsToCheck"/> has <see cref="LevelTime"/> improvements over <paramref name="statsToCompareWith"/>, otherwise false.</returns>
        public static bool IsImprovedTime(in LevelStats statsToCheck, in LevelStats statsToCompareWith)
        {
            if (statsToCheck == null)
                return false;

            if (statsToCheck != null && statsToCompareWith == null)
                return true;

            return (statsToCheck.LevelTime < statsToCompareWith.LevelTime);
        }

        /// <summary>
        /// Returns if one set of <see cref="LevelStats"/> is improved over another.
        /// </summary>
        /// <param name="statsToCheck">The stats to check.</param>
        /// <param name="statsToCompareWith">The stats to compare with.</param>
        /// <returns>true if <paramref name="statsToCheck"/> has improvements over <paramref name="statsToCompareWith"/>, otherwise false.</returns>
        public static bool IsImprovedData(in LevelStats statsToCheck, in LevelStats statsToCompareWith)
        {
            return (IsImprovedMoves(statsToCheck, statsToCompareWith) == true || IsImprovedTime(statsToCheck, statsToCompareWith) == true);
        }
    }

    public sealed class LevelData
    {
        /// <summary>
        /// The stats for the level.
        /// </summary>
        public LevelStats LvlStats = new LevelStats();

        public uint TimesComplete = 0u;

        [Newtonsoft.Json.JsonIgnore]
        public bool Complete => (TimesComplete > 0);

        public void IncrementTimesComplete()
        {
            if (TimesComplete != uint.MaxValue) TimesComplete++;
        }
    }

    public sealed class DemonstrationData
    {
        //Basic actions
        public bool HasWalk = true;
        public bool HasGrab = true;
        public bool HasGrabMove = true;
        public bool HasUndo = true;

        //Puzzle elements
        public bool HasSpot = false;
        public bool HasSwitch = false;
        public bool HasPipes = false;
        public bool HasDirections = false;
        public bool HasWarps = false;
        public bool HasIce = false;
        public bool HasMud = false;
        public bool HasPaired = false;

        /// <summary>
        /// Resets the demonstration data to its default values.
        /// </summary>
        public void Reset()
        {
            HasWalk = true;
            HasGrab = true;
            HasGrabMove = true;
            HasUndo = true;

            HasSpot = false;
            HasSwitch = false;
            HasPipes = false;
            HasDirections = false;
            HasWarps = false;
            HasIce = false;
            HasMud = false;
            HasPaired = false;
        }

        /// <summary>
        /// Copies data from another <see cref="DemonstrationData"/>.
        /// </summary>
        /// <param name="demoData">The data to copy from.</param>
        public void CopyDataFrom(in DemonstrationData demoData)
        {
            HasWalk |= demoData.HasWalk;
            HasGrab |= demoData.HasGrab;
            HasGrabMove |= demoData.HasGrabMove;
            HasUndo |= demoData.HasUndo;

            HasSpot |= demoData.HasSpot;
            HasSwitch |= demoData.HasSwitch;
            HasPipes |= demoData.HasPipes;
            HasDirections |= demoData.HasDirections;
            HasWarps |= demoData.HasWarps;
            HasIce |= demoData.HasIce;
            HasMud |= demoData.HasMud;
            HasPaired |= demoData.HasPaired;
        }
    }

    public sealed class CompletionData
    {
        public bool UnlockBonus = false;
    }

    public sealed class MiscData
    {
        public List<int> JustFinishedLevels = new List<int>(8);
        public bool InCustomLevel = false;
        public bool ShowBonusUnlock = false;
    }

    public sealed class SettingsData
    {
        public delegate void SettingsChanged();

        public event SettingsChanged SettingsChangedEvent = null;

        public int ResOption = 0;
        public int FullscreenOption = 0;
        public float MusicVolume = .5f;
        public float SoundVolume = .5f;
        public int GrabOption = 0;
        public int InputOption = (int)PlayerInputPriorityOptions.Buffered;
        public int Timestep = (int)TimestepSettings.Variable;
        public int VSync = (int)VSyncSettings.Enabled;
        public int HUDDisplaySetting = (int)HUDDisplayOptions.All;

        #region Wrappers

        public void ChangeResOption(in int resOption)
        {
            ChangeSetting(ref ResOption, resOption);
        }

        public void ChangeFullscreenOption(in int fullScreen)
        {
            ChangeSetting(ref FullscreenOption, fullScreen);
        }

        public void ChangeMusicVolume(in float musicVolume)
        {
            ChangeSetting(ref MusicVolume, musicVolume);
        }

        public void ChangeSoundVolume(in float soundVolume)
        {
            ChangeSetting(ref SoundVolume, soundVolume);
        }

        public void ChangeHUDSetting(in int hudDisplaySetting)
        {
            ChangeSetting(ref HUDDisplaySetting, hudDisplaySetting);
        }

        public void ChangeGrabOption(in int grabOption)
        {
            ChangeSetting(ref GrabOption, grabOption);
        }

        public void ChangeInputOption(in int inputOption)
        {
            ChangeSetting(ref InputOption, inputOption);
        }

        public void ChangeTimestep(in int timeStep)
        {
            ChangeSetting(ref Timestep, timeStep);
        }

        public void ChangeVSync(in int vsync)
        {
            ChangeSetting(ref VSync, vsync);
        }

        private void ChangeSetting(ref int setting, in int newSetting)
        {
            bool changed = (setting != newSetting);
            setting = newSetting;
            if (changed == true) SettingsChangedEvent?.Invoke();
        }

        private void ChangeSetting(ref float setting, in float newSetting)
        {
            bool changed = (setting != newSetting);
            setting = newSetting;
            if (changed == true) SettingsChangedEvent?.Invoke();
        }

        #endregion
    }

    public sealed class AllDescriptionData
    {
        public Dictionary<int, WorldDescriptionData> WorldDescriptions = new Dictionary<int, WorldDescriptionData>();

        /// <summary>
        /// Creates description data by loading a specified number of the world maps and retrieving world and level description data.
        /// </summary>
        /// <param name="numWorlds">The number of worlds to obtain descriptions for.</param>
        /// <returns>An AllDescriptionData containing WorldDescriptions for the designated number of worlds.</returns>
        public static AllDescriptionData CreateAllDescriptionData(in int numWorlds)
        {
            AllDescriptionData allDescData = new AllDescriptionData();

            //Load all the overworld maps for the number of worlds and fetch their data
            for (int i = 0; i < numWorlds; i++)
            {
                //Catch any errors when loading the world
                try
                {
                    string worldMapToLoadName = "World" + i;
                    OverworldMap overworldMap = new OverworldMap(AssetManager.Instance.LoadTiledMap(worldMapToLoadName));
                    WorldDescriptionData worldDesc = WorldDescriptionData.CreateWorldDescription(overworldMap.WorldID, overworldMap.WorldName,
                        worldMapToLoadName, overworldMap.Nodes);
                    allDescData.WorldDescriptions.Add(i, worldDesc);
                }
                catch (Exception e)
                {
                    Debug.LogError($"Unable to load description data for World {i}: {e.Message}");
                }
            }

            return allDescData;
        }
    }

    public sealed class WorldDescriptionData
    {
        public int WorldID = 0;
        public string WorldName = string.Empty;
        public int LevelCount = 0;
        public string MapToLoad = string.Empty;
        public LevelDescriptionData[] LvlDescriptions = null;

        private static Comparison<LevelMapNode> LevelMapNodeSort = SortLevelMapNodes;

        public static WorldDescriptionData CreateWorldDescription(in int worldID, string worldName, string mapToLoad, List<OverworldMapNode> overworldMapNodes)
        {
            WorldDescriptionData worldDescription = new WorldDescriptionData();
            worldDescription.WorldID = worldID;
            worldDescription.WorldName = worldName;
            worldDescription.MapToLoad = mapToLoad;

            int mapNodesCount = overworldMapNodes.Count;

            //Store all levels in a list so we can sort them
            List<LevelMapNode> levelNodes = new List<LevelMapNode>(mapNodesCount);

            //Get all the levels
            for (int i = 0; i < mapNodesCount; i++)
            {
                OverworldMapNode mapNode = overworldMapNodes[i];
                if (mapNode.NodeType == MapNodeTypes.Level)
                {
                    LevelMapNode lvlMapNode = (LevelMapNode)mapNode;
                    levelNodes.Add(lvlMapNode);

                    worldDescription.LevelCount++;
                }
            }

            //Sort the level nodes in order
            levelNodes.Sort(LevelMapNodeSort);

            LevelDescriptionData[] levelDescriptions = new LevelDescriptionData[levelNodes.Count];

            //Go in order and add the descriptions
            for (int i = 0; i < levelDescriptions.Length; i++)
            {
                LevelMapNode lvlNode = levelNodes[i];

                LevelDescriptionData lvlDescription = new LevelDescriptionData();
                levelDescriptions[i] = lvlDescription;

                lvlDescription.LevelID = lvlNode.LevelID;
                lvlDescription.LevelDisplayName = lvlNode.DisplayName;
                lvlDescription.LevelDescription = lvlNode.Description;
            }

            worldDescription.LvlDescriptions = levelDescriptions;

            return worldDescription;
        }

        private static int SortLevelMapNodes(LevelMapNode lvlNode1, LevelMapNode lvlNode2)
        {
            if (lvlNode1 == null)
                return 1;
            if (lvlNode2 == null)
                return -1;

            if (lvlNode1.NumberInWorld < lvlNode2.NumberInWorld)
                return -1;
            if (lvlNode1.NumberInWorld > lvlNode2.NumberInWorld)
                return 1;

            return 0;
        }
    }

    public sealed class LevelDescriptionData
    {
        public int LevelID = 0;
        public string LevelDisplayName = string.Empty;
        public string LevelDescription = string.Empty;
    }
}
