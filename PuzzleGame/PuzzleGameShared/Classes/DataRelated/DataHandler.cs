/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace PuzzleGame
{
    /// <summary>
    /// Handles several types of data that need to be accessed at a global level.
    /// </summary>
    public static class DataHandler
    {
        public static SaveData saveData { get; private set; } = null;
        public static AllDescriptionData levelDescData { get; private set; } = null;
        public static InputConfigData inputConfigData { get; private set; } = null;

        #region Save Data

        public static void SaveSaveData()
        {
            //Save to the save data file
            try
            {
                string folderPath = ConfigGlobals.GetApplicationDataPath();

                //If the directory doesn't exist, create it
                if (string.IsNullOrEmpty(folderPath) == false && Directory.Exists(folderPath) == false)
                {
                    try
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError($"Failed creating save data folder at \"{folderPath}\". Message: {e.Message}");
                        return;
                    }
                }

                string filePath = ContentGlobals.GetSaveDataPath();

                string json = string.Empty;
#if DEBUG
                json = JsonConvert.SerializeObject(saveData, Formatting.Indented);
#else
                using (MemoryStream ms = new MemoryStream())
                {
                    using (BsonWriter writer = new BsonWriter(ms))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Culture = System.Globalization.CultureInfo.InvariantCulture;
                        
                        serializer.Serialize(writer, saveData);
                    }

                    json = Convert.ToBase64String(ms.ToArray());
                }
#endif
                File.WriteAllText(filePath, json);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }

        public static void LoadSaveData()
        {
            //Load the save data file if it exists
            try
            {
                //Handle text files for debug and binary for release
                string filePath = ContentGlobals.GetSaveDataPath();

                if (File.Exists(filePath) == true)
                {
                    string text = File.ReadAllText(filePath);
#if DEBUG
                    saveData = JsonConvert.DeserializeObject<SaveData>(text);
#else
                    byte[] data = Convert.FromBase64String(text);

                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Culture = System.Globalization.CultureInfo.InvariantCulture;

                    using (MemoryStream ms = new MemoryStream(data))
                    {
                        using (BsonReader reader = new BsonReader(ms))
                        {
                            saveData = serializer.Deserialize<SaveData>(reader);
                        }
                    }
#endif
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }

        public static void LoadAndValidateSaveData()
        {
            LoadSaveData();
            saveData = SaveData.ValidateSaveData(saveData);
        }

        #endregion

        #region Input Config Data

        /// <summary>
        /// Creates the directory and all root directories of where config files are stored.
        /// </summary>
        /// <returns>true if the config directory already exists or is created, otherwise false.</returns>
        public static bool CreateConfigDirectory()
        {
            string folderPath = ConfigGlobals.GetInputConfigFolderPath();

            //If the directory doesn't exist, create it
            if (string.IsNullOrEmpty(folderPath) == false && Directory.Exists(folderPath) == false)
            {
                try
                {
                    Directory.CreateDirectory(folderPath);
                }
                catch (Exception e)
                {
                    Debug.LogError($"Failed creating input config data folder at \"{folderPath}\". Message: {e.Message}");
                    return false;
                }
            }

            return true;
        }

        public static void SaveInputConfigData()
        {
            //Save to the input config data file
            try
            {
                string folderPath = ConfigGlobals.GetInputConfigFolderPath();

                //If the directory doesn't exist, create it
                if (string.IsNullOrEmpty(folderPath) == false && Directory.Exists(folderPath) == false)
                {
                    try
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError($"Failed creating input config data folder at \"{folderPath}\". Message: {e.Message}");
                        return;
                    }
                }

                string filePath = ConfigGlobals.GetInputConfigDataPath();

                string json = JsonConvert.SerializeObject(inputConfigData, Formatting.Indented);

                File.WriteAllText(filePath, json);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }

        public static void LoadInputConfigData()
        {
            //Load the input config data file if it exists
            try
            {
                //Handle text files for debug and binary for release
                string filePath = ConfigGlobals.GetInputConfigDataPath();

                if (File.Exists(filePath) == true)
                {
                    string text = File.ReadAllText(filePath);

                    inputConfigData = JsonConvert.DeserializeObject<InputConfigData>(text);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }

        public static void LoadAndValidateInputConfigData()
        {
            LoadInputConfigData();
            inputConfigData = InputConfigData.ValidateConfigData(inputConfigData);
        }

        #endregion

        /// <summary>
        /// Reloads the cached level description data.
        /// </summary>
        public static void ReloadLevelDescriptionData(in int numWorlds)
        {
            levelDescData = AllDescriptionData.CreateAllDescriptionData(numWorlds);
        }
    }
}