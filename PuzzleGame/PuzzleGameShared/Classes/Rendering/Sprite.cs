﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// Represents a Sprite.
    /// </summary>
    public class Sprite
    {
        public Texture2D Tex = null;
        public Rectangle? SourceRect = null;

        /// <summary>
        /// The pivot of the Sprite, from 0 to 1.
        /// </summary>
        public Vector2 Pivot = new Vector2(.5f, .5f);

        public Sprite(Texture2D texture, in Rectangle? sourceRect)
        {
            Tex = texture;
            SourceRect = sourceRect;
        }

        public Sprite(Texture2D texture, in Rectangle? sourceRect, in Vector2 pivot)
            : this(texture, sourceRect)
        {
            Pivot = pivot;
        }

        public Vector2 GetOrigin()
        {
            Rectangle rect = Rectangle.Empty;

            if (SourceRect != null)
            {
                rect = SourceRect.Value;
            }
            else if (Tex != null)
            {
                rect = Tex.Bounds;
            }

            return rect.GetOrigin(Pivot.X, Pivot.Y);
        }

        public Vector2 GetOriginFloat()
        {
            Rectangle rect = Rectangle.Empty;

            if (SourceRect != null)
            {
                rect = SourceRect.Value;
            }
            else if (Tex != null)
            {
                rect = Tex.Bounds;
            }

            return rect.GetOriginFloat(Pivot.X, Pivot.Y);
        }
    }
}
