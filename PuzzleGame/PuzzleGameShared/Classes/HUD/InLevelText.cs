/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Text shown in a level to tell the player something.
    /// </summary>
    public sealed class InLevelText : ITransformable, ITintable, IUpdateable
    {
        public Transform transform { get; private set; } = new Transform();

        /// <summary>
        /// The text to display.
        /// </summary>
        private string DisplayText = string.Empty;

        /// <summary>
        /// The font used to render the text.
        /// </summary>
        private SpriteFont Font = null;

        public Color TintColor { get; set; } = Color.White;
        public Color ShadowColor = Color.Black * .5f;

        public bool IsChangingColor = false;
        private Color StartColor = Color.White;
        private Color EndColor = Color.White;
        private double ColorChangeTime = 0d;
        private double ElapsedTime = 0d;

        public InLevelText()
        {
            
        }

        public void Initialize(PlayableContext context, in ObjectInitInfo initInfo)
        {
            string fontName = ContentGlobals.SpecialFont19px;

            transform.Position = initInfo.Position;

            //Get the text
            if (initInfo.Properties.TryGetValue(LevelGlobals.TextKey, out DisplayText) == false)
            {
                //There's no text to render
                Debug.LogError("No text to render!");
                return;
            }

            //Get the font
            if (initInfo.Properties.TryGetValue(LevelGlobals.TextFontKey, out string textFontName) == true)
            {
                fontName = textFontName;
            }

            //Get the font scale
            if (initInfo.Properties.TryGetValue(LevelGlobals.ScaleKey, out string textScaleStr) == true)
            {
                if (float.TryParse(textScaleStr, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture,
                    out float textScale) == true)
                {
                    transform.Scale = new Vector2(textScale, textScale);
                }
            }

            //Get the color
            Color color = Color.White;

            if (initInfo.Properties.TryGetValue(LevelGlobals.TextRedKey, out string redStr) == true)
            {
                if (byte.TryParse(redStr, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture,
                    out byte red) == true)
                {
                    color.R = red;
                }
            }
            if (initInfo.Properties.TryGetValue(LevelGlobals.TextGreenKey, out string greenStr) == true)
            {
                if (byte.TryParse(greenStr, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture,
                    out byte green) == true)
                {
                    color.G = green;
                }
            }
            if (initInfo.Properties.TryGetValue(LevelGlobals.TextBlueKey, out string blueStr) == true)
            {
                if (byte.TryParse(blueStr, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture,
                    out byte blue) == true)
                {
                    color.B = blue;
                }
            }
            float alpha = 1f;
            if (initInfo.Properties.TryGetValue(LevelGlobals.TextAlphaKey, out string alphaStr) == true)
            {
                if (float.TryParse(alphaStr, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture,
                    out alpha) == false)
                {
                    alpha = 1f;
                }
            }

            TintColor = color * alpha;

            Font = AssetManager.Instance.LoadFont(fontName);
        }

        public void ChangeColor(in Color startColor, in Color endColor, in double changeTime)
        {
            IsChangingColor = true;
            ColorChangeTime = changeTime;
            StartColor = startColor;
            EndColor = endColor;
            ElapsedTime = 0d;

            TintColor = StartColor;
        }

        public void ChangeDisplayText(string displayText)
        {
            DisplayText = displayText;
        }

        public void Update()
        {
            if (IsChangingColor == false)
            {
                return;
            }

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
            if (ElapsedTime >= ColorChangeTime)
            {
                TintColor = EndColor;
                IsChangingColor = false;
                ElapsedTime = 0d;
            }
            else
            {
                TintColor = Interpolation.Interpolate(StartColor, EndColor, ElapsedTime / ColorChangeTime, Interpolation.InterpolationTypes.Linear);
            }
        }

        public void Render()
        {
            if (TintColor != Color.Transparent)
            {
                RenderingManager.Instance.spriteBatch.DrawStringWithShadow(new Vector2(3f, 3f), ShadowColor * (TintColor.A / 255f), -.001f, Font, DisplayText, transform.Position,
                    TintColor, 0f, Vector2.Zero, transform.Scale, SpriteEffects.None, LevelGlobals.BaseTextRenderDepth);
            }
        }
    }
}