/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    public sealed class GameHUD : ICleanup
    {
        private IngameState InGameState = null;
        private SpriteFont Font = null;

        private int CachedMoves = -1;
        private int CachedTimeSeconds = -1;

        private readonly Color RestartStartColor = Color.White;
        private readonly Color RestartEndColor = Color.LightGray;
        private const double RestartBlinkTime = 150d;
        public bool RestartingLevel = false;
        private const string RestartingText = "Restarting...";

        private double TotalBlinkTime = 0d;

        //private readonly Vector2 ShadowDist = new Vector2(3f, 3f);
        //private readonly Color ShadowColor = Color.Black * .5f;

        #region Controls UI

        private UIText PauseText = null;
        private UIText RestartText = null;
        private UIText UndoText = null;

        private const double ControlsAlphaTime = 500d;
        private double CurAlphaTime = 0d;

        private readonly Color ControlsStartColor = Color.White * 0f;
        private readonly Color ControlsEndColor = Color.White;
        private readonly Color ControlsOutlineStartColor = Color.Black * 0f;
        private readonly Color ControlsOutlineEndColor = Color.Black * .8f;

        private const float TimeYOffset = 20f;
        private Vector2 OrigTimeTextPos = Vector2.Zero;
        private Vector2 OrigTimeIconPos = Vector2.Zero;

        private HUDDisplayOptions CachedHUDSetting = HUDDisplayOptions.All;

        #endregion

        private UIText MovesText = null;
        private UIText TimeText = null;
        private UIText IsRestartingText = null;

        private UISprite MovesIcon = null;
        private UISprite TimeIcon = null;

        public GameHUD(IngameState inGameState, in bool canUndo)
        {
            InGameState = inGameState;
            Font = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont10px);

            CachedHUDSetting = (HUDDisplayOptions)DataHandler.saveData.Settings.HUDDisplaySetting;

            SetUpUIObjects(canUndo);
        }

        public void CleanUp()
        {
            DataHandler.saveData.Settings.SettingsChangedEvent -= OnGameSettingsChanged;
            DataHandler.inputConfigData.KeyboardMappingsChangedEvent -= OnKeyboardMappingsChanged;
        }

        public void Reset()
        {
            if (CachedMoves != 0)
            {
                CachedMoves = 0;
                UpdateMovesText();
            }

            if (CachedTimeSeconds != 0)
            {
                CachedTimeSeconds = 0;
                UpdateTimeText();
            }

            CachedHUDSetting = (HUDDisplayOptions)(-1);
            OnGameSettingsChanged();

            OnKeyboardMappingsChanged(0);

            RestartingLevel = false;
            TotalBlinkTime = 0d;

            CurAlphaTime = 0d;
            PauseText.TintColor = ControlsStartColor;
            RestartText.TintColor = ControlsStartColor;

            if (UndoText != null)
                UndoText.TintColor = ControlsStartColor;

            PauseText.TextData.OutlineColor = ControlsOutlineStartColor;
            RestartText.TextData.OutlineColor = ControlsOutlineStartColor;

            if (UndoText != null)
                UndoText.TextData.OutlineColor = ControlsOutlineStartColor;

            IsRestartingText.Visible = false;
            IsRestartingText.TintColor = RestartStartColor;

            DataHandler.saveData.Settings.SettingsChangedEvent -= OnGameSettingsChanged;
            DataHandler.saveData.Settings.SettingsChangedEvent += OnGameSettingsChanged;

            DataHandler.inputConfigData.KeyboardMappingsChangedEvent -= OnKeyboardMappingsChanged;
            DataHandler.inputConfigData.KeyboardMappingsChangedEvent += OnKeyboardMappingsChanged;
        }

        private void UpdateMovesText()
        {
            MovesText.Text = CachedMoves.ToString();
        }

        private void UpdateTimeText()
        {
            TimeText.Text = LevelGlobals.GetFormattedLevelTimeString(CachedTimeSeconds * Time.MsPerS);
        }

        public void Update()
        {
            TotalBlinkTime += Time.ElapsedTime.TotalMilliseconds;

            if (CachedMoves != InGameState.levelStats.NumMoves)
            {
                CachedMoves = InGameState.levelStats.NumMoves;
                UpdateMovesText();
            }

            int curSeconds = (int)(InGameState.levelStats.LevelTime / Time.MsPerS);

            if (CachedTimeSeconds != curSeconds)
            {
                CachedTimeSeconds = curSeconds;
                UpdateTimeText();
            }

            HandleControlsColor();

            if (RestartingLevel == true)
            {
                double time = UtilityGlobals.PingPong(TotalBlinkTime / RestartBlinkTime, 0f, 1f);
                Color color = Interpolation.Interpolate(RestartStartColor, RestartEndColor, time, Interpolation.InterpolationTypes.Linear);

                IsRestartingText.TintColor = color;
                IsRestartingText.Visible = (CachedHUDSetting != HUDDisplayOptions.None);
            }
            else
            {
                IsRestartingText.Visible = false;
            }
        }

        private void HandleControlsColor()
        {
            CurAlphaTime = UtilityGlobals.Clamp(CurAlphaTime + Time.ElapsedTime.TotalMilliseconds, 0d, ControlsAlphaTime);

            double time = CurAlphaTime / ControlsAlphaTime;
            Color newColor = Interpolation.Interpolate(ControlsStartColor, ControlsEndColor, time,
                Interpolation.InterpolationTypes.Linear);

            Color outlineColor = Interpolation.Interpolate(ControlsOutlineStartColor, ControlsOutlineEndColor, time,
                Interpolation.InterpolationTypes.QuinticIn);
            PauseText.TextData.OutlineColor = outlineColor;
            RestartText.TextData.OutlineColor = outlineColor;

            if (UndoText != null)
                UndoText.TextData.OutlineColor = outlineColor;

            PauseText.TintColor = newColor;
            RestartText.TintColor = newColor;

            if (UndoText != null)
                UndoText.TintColor = newColor;
        }

        private void SetUpUIObjects(in bool canUndo)
        {
            Vector2 controlsOrigin = new Vector2(0f, .5f);
            Vector2 controlsScale = Vector2.One;

            const float controlsX = 8f;

            UITextData outlineData = new UITextData(Color.Black * .8f, 1f, -.001f);

            PauseText = UIHelpers.CreateUIText(Font, "Esc: Pause", controlsOrigin, controlsScale, Color.White,
                UITextOptions.Outline, outlineData);
            PauseText.Position = new Vector2(controlsX, 18f);

            RestartText = UIHelpers.CreateUIText(Font, "R (Hold): Restart", controlsOrigin, controlsScale, Color.White,
                UITextOptions.Outline, outlineData);
            RestartText.Position = new Vector2(controlsX, 38f);

            if (canUndo == true)
            {
                UndoText = UIHelpers.CreateUIText(Font, "U: Undo", controlsOrigin, controlsScale, Color.White,
                    UITextOptions.Outline, outlineData);
                UndoText.Position = new Vector2(controlsX, 58f);
            }

            IsRestartingText = UIHelpers.CreateUIText(Font, RestartingText, controlsOrigin, Vector2.One, Color.White,
                UITextOptions.Outline, outlineData);
            IsRestartingText.Position = new Vector2((RenderingGlobals.BaseResolutionWidth / 2) + 105f, 18f);
            IsRestartingText.Visible = false;

            MovesText = UIHelpers.CreateUIText(Font, string.Empty, controlsOrigin, controlsScale, Color.White,
                UITextOptions.Outline, outlineData);
            MovesText.Position = new Vector2(RenderingGlobals.BaseResolutionWidth / 2, 18f);

            TimeText = UIHelpers.CreateUIText(Font, string.Empty, controlsOrigin, controlsScale, Color.White,
                UITextOptions.Outline, outlineData);
            TimeText.Position = new Vector2(RenderingGlobals.BaseResolutionWidth / 2, 18f + TimeYOffset);

            //Icons
            Texture2D tex = AssetManager.Instance.LoadTexture(ContentGlobals.UITex);

            MovesIcon = new UISprite(new Sprite(tex, ContentGlobals.MovesIconRectSmall, controlsOrigin));
            MovesIcon.Position = MovesText.Position + new Vector2(-MovesIcon.sprite.SourceRect.Value.Width - 8f, -5f);

            TimeIcon = new UISprite(new Sprite(tex, ContentGlobals.TimeIconRectSmall, controlsOrigin));
            TimeIcon.Position = TimeText.Position + new Vector2(-TimeIcon.sprite.SourceRect.Value.Width - 8f, -5f);

            OrigTimeTextPos = TimeText.Position;
            OrigTimeIconPos = TimeIcon.Position;
        }

        private void OnGameSettingsChanged()
        {
            //Change the visibility of the HUD based on the setting
            if ((int)CachedHUDSetting == DataHandler.saveData.Settings.HUDDisplaySetting)
            {
                return;
            }

            CachedHUDSetting = (HUDDisplayOptions)DataHandler.saveData.Settings.HUDDisplaySetting;

            bool movesVisibility = (CachedHUDSetting == HUDDisplayOptions.All || CachedHUDSetting == HUDDisplayOptions.Moves || CachedHUDSetting == HUDDisplayOptions.ControlsMoves || CachedHUDSetting == HUDDisplayOptions.MovesTime);
            bool timeVisibility = (CachedHUDSetting == HUDDisplayOptions.All || CachedHUDSetting == HUDDisplayOptions.Time || CachedHUDSetting == HUDDisplayOptions.ControlsTime || CachedHUDSetting == HUDDisplayOptions.MovesTime);

            bool controlsVisibility = (CachedHUDSetting == HUDDisplayOptions.All || CachedHUDSetting == HUDDisplayOptions.Controls || CachedHUDSetting == HUDDisplayOptions.ControlsMoves || CachedHUDSetting == HUDDisplayOptions.ControlsTime);

            MovesText.Visible = movesVisibility;
            MovesIcon.Visible = movesVisibility;

            TimeText.Visible = timeVisibility;
            TimeIcon.Visible = timeVisibility;

            TimeText.Position = OrigTimeTextPos;
            TimeIcon.Position = OrigTimeIconPos;

            PauseText.Visible = controlsVisibility;
            RestartText.Visible = controlsVisibility;
            if (UndoText != null)
                UndoText.Visible = controlsVisibility;

            //If no HUD is specified, don't show the restarting text
            IsRestartingText.Visible = (CachedHUDSetting != HUDDisplayOptions.None && RestartingLevel == true);

            if (CachedHUDSetting == HUDDisplayOptions.None)
            {
                return;
            }

            if (CachedHUDSetting == HUDDisplayOptions.Time || CachedHUDSetting == HUDDisplayOptions.ControlsTime)
            {
                TimeText.Position = new Vector2(TimeText.Position.X, TimeText.Position.Y - TimeYOffset);
                TimeIcon.Position = new Vector2(TimeIcon.Position.X, TimeIcon.Position.Y - TimeYOffset);
            }
        }

        private void OnKeyboardMappingsChanged(in int kbIndex)
        {
            InputHandler inputHandler = Input.GetInputHandler(kbIndex);
            if (inputHandler == null)
            {
                return;
            }

            //Update controls text
            PauseText.Text = $"{UtilityGlobals.GetKeyStrForInput(0, InputActions.Pause)}: Pause";
            RestartText.Text = $"{UtilityGlobals.GetKeyStrForInput(0, InputActions.Restart)}(Hold): Restart";

            if (UndoText != null)
                UndoText.Text = $"{UtilityGlobals.GetKeyStrForInput(0, InputActions.Undo)}: Undo";
        }

        public void Render()
        {
            if (MovesText.Visible == true)
            {
                MovesText.Render();
            }

            if (TimeText.Visible == true)
            {
                TimeText.Render();
            }

            if (IsRestartingText.Visible == true)
            {
                IsRestartingText.Render();
            }

            if (CurAlphaTime > 0d)
            {
                if (PauseText.Visible == true)
                    PauseText.Render();
                if (RestartText.Visible == true)
                    RestartText.Render();
                if (UndoText != null && UndoText.Visible == true)
                    UndoText.Render();
            }

            if (MovesIcon.Visible == true)
            {
                MovesIcon.Render();
            }

            if (TimeIcon.Visible == true)
            {
                TimeIcon.Render();
            }
        }
    }
}