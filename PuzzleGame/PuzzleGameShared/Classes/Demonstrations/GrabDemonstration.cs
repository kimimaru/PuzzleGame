/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Shows how to grab.
    /// </summary>
    public class GrabDemonstration : DemonstrationBase
    {
        private double GrabTime = 500d;
        private double ElapsedTime = 0d;

        private bool Grabbed = false;

        private BlankState blankState = new BlankState();

        public GrabDemonstration(in Vector2 playerPosition)
        {
            Echidna echidna = new Echidna(blankState);
            echidna.FacingDir = Direction.Right;

            ObjectInitInfo objInit = new ObjectInitInfo();
            objInit.Position = playerPosition - Context.LevelSpace.TileSizeHalf;

            echidna.Initialize(Context, objInit);

            objInit.Position += Context.LevelSpace.TileSizeHalf;

            objInit.Position.X += 16;
            objInit.Position.Y -= 16;
            objInit.Properties = new Dictionary<string, string>() { { LevelGlobals.BlockPatternKey, "none" } };

            Block b = LevelGlobals.CreateBlock(Context, objInit);

            Context.AddObject(echidna);
            Context.AddObject(b);
        }

        private void OnPlayerStateChanged(PlayerStateMachine newState)
        {
            Context.Player.StateChangedEvent -= OnPlayerStateChanged;

            if (Context.Player.PlayerState == PlayerStates.Grab)
            {
                blankState.PlayIdleAnim = false;
                Context.Player.ChangeState(blankState);
            }
        }

        public override void CleanUp()
        {
            Context.Player.StateChangedEvent -= OnPlayerStateChanged;

            blankState = null;

            base.CleanUp();
        }

        public override void Update()
        {
            Context.Player.Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= GrabTime)
            {
                if (Grabbed == false)
                {
                    Context.Player.StateChangedEvent -= OnPlayerStateChanged;
                    Context.Player.StateChangedEvent += OnPlayerStateChanged;

                    Context.Player.ChangeState(new BlockGrabState(Context.Blocks[0], true, false));

                    Grabbed = true;
                }
                else
                {
                    Context.Player.StateChangedEvent -= OnPlayerStateChanged;

                    blankState.PlayIdleAnim = true;
                    Context.Player.ChangeState(blankState);

                    Grabbed = false;
                }

                ElapsedTime = 0d;
            }
        }

        public override void Render()
        {
            Context.Player.Render();
            Context.Blocks[0].Render();
        }
    }
}