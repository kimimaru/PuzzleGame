/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The base class for demonstrations showing off actions.
    /// </summary>
    public abstract class DemonstrationBase : ICleanup, IUpdateable
    {
        public PlayableContext Context = new PlayableContext(new PlayableSpace(new Vector2(32, 32), RenderingGlobals.BaseResolutionWidth, RenderingGlobals.BaseResolutionHeight));
        public Camera2D Camera = null;

        public DemonstrationBase()
        {
            
        }

        public virtual void CleanUp()
        {
            Camera?.CleanUp();

            for (int i = 0; i < Context.LevelCollision.Count; i++)
            {
                Context.LevelCollision[i].CleanUp();
            }
        }

        /// <summary>
        /// Updates the demonstration.
        /// This is abstract since each demonstration is different.
        /// </summary>
        public abstract void Update();

        /// <summary>
        /// Renders the demonstration.
        /// This is abstract since each demonstration will decide which parts of the playable context it needs to render.
        /// </summary>
        public abstract void Render();
    }
}