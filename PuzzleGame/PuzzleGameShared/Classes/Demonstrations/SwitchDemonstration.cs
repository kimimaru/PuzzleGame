/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Shows how switches work.
    /// </summary>
    public class SwitchDemonstration : DemonstrationBase
    {
        private enum SwitchDemoStates
        {
            WaitOff, WalkOn, WaitOn, WalkOff
        }

        private SwitchDemoStates State = SwitchDemoStates.WaitOff;
        private BlankState blankState = new BlankState(true);

        private const double WaitOffTime = 800d;
        private const double WaitOnTime = 800d;
        private double ElapsedTime = 0d;

        public SwitchDemonstration(in Vector2 playerPosition)
        {
            Context.LevelSpace.WidthInPixels = (int)Context.LevelSpace.TileSize.X * 4;
            Context.LevelSpace.HeightInPixels = (int)Context.LevelSpace.TileSize.Y;

            Context.SetSurfaceTiles(new SurfaceTileEngine(playerPosition, Context.LevelSpace.TileSize,
                Context.LevelSpace.WidthInPixels / (int)Context.LevelSpace.TileSize.X, Context.LevelSpace.HeightInPixels / (int)Context.LevelSpace.TileSize.Y));

            Echidna echidna = new Echidna(blankState);
            echidna.FacingDir = Direction.Left;
            echidna.MoveSmoothingOptions = PlayerSmoothingOptions.None;
            //echidna.InputPriorityOptions = PlayerInputPriorityOptions.None;

            ObjectInitInfo objInit = new ObjectInitInfo();
            objInit.Position = playerPosition - Context.LevelSpace.TileSizeHalf;

            echidna.Initialize(Context, objInit);

            objInit.Position += Context.LevelSpace.TileSizeHalf;

            objInit.Position.X += 48;
            objInit.Position.Y -= 16;
            objInit.Properties = new Dictionary<string, string>(8);
            objInit.Properties.Add(LevelGlobals.BlockPatternKey, "none");
            objInit.Properties.Add(LevelGlobals.DirectionKey, Direction.Right.ToString());
            objInit.Properties.Add(LevelGlobals.GrabbableKey, "false");
            objInit.Properties.Add(LevelGlobals.IDKey, "a");
            objInit.Properties.Add(LevelGlobals.ShowIDKey, "false");
            objInit.Properties.Add(LevelGlobals.HasSoundKey, "false");

            Block block = LevelGlobals.CreateBlock(Context, objInit);

            objInit.Properties.Add(LevelGlobals.SwitchAmountKey, "1");

            objInit.Position.X = playerPosition.X + 16;

            Switch switchObj = LevelGlobals.CreateSwitch(Context, objInit);

            Context.AddObject(echidna);
            Context.AddObject(block);
            Context.AddObject(switchObj);

            echidna.PostInitialize(Context);
            block.PostInitialize(Context);
            switchObj.PostInitialize(Context);

            Context.Player.StateChangedEvent -= OnPlayerStateChanged;
            Context.Player.StateChangedEvent += OnPlayerStateChanged;
        }

        private void OnPlayerStateChanged(PlayerStateMachine newState)
        {
            if (Context.Player.PlayerState == PlayerStates.Idle)
            {
                Context.Player.ChangeState(blankState);
            }
        }

        public override void CleanUp()
        {
            Context.Player.StateChangedEvent -= OnPlayerStateChanged;

            base.CleanUp();
        }

        public override void Update()
        {
            Context.Switches[0].Update();
            Context.Blocks[0].Update();
            Context.Player.Update();
            Context.Switches[0].QueryTrigger(Context.LevelCollision);

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (State == SwitchDemoStates.WaitOff)
            {
                if (ElapsedTime >= WaitOffTime)
                {
                    Vector2 moveAmt = DirectionUtil.GetVector2ForDir(Direction.Right) * Context.LevelSpace.TileSize;

                    Context.Player.ChangeState(new PlayerMoveState(moveAmt, Context.Player.MoveTime));
                    Context.Player.FacingDir = Direction.Right;
                    State = SwitchDemoStates.WalkOn;
                    ElapsedTime = 0d;
                }
            }
            else if (State == SwitchDemoStates.WaitOn)
            {
                if (ElapsedTime >= WaitOnTime)
                {
                    Vector2 moveAmt = DirectionUtil.GetVector2ForDir(Direction.Left) * Context.LevelSpace.TileSize;

                    Context.Player.ChangeState(new PlayerMoveState(moveAmt, Context.Player.MoveTime));
                    Context.Player.FacingDir = Direction.Left;
                    State = SwitchDemoStates.WalkOff;
                    ElapsedTime = 0d;
                }
            }
            else if (State == SwitchDemoStates.WalkOn || State == SwitchDemoStates.WalkOff)
            {
                if (ElapsedTime >= Context.Player.MoveTime)
                {
                    State = (State == SwitchDemoStates.WalkOn) ? SwitchDemoStates.WaitOn : SwitchDemoStates.WaitOff;
                    ElapsedTime = 0d;
                }
            }
        }

        public override void Render()
        {
            Context.Player.Render();
            Context.Blocks[0].Render();
            Context.Switches[0].Render();
        }
    }
}