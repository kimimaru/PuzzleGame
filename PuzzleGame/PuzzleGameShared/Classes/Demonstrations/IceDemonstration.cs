/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Shows how ice works.
    /// </summary>
    public class IceDemonstration : DemonstrationBase
    {
        private enum IceDemoStates
        {
            Wait, Slide
        }

        private IceDemoStates State = IceDemoStates.Wait;
        private BlankState blankState = new BlankState(true);

        private Texture2D TilesetTex = null;
        private Rectangle IceTileRect = new Rectangle(64, 96, 32, 32);

        private const double WaitTime = 500d;
        private double ElapsedTime = 0d;

        public IceDemonstration(in Vector2 playerPosition)
        {
            Context.LevelSpace.WidthInPixels = (int)Context.LevelSpace.TileSize.X * 4;
            Context.LevelSpace.HeightInPixels = (int)Context.LevelSpace.TileSize.Y;

            Context.SetSurfaceTiles(new SurfaceTileEngine(playerPosition - Context.LevelSpace.TileSizeHalf, Context.LevelSpace.TileSize,
                Context.LevelSpace.WidthInPixels / (int)Context.LevelSpace.TileSize.X, Context.LevelSpace.HeightInPixels / (int)Context.LevelSpace.TileSize.Y));

            Echidna echidna = new Echidna(blankState);
            echidna.FacingDir = Direction.Left;
            echidna.MoveSmoothingOptions = PlayerSmoothingOptions.None;
            //echidna.InputPriorityOptions = PlayerInputPriorityOptions.None;

            ObjectInitInfo objInit = new ObjectInitInfo();
            objInit.Position = playerPosition - Context.LevelSpace.TileSizeHalf;

            echidna.Initialize(Context, objInit);

            Context.SurfaceTiles.GetColRowFromPos(echidna.transform.Position, out int playerCol, out int playerRow);

            for (int i = 1; i < 3; i++)
            {
                Context.SurfaceTiles.ChangeTileAtRowCol(playerCol + i, playerRow, SurfaceTypes.Ice);
            }

            Context.AddObject(echidna);

            echidna.PostInitialize(Context);

            Context.Player.StateChangedEvent -= OnPlayerStateChanged;
            Context.Player.StateChangedEvent += OnPlayerStateChanged;

            TilesetTex = AssetManager.Instance.LoadAsset<Texture2D>(ContentGlobals.LevelTileset, AssetManager.Instance.TiledMapsContent);
        }

        private void OnPlayerStateChanged(PlayerStateMachine newState)
        {
            if (Context.Player.PlayerState == PlayerStates.Idle)
            {
                Context.Player.ChangeState(blankState);
                State = IceDemoStates.Wait;
                ElapsedTime = 0d;
            }
        }

        public override void CleanUp()
        {
            Context.Player.StateChangedEvent -= OnPlayerStateChanged;
            TilesetTex = null;

            base.CleanUp();
        }

        public override void Update()
        {
            Context.Player.Update();

            if (State == IceDemoStates.Wait)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;
                if (ElapsedTime >= WaitTime)
                {
                    Context.Player.FacingDir = DirectionUtil.GetOppositeDirection(Context.Player.FacingDir);
                    Vector2 moveAmt = DirectionUtil.GetVector2ForDir(Context.Player.FacingDir) * Context.LevelSpace.TileSize;

                    Context.Player.ChangeState(new PlayerMoveState(moveAmt, Context.Player.MoveTime));
                    State = IceDemoStates.Slide;
                    ElapsedTime = 0d;
                }
            }
        }

        public override void Render()
        {
            for (int i = 0; i < Context.SurfaceTiles.NumTiles; i++)
            {
                if (Context.SurfaceTiles.GetTile(i) == SurfaceTypes.Ice)
                {
                    Vector2 drawPos = new Vector2(Context.SurfaceTiles.Position.X + (Context.SurfaceTiles.TileSize.X * i), Context.SurfaceTiles.Position.Y);
                    RenderingManager.Instance.DrawSprite(TilesetTex, drawPos, IceTileRect, Color.White, 0f, Vector2.Zero, Vector2.One,
                        SpriteEffects.None, LevelGlobals.BaseTileLayerDepth);
                }
            }

            Context.Player.Render();
        }
    }
}