/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Shows how to walk.
    /// </summary>
    public class WalkDemonstration : DemonstrationBase
    {
        private const double ChangeDirTime = 700d;
        private double ElapsedTime = 0d;

        public WalkDemonstration(in Vector2 playerPosition)
        {
            Echidna echidna = new Echidna(new BlankState());
            echidna.FacingDir = Direction.Down;
            echidna.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Walk);

            Context.AddObject(echidna);

            ObjectInitInfo objInit = new ObjectInitInfo();
            objInit.Position = playerPosition - Context.LevelSpace.TileSizeHalf;

            echidna.Initialize(Context, objInit);
        }

        public override void Update()
        {
            Context.Player.Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= ChangeDirTime)
            {
                Context.Player.FacingDir = DirectionUtil.GetClockwiseDirection(Context.Player.FacingDir);
                ElapsedTime = 0d;
            }
        }

        public override void Render()
        {
            Context.Player.Render();
        }
    }
}