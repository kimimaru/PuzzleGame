/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// Shows how to undo.
    /// </summary>
    public class UndoDemonstration : DemonstrationBase
    {
        /* 1. Grab block
         * 2. Push block
         * 3. Let go of block
         * 4. Undo
         * 5. Repeat
         */

        private enum UndoDemoStates
        {
            Grab, Push, LetGo, Wait, Undo
        }

        private BlankState blankState = new BlankState();

        private Vector2 OrigPlayerPos = Vector2.Zero;
        private Vector2 EndPlayerPos = Vector2.Zero;
        private Vector2 OrigBlockPos = Vector2.Zero;
        private Vector2 EndBlockPos = Vector2.Zero;

        private UndoDemoStates State = UndoDemoStates.Grab;

        private double GrabTime = 500d;
        private double PushTime = 500d;
        private double WaitTime = 300d;
        private double UndoTime = 500d;
        private double ElapsedTime = 0d;

        public UndoDemonstration(in Vector2 playerPosition, BlockPatterns blockPattern, Direction? blockDirection)
        {
            Context.LevelSpace.WidthInPixels = (int)Context.LevelSpace.TileSize.X * 3;
            Context.LevelSpace.HeightInPixels = (int)Context.LevelSpace.TileSize.Y;

            Context.SetSurfaceTiles(new SurfaceTileEngine(playerPosition, Context.LevelSpace.TileSize,
                Context.LevelSpace.WidthInPixels / (int)Context.LevelSpace.TileSize.X, Context.LevelSpace.HeightInPixels / (int)Context.LevelSpace.TileSize.Y));

            Echidna echidna = new Echidna(blankState);
            echidna.FacingDir = Direction.Right;
            echidna.MoveSmoothingOptions = PlayerSmoothingOptions.None;
            //echidna.InputPriorityOptions = PlayerInputPriorityOptions.None;

            ObjectInitInfo objInit = new ObjectInitInfo();
            objInit.Position = playerPosition - Context.LevelSpace.TileSizeHalf;

            echidna.Initialize(Context, objInit);

            objInit.Position += Context.LevelSpace.TileSizeHalf;

            objInit.Position.X += 16;
            objInit.Position.Y -= 16;
            objInit.Properties = new Dictionary<string, string>(3);
            objInit.Properties.Add(LevelGlobals.BlockPatternKey, blockPattern.ToString());
            objInit.Properties.Add(LevelGlobals.HasSoundKey, "false");
            if (blockDirection != null)
            {
                objInit.Properties.Add(LevelGlobals.DirectionKey, blockDirection.ToString());
            }

            Block block = LevelGlobals.CreateBlock(Context, objInit);

            Context.AddObject(echidna);
            Context.AddObject(block);

            echidna.PostInitialize(Context);
            block.PostInitialize(Context);

            OrigPlayerPos = echidna.transform.Position;
            OrigBlockPos = block.transform.Position;
            EndPlayerPos = OrigPlayerPos + new Vector2(32, 0);
            EndBlockPos = OrigBlockPos + new Vector2(32, 0);

            Context.Player.StateChangedEvent -= OnPlayerStateChanged;
            Context.Player.StateChangedEvent += OnPlayerStateChanged;
        }

        private void OnPlayerStateChanged(PlayerStateMachine newState)
        {
            if (Context.Player.PlayerState == PlayerStates.Grab)
            {
                blankState.PlayIdleAnim = false;
                Context.Player.ChangeState(blankState);
                Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Grab);
            }
        }

        public override void CleanUp()
        {
            Context.Player.StateChangedEvent -= OnPlayerStateChanged;

            blankState = null;

            base.CleanUp();
        }

        public override void Update()
        {
            Context.Player.Update();
            Context.Blocks[0].Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (State == UndoDemoStates.Grab)
            {
                if (ElapsedTime >= GrabTime)
                {
                    Context.Player.ChangeState(new BlockGrabState(Context.Blocks[0], true, false));
                    State = UndoDemoStates.Push;
                    ElapsedTime = 0d;
                }
            }
            else if (State == UndoDemoStates.Push)
            {
                if (ElapsedTime >= PushTime)
                {
                    Vector2 moveAmt = DirectionUtil.GetVector2ForDir(Context.Player.FacingDir) * Context.LevelSpace.TileSize;

                    Context.Blocks[0].ChangeState(new BlockMoveState(Context.Blocks[0], moveAmt, Context.Player.MoveTime, false));
                    Context.Player.ChangeState(new PlayerBlockMoveState(Context.Blocks[0], moveAmt, Context.Player.MoveTime));

                    State = UndoDemoStates.LetGo;
                    ElapsedTime = 0d;
                }
            }
            else if (State == UndoDemoStates.LetGo)
            {
                if (ElapsedTime >= Context.Player.MoveTime)
                {
                    blankState.PlayIdleAnim = true;
                    Context.Player.ChangeState(blankState);

                    State = UndoDemoStates.Wait;
                    ElapsedTime = 0d;
                }
            }
            else if (State == UndoDemoStates.Wait)
            {
                if (ElapsedTime >= WaitTime)
                {
                    State = UndoDemoStates.Undo;
                    ElapsedTime = 0d;
                }
            }
            else
            {
                if (ElapsedTime >= UndoTime)
                {
                    Context.Player.transform.Position = OrigPlayerPos;
                    Context.Blocks[0].transform.Position = OrigBlockPos;

                    State = UndoDemoStates.Grab;
                    ElapsedTime = 0d;
                }
                else
                {
                    Context.Player.transform.Position = Interpolation.Interpolate(EndPlayerPos, OrigPlayerPos, ElapsedTime / UndoTime, Interpolation.InterpolationTypes.CubicInOut);
                    Context.Blocks[0].transform.Position = Interpolation.Interpolate(EndBlockPos, OrigBlockPos, ElapsedTime / UndoTime, Interpolation.InterpolationTypes.CubicInOut);
                }
            }
        }

        public override void Render()
        {
            Context.Player.Render();
            Context.Blocks[0].Render();
        }
    }
}