/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// Shows how mud works.
    /// </summary>
    public class MudDemonstration : DemonstrationBase
    {
        /* 1. Walk forward
         * 2. Grab block
         * 3. Pull block
         * 4. Undo
         * 5. Repeat
         */

        private enum MudDemoStates
        {
            WaitWalk, Walk, Grab, Pull, Wait, Undo
        }

        private BlankState blankState = new BlankState();

        private Texture2D TilesetTex = null;
        private Rectangle MudTileRect = new Rectangle(0, 96, 32, 32);

        private Vector2 OrigPlayerPos = Vector2.Zero;
        private Vector2 EndPlayerPos = Vector2.Zero;
        private Vector2 OrigBlockPos = Vector2.Zero;
        private Vector2 EndBlockPos = Vector2.Zero;

        private MudDemoStates State = MudDemoStates.WaitWalk;

        private double WaitTime = 300d;
        private double WaitGrabAdditionalTime = 100d;
        private double GrabTime = 500d;
        private double WaitLetGoAdditionalTime = 100d;
        private double UndoTime = 500d;
        private double ElapsedTime = 0d;

        private double PlayerMudMoveTime = 0d;

        public MudDemonstration(in Vector2 playerPosition)
        {
            Context.LevelSpace.WidthInPixels = (int)Context.LevelSpace.TileSize.X * 3;
            Context.LevelSpace.HeightInPixels = (int)Context.LevelSpace.TileSize.Y;

            Context.SetSurfaceTiles(new SurfaceTileEngine(playerPosition - Context.LevelSpace.TileSizeHalf, Context.LevelSpace.TileSize,
                Context.LevelSpace.WidthInPixels / (int)Context.LevelSpace.TileSize.X, Context.LevelSpace.HeightInPixels / (int)Context.LevelSpace.TileSize.Y));

            Echidna echidna = new Echidna(blankState);
            echidna.FacingDir = Direction.Right;
            echidna.MoveSmoothingOptions = PlayerSmoothingOptions.None;
            //echidna.InputPriorityOptions = PlayerInputPriorityOptions.None;

            ObjectInitInfo objInit = new ObjectInitInfo();
            objInit.Position = playerPosition - Context.LevelSpace.TileSizeHalf;

            echidna.Initialize(Context, objInit);

            objInit.Position += Context.LevelSpace.TileSizeHalf;

            Context.SurfaceTiles.GetColRowFromPos(playerPosition, out int playerCol, out int playerRow);

            for (int i = 0; i < 3; i++)
            {
                Context.SurfaceTiles.ChangeTileAtRowCol(playerCol + i, playerRow, SurfaceTypes.Mud);
            }

            objInit.Position.X += 48;
            objInit.Position.Y -= 16;
            objInit.Properties = new Dictionary<string, string>(2);
            objInit.Properties.Add(LevelGlobals.BlockPatternKey, "none");
            objInit.Properties.Add(LevelGlobals.HasSoundKey, "false");

            Block block = LevelGlobals.CreateBlock(Context, objInit);

            Context.AddObject(echidna);
            Context.AddObject(block);

            echidna.PostInitialize(Context);
            block.PostInitialize(Context);

            OrigPlayerPos = echidna.transform.Position;
            OrigBlockPos = block.transform.Position;
            EndPlayerPos = OrigPlayerPos;
            EndBlockPos = OrigBlockPos + new Vector2(-32, 0);

            Context.Player.StateChangedEvent -= OnPlayerStateChanged;
            Context.Player.StateChangedEvent += OnPlayerStateChanged;

            PlayerMudMoveTime = Context.Player.GetMoveTimeWithModifier(Context.Player.MudTimeModifier);
            TilesetTex = AssetManager.Instance.LoadAsset<Texture2D>(ContentGlobals.LevelTileset, AssetManager.Instance.TiledMapsContent);
        }

        private void OnPlayerStateChanged(PlayerStateMachine newState)
        {
            if (Context.Player.PlayerState == PlayerStates.Idle)
            {
                blankState.PlayIdleAnim = true;
                Context.Player.ChangeState(blankState);
            }
            else if (Context.Player.PlayerState == PlayerStates.Grab)
            {
                blankState.PlayIdleAnim = false;
                Context.Player.ChangeState(blankState);
                Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Grab);
            }
        }

        public override void CleanUp()
        {
            Context.Player.StateChangedEvent -= OnPlayerStateChanged;

            blankState = null;
            TilesetTex = null;

            base.CleanUp();
        }

        public override void Update()
        {
            Context.Player.Update();
            Context.Blocks[0].Update();
            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (State == MudDemoStates.WaitWalk)
            {
                if (ElapsedTime >= WaitTime)
                {
                    Context.Player.ChangeState(new PlayerMoveState(new Vector2(Context.LevelSpace.TileSize.X, 0f), PlayerMudMoveTime));
                    State = MudDemoStates.Walk;
                    ElapsedTime = 0d;
                }
            }
            else if (State == MudDemoStates.Walk)
            {
                if (ElapsedTime >= (PlayerMudMoveTime + WaitGrabAdditionalTime))
                {
                    Context.Player.ChangeState(new BlockGrabState(Context.Blocks[0], true, false));
                    State = MudDemoStates.Grab;
                    ElapsedTime = 0d;
                }
            }
            else if (State == MudDemoStates.Grab)
            {
                if (ElapsedTime >= GrabTime)
                {
                    Vector2 moveAmt = -DirectionUtil.GetVector2ForDir(Context.Player.FacingDir) * Context.LevelSpace.TileSize;
                    
                    Context.Blocks[0].ChangeState(new BlockMoveState(Context.Blocks[0], moveAmt, PlayerMudMoveTime, false));
                    Context.Player.ChangeState(new PlayerBlockMoveState(Context.Blocks[0], moveAmt, PlayerMudMoveTime));

                    State = MudDemoStates.Pull;
                    ElapsedTime = 0d;
                }
            }
            else if (State == MudDemoStates.Pull)
            {
                if (ElapsedTime >= (PlayerMudMoveTime + WaitLetGoAdditionalTime))
                {
                    blankState.PlayIdleAnim = true;
                    Context.Player.ChangeState(blankState);

                    State = MudDemoStates.Wait;
                    ElapsedTime = 0d;
                }
            }
            else if (State == MudDemoStates.Wait)
            {
                if (ElapsedTime >= WaitTime)
                {
                    State = MudDemoStates.Undo;
                    ElapsedTime = 0d;
                }
            }
            else
            {
                if (ElapsedTime >= UndoTime)
                {
                    Context.Player.transform.Position = OrigPlayerPos;
                    Context.Blocks[0].transform.Position = OrigBlockPos;

                    State = MudDemoStates.WaitWalk;
                    ElapsedTime = 0d;
                }
                else
                {
                    Context.Player.transform.Position = Interpolation.Interpolate(EndPlayerPos, OrigPlayerPos, ElapsedTime / UndoTime, Interpolation.InterpolationTypes.CubicInOut);
                    Context.Blocks[0].transform.Position = Interpolation.Interpolate(EndBlockPos, OrigBlockPos, ElapsedTime / UndoTime, Interpolation.InterpolationTypes.CubicInOut);
                }
            }
        }

        public override void Render()
        {
            for (int i = 0; i < Context.SurfaceTiles.NumTiles; i++)
            {
                if (Context.SurfaceTiles.GetTile(i) == SurfaceTypes.Mud)
                {
                    Vector2 drawPos = new Vector2(Context.SurfaceTiles.Position.X + (Context.SurfaceTiles.TileSize.X * i), Context.SurfaceTiles.Position.Y);
                    RenderingManager.Instance.DrawSprite(TilesetTex, drawPos, MudTileRect, Color.White, 0f, Vector2.Zero, Vector2.One,
                        SpriteEffects.None, LevelGlobals.BaseTileLayerDepth);
                }
            }

            Context.Player.Render();
            Context.Blocks[0].Render();
        }
    }
}