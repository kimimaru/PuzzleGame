/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Shows how warps work.
    /// </summary>
    public class WarpDemonstration : DemonstrationBase
    {
        private enum WarpDemoStates
        {
            Wait, Teleport
        }

        private WarpDemoStates State = WarpDemoStates.Wait;
        private BlankState blankState = new BlankState(true);

        private const double WaitTime = 800d;
        private double ElapsedTime = 0d;

        public WarpDemonstration(in Vector2 playerPosition)
        {
            Context.LevelSpace.WidthInPixels = (int)Context.LevelSpace.TileSize.X * 2;
            Context.LevelSpace.HeightInPixels = (int)Context.LevelSpace.TileSize.Y;

            Context.SetSurfaceTiles(new SurfaceTileEngine(playerPosition, Context.LevelSpace.TileSize,
                Context.LevelSpace.WidthInPixels / (int)Context.LevelSpace.TileSize.X, Context.LevelSpace.HeightInPixels / (int)Context.LevelSpace.TileSize.Y));

            Echidna echidna = new Echidna(blankState);
            echidna.FacingDir = Direction.Right;
            echidna.MoveSmoothingOptions = PlayerSmoothingOptions.None;
            //echidna.InputPriorityOptions = PlayerInputPriorityOptions.None;

            ObjectInitInfo objInit = new ObjectInitInfo();
            objInit.Position = playerPosition - Context.LevelSpace.TileSizeHalf;

            echidna.Initialize(Context, objInit);

            objInit.Position += Context.LevelSpace.TileSizeHalf;

            Context.LevelSpace.GetTileColRol(echidna.transform.Position, out int warpCol, out int warpRow);

            objInit.Properties = new Dictionary<string, string>(3);
            objInit.Properties.Add(LevelGlobals.HasSoundKey, "false");
            objInit.Properties.Add(LevelGlobals.WarpTileXKey, warpCol.ToString());
            objInit.Properties.Add(LevelGlobals.WarpTileYKey, warpRow.ToString());

            objInit.Position.X += 16;
            objInit.Position.Y -= 16;

            Warp warp = LevelGlobals.CreateWarp(Context, objInit);
            warp.SetWarpDestination(echidna.transform.Position);

            Context.AddObject(echidna);
            Context.AddObject(warp);

            echidna.PostInitialize(Context);
            warp.PostInitialize(Context);

            Context.Player.StateChangedEvent -= OnPlayerStateChanged;
            Context.Player.StateChangedEvent += OnPlayerStateChanged;
        }

        private void OnPlayerStateChanged(PlayerStateMachine newState)
        {
            if (Context.Player.PlayerState == PlayerStates.Idle)
            {
                Context.Player.ChangeState(blankState);
                State = WarpDemoStates.Wait;
                ElapsedTime = 0d;
            }
        }

        public override void CleanUp()
        {
            Context.Player.StateChangedEvent -= OnPlayerStateChanged;

            base.CleanUp();
        }

        public override void Update()
        {
            Context.Player.Update();
            Context.Warps[0].Update();
            Context.Warps[0].QueryTrigger(Context.LevelCollision);

            if (Context.Player.PlayerState == PlayerStates.Other && State == WarpDemoStates.Wait)
            {
                ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

                if (ElapsedTime >= WaitTime)
                {
                    Vector2 moveAmt = DirectionUtil.GetVector2ForDir(Direction.Right) * Context.LevelSpace.TileSize;

                    Context.Player.ChangeState(new PlayerMoveState(moveAmt, Context.Player.MoveTime));
                    Context.Player.FacingDir = Direction.Right;
                    State = WarpDemoStates.Teleport;
                    ElapsedTime = 0d;
                }
            }
        }

        public override void Render()
        {
            Context.Player.Render();
            Context.Warps[0].Render();
        }
    }
}