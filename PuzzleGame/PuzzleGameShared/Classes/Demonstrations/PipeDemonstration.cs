/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Shows how pipe blocks work.
    /// </summary>
    public class PipeDemonstration : DemonstrationBase
    {
        public PipeDemonstration(in Vector2 playerPosition)
        {
            Context.LevelSpace.WidthInPixels = (int)Context.LevelSpace.TileSize.X * 5;
            Context.LevelSpace.HeightInPixels = (int)Context.LevelSpace.TileSize.Y;

            Context.SetSurfaceTiles(new SurfaceTileEngine(playerPosition, Context.LevelSpace.TileSize,
                Context.LevelSpace.WidthInPixels / (int)Context.LevelSpace.TileSize.X, Context.LevelSpace.HeightInPixels / (int)Context.LevelSpace.TileSize.Y));

            ObjectInitInfo objInit = new ObjectInitInfo();
            objInit.Position = playerPosition - Context.LevelSpace.TileSizeHalf;

            //Player object
            Echidna echidna = new Echidna(new BlankState(true));
            echidna.FacingDir = Direction.Right;

            echidna.Initialize(Context, objInit);

            objInit.Position += Context.LevelSpace.TileSizeHalf;

            //Pipe Block
            objInit.Position.X += 48f;
            objInit.Position.Y -= 16f;
            objInit.Type = LevelGlobals.PipeBlockType;
            objInit.Properties = new Dictionary<string, string>(4);
            objInit.Properties.Add(LevelGlobals.PipeBlockEntranceDirKey, Direction.Left.ToString());
            objInit.Properties.Add(LevelGlobals.PipeBlockExitDirKey, Direction.Right.ToString());
            objInit.Properties.Add(LevelGlobals.HasSoundKey, "false");

            Block block = LevelGlobals.CreateBlock(Context, objInit);

            //Mole
            objInit.Position.X += 64f;
            objInit.Properties.Add(LevelGlobals.DirectionKey, Direction.Left.ToString());

            RockShooter mole = LevelGlobals.CreateRockShooter(Context, objInit);

            Context.AddObject(echidna);
            Context.AddObject(block);
            Context.AddObject(mole);

            echidna.PostInitialize(Context);
            block.PostInitialize(Context);
            mole.PostInitialize(Context);
        }

        public override void CleanUp()
        {
            base.CleanUp();
        }

        public override void Update()
        {
            Context.Blocks[0].Update();
            Context.Player.Update();
            Context.RockShooters[0].Update();
        }

        public override void Render()
        {
            Context.Player.Render();
            Context.Blocks[0].Render();
            Context.RockShooters[0].Render();
        }
    }
}