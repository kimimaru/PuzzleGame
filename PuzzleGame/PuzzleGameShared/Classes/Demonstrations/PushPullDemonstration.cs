/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Shows how to push and pull.
    /// </summary>
    public class PushPullDemonstration : DemonstrationBase
    {
        private double PushTime = 500d;
        private double ElapsedTime = 0d;

        private bool Pushed = false;

        private BlankState blankState = new BlankState();

        public PushPullDemonstration(in Vector2 playerPosition)
        {
            Context.LevelSpace.WidthInPixels = (int)Context.LevelSpace.TileSize.X * 3;
            Context.LevelSpace.HeightInPixels = (int)Context.LevelSpace.TileSize.Y;

            Context.SetSurfaceTiles(new SurfaceTileEngine(playerPosition, Context.LevelSpace.TileSize,
                Context.LevelSpace.WidthInPixels / (int)Context.LevelSpace.TileSize.X, Context.LevelSpace.HeightInPixels / (int)Context.LevelSpace.TileSize.Y));

            Echidna echidna = new Echidna(blankState);
            echidna.FacingDir = Direction.Right;
            echidna.MoveSmoothingOptions = PlayerSmoothingOptions.None;
            //echidna.InputPriorityOptions = PlayerInputPriorityOptions.None;

            ObjectInitInfo objInit = new ObjectInitInfo();
            objInit.Position = playerPosition - Context.LevelSpace.TileSizeHalf;

            echidna.Initialize(Context, objInit);

            objInit.Position += Context.LevelSpace.TileSizeHalf;

            objInit.Position.X += 16;
            objInit.Position.Y -= 16;
            objInit.Properties = new Dictionary<string, string>(2) { { LevelGlobals.BlockPatternKey, "none" }, { LevelGlobals.HasSoundKey, "false" } };

            Block block = LevelGlobals.CreateBlock(Context, objInit);

            Context.AddObject(echidna);
            Context.AddObject(block);

            echidna.PostInitialize(Context);
            block.PostInitialize(Context);

            Context.Player.StateChangedEvent -= OnPlayerStateChanged;
            Context.Player.StateChangedEvent += OnPlayerStateChanged;

            Context.Player.ChangeState(new BlockGrabState(Context.Blocks[0], true, false));
        }

        private void OnPlayerStateChanged(PlayerStateMachine newState)
        {
            if (Context.Player.PlayerState == PlayerStates.Grab)
            {
                blankState.PlayIdleAnim = false;
                Context.Player.ChangeState(blankState);
                Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Grab);
                ElapsedTime = 0d;
            }
        }

        public override void CleanUp()
        {
            Context.Player.StateChangedEvent -= OnPlayerStateChanged;

            base.CleanUp();
        }

        public override void Update()
        {
            Context.Player.Update();
            Context.Blocks[0].Update();

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= PushTime)
            {
                Vector2 moveAmt = DirectionUtil.GetVector2ForDir(Context.Player.FacingDir) * Context.LevelSpace.TileSize;

                if (Pushed == true)
                {
                    moveAmt = -moveAmt;   
                }

                Context.Blocks[0].ChangeState(new BlockMoveState(Context.Blocks[0], moveAmt, Context.Player.MoveTime, false));
                Context.Player.ChangeState(new PlayerBlockMoveState(Context.Blocks[0], moveAmt, Context.Player.MoveTime));

                Pushed = !Pushed;
                ElapsedTime = 0d;
            }
        }

        public override void Render()
        {
            Context.Player.Render();
            Context.Blocks[0].Render();
        }
    }
}