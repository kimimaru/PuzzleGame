/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// Shows how paired blocks work.
    /// </summary>
    public class PairedDemonstration : DemonstrationBase
    {
        private double PushTime = 500d;
        private double ElapsedTime = 0d;

        private bool Pushed = false;

        private BlankState blankState = new BlankState();

        public PairedDemonstration(in Vector2 playerPosition)
        {
            Context.LevelSpace.WidthInPixels = (int)Context.LevelSpace.TileSize.X * 5;
            Context.LevelSpace.HeightInPixels = (int)Context.LevelSpace.TileSize.Y;

            Context.SetSurfaceTiles(new SurfaceTileEngine(playerPosition, Context.LevelSpace.TileSize,
                Context.LevelSpace.WidthInPixels / (int)Context.LevelSpace.TileSize.X, Context.LevelSpace.HeightInPixels / (int)Context.LevelSpace.TileSize.Y));

            Echidna echidna = new Echidna(blankState);
            echidna.FacingDir = Direction.Right;
            echidna.MoveSmoothingOptions = PlayerSmoothingOptions.None;
            //echidna.InputPriorityOptions = PlayerInputPriorityOptions.None;

            ObjectInitInfo objInit = new ObjectInitInfo();
            objInit.Position = playerPosition - Context.LevelSpace.TileSizeHalf;

            echidna.Initialize(Context, objInit);

            objInit.Position += Context.LevelSpace.TileSizeHalf;

            objInit.Type = LevelGlobals.PairedBlockType;
            objInit.Position.X += 16;
            objInit.Position.Y -= 16;
            objInit.Properties = new Dictionary<string, string>(4);
            objInit.Properties.Add(LevelGlobals.BlockPatternKey, "square");
            objInit.Properties.Add(LevelGlobals.PairedBlockIDKey, "a");
            objInit.Properties.Add(LevelGlobals.ShowIDKey, "false");
            objInit.Properties.Add(LevelGlobals.HasSoundKey, "false");

            Block pairedA = LevelGlobals.CreateBlock(Context, objInit);

            objInit.Properties[LevelGlobals.BlockPatternKey] = "diamond";
            objInit.Position.X += 64;
            Block pairedB = LevelGlobals.CreateBlock(Context, objInit);

            Context.AddObject(echidna);
            Context.AddObject(pairedA);
            Context.AddObject(pairedB);

            echidna.PostInitialize(Context);
            pairedA.PostInitialize(Context);
            pairedB.PostInitialize(Context);

            Context.Player.StateChangedEvent -= OnPlayerStateChanged;
            Context.Player.StateChangedEvent += OnPlayerStateChanged;

            Context.Player.ChangeState(new BlockGrabState(Context.Blocks[0], true, false));
        }

        private void OnPlayerStateChanged(PlayerStateMachine newState)
        {
            if (Context.Player.PlayerState == PlayerStates.Grab)
            {
                blankState.PlayIdleAnim = false;
                Context.Player.ChangeState(blankState);
                Context.Player.AnimationManager.PlayAnimation(AnimationGlobals.PlayerAnimations.Grab);

                //We let go of the block when the player goes into the blank state
                //Manually grab it again so it stays glowing and the paired block's movement is linked
                Context.Blocks[0].Grab();
                ElapsedTime = 0d;
            }
        }

        public override void CleanUp()
        {
            Context.Player.StateChangedEvent -= OnPlayerStateChanged;

            base.CleanUp();
        }

        public override void Update()
        {
            Context.Player.Update();
            for (int i = 0; i < Context.Blocks.Count; i++)
            {
                Context.Blocks[i].Update();
            }

            ElapsedTime += Time.ElapsedTime.TotalMilliseconds;

            if (ElapsedTime >= PushTime)
            {
                Vector2 moveAmt = DirectionUtil.GetVector2ForDir(Context.Player.FacingDir) * Context.LevelSpace.TileSize;

                if (Pushed == true)
                {
                    moveAmt = -moveAmt;   
                }

                //We let go of the block when the player goes into the blank state
                //Manually grab it again so it stays glowing and the paired block's movement is linked
                Context.Blocks[0].Grab();

                Context.Player.InvokeBlockMoved(Context.Blocks[0], moveAmt, Context.Player.MoveTime);

                Context.Blocks[0].ChangeState(new BlockMoveState(Context.Blocks[0], moveAmt, Context.Player.MoveTime, false));
                Context.Player.ChangeState(new PlayerBlockMoveState(Context.Blocks[0], moveAmt, Context.Player.MoveTime));

                Pushed = !Pushed;
                ElapsedTime = 0d;
            }
        }

        public override void Render()
        {
            Context.Player.Render();
            for (int i = 0; i < Context.Blocks.Count; i++)
            {
                Context.Blocks[i].Render();
            }
        }
    }
}