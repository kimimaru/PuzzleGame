/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PuzzleGame
{
    /// <summary>
    /// Displays the game credits.
    /// </summary>
    public sealed class CreditsDisplay : IUpdateable
    {
        private readonly Color CreditsHeaderColor = new Color(59, 114, 169, 255);
        private readonly Color CreditsSectionHeaderColor = new Color(29, 139, 84);
        private readonly Color CreditsSectionColor = new Color(59, 169, 114);

        private readonly Color OutlineColor = Color.Black;

        private const float StartingYScroll = RenderingGlobals.BaseResolutionHeight - 28f;
        public const float CreditsScrollSpeed = 1f * 60f;
        public const float CreditsScrollSpeedFaster = 3f * 60f;
        public const float CreditsYScrollWrapEnd = 2335f;
        public const float ScrollEndY = (-StartingYScroll - CreditsYScrollWrapEnd);

        private const float SectionYOffset = 80f;

        private readonly Vector2 CreditsStartPos = RenderingGlobals.BaseResolutionHalved;

        public float YScroll { get; private set; } = StartingYScroll;
        public float ScrollSpeed = CreditsScrollSpeed;

        public bool ScrollWrap { get; private set; } = true;

        private SpriteFont Font24px = null;
        private SpriteFont Font18px = null;
        private SpriteFont Font17px = null;
        private SpriteFont Font15px = null;
        private SpriteFont Font12px = null;
        private SpriteFont Font10px = null;
        private Sprite MGLogo = null;

        #region Credits Strings

        private const string CreditsText = "Maze Burrow Credits";
        private const string TopHeader = "Concept, Programming, Development";
        private const string TopAuthor = "Thomas \"Kimimaru\" Deeb";

        private const string GfxHeader = "Art & Graphics";
        private const string GfxAuthor = "zaicuch (characters, objects)";
        private const string GfxAuthor2 = "jeimansutrisman (tiles)";
        private const string GfxAuthor3 = "pzuh88 (logo)";

        private const string MusicHeader = "Music";
        private const string MusicName1 = "\"Puzzle Caves\"";
        private const string MusicName2 = "\"Puzzle Dreams\"";
        private const string MusicName3 = "\"Puzzle Dreams 2\"";
        private const string MusicName4 = "\"Digital Dreaming\"";
        private const string MusicName5 = "\"Puzzle Action\"";
        private const string MusicName6 = "\"Puzzle Action 2\"";
        private const string MusicName7 = "\"Puzzle Action 3\"";
        private const string MusicName8 = "\"Brain Teaser\"";
        private const string MusicName9 = "\"Quirky Puzzler\"";
        private const string MusicName10 = "\"Light Puzzles\"";
        private const string MusicName11 = "\"Cool Puzzle Groovin'\"";
        private const string MusicName12 = "\"Building Stuff\"";
        private const string MusicName13 = "\"Bounce Light\"";
        private const string MusicName14 = "\"Mellow Puzzler\"";
        private const string MusicName15 = "\"Puzzle Madness\"";
        private const string MusicAuthor = "by Eric Matyas";
        private const string MusicSite = "www.soundimage.org";

        private const string SoundHeader = "Sound Effects";
        private const string SoundCredit1 = "\"GameTinyWarp\" by RICHERlandTV,\nused under CC BY 3.0 / shortened from original";
        private const string SoundCredit2 = "\"Switch click 4\" by Adam_N";
        private const string SoundCredit3 = "\"sf3-sfx-menu-select\" by broumbroum,\nused under CC BY 3.0 / intro shortened from original";
        private const string SoundCredit4 = "\"PowerUp\" by kianda / shortened from original";
        private const string SoundCredit5 = "\"Success2\" by kagateni / shortened from original";
        private const string SoundCredit6 = "\"Invalid\" by VincentM400,\nused under CC BY 3.0 / shortened from original";
        private const string SoundCredit7 = "\"Game Over Arcade\" by myfox14";
        private const string SoundCredit8 = "\"shoot\" by Leszek_Szary";
        private const string SoundCredit9 = "\"stonebreak01\" by WIM,\nused under CC BY 3.0 / shortened from original";
        private const string SoundCredit10 = "\"Warping\" by LloydEvans09,\nused under CC BY 3.0 / shortened from original";
        private const string SoundCredit11 = "\"8-Bit Rumbling\" by Terry93D,\ntrimmed to loop";
        private const string SoundCredit12 = "\"tink01\" by wjoojoo,\nused under CC BY 3.0 / shortened from original";
        private const string SoundCredit13 = "\"8bit Question Mark\" by plasterbrain";
        private const string SoundCredit14 = "\"Cartoon UI Back/Cancel\" by plasterbrain,\npitch lowered from original";
        private const string SoundCredit15 = "\"Jumping SFX\" by Lefty_Studios /\nshortened from original";
        private const string SoundCredit16 = "\"Completion Sound.\" by HaelDB";
        private const string SoundCredit17 = "\"Ambient Pulse Noise\" by Gobusto, used under\nCC BY-SA 3.0";
        private const string SoundCredit18 = "\"Jingle_Win_Synth_00\" by LittleRobotSoundFactory\n used under CC BY 3.0 / shortened from original";
        private const string SoundCredit19 = "Sound Effects by Nicole Marie T";

        private const string SpecialThanksHeader = "Special Thanks";
        private const string SpecialThanksName1 = "Alex Deeb";
        private const string SpecialThanksName2 = "Max Schultz";
        private const string SpecialThanksName3 = "Taximadish";
        private const string SpecialThanksName4 = "All testers";

        private const string ThanksText = "Thanks for playing!";

        #endregion

        public CreditsDisplay()
        {
            Font24px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont24px);
            Font18px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont18px);
            Font17px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont17px);
            Font15px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont15px);
            Font12px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont12px);
            Font10px = AssetManager.Instance.LoadFont(ContentGlobals.DefaultFont10px);
            Font10px.LineSpacing = 24;

            MGLogo = new Sprite(AssetManager.Instance.LoadTexture(ContentGlobals.BGIcons), ContentGlobals.MonoGameLogoRect);
        }

        public CreditsDisplay(in bool shouldScrollWrap) : this()
        {
            SetScrollWrap(shouldScrollWrap);
        }

        public void SetScrollWrap(in bool shouldScrollWrap)
        {
            ScrollWrap = shouldScrollWrap;
        }

        public void Reset()
        {
            YScroll = StartingYScroll;
            ScrollSpeed = CreditsScrollSpeed;
        }

        public void Update()
        {
            float scrollSpeed = ScrollSpeed * (float)Time.ElapsedTime.TotalSeconds;

            if (ScrollWrap == true)
            {
                YScroll = UtilityGlobals.Wrap(YScroll - scrollSpeed, ScrollEndY, StartingYScroll);
            }
            else
            {
                YScroll = UtilityGlobals.Clamp(YScroll - scrollSpeed, ScrollEndY, StartingYScroll);
            }
        }

        private void DrawCreditsString(SpriteFont font, in string text, in Vector2 pos, in Color color, in float scale)
        {
            RenderingManager.Instance.spriteBatch.DrawStringOutline(2f, OutlineColor, font, text, pos, color, 0f, font.GetCenterOrigin(text), scale,
                SpriteEffects.None, .3f);
        }

        private void DrawCreditsStringLeft(SpriteFont font, in string text, in Vector2 pos, in Color color, in float scale)
        {
            RenderingManager.Instance.spriteBatch.DrawStringOutline(2f, OutlineColor, font, text, pos, color, 0f, font.GetOrigin(text, 0f, .5f), scale,
                SpriteEffects.None, .3f);
        }

        public void RenderCredits()
        {
            Vector2 creditsPos = CreditsStartPos;
            creditsPos.Y = 60f + YScroll;

            DrawCreditsString(Font24px, CreditsText, creditsPos, CreditsHeaderColor, 1f);

            Vector2 staffPos = creditsPos;
            staffPos.Y += SectionYOffset;

            DrawCreditsString(Font15px, TopHeader, staffPos, CreditsSectionHeaderColor, 1f);

            staffPos.Y += 43f;
            DrawCreditsString(Font15px, TopAuthor, staffPos, CreditsSectionColor, 1f);

            Vector2 gfxPos = staffPos + new Vector2(0f, SectionYOffset);

            DrawCreditsString(Font15px, GfxHeader, gfxPos, CreditsSectionHeaderColor, 1f);
            gfxPos.Y += 43f;
            DrawCreditsString(Font15px, GfxAuthor, gfxPos, CreditsSectionColor, 1f);
            gfxPos.Y += 40f;
            DrawCreditsString(Font15px, GfxAuthor2, gfxPos, CreditsSectionColor, 1f);
            gfxPos.Y += 40f;
            DrawCreditsString(Font15px, GfxAuthor3, gfxPos, CreditsSectionColor, 1f);

            Vector2 musicSoundsCreditsPos = gfxPos;
            musicSoundsCreditsPos.Y += SectionYOffset;

            Color musicHeaderColor = CreditsSectionHeaderColor;//new Color(225, 119, 62, 255);

            DrawCreditsString(Font17px, MusicHeader, musicSoundsCreditsPos, musicHeaderColor, 1f);

            Color musicColor = CreditsSectionColor;//new Color(255, 149, 92, 255);

            musicSoundsCreditsPos.Y += 40f;
            DrawCreditsString(Font12px, MusicName1, musicSoundsCreditsPos, musicColor, 1f);

            const float musicSeparator = 34f;

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName2, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName3, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName4, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName5, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName6, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName7, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName8, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName9, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName10, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName11, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName12, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName13, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName14, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicName15, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicAuthor, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += musicSeparator;
            DrawCreditsString(Font12px, MusicSite, musicSoundsCreditsPos, musicColor, 1f);

            musicSoundsCreditsPos.Y += SectionYOffset;

            Color sfxHeaderColor = CreditsSectionHeaderColor;//new Color(165, 42, 42, 255);
            Color sfxColor = CreditsSectionColor;//new Color(sfxHeaderColor.R + 25, sfxHeaderColor.G + 25, sfxHeaderColor.B + 25, 255);
            DrawCreditsString(Font17px, SoundHeader, musicSoundsCreditsPos, sfxHeaderColor, 1f);

            const float xOffset = -220f;
            musicSoundsCreditsPos.X += xOffset;

            musicSoundsCreditsPos.Y += 70f;
            DrawCreditsStringLeft(Font10px, SoundCredit1, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 55f;
            DrawCreditsStringLeft(Font10px, SoundCredit2, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 55f;
            DrawCreditsStringLeft(Font10px, SoundCredit3, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 55f;
            DrawCreditsStringLeft(Font10px, SoundCredit4, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 45f;
            DrawCreditsStringLeft(Font10px, SoundCredit5, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 65f;
            DrawCreditsStringLeft(Font10px, SoundCredit6, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 55f;
            DrawCreditsStringLeft(Font10px, SoundCredit7, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 45f;
            DrawCreditsStringLeft(Font10px, SoundCredit8, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 55f;
            DrawCreditsStringLeft(Font10px, SoundCredit9, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 65f;
            DrawCreditsStringLeft(Font10px, SoundCredit10, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 65f;
            DrawCreditsStringLeft(Font10px, SoundCredit11, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 65f;
            DrawCreditsStringLeft(Font10px, SoundCredit12, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 55f;
            DrawCreditsStringLeft(Font10px, SoundCredit13, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 55f;
            DrawCreditsStringLeft(Font10px, SoundCredit14, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 65f;
            DrawCreditsStringLeft(Font10px, SoundCredit15, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 55f;
            DrawCreditsStringLeft(Font10px, SoundCredit16, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 55f;
            DrawCreditsStringLeft(Font10px, SoundCredit17, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 65f;
            DrawCreditsStringLeft(Font10px, SoundCredit18, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.Y += 55f;
            DrawCreditsStringLeft(Font10px, SoundCredit19, musicSoundsCreditsPos, sfxColor, 1f);

            musicSoundsCreditsPos.X -= xOffset;

            Vector2 specialThanksPos = musicSoundsCreditsPos;
            specialThanksPos.Y += 70;

            Color specialThanksHeaderColor = CreditsSectionHeaderColor;//new Color(95, 175, 95, 255)
            Color specialThanksColor = CreditsSectionColor;//new Color(135, 215, 135, 255)

            DrawCreditsString(Font17px, SpecialThanksHeader, specialThanksPos, specialThanksHeaderColor, 1f);

            specialThanksPos.Y += 40f;
            DrawCreditsString(Font15px, SpecialThanksName1, specialThanksPos, specialThanksColor, 1f);
            specialThanksPos.Y += 40f;
            DrawCreditsString(Font15px, SpecialThanksName2, specialThanksPos, specialThanksColor, 1f);
            specialThanksPos.Y += 40f;
            DrawCreditsString(Font15px, SpecialThanksName3, specialThanksPos, specialThanksColor, 1f);
            specialThanksPos.Y += 40f;
            DrawCreditsString(Font15px, SpecialThanksName4, specialThanksPos, specialThanksColor, 1f);

            //Made with - technologies
            //const string madeWithText = "Made With";

            Vector2 madeWithPos = specialThanksPos + new Vector2(0f, 80f);

            //DrawCreditsString(Font17px, madeWithText, madeWithPos, CreditsSectionHeaderColor, 1f);
            madeWithPos.Y += 40f;

            //Thanks text - always at the bottom
            Vector2 thanksPos = madeWithPos + new Vector2(0f, 160f);

            DrawCreditsString(Font24px, ThanksText, thanksPos, CreditsHeaderColor/*new Color(100, 30, 225, 255)*/, 1f);

            //RenderingManager.Instance.DrawSprite(MGLogo.Tex, madeWithPos + new Vector2(-1f, -1f), MGLogo.SourceRect, Color.Black, 0f, MGLogo.GetOrigin(),
            //    Vector2.One, SpriteEffects.None, .4f);
            //RenderingManager.Instance.DrawSprite(MGLogo.Tex, madeWithPos + new Vector2(1f, -1f), MGLogo.SourceRect, Color.Black, 0f, MGLogo.GetOrigin(),
            //    Vector2.One, SpriteEffects.None, .4f);
            //RenderingManager.Instance.DrawSprite(MGLogo.Tex, madeWithPos + new Vector2(-1f, 1f), MGLogo.SourceRect, Color.Black, 0f, MGLogo.GetOrigin(),
            //    Vector2.One, SpriteEffects.None, .4f);
            //RenderingManager.Instance.DrawSprite(MGLogo.Tex, madeWithPos + new Vector2(1f, 1f), MGLogo.SourceRect, Color.Black, 0f, MGLogo.GetOrigin(),
            //    Vector2.One, SpriteEffects.None, .4f);

            //MonoGame logo
            RenderingManager.Instance.DrawSprite(MGLogo.Tex, madeWithPos, MGLogo.SourceRect, Color.White, 0f, MGLogo.GetOrigin(),
                Vector2.One, SpriteEffects.None, .4f);
        }
    }
}