﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using MonoGame.Extended.Tiled;

namespace PuzzleGame
{
    /// <summary>
    /// The entry point to the engine.
    /// </summary>
    public class Engine : Game
    {
        #region Curious Message

        private const string CuriousMessage =
            "Dear Curious Fellow,\n"
              + "Thank you for your interest in Maze Burrow! As an avid gamer and engineer myself, I like learning new things about my\n"
              + "favorite games and how they work. With that said, if you haven't already, please consider purchasing this game.\n"
              + "I poured my heart and soul into creating this game, and I'd love for you to experience it the way I intended.\n"
              + "I hope you find something interesting while looking through here. Enjoy the game!\n"
              + " - Thomas \"Kimimaru\" Deeb";

        #endregion

        //Delegate and event for losing window focus
        public delegate void OnLostFocus();

        /// <summary>
        /// The event invoked when the window loses focus. This is invoked at the start of the update loop.
        /// </summary>
        public event OnLostFocus LostFocusEvent = null;

        //Delegate and event for regaining window focus
        public delegate void OnRegainedFocus();

        /// <summary>
        /// The event invoked when the window regains focus. This is invoked at the start of the update loop.
        /// </summary>
        public event OnRegainedFocus RegainedFocusEvent = null;

        /// <summary>
        /// Tells if the game window was focused at the start of the update loop.
        /// </summary>
        public bool WasFocused { get; private set; } = false;

        private GraphicsDeviceManager graphics;

        /// <summary>
        /// The game window.
        /// </summary>
        public GameWindow GameWindow => Window;

        public static bool ShouldQuit { get; private set; } = false;

        /// <summary>
        /// The name of this game.
        /// </summary>
        public const string GameName = "Maze Burrow";

        /// <summary>
        /// A string containing the version of the game.
        /// </summary>
        public const string VersionString = "1.0.4";

        /// <summary>
        /// Indicates whether to ignore audio errors. This is true if no audio device can be found at the start of the game.
        /// </summary>
        public static bool IgnoreAudioErrors { get; private set; } = false;

        /// <summary>
        /// The number of worlds in the game.
        /// NOTE: There might be a better way to do this.
        /// </summary>
        public const int NumWorlds = 7;

        public Engine()
        {
            Debug.Log("Starting up engine");

            Debug.Log("Initializing graphics device");

            Debug.Log($"OS: {Debug.DebugGlobals.GetOSInfo()} | Platform: {MonoGame.Framework.Utilities.PlatformInfo.MonoGamePlatform} | Renderer: {MonoGame.Framework.Utilities.PlatformInfo.GraphicsBackend}");

            graphics = new GraphicsDeviceManager(this);

            //Set max frame time to 30 FPS
            Time.MaxElapsedTime = Time.GetTimeSpanFromFPS(30d);

            Window.AllowUserResizing = false;
            IsMouseVisible = true;

            //MonoGame sets x32 MSAA by default if enabled
            //If enabled and we want a lower value, set the value in the PreparingDeviceSettings event
            graphics.PreferMultiSampling = false;

            //Maximize compatibility with Reach; shaders need to specify shader model 4 with level 9.1 for DX9
            graphics.GraphicsProfile = GraphicsProfile.Reach;

            //Make switching to full screen fast but less efficient; we want to switch as fast as possible
            //On top of that, this allows for borderless full screen on DesktopGL
            graphics.HardwareModeSwitch = false;

            //Enable half pixel offset because some text looks off otherwise
            //This is to maintain compatibility with older versions of MonoGame
            graphics.PreferHalfPixelOffset = true;

            graphics.PreparingDeviceSettings -= OnPreparingDeviceSettings;
            graphics.PreparingDeviceSettings += OnPreparingDeviceSettings;

            //See if the config directory exists and create it if it doesn't
            if (DataHandler.CreateConfigDirectory() == true)
            {
                string path = string.Empty;

                try
                {
                    path = System.IO.Path.Combine(ConfigGlobals.GetApplicationDataPath(), ConfigGlobals.ControllerMappingFile);
                }
                catch (Exception combineExc)
                {
                    Debug.LogError($"Failed to combine config paths: {combineExc.Message}");
                    path = string.Empty;
                }

                //Check if the mappings file is already in there
                if (string.IsNullOrEmpty(path) == false && File.Exists(path) == false)
                {
                    string localControllerMappings = string.Empty;

                    try
                    {
                        localControllerMappings = System.IO.Path.Combine(ConfigGlobals.GetInstallDataPath(), ConfigGlobals.ControllerMappingFile);
                    }
                    catch (Exception combineLocalExc)
                    {
                        Debug.LogError($"Failed to combine local config data paths: {combineLocalExc.Message}");
                        localControllerMappings = string.Empty;
                    }

                    //It's not in there, so check if the local mappings file does exist and copy it from here to there if so
                    if (string.IsNullOrEmpty(localControllerMappings) == false && File.Exists(localControllerMappings) == true)
                    {
                        try
                        {
                            File.Copy(ConfigGlobals.ControllerMappingFile, path);
                        }
                        catch (Exception copyExc)
                        {
                            Debug.LogError($"Failed copying input mappings file: {copyExc.Message}");
                        }
                    }
                }
            }

            //Load controller mappings here so the game picks up the controllers as GamePads if they're already plugged in
            //This must be done before the first event loop, otherwise the controller will be added without being registered as a GamePad
            int mappingsAdded = ConfigGlobals.LoadControllerMappingsFromConfig();

            if (mappingsAdded > 0)
            {
                Debug.Log($"Loaded {mappingsAdded} additional controller mapping(s)");
            }
        }

        private void OnPreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            //Prepare any graphics device settings here
            //Note that OpenGL does not provide a way to set the adapter; the driver is responsible for that
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //Set the culture to invariant
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;

            Window.Title = GameName;
            MaxElapsedTime = Time.MaxElapsedTime;

            Debug.Log($"GPU: {GraphicsDevice.Adapter.Description} | Display: {GraphicsDevice.Adapter.CurrentDisplayMode} | Widescreen: {GraphicsDevice.Adapter.IsWideScreen}");

            //Initialize the audio system
            try
            {
                Microsoft.Xna.Framework.Audio.SoundEffect.Initialize();
            }
            catch (Microsoft.Xna.Framework.Audio.NoAudioHardwareException)
            {
                Debug.Log("No audio hardware present. Disabling audio exception logs.");
                IgnoreAudioErrors = true;
            }

            AssetManager.Instance.Initialize(Content);

            //Dispose the original ContentManager since we set up our own and are no longer using it
            Content.Dispose();

            //Load and validate our save data
            DataHandler.LoadAndValidateSaveData();

            //Set version number
            DataHandler.saveData.Version = VersionString;

            //Load and validate input config data
            DataHandler.LoadAndValidateInputConfigData();
            InputGlobals.LoadInputsFromConfigData(DataHandler.inputConfigData);

            //Set saved timestep and vsync settings
            Time.TimeStep = (TimestepSettings)DataHandler.saveData.Settings.Timestep;
            Time.VSyncSetting = (VSyncSettings)UtilityGlobals.Clamp(DataHandler.saveData.Settings.VSync, (int)VSyncSettings.Enabled, (int)VSyncSettings.Disabled);

            //Set up the level description data
            DataHandler.ReloadLevelDescriptionData(NumWorlds);

            RenderingManager.Instance.Initialize(graphics, GameWindow, RenderingGlobals.BaseResolution);

            //Set the resolution using the saved data
            //If it's fullscreen, turn that on, otherwise set the resolution
            if (DataHandler.saveData.Settings.FullscreenOption == (int)FullscreenSettings.On)
            {
                OptionsMenuNew.FullscreenOption.Invoke();
            }
            else
            {
                OptionsMenuNew.GraphicsOptions[UtilityGlobals.Clamp(DataHandler.saveData.Settings.ResOption, 0, OptionsMenuNew.GraphicsOptions.Length - 1)].Invoke();
            }

            SoundManager.Instance.MusicVolume = DataHandler.saveData.Settings.MusicVolume;
            SoundManager.Instance.SoundVolume = DataHandler.saveData.Settings.SoundVolume;

            //Set up the Recordable cache
            SetupRecordableCache();

            //If the unlock bonus screen should show up, show it, otherwise go to the title demo
            if (DataHandler.saveData.Misc.ShowBonusUnlock == true)
            {
                GameStateManager.Instance.ChangeGameState(new BonusUnlockState());
            }
            else
            {
                Cutscene titleDemo = CutsceneLoader.LoadCutscene(CutsceneEnums.TitleDemo);
                titleDemo.Start();
            }
            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            //Save our save data on unload
            DataHandler.SaveSaveData();

            graphics.PreparingDeviceSettings -= OnPreparingDeviceSettings;
            
            SoundManager.Instance.CleanUp();
            AssetManager.Instance.CleanUp();
            GameStateManager.Instance.CleanUp();
            RenderingManager.Instance.CleanUp();

            Debug.DebugCleanup();

            LostFocusEvent = null;
            RegainedFocusEvent = null;
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            PreUpdate(gameTime);

            //This conditional is for enabling frame advance debugging
            if (Debug.DebugPaused == false || Debug.AdvanceNextFrame == true)
                MainUpdate(gameTime);
            
            PostUpdate(gameTime);
        }

        /// <summary>
        /// Any update logic that should occur immediately before the main Update loop
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values</param>
        private void PreUpdate(in GameTime gameTime)
        {
            //Tell if we change window focus state
            bool focused = IsActive;

            //Lost focus
            if (focused == false && WasFocused == true)
            {
                LostFocusEvent?.Invoke();
            }
            //Regained focus
            else if (focused == true && WasFocused == false)
            {
                RegainedFocusEvent?.Invoke();
            }

            //Set focus state
            WasFocused = focused;

            Debug.DebugUpdate();
            Time.UpdateTime(gameTime);
        }

        /// <summary>
        /// The main update loop.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        private void MainUpdate(in GameTime gameTime)
        {
            //Update current game state
            GameStateManager.Instance.Update();
        }
        
        /// <summary>
        /// Any update logic that should occur immediately after the main Update loop.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        private void PostUpdate(in GameTime gameTime)
        {
            SoundManager.Instance.Update();

            //Update mouse visibility
            MouseHider.Update();
            IsMouseVisible = MouseHider.MouseVisible;

            //Frame advance debugging for input
            if (Debug.DebugPaused == false || Debug.AdvanceNextFrame == true)
            {
                MouseInput.UpdateMouseState();
                KeyboardInput.UpdateKeyboardState();
                Input.UpdateInput();
            }

            base.Update(gameTime);

            //Set time step and VSync settings
            IsFixedTimeStep = Time.TimeStep == TimestepSettings.Fixed ? true : false;

            bool vSync = Time.VSyncSetting == VSyncSettings.Enabled ? true : false;

            if (graphics.SynchronizeWithVerticalRetrace != vSync)
            {
                graphics.SynchronizeWithVerticalRetrace = vSync;
                graphics.ApplyChanges();
            }

            MaxElapsedTime = Time.MaxElapsedTime;

            //If we should exit, do so
            if (ShouldQuit == true)
            {
                Exit();
                return;
            }

            /* This should always be at the end of PostUpdate() */
            TargetElapsedTime = Time.GetTimeSpanFromFPS(Time.FPS);

            //Prevent a game freeze if the elapsed time will be greater than the max elapsed time
            if (TargetElapsedTime > MaxElapsedTime)
                TargetElapsedTime = MaxElapsedTime;
        }

        /// <summary>
        /// Anything that should occur immediately before the main Draw method.
        /// </summary>
        private void PreDraw()
        {
            Time.UpdateFrames();
            
            RenderingManager.Instance.StartDraw();
            if (Debug.DebugEnabled == true && Debug.DebugDrawEnabled == true)
            {
                Debug.DebugStartDraw();
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            PreDraw();

            GameStateManager.Instance.Render();
            Debug.DebugDraw();

            base.Draw(gameTime);

            PostDraw();
        }

        /// <summary>
        /// Anything that should occur immediately after the main Draw method.
        /// </summary>
        private void PostDraw()
        {
            RenderingManager.Instance.EndDraw();

            //Draw debug information on top of everything else
            if (Debug.DebugEnabled == true && Debug.DebugDrawEnabled == true)
            {
                Debug.DebugEndDraw();
            }
        }

        /// <summary>
        /// Notifies the engine to terminate the game at the end of the next update loop.
        /// </summary>
        public static void QuitGame()
        {
            ShouldQuit = true;
        }

        /// <summary>
        /// Sets up the Recordable cache for quicker lookups during gameplay.
        /// </summary>
        private void SetupRecordableCache()
        {
            RecordingGlobals.AddTypeIntoCache(typeof(Echidna));

            RecordingGlobals.AddTypeIntoCache(typeof(Block));
            RecordingGlobals.AddTypeIntoCache(typeof(PairedBlock));
            RecordingGlobals.AddTypeIntoCache(typeof(PipeBlock));

            RecordingGlobals.AddTypeIntoCache(typeof(Switch));
            RecordingGlobals.AddTypeIntoCache(typeof(Warp));
            RecordingGlobals.AddTypeIntoCache(typeof(RockShooter));
            RecordingGlobals.AddTypeIntoCache(typeof(Rock));
        }
    }
}
