﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

sampler s0;
float lerpVal;

float4 Grayscale(VertexShaderOutput input) : COLOR
{
	float4 color = tex2D(s0, input.TextureCoordinates);
	float4 avgColor = color;

	//Set the RGB to the average of the RGB components
	avgColor.rgb = (color.r + color.g + color.b) / 3.0f;

	//Lerp based on the lerp value - this lets it move between full-color and grayscale
	color = lerp(color, avgColor, lerpVal) * input.Color;

	return color;
}

technique BasicColorDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL Grayscale();
	}
};