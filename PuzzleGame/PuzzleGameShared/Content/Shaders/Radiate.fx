﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

SamplerState s0;
float2 TextureSize;
float4 RadiateColorMin;
float4 RadiateColor;
float Amount;
float Offset;

float4 Radiate(VertexShaderOutput input) : COLOR
{
	float4 color = tex2D(s0, input.TextureCoordinates);
	float2 uvPix = float2(1 / TextureSize.x, 1 / TextureSize.y);

	//If this pixel is transparent, return the color
	if (color.a == 0)
		return color;

	//Get the minimum texture coordinate by factoring in the Offset (which is in pixels)
	//Wrap with frac
	float2 texCoordMin = frac(uvPix * Offset);

	//Get the maximum texture coordinate, using the min + the Amount (which is also in pixels)
	float2 texCoordMax = frac(texCoordMin + (uvPix * Amount));

	//float2 texCoordMinRight = frac(uvPix * Offset);

	//If this pixel is <= the max tex coord and the max tex coord is less than the min, due to wrapping, then return the color
	//if (input.TextureCoordinates.x <= texCoordMax.x && texCoordMax.x < texCoordMin.x)
	//	return RadiateColor;
	//
	////If this pixel is >= the min tex coord
	//if (input.TextureCoordinates.x >= texCoordMin.x)
	//{
	//	//Then if the pixel is <= the max tex coord or the max tex coord is less than the min tex coord due to wrapping, return the color
	//	if (input.TextureCoordinates.x <= texCoordMax.x || texCoordMax.x < texCoordMin.x)
	//		return RadiateColor;
	//}

	float4 col = color * input.Color;

	//Right half
	float4 newColorRight = (lerp(RadiateColorMin, RadiateColor, (1 - input.TextureCoordinates.x) / 1)) * col;

	//Left half
	float4 newColorLeft = (lerp(RadiateColorMin, RadiateColor, (1 - input.TextureCoordinates.x) / 0.5)) * col;

	float4 newColor = newColorRight;//input.TextureCoordinates.x > 0.5 ? newColorLeft : newColorRight;

	return newColor;
}

technique BasicColorDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL Radiate();
	}
};