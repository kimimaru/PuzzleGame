﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

SamplerState s0;
float2 TextureSize;
float2 PixelOffset;

float4 Scroll(VertexShaderOutput input) : COLOR
{
	float2 uvPix = float2(1 / TextureSize.x, 1 / TextureSize.y);
	return tex2D(s0, input.TextureCoordinates + (uvPix * PixelOffset)) * input.Color;
}

technique BasicColorDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL Scroll();
	}
};