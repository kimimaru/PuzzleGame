﻿/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Maze Burrow
 * Copyright (C) 2020 Thomas "Kimimaru" Deeb (kimimaru@posteo.net)
 */

using System;
using PuzzleGame;

namespace PuzzleGameGL
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        private static CrashHandler crashHandler = null;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Set current working directory
            try
            {
                Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
                //Debug.Log($"Set current working directory to: {Environment.CurrentDirectory}");
            }
            catch (Exception e)
            {
                Debug.LogError($"Attempted to set current directory to AppDomain base directory, but something bad happened: {e.Message}");
            }

            crashHandler = new CrashHandler();

            Debug.Log("Initializing engine");

            using (Engine game = new Engine())
                game.Run();

            crashHandler.CleanUp();
            crashHandler = null;
        }
    }
}
